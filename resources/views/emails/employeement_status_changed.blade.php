<div>
    @if(isset($data['temp']) && $data['temp'] != '')
        {!! $data['temp'] !!}
    @else
    <p>Hi {{ $data['first_name'] }}</p>

    <p>Congratulations! Your employment at Tridhya Tech has been confirmed.</p>

    Regards,<br>
    {{$data['regards']}}
    </p>
    @endif
</div>
