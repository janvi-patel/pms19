<div>
    <p>Hello {{$data['employee_name']}},</p>

    <p>Your leave application from {{$data['leave_start_date']}} to {{$data['leave_end_date']}} has been {{$data['leave_status']}}.</p>
    <p>Kindly <a href="{{url('/leaverequests')}}">click here</a> to see leave request</p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
