<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<div>
    @php 
        $status = config('constant.estimation_status_id');
        $est_status = '';
        $l_t='';
        $c_n='';
        $tech='';
        $p_m='';
        $t_l='';
        $date='';
        $source='';
        $description='';
        if(($data['status'] == 'edit') && (isset($data['oldLead']))){
            if(isset($data['lead_title']) && isset($data['oldLead']['lead_title']) && $data['lead_title'] != $data['oldLead']['lead_title'])
            {
                $l_t='font-weight:bolder;';
            }
            if(isset($data['client_name']) && isset($data['oldLead']['client_name']) && $data['client_name'] != $data['oldLead']['client_name'])
            {
                $c_n='font-weight:bolder;';
            }
            if(isset($data['technology']) && isset($data['oldLead']['technology']) && $data['technology'] != $data['oldLead']['technology'])
            {
                $tech='font-weight:bolder;';
            }
            if(isset($data['project_manager']) && isset($data['oldLead']['project_manager']) && $data['project_manager'] != $data['oldLead']['project_manager'])
            {
                $p_m='font-weight:bolder;';
            }
            if(isset($data['team_leader']) && isset($data['oldLead']['team_leader']) && $data['team_leader'] != $data['oldLead']['team_leader'])
            {
                $t_l='font-weight:bolder;';
            }
            if(isset($data['estimation_end_date']) && isset($data['oldLead']['estimation_end_date']) && $data['estimation_end_date'] != $data['oldLead']['estimation_end_date'])
            {
                $date='font-weight:bolder;';
            }
            if(isset($data['source']) && isset($data['oldLead']['source']) && $data['source'] != $data['oldLead']['source'])
            {
                $source='font-weight:bolder;';
            }
            if(isset($data['description']) && isset($data['oldLead']['description']) && $data['description'] != $data['oldLead']['description'])
            {
                $description='font-weight:bolder;';
            }
            if(isset($data['estimation_status']) && isset($data['oldLead']['estimation_status']) && $data['estimation_status'] != $data['oldLead']['estimation_status'])
            {
                $est_status='font-weight:bolder;';
            }
        }
    @endphp
    <p>Hello ,</p>
    <p>Please find recently @if($data['status'] == 'edit') updated @else added @endif project lead details as mentioned below.</p>
    <div class="container">
        <div class="row" style="width:100%;">
            @if($data['status'] == 'edit')
                <div style="width:48%;float: left;">
                    <h3> old Project Lead Information</h3>
                    <table border="1" cellpadding="0" cellspacing="0" style="width:100%;border-color: #000;border-collapse:collapse;color: #3682b9;">
                        <tbody>
                            <tr>
                                <td style="padding:5px"><b>Lead Name</b></td>
                                <td style="padding:5px">
                                    {{$data['oldLead']['lead_title']}}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;"><b>Client Name</b></td>
                                <td style="padding:5px">{{$data['oldLead']['client_name']}}</td>
                            </tr>
                            <tr>
                                <td style="padding:5px;"><b>Technology</b></td>
                                <td style="padding:5px">
                                    @php $tech_name = (new \App\Helpers\CommonHelper)->getTechnology($data['oldLead']['technology']); @endphp
                                    {{$tech_name["name"]!=null?$tech_name["name"]:'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;"><b>Project Manager</b></td>
                                <td style="padding:5px">
                                    @php $pm = (new \App\Helpers\CommonHelper)->getAnyUserById($data['oldLead']['project_manager']); @endphp
                                    {{$pm["first_name"]!=null?$pm["first_name"].' '.$pm["last_name"]:'N/A'}}
                                </td>
                            </tr>
                            @if(isset($data['oldLead']['team_leader']))
                                <tr>
                                    <td style="padding:5px;"><b>Team Leader</b></td>
                                    <td style="padding:5px">
                                        @php $tl = (new \App\Helpers\CommonHelper)->getAnyUserById($data['oldLead']['team_leader']); @endphp
                                        {{$tl["first_name"]!=null?$tl["first_name"].' '.$tl["last_name"]:'N/A'}}
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td style="padding:5px;"><b>Added By</b></td>
                                <td style="padding:5px">
                                    @php $added_by = (new \App\Helpers\CommonHelper)->getAnyUserById($data['oldLead']['created_by']); @endphp
                                    {{$added_by["first_name"]!=null?$added_by["first_name"].' '.$added_by["last_name"]:'N/A'}}
                                </td>
                            </tr>
                            @if(isset($data['oldLead']['estimation_end_date']))
                                <tr>
                                    <td style="padding:5px;"><b>Estimation End Date</b></td>
                                    <td style="padding:5px">
                                        {{(isset($data['oldLead']['estimation_end_date']) && $data['oldLead']['estimation_end_date'] != '') ?date('d-M-Y',strtotime($data['oldLead']['estimation_end_date'])):'N/A'}}
                                    </td>
                                </tr>
                            @endif
                            @if(isset($data['oldLead']['created_at']))
                                <tr>
                                    <td style="padding:5px;"><b>Created Date</b></td>
                                    <td style="padding:5px">
                                        {{(isset($data['oldLead']['created_at']) && $data['oldLead']['created_at'] != '') ?date('d-M-Y (h:i A)',strtotime($data['oldLead']['created_at'])):'N/A'}}
                                    </td>
                                </tr>
                            @endif
                            @if(isset($data['oldLead']['updated_at']))
                                <tr>
                                    <td style="padding:5px;"><b>Updated Date</b></td>
                                    <td style="padding:5px">
                                        {{(isset($data['oldLead']['updated_at']) && $data['oldLead']['updated_at'] != '') ?date('d-M-Y (h:i A)',strtotime($data['oldLead']['updated_at'])):'N/A'}}
                                    </td>
                                </tr>
                            @endif
                            @if(isset($data['oldLead']['source']) && $data['oldLead']['source'] != 0)
                                <tr>
                                    <td style="padding:5px;"><b>Source</b></td>
                                    <td style="padding:5px">
                                        {{$data['oldLead']['source']==1?"Up-Work":"N/A"}}
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td style="padding:5px;"><b>Lead Status</b></td>
                                <td style="padding:5px;">
                                    {{$status[$data['oldLead']['estimation_status']]}}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:5px;"><b>Lead Description</b></td>
                                <td style="padding:5px;">
                                    {{$data['oldLead']['description']}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @endif
            <div @if($data['status'] == 'edit') style="width:48%;float: right;" @else style="width:100%;" @endif>
                <h3>New Project Lead Information</h3>
                <table border="1" cellpadding="0" cellspacing="0" style="width:100%;border-color: #000;border-collapse:collapse;color: #3682b9;">
                    <tbody>
                        <tr>
                            <td style="padding:5px"><b>Lead Name</b></td>
                            <td style="padding:5px;{{$l_t}}">
                                {{$data['lead_title']}}
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><b>Client Name</b></td>
                            <td style="padding:5px;{{$c_n}}">
                                {{$data['client_name']}}
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><b>Technology</b></td>
                            <td style="padding:5px;{{$tech}}">
                                @php $tech_name = (new \App\Helpers\CommonHelper)->getTechnology($data['technology']); @endphp
                                {{$tech_name["name"]!=null?$tech_name["name"]:'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><b>Project Manager</b></td>
                            <td style="padding:5px;{{$p_m}}">
                                @php $pm = (new \App\Helpers\CommonHelper)->getAnyUserById($data['project_manager']); @endphp
                                {{$pm["first_name"]!=null?$pm["first_name"].' '.$pm["last_name"]:'N/A'}}
                            </td>
                        </tr>
                        @if(isset($data['team_leader']))
                            <tr>
                                <td style="padding:5px;"><b>Team Leader</b></td>
                                <td style="padding:5px;{{$t_l}}">
                                    @php $tl = (new \App\Helpers\CommonHelper)->getAnyUserById($data['team_leader']); @endphp
                                    {{$tl["first_name"]!=null?$tl["first_name"].' '.$tl["last_name"]:'N/A'}}
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <td style="padding:5px;"><b>Added By</b></td>
                            <td style="padding:5px;">
                                @php $added_by = (new \App\Helpers\CommonHelper)->getAnyUserById($data['created_by']); @endphp
                                {{$added_by["first_name"]!=null?$added_by["first_name"].' '.$added_by["last_name"]:'N/A'}}
                            </td>
                        </tr>
                        @if(isset($data['estimation_end_date']))
                            <tr>
                                <td style="padding:5px;"><b>Estimation End Date</b></td>
                                <td style="padding:5px;{{$date}}">
                                    {{(isset($data['estimation_end_date']) && $data['estimation_end_date'] != '') ? date('d-M-Y',strtotime($data['estimation_end_date'])):'N/A'}}
                                </td>
                            </tr>
                        @endif
                        @if(isset($data['source']) && $data['source'])
                            <tr>
                                <td style="padding:5px;"><b>Source</b></td>
                                <td style="padding:5px;{{$source}}">
                                    {{$data['source']==1?"Up-Work":"N/A"}}
                                </td>
                            </tr>
                        @endif
                        @if(isset($data['created_at']))
                            <tr>
                                <td style="padding:5px;"><b>Created Date</b></td>
                                <td style="padding:5px">
                                    {{(isset($data['created_at']) && $data['created_at'] != '') ?date('d-M-Y (h:i A)',strtotime($data['created_at'])):'N/A'}}
                                </td>
                            </tr>
                        @endif
                        @if(isset($data['oldLead']['updated_at']) && isset($data['updated_at']))
                            <tr>
                                <td style="padding:5px;"><b>Updated Date</b></td>
                                <td style="padding:5px">
                                    {{(isset($data['updated_at']) && $data['updated_at'] != '') ?date('d-M-Y (h:i A)',strtotime($data['updated_at'])):'N/A'}}
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <td style="padding:5px;"><b>Lead Status</b></td>
                            <td style="padding:5px;{{$est_status}}">
                                {{$status[$data['estimation_status']]}}
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><b>Lead Description</b></td>
                            <td style="padding:5px;{{$description}}">
                                {{$data['description']}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @if($data['status'] == 'edit')
                <div style="clear: both;"></div>
            @endif
        </div>
    </div>

    @if(isset($data['attachFile']) && count($data['attachFile'])>0)
        <h3>{{count($data['attachFile'])}} Attachment Files</h3>
        <table border="1" cellpadding="0" cellspacing="0" style="width:auto;border-color: #000;border-collapse:collapse;color: #3682b9;">
            <tbody>
                @foreach($data['attachFile'] as $v)
                    <tr>
                        <td style="padding:5px;"><b> =>  <a href="{{asset('upload/leads/'.$v[0])}}" download>{{$v[1]}}</a></b></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    <h5>For More Information Please <a href="{{$data['link']}}">Click Here</a></h5><br>
    <br>
    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
