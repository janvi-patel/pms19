<div>
    {{--  <p>Hello {{$data['first_name']}},</p>  --}}
    <p>"{{$data['chat']}}"</p><br>

    <h5><a href="{{$data['link']}}">Click Here</a> To View This Message</h5><br>

    @if(isset($data['attachFile']) && count($data['attachFile'])>0)
        <h3>{{count($data['attachFile'])}} Attachment Files</h3>
        <table border="1" cellpadding="0" cellspacing="0" style="width:auto;border-color: #000;border-collapse:collapse;color: #3682b9;">
            <tbody>
                @foreach($data['attachFile'] as $v)
                    <tr>
                        <td style="padding:5px;"><b> =>  <a href="{{asset('upload/leads/'.$v[0])}}" download>{{$v[1]}}</a></b></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    <p>
        Regards,<br>
        {{$data['sender']}}
    </p>
</div>