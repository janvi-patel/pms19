<div>
    <p>Hello,</p>

    <p>We have received a early leave request. Please find details as mentioned below.</p>

    <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse;width:100%">
        <tbody>
            <tr>
                <td>Name</td>
                <td>{{$data['employee_name']}}</td>
            </tr>
            <tr>
                <td>Date</td>
                <td>{{$data['date']}}</td>
            </tr>
            <tr>
                <td>Reason</td>
                <td>{{$data['reason']}}</td>
            </tr>
        </tbody>
    </table>

    <p>Kindly <a href="{{url('/earlyLeave/approve/'.$data['id'])}}">click here</a> to see early leave request</p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
