<div>
	<p>
	    <p>Hi {{$data['first_name']}}</p>

	    <p>You have not added your task entries of {{$data['entryDate']}}. <br>
		Kindly add your entries asap.</p>

	    Regards,<br>
            {{$data['regards']}}
    </p>
</div>
