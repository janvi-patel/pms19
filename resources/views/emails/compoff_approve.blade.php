<div>
    <p>Hello {{$data['employee_name']}},</p>

    <p>Your Comp-off request from {{$data['compoff_start_date']}} to {{$data['compoff_end_date']}} has been {{$data['compoff_status']}}.</p>
    <p>Kindly <a href="{{url('/compoffrequests')}}">click here</a> to see compoff request</p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
