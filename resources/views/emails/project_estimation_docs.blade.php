<div>
    <style>
        .tooltip {
          position: relative;
          display: inline-block;
          border-bottom: 1px dotted black;
        }
        
        .tooltip .tooltiptext {
          visibility: hidden;
          width: 120px;
          background-color: black;
          color: #fff;
          text-align: center;
          border-radius: 6px;
          padding: 5px 0;
          
          /* Position the tooltip */
          position: absolute;
          z-index: 1;
          top: -5px;
          left: 105%;
        }
        
        .tooltip:hover .tooltiptext {
          visibility: visible;
        }
        </style>
    <p>Hello ,</p>

	<p>New Documents Uploaded on <b>{{$data['lead_title']}}</b>.</p>

    <p>For More Information Please <a href="{{$data['link']}}">click here</a><p>

    @if(isset($data['attachFile']) && count($data['attachFile'])>0)
        <h3>{{count($data['attachFile'])}} Attachment Files</h3>
        <table border="1" cellpadding="0" cellspacing="0" style="width:auto;border-color: #000;border-collapse:collapse;color: #3682b9;">
            <tbody>
                @foreach($data['attachFile'] as $v)
                    <tr>
                        <td style="padding:5px;"><b> =>  <a href="{{asset('upload/leads/'.$v[0])}}" download>{{$v[1]}}</a></b></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
    
    <p>
        Regards,<br>
        {{$data['sender']}}
    </p>
</div>