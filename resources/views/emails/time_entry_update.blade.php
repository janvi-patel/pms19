<div>
    <p>Hello,</p>

    <p>{{ $data['username']}} has updated {{$data['empname']}}'s time entries for {{date('d-m-Y',strtotime($data['date']))}}</p>
    <p>Entries details are mentioned in the below tables</p>
    <p>Old Data</p>

    <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse;width:100%">
        <thead>
            <th>In Time</th>
            <th>Out Time</th>
        </thead>
        <tbody>
            @foreach ($data['oldEntryData'] as $key => $value)
           
               <tr>
                    <td>{{$value['in_time']}}</td>
                    <td>{{$value['out_time']}}</td>
                </tr>
           @endforeach
            
        </tbody>

    </table>
    <p>New Data</p>

    <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse;width:100%">
        <thead>
            <th>In Time</th>
            <th>Out Time</th>
        </thead>
        <tbody>
            @foreach ($data['newEntryData'] as $key => $value)
               <tr>
                   <td>{{$value['in_time']}}</td>
                    <td>{{$value['out_time']}}</td>
                </tr>
           @endforeach
            
        </tbody>
    </table>
    

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
