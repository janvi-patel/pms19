<div>
    <p>Hello, {{$data['name']}}</p>

    <p>Please find daily summary details as mentioned below. </p>

    <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse;width:100%">
        <tbody>
            <tr>
                <td>Employee Name</td>
                <td>{{$data['name']}}</td>
            </tr>
            <tr>
                <td>Working Date</td>
                <td>{{$data['date']}}</td>
            </tr>
            <tr>
                <td>Woking Hour</td>
                <td>{{$data['working_hour']}}</td>
            </tr>
            <tr>
                <td>Logged Hours</td>
                <td>{{$data['loggeed_hour']}}</td>
            </tr>
        </tbody>
    </table>

    <p>
        Note:- If you have any issue in your working hours or logged hours, please contact <b>hr@tridhya.com</b> within 3 days.  After 3 days no changes will be accepted.
    </p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
