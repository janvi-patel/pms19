<div>
    <p>Hello {{$data['name']}},</p>

	<p>I Have updated the reviewer date for the <b>{{$data['lead_title']}}</b>.</p>

    <p>For More Information Please <a href="{{$data['link']}}">Click Here</a><p>
    <p>
        Regards,<br>
        {{$data['regards']}}
    </p>
</div>