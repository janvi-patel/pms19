<div>
    <p>Hello,</p>

    <p>We have received a comp-off request. Please find details as mentioned below.</p>

    <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse;width:100%">
        <tbody>
            <tr>
                <td>Name</td>
                <td>{{$data['employee_name']}}</td>
            </tr>
            <tr>
                <td>Start Date</td>
                <td>{{$data['compoff_start_date']}}</td>
            </tr>
            <tr>
                <td>End Date</td>
                <td>{{$data['compoff_end_date']}}</td>
            </tr>
            <tr>
                <td>Comp-off Days</td>
                <td>{{$data['compoff_days']}}</td>
            </tr>
            <tr>
                <td>Comp-off Description</td>
                <td>{{$data['compoff_description']}}</td>
            </tr>
        </tbody>
    </table>

    <p>Kindly <a href="{{url('/compoffs/approve/'.$data['id'])}}">click here</a> to see comp-off request</p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
