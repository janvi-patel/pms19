<div>
    <p>Hello,</p>

    <p>We have received a leave request. Please find details as mentioned below.</p>

    <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse;width:100%">
        <tbody>
            <tr>
                <td>Name</td>
                <td>{{$data['employee_name']}}</td>
            </tr>
            <tr>
                <td>Start Date</td>
                <td>{{$data['leave_start_date']}}</td>
            </tr>
            <tr>
                <td>End Date</td>
                <td>{{$data['leave_end_date']}}</td>
            </tr>
            <tr>
                <td>Return Date</td>
                <td>{{$data['return_date']}}</td>
            </tr>
            <tr>
                <td>Leave Days</td>
                <td>{{$data['leave_days']}}</td>
            </tr>
            <tr>
                <td>Reason</td>
                <td>{{$data['reason']}}</td>
            </tr>
        </tbody>
    </table>

    <p>Kindly <a href="{{url('/leaves/approve/'.$data['id'])}}">click here</a> to see leave request</p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
