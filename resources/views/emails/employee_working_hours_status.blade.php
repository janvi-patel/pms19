<div>
    <p>Hi {{ $data['name'] }}</p>

    <p>Your Last months ({{$data['month']}}) average working hours are {{ $data['hours']  }}, which is less than the minimum working hours {{ $data['standard_hours'] }}.</p>
    <p>You salary will be affected according to that.</p>
    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
