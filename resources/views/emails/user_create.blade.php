<div>
    @if(isset($data['temp']) && $data['temp'] != '')
        {!! $data['temp'] !!}
    @else
        <p>Hello {{ $data['first_name'] }},</p>

        <p>Your account has been successfully created .</p>
        <p>Below are login credentials:</p>
        <p>Username: {{ $data['username'] }}</p>
        <p>Password: {{ $data['password'] }}</p>
        <p>Kindly <a href="{{url('/login')}}">click here</a> to login.</p>

        <p>
        Regards,<br>
        {{$data['regards']}}
        </p>
    @endif
</div>
