<div>
    <p>Hello {{$data['employee_name']}},</p>

    <p>Your work from home application from {{$data['leave_start_date']}} to {{$data['leave_end_date']}} has been {{$data['leave_status']}}.</p>
    <p>Kindly <a href="{{url('/workfromhomeequests')}}">click here</a> to see work from home request</p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
