<div>
    <p>Hello {{ $data['name'] }},</p>

    <p>It has been observed from the attendance record that your late comings are increasing frequently. </p>
    <p>You would be aware that our code of conduct does not allow our employee more than 3 late coming in the month.</p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>

</div>
