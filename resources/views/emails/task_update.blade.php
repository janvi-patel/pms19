<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<div>
    <p>Hello, </p>
    <p>Please find recently updated task details as mentioned below.</p>
    <div class="container">
        <div class="row" style="width:100%;">
            <div>
                <table border="1" cellpadding="0" cellspacing="0" style="width:100%;border-color: #000;border-collapse:collapse;color: #3682b9;">
                    <tbody>
                        <tr>
                            <td style="padding:5px"><b>Project Name</b></td>
                            <td style="padding:5px">{{($data['projectName'])?$data['projectName']->project_name	:''}}</td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><b>User Name</b></td>
                            <td style="padding:5px">{{ ($data['userData'])?$data['userData']->first_name.' '.$data['userData']->last_name:'' }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div style="width:48%;float: left;">
                <center><h4><b>Old Task Details</b></h4></center>
                <table border="1" cellpadding="0" cellspacing="0" style="width:100%;border-color: #000;border-collapse:collapse;color: #3682b9;">
                    <tbody>
                        <tr>
                            <td style="padding:5px;"><b>Task Name</b></td>
                            <td style="padding:5px">{{ ($data['oldData'])?$data['oldData']->task_name:'' }}</td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><b>Task Description</b></td>
                            <td style="padding:5px">{{ ($data['oldData'])?$data['oldData']->task_desc:'' }}</td>
                        </tr>
                        <tr>
                            <td style="padding:5px"><b>Billable Hours</b></td>
                            <td style="padding:5px">{{($data['oldData'])?(new \App\Helpers\CommonHelper)->displayTaskTime($data['oldData']->estimated_hours):'' }}</td>
                        </tr>
                         <tr>
                            <td style="padding:5px"><b>Created By</b></td>
                            <td style="padding:5px">{{ ($data['createdName'])?$data['createdName']->first_name.' '.$data['createdName']->last_name:'' }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>   
            <br>
            <div style="width:48%;float: right;">
                <center><h4><b>New Task Details</b></h4></center>
                <table border="1" cellpadding="0" cellspacing="0" style="width:100%;border-color: #000;border-collapse:collapse;color: #3682b9;">
                    <tbody>
                        <tr>
                            <td style="padding:5px;"><b>Task Name</b></td>
                            <td style="padding:5px">{{ ($data['newData'])?$data['newData']->task_name:'' }}</td>
                        </tr>
                        <tr>
                            <td style="padding:5px;"><b>Task Description</b></td>
                            <td style="padding:5px">{{ ($data['newData'])?$data['newData']->task_desc:'' }}</td>
                        </tr>
                        <tr>
                            <td style="padding:5px"><b>Billable Hours</b></td>
                            <td style="padding:5px">{{($data['newData'])?(new \App\Helpers\CommonHelper)->displayTaskTime($data['newData']->estimated_hours):'' }}</td>
                        </tr>
                         <tr>
                            <td style="padding:5px"><b>Updated By</b></td>
                            <td style="padding:5px">{{ ($data['updatedName'])?$data['updatedName']->first_name.' '.$data['updatedName']->last_name:'' }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br>
    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
