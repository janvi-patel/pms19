<div>
    <p>Hi {{ $data['name'] }}</p>

    <p>Your Last months ({{$data['month']}}) average logged hours are {{$data['hours']}}, which is less than the minimum logged hours {{$data['standard_hours']}}.</p>
    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
