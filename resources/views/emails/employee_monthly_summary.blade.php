<div>
    <p>Hello, {{$data['name']}}</p>

    <p>Please find monthly summary details as mentioned below. </p>

    <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse;width:100%">
        <tbody>
            <tr>
                <td>Employee Name</td>
                <td>{{$data['name']}}</td>
            </tr>
            <tr>
                <td>Working Days</td>
                <td>{{$data['totalWorkingDay']}}</td>
            </tr>
            <tr>
                <td>Absent Days</td>
                <td>{{$data['leaveDay']}}</td>
            </tr>
            <tr>
                <td>Average Working Hours</td>
                <td>{{$data['avg_working']}}</td>
            </tr>
            <tr>
                <td>Average Logged Hours</td>
                <td>{{$data['avg_loggeed']}}</td>
            </tr>
        </tbody>
    </table>
    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
