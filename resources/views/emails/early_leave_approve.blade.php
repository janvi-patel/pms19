<div>
    <p>Hello {{$data['employee_name']}},</p>

    <p>Your wearly leave application for {{$data['date']}} has been {{$data['status']}}.</p>
    <p>Kindly <a href="{{url('/earlyLeaveRequest')}}">click here</a> to see early leave request</p>

    <p>
    Regards,<br>
    {{$data['regards']}}
    </p>
</div>
