<div>
    @if(isset($data['temp']) && $data['temp'] != '')
        {!! $data['temp'] !!}
    @else
    <p>Hello {{$data['full_name']}} ,</p>

    <p>Annual performance appraisal and compensation review process are designed to recognize the employee contribution and espousal to company values.</p>

    <p>In line with the above, your performance has been assessed and reviewed recently. In recognition of your performance, your compensation has been revised to Rs. {{$data['new_ctc']}}/- per month(CTC) @if(isset($data['from_date']) && $data['from_date'] != '')effective from {{$data['from_date']}}@endif.</p>

    @if(isset($data['to_date']) && $data['to_date'] != '') <p>Your next increment will be due on {{$data['to_date']}}.</p> @endif

    @if(isset($data['update_designation']) && $data['update_designation'] != '') <p>We would also like to inform you that your current designation is {{$data['designation']}}. Kindly understand your KRAs, roles, and responsibilities as per your designation.</p> @endif

    <p>All other terms and conditions of the employment are as per company policy, as amended from time to time. Compensation details are very strictly confidential matters of the company.</p>

    <p>We look forward to your very active participation and contribution in our journey of scaling newer heights and in making {{$data['regards']}} a better organization.</p>

    <p>Wishing you a happy and rewarding career with {{$data['regards']}}!</p>

    <p>Regards,<br>
        {{$data['regards']}}
    </p>
    @endif
</div>