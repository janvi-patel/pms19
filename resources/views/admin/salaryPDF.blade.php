<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Grids Master Template</title>
    <style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    body,
    table,
    td,
    a {
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
    }

    table,
    td {
        -mso-table-lspace: 0pt;
        -mso-table-rspace: 0pt;
    }

    img {
        --ms-interpolation-mode: bicubic;
    }

    /* RESET STYLES */
    img {
        border: 0;
        outline: none;
        text-decoration: none;
    }

    table {
        border-collapse: collapse !important;
    }

    body {
        margin: 0 !important;
        padding: 0 !important;
        width: 100% !important;
    }

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }

    /* ANDROID CENTER FIX */
    div[style*="margin: 16px 0;"] {
        margin: 0 !important;
    }

    @page {
        margin: 10px;
    }

    @media print {

        body {
            padding: 40px 0;
            ;
        }



        .your-selectors {
            display: none;
        }

        a[href]:after {
            content: " ("attr(href) ")";
        }

        a[href^="#"]:after,
        a[href^="javascript"]:after {
            content: "";
        }

        /* Fix Header and footer class on page */
        .footer,
        .header {
            position: fixed;
            top: 0;
            left: 0;
            height: 40px;
            width: 100%;
            background-color: white;
            margin-left: 25px;
        }

        .footer {
            top: auto;
            bottom: 0;
        }

        @page {
            counter-increment: page;
            counter-reset: page 1;
        }

        .footer:after {
            content: "Page "counter(page) " of "counter(pages);
            color: blue;
            float: right;
        }
    }
    </style>
</head>

<body style="margin:0; padding:0; background-color:#ffffff;">
    <span style="display: block; width: 640px !important; max-width: 768px; height: 1px" class="mobileOff"></span>
    <table align="center" width="768px" border="1" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="">
        <tr>
            <td align="center" valign="top" style="">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF" style="">
                    <tr>
                        <td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                <tr>
                                    <td width="60%" class="mobile" valign="top" style="font-family:Arial, sans-serif; font-weight: 700; font-size: 12px;padding-left:10px;">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="1" class="container">
                                            <tr>
                                                <img src="{{url('upload/company_logo/'.$comp_logo)}}" alt="{{$comp_name}}" width="300">
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <h4 style="margin-bottom: 0px;">{{$comp_name}}</h4>
                                        <span>{{$comp_address}}</span>
                                        <p style="margin-bottom: 0px;">E-Mail :{{$comp_email}}</p>
                                        <span>Contact : {{$comp_contact}}</span><br>
                                        <span>Website : {{$comp_website}}</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="30" style="font-size:30px; line-height:30px;">&nbsp;</td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF" style="">
                    <tr>
                        <td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                <tr>
                                    <td align="center" valign="top" style="font-family:Arial, sans-serif; font-weight: 700; font-size: 12px;">
                                        Salary Slip For The {{$month_year}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
                    <tr>
                        <td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                <tr>
                                    <td width="100%" class="mobile" align="center" valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                            <tr>
                                                <td colspan="4" valign="top" style=" font-size: 12px;font-family: Arial,sans-serif;text-align: center;font-weight: 700;padding: 2px 10px;border-top: 1px solid #000; border-bottom: 1px solid #000;">Employee Details</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Employee Code:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$emp_code}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Bank Name:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-top: 1px solid #000;">{{$bank_name}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Employee Name:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$emp_name}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Bank Account No:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$bank_account_no}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Designation:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$designation}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">PAN No:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$pan_no}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Department:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$department}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">UAN No:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$uan_no}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Date Of Joining:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$joining_date}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">PF NO:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$pf_no}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Status:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$status}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;" nowrap>ESIC No:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$esic_no}}</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td height="20" style="font-size:10px; line-height:10px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                <tr>
                                    <td width="100%" class="mobile" align="center" valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                            <tr>
                                                <td colspan="2" valign="top" style=" font-size: 12px;font-family: Arial,sans-serif;text-align: center;font-weight: 700;padding: 2px 7px;border-right: 1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Leave Details</td>
                                                <td colspan="2" valign="top" style=" font-size: 12px;font-family: Arial,sans-serif;text-align: center;font-weight: 700;padding: 2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;">Attendance Detials</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Leave Issued:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$leave_issue}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Total Day:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$total_days}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Leave Balance:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$leave_balance}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Working Day:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$working_days}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Leave Taken:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$leave_taken}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Present Day:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$present_days}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">EL Encash No:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$el_encash_no}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Leave Without Pay:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$leave_without_pay_no}}</td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Adjusted:</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$adjusted}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                            </tr>
                                            <tr>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Balance C/F</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$balance_cf}}</td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;" nowrap></td>
                                                <td width="25%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td height="20" style="font-size:10px; line-height:10px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                <tr>
                                    <td width="100%" class="mobile" align="center" valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                            <tr>
                                                <td valign="top" style="font-size: 12px;font-family: Arial,sans-serif;text-align: left;font-weight: 700;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;padding: 2px 7px;">Earning</td>
                                                <td valign="top" style="font-size: 12px;font-family: Arial,sans-serif;text-align:right;font-weight: 700;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;padding: 2px 7px;">CTC Amount</td>
                                                <td valign="top" style="font-size: 12px;font-family: Arial,sans-serif;text-align:right;font-weight: 700;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;padding: 2px 7px;">Salary Amt.</td>
                                                <td valign="top" style="font-size: 12px;font-family: Arial,sans-serif;text-align: left;font-weight: 700;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;padding: 2px 7px;">Deduction</td>
                                                <td valign="top" style="font-size: 12px;font-family: Arial,sans-serif;text-align: right;font-weight: 700;padding: 2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Amount</td>
                                            </tr>
                                            <tr>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Basic Salary:</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$basic_salary_ctc}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$basic_salary_amount}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Professional Tax</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$professional_tax}}</td>
                                            </tr> 
                                            <tr>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">House Rent Allow:</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$house_rent_allowance_ctc}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$house_rent_allowance_salary}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Income Tax</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$income_tax}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Other Allownces:</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$other_allownces_ctc}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$other_allownces_salary}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Gratuity</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$gratuity}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Performance Allownces:</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$performance_allowance_ctc}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$performance_allowance}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">PF Employee's Contribution</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$pf_employee_contribution}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Bonus:</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;"></td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$bonus}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">ESI Employee's Contribution</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$esi_employee_contribution}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Other:</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;"></td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$other}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Other Deduction</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$other_deduction}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">EL Encash Amount:</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;"></td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;text-align: right;">{{$el_encash_amount}}</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Leave Without Pay Amount</td>
                                                <td width="20%" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$leave_without_pay_amount}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                            </tr>
                                            <tr>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;"></td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Total Deduction</td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$total_deduction}}</td>
                                            </tr>
                                            <tr style="border-top: 2px solid #000; border-bottom: 2px solid #000;">
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px; font-weight: 700;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;">Total Earning</td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;font-weight: 700;text-align: right;">{{$ctc}}</td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;font-weight: 700;text-align: right;">{{$sumSalAmount}}</td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;border-right:1px solid #000;border-top: 1px solid #000; border-bottom: 1px solid #000;font-weight: 700;">Net Amount</td>
                                                <td width="20%" height="20" valign="top" style="font-family: Arial,sans-serif;font-size: 12px;padding:2px 7px;font-weight: 700;text-align: right;border-top: 1px solid #000; border-bottom: 1px solid #000;">{{$net_amount}}</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
                    </tr>
                </table>                
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF" style="">
                    <tr>
                        <td align="center" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="container">
                                <tr>
                                    <td align="center" valign="top" style="font-family:Arial, sans-serif; font-size: 12px; font-weight: 700">
                                        This salary slip is auto generated by computer which does not require signature.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="10" style="font-size:10px; line-height:10px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>