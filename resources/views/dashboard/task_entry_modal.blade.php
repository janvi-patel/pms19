<div class="card-body">
    <div class="table-responsive">
        <table id="project_details_listing" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>User Name</th>
                    <th>Date</th>
                    <th>Hours</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($projectEntry))
                    @foreach($projectEntry as $projectEntryVal)
                    <tr>
                        <td>{{$projectEntryVal['first_name'].' '.$projectEntryVal['last_name']}}</td>
                        <td>{{date('d-m-Y',strtotime($projectEntryVal['log_date']))}}</td>
                        <td>{{$projectEntryVal['log_hours'] != 0 ?(new \App\Helpers\CommonHelper)->displayTaskTime($projectEntryVal['log_hours']):'00:00'}}</td>
                        <td>{{$projectEntryVal['desc']}}</td>
                    </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="4" class="text-center">No Task Entry</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>