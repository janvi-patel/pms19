@extends('layouts.master')
@section('content')
@section('moduleName')
    Home
@endsection
@php
$leaveStatusArray = config('constant.leave_status');
@endphp
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-4">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div>
            <div class="col-sm-4">
              <ol class="breadcrumb float-sm-center" style="justify-content:center;">
                    <?php if(Auth::user()->employee_status == 3){?>
                      <li style="padding-right:15px;" href="{{asset('css/text-blink.css')}}"><a class="blink" style="color:red"href="{{asset('/doc/exit_clearance_form.docx')}}" download>Download Exit Clearance Form</a></li>
                      <div class="slideUp" id="news-bar">
<marquee direction="left" scrollamount="3" behavior="scroll" onmouseover="this.stop()"  onmouseout="this.start()" style="color:red">
<b>Please submit the laptop and other assets before 6:00 pm on your last day</b></marquee></div>
                    <?php }?>
                  </ol>
                </div>
            <div class="col-sm-4">
                <ol class="breadcrumb float-sm-right">

                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
  <section class="content">
    <div class="container-fluid">
       @if(Auth::user()->designation_id == 19)
       <div class="row">
       
        
        <div class="col-lg-3 col-6">
          <div class="small-box bg-secondary">
            <div class="inner">
              <h3>{{$total_leaves['taken_leaves']}}</h3>
              <p>Leaves Taken</p>
            </div>
            <div class="icon">
              <i class="fas fa-signal"></i>
            </div>
          </div>
      </div>
      <div class="col-lg-3 col-6">
          <div class="small-box bg-success">
            <div class="inner">
              <h3>{{$total_leaves['future_leave_taken']}}</h3>
              <p>Approved Future Leaves</p>
            </div>
            <div class="icon">
              <i class="fas fa-signal"></i>
            </div>
          </div>
      </div>
         <div class="col-lg-3 col-6">
          <div class="small-box bg-secondary">
            <div class="inner">
              <h3>{{Request::is($leaveData['leavePendingToApprove']!='') ? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['leavePendingToApprove'] ) : 0 }}</h3>
              <p>Leave Pending To Approve</p>
            </div>
            <div class="icon">
              <i class="fas fa-signal"></i>
            </div>
            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <div class="small-box bg-warning">
            <div class="inner">

              <h3>{{(Request::is($leaveData['remaningLeave'] != '') && $leaveData['remaningLeave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['remaningLeave'] ) : 0}}</h3>
              <p>Leaves Remaining</p>
            </div>
            <div class="icon">
              <i class="fas fa-chart-line"></i>
            </div>
            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6">
          <div class="small-box bg-warning">
            <div class="inner">

              <h3>{{(Request::is($leaveData['compoffleave'] != '') && $leaveData['compoffleave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['compoffleave'] ) : 0}}</h3>
              <p>Leave Comp Off</p>
            </div>
            <div class="icon">
              <i class="fas fa-chart-line"></i>
            </div>
            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <div class="small-box bg-danger">
            <div class="inner">

              <h3><?php
                $lwpdata = getUserLwpLeave();
                echo $lwpdata['lwpLeaveCount'];
              ?></h3>
              <p>Leave Without Pay</p>
            </div>
            <div class="icon">
              <i class="fas fa-chart-line"></i>
            </div>
            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
          </div>
        </div>
      </div>
      @elseif(Auth::user()->hasRole('Networking'))
        <div class="row">
          <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                  <h3>{{$totalWorkingDay}}</h3>
                <p>Total Working Days</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{isset($late_days)?$late_days:'0'}}</h3>
                <p>Late Days</p>
              </div>
              <div class="icon">
                <i class="fas fa-signal"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
            <div class="col-lg-3 col-6">
                <div class="small-box bg-dark">
                  <div class="inner">
                    <h3>{{$avg_working_hr}}</h3>
                    <p>Monthly Working Hours</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-user"></i>
                  </div>
                  <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
            </div>
            
            
            <div class="col-lg-3 col-6">
                    <div class="small-box bg-secondary">
                      <div class="inner">
                        <h3>{{$total_leaves['taken_leaves']}}</h3>
                        <p>Leaves Taken</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                      <div class="inner">
                        <h3>{{$total_leaves['future_leave_taken']}}</h3>
                        <p>Approved Future Leaves</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
             <div class="col-lg-3 col-6">
              <div class="small-box bg-secondary">
                <div class="inner">
                  <h3>{{Request::is($leaveData['leavePendingToApprove']!='') ? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['leavePendingToApprove'] ) : 0 }}</h3>
                  <p>Leave Pending To Approve</p>
                </div>
                <div class="icon">
                  <i class="fas fa-signal"></i>
                </div>
                <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
              </div>
            </div>
            {{-- </div>
            <div class="row"> --}}
            <div class="col-lg-3 col-6">
                <div class="small-box bg-warning">
                  <div class="inner">

                    <h3>{{(Request::is($leaveData['remaningLeave'] != '') && $leaveData['remaningLeave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['remaningLeave'] ) : 0}}</h3>
                    <p>Leaves Remaining</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-chart-line"></i>
                  </div>
                  <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
            </div>
            <div class="col-lg-3 col-6">
                   <div class="small-box bg-warning">
                      <div class="inner">
                        <h3>{{(Request::is($leaveData['compoffleave'] != '') && $leaveData['compoffleave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['compoffleave'] ) : 0}}</h3>
                        <p> Leave Comp Off </p>
                      </div>
                        <div class="icon">
                         <i class="fas fa-chart-line"></i>
                       </div>
                      <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                  </div>
              </div>
        </div>

       @elseif(Auth::user()->hasRole(config('constant.hr_slug')))
       <div class="row">
           <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ Request::is($totalUser != '') ? $totalUser: 0 }}</h3>
                <p>Total Employees</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
           <div class="col-lg-3 col-6">
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{  (count($todayLeaveListing) > 0 ) ? count($todayLeaveListing): 0 }}</h3>
                <p>Absent Employee</p>
              </div>
              <div class="icon">
                <i class="fas fa-chart-line"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{ Request::is($remaningEntry != '') ? $remaningEntry: 0}}</h3>
                <p>Remaining Time Entries</p>
              </div>
              <div class="icon">
                <i class="fas fa-signal"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
           
            
            <div class="col-lg-3 col-6">
                    <div class="small-box bg-secondary">
                      <div class="inner">
                        <h3>{{$total_leaves['taken_leaves']}}</h3>
                        <p>Leaves Taken</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                      <div class="inner">
                        <h3>{{$total_leaves['future_leave_taken']}}</h3>
                        <p>Approved Future Leaves</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
             <div class="col-lg-3 col-6">
                <div class="small-box bg-secondary">
                  <div class="inner">
                    <h3>{{Request::is($leaveData['leavePendingToApprove']!='') ? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['leavePendingToApprove'] ) : 0 }}</h3>
                    <p>Leave Pending To Approve</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-signal"></i>
                  </div>
                  <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
              </div>
            <div class="col-lg-3 col-6">
                <div class="small-box bg-warning">
                  <div class="inner">
                    <h3>{{(Request::is($leaveData['remaningLeave'] != '') && $leaveData['remaningLeave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['remaningLeave'] ) : 0}}</h3>
                    <p>Leaves Remaining</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-chart-line"></i>
                  </div>
                </div>
            </div>
            <div class="col-lg-3 col-6">
                     <div class="small-box bg-warning">
                        <div class="inner">

                          <h3>{{(Request::is($leaveData['compoffleave'] != '') && $leaveData['compoffleave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['compoffleave'] ) : 0}}</h3>
                          <p> Leave Comp Off</p>
                        </div>
                          <div class="icon">
                           <i class="fas fa-chart-line"></i>
                         </div>
                        <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                      <div class="inner">

                        <h3><?php
                          $lwpdata = getUserLwpLeave();
                          echo $lwpdata['lwpLeaveCount'];
                        ?></h3>
                        <p>Leave Without Pay</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-chart-line"></i>
                      </div>
                      <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                    </div>
                  </div>
       </div>
       @elseif(Auth::user()->hasRole(config('constant.team_leader_slug'))||
            Auth::user()->hasRole(config('constant.project_manager_slug'))||
            Auth::user()->designation_id == 16 )
            <div class="row">
                <div class="col-lg-3 col-6">
                 <div class="small-box bg-info">
                   <div class="inner">
                     <h3>{{Request::is(count($getTeamMember) != '') ? count($getTeamMember): 0}}</h3>
                     <p>Team Members</p>
                   </div>
                   <div class="icon">
                     <i class="fas fa-users"></i>
                   </div>
                   <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                 </div>
               </div>
                <div class="col-lg-3 col-6">
                 <div class="small-box bg-success">
                   <div class="inner">
                     <h3>{{Request::is($getAssignedProj != '') ? $getAssignedProj: 0}}</h3>
                     <p>Assigned Projects</p>
                   </div>
                   <div class="icon">
                     <i class="fas fa-shopping-cart"></i>
                   </div>
                   <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                 </div>
               </div>
                <div class="col-lg-3 col-6">
                 <div class="small-box bg-warning">
                   <div class="inner">
                     <h3>{{Request::is($getActiveProjects != '') ? $getActiveProjects: 0}}</h3>
                     <p>Active Projects</p>
                   </div>
                   <div class="icon">
                     <i class="fas fa-signal"></i>
                   </div>
                   <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                 </div>
               </div>
                 <div class="col-lg-3 col-6">
                     <div class="small-box bg-secondary">
                       <div class="inner">
                        <h3>{{(Request::is($leaveData['remaningLeave'] != '') && $leaveData['remaningLeave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['remaningLeave'] ) : 0}}</h3>
                        <p>Leaves Remaining</p>
                       </div>
                       <div class="icon">
                         <i class="fas fa-user"></i>
                       </div>
                       <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                     </div>
                 </div>
                  <div class="col-lg-3 col-6">
                     <div class="small-box bg-warning">
                        <div class="inner">

                          <h3>{{(Request::is($leaveData['compoffleave'] != '') && $leaveData['compoffleave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['compoffleave'] ) : 0}}</h3>
                          <p>Leave Comp Off</p>
                        </div>
                          <div class="icon">
                           <i class="fas fa-chart-line"></i>
                         </div>
                        <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                      <div class="inner">

                        <h3><?php
                          $lwpdata = getUserLwpLeave();
                          echo $lwpdata['lwpLeaveCount'];
                        ?></h3>
                        <p>Leave Without Pay</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-chart-line"></i>
                      </div>
                      <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                    </div>
                  </div>


            {{-- </div>
            <div class="row"> --}}
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-dark">
                      <div class="inner">
                        <h3>{{$total_leaves['taken_leaves']}}</h3>
                        <p>Leaves Taken</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                      <div class="inner">
                        <h3>{{$total_leaves['future_leave_taken']}}</h3>
                        <p>Approved Future Leaves</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
                 <div class="col-lg-3 col-6">
                    <div class="small-box bg-secondary">
                      <div class="inner">
                        <h3>{{Request::is($leaveData['leavePendingToApprove']!='') ? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['leavePendingToApprove'] ) : 0 }}</h3>
                        <p>Leave Pending To Approve</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                      <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                    </div>
                  </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                      <div class="inner">
                        <h3>{{isset($late_days)?$late_days:'0'}}</h3>
                        <p>Late Days</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
                @if(!(Auth::user()->designation_id == 16))
                    <div class="col-lg-3 col-6">
                      <div class="small-box bg-dark">
                        <div class="inner">
                          <h3>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getNonBillableLoggedHours)}}</h3>
                          <p>Non-billable Logged hours</p>
                        </div>
                        <div class="icon">
                          <i class="fas fa-users"></i>
                        </div>
                        <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                      </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="small-box bg-info">
                          <div class="inner">
                          <h3>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getBillableAssignedHours)}}</h3>
                          <p>Billable assigned hours</p>
                          </div>
                          <div class="icon">
                            <i class="fas fa-users"></i>
                          </div>
                          <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                        </div>
                      </div>
                  @endif
            </div>
       @elseif(Auth::user()->hasRole(config('constant.superadmin_slug'))||
            Auth::user()->hasRole(config('constant.admin_slug')))
       <div class="row">
           <div class="col-lg-3 col-6">
            <div class="small-box bg-secondary">
              <div class="inner">
                <h3>{{ Request::is($totalUser != '') ? $totalUser: 0 }}</h3>
                <p>Total Employees</p>
              </div>
              <div class="icon">
                <i class="fas fa-user"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
           <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ Request::is($getTotalActiveProjects != '') ? $getTotalActiveProjects: 0 }}</h3>
                <p>Active Projects</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
           <div class="col-lg-3 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ Request::is($getTotalClosedProject != '') ? $getTotalClosedProject: 0 }}</h3>
                <p>Closed Projects</p>
              </div>
              <div class="icon">
                <i class="fas fa-signal"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
           <div class="col-lg-3 col-6">
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ Request::is($getTotalClient != '') ? $getTotalClient: 0 }}</h3>
                <p>Total Clients</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
       </div>
       @else
          <div class="row">
          <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                  <h3>{{$totalWorkingDay}}</h3>
                <p>Total Working Days</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{isset($late_days)?$late_days:'0'}}</h3>
                <p>Late Days</p>
              </div>
              <div class="icon">
                <i class="fas fa-signal"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{Request::is($assignedProject != '') ? $assignedProject : 0}}</h3>
                <p>Assigned Projects</p>
              </div>
              <div class="icon">
                <i class="fas fa-user"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>{{$avg_log_hr}}</h3>
                <p>Monthly Logged Hours</p>
              </div>
              <div class="icon">
                <i class="fas fa-chart-line"></i>
              </div>
              <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
            </div>
          </div>
        {{-- </div>
            <div class="row"> --}}
            <div class="col-lg-3 col-6">
                <div class="small-box bg-dark">
                  <div class="inner">
                    <h3>{{$avg_working_hr}}</h3>
                    <p>Monthly Working Hours</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-user"></i>
                  </div>
                  <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
            </div>
            
            <div class="col-lg-3 col-6">
                    <div class="small-box bg-secondary">
                      <div class="inner">
                        <h3>{{$total_leaves['taken_leaves']}}</h3>
                        <p>Leaves Taken</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                      <div class="inner">
                        <h3>{{$total_leaves['future_leave_taken']}}</h3>
                        <p>Approved Future Leaves</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-signal"></i>
                      </div>
                    </div>
                </div>
             <div class="col-lg-3 col-6">
                <div class="small-box bg-secondary">
                  <div class="inner">
                    <h3>{{Request::is($leaveData['leavePendingToApprove']!='') ? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['leavePendingToApprove'] ) : 0 }}</h3>
                    <p>Leave Pending To Approve</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-signal"></i>
                  </div>
                  <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
              </div>
            <div class="col-lg-3 col-6">
                <div class="small-box bg-warning">
                  <div class="inner">

                    <h3>{{(Request::is($leaveData['remaningLeave'] != '') && $leaveData['remaningLeave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['remaningLeave'] ) : 0}}</h3>
                    <p>Leaves Remaining</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-chart-line"></i>
                  </div>
                  <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
            </div>
            <div class="col-lg-3 col-6">
                   <div class="small-box bg-warning">
                      <div class="inner">
                        <h3>{{(Request::is($leaveData['compoffleave'] != '') && $leaveData['compoffleave'] > 0 )? (new \App\Helpers\CommonHelper)->displayLeaveInDashboard($leaveData['compoffleave'] ) : 0}}</h3>
                        <p> Leave Comp Off </p>
                      </div>
                        <div class="icon">
                         <i class="fas fa-chart-line"></i>
                       </div>
                      <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                  </div>
              </div>
              <div class="col-lg-3 col-6">
                  <div class="small-box bg-danger">
                    <div class="inner">

                      <h3><?php
                        $lwpdata = getUserLwpLeave();
                        echo $lwpdata['lwpLeaveCount'];
                      ?></h3>
                      <p>Leave Without Pay</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-chart-line"></i>
                    </div>
                    <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                  </div>
                </div>
          </div>
       @endif
       @if((Auth::user()->hasRole(config('constant.team_leader_slug'))|| Auth::user()->hasRole(config('constant.project_manager_slug'))))
        <div class="row">
          <div class="col-md-12">
            <div class="card">
                <div class="card-header border-bottom-0 {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
					<h3 class="card-title">Non-billable Task</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapsed">
							<i class="fa fa-chevron-down"></i>
						</button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/dashboard')}}" name="search_filter" id="search_filter">
						<div class="card-body border-top">
							<div class="row">
								<div class="form-group col-md-4 col-sm-6">
								<label class="">User Name</label>
								<select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_user_name"
										id="search_by_user_name">
									<option value="">Select</option>
									@if(!empty($getTeamMember))
										@foreach($getTeamMember as $teamMembers)
											<option value="{{$teamMembers['id']}}" {{(isset($request->search_by_user_name) && ($request->search_by_user_name == $teamMembers['id']))?'selected':''}}>{{$teamMembers['first_name'].' '.$teamMembers['last_name']}}</option>
										@endforeach
									@endif
								</select>
								</div>
								<div class="form-group col-md-4 col-sm-6">
									<label class="">Task Start date</label>
									<div class="input-group ">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" name="search_by_start_date" id="search_by_start_date"
											value="{{(isset($request->search_by_start_date)?$request->search_by_start_date:date('Y-m-01'))}}"
											class="form-control" readonly="readonly">
									</div>
								</div>
								<div class="form-group col-md-4 col-sm-6">
									<label class="">Task End date</label>
									<div class="input-group ">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
										<input type="text" name="search_by_end_date" id="search_by_end_date"
												value="{{(isset($request->search_by_end_date)?$request->search_by_end_date:date('Y-m-t'))}}"
												class="form-control" readonly="readonly">
									</div>
								</div>
							</div>
						</div>
						<input name="search_by_project_hours_type" type="hidden" value="non_billable_hours">
						<div class="card-footer border-bottom">
							<button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
							Search
							</button>
							<button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/dashboard')}}'">
							Reset
							</button>
							<button name="search_redirect" id="search_redirect" type="reset" class="btn btn-info btn-primary">
							View in Details
							</button>
						</div>
                  </div>

			  @if (count($team_task_entry)>0)
			  	<div class="table table-responsive">
					<table id="team_task_listing" class="table">
						<thead>
								<tr>
									<th scope="col" width="10%">User Name</th>
									<th scope="col" width="15%">Project Name</th>
									<th scope="col" width="20%">Task Name</th>
									<th scope="col" width="10%">Start Date</th>
									<th scope="col" width="10%">End Date</th>
									<th scope="col" width="10%">Task Hours</th>
									<th scope="col" width="10%">Logged Hours</th>
									<th scope="col" width="12%" class="text-center">View Entry</th>
								</tr>
						</thead>
						<tbody>
							@foreach ($team_task_entry as $task)
								<tr>
									@foreach($getTeamMember as $name)
									@if($name['id'] == $task['user_id'])
									<td scope="col" width="10%">{{$name['first_name'].' '.$name['last_name']}}</td>
									@endif
									@endforeach
									<td scope="col" width="15%">{{$task->project_name}}</td>
									<td scope="col" width="20%">{{$task->task_name}}</td>
									<td scope="col" width="10%">{{date('d-m-Y',strtotime($task->task_start_date))}}</td>
									<td scope="col" width="10%">{{date('d-m-Y',strtotime($task->task_end_date))}}</td>
									<td scope="col" width="10%">{{$task->estimated_hours != '' ?(new \App\Helpers\CommonHelper)->displayTaskTime($task->estimated_hours):'00:00'}}</td>
									<td scope="col" width="10%">{{$task->total_logged_hours != '' ?(new \App\Helpers\CommonHelper)->displayTaskTime($task->total_logged_hours):'00:00'}}</td>
									<td scope="col" width="12%" class="text-center"><i class="mdi mdi-information-outline task-info text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" task_id="{{$task->id}}"></i></td>
								</tr>
							@endforeach
						</tbody>
					</table>
			  	</div>
                @else
                  <div class="border-top">
                      <h4 align="center" style="padding : 20px;">No Task Available</h4>
                  </div>
                @endif
          </div>
        </div>
        </div>
       @endif
        <div class="row">
            <div class="col-md-6">
              <link rel="stylesheet" href="{{asset('css/text-blink.css')}}" />
              @if((Auth::user()->hasRole(config('constant.team_leader_slug'))|| Auth::user()->hasRole(config('constant.project_manager_slug'))) || Auth::user()->hasRole('ba'))
                  <div class="card">
                    <div class="card-body">
                      <h5 class="card-title m-b-0">
                        Project Lead End Date Reminder
                        @if(isset($leadEndDateReminder['leadAlert']) && $leadEndDateReminder['leadAlert']>0)
                          <span class="ml-2 mdi mdi-information-outline mdi-24px information"  data-trigger="hover" style="color:#ff0000;" data-toggle="tooltip" data-placement="top" data-content="<h6 style='color:#ff0000;'>Alert: Project Lead Status Still Not Completed!!</h6>"></span>
                        @endif
                      </h5>
                    </div>
                    @if (count($leadEndDateReminder['allLead'])>0)
                    <table id="lead_end_date_listing" class="table">
                        <thead>
                            <tr>
                              <th scope="col">Lead Title</th>
                              <th scope="col" @if($leadEndDateReminder['leadAlert']>0) style="color:#ff0000;" @endif>End Date</th>
                              <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($leadEndDateReminder['allLead'] as $lead)
                            <tr>
                                <td class="text-capitalize">{{$lead['lead_title']}}</td>
                                <td><span @if(date('Y-m-d') >= date('Y-m-d',strtotime($lead['reviewer_end_date']))) style="background:#000;" class="p-1 rounded" @endif> <custom @if(date('Y-m-d') >= date('Y-m-d',strtotime($lead['reviewer_end_date']))) class="blink font-weight-bold" style="color:#ff0000;" @endif>{{ date('d-m-Y',strtotime($lead['reviewer_end_date'])) }} </custom></span></td>
                                <td align="center"><a href="{{ url('/lead/details/'.$lead['id']) }}"><i class='mdi mdi-information mdi-18px' data-trigger="hover" data-toggle="tooltip" data-placement="top" data-content="View In Details"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No records found.</h4>
                    </div>
                    @endif
                  </div>
              @endif
              <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Leave Without Pay</h5>
                    </div>
                    <?php $lwpData = getUserLwpLeave(); ?>
                    @if (count($lwpData['lwpLeaveDetails'])>0)
                    <table id="today_leave_without_pay_listing" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Leave Starts</th>
                                <th scope="col">Leave Ends</th>
                                <?php
                                if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug'))){
                                 ?>
                                  <th scope="col">User Name</th>
                               <?php }?>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php if(isset($lwpData['lwpLeaveDetails']) && !empty($lwpData['lwpLeaveDetails'])) { ?>
                            @foreach($lwpData['lwpLeaveDetails'] as $lwpleaveDetail)
                            <tr>
                                <td data-sort='{{$lwpleaveDetail['leave_start_date']}}'>{{(new \App\Helpers\CommonHelper)->displayDate($lwpleaveDetail['leave_start_date'])}}</td>
                                <td data-sort='{{$lwpleaveDetail['leave_end_date']}}'>{{(new \App\Helpers\CommonHelper)->displayDate($lwpleaveDetail['leave_end_date'])}}</td>

                                @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug')))
                                  <td>
                                 @if(isset($lwpleaveDetail['user']['first_name']))
                                 {{$lwpleaveDetail['user']['first_name']." ".$lwpleaveDetail['user']['last_name']}}
                                 @endif
                                  </td>
                               @endif
                                <td>{{$leaveStatusArray[$lwpleaveDetail['leave_status']]}}</td>

                            </tr>
                            @endforeach
                          <?php } ?>
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                    </div>
                    @endif
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Employee On Leave Today</h5>
                    </div>
                    @if (count($todayLeaveListing)>0)
                    <table id="today_leave_listing" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Days</th>
                                <th scope="col">Leave Starts</th>
                                <th scope="col">Leave Ends</th>
                                <th scope="col">Leave Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($todayLeaveListing as $leaveDetail)
                            <tr>
                                <td>{{$leaveDetail['user']['first_name'].' '.$leaveDetail['user']['last_name']}}</td>
                                <td>{{$leaveDetail['leave_days']}}</td>
                                <td data-sort='{{$leaveDetail['leave_start_date']}}'>{{(new \App\Helpers\CommonHelper)->displayDate($leaveDetail['leave_start_date'])}}</td>
                                <td data-sort='{{$leaveDetail['leave_end_date']}}'>{{(new \App\Helpers\CommonHelper)->displayDate($leaveDetail['leave_end_date'])}}</td>
                                <td> @if($leaveDetail['leave_start_type'] != '' && $leaveDetail['leave_start_type'] != 0 && $leaveDetail['leave_days'] == "0.50") {{$leaveDetail['leave_start_type'] == 1?"1st Half" : "2nd Half"}} @else {{ "Full Day"}} @endif</td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No leaves for today.</h4>
                    </div>
                    @endif
                </div>
                  <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Holidays for the Year – {{ date('Y') }}</h5>
                    </div>
                    @if (count($holidaysData)>0)
                    <table id="holidays" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Holiday</th>
                                <th scope="col">Date</th>
                                <th scope="col">Day</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($holidaysData as $holidaysDetail)
                            <tr>
                                <td>{{ $holidaysDetail['holiday_name'] }}</td>
                                <td>{{ (new \App\Helpers\CommonHelper)->displayDate($holidaysDetail['holiday_date']) }}</td>
                                <td>{{ (new \App\Helpers\CommonHelper)->displayDay($holidaysDetail['holiday_date']) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No holidays available.</h4>
                    </div>
                    @endif
                </div>
                @if((Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug'))  || Auth::user()->hasRole(config('constant.superadmin_slug')) ))
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title m-b-0">Employee On Training</h5>
                  </div>
                  @if (isset($traineeList) && count($traineeList )>0)
                    <div class="table table-responsive">
                      <table id="emp_on_training_on_listing" class="table">
                        <thead>
                          <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Joining Date</th>
                            <th scope="col">Designation</th>
                            <th scope="col">Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($traineeList as $tList)
                            <tr>
                              <td scope="col">{{$tList['first_name'].' '.$tList['last_name']}}</td>
                              <td scope="col">{{date('d-M-Y',strtotime($tList['joining_date']))}}</td>
                              <td scope="col">{{isset($tList['designation']['designation_name']) && $tList['designation']['designation_name'] != "" ? $tList['designation']['designation_name'] : ""}}</td>
                              <td scope="col">{{$tList['employee_status'] == 1?"Confirmed" : "Probation"}}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No Record Available</h4>
                    </div>
                  @endif
                </div>
                <div class="card">
                    <div class="card-body">
                    <h5 class="card-title m-b-0">Employee Has Completed Probation</h5>
                    </div>
                    @php
                        $getEmpListHasCompletedProbation = getEmpListHasCompletedProbation(auth()->user()->company_id);
                    @endphp
                    @if (isset($getEmpListHasCompletedProbation) && count($getEmpListHasCompletedProbation )>0)
                    <div class="table table-responsive">
                        <table id="completed_probation_listing" class="table">
                        <thead>
                            <tr>
                            <th scope="col" width="25%">Name</th>
                            <th scope="col" width="22%">Joining Date</th>
                            <th scope="col" width="43%">Designation</th>
                            <th scope="col" width="20%">Status</th>
                            <th scope="col" width="20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($getEmpListHasCompletedProbation as $prob_user_list)
                                <tr>
                                    <td scope="col" width="25%">{{$prob_user_list['first_name'].' '.$prob_user_list['last_name']}}</td>
                                    <td scope="col" width="22%">{{date('d-M-Y',strtotime($prob_user_list['joining_date']))}}</td>
                                    <td scope="col" width="43%">{{isset($prob_user_list['designation']['designation_name']) && $prob_user_list['designation']['designation_name'] != "" ? $prob_user_list['designation']['designation_name'] : ""}}</td>
                                    <td scope="col" width="20%">{{$prob_user_list['employee_status'] == 1?"Confirmed" : "Probation"}}</td>
                                    <td align="center"><a href="{{ url('/users/edit/'.$prob_user_list['id']) }}"><i class='mdi mdi-pencil mdi-18px' data-trigger="hover" data-toggle="tooltip" data-placement="top" data-content="Update status"></i></a></td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No Record Available</h4>
                    </div>
                    @endif
                </div>
              @endif
            </div>
            <div class="col-md-6">
              <link rel="stylesheet" href="{{asset('css/text-blink.css')}}" />
              @if((Auth::user()->hasRole(config('constant.team_leader_slug'))|| Auth::user()->hasRole(config('constant.project_manager_slug')) || Auth::user()->hasRole('ba')))
                  <div class="card">
                    <div class="card-body">
                      <h5 class="card-title m-b-0">
                        Project Lead Pending Date Reminder
                        @if(isset($leadPendingDateReminder['leadAlert']) && $leadPendingDateReminder['leadAlert']>0)
                          <span class="ml-2 mdi mdi-information-outline mdi-24px information"  data-trigger="hover" style="color:#ff0000;" data-toggle="tooltip" data-placement="top" data-content="<h6 style='color:#ff0000;'>Alert: Reviewer Date Still Not Updated!!</h6>"></span>
                        @endif
                      </h5>
                    </div>
                    @if (count($leadPendingDateReminder['allLead'])>0)
                    <table id="lead_pending_date_listing" class="table">
                        <thead>
                            <tr>
                              <th scope="col">Lead Title</th>
                              <th scope="col" @if($leadPendingDateReminder['leadAlert']>0) style="color:#ff0000;" @endif>Created Date</th>
                              <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($leadPendingDateReminder['allLead'] as $lead)
                            <tr>
                                <td class="text-capitalize">{{$lead['lead_title']}}</td>
                                <td>
                                  <span @if(date('Y-m-d',strtotime("-2 days")) > date('Y-m-d',strtotime($lead['created_at']))) style="background:#000;" class="p-1 rounded" @endif> <custom @if(date('Y-m-d',strtotime("-2 days")) > date('Y-m-d',strtotime($lead['created_at']))) class="blink font-weight-bold" style="color:#ff0000;" @endif>{{ date('d-m-Y',strtotime($lead['created_at'])) }} </custom><span>
                                </td>
                                <td align="center"><a href="{{ url('/lead?search_by_lead_name='.$lead['lead_title'].'&search_submit=1') }}"><i class='mdi mdi-information mdi-18px' data-trigger="hover" data-toggle="tooltip" data-placement="top" data-content="View In Details"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No records found.</h4>
                    </div>
                    @endif
                  </div>
              @endif
              @if((Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug'))  || Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole('administration') ||  Auth::user()->hasRole('account_manager') ))

                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title m-b-0">Upcoming Employee Birthday</h5>
                  </div>
                  @if (isset($birthdayList) && count($birthdayList)>0)
                    <div class="table table-responsive">
                      <table id="emp_birthday_listing" class="table">
                        <thead>
                          <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Birth Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($birthdayList as $bList)
                            <tr>
                              <td scope="col">{{$bList['first_name'].' '.$bList['last_name']}}</td>
                              <td scope="col">{{date('d-M',strtotime($bList['birthdate']))}}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  @else
                    <div class="border-top">
                      <h4 align="center" style="padding : 20px;">No Record Available</h4>
                    </div>
                  @endif
                </div>
              @endif
              @if((Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug'))  || Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole('administration') ||  Auth::user()->hasRole('account_manager') ))

                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title m-b-0">Upcoming Work Anniversary</h5>
                  </div>
                  @if (isset($userListingWithJoiningDateOnly) && count($userListingWithJoiningDateOnly)>0)
                    <div class="table table-responsive">
                      <table id="emp_joining_listing" class="table">
                        <thead>
                          <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Joining Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($userListingWithJoiningDateOnly as $bList)
                            <tr>
                              <td scope="col">{{$bList['first_name'].' '.$bList['last_name']}}</td>
                              <td scope="col">{{date('d-M-Y',strtotime($bList['joining_date']))}}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  @else
                    <div class="border-top">
                      <h4 align="center" style="padding : 20px;">No Record Available</h4>
                    </div>
                  @endif
                </div>
              @endif
                  <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Upcoming Employee Leaves</h5>
                    </div>
                    @if (count($upcomingLeaveListing)>0)
                    <table id="upcoming_leave_listing" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Days</th>
                                <th scope="col">Leave Starts</th>
                                <th scope="col">Leave Ends</th>
                                <th scope="col">Leave Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($upcomingLeaveListing as $leaveDetail)
                            <tr>
                                <td>{{$leaveDetail['user']['first_name'].' '.$leaveDetail['user']['last_name']}}</td>
                                <td>{{$leaveDetail['leave_days']}}</td>
                                <td data-sort='{{$leaveDetail['leave_start_date']}}'>{{(new \App\Helpers\CommonHelper)->displayDate($leaveDetail['leave_start_date'])}}</td>
                                <td data-sort='{{$leaveDetail['leave_end_date']}}'>{{(new \App\Helpers\CommonHelper)->displayDate($leaveDetail['leave_end_date'])}}</td>
                                <td>@if($leaveDetail['leave_start_type'] != '' && $leaveDetail['leave_start_type'] != 0 && $leaveDetail['leave_days'] == "0.50") {{$leaveDetail['leave_start_type'] == 1?"1st Half" : "2nd Half"}} @else {{ "Full Day"}} @endif</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No upcoming leaves.</h4>
                    </div>
                    @endif
                </div>
                    @if(Auth::user()->hasRole(config('constant.team_leader_slug'))||
                        Auth::user()->designation_id == 16 )
                    <div class="card dashboard_chart">
                        <div class="card-body">
                            <h5 class="card-title col-md-6">Team Member Report</h5>
                        </div>
                            <div class="border-top">
                                <select class="select2 form-control custom-select" name="team_member" id="team_member" >
                                    @foreach($getTeamMember as $getTeamMemberVal)
                                    <option value="{{$getTeamMemberVal['id']}}" >{{$getTeamMemberVal['first_name'].' '.$getTeamMemberVal['last_name']}}</option>
                                    @endforeach
                                </select>
                                <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                            </div>
                    </div>
                @endif
                @if(Auth::user()->hasRole(config('constant.superadmin_slug'))||
                    Auth::user()->hasRole(config('constant.admin_slug')))
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Open Projects</h5>
                    </div>
                    @if (count($openProjectList)>0)
                    <table id="open_project_list" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Project Name</th>
                                <th scope="col">Project Type</th>
                                <th></th>
                            </tr>
                        </thead>
                        @php
                            $j=1;
                        @endphp
                            @foreach($openProjectList as $openProjectVal)
                            <tr>
                                <td style="width:300px;">{{$openProjectVal['project_name']}}</td>
                                <td>{{$openProjectVal['project_type_name']}}</td>
                                <td></td>
                            </tr>
                            @php
                            $j++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No Projects.</h4>
                    </div>
                    @endif
                </div>
                @endif
            @if((Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug'))  || Auth::user()->hasRole(config('constant.superadmin_slug'))))
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title m-b-0">Employee On Probation</h5>
                </div>
                @if (isset($onProbationList) && count($onProbationList )>0)
                  <div class="table table-responsive">
                    <table id="on_probation_listing" class="table">
                      <thead>
                        <tr>
                          <th scope="col" width="25%">Name</th>
                          <th scope="col" width="22%">Joining Date</th>
                          <th scope="col" width="43%">Designation</th>
                          <th scope="col" width="20%">Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($onProbationList as $pList)
                          <tr>
                            <td scope="col" width="25%">{{$pList['first_name'].' '.$pList['last_name']}}</td>
                            <td scope="col" width="22%">{{date('d-M-Y',strtotime($pList['joining_date']))}}</td>
                            <td scope="col" width="43%">{{isset($pList['designation']['designation_name']) && $pList['designation']['designation_name'] != "" ? $pList['designation']['designation_name'] : ""}}</td>
                            <td scope="col" width="20%">{{$pList['employee_status'] == 1?"Confirmed" : "Probation"}}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                @else
                  <div class="border-top">
                      <h4 align="center" style="padding : 20px;">No Record Available</h4>
                  </div>
                @endif
              </div>
            @endif
            @if(Auth::user()->hasRole('Networking') || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug'))||
            Auth::user()->hasRole(config('constant.admin_slug')))
             <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Resign Users</h5>
                    </div>
                    @if (isset($resignUser) && count($resignUser)>0)
                    <table id="resign_user_listing" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">End Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($resignUser as $value)
                            <tr>
                                <td>{{$value['first_name'].' '.$value['last_name']}}</td>
                                <td data-sort='{{$value['last_date']}}'>{{(new \App\Helpers\CommonHelper)->displayDate($value['last_date'])}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No Resign Users.</h4>
                    </div>
                    @endif
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">Upcoming Joining Users</h5>
                    </div>
                    @if (isset($joiningUser) && count($joiningUser)>0)
                    <table id="upcoming_joining_user_listing" class="table">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Department</th>
                                <th scope="col">Designation</th>
                                <th scope="col">Reporting To</th>
                                <th scope="col">Employee Code</th>
                                <th scope="col">Joining Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($joiningUser as $value)
                            <tr>
                                <td>{{$value['first_name'].' '.$value['last_name']}}</td>
                                <td>{{(config('constant.department')[$value->department])?config('constant.department')[$value->department]:''}}</td>
                                <td>{{(isset($value->designation->designation_name))?$value->designation->designation_name:''}}</td>
                                <td>{{(isset($value->reportingto->first_name))?$value->reportingto->first_name.' '.$value->reportingto->last_name:''}}</td>
                                <td>{{
                                          $value->getCompanyShortName($value->company_id).sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $value->employee_id)))
                                      }}  </td>
                                <td data-sort='{{$value['joining_date']}}'>{{(new \App\Helpers\CommonHelper)->displayDate($value['joining_date'])}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="border-top">
                        <h4 align="center" style="padding : 20px;">No Upcoming Joining Users.</h4>
                    </div>
                    @endif
                </div>
                @endif
          </div>
        </div>
       <input type="hidden" class="login_role_name" id="login_role_name" value="{{(Auth::user()->designation_id) ? Auth::user()->designation_id: 0 }}">
       <?php
       $user_slug = '';
       if(Auth::user()->hasRole(config('constant.team_leader_slug'))||
               Auth::user()->designation_id == 16){
         $user_slug = 'team_leader';
       }
       ?>
       <input type="hidden" class="login_user_role_type" id="login_user_role_type" value="<?php echo $user_slug; ?>">
    </div>
      </section>
      <div id="task_information" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Task Info</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"><a href="" class='task_info_details'>View</a></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<!-- jQuery -->

<script>
 $.widget.bridge('uibutton', $.ui.button);
 $('.task-info').on('click', function () {
    var task_id = $(this).attr('task_id');
    $.ajax({
        type: "get",
        url: getsiteurl() + '/view/get_task_entry_details/' + task_id,
        data: "",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $('#modal-body').html(response);
            $('.task_info_details').attr("href", getsiteurl() +'/view/project_task_entry/'+task_id);
            $('#task_information').modal();
        }
    });
    return false;
});
</script>
<script src="{{asset('/dist/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('/dist/plugins/knob/jquery.knob.js')}}"></script>
<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js')}}"></script>
<script src="{{asset('/dist/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('/dist/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{asset('/dist/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('/dist/plugins/fastclick/fastclick.js')}}"></script>
<!--<script src="{{asset('/dist/js/adminlte.js')}}"></script>-->
<script src="{{asset('/js/dashboard.js')}}"></script>
<script src="{{asset('/dist/js/demo.js')}}"></script>
<script src="{{asset('js/raphael-min.js')}}"></script>
<script src="{{asset('js/morris.min.js')}}"></script>
@endsection
