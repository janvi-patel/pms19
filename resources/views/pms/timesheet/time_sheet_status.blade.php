@extends('layouts.master')

@section('moduleName')
    TimeSheetStatus
@endsection

@php
    $pageRangArray = config('constant.page_range');
@endphp

@section('content')
@include('sweet::alert')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">        
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Time Sheet Status</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Time Sheet Status</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="post" action="{{url('/upload/old/entries')}}" name="search_submit" id="search_submit">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                    <div class="form-group col-md-4 col-sm-6">
                                        <label class="">Status</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_status" id="search_by_status">
                                            <option value="">Select Status</option>
                                                <option value="success" {{(isset($request->search_by_status) && ($request->search_by_status == 'success'))?'selected':''}}>Success</option>
                                                <option value="skipped" {{(isset($request->search_by_status) && ($request->search_by_status == 'skipped'))?'selected':'' }}>Skipped</option>
                                                <option value="failed"  {{(isset($request->search_by_status) && ($request->search_by_status == 'failed'))?'selected':'' }}>Failed</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4 col-sm-6">
                                        <label class="">Entry Date</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="search_by_entry_date" id="search_by_entry_date" value="{{(isset($request->search_by_entry_date)?$request->search_by_entry_date:'')}}"  class="form-control" readonly />
                                        </div>
                                        <a href="javascript:void(0)" id="reset_entry_date">Reset</a>
                                    </div>
                            </div>
                        </div>
                        <input type="hidden" id="submit_type" value="search" name="submit_type">
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/upload/old/entries')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{!!session('success')!!}</strong>
                            </div>
                            {{Session::forget('success')}}
                            {{Session::put('success','')}}
                        @endif    
                        @if (Session::has('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{!!session('error')!!}</strong>
                            </div>
                            {{Session::forget('error')}}
                        @endif
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex align-items-center justify-content-between">
                                <div class="col-6">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                        @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                                <div class="show_total_no float-right">
                                    Showing {{ $records->firstItem() }} to {{ $records->lastItem() }} of total {{$records->total()}} entries
                                </div> 
                                <div class="right col-2">
                                    <a href="#" title="Download Excel" id="export_to_excel">
                                        <button type="button" class="btn btn-dark">Export Failed Record</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="leave_listing" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>Entry Date</th>
                                        <th>Punch Time</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($records)>0)
                                        @foreach($records as $statusData)
                                        <tr>
                                            <td>{{$statusData['emp_code']}}</td>
                                            <td>{{$statusData['emp_name']}}</td>
                                            <td>{{$statusData['entry_date']}}</td>
                                            <td>{{str_replace(";","; ",$statusData['punch_time'])}}</td>
                                            <td>
                                                @if($statusData['status'] == "success")
                                                    <span class="badge badge-success">{{ $statusData['status'] }}</span>
                                                @elseif($statusData['status'] == "skipped")
                                                    <span class="badge badge-warning">{{ $statusData['status'] }}</span>
                                                @else
                                                    <span class="badge badge-danger">{{ $statusData['status'] }}</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach 
                                    @else
                                        <tr>
                                            <td colspan='10'>
                                                <center>
                                                    {{'No Record Found'}}
                                                </center>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row w-100">
                            @if($records && !empty($records))
                                <div class="pt-4 col-md-6">
                                    {!! $records->appends(\Request::except('page'))->render() !!}
                                </div>
                            @endif
                                @if(count($records) >0)
                                <div class="show_total_no_bottom col-md-6"> 
                                    <div class="card-body float-right">
                                        Showing {{ $records->firstItem() }} to {{ $records->lastItem() }} of total {{$records->total()}} entries
                                    </div>
                                </div> 
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('javascript')
    <script>
        $("#search_by_entry_date").datepicker({
            todayBtn: 1,
            autoclose: true,
            format: 'dd-mm-yyyy',
        });
        $("#reset_entry_date").click(function () {
            $('#search_by_entry_date').val('');
        });

    $(document).on("click", ".deleteTimeStatus", function(){
		var statusId = $(this).attr('data-task-id');
		if(statusId != ''){
        swal({
            title: "Are you sure you want to delete Time Status ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteTimeStatus(statusId);
                }
            });
		}
	});

    function deleteTimeStatus(statusId){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		jQuery.ajax({
	      	url  : getsiteurl() + '/delete/old/entries',
	    	type : "DELETE",
	      	data: {_token: CSRF_TOKEN,statusId:statusId},
	      	success: function(response) {
                    if(response.status == 'success'){
                        swal(response.message,"", "success");
                        location.reload();
                    }else if(response.status == 'error'){
                        swal(response.message,"", "error");
                    }
	       	}
	    });
    }
    
    $('#export_to_excel').on('click', function () {
        $('#submit_type').val('export_excel');
        $('#search_submit').submit();
        $('#submit_type').val('');

    })

    $('#page_range_dropdown').change(function(){
        var limit = $(this).val();
        $('#page_range').val(limit);
        $('#search_submit').submit();
	});
    </script>
@endsection