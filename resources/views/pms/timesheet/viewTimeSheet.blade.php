@extends('layouts.master')

@section('moduleName')
    TimeSheet
@endsection

@section('content')
    @php
        $leaveStatusArray = config('constant.leave_status');
        $departmentArray = config('constant.department');
        $departmentExclude = [5, 8, 9];
    @endphp
    @include('sweet::alert')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">View Time Sheet</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div id="accordion">
                <div class="card">
                    <div class="card-header bg-dark collapsed" data-toggle="collapse" data-parent="#accordion"
                        href="#collapseOne" aria-expanded="false">
                        <h3 class="card-title">Filter Time Sheet</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i
                                    class="fa fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div id="collapseOne" class="panel-collapse in collapse">
                        <form class="form-horizontal" method="post" action="{{ url('/pms/viewtimesheet') }}"
                            name="timeSheetFilterForm" id="timeSheetFilterForm">
                            @csrf
                            <div class="card-body">
                                <div class="row">

                                    @if (count($userListing) > 0)
                                        <div class="form-group col-lg col-md-4 col-sm-4">
                                            <label class="">Employee</label>
                                            <select class="form-control select2" style="width: 100%; height:36px;"
                                                name="search_employee" id="search_employee">
                                                <option value="">Select Employee</option>
                                                @foreach ($userListing as $userDetail)
                                                    <option value="{{ $userDetail['employee_id'] }}"
                                                        {{ isset($request->search_employee) && $request->search_employee == $userDetail['employee_id'] ? 'selected' : '' }}>
                                                        {{ $userDetail['first_name'] . ' ' . $userDetail['last_name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif

                                    <div class="form-group col-lg col-md-4 col-sm-4">
                                        <label class="">Month</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;"
                                            name="search_month" id="search_month">
                                            <option value="">Select Month</option>
                                            @foreach (config('constant.months') as $key => $value)
                                                <option value="{{ $key }}"
                                                    {{ (isset($request->search_month) && $request->search_month == $key) || (isset($currentMonth) && $currentMonth == $key) ? 'selected' : '' }}>
                                                    {{ $value }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-lg col-md-4 col-sm-4">
                                        <label class="">Year</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;"
                                            name="search_year" id="search_year">
                                            <option value="">Select Year</option>
                                            @foreach ($years as $key => $value)
                                                <option value="{{ $key }}"
                                                    {{ (isset($request->search_year) && $request->search_year == $key) || (isset($currentYear) && $currentYear == $key) ? 'selected' : '' }}>
                                                    {{ $value }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    @if (
                                        (Auth::check() &&
                                            (Auth::user()->hasRole('admin') ||
                                                Auth::user()->hasRole('hr') ||
                                                Auth::user()->hasRole(config('constant.superadmin_slug')))) ||
                                            Auth::user()->hasRole('Networking') ||
                                            Auth::user()->hasRole('administration'))
                                        <div class="form-group col-lg-5 col-md-6 col-sm-12">
                                            <label>Department</label>
                                            <select class="form-control select2" style="width: 100%; height:36px;"
                                                name="search_department[]" id="search_department" multiple="multiple">
                                                <option value="">Select Department</option>
                                                @if (isset($departmentArray) && !empty($departmentArray))
                                                    @foreach ($departmentArray as $key => $val)
                                                        @if (Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin')))
                                                            @if (!in_array($key, $departmentExclude) && !isset($request->search_department))
                                                                <option selected="selected" value="{{ $key }}">
                                                                    {{ $val }}</option>
                                                            @else
                                                                <option value="{{ $key }}"
                                                                    {{ isset($request->search_department) && in_array($key, $request->search_department) ? 'selected' : '' }}>
                                                                    {{ $val }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $key }}"
                                                                {{ isset($request->search_department) && $request->search_department == $key ? 'selected' : '' }}>
                                                                {{ $val }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        @php asort($reportingUserArray); @endphp
                                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                            <label class="">Reporting To</label>
                                            <select class="form-control select2" style="width: 100%; height:36px;"
                                                name="search_reporting_to" id="search_reporting_to">
                                                <option value="">Select</option>
                                                @foreach ($reportingUserArray as $key => $value)
                                                    <option value="{{ $key }}"
                                                        {{ isset($request->search_reporting_to) && $request->search_reporting_to == $key ? 'selected' : '' }}>
                                                        {{ $value }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- <div class="form-group col-lg col-md-4 col-sm-4"></div> -->
                                    @endif
                                    @if (
                                        (Auth::check() &&
                                            (Auth::user()->hasRole('admin') ||
                                                Auth::user()->hasRole('hr') ||
                                                Auth::user()->hasRole(config('constant.superadmin_slug')))) ||
                                            Auth::user()->hasRole('Networking') ||
                                            Auth::user()->hasRole('administration') ||
                                            Auth::user()->hasRole('pm') ||
                                            Auth::user()->hasRole('tl'))
                                        <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                            <label>Designation</label>
                                            <select class="form-control select2" style="width: 100%; height:36px;"
                                                name="search_by_designation" id="search_by_designation">
                                                <option value="">Select Designation</option>
                                                @if (isset($designationList) && !empty($designationList))
                                                    @foreach ($designationList as $designationDataVal)
                                                        <option value="{{ $designationDataVal->id }}"
                                                            {{ isset($request->search_by_designation) && $request->search_by_designation == $designationDataVal->id ? 'selected' : '' }}>
                                                            {{ $designationDataVal->designation_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="form-group offset-4 col-lg col-md-6 col-sm-6">
                                        <label id="search_month-error" class="error" for="search_month"></label>
                                    </div>
                                    <div class="form-group col-lg col-md-6 col-sm-6">
                                        <label id="search_year-error" class="error" for="search_year"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                    Search
                                </button>
                                <button name="search_reset" type="reset" class="btn btn-info btn-secondary"
                                    onclick="location.href ='{{ url('/pms/viewtimesheet') }}'">
                                    Reset
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{!! session('success') !!}</strong>
                                </div>
                                {{ Session::forget('success') }}
                                {{ Session::put('success', '') }}
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{!! session('error') !!}</strong>
                                </div>
                                {{ Session::forget('error') }}
                            @endif

                            <div class="table-responsive">
                                <table id="leave_listing" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>@sortablelink('emp_name', 'Employee')</th>
                                            <th>@sortablelink('emp_code', 'Employee Code')</th>
                                            @if (!Auth::user()->hasRole(config('constant.developer_slug')))
                                                <th>Reporting To</th>
                                                <th>Department</th>
                                            @endif
                                            <th>Total Days</th>
                                            <th>Working Days</th>
                                            <th>Absent Days</th>
                                            <th>Approved Leaves</th>
                                            <th>Pending Leaves</th>
                                            <th>LWP Leaves</th>
                                            <th>Incorrect Entry Days</th>
                                            <th>Early Going Days</th>
                                            <th>Adjustments</th>
                                            <th>Average</th>
                                            <th>Logged Hours</th>
                                            <th>Summary</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($records) > 0)
                                            <?php
                                            $viewSummeryCurrentMonth = isset($request->search_month) ? $request->get('search_month') : $currentMonth;

                                            $viewSummeryCurrentMonth = sprintf('%02d', $viewSummeryCurrentMonth);

                                            $viewSummeryCurrentYear = isset($request->search_year) ? $request->get('search_year') : $currentYear;
                                            ?>
                                            @foreach ($records as $userSummaryDetail)
                                                @if ((new \App\Helpers\CommonHelper())->checkUserStatus($userSummaryDetail['emp_code']) == 1)
                                                    <?php
                                                    $reportingName = (new \App\Helpers\CommonHelper())->getUserById($userSummaryDetail['reporting_to']);

                                                    ?>
                                                    @if (
                                                        $userSummaryDetail->absent_days != '' ||
                                                            $userSummaryDetail->half_days != '' ||
                                                            $userSummaryDetail->incorrect_days != '')
                                                        <?php
                                                        $absentDays = ($userSummaryDetail->absent_days == '' ? 0 : $userSummaryDetail->absent_days) + ($userSummaryDetail->half_days == '' ? 0 : $userSummaryDetail->half_days / 2) + ($userSummaryDetail->incorrect_days == '' ? 0 : $userSummaryDetail->incorrect_days);
                                                        ?>
                                                    @else
                                                        <?php $absentDays = 0; ?>
                                                    @endif
                                                    <tr>
                                                        <td>{{ $userSummaryDetail['emp_name'] }}</td>
                                                        <td>{{ (new \App\Helpers\CommonHelper())->takeEmpShortName(Auth::user()->company_id) }}{{ $userSummaryDetail['emp_code'] }}
                                                        </td>

                                                        @if (!Auth::user()->hasRole(config('constant.developer_slug')))
                                                            <td>
                                                                @isset($reportingName['first_name'])
                                                                    {{ $reportingName['first_name'] . ' ' . $reportingName['last_name'] }}
                                                                @endisset
                                                            </td>


                                                            <td>{{ $departmentArray[$userSummaryDetail['department']] }}
                                                            </td>
                                                        @endif
                                                        <td>{{ $total_days }}</td>
                                                        <td>
                                                            {{ $totalWorkingDay = $userSummaryDetail['full_days'] + $userSummaryDetail['half_days'] / 2 }}
                                                        </td>
                                                        <td>{{ $absentDays }}</td>
                                                        <td>{{ $userSummaryDetail['leaveDetails']['approvedLeave'] }}</td>
                                                        <td>{{ $userSummaryDetail['leaveDetails']['pendingLeave'] }}</td>
                                                        <td>{{ $userSummaryDetail['leaveDetails']['lwp'] }}</td>
                                                        <td>{{ $userSummaryDetail['incorrect_days'] }}</td>
                                                        <td>{{ $userSummaryDetail['early_going_days'] }}</td>
                                                        <td>{{ $userSummaryDetail['adjustment'] }}</td>
                                                        <td>
                                                            <?php
                                                            $working_seconds = 0;
                                                            $avg_hr_sec = 0;
                                                            $avg_hr = '00:00:00';
                                                            foreach ($working_details as $working_details_val) {
                                                                if ($working_details_val['emp_code'] == $userSummaryDetail['emp_code']) {
                                                                    $parsed = date_parse($working_details_val['working_hours']);
                                                                    $working_seconds = $working_seconds + $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                                                                }
                                                            }
                                                            if ($totalWorkingDay != 0) {
                                                                $avg_hr_sec = $working_seconds / $totalWorkingDay;
                                                                $avg_hr = gmdate('H:i:s', $avg_hr_sec);
                                                            } ?>
                                                            @if ($avg_hr_sec < $avg_working_time_in_sec)
                                                                <span
                                                                    class="badge badge-danger">{{ $avg_hr }}</span>
                                                            @else
                                                                <span class="badge badge-light">{{ $avg_hr }}</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $avg_log_hr = '00:00:00';
                                                            $time_seconds = 0;
                                                            $avg_log_hr_sec = 0;
                                                            $getUserId = (new \App\Helpers\CommonHelper())->getUserId($userSummaryDetail['emp_code'], $userSummaryDetail['company_id']);
                                                            ?>
                                                            @if ($getUserId && !$getUserId->isempty())
                                                                @if (array_key_exists($getUserId->id, $loggedHours))
                                                                    <?php

                                                                    //                                                    $logged_hours = sprintf('%0.2f',$loggedHours[$userSummaryDetail['emp_code']]);
                                                                    $logged_hours = (new \App\Helpers\CommonHelper())->displayTaskTime($loggedHours[$getUserId->id]);
                                                                    $hoursMinutes = explode('.', $logged_hours);
                                                                    $hours = $minutes = '00';

                                                                    if (isset($hoursMinutes[0])) {
                                                                        $hours = $hoursMinutes[0];
                                                                        $hours = strlen($hours) == 1 ? '0' . $hours : $hours;
                                                                    }

                                                                    if (isset($hoursMinutes[1])) {
                                                                        $minutes = $hoursMinutes[1];
                                                                        $minutes = strlen($minutes) == 1 ? $minutes . '0' : $minutes;
                                                                    }

                                                                    $str_time = $hours . ':' . $minutes . ':00';

                                                                    sscanf($str_time, '%d:%d:%d', $hours, $minutes, $seconds);

                                                                    $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
                                                                    if ($totalWorkingDay != 0) {
                                                                        $avg_log_hr_sec = $time_seconds / $totalWorkingDay;
                                                                        $avg_log_hr = gmdate('H:i:s', $avg_log_hr_sec);
                                                                    }
                                                                    ?>
                                                                @endif
                                                            @endif
                                                            <?php
                                                            $avg_high_class = 'green-color';
                                                            if (strtotime($avg_log_limit) > strtotime($avg_log_hr)) {
                                                                $avg_high_class = 'blue-color';
                                                            }
                                                            ?>
                                                            <span
                                                                class="badge <?php echo $avg_high_class; ?>">{{ $avg_log_hr }}</span>
                                                        </td>
                                                        <td align="center">
                                                            <a href="{{ url('/viewsummary?month=' . $viewSummeryCurrentMonth . '&year=' . $viewSummeryCurrentYear . '&employee=' . $userSummaryDetail['emp_code']) }}"
                                                                class="btn btn-primary btn-dark"
                                                                style="color:#ffffff">View Summary</a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            <tr>
                                                @if (isset($data['month']))
                                                    <td colspan='11'>
                                                        <center>
                                                            No records found for {{ date('M', $data['month']) }} month.
                                                        </center>
                                                    </td>
                                                @else
                                                    @if (!Auth::user()->hasRole(config('constant.developer_slug')))
                                                        <td colspan='16'>
                                                        @else
                                                        <td colspan='14'>
                                                    @endif

                                                    <center>
                                                        {{ 'No Record Found' }}
                                                    </center>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('javascript')
    <script src="{{ asset('js/timesheet.js?' . time()) }}"></script>
    <script src="{{ asset('dist/plugins/input-mask/jquery.inputmask.js?' . time()) }}"></script>
    <script src="{{ asset('dist/plugins/input-mask/jquery.inputmask.date.extensions.js?' . time()) }}"></script>
    <script src="{{ asset('dist/plugins/input-mask/jquery.inputmask.extensions.js?' . time()) }}"></script>
@endsection
