@extends('layouts.master')

@section('moduleName')
    View Detail Summary
@endsection

@section('style')
    <style type="text/css">
        .removeIcon {
            margin-top: 5px;
        }
    </style>
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage"><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Detail Summary</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')
<?php // echo "<prE>";print_R(date('d-m-Y',strtotime($attendance_data->entry_date)) );exit;?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tr> 
                                            <th  style="width: 20%">Date</th>
                                            <td>{{($attendance_data != NULL)?(date('d-m-Y',strtotime($attendance_data->entry_date))):''}}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>@sortablelink('project_name','Project Name')</th>
                                                <th>Description / Task Name</th>
                                                <th>Logged Hours</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($getProjectData)>0)   
                                            <?php $total_logged = 0 ;?>
                                                @foreach($getProjectData as $getProjectDataVal)
                                                <?php $total_logged = $total_logged + $getProjectDataVal->log_hours ?>
                                                <tr>
                                                    <td>{{$getProjectDataVal['project_name']}}</td>
                                                    <td>{{($getProjectDataVal['task_title'])?$getProjectDataVal['task_title']:$getProjectDataVal['desc']}}</td>
                                                    <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getProjectDataVal['log_hours'])}}</td>
                                                    <td>
                                                        @if($getProjectDataVal['user_id'] == Auth::user()->id)
                                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update" data-task-entry-id="{{$getProjectDataVal['id']}}" class="updateTaskEntry">
                                                                <i class="mdi mdi-pencil"></i>
                                                        </a>

                                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteTaskEntry" data-task-entry-id="{{$getProjectDataVal['id']}}">
                                                            <i class="mdi mdi-close"></i>
                                                        </a> 
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                                <tr>
                                                    <td></td>
                                                    <td><b>Total</b></td>
                                                    <td><b> {{(new \App\Helpers\CommonHelper)->displayTaskTime($total_logged)}}</b></td>
                                                    <td></td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td colspan='4'><center>No Entry Found</center></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="editTaskEntryDiv">
    @include('pms.project_entry.modal.edit_task_time_entry')
</div>
@endsection

@section('javascript')
<script src="{{asset('js/task_entry.js?'.time())}}"></script>
@endsection