<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header card-header bg-dark">
            <h4 class="modal-title">Add Time Entry</h4>
            <button type="button" class="close" data-dismiss="modal"
                style="color: #fff; opacity: 1;">&times;</button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-12">
                        <div class="input-group">
                            <input type="text" name="add_time_entry_date" id="add_time_entry_date" value="" class="form-control valid"  aria-invalid="false" placeholder="Please select time entry date" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="timeEntriesRow">
            
        </div>
    </div>
</div>
<script type="text/javascript">
    @php 
        $first_date = isset($selectedMonthDate) && $selectedMonthDate != date('01-m-Y') ? date('d-m-Y',strtotime('first day of '.$selectedMonthDate)) : date('d-m-Y',strtotime('first day of this month'));
        $end_date = isset($selectedMonthDate) && $selectedMonthDate != date('01-m-Y') ? date('d-m-Y',strtotime('last day of '.$selectedMonthDate)) : date('d-m-Y',strtotime('-1 days'));
    @endphp
    $(document).ready(function(){
        var d = new Date();
        
        $('#add_time_entry_date').datepicker({
            todayBtn: 1,
            autoclose: true,
            format: 'dd-mm-yyyy',
            startDate: "{{ $first_date }}",
            endDate: "{{ $end_date }}",
        });

        $(document).on('change',"#add_time_entry_date", function() {
            var emp_id = $("#emp_id").val();
            var entry_date = $(this).val();
            jQuery.ajax({
                url  : getsiteurl() + '/get_old_punch_data',
                type : "GET",
                data: {emp_id:emp_id,entry_date:entry_date},
                success: function(response) {
                    if(response.status == 'success'){
                        $('.timeEntriesRow').html(response.html);
                        $('#hidenTotalElement').val($(".element").length);
                    } else {
                        $('#addTimeEntryModal').modal('hide');
                        swal(response.message,"", "error");
                        location.reload();
                    }
                }
            });
        });
    });
</script>