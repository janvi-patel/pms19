<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header card-header bg-dark">
            <h4 class="modal-title">Update Time Entry</h4>
            <button type="button" class="close" data-dismiss="modal"
                style="color: #fff; opacity: 1;">&times;</button>
        </div>
        <div class="modal-body">
            <div class='add_leave_content'>
                <form method="post" action="{{url('/add/auto/leave')}}" id="addAutoLeaveForm">
                    @csrf 
                   
                    <input type="hidden" name="hiddenAtteIdToLeave" id="hiddenAtteIdToLeave" value="{{ $data['attendanceId'] }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check">
                                <input type="checkbox" id="update_auto_leave" name="auto_leave" class="form-check-input auto_leave_check_box" {{ old('auto_leave') ? 'checked' : '' }} style="width:auto !important;"> 
                                <label for="reason">Add Leave as auto generated?</label>
                            </div>
                            <input type="submit" class="btn btn-primary btn-dark update_auto_leave" id="addAutoLeaveBtn" value="Add Leave" disabled>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal-footer"></div>
        <form method="post" action="{{url('/update/time/entry')}}" id="timeEntryForm">
            @csrf
            <input type="hidden" name="hidenTotalElement" id="hidenTotalElement" value="">
            <input type="hidden" name="hiddenAttendanceId" id="hiddenAttendanceId" value="{{ $data['attendanceId'] }}">

            <div class="modal-body timeEntryBody_modal">
                 <div class="row">
                        
                              <div class="form-check">
                                <input type="checkbox" id="adjustment" name="adjustment" class="form-check-input auto_leave_check_box" checked style="width:auto !important;" {{(!Auth::user()->hasRole('admin'))?'disabled':''}}> 
                                <label for="reason">Adjustment?</label>
                            </div>
                       
                    </div>
                @if(isset($data['attendanceDetail']) && !empty($data['attendanceDetail']))
                    @php $i = 1; @endphp
                    @foreach ($data['attendanceDetail'] as $key => $value)
                        <div class="row element" id="inOutEntryDiv_{{ $i }}">
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <input type="text" id="inEntry_{{ $i }}" name="inEntry_{{ $i }}" value="{{ $value['in_time'] }}" class="form-control allownumeric getEnteryKey" data-key="{{ $i }}" autocomplete="off" placeholder="In Entry (hh:mm:ss)" />
                                <div class="invalid-feedback inEntry_{{ $i }}" role="alert"></div>
                            </div>
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <input type="text" id="outEntry_{{ $i }}" name="outEntry_{{ $i }}" value="{{ $value['out_time'] }}" class="form-control allownumeric" autocomplete="off" placeholder="Out Entry (hh:mm:ss)" />
                                <div class="invalid-feedback outEntry_{{ $i }}" role="alert"></div>
                            </div>
                            <div class="form-group removeIcon">
                                <a href="javascript:void(0);" title="Remove Time Entry" id="remove_{{ $i }}" class="remove" data-attendance-detail-id="{{ $value['id'] }}">X</a>
                            </div>
                        </div>
                        @php $i++ @endphp
                    @endforeach
                @else
                    No Record Found ...
                @endif
            </div>

            <div id="timeEntryError" class="alert alert-danger alert-block timeEntryError">
                <strong><span id="timeEntryErrorMessage"></span></strong>
            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-primary btn-dark" id="addRow" value="Add Row">
                <input type="submit" class="btn btn-primary btn-dark" id="updateEntry" value="Update">
                <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>