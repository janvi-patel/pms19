@if(!(isset($data['attendanceDetail']) && count($data['attendanceDetail']) > 0))
<hr class="m-0">
<div class="modal-body mb-0">
    <div class='add_leave_content'>
        <form method="post" action="{{url('/add/auto/leave')}}" id="addAutoLeaveForm">
            @csrf
            <input type="hidden" name="type" id="type" value="addNewTimeEntrie">
            <input type="hidden" name="emp_id" id="emp_id" value="{{ isset($data['emp_id']) && $data['emp_id'] != "" ? $data['emp_id'] : null  }}">
            <input type="hidden" name="entry_date" id="entry_date" value="{{ isset($data['entry_date']) && $data['entry_date'] != "" ? $data['entry_date'] : null }}">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-check">
                        <input type="checkbox" id="add_auto_leave" name="auto_leave" class="form-check-input auto_leave_check_box" {{ old('auto_leave') ? 'checked' : '' }} style="width:auto !important;"> 
                        <label for="reason">Add Leave as auto generated?</label>
                    </div>
                    <input type="submit" class="btn btn-primary btn-dark add_auto_leave" id="addAutoLeaveBtn" value="Add Leave" disabled>
                </div>
            </div>
        </form>
    </div>
</div>
<hr class="m-0">
@endif
<form method="post" action="{{url('/update/time/entry')}}" id="timeEntryForm">
    @csrf
    <input type="hidden" name="hidenTotalElement" id="hidenTotalElement" value="">
    <input type="hidden" name="type" id="type" value="addNewTimeEntrie">
    <input type="hidden" name="emp_id" id="emp_id" value="{{ isset($data['emp_id']) && $data['emp_id'] != "" ? $data['emp_id'] : null  }}">
    <input type="hidden" name="entry_date" id="entry_date" value="{{ isset($data['entry_date']) && $data['entry_date'] != "" ? $data['entry_date'] : null }}">

    <div class="modal-body timeEntryBody_modal">
        @if(isset($data['attendanceDetail']) && count($data['attendanceDetail']) > 0)
            @php $i = 1; @endphp
            @foreach ($data['attendanceDetail'] as $key => $value)
                <div class="row element" id="inOutEntryDiv_{{ $i }}">
                    <div class="form-group col-lg col-md-6 col-sm-6">
                        <input type="text" id="inEntry_{{ $i }}" name="inEntry_{{ $i }}" value="{{ $value['in_time'] }}" class="form-control allownumeric getEnteryKey" data-key="{{ $i }}" autocomplete="off" placeholder="In Entry (hh:mm:ss)" />
                    </div>
                    <div class="form-group col-lg col-md-6 col-sm-6">
                        <input type="text" id="outEntry_{{ $i }}" name="outEntry_{{ $i }}" value="{{ $value['out_time'] }}" class="form-control allownumeric" autocomplete="off" placeholder="Out Entry (hh:mm:ss)" />
                    </div>
                </div>
                @php $i++ @endphp
            @endforeach
        @else
            <div class="row element" id="inOutEntryDiv_1">
                <div class="form-group col-lg col-md-6 col-sm-6">
                    <input type="text" id="inEntry_1" name="inEntry_1" value="" class="form-control allownumeric getEnteryKey" data-key="1" autocomplete="off" placeholder="In Entry (hh:mm:ss)">
                    <div class="invalid-feedback inEntry_1" role="alert"></div>
                </div>
                <div class="form-group col-lg col-md-6 col-sm-6">
                    <input type="text" id="outEntry_1" name="outEntry_1" value="" class="form-control allownumeric" autocomplete="off" placeholder="Out Entry (hh:mm:ss)">
                    <div class="invalid-feedback outEntry_1" role="alert"></div>
                </div>
                <div class="form-group removeIcon">
                    <a href="javascript:void(0);" title="Remove Time Entry" id="remove_1" data-attendance-detail-id="" class="remove">X</a>
                </div>
            </div>
        @endif
    </div>

    <div id="timeEntryError" class="alert alert-danger alert-block timeEntryError">
        <strong><span id="timeEntryErrorMessage"></span></strong>
    </div>

    <div class="modal-footer">
        @if(!(isset($data['attendanceDetail']) && count($data['attendanceDetail']) > 0))
            <input type="button" class="btn btn-primary btn-dark" id="addRow" value="Add Row">
            <input type="submit" class="btn btn-primary btn-dark" id="updateEntry" value="Add">
        @endif
        <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
    </div>
</form>