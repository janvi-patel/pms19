@extends('layouts.master')

@section('moduleName')
View Time Sheet
@endsection

@section('style')
<style type="text/css">
    .removeIcon {
        margin-top: 5px;
    }
    .afterTimeLimitComing{
        background-color:#ff7a7a !important;
    }
</style>
@endsection

@section('content')
@php
    $departmentArray = config('constant.department');
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage"><i
                        class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Time Sheet</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body mb-0">
                    @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole('administration'))
                        <div class="right mt-3 mt-sm-0 float-right">
                            <a href="javascript:void(0);" title="Add Time Entries" class="btn btn-primary btn-dark btn-block padding-5 addTimeEntriesBtn">Add Time Entries</a>
                        </div>
                        <input type="hidden" id="emp_id" value="{{($userDetail)?$userDetail->employee_id:''}}" />
                        <input type="hidden" id="selectedMonthDate" value="{{date('01-m-Y',strtotime($monthNameYear))}}" />
                    @endif
                </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <th style="width: 20%">Employee Name</th>
                                            <td>
                                                {{($userDetail)?$userDetail->first_name.' '.$userDetail->last_name:''}}
                                            </td>
                                            <th>Month</th>
                                            <td>
                                                {{$monthNameYear}}
                                            </td>
                                        </tr>


                                        <tr>
                                            <th style="width: 20%">Employee Code</th>
                                            <td>
                                            {{(new \App\Helpers\CommonHelper)->takeEmpShortName(Auth::user()->company_id)}}{{$userDetail->employee_id}}
                                            </td>
                                             <th>Reporting To</th>
                                            <td>
                                            <?php
                                                $reportingName = (new \App\Helpers\CommonHelper)->getUserById($userDetail['reporting_to']);


                                            ?>
                                            @isset($reportingName['first_name']){{$reportingName['first_name'].' '.$reportingName['last_name']}}@endisset
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 20%">Department</th>
                                            <td>
                                            {{isset($departmentArray[$userDetail['department']]) ? $departmentArray[$userDetail['department']] : ""}}
                                            </td>
                                            <th>Total Days</th>
                                            <td>
                                            {{$total_days}}
                                            </td>
                                        </tr>


                                        <tr>
                                            <th style="width: 20%">Working Days</th>
                                            <td>
                                            {{$totalWorkingDay = $userLeaveDetails['full_days'] + ($userLeaveDetails['half_days'] / 2)}}
                                            </td>
                                            <th>Absent Days</th>
                                            <td>

                                            @if($userLeaveDetails->absent_days != '' || $userLeaveDetails->half_days != '' || $userLeaveDetails->incorrect_days != '' )
                                                <?php
                                                    $absentDays = (($userLeaveDetails->absent_days == '')?0:($userLeaveDetails->absent_days)) + (($userLeaveDetails->half_days == '')?0:($userLeaveDetails->half_days/2)) + (($userLeaveDetails->incorrect_days == '')?0:($userLeaveDetails->incorrect_days));
                                                ?>
                                            @else
                                                <?php $absentDays = 0;?>
                                            @endif
                                            {{$absentDays}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 20%">Average</th>
                                            <td>

                                            {{$avg_working_hr}}
                                            </td>
                                              <th>Monthly Logged Hours</th>
                                            <td>

                                            {{$avg_log_hr}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 20%">Approved Leaves</th>
                                            <td>

                                            {{$leaveDetails['approvedLeave']}}
                                            </td>
                                            <th>Pending Leaves</th>
                                            <td>

                                            {{$leaveDetails['pendingLeave']}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>LWP Leaves</th>
                                            <td>

                                            {{$leaveDetails['lwp']}}
                                            </td>
                                        </tr>


                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive-md table-padding">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>First In</th>
                                                <th>Last Out</th>
                                                <th>Late Comer?</th>
                                                <th>Adjustments?</th>
                                                <th>Break Hours</th>
                                                <th>Working Hours</th>
                                                <th>Logged Hours</th>
                                                <th>View</th>
                                                <th>Status</th>
                                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole('Networking') || Auth::user()->hasRole('administration'))
                                                <th>Action</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @if(count($userAttendanceDetails) > 0)
                                            @foreach($userAttendanceDetails as $value)
                                           <?php  $afterTimeLimitComing = ''; ?>

                                            @if(($value->first_in && $value->last_out) && ($value->late_comer ||
                                            $value->full_day || $value->half_day ||
                                            $value->early_going || $value->absent || $value->incorrect_entry))
                                            @if(!is_null($value->full_day) || !is_null($value->early_going))
                                                @if(date('H:i:s',strtotime($value->first_in)) > date('H:i:s',strtotime($full_day_time)))
                                                  <?php $afterTimeLimitComing = 'afterTimeLimitComing'; ?>

                                                @endif
                                                @if(!is_null($value->early_going))
                                                <?php $timeAlertColor = 'early_going_color';?>
                                                @else
                                                <?php $timeAlertColor = 'full_day_color' ;?>
                                                @endif
                                            @elseif(!is_null($value->half_day))
                                                @php
                                                    $leaveDetails = getUserLeaveDetails(['where' => ['user_id' => $userDetail->id,'leave_start_date' => $value->entry_date, 'leave_start_type' => 1]]);
                                                    $is_on_second_half = isset($leaveDetails->id) ? true : false;
                                                    if($is_on_second_half == true){
                                                        $max_half_time = date('H:i:s',strtotime($second_half_time));
                                                    } else {
                                                        $max_half_time = date('H:i:s',strtotime($first_half_time));
                                                    }
                                                @endphp
                                                @if(date('H:i:s',strtotime($value->first_in)) > $max_half_time)
                                                  <?php $afterTimeLimitComing = 'afterTimeLimitComing'; ?>

                                                @endif
                                                <?php $timeAlertColor = 'half_day_color'; ?>
                                            @elseif(!is_null($value->absent))
                                                <?php $timeAlertColor = 'absent_day_color'; ?>
                                            @else
                                                <?php $timeAlertColor = 'incorrect_day_color'; ?>
                                            @endif
                                            <tr class="{{$afterTimeLimitComing}}">
                                                <td>
                                                    <?php $entryDate = date('d-m-Y',strtotime($value->entry_date));?>
                                                    {{$entryDate.' ('.date('l', strtotime($value->entry_date)).')'}}

                                                </td>
                                                <td>{{$value->first_in}}</td>
                                                <td>{{$value->last_out}}</td>
                                                <td>
                                                    {{(is_null($value->late_comer))?'No':'Yes'}}
                                                </td>
                                                <td>
                                                    {{($value->adjustment == 0)?'No':'Yes'}}
                                                </td>
                                                <td>
                                                    {{(!is_null($value->incorrect_entry)?'00:00:00':(!is_null($value->break_hours)?$value->break_hours:'00:00:00'))}}
                                                </td>
                                                <td>
                                                    <span
                                                        class="badge {{$timeAlertColor}}">{{(is_null($value->incorrect_entry))?(!is_null($value->working_hours)?$value->working_hours:'00:00:00'):'00:00:00'}}</span>
                                                </td>
                                                <td>

                                                    @if(array_key_exists($value->entry_date, $taskLogHours))
                                                    <?php
                                                                  /*  $logged_hours = sprintf('%0.2f',$taskLogHours[$value->entry_date]);
                                                                    $hoursMinutes = (explode(".",$logged_hours));
                                                                    $hours = $minutes = '00';

                                                                    if(isset($hoursMinutes[0])){
                                                                        $hours = $hoursMinutes[0];
                                                                        $hours = (strlen($hours) == 1)?'0'.$hours:$hours;
                                                                    }

                                                                    if(isset($hoursMinutes[1])){
                                                                        $minutes = $hoursMinutes[1];
                                                                        $minutes = (strlen($minutes) == 1)?$minutes.'0':$minutes;
                                                                    }

                                                                    $str_time = $hours.':'.$minutes.':00';

                                                                    sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

                                                                    $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;*/
                                                                ?>
                                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime($taskLogHours[$value->entry_date]).':00'}}
                                                    @else
                                                    {{'00:00:00'}}
                                                    @endif
                                                </td>
                                                <td>

                                                    <center>
                                                        @if(!is_null($value->working_hours))

                                                        <a href="{{url('detailSummary/'.$value->attendance_id)}}"
                                                            data-toggle="tooltip" data-placement="top" title=""
                                                            data-original-title="View" class="project-info">
                                                            <i class="mdi mdi-eye"></i>
                                                        </a>

                                                        @else
                                                        {{'N/A'}}
                                                        @endif
                                                    </center>
                                                </td>
                                                <td>
                                                    <div class="custom_tooltip">
                                                        <a href="javascript:void(0);" id="{{$value->attendance_id}}"
                                                            class="{{(empty($value->content))?'punchData':'todayPunchData'}}">
                                                            @if(!is_null($value->full_day) ||
                                                            !is_null($value->early_going))
                                                            @if(!is_null($value->early_going))
                                                            {{'Early Going'}}
                                                            @else
                                                            {{'Present'}}
                                                            @endif
                                                            @elseif(!is_null($value->half_day))
                                                            {{'Half Day'}}
                                                            @elseif(!is_null($value->absent))
                                                            {{'Absent'}}
                                                            @else
                                                            {{'Incorrect Punch'}}
                                                            @endif
                                                        </a>
                                                        <table
                                                            class="table table-bordered view_summary_table  table-sm">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        <center>In</center>
                                                                    </th>
                                                                    <th>
                                                                        <center>Out</center>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="{{'punchInOut_'.$value->attendance_id}}">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole('Networking') || Auth::user()->hasRole('administration'))
                                                <td>



                                                    <a href="javascript:void(0);" class="nav-link updateTimeEntry"
                                                        title="Update Time Entry" id="{{$value->id}}">
                                                        <i class="mdi mdi-pencil"></i>
                                                    </a>


                                                </td>
                                                @endif
                                            </tr>
                                            @endif
                                            @endforeach
                                            @if(!empty($todayAttendance))
                                            <tr>
                                                <td>
                                                    <?php $entryDate = date('d-m-Y',strtotime($todayAttendance->entry_date));?>
                                                    {{$entryDate.' ('.date('l', strtotime($todayAttendance->entry_date)).')'}}

                                                </td>
                                                <td>{{$todayAttendance->first_in}}</td>
                                                <td>{{$todayAttendance->last_out}}</td>
                                                <td>

                                                </td>
                                                <td>

                                                </td>
                                                <td>
                                                   {{$break_hours}}
                                                </td>
                                                <td>
                                                     <span class="badge {{ $class }}">{{$working_hours}}</span>
                                                </td>
                                                <td>

                                                    @if(array_key_exists($todayAttendance->entry_date, $taskLogHours))

                                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime($taskLogHours[$todayAttendance->entry_date]).':00'}}
                                                    @else
                                                    {{'00:00:00'}}
                                                    @endif
                                                </td>
                                                <td>

                                                    <center>

                                                    </center>
                                                </td>
                                                <td>
                                                    <div class="custom_tooltip">
                                                        <a href="javascript:void(0);" id="{{$todayAttendance->id}}"
                                                            class="todayPunchData ">
                                                            Today Entry
                                                        </a>
                                                        <table
                                                            class="table table-bordered view_summary_table  table-sm">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        <center>In</center>
                                                                    </th>
                                                                    <th>
                                                                        <center>Out</center>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="{{'punchInOut_'.$todayAttendance->id}}">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole('Networking') || Auth::user()->hasRole('administration'))
                                                <td>



                                                    <a href="javascript:void(0);" class="nav-link updateTimeEntry"
                                                        title="Update Time Entry" id="{{$todayAttendance->id}}">
                                                        <i class="mdi mdi-pencil"></i>
                                                    </a>


                                                </td>
                                                @endif
                                            </tr>
                                            @endif
                                            @else
                                            <tr>
                                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                                <td colspan="10">
                                                    <center>{{'No Record Foud'}}</center>
                                                </td>
                                                @else
                                                <td colspan="9">
                                                    <center>{{'No Record Foud'}}</center>
                                                </td>
                                                @endif
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="updateTimeEntryModal" class="modal fade" role="dialog">

</div>

<div id="addTimeEntryModal" data-backdrop="static" class="modal fade" role="dialog"></div>
@endsection

@section('javascript')
<script src="{{asset('js/popper.min.js?'.time())}}"></script>
<script src="{{asset('js/viewsummary.js?'.time())}}"></script>
@endsection
