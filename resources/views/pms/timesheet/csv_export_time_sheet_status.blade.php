<div class="card-body">
    <div class="table-responsive">
        <table id="holiday_listing" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Employee Code</th>
                    <th>Employee Name</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
            @if(count($exportStatus)>0)
                @foreach($exportStatus as $data)
                <tr>
                    <td>{{$data['emp_code']}}</td>
                    <td>{{$data['emp_name']}}</td>
                    <td>{{$data['punch_time']}}</td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
