@extends('layouts.master')
@section('content')
@section('moduleName')
    Send Appraisal information
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Appraisal</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if (Session::has('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!!session('success')!!}</strong>
                </div>
                @endif    
                @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!!session('error')!!}</strong>
                </div>
                @endif
                <div class="card">
                    <div class='card-body download_text'><a href="{{asset('doc/appraisal_amount_sample.xlsx')}}"><i class="fa fa-file-excel" aria-hidden="true"></i><b> please download sample file.</b></a></div>
                </div> 
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{url('/upload/appraisal_file')}}" name="upload_csv" id="upload_csv" enctype="multipart/form-data">
                    @csrf 
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3">File Upload(Allow to upload .xlsx file)</label>
                                <div class="custom-file">
                                    <input type="file" name="csvfile" id="validatedCustomFile" class="custom-file-input" required/>
                                    <label class="custom-file-label" for="validatedCustomFile" id="uploadedFileName">Choose file...</label>
                                </div>
                            </div> 
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button name="submit" type="submit" class="btn btn-primary btn-dark">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Appraisal</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('/update_appraisal')}}" id="appraisal_form">
                    @csrf
                        <div class="card-body">
                            <div class="new_id">
                                {{-- <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="col-form-label">Effective from<span class="error">*</span></label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" name="from_date" id="from_date" value="" class="form-control valid"  aria-invalid="false">
                                            </div>
                                            <label class="from_date-error error" for="from_date" style="display : none;"></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="col-form-label">Next Increment<span class="error">*</span></label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" name="to_date" id="to_date" value="" class="form-control valid"  aria-invalid="false">
                                            </div>
                                            <label class="to_date-error error" class="error" style="display : none;" for="to_date"></label>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="row user">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                        <label for="name">Name</label>
                                        <select class="select2 form-control custom-select name" type="select" style="width: 100%; height:36px;" custom="form"  name="name_1">
                                            <option value="">Select Employee</option>
                                            @if(isset($userDetails) && $userDetails != null)
                                                @foreach($userDetails as $user)
                                                <option value="{{$user['id']}}" class="{{$user['designation_id']}}">{{$user['first_name'].' '.$user['last_name']}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="designation">Designation</label>
                                            <select class="select2 form-control custom-select designation" type="select" custom="form" style="width: 100%; height:36px;"  name="designation_1" >
                                                <option value="">Select Designation</option>
                                                @if(isset($user_designation) && $user_designation != null)
                                                    @foreach($user_designation as $designation)
                                                        <option value="{{$designation['id']}}">{{$designation['designation_name']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <label class="error" class="error" data-val-designation="designation_1" custom="label-designation" for="designation" style="display : none;"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="amount">Appraisal Amount</label>
                                            <div class="input-group"> 
                                                <input type="text" name="amount_1" onkeypress="return validateKeyStrokesNumberOnly(event)" value="0" custom="form" class="form-control amount">
                                            </div>
                                            <label class="error" class="error" custom="label-amount" data-val-amount="amount_1" for="amount" style="display : none;"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="newCtc">New CTC</label>
                                            <div class="input-group"> 
                                                <input type="text" name="newCtc_1" onkeypress="return validateKeyStrokesNumberOnly(event)" value="0" custom="form" class="form-control newCtc">
                                            </div>
                                            <label class="error" for="newCtc" custom="label-newCtc" data-val-newCtc="newCtc_1" style="display : none;"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Send" class="btn btn-primary btn-dark" >
                            <input type="button" value="Add New Row" class="btn btn-primary btn-dark" id="add" >
                            <a href="{{url('/appraisal')}}"><button type="button" value="Cancel" class="btn btn-info btn-secondary">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script src="{{asset('js/appraisal.js?'.time())}}"></script>
<script>
    $(".name").on('change',function() {
        var name_id = $(this).attr('name').split("_")[1];
        var userId = $(this).val();
        // var name_id = $(".select2").attr('name').split("_")[1];
        $.ajax({
            type: "GET",
            url: getsiteurl() + '/get_designation/' + userId,
            success: function (response) {
                $('[name=designation_'+name_id+']').val( response );
                $('[data-val-newCtc=newCtc_'+name_id+']').attr("class","error newCtc_error_"+name_id);
                $('[data-val-designation=designation_'+name_id+']').attr("class","error designation_error_"+name_id);
                $('[data-val-amount=amount_'+name_id+']').attr("class","error amount_error_"+name_id);
            }
        });
    });
    $(document).ready(function(){
        $('input[type="file"]').change(function(e){
                var fileName = e.target.files[0].name;
                $('#uploadedFileName').text(fileName);
        });
    });
    function validateKeyStrokesNumberOnly(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>
@endsection