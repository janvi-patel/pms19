<div class="card-body">
    <div class="table-responsive">
        <table id="appraisal_record" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Employee Code</th>
                    <th>Company</th>
                    <th>Designation</th>
                    <th>Appraisal Amount</th>
                    <th>New CTC</th>
                </tr>
            </thead>
            <tbody>
            @if(count($exportRecord)>0)
                @foreach($exportRecord as $data)
                <tr>
                    <td>{{$data['empcode']}}</td>
                    <td>{{$data['company']}}</td>
                    <td>{{$data['designation']}}</td>
                    <td>{{$data['appraisal_amount']}}</td>
                    <td>{{$data['new_ctc']}}</td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
