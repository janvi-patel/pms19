<table id="project_report_table" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>User Name</th>
            <th>Project Name</th>
            <th>Assigned hours</th>
            <th>Logged Hours</th>
            <th>Bench</th>
            <th>Miscellaneous</th>
        </tr>
    </thead>
    <tbody>
        @php
            $overAllTaskHours = $overAllTotalHours = $overAllLoggedHours = $overAllProHours = $overAllAssignedHours = $overAllBenchHours = $overAllMiscellaneousHours = 0;
            $test = array();
        @endphp
        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))
            @if(isset($request->search_by_reporting_to) && ($request->search_by_reporting_to != ''))
                @if(isset($req_user_team_name) && count($req_user_team_name)>0)
                    @php
                        $userIdListing = $req_user_team_name;
                    @endphp
                @else
                    @php
                        $userIdListing = $teamUserName;
                    @endphp
                @endif
            @else
                @php
                    $userIdListing = $teamUserName;
                @endphp
            @endif
        @else
            @php
                $userIdListing = $teamUserName;
            @endphp
        @endif
        @foreach ($userIdListing as $key => $val)
            @php
                $pro = 0;
                $row_count = 0;
                $userIdArray['project_id'] = [];      
                $userIdArray['details'] = [];      
                $getTotalProHours = $getLoggedHr = $getTotalHours = $getAssignedHr = $getTotalTaskHours = $getTotalBenchHours = $getTotalMiscellaneousHours = 0;
            @endphp
            @foreach ($projectReportArray as $pro_id => $pro_user)
                @php
                    if(in_array($key,array_keys($pro_user[0])))
                    {
                        if($pro == 0){
                            $userIdArray['project_id'][$key] = array($pro_id);
                            $userIdArray['details'][$key] = array($pro_user[1]);
                        }
                        else{
                            $userIdArray['project_id'][$key] = array_merge($userIdArray['project_id'][$key],array($pro=>$pro_id));   
                            $userIdArray['details'][$key] = array_merge($userIdArray['details'][$key],array($pro=>$pro_user[1]));   
                        }
                        $pro++;
                    }
                @endphp
            @endforeach
            @if(in_array($key,array_keys($userIdArray['project_id'])))
                @foreach ($userIdArray['project_id'][$key] as $project_id)
                    @foreach($projectEmpLoggedArray[$project_id] as $keys => $values)
                        @if($values['id'] == $key)
                            @php $row_count++; @endphp
                        @endif
                    @endforeach
                @endforeach
            @else
                @php $row_count = 1; @endphp
            @endif
            <tr class="{{$row_count}}"> 
                 @php
                    if(in_array($key,array_keys($userIdArray['project_id']))){
                        if(is_array($userIdArray['project_id'][$key])){
                            sort($userIdArray['project_id'][$key]);
                        }
                    }
                @endphp
                <td rowspan="{{(in_array($key,array_keys($userIdArray['project_id'])))?count($userIdArray['project_id'][$key]):1}}">{{ $loop->iteration }}</td>
                <th rowspan="{{(in_array($key,array_keys($userIdArray['project_id'])))?count($userIdArray['project_id'][$key]):1}}">{{ $val}}</th>
               
                @if(in_array($key,array_keys($userIdArray['project_id'])))
                    @foreach($userIdArray['details'][$key] as $projectDataId)
                        @php
                            $projectData = $projectDataId['project_details'];
                            $estimatedHours = (isset($projectData['pms_hours']) && $projectData['pms_hours']!='')?$projectData['pms_hours']:'0.00'; 
                            $crHours = (isset($projectCrHours[$projectDataId['project_id']]) && $projectCrHours[$projectDataId['project_id']]!='')?$projectCrHours[$projectDataId['project_id']]:'0';
                            $getTotalProHours = $crHours + $estimatedHours;
                        @endphp
                        @php $totalHours = 0;$percentage = 0.00;$totalTaskHours =0;$subArray = array();@endphp 
                        @foreach($projectEmpLoggedArray[$projectDataId['project_id']] as $k => $v)
                            @if($v['id'] == $key)
                                @php    $subArray[$v['id']]['user_id'][] = $v['id'];
                                        $subArray[$v['id']]['task_name'][] = isset($v['task_name'])?$v['task_name']:'Old Entry';
                                        $subArray[$v['id']]['taskHours'][] = isset($v['taskHours'])?$v['taskHours']:'0.00';
                                        $subArray[$v['id']]['loggedHours'][] = $v['total_logged_hours']; 
                                        $subArray[$v['id']]['taskId'][] = isset($v['taskId'])?$v['taskId']:'0';
                                @endphp
                            @endif
                        @endforeach
                        @if(count($subArray)>0)
                            @foreach($subArray as $subArrayKey => $subArrayVal)
                                @php $row_task_count = count($subArrayVal['task_name']); @endphp
                            @endforeach
                        @endif
                        <td rowspan='1'>{{$projectData['project_name']}}</td>
                        @if(count($subArray) == 0)
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                        @endif
                        
                        @if(count($subArray)>0)
                            @foreach($subArray as $subArrayKey => $subArrayVal)
                             @php
                                    $userTaskHours = $userloggedHours = $getBenchHours = $getMiscellaneousHours = 0 ;
                                @endphp
                                @for ($i = 0; $i < count($subArrayVal['task_name']); $i++)
                                
                                    @if($i != count($subArrayVal['task_name']))
                                    @php
                                        if ($projectData['project_name'] == 'Bench' ) {
                                            $getBenchHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                        }else if( $projectData['project_name'] == 'Miscellaneous Tasks' ) {
                                            $getMiscellaneousHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                        } else{
                                            $totalTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                            $totalHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                            $userTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                            $userloggedHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                        }
                                    @endphp
                                       
                                    @endif
                                    @endfor
                                     <td>@if ($projectData['project_name'] == 'Bench' || $projectData['project_name'] == 'Miscellaneous Tasks' )
                                            --
                                        @else
                                            {{(new \App\Helpers\CommonHelper)->displayTaskTime($userTaskHours)}}
                                        @endif</td>
                                        <td>@if ($projectData['project_name'] == 'Bench' || $projectData['project_name'] == 'Miscellaneous Tasks' )
                                                --
                                            @else
                                                {{(new \App\Helpers\CommonHelper)->displayTaskTime($userloggedHours)}}
                                            @endif</td>
                                        <td>@if ($projectData['project_name'] == 'Bench')
                                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime($getBenchHours)}}
                                                @else
                                                    --
                                            @endif
                                        </td>
                                        <td>@if ($projectData['project_name'] == 'Miscellaneous Tasks')
                                                {{(new \App\Helpers\CommonHelper)->displayTaskTime($getMiscellaneousHours)}}
                                            @else
                                                --
                                            @endif</td>
                                    </tr>
                            @endforeach
                        @endif
                @php
                    // user wise hours
                        $getTotalTaskHours = $getTotalTaskHours + $totalTaskHours;
                        $getTotalBenchHours += $getBenchHours;
                        $getTotalMiscellaneousHours += $getMiscellaneousHours;
                        $getTotalHours = $getTotalHours + $totalHours;
                        $totalLogged = (isset($projectLoggedArray[$projectDataId['project_id']][0]) && $projectLoggedArray[$projectDataId['project_id']][0] != 0)?$projectLoggedArray[$projectDataId['project_id']][0] : 0;
                        $getLoggedHr = $getLoggedHr + $totalLogged;
                        $totalAssigned = (isset($projectEstimatedArray[$projectDataId['project_id']][0]) && $projectEstimatedArray[$projectDataId['project_id']][0] != 0) ? $projectEstimatedArray[$projectDataId['project_id']][0] : 0;
                        $getAssignedHr = $getAssignedHr + $totalAssigned;
                        $overAllProHours = $overAllProHours + $getTotalProHours;
                @endphp
                    </tr>
                @endforeach
                @php
                    //over all total hours
                        $overAllTaskHours = $overAllTaskHours + $getTotalTaskHours;
                        $overAllTotalHours = $overAllTotalHours + $getTotalHours;
                        $overAllLoggedHours = $overAllLoggedHours + $getLoggedHr;
                        $overAllAssignedHours = $overAllAssignedHours + $getAssignedHr;
                        $overAllBenchHours += $getTotalBenchHours;
                        $overAllMiscellaneousHours += $getTotalMiscellaneousHours;
                @endphp
                @else
                <td>--</td>
                <td>--</td>
                <td>--</td>
                <td>--</td>
                <td>--</td>

                @endif
                <tr><td colspan="7"></td></tr>
                    <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                        <th colspan="3" class="text-right">Total</th>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalTaskHours)}}</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalHours)}}</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalBenchHours)}}</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalMiscellaneousHours)}}</b></td>
                    </tr>
                <tr><td colspan="7"></td></tr>
        @endforeach
            <tr><td colspan="7"></td></tr>
                <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                    <th colspan="3" class="text-right">Total</th>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTaskHours)}}</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalHours)}}</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllBenchHours)}}</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllMiscellaneousHours)}}</b></td>
                </tr>
            <tr><td colspan="7"></td></tr>
    </tbody>
</table>