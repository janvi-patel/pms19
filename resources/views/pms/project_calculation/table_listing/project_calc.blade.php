<table id="project_report_table" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Project ID</th>
            <th>Project Type</th>
            @if(Auth::user()->hasRole('developer') || isset($request->search_by_user_name) && $request->search_by_user_name != '')
                <th>User Name</th>
            @endif
            <th>Project Name</th>
            @if(!isset($request->search_by_user_name) && !Auth::user()->hasRole('developer'))
                <th>User Name</th>
            @endif
            <th>Assigned hours</th>
            <th>Logged Hours</th>
        </tr>
    </thead>
    <tbody>
        @php
            $non_billable = array();
            $is_billable = $is_non_billable = $totalBillableLoggedHours = $totalBillableTotalProjectAssignedHours = $totalBillableAssignedHours = $totalBillableProjectLoggedHours = $totalBillableProjectHours = $totalNonBillableLoggedHours = $totalNonBillableAssignedHours = $totalNonBillableProjectLoggedHours = $totalNonBillableProjectHours = $totalNonBillableTotalProjectAssignedHours = 0;
            $totalProHours = $totalEstimatedHours = $estimatedHours = 0;
            $overAllTaskHours = $overAllTotalHours = $overAllLoggedHours = $overAllProHours = $overAllAssignedHours = 0;
            $userRowCount = count($projectReportArray) * 2 ;
        @endphp
        @foreach($projectReportArray as $key => $projectReportVal)
            @if(!in_array($key,$non_billable_projects))
                @if(count($projectReportVal[0])>0)
                <?php
                    $userCount = isset($projectReportArray[$key][0]) && $projectReportArray[$key][0] > 0 ? count($projectReportArray[$key][0]) : 0;
                    $is_billable = 1;
                    $rowCount = $projectRowCount[$key];
                    $rowspan = 'rowspan='.$userCount;
                    $projectData = $projectReportVal[1]["project_details"];
                    $projectType = $projectReportVal[1]["project_details"]['projecttype'];
                    //echo "<pre>";print_r($projectType);exit;
                    $estimatedHours = (isset($projectData['pms_hours']) && $projectData['pms_hours']!='')?$projectData['pms_hours']:'0.00';
                    $crHours = (isset($projectCrHours[$key]) && $projectCrHours[$key]!='')?$projectCrHours[$key]:'0';
                    $totalProHours = $crHours + $estimatedHours;
                ?>
                <tr class='{{$rowCount}}'>
                    <td {{ $rowCount>0?$rowspan:'' }}>{{$key}}</td>
                    <td  {{ $rowCount>0?$rowspan:'' }}>{{(isset($projectType['project_type_name']) ? $projectType['project_type_name'] : '') }}</td>
                    @if(Auth::user()->hasRole('developer') || isset($request->search_by_user_name) && $request->search_by_user_name != '')
                        <th {{ $rowCount>0?$rowspan:'' }} >{{isset($projectReportArray[$key][1]['users']['user_name'])? $projectReportArray[$key][1]['users']['user_name']:''}}</th>
                    @endif
                    <td {{ $rowCount>0?$rowspan:'' }}>{{$projectData['project_name']}}</td>

                    @php $totalHours = 0;$percentage = 0.00;$totalTaskHours =0;$subArray = array();  @endphp
                        @foreach($projectEmpLoggedArray[$key] as $k => $v)
                            @php    $subArray[$v['id']]['user_id'][] = $v['id'];
                                    $subArray[$v['id']]['user_name'] = $v['users']['user_name'];
                                    $subArray[$v['id']]['task_name'][] = isset($v['task_name'])?$v['task_name']:'Old Entry';
                                    $subArray[$v['id']]['taskHours'][] = isset($v['taskHours'])?$v['taskHours']:'0.00';
                                    $subArray[$v['id']]['loggedHours'][] = $v['total_logged_hours'];
                                    $subArray[$v['id']]['taskId'][] = isset($v['taskId'])?$v['taskId']:'0';
                            @endphp
                        @endforeach
                        @if(count($subArray) == 0)
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                        @if(count($subArray)>0)
                            @foreach($subArray as $subArrayKey => $subArrayVal)
                                @if(!Auth::user()->hasRole('developer') && !isset($request->search_by_user_name))
                                    <td>{{$subArrayVal['user_name']}}</td>
                                @endif
                                @php
                                    $userTaskHours = $userloggedHours = 0 ;
                                @endphp
                                @for($i=0;$i<= count($subArrayVal['task_name']); $i++)
                                    @if($i != count($subArrayVal['task_name']))
                                        @php
                                            $totalTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                            $totalHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                            $userTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                            $userloggedHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                        @endphp
                                    @endif
                                @endfor
                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userTaskHours)}}</td>
                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userloggedHours)}}</td>
                </tr>
                            @endforeach
                        @endif
                        
                    @php
                        $getTotalTaskHours = ($totalTaskHours != 0)?$totalTaskHours : 0;
                        $overAllTaskHours = $overAllTaskHours + $getTotalTaskHours;

                        $getTotalHours = ($totalHours != 0)?$totalHours : 0;
                        $overAllTotalHours = $overAllTotalHours + $getTotalHours;

                        $getLoggedHr = (isset($projectLoggedArray[$key][0]) && $projectLoggedArray[$key][0] != 0)?$projectLoggedArray[$key][0] : 0;
                        $overAllLoggedHours = $overAllLoggedHours + $getLoggedHr;
                        $getAssignedHr = (isset($projectEstimatedArray[$key][0]) && $projectEstimatedArray[$key][0] != 0) ? $projectEstimatedArray[$key][0] : 0;
                        $overAllAssignedHours = $overAllAssignedHours + $getAssignedHr;

                        $percentage = 0.00;

                        if($projectLoggedArray[$key][0] != 0 && $totalProHours != 0){
                            $percentage = ($projectLoggedArray[$key][0]/$totalProHours)*100;
                        }
                        $getProHr = ($totalProHours != 0)?sprintf("%.2f",$totalProHours):0;
                        $overAllProHours = $overAllProHours + $totalProHours;
                    @endphp
                    <tr><td colspan="6"></td></tr>
                    <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                        <th colspan="4" class="text-right">Total</th>
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalTaskHours)}}</b></td>
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalHours)}}</b></td>
                    </tr>
                    <tr><td colspan="6"></td></tr>
                    @php
                        $totalBillableAssignedHours = $totalBillableAssignedHours + $getTotalTaskHours;
                        $totalBillableLoggedHours = $totalBillableLoggedHours + $getTotalHours;
                        $totalBillableProjectLoggedHours = $totalBillableProjectLoggedHours + $getLoggedHr;
                        $totalBillableTotalProjectAssignedHours = $totalBillableTotalProjectAssignedHours + $getAssignedHr;
                        $totalBillableProjectHours = $totalBillableProjectHours + $getProHr;
                    @endphp
                @endif
            @else
                @php
                    $non_billable[$key] = $projectReportVal;
                @endphp
            @endif
        @endforeach
         @if($is_billable == 1)
            <tr><td colspan="6"></td></tr>
            <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                <td colspan="4" class="text-bold text-right">Total Billable Hours</td>
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableAssignedHours)}}</td>
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableLoggedHours)}}</td>
            </tr>
            <tr><td colspan="6"></td></tr>
        @endif
{{-- Non Billable --}}
        @foreach($non_billable as $key => $projectReportVal)
            @if(count($projectReportVal[0])>0)
            <?php
                $userCount = isset($projectReportArray[$key][0]) && $projectReportArray[$key][0] > 0 ? count($projectReportArray[$key][0]) : 0;
                $is_non_billable = 1;
                $rowCount = $userCount;
                $rowspan = 'rowspan='.$rowCount;
                $projectData = $projectReportVal[1]['project_details'];
                $estimatedHours = (isset($projectData['pms_hours']) && $projectData['pms_hours']!='')?$projectData['pms_hours']:'0.00';
                $crHours = (isset($projectCrHours[$key]) && $projectCrHours[$key]!='')?$projectCrHours[$key]:'0';
                $totalProHours = $crHours + $estimatedHours;
            ?>
            <tr class='{{$rowCount}}'>
                    <td {{ $rowCount>0?$rowspan:'' }}>{{$key}}</td>
                    <td  {{ $rowCount>0?$rowspan:'' }}>{{(isset($projectType['project_type_name']) ? $projectType['project_type_name'] : '') }}</td>
                    @if(Auth::user()->hasRole('developer') || isset($request->search_by_user_name) && $request->search_by_user_name != '')
                        <th {{ $rowCount>0?$rowspan:'' }}>{{isset($projectReportArray[$key][1]['users']['user_name'])? $projectReportArray[$key][1]['users']['user_name']:''}}</th>
                    @endif
                    <td {{ $rowCount>0?$rowspan:'' }}>{{$projectData['project_name']}}</td>

                    @php $totalHours = 0;$percentage = 0.00;$totalTaskHours =0;$subArray = array();  @endphp

                        @foreach($projectEmpLoggedArray[$key] as $k => $v)
                            @php    $subArray[$v['id']]['user_id'][] = $v['id'];
                                    $subArray[$v['id']]['user_name'] = $v['users']['user_name'];
                                    $subArray[$v['id']]['task_name'][] = isset($v['task_name'])?$v['task_name']:'Old Entry';
                                    $subArray[$v['id']]['taskHours'][] = isset($v['taskHours'])?$v['taskHours']:'0.00';
                                    $subArray[$v['id']]['loggedHours'][] = $v['total_logged_hours'];
                                    $subArray[$v['id']]['taskId'][] = isset($v['taskId'])?$v['taskId']:'0';
                            @endphp
                        @endforeach
                        @if(count($subArray) == 0)
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                        @if(count($subArray)>0)
                            @foreach($subArray as $subArrayKey => $subArrayVal)
                                @if(!Auth::user()->hasRole('developer') && !isset($request->search_by_user_name))
                                    <td>{{$subArrayVal['user_name']}}</td>
                                @endif
                                @php
                                    $userTaskHours = $userloggedHours = 0 ;
                                @endphp
                                @for($i=0;$i<= count($subArrayVal['task_name']); $i++)
                                    @if($i != count($subArrayVal['task_name']))
                                        @php
                                            $totalTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                            $totalHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                            $userTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                            $userloggedHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                        @endphp
                                    @endif
                                @endfor
                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userTaskHours)}}</td>
                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userloggedHours)}}</td>
                </tr>
                            @endforeach
                        @endif
                @php
                    $getTotalTaskHours = ($totalTaskHours != 0)?$totalTaskHours : 0;
                    $overAllTaskHours = $overAllTaskHours + $getTotalTaskHours;

                    $getTotalHours = ($totalHours != 0)?$totalHours : 0;
                    $overAllTotalHours = $overAllTotalHours + $getTotalHours;

                    $getLoggedHr = (isset($projectLoggedArray[$key][0]) && $projectLoggedArray[$key][0] != 0)?$projectLoggedArray[$key][0] : 0;
                    $overAllLoggedHours = $overAllLoggedHours + $getLoggedHr;
                    $getAssignedHr = (isset($projectEstimatedArray[$key][0]) && $projectEstimatedArray[$key][0] != 0) ? $projectEstimatedArray[$key][0] : 0;
                    $overAllAssignedHours = $overAllAssignedHours + $getAssignedHr;

                    $percentage = 0.00;

                    if($projectLoggedArray[$key][0] != 0 && $totalProHours != 0){
                        $percentage = ($projectLoggedArray[$key][0]/$totalProHours)*100;
                    }
                    $getProHr = ($totalProHours != 0)?sprintf("%.2f",$totalProHours):0;
                    $overAllProHours = $overAllProHours + $totalProHours;
                @endphp
                <tr><td colspan="6"></td></tr>
                 <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                        <th colspan="4" class="text-right">Total</th>
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalTaskHours)}}</b></td>
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalHours)}}</b></td>
                    </tr>
                    <tr><td colspan="6"></td></tr>
                @php
                    $totalNonBillableAssignedHours = $totalNonBillableAssignedHours + $getTotalTaskHours;
                    $totalNonBillableLoggedHours = $totalNonBillableLoggedHours + $getTotalHours;
                    $totalNonBillableProjectLoggedHours = $totalNonBillableProjectLoggedHours + $getLoggedHr;
                    $totalNonBillableTotalProjectAssignedHours = $totalNonBillableTotalProjectAssignedHours + $getAssignedHr;
                    $totalNonBillableProjectHours = $totalNonBillableProjectHours + $getProHr;
                @endphp
            @endif
        @endforeach
         @if($is_non_billable == 1)
            <tr><td colspan="6"></td></tr>
            <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                <td colspan="4" class="text-bold text-right">Total Non Billable Hours</td>
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableAssignedHours)}}</td>
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableLoggedHours)}}</td>
            </tr>
        @endif
    </tbody>
</table>
