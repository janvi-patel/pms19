@extends('layouts.master')
@section('content')
@section('moduleName')
    Approve Leave Request
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/teamleaves')}}">Team Leave Requests</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Approve Leave Request</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
     <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header  bg-dark">
                        <h3 class="card-title">Approve Leave Request</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('/leaves/update/'.$leaveDetails['id'])}}" name="leave_update" id="leave_update">
                    @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="name" >Name</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text"><i class="fa fa-user"></i></span>
                                        </div> 
                                    <input type="text" name="name" id="name" value="{{$leaveDetails['user']['first_name'].' '.$leaveDetails['user']['last_name']}}" class="form-control" disabled="">
                                    </div> 
                                </div>
                                <div class="col-md-2">
                                    <label>Approver</label>
                                    <select class="select2 form-control custom-select" name="approver_id" id="approver_id" style="width: 100%; height:36px;" disabled="" >
                                        <option value="">Select Approver</option>
                                        @foreach($reportingToList as $reportingTo)
                                        <option value="{{$reportingTo['id']}}" {{ $leaveDetails['approver_id'] == 
                                        $reportingTo['id'] ? 'selected' : ''}}>{{$reportingTo['first_name'].' '.$reportingTo['last_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label >Leave Start Date</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div> 
                                                <input type="text" name="leave_start_date" id="leave_start_date" value="{{ old('leave_start_date') != '' ? old('leave_start_date') : $leaveDetails['leave_start_date']}}" class="form-control" readonly="" />
                                                @if ($errors->has('leave_start_date'))
                                                    <div class="error">{{ $errors->first('leave_start_date') }}</div>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="leave_start_type" id="leave_start_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" disabled="" >
                                                    <option value="0" {{ old('leave_start_type') != '' ? (old('leave_start_type') == 0 ? 'selected' : '') : ($leaveDetails['leave_start_type'] == 0 ? 'selected' : '')}}>Full Day</option>
                                                    <option value="1" {{ old('leave_start_type') != '' ? (old('leave_start_type') == 1 ? 'selected' : '') : ($leaveDetails['leave_start_type'] == 1 ? 'selected' : '')}}>First Half</option>
                                                    <option value="2" {{ old('leave_start_type') != '' ? (old('leave_start_type') == 2 ? 'selected' : '') : ($leaveDetails['leave_start_type'] == 2 ? 'selected' : '')}}>Second Half</option>
                                                </select>
                                                @if ($errors->has('leave_start_type'))
                                                    <div class="error">{{ $errors->first('leave_start_type') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label >Leave End Date</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div> 
                                                <input type="text" name="leave_end_date" id="leave_end_date" value="{{ old('leave_end_date') != '' ? old('leave_end_date') : $leaveDetails['leave_end_date']}}" class="form-control" readonly="" />
                                                <input type="hidden" name="joinning_date" id="joinning_date" value= "{{ Auth::user()->joining_date }}" class="form-control"/>
                                                @if ($errors->has('leave_end_date'))
                                                <div class="error">{{ $errors->first('leave_end_date') }}</div>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="leave_end_type" id="leave_end_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" disabled="" >
                                                    <option value="0" {{ old('leave_end_type') != '' ? (old('leave_end_type') == 0 ? 'selected' : '') : ($leaveDetails['leave_end_type'] == 0 ? 'selected' : '')}}>Full Day</option>
                                                    <option value="1" {{ old('leave_end_type') != '' ? (old('leave_end_type') == 1 ? 'selected' : '') : ($leaveDetails['leave_end_type'] == 1 ? 'selected' : '')}}>First Half</option>
                                                    <option value="2" {{ old('leave_end_type') != '' ? (old('leave_end_type') == 2 ? 'selected' : '') : ($leaveDetails['leave_end_type'] == 2 ? 'selected' : '')}}>Second Half</option>
                                                </select>
                                                @if ($errors->has('leave_end_type'))
                                                    <div class="error">{{ $errors->first('leave_end_type') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                     <label for="reason" >Leave Reason</label>
                                        <textarea class="form-control" name="reason" id="reason" readonly="">{{ old('reason') != '' ? old('reason') : $leaveDetails['reason']}}</textarea>
                                        @if ($errors->has('reason'))
                                            <div class="error">{{ $errors->first('reason') }}</div>
                                        @endif
                                </div>
                                <div class="col-md-4">
                                    <label for="approver_comment" >Approver Comment</label>
                                    <span class="error">*</span>
                                        <textarea class="form-control" name="approver_comment" id="approver_comment" >{{ old('approver_comment') != '' ? old('approver_comment') : $leaveDetails['approver_comment']}}</textarea>
                                        @if ($errors->has('approver_comment'))
                                            <div class="error">{{ $errors->first('approver_comment') }}</div>
                                        @endif
                                </div>
                                <div class="col-md-4">
                                    <label >Leave Status</label>
                                    <?php 
                                    $disabled = '';
//                                    if($leaveDetails['leave_end_date']){
//                                        if($leaveDetails['leave_end_date'] < date('Y-m-d')){
//                                            $disabled = 'disabled';
//                                        }
//                                    }
                                    ?>
                                @if($leaveDetails['leave_status']==1)
                                <select name="leave_status" id="leave_status" class="select2 form-control custom-select" style="width: 100%; height:36px;" required="" <?php echo $disabled;?>>
                                    <option value="">Select Leave Status</option>
                                    @foreach(config('constant.leave_status') as $key => $value)
                                    <?php
                                    if($value == "Pending"){
                                        continue;
                                    }
                                    
                                    ?>
                                    <option value="{{$key}}" {{ old('leave_status') != '' ? (old('leave_status') == $key ? 'selected' : '') : ($leaveDetails['leave_status'] == $key ? 'selected' : '')}}>{{$value}}</option>
                                    @endforeach
                                </select>
                                @else
                                <select name="leave_status" id="leave_status" class="select2 form-control custom-select" style="width: 100%; height:36px;" required="" <?php echo $disabled;?>>
                                    <option value="">Select Leave Status</option>
                                    @foreach(config('constant.leave_status') as $key => $value)
                                    <option value="{{$key}}" {{ old('leave_status') != '' ? (old('leave_status') == $key ? 'selected' : '') : ($leaveDetails['leave_status'] == $key ? 'selected' : '')}}>{{$value}}</option>
                                    @endforeach
                                </select>
                                @endif
                                
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark">
                            <a href="{{url('/teamleaves')}}"><button type="button" value="Cancel" class="btn btn-info btn-secondary">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script src="{{asset('js/leaves?'.time())}}"></script>
@endsection