@extends('layouts.master')
@section('content')
@section('moduleName')
    Add Leave Request
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/leaverequests')}}">Leave Requests</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add Leave Request</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Add Leave Request</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{!! route('leaveStore') !!}" name="task_create" id="task_leave_create" autocomplete="off">
                    @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2 form-group col">
                                    <label for="name">Name</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text"><i class="fa fa-user"></i></span>
                                        </div>  
                                         <input type="text" value="{{$userDetails['first_name'].' '.$userDetails['last_name']}}" class="form-control" disabled="">
                                    </div>
                                </div> 
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Approver</label>
                                        @php $approver = (new \App\Helpers\CommonHelper)->getAnyUserById($reportingTo); @endphp
                                        <input type="text" value="{{$approver["first_name"]!=null?$approver["first_name"].' '.$approver["last_name"]:'N/A'}}" class="form-control" disabled="">
                                    </div> 
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Leave Start Date</label>
                                            <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                              <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                            </div> 
                                                            <input type="text" name="leave_start_date" id="cre_leave_start_date" value="{{ old('leave_start_date') }}" class="form-control" />
                                                        </div>
                                                        @if ($errors->has('leave_start_date'))
                                                            <div class="error">{{ $errors->first('leave_start_date') }}</div>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select name="leave_start_type" id="leave_start_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                                            <option value="0">Full Day</option>
                                                            <option value="1">First Half</option>
                                                            <option value="2">Second Half</option>
                                                        </select>
                                                        @if ($errors->has('leave_start_type'))
                                                            <div class="error">{{ $errors->first('leave_start_type') }}</div>
                                                        @endif
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Leave End Date</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div> 
                                                    <input type="text" name="leave_end_date" id="cre_leave_end_date" value="{{ old('leave_end_date') }}" class="form-control" />
                                                    <input type="hidden" name="joinning_date" id="joinning_date" value= "{{ Auth::user()->joining_date }}" class="form-control"/>
                                                </div>
                                                @if ($errors->has('leave_end_date'))
                                                    <div class="error">{{ $errors->first('leave_end_date') }}</div>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <select name="leave_end_type" id="leave_end_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                                    <option value="0">Full Day</option>
                                                    <option value="1">First Half</option>
                                                    <option value="2">Second Half</option>
                                                </select>
                                                @if ($errors->has('leave_end_type'))
                                                    <div class="error">{{ $errors->first('leave_end_type') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                          
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="reason">Leave Reason</label>
                                        <textarea class="form-control" name="reason" id="reason" ></textarea>
                                        @if ($errors->has('reason'))
                                            <div class="error">{{ $errors->first('reason') }}</div>
                                        @endif
                                    </div>
                                </div>                          
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                        <div class="form-check">
                                            <input type="checkbox" id="adhoc_leave" name="adhoc_leave" class="form-check-input" {{ old('adhoc_leave') ? 'checked' : '' }} style="width:auto !important;"> 
                                            <label for="reason">Adhoc Leave</label>
                                        </div>
                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark" >
                            <a href="{{url('/leaverequests')}}"><button type="button" value="Cancel" class="btn btn-info btn-secondary">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script src="{{asset('js/leaves.js?'.time())}}"></script>
@endsection