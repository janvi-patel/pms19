@extends('layouts.master')


@section('moduleName')
    Holiday
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('css/fullcalendar.min.css')}}">
    <link rel="stylesheet" type="text/css" media="print" href="{{asset('css/fullcalendar.print.css')}}">
    <style type="text/css">
        .fc-time{
           display : none;
        }
        .fc-sat, .fc-sun {
            background-color: #FCF8E3 !important;
        }
        .fc-today {
            background-color: #9ee26c !important;
        }
    </style>
@endsection

@include('sweet::alert')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Holiday</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Holiday</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content" id="main_calendar">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">Draggable Holiday</h4>
          </div>
          <div class="card-body">
            <!-- the events -->
            <div id="external-events">
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /. box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Create Holiday</h3>
          </div>
          <div class="card-body">
            <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
              <ul class="fc-color-picker" id="color-chooser">
                <li><a class="text-primary" href="#"><i class="fa fa-square"></i></a></li>
                <li><a class="text-warning" href="#"><i class="fa fa-square"></i></a></li>
                <li><a class="text-success" href="#"><i class="fa fa-square"></i></a></li>
                <li><a class="text-danger" href="#"><i class="fa fa-square"></i></a></li>
                <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
              </ul>
            </div>
            <!-- /btn-group -->
            <div class="input-group">
              <input id="new-event" type="text" class="form-control" placeholder="Event Title">

              <div class="input-group-append">
                <button id="add-new-event" type="button" class="btn btn-primary btn-flat rounded-right">Add</button>
              </div>
              <!-- /btn-group -->
            </div>
            <!-- /input-group -->
          </div>
        </div>
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Create Weekend</h3>
          </div>
          <div class="card-body">
            <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
              <ul class="fc-color-picker" id="weekend-color-chooser">
                <li><a class="text-primary" href="#"><i class="fa fa-square"></i></a></li>
                <li><a class="text-warning" href="#"><i class="fa fa-square"></i></a></li>
                <li><a class="text-success" href="#"><i class="fa fa-square"></i></a></li>
                <li><a class="text-danger" href="#"><i class="fa fa-square"></i></a></li>
                <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
              </ul>
            </div>
            <!-- /btn-group -->
            <div class="input-group">
              <input id="new-weekend-event" type="text" class="form-control" placeholder="Event Title">

              <div class="input-group-append">
                <button id="add-new-weekend-event" type="button" class="btn btn-primary btn-flat rounded-right">Add</button>
              </div>
              <!-- /btn-group -->
            </div>
            <!-- /input-group -->
          </div>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card card-primary">
          <div class="card-body p-0">
            <!-- THE CALENDAR -->
            <div id="calendar"></div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /. box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>  

@endsection

@section('javascript')
<script src="{{asset('js/moment.min.js?'.time())}}"></script>
<script src="{{asset('js/fullcalendar.min.js?'.time())}}"></script>
<script src="{{asset('js/holiday.js?'.time())}}"></script>
@endsection