@extends('layouts.master')
@section('content')

@section('moduleName')
    Setting
@endsection

@php
$pageRangArray = config('constant.page_range');
$statusArray = config('constant.status');
@endphp
@include('sweet::alert')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Designation Management</h1> -->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Setting Management</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
 <section class="content">
    <div class="container-fluid">
        <div id="accordion">
             <div class="card">
                    <div class="card-header bg-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                        <h3 class="card-title">Setting</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div id="collapseOne" class="panel-collapse in collapse show" style="">
                        <br>
                        <div class="p-3">
                            <h2 class="card-title">Setting for PMS restriction</h2>
                            <hr>
                                <div class="p-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Allow Timeentry Day
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/taskEntryDayUpdate')}}" name="timeentryDay_form" id="timeentryDay_form">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="timeentry_day" id="timeentry_day" value="{{$dataArray['TaskEntry']['days']}}" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_timeentry_day"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_timeentry_day"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    @php
                                                        $crMonth = isset($dataArray['cr_operation']['constant_int_value']) && $dataArray['cr_operation']['constant_int_value'] != "" ? $dataArray['cr_operation']['constant_int_value'] : "0";
                                                    @endphp
                                                    Allow time to select CR / Project while creating task (Restricted before {{ date("01-m-Y",strtotime("-".$crMonth." months")) }})
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/cr_select_time_restriction')}}" name="crOperationMonth" id="crOperationMonth">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="cr_operation_month" id="cr_operation_month" value="{{ $crMonth }}" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_cr_operation_month"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_cr_operation_month"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Task Related Operation ({{date($dataArray['task_operation']['days']."-M-Y")}})
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/taskOperationDaysUpdate')}}" name="task_operation_day_form" id="task_operation_day_form">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="task_operation_day" id="task_operation_day" value="{{$dataArray['task_operation']['days']}}" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_task_operation_day"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_task_operation_day"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Late Come Time(In Second)
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/lateComerTimeUpdate')}}" name="lateComeTime_form" id="lateComeTime_form">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="lateComeTime" id="lateComeTime" value="{{$dataArray['LateComeTime']['constant_str_value'] != null?$dataArray['LateComeTime']['constant_str_value']:config('constant.late_from_sec') }}" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_lateComeTime"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_lateComeTime"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Daily working time(In Second)
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/workingHourUpdate')}}" name="timeentryDay_form" id="timeentryDay_form">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="working_hour" id="daily_working_time" value="{{$dataArray['average_working_time']['constant_str_value'] != null?$dataArray['average_working_time']['constant_str_value'] :config('constant.avg_working_time')}}" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_working_time"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_working_time"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        @php
                                            $hoursMinutes = array();
                                            $storedHours = $storedMinutes = '';
                                            if(isset($dataArray) && isset($dataArray['average_logged_time']['constant_time']) && $dataArray['average_logged_time']['constant_time'] != ''){
                                                $hoursMinutes   = explode(":",$dataArray['average_logged_time']['constant_time']);
                                                $storedHours    = isset($hoursMinutes[0])?$hoursMinutes[0]:0;
                                                $storedMinutes  = isset($hoursMinutes[1])?$hoursMinutes[1]:0;
                                            }
                                            else{
                                                $hoursMinutes   = explode(":",config('constant.avg_log_limit'));
                                                $storedHours    = isset($hoursMinutes[0])?$hoursMinutes[0]:0;
                                                $storedMinutes  = isset($hoursMinutes[1])?$hoursMinutes[1]:0;
                                            }
                                        @endphp
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Daily logged time
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/loggedHourUpdate')}}" name="lateComeTime_form" id="lateComeTime_form">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <select class="form-control" name="logged_hours_time" id="logged_hours_time" style="width: 90px;" disabled="true">
                                                        <option value="00" @if($storedHours == 0){{"selected"}}@endif>00</option>
                                                        <option value="01" @if($storedHours == 1){{"selected"}}@endif>01</option>
                                                        <option value="02" @if($storedHours == 2){{"selected"}}@endif>02</option>
                                                        <option value="03" @if($storedHours == 3){{"selected"}}@endif>03</option>
                                                        <option value="04" @if($storedHours == 4){{"selected"}}@endif>04</option>
                                                        <option value="05" @if($storedHours == 5){{"selected"}}@endif>05</option>
                                                        <option value="06" @if($storedHours == 6){{"selected"}}@endif>06</option>
                                                        <option value="07" @if($storedHours == 7){{"selected"}}@endif>07</option>
                                                        <option value="08" @if($storedHours == 8){{"selected"}}@endif>08</option>
                                                        <option value="09" @if($storedHours == 9){{"selected"}}@endif>09</option>
                                                        <option value="10" @if($storedHours == 10){{"selected"}}@endif>10</option>
                                                        <option value="11" @if($storedHours == 11){{"selected"}}@endif>11</option>
                                                        <option value="12" @if($storedHours == 12){{"selected"}}@endif>12</option>
                                                    </select>
                                                    <select class="form-control" name="logged_minute_time" id="logged_minute_time" style="width: 90px" disabled="true">
                                                        <option value="00" @if($storedMinutes == 0){{"selected"}}@endif>00</option>
                                                        <option value="15" @if($storedMinutes == 15){{"selected"}}@endif>15</option>
                                                        <option value="30" @if($storedMinutes == 30){{"selected"}}@endif>30</option>
                                                        <option value="45" @if($storedMinutes == 45){{"selected"}}@endif>45</option>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_logged_time"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_logged_time"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Full Day
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/startTimeforfullday')}}" name="other_allownces" id="other_allownces">
                                                    @csrf
                                                    <?php
                                                    $totalhoursMinutesfull = array();
                                                    $fullstoredHours = $fullstoredMinutes = '';
                                                    if(isset($dataArray) && isset($dataArray['max_full_day_start_time']['constant_time']) && $dataArray['max_full_day_start_time']['constant_time'] != ''){
                                                        $totalhoursMinutesfull   = explode(":",$dataArray['max_full_day_start_time']['constant_time']);
                                                        $fullstoredHours    = isset($totalhoursMinutesfull[0])?$totalhoursMinutesfull[0]:0;
                                                        $fullstoredMinutes  = isset($totalhoursMinutesfull[1])?$totalhoursMinutesfull[1]:0;
                                                    }
                                                    else{
                                                        $totalhoursMinutesfull   = explode(":",config('constant.max_full_day_start_time'));
                                                        $fullstoredHours    = isset($totalhoursMinutesfull[0])?$totalhoursMinutesfull[0]:0;
                                                        $fullstoredMinutes  = isset($totalhoursMinutesfull[1])?$totalhoursMinutesfull[1]:0;
                                                    }
                                                ?>
                                                    <div class="input-group mb-3">
                                                        <select class="form-control" name="full_day_hours_time" id="full_day_hours_time" style="width: 90px;" disabled="true">
                                                            <option value="00" @if($fullstoredHours==0){{"selected"}}@endif>00</option>
                                                            <option value="01" @if($fullstoredHours==1){{"selected"}}@endif>01</option>
                                                            <option value="02" @if($fullstoredHours==2){{"selected"}}@endif>02</option>
                                                            <option value="03" @if($fullstoredHours==3){{"selected"}}@endif>03</option>
                                                            <option value="04" @if($fullstoredHours==4){{"selected"}}@endif>04</option>
                                                            <option value="05" @if($fullstoredHours==5){{"selected"}}@endif>05</option>
                                                            <option value="06" @if($fullstoredHours==6){{"selected"}}@endif>06</option>
                                                            <option value="07" @if($fullstoredHours==7){{"selected"}}@endif>07</option>
                                                            <option value="08" @if($fullstoredHours==8){{"selected"}}@endif>08</option>
                                                            <option value="09" @if($fullstoredHours==9){{"selected"}}@endif>09</option>
                                                            <option value="10" @if($fullstoredHours==10){{"selected"}}@endif>10</option>
                                                            <option value="11" @if($fullstoredHours==11){{"selected"}}@endif>11</option>
                                                            <option value="12" @if($fullstoredHours==12){{"selected"}}@endif>12</option>
                                                        </select>
                                                        <select class="form-control" name="full_day_minute_time" id="full_day_minute_time" style="width: 90px" disabled="true">
                                                            <option value="00" @if($fullstoredMinutes==0){{"selected"}}@endif>00</option>
                                                            <option value="15" @if($fullstoredMinutes==15){{"selected"}}@endif>15</option>
                                                            <option value="30" @if($fullstoredMinutes==30){{"selected"}}@endif>30</option>
                                                            <option value="45" @if($fullstoredMinutes==45){{"selected"}}@endif>45</option>
                                                        </select>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="button" id="edit_full_day"><i class="mdi mdi-pencil"></i></button>
                                                            <button class="btn btn-outline-secondary" type="submit" id="save_full_day"><i class="mdi mdi-content-save"></i></button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    First Half
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/startTimeforFirstHalf')}}" name="other_allownces" id="other_allownces">
                                                    @csrf
                                                    <?php
                                                    $totalhoursMinutes = array();
                                                    $firststoredHours = $firststoredMinutes = '';
                                                    if(isset($dataArray) && isset($dataArray['max_first_half_start_time']['constant_time']) && $dataArray['max_first_half_start_time']['constant_time'] != ''){
                                                        $totalhoursMinutes   = explode(":",$dataArray['max_first_half_start_time']['constant_time']);
                                                        $firststoredHours    = isset($totalhoursMinutes[0])?$totalhoursMinutes[0]:0;
                                                        $firststoredMinutes  = isset($totalhoursMinutes[1])?$totalhoursMinutes[1]:0;
                                                    }
                                                    else{
                                                        $totalhoursMinutes   = explode(":",config('constant.max_first_half_start_time'));
                                                        $firststoredHours    = isset($totalhoursMinutes[0])?$totalhoursMinutes[0]:0;
                                                        $firststoredMinutes  = isset($totalhoursMinutes[1])?$totalhoursMinutes[1]:0;
                                                    }
                                                ?>
                                                    <div class="input-group mb-3">
                                                        <select class="form-control" name="first_half_hours_time" id="first_half_hours_time" style="width: 90px;" disabled="true">
                                                            <option value="00" @if($firststoredHours==0){{"selected"}}@endif>00</option>
                                                            <option value="01" @if($firststoredHours==1){{"selected"}}@endif>01</option>
                                                            <option value="02" @if($firststoredHours==2){{"selected"}}@endif>02</option>
                                                            <option value="03" @if($firststoredHours==3){{"selected"}}@endif>03</option>
                                                            <option value="04" @if($firststoredHours==4){{"selected"}}@endif>04</option>
                                                            <option value="05" @if($firststoredHours==5){{"selected"}}@endif>05</option>
                                                            <option value="06" @if($firststoredHours==6){{"selected"}}@endif>06</option>
                                                            <option value="07" @if($firststoredHours==7){{"selected"}}@endif>07</option>
                                                            <option value="08" @if($firststoredHours==8){{"selected"}}@endif>08</option>
                                                            <option value="09" @if($firststoredHours==9){{"selected"}}@endif>09</option>
                                                            <option value="10" @if($firststoredHours==10){{"selected"}}@endif>10</option>
                                                            <option value="11" @if($firststoredHours==11){{"selected"}}@endif>11</option>
                                                            <option value="12" @if($firststoredHours==12){{"selected"}}@endif>12</option>
                                                        </select>
                                                        <select class="form-control" name="first_half_minute_time" id="first_half_minute_time" style="width: 90px" disabled="true">
                                                            <option value="00" @if($firststoredMinutes==0){{"selected"}}@endif>00</option>
                                                            <option value="15" @if($firststoredMinutes==15){{"selected"}}@endif>15</option>
                                                            <option value="30" @if($firststoredMinutes==30){{"selected"}}@endif>30</option>
                                                            <option value="45" @if($firststoredMinutes==45){{"selected"}}@endif>45</option>
                                                        </select>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="button" id="edit_first_half"><i class="mdi mdi-pencil"></i></button>
                                                            <button class="btn btn-outline-secondary" type="submit" id="save_first_half"><i class="mdi mdi-content-save"></i></button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Second Half
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/startTimeforSecondtHalf')}}" name="other_allownces" id="other_allownces">
                                                    @csrf
                                                    <?php
                                                    $totalhoursMinutes = array();
                                                    $secondstoredHours = $secondstoredMinutes = '';
                                                    if(isset($dataArray) && isset($dataArray['max_second_half_start_time']['constant_time']) && $dataArray['max_second_half_start_time']['constant_time'] != ''){
                                                        $totalhoursMinutes   = explode(":",$dataArray['max_second_half_start_time']['constant_time']);
                                                        $secondstoredHours    = isset($totalhoursMinutes[0])?$totalhoursMinutes[0]:0;
                                                        $secondstoredMinutes  = isset($totalhoursMinutes[1])?$totalhoursMinutes[1]:0;
                                                    }
                                                    else{
                                                        $totalhoursMinutes   = explode(":",config('constant.max_second_half_start_time'));
                                                        $secondstoredHours    = isset($totalhoursMinutes[0])?$totalhoursMinutes[0]:0;
                                                        $secondstoredMinutes  = isset($totalhoursMinutes[1])?$totalhoursMinutes[1]:0;
                                                    }
                                                ?>
                                                    <div class="input-group mb-3">
                                                        <select class="form-control" name="second_half_hours_time" id="second_half_hours_time" style="width: 90px;" disabled="true">
                                                            <option value="00" @if($secondstoredHours==0){{"selected"}}@endif>00</option>
                                                            <option value="01" @if($secondstoredHours==1){{"selected"}}@endif>01</option>
                                                            <option value="02" @if($secondstoredHours==2){{"selected"}}@endif>02</option>
                                                            <option value="03" @if($secondstoredHours==3){{"selected"}}@endif>03</option>
                                                            <option value="04" @if($secondstoredHours==4){{"selected"}}@endif>04</option>
                                                            <option value="05" @if($secondstoredHours==5){{"selected"}}@endif>05</option>
                                                            <option value="06" @if($secondstoredHours==6){{"selected"}}@endif>06</option>
                                                            <option value="07" @if($secondstoredHours==7){{"selected"}}@endif>07</option>
                                                            <option value="08" @if($secondstoredHours==8){{"selected"}}@endif>08</option>
                                                            <option value="09" @if($secondstoredHours==9){{"selected"}}@endif>09</option>
                                                            <option value="10" @if($secondstoredHours==10){{"selected"}}@endif>10</option>
                                                            <option value="11" @if($secondstoredHours==11){{"selected"}}@endif>11</option>
                                                            <option value="12" @if($secondstoredHours==12){{"selected"}}@endif>12</option>
                                                            <option value="13" @if($secondstoredHours==13){{"selected"}}@endif>13</option>
                                                            <option value="14" @if($secondstoredHours==14){{"selected"}}@endif>14</option>
                                                            <option value="15" @if($secondstoredHours==15){{"selected"}}@endif>15</option>
                                                            <option value="16" @if($secondstoredHours==16){{"selected"}}@endif>16</option>
                                                            <option value="17" @if($secondstoredHours==17){{"selected"}}@endif>17</option>
                                                            <option value="18" @if($secondstoredHours==18){{"selected"}}@endif>18</option>
                                                            <option value="19" @if($secondstoredHours==19){{"selected"}}@endif>19</option>
                                                            <option value="20" @if($secondstoredHours==20){{"selected"}}@endif>20</option>
                                                            <option value="21" @if($secondstoredHours==21){{"selected"}}@endif>21</option>
                                                            <option value="22" @if($secondstoredHours==22){{"selected"}}@endif>22</option>
                                                            <option value="23" @if($secondstoredHours==23){{"selected"}}@endif>23</option>
                                                            <option value="24" @if($secondstoredHours==24){{"selected"}}@endif>24</option>
                                                        </select>
                                                        <select class="form-control" name="second_half_minute_time" id="second_half_minute_time" style="width: 90px" disabled="true">
                                                            <option value="00" @if($secondstoredMinutes==0){{"selected"}}@endif>00</option>
                                                            <option value="15" @if($secondstoredMinutes==15){{"selected"}}@endif>15</option>
                                                            <option value="30" @if($secondstoredMinutes==30){{"selected"}}@endif>30</option>
                                                            <option value="45" @if($secondstoredMinutes==45){{"selected"}}@endif>45</option>
                                                        </select>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="button" id="edit_second_half"><i class="mdi mdi-pencil"></i></button>
                                                            <button class="btn btn-outline-secondary" type="submit" id="save_second_half"><i class="mdi mdi-content-save"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Email For Server
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/email_for_server')}}" name="email_for_server_menu" id="email_for_server_menu">
                                                    @csrf
                                                    <div class="input-group mb-3">
                                                        <select class="form-control" name="email_for_server" id="email_for_server" style="width: 90px;" disabled="true">
                                                            <option value="0" {{isset($dataArray['email_for_server']['constant_int_value']) && $dataArray['email_for_server']['constant_int_value'] == 0 ? "selected" : ""}}>In Active</option>
                                                            <option value="1" {{isset($dataArray['email_for_server']['constant_int_value']) && $dataArray['email_for_server']['constant_int_value'] == 1 ? "selected" : ""}}>Active</option>
                                                        </select>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-outline-secondary" type="button" id="edit_email_for_server"><i class="mdi mdi-pencil"></i></button>
                                                            <button class="btn btn-outline-secondary" type="submit" id="save_email_for_server"><i class="mdi mdi-content-save"></i></button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <hr>
                                <h2 class="card-title">Setting for employee</h2>
                            <hr>
                                <div class="p-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Probation time(In months)
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/probationTimeUpdate')}}" name="timeentryDay_form" id="timeentryDay_form">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="probation_time" id="probation_time" value="{{isset($dataArray['probation_time']['constant_int_value']) && $dataArray['probation_time']['constant_int_value'] != "" ? $dataArray['probation_time']['constant_int_value'] : config('constant.probation_period')}}" class="form-control" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_probation_time"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_probation_time"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                    Gratuity
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/gratuityUpdate')}}" name="gratuity" id="gratuity">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="gratuity_text" id="gratuity_text" value="<?php if(isset($dataArray['gratuity'])){ ?>{{$dataArray['gratuity']['gratuity'] != null?$dataArray['gratuity']['gratuity']:''}}<?php } ?>" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_gratuity"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_gratuity"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                Professional Tax
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/professionalTaxUpdate')}}" name="professional_tax" id="professional_tax">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="professional_tax_text" id="professional_tax_text" value="<?php if(isset($dataArray['professional_tax'])){ ?>{{$dataArray['professional_tax']['professional_tax'] != null?$dataArray['professional_tax']['professional_tax']:''}}<?php } ?>" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_professional_tax"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_professional_tax"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                House Rent
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/houseRantUpdate')}}" name="house_rent" id="house_rent">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="house_rent_text" id="house_rent_text" value="<?php if(isset($dataArray['house_rent'])){ ?>{{$dataArray['house_rent']['house_rent'] != null?$dataArray['house_rent']['house_rent']:''}}<?php } ?>" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_house_rent"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_house_rent"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                Basic Salary
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/BasicSalaryUpdate')}}" name="basic_salary" id="basic_salary">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="basic_salary_text" id="basic_salary_text" value="<?php if(isset($dataArray['basic_salary'])){ ?>{{$dataArray['basic_salary']['basic_salary'] != null?$dataArray['basic_salary']['basic_salary']:''}}<?php } ?>" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_basic_salary"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_basic_salary"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                Performance Allowance
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/performance_allowanceUpdate')}}" name="performance_allowance" id="performance_allowance">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="performance_allowance_text" id="performance_allowance_text" value="<?php if(isset($dataArray['performance_allowance'])){ ?>{{$dataArray['performance_allowance']['performance_allowance'] != null?$dataArray['performance_allowance']['performance_allowance']:''}}<?php } ?>" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_performance_allowance"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_performance_allowance"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                Other Allownces
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/other_allowncesUpdate')}}" name="other_allownces" id="other_allownces">
                                                @csrf
                                                <div class="input-group mb-3">
                                                    <input type="text"  name="other_allownces_text" id="other_allownces_text" value="<?php if(isset($dataArray['other_allownces'])){ ?>{{$dataArray['other_allownces']['other_allownces'] != null?$dataArray['other_allownces']['other_allownces']:''}}<?php } ?>" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_other_allownces"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_other_allownces"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="">
                                                Bonas Applicable Percentage
                                                </label>
                                                <form class="form-horizontal" method="post" action="{{url('/setting/bonus_applicable_percentage')}}" name="bonus_applicable_percentage" id="bonus_applicable_percentage">
                                                @csrf
                                                <div class="input-group mb-3">

                                                    <input type="text"  name="bonus_applicable_percentage_text" id="bonus_applicable_percentage_text" value="<?php if(isset($dataArray['bonus_applicable_percentage'])){ ?>{{$dataArray['bonus_applicable_percentage']['constant_float_value'] != null?$dataArray['bonus_applicable_percentage']['constant_float_value']:''}}<?php } ?>" class="form-control" readonly="">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="button" id="edit_bonus_applicable_percentage"><i class="mdi mdi-pencil"></i></button>
                                                        <button class="btn btn-outline-secondary" type="submit"  id="save_bonus_applicable_percentage"><i class="mdi mdi-content-save"></i></button>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                   </div>
            </div>
        </div>
    </div>
 </section>
@endsection


@section('javascript')
<script type="text/javascript">
    $(function () {
        $("#timeentry_day").prop("readonly", true);
        $("#email_for_server").prop("disabled", true);
        $("#lateComeTime").prop("readonly", true);
        $("#daily_working_time").prop("readonly", true);
        $("#probation_time").prop("readonly", true);
        $("#daily_logged_time").prop("readonly", true);
        $("#task_operation_day").prop("readonly", true);
        $("#cr_operation_month").prop("readonly", true);
        $("#save_task_operation_day").css("display",'none');
        $("#save_cr_operation_month").css("display",'none');
        $("#save_timeentry_day").css("display",'none');
        $("#save_email_for_server").css("display",'none');
        $("#save_lateComeTime").css("display",'none');
        $("#save_working_time").css("display",'none');
        $("#save_probation_time").css("display",'none');
        $("#save_logged_time").css("display",'none');
        $("#save_gratuity").css("display",'none');
        $("#save_professional_tax").css("display",'none');
        $("#save_house_rent").css("display",'none');
        $("#save_basic_salary").css("display",'none');
        $("#save_other_allownces").css("display",'none');
        $("#save_bonus_applicable_percentage").css("display",'none');
        $("#save_first_half").css("display",'none');
        $("#save_second_half").css("display",'none');
        $("#save_full_day").css("display",'none');
        $("#save_performance_allowance").css("display",'none');



        $('#edit_email_for_server').click(function(){
            $("#email_for_server").prop("disabled", false);
            $("#save_email_for_server").css("display", 'block');
        });
        $('#edit_timeentry_day').click(function(){
            $("#timeentry_day").prop("readonly", false);
            $("#save_timeentry_day").css("display", 'block');
        });
        $('#edit_task_operation_day').click(function(){
            $("#task_operation_day").prop("readonly", false);
            $("#save_task_operation_day").css("display", 'block');
        });

        $('#edit_cr_operation_month').click(function(){
            $("#cr_operation_month").prop("readonly", false);
            $("#save_cr_operation_month").css("display", 'block');
        });

        $('#edit_lateComeTime').click(function(){
            $("#lateComeTime").prop("readonly", false);
            $("#save_lateComeTime").css("display", 'block');
        });

        $('#edit_working_time').click(function(){
            $("#daily_working_time").prop("readonly", false);
            $("#save_working_time").css("display", 'block');
        });

        $('#edit_probation_time').click(function(){
            $("#probation_time").prop("readonly", false);
            $("#save_probation_time").css("display", 'block');
        });

        $('#edit_logged_time').click(function(){
            $("#logged_hours_time").prop("disabled", false);
            $("#logged_minute_time").prop("disabled", false);
            $("#save_logged_time").css("display", 'block');
        });
         $('#edit_gratuity').click(function(){
            $("#gratuity_text").prop("readonly", false);
            $("#save_gratuity").css("display", 'block');
        });
         $('#edit_professional_tax').click(function(){
            $("#professional_tax_text").prop("readonly", false);
            $("#save_professional_tax").css("display", 'block');
        });

         $('#edit_house_rent').click(function(){
            $("#house_rent_text").prop("readonly", false);
            $("#save_house_rent").css("display", 'block');
        });
            $('#edit_basic_salary').click(function(){
            $("#basic_salary_text").prop("readonly", false);
            $("#save_basic_salary").css("display", 'block');
        });
           $('#edit_performance_allowance').click(function(){
            $("#performance_allowance_text").prop("readonly", false);
            $("#save_performance_allowance").css("display", 'block');
        });
           $('#edit_other_allownces').click(function(){
            $("#other_allownces_text").prop("readonly", false);
            $("#save_other_allownces").css("display", 'block');
        });
           $('#edit_bonus_applicable_percentage').click(function(){
            $("#bonus_applicable_percentage_text").prop("readonly", false);
            $("#save_bonus_applicable_percentage").css("display", 'block');
        });
           $('#edit_first_half').click(function(){
           $("#first_half_hours_time").prop("disabled", false);
            $("#first_half_minute_time").prop("disabled", false);
            $("#save_first_half").css("display", 'block');
        });
           $('#edit_second_half').click(function(){
           $("#second_half_hours_time").prop("disabled", false);
            $("#second_half_minute_time").prop("disabled", false);

            $("#save_second_half").css("display", 'block');
        });
           $('#edit_full_day').click(function(){
           $("#full_day_hours_time").prop("disabled", false);
            $("#full_day_minute_time").prop("disabled", false);

            $("#save_full_day").css("display", 'block');
        });
    });
</script>
@endsection
