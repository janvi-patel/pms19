@extends('layouts.master')
@section('content')
@include('sweet::alert')
@section('moduleName')
    View Profile
@endsection
<section class="content-header">
    <div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 no-block align-items-center">
                    <div class="text-left">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Profile Details</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>    
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
<section class="content">    
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row view_profile">
            <div class="col-md-12">
                @if (Session::has('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!!session('success')!!}</strong>
                </div>
                @endif    
                @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!!session('error')!!}</strong>
                </div>
                @endif 
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Profile Details</h3>
                    </div>
                    <div class="card-body">
                         <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                      <tr>
                                        <th>Name</th>
                                        <td>{{ $user->first_name.' '.$user->last_name }}</td>
                                      </tr>
                                      <tr>
                                        <th>Designation</th>
                                        <td>{{ $designation_name }}</td>
                                      </tr>
                                      <tr>
                                        <th>User Code</th>
                                        <td>{{(new \App\Helpers\CommonHelper)->takeEmpShortName($user->company_id)}}{{ $user->employee_id }}</td>
                                      </tr>
                                      <tr>
                                        <th>Company Email</th>
                                        <td>{{ $user->email }}</td>
                                      </tr>
                                      <form method="post" action="{{url('/viewprofile')}}" name="edit_personal_details" id="edit_personal_details">
                                      @csrf
                                      <tr>
                                        <th>Personal Email</th>
                                        <td class=""><div>{{ $user->personal_email }}</div> 
                                            <input type="text" name="personal_email" id="personal_email" value="{{ $user->personal_email }}" class="form-control" style="display:none" required/>

                                                <i class="fa fa-edit edit_form"></i>
                                           
                                            @if ($errors->has('personal_email'))
                                                <div class="error">{{ $errors->first('personal_email') }}</div>
                                            @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Phone Number</th>
                                        <td class=""><div>{{ $user->phone }}</div>
                                            <input type="text" name="phone" id="phone" value="{{ $user->phone }}" class="form-control" style="display:none" required />
                                            
                                                <i class="fa fa-edit edit_form"></i>
                                           
                                            @if ($errors->has('phone'))
                                                <div class="error">{{ $errors->first('phone') }}</div>
                                            @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Date Of Birth</th>
                                        <td class=""><div>{{ $user->birthdate }}</div>
                                            
                                                <input type="text" name="dob" id="dob" value="{{ $user->birthdate }}" class="form-control"  style="display:none" required />
                                                <i class="fa fa-edit edit_form"></i>
                                                @if ($errors->has('dob'))
                                                        <div class="error">{{ $errors->first('dob') }}</div>
                                                @endif

                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Shift Time</th>
                                        <td>{{ $user->from_shift.' - '.$user->to_shift}}</td>
                                      </tr>
                                    </tbody>
                                </table>
                                <button name="submit" id ="submitForm" type="submit" class="d-none">
                                    Save
                                </button>
                                </div>  
                            </form>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                            <th>Department</th>
                                            @foreach(config('constant.department') as $key => $value)
                                                @if ($key == $user->department)
                                                    <td>{{ $value }}</td>
                                                @endif
                                            @endforeach
                                          </tr>                                            
                                          <tr>
                                            <th>Bank Name</th>
                                            <td>{{ $user->bank_name }}</td>
                                          </tr>
                                          <tr>
                                            <th>Bank Account No</th>
                                            <td>{{ $user->bank_account_no }}</td>
                                          </tr>
                                          <tr>
                                            <th>PAN No</th>
                                            <td>{{ $user->pan_no }}</td>
                                          </tr>
                                          <tr>
                                            <th>Employee Status</th>
                                            @foreach(config('constant.employee_status') as $key => $value)
                                                @if ($key == $user->employee_status)
                                                    <td>{{ $value }}</td>
                                                @endif
                                            @endforeach
                                          </tr>
                                          
                                          <tr>
                                            <th>Reporting Person</th>
                                            <td>{{ $reportingPerson }}</td>
                                          </tr>
                                          
                                          <tr>
                                            <th>Joining Date</th>
                                            <td>
                                                @if ($user->joining_date == '' || $user->joining_date == '0000-00-00 00:00:00' || $user->joining_date == '1970-01-01 00:00:00')
                                                    {{ '-' }}
                                                @else
                                                    {{ (new \App\Helpers\CommonHelper)->displayDate($user->joining_date) }}  
                                                @endif
                                            </td>
                                          </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button name="submit" id="bottomButton" onclick="submitForm()"class="btn btn-primary btn-dark" disabled="disabled">
                            Save
                        </button>
                        <a href="{{url('dashboard')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                    </div>                    
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</section>
@endsection

@section('javascript')
   <script src="{{asset('js/viewProfile.js?'.time())}}"></script>
@endsection