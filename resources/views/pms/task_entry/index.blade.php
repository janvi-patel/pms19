@extends('layouts.master')

@section('moduleName')
   Task Entries
@endsection

@php
    $pageRangArray = config('constant.page_range');
@endphp

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Task Entries</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Task Entry</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/view/task_entry/'.$taskId)}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">From Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_log_from_date" id="search_by_log_from_date" value="{{(isset($request->search_by_log_from_date)?$request->search_by_log_from_date:'')}}" class="form-control" readonly />
                                    </div>
                                </div>
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">To Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_log_to_date" id="search_by_log_to_date" value="{{(isset($request->search_by_log_to_date)?$request->search_by_log_to_date:'')}}" class="form-control" readonly />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <a href="javascript:void(0)" id="reset_log_from_date">Reset</a>
                                </div>
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <a href="javascript:void(0)" id="reset_log_to_date">Reset</a>
                                </div>
                            </div>
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/view/task_entry/'.$taskId)}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-1 mb-3"> 
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 offset-md-8 mb-4"> 
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select" name="taskEntryDropdown" id="taskEntryDropdown">
                                        <option value="">Select Action</option>
                                        <option value="1">Delete</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive-md">
                                    <table id="taskEntryTable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" id="checkAll" class="custom-control-input">
                                                        <label class="custom-control-label" for="checkAll"></label>
                                                    </div>
                                                </th>
                                                <th>@sortablelink('log_date','Date')</th>
                                                <th>@sortablelink('log_hours','Hours')</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($taskEntry) > 0)
                                                @foreach($taskEntry as $index => $value)
                                                    <tr id="{{$value->id}}">
                                                        <td>
                                                            @if($value->emp_code == Auth::user()->employee_id)
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="chboxTaskEntry[]" id="chboxTaskEntry_{{$value->id}}" value="{{$value->id}}" class="custom-control-input chboxTaskEntry">
                                                                    <label class="custom-control-label" for="chboxTaskEntry_{{$value->id}}"></label>
                                                            </div>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{date('d-m-Y',strtotime($value->log_date))}}
                                                        </td>
                                                        <td>
                                                            {{sprintf('%0.2f', $value->log_hours)}}
                                                        </td>
                                                        <td>
                                                            {{$value->log_description}}
                                                        </td>
                                                        <td>
                                                            @if($value->emp_code == Auth::user()->employee_id)
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update" data-task-entry-id="{{$value->id}}" class="updateTaskEntry">
                                                                    <i class="mdi mdi-pencil"></i>
                                                            </a>

                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteTaskEntry" data-task-entry-id="{{$value->id}}">
                                                                <i class="mdi mdi-close"></i>
                                                            </a> 
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="5"><center>No Task Entry Found</center></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if($taskEntry && !empty($taskEntry))
                                    <div class="pt-3">
                                        {!! $taskEntry->appends(\Request::except('page'))->render() !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="editTaskEntryDiv">
    @include('pms.task.modal.edit_task_time_entry')
</div>
@endsection

@section('javascript')
<script src="{{asset('js/task_entry.js?'.time())}}"></script>
@endsection