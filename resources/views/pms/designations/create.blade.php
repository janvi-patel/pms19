@extends('layouts.master')
@section('content')
@section('moduleName')
    Designation
@endsection
@include('sweet::alert')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
 <section class="content-header">
  <div class="container-fluid">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12  no-block align-items-center">
                <div class="text-left">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{url('/designation')}}">Designation Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Create Designation</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
  </div><!-- /.container-fluid --> 
 </section>      
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
<section class="content">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                   <div class="card-header bg-dark">
                      <h3 class="card-title">Create Designation</h3>
                   </div>                    
                    <form class="form-horizontal" method="post" action="{{url('designation/store')}}" name="designation_create" id="designation_create">
                    @csrf
                        <div class="card-body">
                            <div class="form-group col-sm-6">
                                <label for="designation_name" >
                                    Designation Name
                                    <span class="error">*</span>
                                </label>
                                <input type="text" name="designation_name" id="designation_name" value="{{ old('designation_name') }}" class="form-control"  />
                                @if ($errors->has('designation_name'))
                                    <div class="error">{{ $errors->first('designation_name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark">
                            <a href="{{url('designation')}}"><button type="button" value="Cancel" class="btn btn-secondary  btn-dark">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    </div>
</section>
@endsection
@section('javascript')
   <script src="{{asset('js/designation.js')}}"></script>
@endsection