@extends('layouts.master')
@section('content')
@section('moduleName')
    Designation
@endsection
@include('sweet::alert')
<section class="content-header">
    <div class="content-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 no-block align-items-center">
                    <div class="text-left">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{url('designation')}}">Designation Management</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit Designation</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
   </div>
</section>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
<section class="content">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Designation</h3>
                    </div>                      
                    <form class="form-horizontal" method="post" action="{{url('designation/update/'.$designationDetails['id'])}}" name="designation_update" id="designation_update">
                    @csrf
                        <div class="card-body">
                            <div class="form-group col-sm-6">
                                <label for="designation_name" class="">
                                    Designation Name
                                    <span class="error">*</span>
                                </label>
                                    <input type="text" name="designation_name" id="designation_name" value="{{ old('designation_name') != '' ? old('designation_name') : $designationDetails['designation_name']}}" class="form-control"  />
                                    @if ($errors->has('designation_name'))
                                        <div class="error">{{ $errors->first('designation_name') }}</div>
                                    @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark">
                            <a href="{{url('/designation')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</section>
@endsection
@section('javascript')
   <script src="{{asset('js/designation.js')}}"></script>
@endsection