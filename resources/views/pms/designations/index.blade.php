@extends('layouts.master')
@section('content')

@section('moduleName')
    Designation
@endsection

@php
$pageRangArray = config('constant.page_range');
$statusArray = config('constant.status');
@endphp
@include('sweet::alert')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Designation Management</h1> -->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Designation Management</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>     
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
 <section class="content">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">    
                    @permission('designation.create')              
                    <div class="card-body">
                      <div class="row top-top-row">
                        <div class="col-sm-6 col-6 mb-3 pl-0 ">
                            <div class="form-group col">
                                <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown">
                                    @if(is_array($pageRangArray))
                                       @foreach ($pageRangArray as $key => $node)
                                            <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                        @endforeach 
                                    @endif
                                </select>
                            </div>                           
                        </div>
                        <div class="col-sm-6 col-6 mb-4 text-right"> 
                          <a href="{{url('designation/create')}}" title="Create Designation" class="float-right">
                            <button type="button" class="btn btn-primary btn-dark btn-block padding-5">Create Designation</button>
                          </a>
                        </div>
                      </div>
                    @endpermission
                    <div class="row">
                      <div class="col-md-12">
                        @if (count($designationData)>0)  
                        
                            <table id="designation_listing" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                      <th style="width: 10px" >No</th>
                                      <th>@sortablelink('designation_name','Designation Name')</th>
                                      <!-- <th>Designation Name</th> -->
                                      @permission('designation.edit|designation.delete')
                                      <th style="    width: 159px;text-align: center;">Action</th>
                                      @endpermission
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                $i = 1;
                                $j = $designationData->firstItem();
                                @endphp 
                                @foreach($designationData as $designation)
                                  <tr id ="{{$designation['id']}}">
                                    <td >{{$j}}</td>
                                    <td>{{$designation['designation_name']}}</td>
                                    @permission('designation.edit|designation.delete')
                                    <td align="center">
                                      @permission('designation.edit')
                                      <a href="{{url('designation/edit/'.$designation['id'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update">
                                          <i class="mdi mdi-pencil"></i>
                                      </a>
                                      @endpermission
                                      @permission('designation.delete')
                                      <a  data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" data-user-id ="{{$designation['id']}}" class='deleteDesigantion'">
                                          <i class="mdi mdi-close"></i>
                                      </a>
                                      @endpermission
                                    </td>
                                    @endpermission
                                  </tr>
                                  @php
                                  $i++;$j++;
                                  @endphp 
                                 @endforeach 
                                </tbody>
                            </table>
                        
                        @else
                        <div class="border-top">
                            <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                        </div> 
                        @endif
                      </div>
                    </div>
                    @if($designationData && !empty($designationData))
                    <div class="pt-4 ">{!! $designationData->appends(\Request::except('page'))->render() !!}</div>
                    @endif                      
                  </div>
                </div>
            </div>
        </div>
    </div>
 </section>     
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
<!-- this page js -->
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
@endsection


@section('javascript')
   <script src="{{asset('js/designation.js')}}"></script>
@endsection