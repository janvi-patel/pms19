@extends('layouts.master')

@section('moduleName')
    View CR
@endsection

@php
    $pageRangArray = config('constant.page_range');
@endphp

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View All CR</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter CR</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/view/crs/'.$request->company)}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Cr Title</label>
                                 
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_cr_title" id="search_by_cr_title">
                                        <option value="">Select</option>
                                        @if(!empty($crTitle))      
                                            @foreach($crTitle as $key => $val)
                                                <option value="{{$val}}" {{(isset($request->search_by_cr_title) && ($request->search_by_cr_title == $val))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select> 
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Project Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_name" id="search_by_project_name">
                                        <option value="">Select</option>
                                        @if(!empty($projectName))      
                                            @foreach($projectName as $key => $val)
                                                <option value="{{$val}}" {{(isset($request->search_by_project_name) && ($request->search_by_project_name == $val))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select> 
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Client Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_client" id="search_by_client">
                                        <option value="">Select</option>
                                        @if(!empty($clientName))      
                                            @foreach($clientName as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_client) && ($request->search_by_client == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Account Manager</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_account" id="search_by_account">
                                        <option value="">Select</option>
                                        @if(!empty($accountManager))      
                                            @foreach($accountManager as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_account) && ($request->search_by_account == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Project Manager</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_manager" id="search_by_project_manager">
                                        <option value="">Select</option>
                                        @if(!empty($projectManager))      
                                            @foreach($projectManager as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_project_manager) && ($request->search_by_project_manager == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Company Name</label>
                                    <select name="search_by_company" id="search_by_company" class="form-control select2" style="width: 100%; height:36px;">
                                        <option value="">All Companies</option>          
                                        @if(count($companyName)>0)              
                                        @foreach($companyName as $key => $val)
                                        <option value="{{$key}}" {{(isset($request->search_by_company) && $request->search_by_company == $key)?'selected':'' }}>
                                            {{$val}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>                          
                            
                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Assigned Company</label>
                                    <select name="search_by_assign_company" id="search_by_assign_company" class="form-control select2" style="width: 100%; height:36px;">
                                        <option value="">All Companies</option>          
                                        @if(count($assignedCompany)>0)              
                                        @foreach($assignedCompany as $key => $val)
                                        <option value="{{$key}}" {{(isset($request->search_by_assign_company) && $request->search_by_assign_company == $key)?'selected':'' }}>
                                            {{$val}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group {{(Auth::user()->hasRole(config('constant.superadmin_slug')))?'col-lg col-md-6 col-sm-6':'col-md col-md-4 col-sm-4'}}">
                                    <label class="">Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_start_date" id="search_by_start_date" value="{{(isset($request->search_by_start_date)?$request->search_by_start_date:'')}}" class="form-control" autocomplete="off" readonly />
                                    </div>
                                    <a href="javascript:void(0)" id="reset_startdate">Reset</a>
                                </div>
                                <div class="form-group {{(Auth::user()->hasRole(config('constant.superadmin_slug')))?'col-lg col-md-6 col-sm-6':'col-md col-md-4 col-sm-4'}}">
                                    <label class="">End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_end_date" id="search_by_end_date" value="{{(isset($request->search_by_end_date)?$request->search_by_end_date:'')}}"  class="form-control" autocomplete="off" readonly />
                                    </div>
                                    <a href="javascript:void(0)" id="reset_enddate">Reset</a>
                                </div>
                            </div>
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                <div class="row">
                                    <div class="form-group col-lg col-md-6 col-sm-6">
                                        <label class="">Company</label>
                                        <select name="company" id="company" class="form-control select2" style="width: 100%; height:36px;">
                                            <option value="0">All Companies</option>            
                                            @if(count($companies)>0)              
                                                @foreach($companies as $company)
                                                <option value="{{$company->id}}" {{(isset($request->company) && $request->company == $company->id)?'selected':'' }}>
                                                        {{$company->company_name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/view/crs/0')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex align-items-center justify-content-between"> 
                                
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>

                                    <div class="show_total_no float-right pl-3">
                                    Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of total {{$data->total()}} entries
                                </div>                                 
                            </div>                            
                        </div>

                        <div id="cr_listing">
                            @include('pms.cr.cr_listing_table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascript')
<script src="{{asset('js/moment-with-locales.js')}}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('js/select2.min.js')}}"></script>
<script src="{{asset('js/cr.js')}}"></script>
@endsection