<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="holiday_listing" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th><center>No</center></th>
                        <th>@sortablelink('title','Cr Title')</th>
                        <th>@sortablelink('project_name','Project Name')</th>
                        <th>@sortablelink('client_name','Client Name')</th>
                        <th>@sortablelink('account_manager_name','Account Manager')</th>
                        <th>@sortablelink('project_manager_name','Project Manager')</th>
                        <th>@sortablelink('company_name','Company name')</th>
                        <th>@sortablelink('assigned_company','Assign to (Company)')</th>
                        @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug')))
                        <th>@sortablelink('estimated_hours','Estimated Hours')</th>
                        @endif
                        <th>@sortablelink('pms_hours','Pms Hours')</th>
                        <th>@sortablelink('cr_start_date','Start Date')</th>
                        <th>@sortablelink('cr_end_date','End Date')</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($data)>0)  
                    @php
                    $i = 1;
                    $j = $data->firstItem();
                    @endphp 
                    @foreach($data as $dataVal)
                    <tr>
                        <td><center>{{$j}}</center></td>
                        <td>{{(isset($dataVal->title) && $dataVal->title!='')?$dataVal->title:'N/A'}}</td>
                        <td>{{(isset($dataVal->project_name) && $dataVal->project_name!='')?$dataVal->project_name:'N/A'}}</td>
                        <td>{{(isset($dataVal->client_name) && $dataVal->client_name!='')?$dataVal->client_name:'N/A'}}</td>
                        <td>{{(isset($dataVal->account_manager_name) && $dataVal->account_manager_name!='')?$dataVal->account_manager_name:'N/A'}}</td>
                        <td>{{(isset($dataVal->project_manager_name) && $dataVal->project_manager_name!='')?$dataVal->project_manager_name:'N/A'}}</td>
                        <td>{{(isset($dataVal->company_name) && $dataVal->company_name!='')?$dataVal->company_name:'N/A'}}</td>
                        <td>{{(isset($dataVal->assigned_company) && $dataVal->assigned_company!='')?$dataVal->assigned_company:'N/A'}}</td>
                        @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug')))
                        <td>{{(isset($dataVal->estimated_hours) && $dataVal->estimated_hours !='')?(new \App\Helpers\CommonHelper)->displayTaskTime($dataVal->estimated_hours):'N/A'}}</td>
                        @endif
                        <td>{{(isset($dataVal->pms_hours) && $dataVal->pms_hours !='')?(new \App\Helpers\CommonHelper)->displayTaskTime($dataVal->pms_hours):'N/A'}}</td>
                        <td>{{(isset($dataVal->cr_start_date ) && $dataVal->cr_start_date !='')?date('d-m-Y',strtotime($dataVal->cr_start_date)) :'N/A'}}</td>
                        <td>{{(isset($dataVal->cr_end_date) && $dataVal->cr_end_date!='')?date('d-m-Y',strtotime($dataVal->cr_end_date)):'N/A'}}</td>
                    </tr>
                    @php
                    $i++;$j++;
                    @endphp 
                    @endforeach 
                    @else
                        <tr>
                            <td colspan='12'><center>No CR Found</center></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

        @if($data && !empty($data))
            <div class="pt-3">{!! $data->appends(\Request::except('page'))->render() !!}</div>
        @endif
    </div>
</div>