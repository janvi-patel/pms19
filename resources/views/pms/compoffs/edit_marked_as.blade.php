@extends('layouts.master')
@section('content')

@section('moduleName')
    Marked As Comp-off
@endsection

@php
$encashed = 0;
if(null !==  Session::get('data')){
    $encashed = Session::get('data');
}
if($compoffDetails['compoff_marked_as'] != 1){
    $monthDivStyle='style=display:none';
}else{
    $monthDivStyle='';
}
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/teamcompoffs')}}">Employee Comp-off Requests</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Marked As Comp-off Request</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Marked As Comp-off Request</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('/compoffs/updateMarkedAs/'.$compoffDetails['id'])}}" name="compoff_update" id="compoff_marked_as_update">
                    @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="name" >Name</label>
                                    <div class="input-group">
                                         <div class="input-group-prepend">
                                             <span class="input-group-text"><i class="fa fa-user"></i></span>
                                         </div>
                                         <input type="text" name="name" id="name" value="{{$compoffDetails['user']['first_name'].' '.$compoffDetails['user']['last_name']}}" class="form-control" disabled="">
                                     </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Comp-off Marked As</label>
                                    <select name="compoff_marked_as" id="compoff_marked_as" class="select2 form-control custom-select" style="width: 100%; height:36px;">
                                        <option value="">Select Compoff Marked As</option>
                                        @foreach(config('constant.compoff_marked_as') as $key => $value)
                                        <option value="{{$key}}" {{ old('compoff_marked_as') != '' ? (old('compoff_marked_as') == $key ? 'selected' : '') : ($compoffDetails['compoff_marked_as'] == $key ? 'selected' : '')}}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('compoff_marked_as'))
                                        <div class="error">{{ $errors->first('compoff_marked_as') }}</div>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label >Comp-off Start Date</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="compoff_start_date" id="compoff_start_date" value="{{ old('compoff_start_date') != '' ? old('compoff_start_date') : $compoffDetails['compoff_start_date']}}" class="form-control" readonly="" />
                                                    @if ($errors->has('compoff_start_date'))
                                                        <div class="error">{{ $errors->first('compoff_start_date') }}</div>
                                                    @endif
                                                    </div>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="compoff_start_type" id="compoff_start_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" disabled="" >
                                                    <option value="0" {{ old('compoff_start_type') != '' ? (old('compoff_start_type') == 0 ? 'selected' : '') : ($compoffDetails['compoff_start_type'] == 0 ? 'selected' : '')}}>Full Day</option>
                                                    <option value="1" {{ old('compoff_start_type') != '' ? (old('compoff_start_type') == 1 ? 'selected' : '') : ($compoffDetails['compoff_start_type'] == 1 ? 'selected' : '')}}>First Half</option>
                                                    <option value="2" {{ old('compoff_start_type') != '' ? (old('compoff_start_type') == 2 ? 'selected' : '') : ($compoffDetails['compoff_start_type'] == 2 ? 'selected' : '')}}>Second Half</option>
                                                </select>
                                                @if ($errors->has('compoff_start_type'))
                                                    <div class="error">{{ $errors->first('compoff_start_type') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                    <label >Comp-off End Date</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" name="compoff_end_date" id="compoff_end_date" value="{{ old('compoff_end_date') != '' ? old('compoff_end_date') : $compoffDetails['compoff_end_date']}}" class="form-control" readonly="" />
                                                @if ($errors->has('compoff_end_date'))
                                                    <div class="error">{{ $errors->first('compoff_end_date') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <select name="compoff_end_type" id="compoff_end_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" disabled="" >
                                                <option value="0" {{ old('compoff_end_type') != '' ? (old('compoff_end_type') == 0 ? 'selected' : '') : ($compoffDetails['compoff_end_type'] == 0 ? 'selected' : '')}}>Full Day</option>
                                                <option value="1" {{ old('compoff_end_type') != '' ? (old('compoff_end_type') == 1 ? 'selected' : '') : ($compoffDetails['compoff_end_type'] == 1 ? 'selected' : '')}}>First Half</option>
                                                <option value="2" {{ old('compoff_end_type') != '' ? (old('compoff_end_type') == 2 ? 'selected' : '') : ($compoffDetails['compoff_end_type'] == 2 ? 'selected' : '')}}>Second Half</option>
                                            </select>
                                            @if ($errors->has('compoff_end_type'))
                                                <div class="error">{{ $errors->first('compoff_end_type') }}</div>
                                            @endif
                                        </div>
                                    </div>  
                                </div>
                            </div>
                            </div>
                            <div class='row'>
                                <div class="col-md-2">
<!--                                    <div id='monthDiv' class="form-group" {{ $monthDivStyle }}>
                                        <label>Encash Month</label>
                                        <span class="error">*</span>
                                        <input type="text" name="compoff_encash_month_year" id="compoff_encash_month_year" value="{{ old('compoff_encash_month_year') != '' ? old('compoff_encash_month_year') : $compoffDetails['compoff_encash_month_year'] }}" class="form-control" />
                                        @if ($errors->has('compoff_encash_month_year'))
                                            <div class="error">{{ $errors->first('compoff_encash_month_year') }}</div>
                                        @endif
                                    </div>-->
                                </div>
                            </div>   
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark">
                            <a href="{{url('/teamcompoffs')}}"><button type="button" value="Cancel" class="btn btn-secondary">Cancel</button></a>
                        </div>
                        <input type="hidden" id="old_marked_as" name="old_marked_as" value="{{$compoffDetails['compoff_marked_as']}}">
                        <input type="hidden" id="user_id" name="user_id" value="{{$compoffDetails['user_id']}}">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
    <script type="text/javascript">
    $( document ).ready(function() {
        if({{ $encashed }} == 1 || ({{ $compoffDetails['compoff_marked_as']}} !== null && {{ $compoffDetails['compoff_marked_as'] }} == 1)){
            $('#monthDiv').show();
        }else{
            $('#monthDiv').hide();
        }
    });  
</script>
@endsection
@section('javascript')
<script src="{{asset('js/compoffs.js?'.time())}}"></script>
@endsection