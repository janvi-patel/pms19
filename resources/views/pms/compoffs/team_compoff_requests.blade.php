@extends('layouts.master')
@section('content')

@section('moduleName')
   Team Comp-off
@endsection
 
@php
$compOffStatusArray = config('constant.compoff_status');
$compoffMarkedAsArray = config('constant.compoff_marked_as');
$pageRangArray = config('constant.page_range');
$departmentArray = config('constant.department');
$internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet">
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="card-title">Team Comp-off Requests</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Team Comp-off Requests</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<?php 
 $search_class = "";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "show";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header bg-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                <h3 class="card-title">View 
                                @role('pm|tl') Team @endrole 
                                @role('hr|admin') Employee @endrole 
                                Comp-off Requests</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="collapseOne" class="panel-collapse in collapse <?php echo $search_class;?>" style="">
                <form class="form-horizontal" method="get" action="{{url('/teamcompoffs')}}" name="search_filter" id="search_filter">
                    <div class="card-body">
                        <div class="row">
                            <!-- add by mohit -->
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                                @if(isset($internalCompany) && count($internalCompany) > 0)
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Department</label>
                                        <select class="select2 form-control" style="width: 100%; height:36px;" name="search_by_department[]" id="search_by_department" multiple="multiple">
                                            <option value="">Select Department</option>  
                                                @if(isset($departmentArray) && !empty($departmentArray))
                                                    @foreach($departmentArray as $key => $val)
                                                        @if(!isset($request->search_by_department))
                                                            <option selected="selected" value="{{ $key }}">{{ $val }}</option>
                                                        @else
                                                            <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Internal Company</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                            <option value="">Select Internal Company</option> 
                                            @if(isset($internalCompany) && !empty($internalCompany))
                                                @foreach($internalCompany as $key => $val)
                                                    <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                                @endforeach
                                            @endif
                                            <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                        </select>
                                    </div>
                                @endif
                            @endif
                            <!-- end by mohit -->
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label>User</label>
                                <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_team_compoff_emp" id="search_team_compoff_emp">
                                    <option value="">All</option>
                                    @foreach($userNameArray as $key => $value)
                                    <option value="{{$key}}" {{ (isset($userSelected) && $userSelected == $key) ? 'selected' : ''}} {{ ($key == request()->input('search_team_compoff_emp'))? 'selected' : ''  }}>{{$value}}</option>
                                    @endforeach
                                </select>  
                            </div>
                            @if(Auth::user()->hasRole(config('constant.tl_slug')))
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label>Approver</label>
                                <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_team_compoff_approver" id="search_team_compoff_approver">
                                    <option value="">All</option>
                                    @foreach($allUserListArray as $key => $value)
                                    <option value="{{$key}}" {{ (isset($userSelected) && $userSelected == $key) ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label>Start Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="search_compoff_team_start_date" id="search_compoff_team_start_date" value="{{ $request->search_compoff_team_start_date}}" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label>End Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="search_compoff_team_end_date" id="search_compoff_team_end_date" value="{{$request->search_compoff_team_end_date}}" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label>Comp-off Status</label>
                                <select class="select2 form-control custom-select" name="ddlCompOffStatus" id="ddlCompOffStatus" style="width: 100%">
                                    <option value="">All</option>
                                    @foreach($compoffStatus as $cs)
                                    @if(Auth::user()->hasRole(config('constant.superadmin_slug'))|| Auth::user()->hasRole(config('constant.admin')))
                                        <option value="{{$cs['id']}}" {{(isset($request->ddlCompOffStatus) && ($request->ddlCompOffStatus == $cs['id']) || !isset($request->ddlCompOffStatus) && $cs['id'] == 2)?'selected':''}} >{{$cs['statusName']}}</option>
                                    @else
                                        <option value="{{$cs['id']}}" {{ ($cs['id'] == request()->input('ddlCompOffStatus',old('ddlCompOffStatus'))) ? 'selected' : ''  }}>{{$cs['statusName']}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug'))|| Auth::user()->hasRole(config('constant.admin')))
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <label>Marked-as Status</label>
                                <select class="select2 form-control custom-select" name="searchMarkedStatus" id="searchMarkedStatus" style="width: 100%">
                                    <option value="0">All</option>
                                    @foreach($compoffMarkedAsArray as $key => $val)
                                    <option value="{{$key}}" {{ ($key == request()->input('searchMarkedStatus'))? 'selected' : ''  }}>{{$val}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                        
                            <div class="form-group col-lg-3 col-md-4 col-sm-6">                                
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_emp_compoff_company" id="search_by_emp_compoff_company" >
                                    <option value="0">All Companies</option>
                                        @foreach($companyList as $companyDataVal)
                                            <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_emp_compoff_company) && ($request->search_by_emp_compoff_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                        @endforeach
                                </select>
                            @endif
                            </div>                           
                        </div>
                    </div>
                    {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                    <div class="card-footer">
                       <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/teamcompoffs')}}'">
                            Reset
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 d-flex justify-content-between">
                                <div class="left w-100"> 
                                    <div class="top-pagination">
                                        <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="width: 100%; height:36px;"  >
                                            @if(is_array($pageRangArray))
                                               @foreach ($pageRangArray as $key => $node)
                                                    <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                                @endforeach 
                                            @endif
                                        </select>
                                    </div>
                                    @if(count($compoffData)>0)
                                        <p class="mt-1">Showing {{ $compoffData->firstItem() }} to {{ $compoffData->lastItem() }} of total {{$compoffData->total()}} entries</p>
                                    @endif
                                </div>
                                <div class="right col-2">
                                    @if(!Auth::user()->hasRole('developer'))
                                    <div class="form-group col">
                                        <select class="select2 form-control custom-select update_checked_compoff" name="update_checked_compoff" id="update_checked_compoff" style="width: 100%; height:36px;"  >
                                           <option value="">Select Action</option>
                                            <option value="1">Approve</option>
                                            <option value="3">Reject</option>
                                            <option value="0">Delete</option>
                                            @if(!Auth::user()->hasRole(config('constant.project_manager_slug')) &&
                                                !Auth::user()->hasRole(config('constant.team_leader_slug'))&&
                                                 Auth::user()->designation_id != 16 )
                                            <option value="11">Encashed</option>
                                            <option value="22">Leave Added</option>
                                            @endif
                                            </select>
                                    </div> 
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                @if (count($compoffData)>0)  
                                <div class="table-responsive">
                                    <table id="team_compoff_listing" class="table table-striped table-bordered compoff_listing">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" id="checkAll" class="custom-control-input">
                                                        <label class="custom-control-label" for="checkAll"></label>
                                                    </div>
                                                </th>
                                                <th>@sortablelink('user.first_name','User')</th>
                                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                                    <th>@sortablelink('company_name','Company')</th>
                                                @endif  
                                                <th>@sortablelink('compoff_start_date','Start Date')</th>
                                                <th>@sortablelink('compoff_end_date','End Date')</th>
                                                <th>@sortablelink('compoff_days','Comp-off Days')</th>
                                                <th>@sortablelink('compoff_description','Comp-off Description')</th>
                                                <th>@sortablelink('user.first_name','Approver')</th>
                                                <th>@sortablelink('compoff_status','Comp-off Status')</th>
                                                <th>@sortablelink('compoff_marked_as','Encashed / Leave Added')</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $i = 1;
                                            $j = $compoffData->firstItem();
                                            @endphp 
                                            @foreach($compoffData as $compoff)
                                            <tr id="{{$compoff['id']}}">
                                                <td>{{$j}}</td>
                                                <td>
                                                    <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="update_compoff_row[]" id="update_compoff_row_{{ $compoff['id'] }}" value="{{$compoff['id'] }}" class="custom-control-input update_compoff_row">
                                                    <label class="custom-control-label" for="update_compoff_row_{{ $compoff['id'] }}"></label>
                                                    </div>
                                                </td>
                                                <td>{{$compoff['user']['first_name'].' '.$compoff['user']['last_name']}}</td>
                                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                                    <td>{{$compoff['company_name']}}</td>
                                                @endif
                                                <td data-sort='{{ $compoff['compoff_start_date'] }}'>{{(new \App\Helpers\CommonHelper)->displayDate($compoff['compoff_start_date'])}}</td>
                                                <td data-sort='{{ $compoff['compoff_end_date'] }}'>{{(new \App\Helpers\CommonHelper)->displayDate($compoff['compoff_end_date'])}}</td>

                                                <td>{{$compoff['compoff_days']}}</td>
                                                <td>{{$compoff['compoff_description']}}</td>
                                                <td>{{$compoff['approver']['first_name'].' '.$compoff['approver']['last_name']}}</td>
                                                <td>
                                                    {{$compOffStatusArray[$compoff['compoff_status']]}}
                                                </td>
                                                <td>
                                                    @if($compoff['compoff_marked_as'] != '')  
                                                    {{$compoffMarkedAsArray[$compoff['compoff_marked_as']]}}
                                                    @else
                                                    -
                                                    @endif
                                                </td>
                                                <td align="center">
                                                    @if ((Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug'))))
                                                    <a href="{{url('/compoffs/markedas/'.$compoff['id'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update Marked As Status">
                                                        <i class="mdi mdi-pencil-box"></i>
                                                    </a>
                                                    @endif
                                                    @if (($loggedInUserID == $compoff['approver_id'] || Auth::user()->hasRole(config('constant.project_manager_slug'))) && $compoff['compoff_marked_as'] == '')
                                                    <a href="{{url('/compoffs/approve/'.$compoff['id'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update Status">
                                                        <i class="mdi mdi-pencil"></i>
                                                    </a>
                                                    @endif                 
                                                    @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                        Auth::user()->hasRole(config('constant.hr_slug')))
                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deletecompoff" data-task-entry-id={{$compoff['id']}}>
                                                        <i class="mdi mdi-close"></i>
                                                    </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @php
                                            $i++;$j++;
                                            @endphp 
                                            @endforeach 
                                            @else
                                            <div class="border-top">
                                                <h4 align="center" style="padding : 20px;">No comp-off from your team.</h4>
                                            </div>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                 <div class="row w-100">
                                @if($compoffData && !empty($compoffData))
                                <div class="pt-4 col-md-6">{!! $compoffData->appends(\Request::except('page'))->render() !!}</div>
                                @endif
                                @if (count($compoffData)>0)
                                    <div class="show_total_no_bottom col-md-6"> 
                                           <div class="card-body float-right">
                                               Showing {{ $compoffData->firstItem() }} to {{ $compoffData->lastItem() }} of total {{$compoffData->total()}} entries
                                           </div> 
                                    </div>
                                @endif
                            </div>
                            </div>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script src="{{asset('js/compoffs.js?'.time())}}"></script>
<script src="{{asset('js/reports.js?'.time())}}"></script>
@endsection