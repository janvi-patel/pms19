@extends('layouts.master')
@section('content')
@section('moduleName')
    Comp-Off
@endsection
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/compoffrequests')}}">Comp-off Requests</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add Comp-off Request</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Add Comp-off Request</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('/compoffs/store')}}" name="compoff_create" id="compoff_create">
                    @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="name" >Name</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                                        </div>
                                    <input type="text" value="{{$userDetails['first_name'].' '.$userDetails['last_name']}}" class="form-control" disabled="">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label>Approver</label>
                                    @php $approver = (new \App\Helpers\CommonHelper)->getAnyUserById($reportingTo); @endphp
                                        <input type="text" value="{{$approver["first_name"]!=null?$approver["first_name"].' '.$approver["last_name"]:'N/A'}}" class="form-control" disabled="">
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label >Comp-off Start Date</label>
                                        <span class="error">*</span>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" name="compoff_start_date" id="compoff_start_date" value="{{ old('compoff_start_date') }}" class="form-control"  />
                                                @if ($errors->has('compoff_start_date'))
                                                    <div class="error">{{ $errors->first('compoff_start_date') }}</div>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="compoff_start_type" id="compoff_start_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                                    <option value="0">Full Day</option>
                                                    <option value="1">First Half</option>
                                                    <option value="2">Second Half</option>
                                                </select>
                                                @if ($errors->has('compoff_start_type'))
                                                    <div class="error">{{ $errors->first('compoff_start_type') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label >Comp-off End Date</label>
                                        <span class="error">*</span>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="compoff_end_date" id="compoff_end_date" value="{{ old('compoff_end_date') }}" class="form-control"  />
                                                    <input type="hidden" name="joinning_date" id="joinning_date" value= "{{ Auth::user()->joining_date }}" class="form-control"/>
                                                    @if ($errors->has('compoff_end_date'))
                                                        <div class="error">{{ $errors->first('compoff_end_date') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="compoff_end_type" id="compoff_end_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                                    <option value="0">Full Day</option>
                                                    <option value="1">First Half</option>
                                                    <option value="2">Second Half</option>
                                                </select>
                                                @if ($errors->has('compoff_end_type'))
                                                    <div class="error">{{ $errors->first('compoff_end_type') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            
                           
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="compoff_description" >Comp-off Description</label>
                                    <span class="error">*</span>
                                        <textarea class="form-control" name="compoff_description" id="compoff_description" >{{ old('compoff_description') }}</textarea>
                                        @if ($errors->has('compoff_description'))
                                            <div class="error">{{ $errors->first('compoff_description') }}</div>
                                        @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark">
                            <a href="{{url('/compoffrequests')}}"><button type="button" value="Cancel" class="btn btn-secondary">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script src="{{asset('js/compoffs.js?'.time())}}"></script>
@endsection