@extends('layouts.master')
@section('content')

@section('moduleName')
    Comp-off
@endsection
@php
$compOffStatusArray = config('constant.compoff_status');
$compoffMarkedAsArray = config('constant.compoff_marked_as');
$pageRangArray = config('constant.page_range');
@endphp
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet">
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Comp-off Requests</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<?php 
 $search_class = "";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "show";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header bg-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                <h3 class="card-title">Comp-off Requests</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="collapseOne" class="panel-collapse in collapse <?php echo $search_class;?>" style="">
                <form class="form-horizontal" method="get" action="{{url('/compoffrequests')}}" name="search_filter" id="search_filter">
                    <div class="card-body">
                        <div class="row">
                            @if(!Auth::user()->hasRole('developer'))
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <label class="">User</label>
                                  <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee">
                                    <option value="">All</option>
                                    @foreach($userNameList as $userNameListVal)
                                        <option value="{{$userNameListVal->id}}" {{(isset($request->search_by_employee) && ($request->search_by_employee == $userNameListVal->id))?'selected':''}}>{{$userNameListVal->first_name.' '.$userNameListVal->last_name}}</option>
                                    @endforeach
                                </select> 
                            </div>
                            @endif
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <label class="">Approver</label>
                                  <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_approver" id="search_by_approver">
                                    <option value="">All</option>
                                    @foreach($userNameList as $userNameListVal)
                                        <option value="{{$userNameListVal->id}}" {{(isset($request->search_by_approver) && ($request->search_by_approver == $userNameListVal->id))?'selected':''}}>{{$userNameListVal->first_name.' '.$userNameListVal->last_name}}</option>
                                    @endforeach
                                </select>   
                            </div>
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                 <label>Start Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="search_compoff_start_date" id="search_compoff_start_date" value="{{ $start_date }}" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <label>End Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="search_compoff_end_date" id="search_compoff_end_date" value="{{$end_date}}" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <label>Comp-off Status</label>
                                <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_compoff_status" id="search_by_compoff_status">
                                    <option value="">-- select status --</option>
                                    @foreach ($compOffStatusArray as $key => $node)
                                        <option value="{{$key}}" {{(isset($request->search_by_compoff_status) && ($request->search_by_compoff_status == $key))?'selected':''}}> {{ $node }}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                    <div class="card-footer">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/compoffrequests')}}'">
                            Reset
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body text-right">
                        <div class="row update_checked_data_row">
                            <div class="col-md-10 mb-3"> 
                                <div class="form-group col pl-0 top-pagination">
                                    <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="width: 100%; height:36px;"  >
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                                <p class="mt-1">Showing {{ $compOffData->firstItem() }} to {{ $compOffData->lastItem() }} of total {{$compOffData->total()}} entries</p>
                            </div>
                            <div class="col-md-2 mb-4 col-sm-3">
                            @if(!Auth::user()->hasRole(config('constant.superadmin_slug')) )
                                <a href="{{url('/compoffs/add')}}" title="Add Comp-off Request">
                                    <button type="button" class="btn btn-primary btn-dark btn-block padding-5">Add Comp-off Request </button>
                                </a>
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (count($compOffData)>0)  
                        <div class="table-responsive">
                            <table id="compoff_listing" class="table table-striped table-bordered  compoff_listing">
                                <thead>
                                    <tr>
                                        @if(!Auth::user()->hasRole('developer'))
                                        <th>
                                             <div class="custom-control custom-checkbox">
                                                <input type="checkbox" id="checkAll" class="custom-control-input">
                                                <label class="custom-control-label" for="checkAll"></label>
                                            </div>
                                        </th>
                                        @else
                                        <th>No</th>
                                        @endif
                                        <th>@sortablelink('user.first_name','User')</th>
                                        <th>@sortablelink('compoff_start_date','Start Date')</th>
                  			<th>@sortablelink('compoff_end_date','End Date')</th>
                                        <th>@sortablelink('compoff_days','Comp-off Days')</th>
                                        <th>@sortablelink('compoff_description','Comp-off Description')</th>
                                        <th>@sortablelink('approver.first_name','Approver')</th>
                                        <th>@sortablelink('compoff_status','Comp-off Status')</th>
                                        <th>@sortablelink('compoff_marked_as','Encashed / Leave Added')</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    $j = $compOffData->firstItem();
                                    @endphp 
                                    @foreach($compOffData as $compoff)
                                    <tr id="{{$compoff['id']}}">
                                        @if(!Auth::user()->hasRole('developer'))
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="update_compoff_row[]" id="update_compoff_row_{{ $compoff['id'] }}" value="{{$compoff['id'] }}" class="custom-control-input update_compoff_row">
                                            <label class="custom-control-label" for="update_compoff_row_{{ $compoff['id'] }}"></label>
                                            </div>
                                        </td>
                                        @else
                                         <td>
                                            {{$j}}
                                        </td>
                                        @endif
                                        <td>{{$compoff['user']['first_name'].' '.$compoff['user']['last_name']}}</td>
                                        <td data-sort='{{ $compoff['compoff_start_date'] }}'>{{(new \App\Helpers\CommonHelper)->displayDate($compoff['compoff_start_date'])}}</td>
                                        <td data-sort='{{ $compoff['compoff_end_date'] }}'>{{(new \App\Helpers\CommonHelper)->displayDate($compoff['compoff_end_date'])}}</td>

                                        <td>{{$compoff['compoff_days']}}</td>
                                        <td>{{$compoff['compoff_description']}}</td>
                                        <td>{{$compoff['approver']['first_name'].' '.$compoff['approver']['last_name']}}</td>
                                        <td>
                                            {{$compOffStatusArray[$compoff['compoff_status']]}}
                                        </td>
                                        <td>
                                            @if($compoff['compoff_marked_as'] != '')  
                                            {{$compoffMarkedAsArray[$compoff['compoff_marked_as']]}}
                                            @else
                                            -
                                            @endif
                                        </td>
                                        <td align="center">
                                        @if (Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                            Auth::user()->hasRole(config('constant.admin_slug')))
                                             <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deletecompoff" data-task-entry-id={{$compoff['id']}}>
                                                <i class="mdi mdi-close"></i>
                                            </a>
                                        @elseif (date('Y-m-d') <= date_format($compoff['created_at'], 'Y-m-d') && $compOffStatusArray[$compoff['compoff_status']] != 'Approved')
                                             <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deletecompoff" data-task-entry-id={{$compoff['id']}}>
                                                <i class="mdi mdi-close"></i>
                                            </a>
                                        @endif
                                    </tr>
                                    @php
                                    $i++;$j++;
                                    @endphp 
                                    @endforeach 
                                    @else
                                    <div class="border-top">
                                        <h4 align="center" style="padding : 20px;">You have not added any comp-off requests yet.</h4>
                                    </div>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        @if($compOffData && !empty($compOffData))
                        <div class="pt-4">{!! $compOffData->appends(\Request::except('page'))->render() !!}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascript')
<script src="{{asset('js/compoffs.js?'.time())}}"></script>
@endsection