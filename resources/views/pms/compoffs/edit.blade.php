@extends('layouts.master')
@section('content')
@section('moduleName')
    Approve Comp-off 
@endsection
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/teamcompoffs')}}">Team Comp-off Requests</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Approve Comp-off Request</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Approve Comp-off Request</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('/compoffs/update/'.$compoffDetails['id'])}}" name="compoff_update" id="compoff_update">
                    @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="name" >Name</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                                        </div>
                                        <input type="text" name="name" id="name" value="{{$compoffDetails['user']['first_name'].' '.$compoffDetails['user']['last_name']}}" class="form-control" disabled="">
                                    </div> 
                                </div>
                                <div class="col-md-2">
                                    <label>Approver</label>
                                    <select class="select2 form-control custom-select compoff_approver" name="approver_id" id="approver_id" style="width: 100%; height:36px;" disabled="" >
                                        <option value="">Select Approver</option>
                                        @foreach($reportingToList as $reportingTo)
                                        <option value="{{$reportingTo['id']}}" {{$compoffDetails['approver_id'] == 
                                        $reportingTo['id'] ? 'selected' : ''}}>{{$reportingTo['first_name'].' '.$reportingTo['last_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label >Comp-off Start Date</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="compoff_start_date" id="compoff_start_date" value="{{ old('compoff_start_date') != '' ? old('compoff_start_date') : $compoffDetails['compoff_start_date']}}" class="form-control" readonly="" disabled="" />
                                                    @if ($errors->has('compoff_start_date'))
                                                        <div class="error">{{ $errors->first('compoff_start_date') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="compoff_start_type" id="compoff_start_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" disabled="" >
                                                    <option value="0" {{ old('compoff_start_type') != '' ? (old('compoff_start_type') == 0 ? 'selected' : '') : ($compoffDetails['compoff_start_type'] == 0 ? 'selected' : '')}}>Full Day</option>
                                                    <option value="1" {{ old('compoff_start_type') != '' ? (old('compoff_start_type') == 1 ? 'selected' : '') : ($compoffDetails['compoff_start_type'] == 1 ? 'selected' : '')}}>First Half</option>
                                                    <option value="2" {{ old('compoff_start_type') != '' ? (old('compoff_start_type') == 2 ? 'selected' : '') : ($compoffDetails['compoff_start_type'] == 2 ? 'selected' : '')}}>Second Half</option>
                                                </select>
                                                @if ($errors->has('compoff_start_type'))
                                                    <div class="error">{{ $errors->first('compoff_start_type') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label >Comp-off End Date</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <input type="text" name="compoff_end_date" id="compoff_end_date" value="{{ old('compoff_end_date') != '' ? old('compoff_end_date') : $compoffDetails['compoff_end_date']}}" class="form-control" readonly=""  disabled="" />
                                                    @if ($errors->has('compoff_end_date'))
                                                        <div class="error">{{ $errors->first('compoff_end_date') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <select name="compoff_end_type" id="compoff_end_type" class="select2 form-control custom-select" style="width: 100%; height:36px;" disabled="" >
                                                    <option value="0" {{ old('compoff_end_type') != '' ? (old('compoff_end_type') == 0 ? 'selected' : '') : ($compoffDetails['compoff_end_type'] == 0 ? 'selected' : '')}}>Full Day</option>
                                                    <option value="1" {{ old('compoff_end_type') != '' ? (old('compoff_end_type') == 1 ? 'selected' : '') : ($compoffDetails['compoff_end_type'] == 1 ? 'selected' : '')}}>First Half</option>
                                                    <option value="2" {{ old('compoff_end_type') != '' ? (old('compoff_end_type') == 2 ? 'selected' : '') : ($compoffDetails['compoff_end_type'] == 2 ? 'selected' : '')}}>Second Half</option>
                                                </select>
                                                @if ($errors->has('compoff_end_type'))
                                                    <div class="error">{{ $errors->first('compoff_end_type') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="compoff_description" >Compoff Description</label>
                                    <textarea class="form-control" name="compoff_description" id="reason" readonly="">{{ old('compoff_description') != '' ? old('compoff_description') : $compoffDetails['compoff_description']}}</textarea>
                                    @if ($errors->has('reason'))
                                        <div class="error">{{ $errors->first('compoff_description') }}</div>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <label for="approver_comment" >Approver Comment</label>
                                    <span class="error">*</span>
                                    <textarea class="form-control" name="approver_comment" id="approver_comment" >{{ old('approver_comment') != '' ? old('approver_comment') : $compoffDetails['approver_comment']}}</textarea>
                                    @if ($errors->has('approver_comment'))
                                        <div class="error">{{ $errors->first('approver_comment') }}</div>
                                    @endif 
                                </div>
                                <div class="col-md-4">
                                    <label class="">Compoff Status</label>
                                    <select name="compoff_status" id="compoff_status" class="select2 form-control custom-select" style="width: 100%; height:36px;">
                                       
                                        @foreach(config('constant.compoff_status') as $key => $value)
                                        <option value="{{$key}}" {{ old('compoff_status') != '' ? (old('compoff_status') == $key ? 'selected' : '') : ($compoffDetails['compoff_status'] == $key ? 'selected' : '')}}>{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> 
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark">
                            <a href="{{url('/teamcompoffs')}}"><button type="button" value="Cancel" class="btn btn-secondary">Cancel</button></a>
                        </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script src="{{asset('js/compoffs.js?'.time())}}"></script>
@endsection