@extends('layouts.master')
@section('content')
@section('moduleName')
    Add Early Leave Request
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/earlyLeaveRequest')}}">Early Leave Requests</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add Early Leave Request</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Add Early Leave Request</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{!! route('earlyLeaveStore') !!}" name="task_create" id="task_leave_create" autocomplete="off">
                    @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2 form-group col">
                                    <label for="name">Name</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text"><i class="fa fa-user"></i></span>
                                        </div>  
                                         <input type="text" value="{{$userDetails['first_name'].' '.$userDetails['last_name']}}" class="form-control" disabled="">
                                    </div>
                                </div> 
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Approver</label>
                                        @php $approver = (new \App\Helpers\CommonHelper)->getAnyUserById($reportingTo); @endphp
                                        <input type="text" value="{{$approver["first_name"]!=null?$approver["first_name"].' '.$approver["last_name"]:'N/A'}}" class="form-control" disabled="">
                                    </div> 
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <span class="error">*</span>
                                           
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                              <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                            </div> 
                                                            <input type="text" name="date" id="cre_date" value="{{ old('date') }}" class="form-control" />
                                                            @if ($errors->has('date'))
                                                            <div class="error">{{ $errors->first('date') }}</div>
                                                        @endif
                                                        </div>
                                                       
                                                    </div>
                                         
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="reason">Reason</label>
                                        <span class="error">*</span>
                                        <textarea class="form-control" name="reason" id="reason" ></textarea>
                                        @if ($errors->has('reason'))
                                            <div class="error">{{ $errors->first('reason') }}</div>
                                        @endif
                                    </div>
                                </div>   
                            </div>
                            
                          
                          
                                                     
                           
                            <!-- <div class="row">
                                <div class="col-md-12">
                                        <div class="form-check">
                                            <input type="checkbox" id="adhoc_leave" name="adhoc_leave" class="form-check-input" {{ old('adhoc_leave') ? 'checked' : '' }} style="width:auto !important;"> 
                                            <label for="reason">Adhoc Leave</label>
                                        </div>
                                </div>

                            </div> -->
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark" >
                            <a href="{{url('/earlyLeaveRequest')}}"><button type="button" value="Cancel" class="btn btn-info btn-secondary">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script src="{{asset('js/early_leave.js?'.time())}}"></script>
@endsection