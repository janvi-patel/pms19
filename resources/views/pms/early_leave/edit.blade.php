@extends('layouts.master')
@section('content')
@section('moduleName')
    ApproveEarly Leave Request
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/teamEarlyLeave')}}">Team Early Leave Requests</a></li>
                    <li class="breadcrumb-item active" aria-current="page">ApproveEarly Leave Request</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
     <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header  bg-dark">
                        <h3 class="card-title">Approve Early Leave Request</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('/earlyLeave/update/'.$leaveDetails['id'])}}" name="leave_update" id="leave_update">
                    @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="name" >Name</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text"><i class="fa fa-user"></i></span>
                                        </div> 
                                    <input type="text" name="name" id="name" value="{{$leaveDetails['user']['first_name'].' '.$leaveDetails['user']['last_name']}}" class="form-control" disabled="">
                                    </div> 
                                </div>
                                <div class="col-md-2">
                                    <label>Approver</label>
                                    <select class="select2 form-control custom-select" name="approver_id" id="approver_id" style="width: 100%; height:36px;" disabled="" >
                                        <option value="">Select Approver</option>
                                        @foreach($reportingToList as $reportingTo)
                                        <option value="{{$reportingTo['id']}}" {{ $leaveDetails['approver_id'] == 
                                        $reportingTo['id'] ? 'selected' : ''}}>{{$reportingTo['first_name'].' '.$reportingTo['last_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label >Date</label>
                                        <span class="error">*</span>
                                       
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div> 
                                            <input type="text" name="date" id="cre_date" value="{{ old('date') != '' ? old('date') : $leaveDetails['date']}}" class="form-control" readonly="" />
                                            @if ($errors->has('date'))
                                                <div class="error">{{ $errors->first('date') }}</div>
                                            @endif
                                            </div>
                                          
                                    </div>
                                </div>
                                <div class="col-md-5">
                                     <label for="reason" >Leave Reason</label>
                                     <span class="error">*</span>
                                        <textarea class="form-control" name="reason" id="reason" readonly="">{{ old('reason') != '' ? old('reason') : $leaveDetails['reason']}}</textarea>
                                        @if ($errors->has('reason'))
                                            <div class="error">{{ $errors->first('reason') }}</div>
                                        @endif
                                </div>
                                
                            </div>
                            <div class="row">
                               
                                <div class="col-md-4">
                                    <label for="approver_comment" >Approver Comment</label>
                                    <span class="error">*</span>
                                        <textarea class="form-control" name="approver_comment" id="approver_comment" >{{ old('approver_comment') != '' ? old('approver_comment') : $leaveDetails['approver_comment']}}</textarea>
                                        @if ($errors->has('approver_comment'))
                                            <div class="error">{{ $errors->first('approver_comment') }}</div>
                                        @endif
                                </div>
                                <div class="col-md-2">
                                    <label >Status</label>
                                    <?php 
                                    $disabled = '';

                                    ?>
                                @if($leaveDetails['status']==1)
                                <select name="status" id="status" class="select2 form-control custom-select" style="width: 100%; height:36px;" required="" <?php echo $disabled;?>>
                                    <option value="">Selec Status</option>
                                    @foreach(config('constant.leave_status') as $key => $value)
                                    <?php
                                    if($value == "Pending"){
                                        continue;
                                    }
                                    
                                    ?>
                                    <option value="{{$key}}" {{ old('status') != '' ? (old('status') == $key ? 'selected' : '') : ($leaveDetails['status'] == $key ? 'selected' : '')}}>{{$value}}</option>
                                    @endforeach
                                </select>
                                @else
                                <select name="status" id="status" class="select2 form-control custom-select" style="width: 100%; height:36px;" required="" <?php echo $disabled;?>>
                                    <option value="">Select Status</option>
                                    @foreach(config('constant.leave_status') as $key => $value)
                                    <option value="{{$key}}" {{ old('status') != '' ? (old('status') == $key ? 'selected' : '') : ($leaveDetails['status'] == $key ? 'selected' : '')}}>{{$value}}</option>
                                    @endforeach
                                </select>
                                @endif
                                
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark">
                            <a href="{{url('/teamEarlyLeave')}}"><button type="button" value="Cancel" class="btn btn-info btn-secondary">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script src="{{asset('js/early_leave.js?'.time())}}"></script>
@endsection