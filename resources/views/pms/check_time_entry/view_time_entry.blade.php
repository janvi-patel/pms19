@extends('layouts.master')
@section('moduleName')
    Report
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row"> 
                    <div class="col-md-12"> 
                        <div class="col-md-10">
                        
                        </div>
                        <div class="col-md-2 mb-3 float-right" >
                         <form class="form-horizontal" method="post" action="{{url('check_entries')}}" name="search_filter" id="search_filter">
                            @csrf
                                <input type="hidden" id="submit_type" value="export" name="submit_type">
                                <input type="hidden" value="{{json_encode($TimeEntryData)}}" name="data">
                            <a title="Download Excel" id="download_excel">
                                <button type="button" class="btn btn-primary btn-dark btn-block">Download</button>
                            </a> 
                            </form>
                        </div>
                                            
                        <div class="table-responsive">
                            @if (count($TimeEntryData)>0)
                                <table id="project_report_table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Employee Code</th>
                                            <th>Name</th>
                                            
                                            <th>Incorrect Entries</th>
                                            <th>Incorrect Entries reason</th>
                                            <th>Incorrect Entries Count</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @if(isset($TimeEntryData) && !empty($TimeEntryData))
                                            @php $i=1;@endphp
                                            @foreach($TimeEntryData[0] as $key=>$val)                                  
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$val['empcode']}}</td>
                                                        <td>{{$val['name']}}</td>
                                                        <td>
                                                        @foreach($val['time'] as $tkey => $tval)
                                                        {{$loop->iteration.'). '.$tval['incorrect_entry']}}<br>
                                                        @endforeach
                                                        </td>
                                                        <td>
                                                            @foreach($val['time'] as $tkey => $tval)
                                                        {{$loop->iteration.'). '.$tval['incorrect_reason']}}<br>
                                                        @endforeach
                                                        </td>
                                                        <td>{{count($val['time'])}}</td>
                                                </tr>
                                                @php $i++;@endphp
                                            @endforeach
                                        @endif  
                                    </tbody>
                                </table>
                            @else
                                <div class="border-top">
                                    <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
        
@endsection

@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
   
@endsection
