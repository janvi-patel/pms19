<div class="card-body">
    <div class="table-responsive">
        <table id="holiday_listing" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Employee Code</th>
                <th>Name</th>  
                <th>Incorrect Entries Count</th>               
                <th>Incorrect Entries</th>
                <th>Incorrect Entries reason</th>
            </tr>
            </thead>
            <tbody>                       
                @if(isset($data) && !empty($data))
                    @foreach($data[0] as $key=>$val)                                  
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$val['empcode']}}</td>
                            <td>{{$val['name']}}</td>
                            <td>{{count($val['time'])}}</td>
                            <td>
                                @foreach($val['time'] as $tkey => $tval)
                                    <p>{{$loop->iteration.'). '.$tval['incorrect_entry']}}</p>
                                @endforeach
                            </td>
                            <td>
                                @foreach($val['time'] as $tkey => $tval)
                                    <p>{{$loop->iteration.'). '.$tval['incorrect_reason']}}</p>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                @endif  
            </tbody>
        </table>
    </div>
</div>
