@extends('layouts.master')

@section('moduleName')
    Upload Time Entry Sheet
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Upload Time Entry Sheet</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')

<section class="content timeEntryFormSec">
    <div class="container-fluid">
   
        <div class="row">
            <div class="col-md-12">                 
                <div class="card">
                <div class='card-body download_text'><a href="{{asset('doc/check_incorrect_entry_sample.csv')}}"><i class="fa fa-file-excel" aria-hidden="true"></i><b> please download sample file.</b></a></div>
                    <form class="form-horizontal" method="post" action="{{url('check_entries')}}" name="frmUploadTimeEntry" id="frmUploadTimeEntry" enctype="multipart/form-data">
                    @csrf 
                        <div class="card-body">
                            <div class="row"><h5 class="col-md-12"><b>Upload Time Entry Sheet</b></h5></div>
                            <div class="row border-top">
                                <div class="col-md-12 col-sm-12"> 
                                    <div class="form-group">
                                        <label>File Upload (Allow to upload .csv file)</label>
                                        <div class="custom-file">
                                            <input type="file" name="xlsfile" id="validatedCustomFile" class="custom-file-input" />
                                            <label class="custom-file-label" id="uploadedFileName">Choose file...</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="submit" class="btn btn-primary btn-dark" value="Upload" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection

@section('javascript')
@php 
    $end_date = date('d-m-Y',strtotime('-1 days'));
@endphp
<script src="{{asset('js/timeentry.js?'.time())}}"></script>
<script>
    $(document).ready(function(){
        $("#timeEntryDate").datepicker({
            todayBtn: 1,
            autoclose: true,
            format: 'dd-mm-yyyy',
            endDate: "{{ $end_date }}",
        });
    });
</script>
@endsection