@extends('layouts.master')

@section('moduleName')
    View Project
@endsection

@php
    $pageRangArray = config('constant.page_range');
@endphp

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View All Projects</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Project</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/view/projects/'.$request->company)}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Project Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_name" id="search_by_project_name">
                                        <option value="">Select</option>
                                        <?php asort($project_name);?>
                                        @if(!empty($project_name))
                                            @foreach($project_name as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_project_name) && ($request->search_by_project_name == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Client Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_client" id="search_by_client">
                                             <?php asort($client_name);?>
                                        <option value="">Select</option>
                                        @if(!empty($client_name))
                                            @foreach($client_name as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_client) && ($request->search_by_client == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Account Manager</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_account" id="search_by_account">
                                          <?php asort($account_manager_name);?>
                                        <option value="">Select</option>
                                        @if(!empty($account_manager_name))
                                            @foreach($account_manager_name as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_account) && ($request->search_by_account == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4 col-sm-6">
                                    <label>Project Status</label>
                                        <select name="search_project_status" id="search_project_status" class="form-control">
                                            <option value="">Select Project Status</option>
                                            <option value="0" <?php echo (isset($request->search_project_status) &&  $request->search_project_status == 0)?'selected':''?>>All</option>
                                            <?php
                                            $pstatus = config('constant.project_status');
                                            asort($pstatus);
                                            ?>
                                            @if(config('constant.project_status') && !empty(config('constant.project_status')))
                                            @foreach($pstatus as $key => $projectStatusVal)
                                            <option value="{{$key}}" <?PHP echo (isset($request->search_project_status) && $key == $request->search_project_status || ($key == 1 && !isset($request->search_project_status)))?'selected':''?>>{{$projectStatusVal}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                </div>
                                 <div class="form-group {{(Auth::user()->hasRole(config('constant.superadmin_slug')))?'col-md-4 col-sm-6':'col-md-4 col-sm-6'}}">
                                    <label class="">Project Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_start_date" id="search_by_start_date" value="{{(isset($request->search_by_start_date)?$request->search_by_start_date:'')}}" class="form-control" readonly />
                                    </div>
                                    <a href="javascript:void(0)" id="reset_startdate">Reset</a>
                                </div>
                                <div class="form-group {{(Auth::user()->hasRole(config('constant.superadmin_slug')))?'col-md-4 col-sm-6':'col-md-4 col-sm-6'}}">
                                    <label class="">Project End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_end_date" id="search_by_end_date" value="{{(isset($request->search_by_end_date)?$request->search_by_end_date:'')}}"  class="form-control" readonly />
                                    </div>
                                    <a href="javascript:void(0)" id="reset_enddate">Reset</a>
                                </div>
<!--                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Company Name</label>
                                    <input type="text" name="search_by_company" id="search_by_company" value="{{(isset($request->search_by_company)?$request->search_by_company:'')}}" class="form-control" autocomplete="off" />
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Assigned Company</label>
                                    <input type="text" name="search_by_assign_company" id="search_by_assign_company" value="{{(isset($request->search_by_assign_company)?$request->search_by_assign_company:'')}}"  class="form-control" autocomplete="off" />
                                </div>-->
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Project Type</label>
                                    <select name="search_project_type" id="search_project_type" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($projectTypes as $key => $projectStatusVal)
                                            <option value="{{$projectStatusVal->id}}" {{(isset($request->search_project_type) && ($request->search_project_type == $projectStatusVal->id))?'selected':''}}>{{$projectStatusVal->project_type_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if(!Auth::user()->hasRole(config('constant.project_manager_slug')))
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Project Manager</label>
                                        <?php asort($project_manager_name);?>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_manager" id="search_by_project_manager">
                                        <option value="">Select</option>
                                        @if(!empty($project_manager_name))
                                            @foreach($project_manager_name as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_project_manager) && ($request->search_by_project_manager == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                @endif
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                    <div class="form-group col-md-4 col-sm-6">
                                        <label class="">Company</label>
                                        <select name="company" id="company" class="form-control select2" style="width: 100%; height:36px;">
                                            <option value="0">All Companies</option>
                                            @if(count($companies)>0)
                                            @foreach($companies as $company)
                                            <option value="{{$company->id}}" {{(isset($request->company) && $request->company == $company->id)?'selected':'' }}>
                                                {{$company->company_name}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                @endif

                            </div>
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/projects/0')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex align-items-center justify-content-between">
                                <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                    @if(is_array($pageRangArray))
                                       @foreach ($pageRangArray as $key => $node)
                                            <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class=" show_total_no float-right pl-3">
                                    Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of total {{$data->total()}} entries
                                </div>
                            </div>
                        </div>

                        <div id="project_listing">
                            @include('pms.project.project_listing_table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="createTeamModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post" action="{{url('project/save_team')}}" id="saveTeamForm">
            @csrf
                <div class="modal-header card-header bg-dark">
                    <h4 class="modal-title">Assign To Team</h4>
                    <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
                </div>
                <div class="modal-body assign_to_team_records" id="modal-body">
                    @include('pms.project.assign_to_team')
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" id="saveTeam" value="Create Team">
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@include('pms.task.modal.create_task')
@endsection
