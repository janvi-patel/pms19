<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="holiday_listing" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th><center>No</center></th>
                        <th>@sortablelink('project_name','Project Name')</th>
                        <th>@sortablelink('project_type_name','Project Type')</th>
                        <th>@sortablelink('project_start_date','Start Date')</th>
                        <th>@sortablelink('client_name','Client Name')</th>
                        <th>@sortablelink('account_manager_name','Account Manager')</th>
                        @if(!Auth::user()->hasRole(config('constant.project_manager_slug')))
                            <th>@sortablelink('project_manager_name','Project Manager')</th>
                        @endif
                        <th>@sortablelink('project_status','Status')</th>
                        <th>PIS Hours</th>
                        <th>Assigned Hours</th>
                        <th>@sortablelink('total_logged_hours','Logged Hours')</th>
                        <th  style="padding-right:1px !Important">Project Completed</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @if (count($data)>0)
                        @php
                            $i = 1;
                            $j = $data->firstItem();
                            $totalEstimatedHours = $totalLoggedHours = $totalCrHours = $totalHours = $sumOfTotalHours= 0.00;
                        @endphp
                        @foreach($data as $dataVal)
                            <?php
                                $estimatedHours = (isset($dataVal->pms_hours) && $dataVal->pms_hours!='')?$dataVal->pms_hours:'0.00';
                                $loggedHours = ($dataVal->total_logged_hours)?$dataVal->total_logged_hours:'0.00';
                                $crHours = (isset($projectCrHours[$dataVal->id]) && $projectCrHours[$dataVal->id]!='')?$projectCrHours[$dataVal->id]:'0';
                                $totalEstimatedHours = $totalEstimatedHours + $estimatedHours;
                                $totalLoggedHours    = $totalLoggedHours + $loggedHours;
                                $totalCrHours = $totalCrHours + $crHours;
                                $totalHours = $estimatedHours + $crHours;
                                $totalWithCrandEst = isset($dataVal->total_logged_hours)?$dataVal->total_logged_hours:'0.00';


                                $sumOfTotalHours = $totalHours + $sumOfTotalHours;
                                $percentage = 0.00;
                                if($loggedHours != 0 && $totalHours != 0){
                                    $percentage          = ($loggedHours/$totalHours)*100;
                                }
                            ?>

                            <tr>
                                <td><center>{{$j}}</center></td>
                                <td>

                                     <a href="{{url('view/task/list/'.$dataVal->id)}}" data-toggle="tooltip" data-placement="top"
                                               title="" data-original-title="View" class="project-task-info a_clickable_link" project_id="{{$dataVal->id}}">
                                                {{(isset($dataVal->project_name) && $dataVal->project_name!='')?$dataVal->project_name:'N/A'}}</a>&nbsp;
                                </td>
                                <td>{{(isset($dataVal->project_type_name) && $dataVal->project_type_name!='')?$dataVal->project_type_name:'N/A'}}</td>
                                <td>{{(isset($dataVal->project_start_date) && $dataVal->project_start_date!='')?date('d-m-Y',strtotime($dataVal->project_start_date)):'N/A'}}</td>
                                <td>{{(isset($dataVal->client_name) && $dataVal->client_name!='')?$dataVal->client_name:'N/A'}}</td>
                                <td>{{(isset($dataVal->account_manager_name) && $dataVal->account_manager_name!='')?$dataVal->account_manager_name:'N/A'}}</td>
                                @if(!Auth::user()->hasRole(config('constant.project_manager_slug')))
                                    <td>{{(isset($dataVal->project_manager_name) && $dataVal->project_manager_name!='')?$dataVal->project_manager_name:'N/A'}}</td>
                                @endif
                                <td>
                                    @if(isset($dataVal->project_status) && $dataVal->project_status == 1)
                                        {{'Open'}}
                                    @elseif(isset($dataVal->project_status)  && $dataVal->project_status == 2)
                                        {{'On Hold'}}
                                    @elseif(isset($dataVal->project_status)  && $dataVal->project_status == 3)
                                        {{'Closed'}}
                                    @elseif(isset($dataVal->project_status)  && $dataVal->project_status == 4)
                                        {{'Completed'}}
                                    @else
                                        {{'N/A'}}
                                    @endif
                                </td>
<!--                                <td>{{(isset($dataVal->estimated_hours) && $dataVal->estimated_hours!='')?$dataVal->estimated_hours:'0.00'}}</td>
                                <td>
                                    {{$crHours.'.00'}}
                                </td>-->
                                <td>
                                    <!--{{$totalHours,'.00'}}-->
                                    {{sprintf("%.2f",$totalHours)}}
                                    @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                        <a href="javascript;" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" class="project-info" project_id="{{$dataVal->id}}"><i class="mdi mdi-information-outline"></i></a>
                                    @endif
                                </td>
                                @php
                                    $totalTaskHours =0;
                                    $taskHour = (new \App\Helpers\CommonHelper)->getTaskHours($dataVal->id,'');
                                    $totalTaskHours = $totalTaskHours + $taskHour->total_task_hours;
                                @endphp
                                <td class="{{((sprintf("%.2f",$totalHours))<(sprintf("%.2f",$totalTaskHours)))?'project_completion_red':''}}">
                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime($totalTaskHours)}}
                                </td>
                                <td>
                                    @if($totalWithCrandEst)
                                        <!--{{sprintf("%.2f",$dataVal->total_logged_hours)}}-->
                                        {{(new \App\Helpers\CommonHelper)->displayTaskTime($totalWithCrandEst)}}
                                    @else
                                        {{$totalWithCrandEst}}
                                    @endif
                                </td>
                                <td>
                                    @if(sprintf("%.2f",$percentage) <= 100)
                                        <span class="project_completion_gree">
                                            {{sprintf("%.2f",$percentage).' %'}}
                                        </span>
                                    @else
                                        <span class="project_completion_red">
                                            {{sprintf("%.2f",$percentage).' %'}}
                                        </span>
                                    @endif
                                </td>

                                <td align="center">
                                    @if((Auth::check() && Auth::user()->hasPermission('projects.task.listing')) || ($dataVal->project_name == 'Bench' || $dataVal->project_name == 'Miscellaneous Tasks'))
                                        <a href="javascript:void(0);" title="Add Task" class="addProjectTaskBtn" data-project-id="{{$dataVal->id}}"><i class="fa fa-list-alt" aria-hidden="true"></i></a>
                                    @endif
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" class="timeEntry" data-task-id="{{$dataVal->id}}"><i class="mdi mdi-alarm"></i></a>
                                    @if($dataVal->project_name != 'Bench' && $dataVal->project_name != 'Miscellaneous Tasks')
                                        <a href="{{url('view/project/notes/'.$dataVal->id)}}" data-toggle="tooltip" data-placement="top" class="viewNotes" data-task-id="{{$dataVal->id}}" title="View Notes">
                                            <i class="mdi mdi-book-open-page-variant"></i>
                                        </a>
                                        @if($loggedInCompanyId == $dataVal->assigned_to || Auth::user()->id == $dataVal->project_manager)
                                            <a href="javascript:void(0);" class="createTeam" id="{{$dataVal->id}}" data-assiged-to-companyid="{{$dataVal->assigned_to}}" title="Create Team">
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                            </a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @php
                                $i++;$j++;
                            @endphp
                        @endforeach

                        @if(count($data)>0)
                            <tr>
                                @if(Auth::user()->hasRole(config('constant.project_manager_slug')))
                                <td colspan="7" align="right"><b>Total</b></td>
                                @else
                                <td colspan="8" align="right"><b>Total</b></td>
                                @endif
                                <td><b>{{sprintf("%.2f",$sumOfTotalHours)}}</b></td>
                                <td></td>
                                <td><b>{{sprintf("%.2f",$totalLoggedHours)}}</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    @else
                        <tr>
                            @if(Auth::user()->hasRole(config('constant.project_manager_slug')))
                                <td colspan='13'><center>No Project Found</center></td>
                            @else
                                <td colspan='14'><center>No Project Found</center></td>
                            @endif
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if($data && !empty($data))
            <div class="pt-3">{!! $data->appends(\Request::except('page'))->render() !!}</div>
        @endif
    </div>
</div>
<div id="project_cost" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Project Info</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"><a href="" class='view_cr_info'>View</a></button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@include('pms.task.modal.time_entry_modal')
<div id="addProjectTaskModal" class="modal fade" role="dialog">

</div>
@section('javascript')
<script src="{{asset('js/moment-with-locales.js?'.time())}}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.min.js?'.time())}}"></script>
<script src="{{asset('js/select2.min.js?'.time())}}"></script>
<script src="{{asset('js/project.js?'.time())}}"></script>
<script src="{{asset('js/project_all.js?'.time())}}"></script>
<script src="{{asset('js/misc_task.js?'.time())}}"></script>
@endsection
