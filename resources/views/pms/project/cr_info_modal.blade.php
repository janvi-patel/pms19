<div class="card-body">
    <div class="table-responsive">
        <table id="project_details_listing" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Project / CR Date</th>
                    <th>Project / CR Title</th>
                    @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug')))
                    <th>Estimated Hours</th>
                    @endif
                    <th>Pms Hours</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($cr_details))
                    @foreach($cr_details as $cr_details_val)
                    <tr>
                        <td>{{ date("d-m-Y", strtotime($cr_details_val['created_at']))}}</td>
                        <td>{{$cr_details_val['title']}}</td>
                        @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug')))
                        <td>{{$cr_details_val['estimated_hours']}}</td>
                        @endif
                        <td>{{$cr_details_val['pms_hours']}}</td>
                    </tr>
                    @endforeach
                @endif
                @if(!empty($project_details))
                     <tr>
                        <td>{{ date("d-m-Y", strtotime($project_details['start_date']))}}</td>
                        <td>{{$project_details['project_name']}}</td>
                        @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug')))
                        <td>{{$project_details['estimated_hours']}}</td>
                        @endif
                        <td>{{$project_details['pms_hours']}}</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>