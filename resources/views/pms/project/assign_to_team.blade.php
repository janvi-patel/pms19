<input type="hidden" name="assginToTeamProjectId" id="assginToTeamProjectId" value="{{(isset($projectId))?$projectId:''}}" />
<input type="hidden" name="assginToTeamCompanyId" id="assginToTeamCompanyId" value="{{(isset($companyId))?$companyId:''}}" />
<div class="table-responsive">
<table class="table table-striped table-bordered">
    <tr>
        <td>Project Manager</td>
        <td>
			{{isset($projectManager)?$projectManager:''}}
		</td>
    </tr>
    <tr>
        <td>Team Leader<span class="error">*</span></td>
        <td>
            <select name="teamLeader" id="teamLeader" class="form-control select2" >
                @if(isset($teamLeaders) && count($teamLeaders) > 0) 
                    <option value="">Select Team Leader</option>
                    @foreach($teamLeaders as $teamLeader)
                        <option value="{{$teamLeader->id}}" <?php if($teamLeader->id == $teamLeaderId){ echo "selected"; } ?>>
                            {{$teamLeader->first_name.' '.$teamLeader->last_name}}
                        </option>
                    @endforeach
                @endif
            </select>
            <label id="teamLeader-error" class="error" for="teamLeader"></label>
		</td>
    </tr>
    <tr>
    	<td>Developer</td>
    	<td>
    		<select name="teamAssign[]" id="teamAssign" class="form-control select2" multiple="multiple" style="width: 100% !important" data-placeholder="Select Developer">
	            @if(isset($users) && !empty($users)) 
	            	@foreach($users as $user)
                        @if(in_array($user->id,$assignedUserIds))
    	            		<option value="{{$user->id}}" selected="selected">
    	            			{{$user->first_name.' '.$user->last_name ."  ( ".$user->emp_short_name."".$user->employee_id." )"}}
    	            		</option>
                        @else
                            <option value="{{$user->id}}">
                                {{$user->first_name.' '.$user->last_name ."  ( ".$user->emp_short_name."".$user->employee_id." )"}}
                            </option>
                        @endif
	            	@endforeach
	            @endif
	        </select>
    	</td>
    </tr>
</table>
</div>