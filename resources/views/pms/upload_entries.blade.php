@extends('layouts.master')

@section('moduleName')
    Upload Time Entry
@endsection
<style>
    @keyframes spinner-border {
        to {transform: rotate(360deg);}
    }
    .spinner-border {
        position:fixed;
        top:50%;
        right:50%;
        width:50%;
        height:50%;
        z-index:10000000;
        opacity: 0.7;
        filter: alpha(opacity=40);
        display: inline-block;
        width: 2rem;
        height: 2rem;
        vertical-align: text-bottom;
        border: .25em solid currentColor;
        border-right-color: transparent;
        border-radius: 50%;
        -webkit-animation: spinner-border .75s linear infinite;
        animation: spinner-border 1s linear infinite;
    }
</style>
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Upload Time Entries</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')

<section class="content timeEntryFormSec">
    <div class="container-fluid">
        <div class="card-body">
            <a href="{{url('/upload/old/entries')}}" class="btn btn-primary btn-dark text-white float-right">Old Entry</a>
        </div>
        <div class="row">
            <span class="spinner-border d-none"></span>
            <div class="col-md-12">                 
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{url('/upload/entries')}}" name="frmUploadTimeEntry" id="frmUploadTimeEntry" enctype="multipart/form-data">
                    @csrf 
                        <div class="card-body">
                            <div class="row"><h5 class="col-md-12"><b>Upload Time Entry</b></h5></div>
                            <div class="row border-top">
                                <div class="col-md-4 col-sm-4"> 
                                    <div class="form-group">
                                        <label>Time Entry Date</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="timeEntryDate" id="timeEntryDate" value="" class="form-control" readonly/>
                                        </div>
                                        <label id="timeEntryDate-error" class="error" for="timeEntryDate">@if ($errors->has('timeEntryDate')) {{ $errors->first('timeEntryDate') }} @endif</label>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-8"> 
                                    <div class="form-group">
                                        <label>File Upload (Allow to upload .csv file)</label>
                                        <div class="custom-file">
                                            <input type="file" name="xlsfile" id="validatedCustomFile" class="custom-file-input" />
                                            <label class="custom-file-label" id="uploadedFileName">Choose file...</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="submit" class="btn btn-primary btn-dark" value="Upload" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">                 
                <div class="card">
                    <form class="form-horizontal" method="post" action="" name="frmUploadDateWiseTimeEntries" id="frmUploadDateWiseTimeEntries" enctype="multipart/form-data">
                    @csrf 
                        <div class="card-body">
                            <div>
                                <div class="row"><h5 class="col-md-6"><b>Upload Time Entry Using Cron</b></h5></div>
                                <div class="row border-top pt-3">
                                    <div class="col-md-4 col-sm-4"> 
                                        <div class="form-group">
                                            <label class="pt-0">Time Entry Date</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" name="timeEntryDateForCron" id="timeEntryDateForCron" value="" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 pt-2 pt-sm-4"> 
                                        <input type="button" class="btn btn-primary btn-dark mt-2" value="upload" id="uploadTimeEntryUsingCron"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">                 
                <div class="card">
                    <form class="form-horizontal" method="post" action="" name="frmDeleteDateWiseTimeEntries" id="frmDeleteDateWiseTimeEntries" enctype="multipart/form-data">
                    @csrf 
                        <div class="card-body">
                            <div>
                                <div class="row"><h5 class="col-md-6"><b>Delete Time Entry</b></h5></div>
                                <div class="row border-top pt-3">
                                    <div class="col-md-4 col-sm-4"> 
                                        <div class="form-group">
                                            <label class="pt-0">Time Entry Date</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" name="entryDate" id="entryDate" value="" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 pt-2 pt-sm-4"> 
                                        <input type="button" class="btn btn-primary btn-dark mt-2" value="Delete" id="deleteTimeEntry"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascript')
@php 
    $end_date = date('d-m-Y',strtotime('-1 days'));
@endphp
<script src="{{asset('js/timeentry.js?'.time())}}"></script>
<script>
    $(document).ready(function(){
        $("#timeEntryDate").datepicker({
            todayBtn: 1,
            autoclose: true,
            format: 'dd-mm-yyyy',
            endDate: "{{ $end_date }}",
        });
         $("#timeEntryDateForCron").datepicker({
            todayBtn: 1,
            autoclose: true,
            format: 'dd-mm-yyyy',
            endDate: "{{ $end_date }}",
        });
    });
</script>
@endsection