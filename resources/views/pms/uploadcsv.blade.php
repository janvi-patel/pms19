@extends('layouts.master')
@section('content')
@section('moduleName')
    Upload Salary xlsx
@endsection
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Upload Salary xlsx</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card-body">
                    <a href="{{url('/old/salary/logs')}}" class="btn btn-primary btn-dark text-white float-right">Old Email Logs</a>
                </div>
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{url('/uploadcsv')}}" name="upload_csv" id="upload_csv" enctype="multipart/form-data">
                    @csrf 
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3">File Upload(Allow to upload .xlsx file)</label>
                                <div class="custom-file">
                                    <input type="file" name="csvfile" id="validatedCustomFile" class="custom-file-input"/>
                                    <label class="custom-file-label" for="validatedCustomFile" id="uploadedFileName">Choose file...</label>
                                </div>
                                @if($errors->has('csvfile'))
                                    <div class="error">{{ $errors->first('csvfile') }}</div>
                                @endif
                            </div>
                            <div class="form-check">
                                <input type="checkbox" id="re_upload" name="re_upload" class="form-check-input" {{ old('re_upload') ? 'checked' : '' }} style="width:auto !important;"> 
                                <label for="re_upload">Re-upload File</label>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button name="submit" type="submit" class="btn btn-primary btn-dark">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#uploadedFileName').text(fileName);
    });
});
</script>
@endsection