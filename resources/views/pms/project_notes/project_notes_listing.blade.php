@extends('layouts.master')

@section('moduleName')
View Project
@endsection

@php
$pageRangArray = config('constant.page_range');
@endphp

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage"
                    style="padding: 10px 12px;"><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Notes Listing</li>
                </ol>
            </div>
        </div>
    </div>
</section>
@include('sweet::alert')
<?php 
$search_class = "";
?>
@if(Request::get('search_submit'))
   <?php 
       $search_class = "show";
   ?>
@endif
<section class="content">
    <section class="container-fluid">
        @php
        
        @endphp
        {{-- <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark collapsed" data-toggle="collapse" data-parent="#accordion"
                    href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Project Tasks</h3>
                    <div class="card-tools"> 
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i
                                class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in collapse @php echo $search_class @endphp">
                    <form class="form-horizontal" method="get" action="" name="timeSheetFilterForm"
                        id="timeSheetFilterForm">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-3 col-sm-3">
                                    <label class="">Employee</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;"
                                        name="search_user" id="search_user">
                                        <option value="">Select Employee</option>
                                        @foreach($project_team_details as $userDetail)
                                            <option value="{{$userDetail->id}}"
                                                {{(isset($request->search_user) && $request->search_user == $userDetail->id)?'selected':''}}>
                                                {{$userDetail->first_name.' '.$userDetail->last_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                            <div class="card-footer">
                                <button name="search_submit" value="1" type="submit"
                                    class="btn btn-primary btn-dark">
                                    Search
                                </button>
                                <button name="search_reset" type="reset" class="btn btn-info btn-secondary"
                                    onclick="location.href ='{{url('/view/project/notes/'.$request->id)}}'">
                                    Reset
                                </button>
                            </div>
                    </form>
                </div>
            </div>
        </div> --}}
       
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3 d-flex w-100 justify-content-between flex-column flex-sm-row">
                                <div class="left d-flex">
                                    <select class="select2 form-control custom-select page_range_dropdown min_width_70"
                                        name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                        @foreach ($pageRangArray as $key => $node)
                                        <option value="{{$key}}"
                                            {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}>
                                            {{ $node }}</option>
                                        @endforeach
                                        @endif
                                    </select>

                                    <div class="pl-3"> Showing {{ $data->firstItem() }} to
                                        {{ $data->lastItem() }} of total {{$data->total()}} entries
                                    </div>
                                </div>
                                <div class="right mt-3 mt-sm-0">
                                    <a href="javascript:void(0);" title="Add Task"
                                        class="btn btn-primary btn-dark btn-block padding-5 addProjectNoteBtn"
                                        data-project-id='{{isset($request)?$request->id:''}}'>Add Notes</a>
                                </div>
                            </div>
                            @php
                                $projectData = (new \App\Helpers\CommonHelper)->getProjectData($id) != ''?(new \App\Helpers\CommonHelper)->getProjectData($id):null;
                                $projectManager = isset($projectData)&& $projectData->project_manager !=  ''?(new \App\Helpers\CommonHelper)->getAnyUserById($projectData->project_manager):null;
                                $teamLeader = isset($projectData)&& $projectData->team_leader !=  ''?(new \App\Helpers\CommonHelper)->getAnyUserById($projectData->team_leader):null;
                            @endphp
                            <div class="col-md-12">
                                <div class="table-responsive-md">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                            <tr>
                                                <th style="width: 20%">Project Name</th>
                                                <td>
                                                    {{isset($projectData)&& $projectData->project_name !=  ''?$projectData->project_name:''}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 20%">Project Manager</th>
                                                <td>
                                                    {{isset($projectManager)&& $projectManager->first_name != ''?$projectManager->first_name.' '.$projectManager->last_name :''}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 20%">Team Leader</th>
                                                <td>
                                                    {{isset($teamLeader)&& $teamLeader->first_name != ''?$teamLeader->first_name.' '.$teamLeader->last_name :''}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="holiday_listing" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@sortablelink('note_title','Title')</th>
                                                <th>@sortablelink('note','Description')</th>
                                                <th>@sortablelink('first_name','Added By')</th>
                                                <th>@sortablelink('created_at','Date')</th>
                                                <th>@sortablelink('type','Note Type')</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $j = $data->firstItem();
                                            @endphp
                                            @if(count($data)>0)
                                            @foreach($data as $note)
                                            <tr>
                                                <td class="text-center">{{$j}}</td>
                                                <td>{{($note['note_title'] != '')?$note['note_title']:''}}</td>
                                                <td>{{($note['note'] != '')?$note['note']:''}}</td>
                                                <td>{{($note['first_name'] != '')?$note['first_name'].' '.$note['last_name']:''}}</td>
                                                <td>{{($note['created_at'] != '')?date('d-m-Y',strtotime($note['created_at'])):''}}</td>
                                                <td>{{($note['type'] == 0) ? 'Public':'Private'}}</td>
                                                <td>
                                                    @if(Auth::check() && Auth::user()->id == $note['user_id'])
                                                        <a href="javascript:void(0);" data-toggle="tooltip"
                                                            data-placement="top" title="Edit Project Notes"
                                                            class="editProjectNote"
                                                            data-note-id="{{$note['id']}}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        <a href="javascript:void(0);" data-toggle="tooltip"
                                                            data-placement="top" title="Delete Task"
                                                            class="delete deleteProjectNote"
                                                            data-note-id="{{$note['id']}}">
                                                            <i class="mdi mdi-close"></i>
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @php
                                                $j++;
                                            @endphp
                                            @endforeach
                                            @else
                                            <tr>
                                                <td colspan="11" class="text-center">No Notes Found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if($data && !empty($data))
                                    <div class="pt-3">{!! $data->appends(\Request::except('page'))->render() !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
<div id="editProjectNoteModal" class="modal fade" role="dialog">

</div>
<div id="addProjectNoteModal" class="modal fade" role="dialog">

</div>
@section('javascript')
<script src="{{asset('js/project-note.js?'.time())}}"></script>
@endsection