<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header card-header bg-dark">
            <h4 class="modal-title">Edit Task</h4>
            <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
        </div>
        <form action="{{url('edit/editProjectNotes')}}" id="editProjectNotes" method="post">
            @csrf
            <input type="hidden" name="editProjectId" id="editProjectId" value="{{(isset($project) && $project)?$project->id:''}}">
            <input type="hidden" name="editProjectNoteId" id="editProjectNoteId" value="{{(isset($noteDetails) && $noteDetails)?$noteDetails->id:''}}">
            <div class="modal-body assign_to_team_records" id="modal-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="col-form-label">Project Name</label>
                    </div>
                    <div class="col-sm-9">
                        <p style="padding: 10px;margin-bottom: 0px;">{{$project->project_name}}</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="col-form-label">User<span class="error">*</span></label>
                    </div>
                    <div class="col-sm-6">
                        <p style="padding: 10px;margin-bottom: 0px;">{{(Auth::check())?Auth::user()->first_name.' '.Auth::user()->last_name:''}}</p>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="col-form-label">Notes Type</label>
                    </div>
                    <div class="col-sm-6">
                        <select name="editNoteType" id="editNoteType" class="form-control select2" style="width: 100% !important" data-placeholder="Select Note Type">
                            <option value="0">Public</option>
                            <option {{$noteDetails->type == 1 ? 'selected':''}} value="1">Private</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="col-form-label">Notes Title<span class="error">*</span></label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" name="editNotesTitle" id="editNotesTitle" autocomplete="off" class="form-control" value="{{(isset($noteDetails->note_title)?$noteDetails->note_title:'')}}"/>
                        <label id="editNoteTitle-error" class="error" for="editNotesTitle"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label class="col-form-label">Notes<span class="error">*</span></label>
                    </div>
                    <div class="col-sm-9">
                        <textarea rows="3" cols="3" name="editNotes" id="editNotes" autocomplete="off" class="form-control">{{(isset($noteDetails->note)?$noteDetails->note:'')}}</textarea>
                        <label id="editNotes-error" class="error" for="editNotes"></label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary btn-dark" value="Update Note"/>
                <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
</div>