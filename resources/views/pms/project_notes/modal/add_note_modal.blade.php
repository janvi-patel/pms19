<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header card-header bg-dark">
            <h4 class="modal-title">Add Notes</h4>
            <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
        </div>
        <form action="{{url('add/addProjectNotes')}}" id="addProjectNotes" method="post">
            @csrf
            <input type="hidden" name="addNotesProjectId" id="addNotesProjectId" value="{{(isset($project) && $project)?$project->id:''}}">
            <div class="modal-body assign_to_team_records" id="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label class="col-form-label">Project Name</label>
                        </div>
                        <div class="col-sm-9">
                            <p style="padding: 10px;margin-bottom: 0px;">{{$project->project_name}}</p>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label class="col-form-label">User</label>
                        </div>
                        <div class="col-sm-6">
                            <p style="padding: 10px;margin-bottom: 0px;">{{(Auth::check())?Auth::user()->first_name.' '.Auth::user()->last_name:''}}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label class="col-form-label">Notes Type</label>
                        </div>
                        <div class="col-sm-6">
                            <select name="noteType" id="noteType" class="form-control select2" style="width: 100% !important" data-placeholder="Select Note Type">
                                    <option value="0">Public</option>
                                    <option value="1">Private</option>
                            </select>  
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label class="col-form-label">Note Title<span class="error">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="addNotesTitle" id="addNotesTitle" autocomplete="off" class="form-control" value=""/>
                            <label id="addNoteTitle-error" class="error" for="addNotesTitle"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label class="col-form-label">Description<span class="error">*</span></label>
                        </div>
                        <div class="col-sm-9">
                            <textarea rows="3" cols="3" name="addNotes" id="addNotes" autocomplete="off" class="form-control"></textarea>
                            <label id="addNotes-error" class="error" for="addNotes"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" value="Add Notes"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.select2').select2();
});
</script>