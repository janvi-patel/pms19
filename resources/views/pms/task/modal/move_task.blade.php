<div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Move Task</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>
            <form id="moveTaskForm" name="moveTaskForm">
                <div class="modal-body assign_to_team_records" id="modal-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label class="">Project Name</label>
                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="move_to_project_id" id="move_to_project_id" required>
                                <option value="">Select</option>
                                @if(!empty($project_name))
                                    @foreach($project_name as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-12">
                            <label class="">Cr / Project Name</label>
                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="project_cr_list" id="project_cr_list" required>
                                <option value="">Select</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark move_task_entry_btn" value="Move Task"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#move_to_project_id').select2();
            $('#project_cr_list').select2();
            $(document).on('change', '#move_to_project_id', function() {
                $('#project_cr_list').select2("val", 0);
                var idState = this.value;
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: getsiteurl() + '/getChangesProjectCr',
                    type: "POST",
                    data: {
                        move_to_project_id:  jQuery('#move_to_project_id').find(':selected').val(),
                        _token: CSRF_TOKEN
                    },
                    dataType: 'json',
                    success: function (res) {
                        console.log(res.project_cr_list.project_name);
                        var html = "";
                         $('#project_cr_list').html('');
                        $.each(res.project_cr_list.get_project_cr, function (key, value) {
                            html += '<option value="cr_' + value.id + '">' + value.title + '</option>';
                        });
                        html += '<option value="project_' + res.project_cr_list.id + '">' + res.project_cr_list.project_name + '</option>';

                        $('#project_cr_list').append(html);
                        $('#project_cr_list').select2();
                    }
                });
            });
        });
    </script>
