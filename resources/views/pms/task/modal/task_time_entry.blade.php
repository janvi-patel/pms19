<div id="taskTimeEntryModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Add Task Entry</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>

            <form method="post" action="{{url('save/task/entry')}}" id="saveTaskEntry">
                @csrf
                <input type="hidden" name="taskId" id="taskId" value="">

                @if(!is_null($projectName))
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="true" />
                @else
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="false" />
                @endif

                <div class="modal-body assign_to_team_records" id="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task Date<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="taskDate" class="form-control" id="taskDate" autocomplete="off" readonly="readonly" style="width: 220px;">
                                @if ($errors->has('taskDate'))
                                    <div class="error">{{'Please select date'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Work Time<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="workTimeHours" id="workTimeHours" style="width: 90px;">
                                    <option value="">Hours</option>
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <label id="workTimeHours-error" class="error" for="workTimeHours"></label>
                                @if ($errors->has('workTimeHours'))
                                    <div class="error">{{'Please select hours'}}</div>
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="workTimeMinutes" id="workTimeMinutes" style="width: 90px">
                                    <option value="">Minutes</option>
                                    <option value="0">0</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                </select>
                                <label id="workTimeMinutes-error" class="error" for="workTimeMinutes"></label>
                                @if($errors->has('workTimeMinutes'))
                                    <div class="error">{{'Please select minutes'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task Description<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <textarea rows="10" cols="10" name="taskDescription" id="taskDescription" autocomplete="off" class="form-control"></textarea>
                                @if($errors->has('taskDescription'))
                                    <div class="error">{{'Please enter task description'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" value="Save"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
