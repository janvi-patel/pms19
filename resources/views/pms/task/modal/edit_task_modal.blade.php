<div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Edit Task</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>
            <form action="{{url('update/updateProjectTask')}}" id="updateProjectTask" method="post">
                @csrf
                <input type="hidden" name="editTaskProjectId" id="editTaskProjectId" value="{{(isset($project) && $project)?$project->id:''}}">
                <input type="hidden" name="editProjectTaskId" id="editProjectTaskId" value="{{(isset($taskDetails) && $taskDetails)?$taskDetails->id:''}}">
                <div class="modal-body assign_to_team_records" id="modal-body">
                     <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Project Name</label>
                            </div>
                            <div class="col-sm-9">
                                <p style="padding: 10px;margin-bottom: 0px;">{{$project->project_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task Name<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="taskName" class="form-control" id="taskName" autocomplete="off" value="{{(isset($taskDetails))?$taskDetails->task_name:''}}">
                                @if ($errors->has('taskName'))
                                    <div class="error">{{'Please enter task name'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(!($project->project_name == 'Bench' || $project->project_name == 'Miscellaneous Tasks'))
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="col-form-label">Project / CR<span class="error">*</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <select name="project_cr_id" id="projectCrId" class="form-control select2" style="width: 100% !important;" data-placeholder="Select Project / CR">
                                        @if( count($data['project_cr_list']) > 0 )
                                            @if(count($data['project_cr_list']['get_project_cr'])>0)
                                                @foreach($data['project_cr_list']['get_project_cr'] as $key => $value)
                                                <option value="cr_{{ $value['id'] }}" @if(isset($taskDetails->cr_id) && $taskDetails->cr_id != "0" && $taskDetails->cr_id == $value['id']) selected @endif>{{$value['title']}}</option>
                                                @endforeach
                                            @endif
                                        @endif
                                        <option value="project_{{$data['project_cr_list']['id']}}" @if(isset($taskDetails->cr_id) && $taskDetails->cr_id == "0") selected @endif>{{ $data['project_cr_list']['project_name'] }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <?php
                            $hoursMinutes = array();
                            $storedHours = $storedMinutes = ''; 
                            if(isset($taskDetails) && isset($taskDetails->estimated_hours) && $taskDetails->estimated_hours != ''){                                
                                $hoursMinutes   = explode(".",$taskDetails->estimated_hours);
                                $storedHours    = isset($hoursMinutes[0])?$hoursMinutes[0]:0;
                                $storedMinutes  = isset($hoursMinutes[1])?$hoursMinutes[1]:0;
                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Work Time<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="editWorkTimeHours" class="form-control" id="editWorkTimeHours" autocomplete="off" value="{{$storedHours}}" placeholder="Hours">
                                <label id="editWorkTimeHours-error" class="error" for="editWorkTimeHours"></label>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="editWorkTimeMinutes" id="editWorkTimeMinutes" style="width: 90px">
                                    <option value="">Minutes</option>
                                    <option value="0" <?php if(isset($taskDetails) && $storedMinutes == 0){ echo "selected"; } ?>>0</option>
                                    <option value="25" <?php if(isset($taskDetails) && $storedMinutes == 25){ echo "selected"; } ?>>15</option>
                                    <option value="5" <?php if(isset($taskDetails) && ($storedMinutes == 5  || $storedMinutes == 50)){ echo "selected"; } ?>>30</option>
                                    <option value="75" <?php if(isset($taskDetails) && $storedMinutes == 75){ echo "selected"; } ?>>45</option>
                                </select>
                                <label id="editWorkTimeMinutes-error" class="error" for="editWorkTimeMinutes"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task Date<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="task_start_date" id="task_start_date" value="{{(isset($taskDetails) && $taskDetails->task_start_date != '')? date('d-m-Y',strtotime($taskDetails->task_start_date)):date('d-m-Y')}}" class="form-control valid"  aria-invalid="false" placeholder="Start Date">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="task_end_date" id="task_end_date" value="{{(isset($taskDetails) && $taskDetails->task_end_date != '')? date('d-m-Y',strtotime($taskDetails->task_end_date)):date('d-m-Y')}}" class="form-control valid"  aria-invalid="false" placeholder="End Date">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">User<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-6">
                                @if($project->project_name == 'Bench' || $project->project_name == 'Miscellaneous Tasks')
                                @if(count($userNameList)>0)
                                    @foreach($userNameList as $key => $value)
                                      @if($taskDetails->user_id == $value->id)
                                            <input type="hidden" name='assigned_user_list' id="assigned_user_list" class="form-control" value="{{$value->id}}">
                                            <p style="padding: 10px;margin-bottom: 0px;">{{$value->first_name.' '.$value->last_name. ' (' . $value->email . ') '}}</p>
                                        @endif  
                                    @endforeach
                                @endif
                                @else
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="assigned_user_list" id="assigned_user_list">
                                      @if(count($userNameList)>0)
                                      @foreach($userNameList as $key => $value)
                                          <option value="{{$value->id}}" {{(isset($taskDetails) && $taskDetails->user_id == $value->id )?'selected':''}}>{{$value->first_name.' '.$value->last_name. ' (' . $value->email . ') '}}</option>
                                      @endforeach
                                      @endif
                                  </select>  
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Description</label>
                            </div>
                            <div class="col-sm-9">
                                <textarea rows="3" cols="3" name="editTaskDescription" id="editTaskDescription" autocomplete="off" class="form-control">{{(isset($taskDetails->task_desc)?$taskDetails->task_desc:'')}}</textarea>
                            </div>
                        </div>
                    </div>
                    
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" value="Update Task"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#task_start_date').datepicker({
            todayBtn: 1,
            autoclose: true,
            format: 'dd-mm-yyyy',
        }).on('changeDate', function (selected) {
            var endDate = new Date(((selected.date).getFullYear()), ((selected.date).getMonth()+1), 0);
            var minDate = new Date(selected.date.valueOf());
            $('#task_end_date').datepicker('setStartDate', minDate);
            $('#task_end_date').datepicker('setEndDate', endDate);
        });
        $('#task_end_date').datepicker({
            autoclose: true,
            todayBtn: 1,
            format: 'dd-mm-yyyy',
        }).on('changeDate', function (selected) {
            var maxDate = new Date(selected.date.valueOf());
            $('#task_start_date').datepicker('setEndDate', maxDate);
        });  
    });
</script>