<div id="taskTimeEntryModal" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Add project Entry</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>

            <form method="post" action="{{url('save/project/entry')}}" id="saveTaskEntry">
                @csrf
                <input type="hidden" name="projectId" id="projectId" value="">

                @if(!is_null($projectName))
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="true" />
                @else
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="false" />
                @endif
                
                <input type="hidden" name="projectTaskId" id="projectTaskId" value="" />

                <div class="modal-body" id="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <input type="hidden" name="projectTaskId" id="projectTaskId" value="" />
                            <div class="col-sm-3">
                                <label class="col-form-label">Log Date<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" name="taskDate" class="form-control" id="taskDate" autocomplete="off" readonly="readonly" style="width: 220px;" value="{{$log_date}}">
                                        @if ($errors->has('taskDate'))
                                            <div class="error">{{'Please select date'}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Work Time<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-lg-2 col-md-6 col-sm-6">                                        
                                        <select class="form-control select2" name="workTimeHours" id="workTimeHours">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8" selected>8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                        <label id="workTimeHours-error" class="error" for="workTimeHours"></label>
                                        @if ($errors->has('workTimeHours'))
                                            <div class="error">{{'Please select hours'}}</div>
                                        @endif
                                    </div>                                        
                                    <div class="col-lg-2 col-md-6 col-sm-6">
                                        <select class="form-control select2" name="workTimeMinutes" id="workTimeMinutes">
                                            <option value="0">0</option>
                                            <option value="15">15</option>
                                            <option value="30" selected>30</option>
                                            <option value="45">45</option>
                                        </select>
                                        <label id="workTimeMinutes-error" class="error" for="workTimeMinutes"></label>
                                        @if($errors->has('workTimeMinutes'))
                                            <div class="error">{{'Please select minutes'}}</div>
                                        @endif
                                    </div>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-6">
                                <select name="task_list" class="form-control select2" id="task_list">
                                    <option value="0">Select Task</option>
                                </select>
                                 <label id="task_list-error" class="error" for="task_list"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Description</label>
                            </div>
                            <div class="col-sm-9">                                
                                <textarea rows="10" cols="10" name="taskDescription" id="taskDescription" autocomplete="off" class="form-control"></textarea>
                                @if($errors->has('taskDescription'))
                                    <div class="error">{{'Please enter task description'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark projectTaskEntry" value="Save"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>