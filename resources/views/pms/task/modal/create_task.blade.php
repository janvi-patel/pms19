<div id="createTask" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Create Task</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>
            <form action="{{url('save/misc_beanch_task')}}" id="saveTask" method="post">
                @csrf
                <input type="hidden" name="createTaskProjectId" id="createTaskProjectId" value="">
                <div class="modal-body assign_to_team_records" id="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task Name<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="taskName" class="form-control" id="taskName" autocomplete="off" value="{{old('taskName')}}">
                                @if ($errors->has('taskName'))
                                    <div class="error">{{'Please enter task name'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Start Date<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="taskStartDate" class="form-control" id="taskStartDate" autocomplete="off" readonly="readonly" value="{{old('taskStartDate')}}">
                                @if ($errors->has('taskStartDate'))
                                    <div class="error">{{'Please select start date'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">End Date<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="taskEndDate" class="form-control" id="taskEndDate" autocomplete="off" readonly="readonly" value="{{old('taskEndDate')}}">
                                @if ($errors->has('taskEndDate'))
                                    <div class="error">{{'Please select end date'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" value="Create Task"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
