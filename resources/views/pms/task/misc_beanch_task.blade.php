@extends('layouts.master')

@section('moduleName')
    Task Management
@endsection

@section('content')
@php
    $pageRangArray = config('constant.page_range');
@endphp

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                    <input type="hidden" name="redirectAllTask" id="redirectAllTask" value="true" />
                    <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" style="padding:10px;"><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Task Management</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Task</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/view/misc_beanch_task/'.$projectId)}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">Task Name</label>
                                    <input type="text" name="search_by_task_name" id="search_by_task_name" value="{{(isset($request->search_by_task_name)?$request->search_by_task_name:'')}}" class="form-control" autocomplete="off" />
                                </div>
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_start_date" id="search_by_start_date" value="{{(isset($request->search_by_start_date)?$request->search_by_start_date:'')}}" class="form-control" autocomplete="off" readonly />
                                    </div>
                                </div>
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_end_date" id="search_by_end_date" value="{{(isset($request->search_by_end_date)?$request->search_by_end_date:'')}}" class="form-control" autocomplete="off" readonly />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md col-md-4 col-sm-4"></div>
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <a href="javascript:void(0)" id="reset_startdate">Reset</a>
                                </div>
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <a href="javascript:void(0)" id="reset_enddate">Reset</a>
                                </div>
                            </div>
                            @if(Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                             || Auth::user()->hasRole(config('constant.project_manager_slug'))
                             || Auth::user()->designation_id == 16
                             || Auth::user()->hasRole(config('constant.admin')))
                            <div class="row">
                                 <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">User</label>
                                      <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee">
                                        <option value="">All</option>
                                        @foreach($userNameArray as $key => $value)
                                            <option value="{{$key}}" {{ (isset($request->search_by_employee) && $request->search_by_employee == $key) ? 'selected' : ''}}>{{$value}}</option>
                                        @endforeach
                                    </select>  
                                </div>
                                <div class="form-group col-md col-md-4 col-sm-4"></div>
                                <div class="form-group col-md col-md-4 col-sm-4"></div>
                            </div>
                           @endif 
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/view/misc_beanch_task/'.$projectId)}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-1"> 
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-7">
                                {{-- <button name="move_task" type="submit" class="btn btn-primary btn-dark" id="move_task" style="display:none;">
                                Move Task
                                </button> --}}
                            </div>
                            <div class="col-md-4"> 
                                <div class="card-body show_total_no">
                                    Showing {{ $task_list->firstItem() }} to {{ $task_list->lastItem() }} of total {{$task_list->total()}} entries
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive-md">
                                    <table id="projectTaskTable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                {{-- <th class="move_all_task_checkbox">
                                                    <div class="custom-control custom-checkbox">
                                                       <input type="checkbox" id="checkAll" class="custom-control-input">
                                                       <label class="custom-control-label" for="checkAll">Move Task</label>
                                                   </div>
                                               </th> --}}
                                                <th>@sortablelink('task_title','Task Name')</th>
                                                @if(Auth::user()->hasRole(config('constant.team_leader_slug'))
                                                 || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                 || Auth::user()->designation_id == 16
                                                 || Auth::user()->hasRole(config('constant.project_manager_slug'))  
                                                 || Auth::user()->hasRole(config('constant.admin')))
                                                    <th>@sortablelink('emp_name','Name')</th>
                                                @endif
                                                <th>@sortablelink('task_start_date','Start Date')</th>
                                                <th>@sortablelink('task_end_date','End Date')</th>
                                                <th>Logged Hours</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                 $total_logged = 0 ;
                                            ?>
                                            @if(count($task_list) > 0)
                                                @php
                                                    $totalEstimatedHours = $totalLoggedHours = 0.00;    
                                                    $j = $task_list->firstItem();
                                                @endphp
                                                @foreach($task_list as $index => $value)
                                                <?php
                                                     $total_logged = $total_logged + $value->logged_hours;
                                                ?>
                                                    <tr id="{{$value->id}}">
                                                        <td>{{$j}}</td>
                                                        {{-- <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="update_task_row[]" id="update_task_row_{{ $value->id }}" value="{{$value->id }}" class="custom-control-input update_task_row">
                                                                    <label class="custom-control-label" for="update_task_row_{{ $value->id }}"></label>
                                                            </div>
                                                        </td> --}}
                                                        <td>
                                                            <a href="{{url('view/project_misc_task_entry/'.$value->id)}}" title="View Task Entry"  class="a_clickable_link">
                                                                {{$value->task_title}}
                                                            </a>
                                                        </td>
                                                        @if(Auth::user()->hasRole(config('constant.team_leader_slug'))
                                                         || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                         || Auth::user()->hasRole(config('constant.project_manager_slug'))
                                                         || Auth::user()->designation_id == 16
                                                         || Auth::user()->hasRole(config('constant.admin')))
                                                        <td>
                                                            {{$value->emp_name}}
                                                        </td>
                                                        @endif
                                                        <td>
                                                            {{date('d-m-Y',strtotime($value->task_start_date))}}
                                                        </td>
                                                        <td>
                                                            {{date('d-m-Y',strtotime($value->task_end_date))}}
                                                        </td>
                                                        <td>
                                                            {{(new \App\Helpers\CommonHelper)->displayTaskTime($value->logged_hours)}}
                                                        </td>
                                                        <td>
                                                            @if($value->user_id == Auth::user()->id)
                                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit Task" id="miscUpdateProjectTask" data-task-id="{{$value->id}}" data-project-id="{{(!is_null($projectId))?$projectId:$value->project->id}}">
                                                                    <i class="mdi mdi-pencil"></i>
                                                                </a>
                                                            @if(!$value->logged_hours)
                                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete Task" class="delete" id="deleteTask" data-task-id="{{$value->id}}">
                                                                    <i class="mdi mdi-close"></i>
                                                                </a>  
                                                            @endif
                                                            @endif
                                                            @if($value->user_id == Auth::user()->id)
                                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" id="timeEntry" data-task-id="{{$projectId}}" data-proje-task-id="{{$value->id}}">
                                                                    <i class="mdi mdi-alarm"></i>
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                   @php $j++; @endphp
                                                @endforeach
                                                <tr>
                                                    <td></td>
                                                    {{-- <td></td> --}}
                                                    @if(Auth::user()->hasRole(config('constant.team_leader_slug'))
                                                         || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                         || Auth::user()->hasRole(config('constant.project_manager_slug'))
                                                         || Auth::user()->designation_id == 16
                                                         || Auth::user()->hasRole(config('constant.admin')))
                                                    <td></td>
                                                    @endif
                                                    <td></td>
                                                    <td></td>
                                                    <td><b>Total</b></td>
                                                    <td> {{(new \App\Helpers\CommonHelper)->displayTaskTime($total_logged)}}</td>
                                                    <td></td>
                                                </tr>
                                            @else
                                                <tr>
                                                     @if(Auth::user()->hasRole(config('constant.team_leader_slug'))
                                                      || Auth::user()->designation_id == 16
                                                      ||Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                      || Auth::user()->hasRole(config('constant.project_manager_slug'))
                                                      || Auth::user()->hasRole(config('constant.admin')))
                                                        <td colspan="7"><center>No Task Found</center></td>
                                                     @else
                                                        <td colspan="6"><center>No Task Found</center></td>
                                                     @endif
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>

                                @if($task_list && !empty($task_list))
                                    <div class="pt-3">{!! $task_list->appends(\Request::except('page'))->render() !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@include('pms.project_entry.modal.misc_task_time_entry')
<div id="editTask" class="modal fade" role="dialog">
    
</div>
{{-- <div id="moveTaskModal" class="modal fade" role="dialog">
    
</div> --}}
@section('javascript')
<script src="{{asset('js/project_all.js?'.time())}}"></script>
<script src="{{asset('js/myprojects.js?'.time())}}"></script>
<script src="{{asset('js/mytask.js?'.time())}}"></script>
@endsection