@extends('layouts.master')

@section('moduleName')
View Project
@endsection

@php
$pageRangArray = config('constant.page_range');
@endphp

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage"
                    style="padding: 10px 12px;"><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Task Listing</li>
                </ol>
            </div>
        </div>
    </div>
</section>
@include('sweet::alert')
<section class="content">
    <section class="container-fluid">
        @if(!Auth::user()->hasRole(config('constant.developer_slug')))
             <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark collapsed" data-toggle="collapse" data-parent="#accordion"
                    href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Project Tasks</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i
                                class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in collapse @if(Request::get('search_submit')) show @endif">
                    <form class="form-horizontal" method="get" action="" name="timeSheetFilterForm"
                        id="timeSheetFilterForm">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-3 col-sm-3">
                                    <label class="">Employee</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;"
                                        name="search_user" id="search_user">
                                        <option value="">Select Employee</option>
                                        @foreach($project_team_details as $userDetail)
                                        <option value="{{$userDetail->id}}"
                                            {{(isset($request->search_user) && $request->search_user == $userDetail->id)?'selected':''}}>
                                            {{$userDetail->first_name.' '.$userDetail->last_name}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3 col-sm-3">
                                    <label class="">Task Name</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;"
                                        name="search_by_task_name" id="search_by_task_name">
                                        <option value="">Select Task</option>
                                        @foreach($taskDropdown as $taskListDropdown)
                                        <option value="{{$taskListDropdown['id']}}"
                                            {{(isset($request->search_by_task_name) && $request->search_by_task_name == $taskListDropdown['id'])?'selected':''}}>
                                            {{$taskListDropdown['task_name']}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3 col-sm-3">
                                    <label class="">Task Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_start_date" id="search_by_start_date" value="{{(isset($request->search_by_start_date)?$request->search_by_start_date:'')}}" class="form-control" readonly />
                                    </div>
                                    <a href="javascript:void(0)" id="reset_startdate">Reset</a>
                                </div>
                                <div class="form-group col-md-3 col-sm-3">
                                    <label class="">Task End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_end_date" id="search_by_end_date" value="{{(isset($request->search_by_end_date)?$request->search_by_end_date:'')}}"  class="form-control" readonly />
                                    </div>
                                    <a href="javascript:void(0)" id="reset_enddate">Reset</a>
                                </div>
                            </div>
                            <div class='row'>
                                <div class="form-group col-md-3 col-sm-3">
                                    <label class="">Task Description</label>
                                    <input type="text" name="search_task_desc" class="form-control" value="{{$request->search_task_desc}}">
                                </div>
                            </div>
                        </div>
                            <div class="card-footer">
                                <button name="search_submit" value="1" type="submit"
                                    class="btn btn-primary btn-dark">
                                    Search
                                </button>
                                <button name="search_reset" type="reset" class="btn btn-info btn-secondary"
                                    onclick="location.href ='{{url('view/task/list/'.$request->id)}}'">
                                    Reset
                                </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3 d-flex w-100 justify-content-between flex-column flex-sm-row">
                                <div class="left d-flex">
                                    <select class="select2 form-control custom-select page_range_dropdown min_width_70"
                                        name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                        @foreach ($pageRangArray as $key => $node)
                                        <option value="{{$key}}"
                                            {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}>
                                            {{ $node }}</option>
                                        @endforeach
                                        @endif
                                    </select>

                                    <div class="pl-3"> Showing {{ $getProjectTask->firstItem() }} to
                                        {{ $getProjectTask->lastItem() }} of total {{$getProjectTask->total()}} entries
                                    </div>
                                </div>
                                @if((Auth::check() && Auth::user()->hasPermission('projects.task.listing')))
                                    @if($getProjectData->project_name != 'Miscellaneous Tasks' && $getProjectData->project_name != 'Bench')
                                        <div class="right mt-3 mt-sm-0">
                                            <a href="javascript:void(0);" title="Add Task"
                                                class="btn btn-primary btn-dark btn-block padding-5 addProjectTaskBtn"
                                                data-project-id='{{isset($request)?$request->id:''}}'>Add Task</a>
                                        </div>
                                    @else
                                        @if($getProjectData->project_name == 'Miscellaneous Tasks' || $getProjectData->project_name == 'Bench')
                                        @if (Auth::user()->hasRole(config('constant.superadmin_slug'))
                                                || Auth::user()->hasRole(config('constant.admin_slug'))
                                                || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                                <div class="right mt-3 mt-sm-0">
                                                    <select class="select2 form-control custom-select move_checked_task" name="move_checked_task" id="move_checked_task" style="width: 100%; height:36px;"  >
                                                        <option value="">Select Action</option>
                                                        <option value="1">Move Task</option>
                                                    </select>
                                                </div>
                                            @endif
                                        @endif
                                    @endif
                                @else
                                    @if($getProjectData->project_name == 'Miscellaneous Tasks' || $getProjectData->project_name == 'Bench')
                                        <div class="right mt-3 mt-sm-0">
                                            <a href="javascript:void(0);" title="Add Task" class="btn btn-primary btn-dark btn-block padding-5 addProjectTaskBtn" data-project-id='{{isset($request)?$request->id:''}}'>Add Task</a>
                                        </div>
                                        @else
                                    @endif
                                @endif
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive-md">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                            <tr>
                                                <th style="width: 20%">Project Name</th>
                                                <td>
                                                    {{isset($getProjectData)&& $getProjectData->project_name !=  ''?$getProjectData->project_name:''}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Project Hours</th>
                                                <td>
                                                    <?php
                                                        $getProjectHr = getProjectPmsHours($projectId);
                                                        $projectHour =  sprintf ("%.2f",$getProjectHr['pms_hours'] + $getProjectHr['total_cr_hours']); ?>
                                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime($projectHour)}}

                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Assigned Hours</th>
                                                <td>
                                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime(getProjectTaskSum(['column' => "estimated_hours", "where"=> ["project_id" => $projectId],"whereIn" => ["column" => "id","array" => $data['projectTaskId']]]))}}

                                                </td>
                                            </tr>
                                            <tr>
                                                <th> Total Logged Hours</th>
                                                <td>
                                                    {{ (new \App\Helpers\CommonHelper)->displayTaskTime(getProjectEntrySum(['column' => "log_hours", "whereIn" => ["column" => "task_id","array" => $data['projectTaskId']]])) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th> Project Efficiency</th>
                                                @php
                                                    $allTaskList = getProjectTaskList(['select' => "id", "where"=> ["project_id" => $projectId], 'pluck' => "id", "whereNull" => "deleted_at"]);
                                                    $allLoggedHoursSum = getProjectEntrySum(['column' => "log_hours", "whereIn" => ["column" => "task_id","array" => $allTaskList], "whereNull" => "deleted_at"]);
                                                    $totalProjectEfficiency = 0;
                                                    if($allLoggedHoursSum != 0.00 && $projectHour != 0.00){
                                                        $totalProjectEfficiency =  ($allLoggedHoursSum/$projectHour)*100;
                                                    }
                                                @endphp
                                                <td class="{{ (sprintf("%.2f",$totalProjectEfficiency) <=100) ? "project_completion_gree" : "project_completion_red" }}">
                                                    {{sprintf("%.2f",$totalProjectEfficiency).' %'}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th> Project Completion</th>
                                                @php
                                                    $allTaskHoursSum = getProjectTaskSum(['column' => "estimated_hours", "whereIn" => ["column" => "id","array" => $allTaskList], "whereNull" => "deleted_at"]);
                                                    $totalProjectCompletion = 0;
                                                    if($allTaskHoursSum != 0.00 && $projectHour != 0.00){
                                                        $totalProjectCompletion =  ($allTaskHoursSum/$projectHour)*100;
                                                    }
                                                @endphp
                                                <td class="{{ (sprintf("%.2f",$totalProjectCompletion) <=100) ? "project_completion_gree" : "project_completion_red" }}">
                                                    {{sprintf("%.2f",$totalProjectCompletion).' %'}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="holiday_listing" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Project / Cr Name</th>
                                                @if($getProjectData->project_name == 'Miscellaneous Tasks' || $getProjectData->project_name == 'Bench')
                                                    @if (Auth::user()->hasRole(config('constant.superadmin_slug'))
                                                            || Auth::user()->hasRole(config('constant.admin_slug'))
                                                            || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                            || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                                        <th class="move_all_task_checkbox">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" id="checkAll" class="custom-control-input">
                                                                <label class="custom-control-label" for="checkAll"></label>
                                                            </div>
                                                        </th>
                                                    @endif
                                               @endif
                                                <th>Task Name</th>
                                                @if((Auth::check() && Auth::user()->hasPermission('projects.task.listing')))
                                                    <th>Description</th>
                                                @endif
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                @if((Auth::check() && Auth::user()->hasPermission('projects.task.listing')))
                                                    <th>User Name</th>
                                                @endif
                                                <th>Assigned hours</th>
                                                <th>Logged Hours</th>
                                                <th>Task Efficiency</th>
                                                <th>CR / Project Efficiency</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($getProjectTask) > 0 )
                                            @php
                                                $j = $getProjectTask->firstItem();
                                                $in_array = array();
                                            @endphp
                                            @foreach($getProjectTask as $getProjectTaskVal)
                                                <tr>
                                                @php
                                                    $userData = (new \App\Helpers\CommonHelper)->getAnyUserById($getProjectTaskVal->user_id);
                                                    $estimatedHours = isset($getProjectTaskVal->estimated_hours) && $getProjectTaskVal->estimated_hours > 0 ? $getProjectTaskVal->estimated_hours : 0;
                                                    $loggedHours = isset($getProjectTaskVal->estimated_hours) && $getProjectTaskVal->logged_hours > 0 ? $getProjectTaskVal->logged_hours : 0;
                                                    $efficiency = 0;
                                                    if($loggedHours != 0.00 && $estimatedHours != 0.00){
                                                        $efficiency =  ($loggedHours/$estimatedHours)*100;
                                                    }
                                                @endphp
                                                    @if(!in_array($getProjectTaskVal->cr_id,$in_array))
                                                        <td rowspan="{{ $data[$getProjectTaskVal->cr_id] }}">{{$j}}</td>
                                                        @if($getProjectTaskVal->cr_id != 0)
                                                            <td rowspan="{{ $data[$getProjectTaskVal->cr_id] }}">{{ isset($getProjectTaskVal->getCr->title) ? $getProjectTaskVal->getCr->title : "" }}</td>
                                                        @else
                                                            <td rowspan="{{ $data[$getProjectTaskVal->cr_id] }}">{{ $getProjectData->project_name}}</td>
                                                        @endif
                                                        @php
                                                            $j++;
                                                        @endphp
                                                    @endif
                                                        @if($getProjectData->project_name == 'Miscellaneous Tasks' || $getProjectData->project_name == 'Bench')
                                                            @if (Auth::user()->hasRole(config('constant.superadmin_slug'))
                                                                || Auth::user()->hasRole(config('constant.admin_slug'))
                                                                || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                                || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                                                <td>
                                                                    @if($getProjectTaskVal->task_start_date != null && $getProjectTaskVal->task_end_date != null)
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" name="update_task_row[]" id="update_task_row_{{ $getProjectTaskVal->id }}" value="{{$getProjectTaskVal->id }}" class="custom-control-input update_task_row">
                                                                            <label class="custom-control-label" for="update_task_row_{{ $getProjectTaskVal->id }}"></label>
                                                                        </div>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        @endif
                                                            <td><a href="{{url('view/project_task_entry/'.$getProjectTaskVal->id)}}" title="View Task Entry" class="a_clickable_link">{{$getProjectTaskVal->task_name}}</a></td>
                                                        @if((Auth::check() && Auth::user()->hasPermission('projects.task.listing')))
                                                            <td>{{$getProjectTaskVal->task_desc}}</td>
                                                        @endif
                                                            <td>{{$getProjectTaskVal->task_start_date == '' ?"":date('d-m-Y',strtotime($getProjectTaskVal->task_start_date))}}</td>
                                                            <td>{{$getProjectTaskVal->task_end_date == '' ?"":date('d-m-Y',strtotime($getProjectTaskVal->task_end_date))}}</td>
                                                        @if((Auth::check() && Auth::user()->hasPermission('projects.task.listing')))
                                                            <td>{{($userData)?$userData->first_name.' '.$userData->last_name:''}}</td>
                                                        @endif
                                                        <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($estimatedHours)}} </td>
                                                        <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($loggedHours) }}</td>
                                                        <td>
                                                            @if(sprintf("%.2f",$efficiency) <= 100) <span class="project_completion_gree">
                                                                {{sprintf("%.2f",$efficiency).' %'}}
                                                                </span>
                                                            @else
                                                                <span class="project_completion_red">
                                                                    {{sprintf("%.2f",$efficiency).' %'}}
                                                                </span>
                                                            @endif
                                                        </td>
                                                        @if(!in_array($getProjectTaskVal->cr_id,$in_array))
                                                            <td rowspan="{{ $data[$getProjectTaskVal->cr_id] }}">
                                                                @php
                                                                    $in_array[] = $getProjectTaskVal->cr_id;
                                                                    $crEstimatedHours = getProjectTaskSum(['column' => "estimated_hours", "where"=> ["project_id" => $projectId,'cr_id' => $getProjectTaskVal->cr_id], "whereIn" => ["column" => "id", "array" => $data["task_id"]]]);
                                                                    $projectTaskList = getProjectTaskList(['select' => "id", "where"=> ["project_id" => $projectId,'cr_id' => $getProjectTaskVal->cr_id], "whereIn" => ["column" => "id", "array" => $data["task_id"]], 'pluck' => "id", "whereNull" => "deleted_at"]);
                                                                    $crLoggedHours = getProjectEntrySum(['column' => "log_hours", "whereIn" => ["column" => "task_id","array" => $projectTaskList], "whereNull" => "deleted_at"]);
                                                                    $crEfficiency = 0;
                                                                    if($crLoggedHours != 0.00 && $crEstimatedHours != 0.00){
                                                                        $crEfficiency = ($crLoggedHours/$crEstimatedHours)*100;
                                                                    }
                                                                @endphp
                                                                <span class="{{ (sprintf("%.2f",$crEfficiency) <=100) ? "project_completion_gree" : "project_completion_red" }}">
                                                                    {{sprintf("%.2f",$crEfficiency).' %'}}
                                                                </span>
                                                            </td>
                                                        @endif
                                                        <td>
                                                            @if ((($userData) && $userData->status == 1)
                                                                && ((date('Y-m',strtotime($getProjectTaskVal->task_start_date)) >= date("Y-m",strtotime("-1 month")))
                                                                && (strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d")) || (date('Y-m',strtotime($getProjectTaskVal->task_start_date)) == date("Y-m"))))
                                                                && $getProjectTaskVal->task_start_date != '')
                                                                @if ((Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                                    || Auth::user()->hasRole(config('constant.project_manager_slug'))
                                                                    || Auth::user()->hasRole(config('constant.team_leader_slug')))
                                                                    || (($getProjectData->project_name == 'Miscellaneous Tasks' || $getProjectData->project_name == 'Bench') && $getProjectTaskVal->user_id == auth()->user()->id))
                                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit Project Task" class="editProjectTask " data-task-id="{{$getProjectTaskVal->id}}"><i class="mdi mdi-pencil"></i></a>
                                                                    @if(in_array($getProjectTaskVal->id, $getDeletebleTaskId))
                                                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete Task" class="delete deleteProjectTask" data-task-id="{{$getProjectTaskVal->id}}"><i class="mdi mdi-close"></i></a>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @if (Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                            Auth::user()->hasRole(config('constant.admin_slug')))
                                                            @if($getProjectTaskVal->task_start_date != null && $getProjectTaskVal->task_end_date != null)
                                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Edit Project Task" class="editProjectTask " data-task-id="{{$getProjectTaskVal->id}}">
                                                                    <i class="mdi mdi-pencil"></i>
                                                                </a>
                                                            @endif
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete Task" class="delete deleteProjectTask" data-task-id="{{$getProjectTaskVal->id}}">
                                                                <i class="mdi mdi-close"></i>
                                                            </a>
                                                        @endif
                                                        @if($getProjectTaskVal->user_id == auth()->user()->id)
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" class="timeEntry" data-task-id="{{$getProjectTaskVal->project_id}}" data-id='{{$getProjectTaskVal->id}}'>
                                                                <i class="mdi mdi-alarm"></i>
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                @if((Auth::check() && Auth::user()->hasPermission('projects.task.listing')))
                                                    <td></td>
                                                @endif
                                                <td></td>
                                                @if((Auth::check() && Auth::user()->hasPermission('projects.task.listing')))
                                                    <td></td>
                                                @endif
                                                <td><b>Total Hours</b></td>
                                                @if($getProjectData->project_name == 'Miscellaneous Tasks' || $getProjectData->project_name == 'Bench')
                                                    @if (Auth::user()->hasRole(config('constant.superadmin_slug'))
                                                            || Auth::user()->hasRole(config('constant.admin_slug'))
                                                            || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                                                            || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                                            <td></td>
                                                        @endif
                                                @endif
                                                <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime(getProjectTaskSum(['column' => "estimated_hours", "where"=> ["project_id" => $projectId], "whereIn" => ["column" => "id","array" => $data['projectListedTaskId']]])) }}</b></td>
                                                <td><b>{{ (new \App\Helpers\CommonHelper)->displayTaskTime(getProjectEntrySum(['column' => "log_hours", "whereIn" => ["column" => "task_id","array" => $data['projectListedTaskId']]])) }}</b></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @else
                                            <tr>
                                                <td colspan='12'>
                                                    <center>No Task Found</center>
                                                </td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if($getProjectTask && !empty($getProjectTask))
                                    <div class="pt-3">{!! $getProjectTask->appends(\Request::except('page'))->render() !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@include('pms.task.modal.time_entry_modal')
@endsection
<div id="editProjectTaskModal" class="modal fade" role="dialog">

</div>
<div id="moveTaskModal" class="modal fade" role="dialog">
</div>
<div id="addProjectTaskModal" class="modal fade" role="dialog">

</div>
@section('javascript')
<script src="{{asset('js/projecttask.js?'.time())}}"></script>
<script src="{{asset('js/project.js?'.time())}}"></script>
<script src="{{asset('js/task-time-entry.js?'.time())}}"></script>
@endsection
