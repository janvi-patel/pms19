@extends('layouts.master')

@section('moduleName')
    Task Listing
@endsection

@section('content')
@php
    $pageRangArray = config('constant.page_range');
@endphp

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Task Listing</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Task</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/view/mytask/list')}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Task Name</label>
                                    <input type="text" name="search_task_name" class="form-control" value="{{$request->search_task_name}}">
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Task Start date</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_task_start_date" id="search_task_start_date" 
                                               value="{{($request->search_task_start_date ==   '')?date('d-m-Y',strtotime($getStartDate)):$request->search_task_start_date}}" 
                                               class="form-control" readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Task End date</label>
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_task_end_date" id="search_task_end_date"
                                               value="{{($request->search_task_end_date ==   '')?date('d-m-Y',strtotime($getEndDate)):$request->search_task_end_date}}" 
                                               class="form-control" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Project Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_name" id="search_by_project_name">
                                        <option value="">Select</option>
                                        @if(!empty($getProjectList))      
                                            @foreach($getProjectList as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_project_name) && ($request->search_by_project_name == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>  
                                </div>  
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Task Description</label>
                                    <input type="text" name="search_task_desc" class="form-control" value="{{$request->search_task_desc}}">
                                </div>
                            </div>
                        </div>
                       
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/view/mytask/list')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex align-items-center justify-content-between">
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                                @if(count($getProjectData)>0)
                                <div class=" show_total_no float-right pl-3">
                                    Showing {{ $getProjectData->firstItem() }} to {{ $getProjectData->lastItem() }} of total {{$getProjectData->total()}} entries
                                </div> 
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="holiday_listing" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@sortablelink('taskName','Task Name')</th>
                                                <th>@sortablelink('projectName','Project Name')</th>
                                                <th>@sortablelink('assignedHours','Assigned hours')</th>
                                                <th>@sortablelink('logged_hours','Logged hours')</th>
                                                <th>@sortablelink('task_start_date','Start Date')</th>
                                                <th>@sortablelink('task_end_date','End Date')</th>
                                                <th>Task Efficiency</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($getProjectData) > 0 )
                                            @php
                                            $j = $getProjectData->firstItem();
                                            $efficiency = $assignedHours = $logged_hours = 0.00;
                                            @endphp
                                            @foreach($getProjectData as $getProjectDataVal)
                                            @php 
                                                $assignedHours = (isset($getProjectDataVal->assignedHours) && $getProjectDataVal->assignedHours!='')?$getProjectDataVal->assignedHours:'0.00'; 
                                                $loggedHours = ($getProjectDataVal->logged_hours)?$getProjectDataVal->logged_hours:'0.00';
                                                if($loggedHours != 0.00 && $assignedHours != 0.00){
                                                    $efficiency =  ($loggedHours/$assignedHours)*100;
                                                }
                                                else {
                                                    $efficiency =  0;
                                                }
                                            @endphp
                                            <tr>
                                                <td>{{$j}}</td>
                                                <td ><a href="{{url('view/project_task_entry/'.$getProjectDataVal->id)}}" title="View Task Entry"  class="a_clickable_link">
                                                        {{$getProjectDataVal->taskName}}</a></td>
                                                <td>{{$getProjectDataVal->projectName}}</td>
                                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($assignedHours)}} </td>
                                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($loggedHours)}} </td>
                                                <td>{{($getProjectDataVal->task_start_date != '')?date('d-m-Y',strtotime($getProjectDataVal->task_start_date)):''}} </td>
                                                <td>{{($getProjectDataVal->task_end_date != '')?date('d-m-Y',strtotime($getProjectDataVal->task_end_date)):''}} </td>
                                                <td>
                                                    @if(sprintf("%.2f",$efficiency) <= 100)
                                                        <span class="project_completion_gree">
                                                            {{sprintf("%.2f",$efficiency).' %'}}
                                                        </span>
                                                    @else
                                                        <span class="project_completion_red">
                                                            {{sprintf("%.2f",$efficiency).' %'}}
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" 
                                                       class="timeEntry" data-task-id="{{$getProjectDataVal->project_id}}" data-id='{{$getProjectDataVal->id}}'>
                                                        <i class="mdi mdi-alarm"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @php $j++;  @endphp
                                            @endforeach
                                            @else
                                                <tr>
                                                    <td colspan='9'><center>No Project Found</center></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if($getProjectData && !empty($getProjectData))
                                    <div class="pt-3">{!! $getProjectData->appends(\Request::except('page'))->render() !!}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('pms.task.modal.time_entry_modal')
@endsection

@section('javascript')
<script src="{{asset('js/projecttask.js?'.time())}}"></script>
<script src="{{asset('js/project.js?'.time())}}"></script>
<script src="{{asset('js/task-time-entry.js?'.time())}}"></script>
@endsection