@extends('layouts.master')

@section('moduleName')
View Project
@endsection

@php
$pageRangArray = config('constant.page_range');
@endphp

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage"
                    style="padding: 10px 12px;"><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Task Info</li>
                </ol>
            </div>
        </div>
    </div>
</section>
@include('sweet::alert')
<?php 
$search_class = "";
?>
@if(Request::get('search_submit'))
   <?php 
       $search_class = "show";
   ?>
@endif
<section class="content">
    <section class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-3 d-flex w-100 justify-content-between flex-column flex-sm-row">
                                <div class="left d-flex">
                                    <select class="select2 form-control custom-select page_range_dropdown min_width_70"
                                        name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                        @foreach ($pageRangArray as $key => $node)
                                        <option value="{{$key}}"
                                            {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}>
                                            {{ $node }}</option>
                                        @endforeach
                                        @endif
                                    </select>

                                    <div class="pl-3"> Showing {{ $getProjectTask->firstItem() }} to
                                        {{ $getProjectTask->lastItem() }} of total {{$getProjectTask->total()}} entries
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="table-responsive-md">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                            <tr>
                                                <th style="width: 20%">Project Name</th>
                                                <td>
                                                    {{isset($getProjectData)&& $getProjectData->project_name !=  ''?$getProjectData->project_name:''}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Project Hours</th>
                                                <td>
                                                    @php
                                                    $totalProjectHours = 0;
                                                    $totalProjectHours = $getProjectData->total_project_hours + $getProjectData->total_cr_hours;
                                                    @endphp
                                                    {{isset($getProjectData)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalProjectHours):'00:00'}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Assigned Hours</th>
                                                <td>
                                                    @php
                                                    $totalHours = $estimatedHours = 0;
                                                    @endphp
                                                    @foreach($getTotalProjectTask as $val)
                                                    @php
                                                    $estimatedHours = (isset($val->estimated_hours) &&
                                                    $val->estimated_hours!='')?$val->estimated_hours:'0.00';
                                                    $totalHours = $totalHours + $estimatedHours;
                                                    @endphp
                                                    @endforeach
                                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime($totalHours)}}

                                                </td>
                                            </tr>
                                            <tr>
                                                <th> Total Logged Hours</th>
                                                <td>
                                                    {{isset($loggedHours)?(new \App\Helpers\CommonHelper)->displayTaskTime($loggedHours->total_logged_hours):'00:00'}}

                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="holiday_listing" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@sortablelink('task_name','Task Name')</th>
                                                <th>@sortablelink('task_start_date','Start Date')</th>
                                                <th>@sortablelink('task_end_date','End Date')</th>
                                                @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                                    <th>@sortablelink('users.first_name','User Name')</th>
                                                @endif
                                                <th>@sortablelink('estimated_hours','Assigned Hours')</th>
                                                <th>@sortablelink('total_logged_hours','Logged Hours')</th>
                                                <th>Task Efficiency</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $j = 1; 
                                                $totalHours = $estimatedHours = $totalLoggedHours = $loggedHours =  $oldLoggedHours = $efficiency =  0;
                                            @endphp
                                            @if(count($getProjectTask) > 0 || $oldEntry)
                                            @php
                                            $j = $getProjectTask->firstItem();
                                            @endphp
                                            @foreach($getProjectTask as $getProjectTaskVal)
                                                @php
                                                $userData = (new \App\Helpers\CommonHelper)->getAnyUserById($getProjectTaskVal->user_id);
                                                $estimatedHours = (isset($getProjectTaskVal->estimated_hours) &&
                                                $getProjectTaskVal->estimated_hours!='')?$getProjectTaskVal->estimated_hours:0;
                                                $totalHours = $totalHours + $estimatedHours;

                                                $loggedHours = (isset($getProjectTaskVal->total_logged_hours) &&
                                                                $getProjectTaskVal->total_logged_hours!='')?$getProjectTaskVal->total_logged_hours:0;
                                                
                                                $totalLoggedHours = $totalLoggedHours + $loggedHours;
                                                if($loggedHours != 0 && $estimatedHours != 0){
                                                    $efficiency = ($loggedHours/ $estimatedHours)*100;
                                                }
                                                else {
                                                    $efficiency =  0;
                                                }
                                                @endphp
                                            <tr>
                                                <td>{{$j}}</td>
                                                <td><a href="{{url('view/project_task_entry/'.$getProjectTaskVal->id)}}" title="View Task Entry"  class="a_clickable_link">{{$getProjectTaskVal->task_name}}</a></td>
                                                <td>{{$getProjectTaskVal->task_start_date == '' ?"":date('d-m-Y',strtotime($getProjectTaskVal->task_start_date))}}</td>
                                                <td>{{$getProjectTaskVal->task_end_date == '' ?"":date('d-m-Y',strtotime($getProjectTaskVal->task_end_date))}}</td>
                                                @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                                    <td>{{($userData)?$userData->first_name.' '.$userData->last_name:''}}</td>
                                                @endif
                                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getProjectTaskVal->estimated_hours)}}</td>
                                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getProjectTaskVal->total_logged_hours)}} </td>
                                                <td>
                                                    @if(sprintf("%.2f",$efficiency) <= 100)
                                                        <span class="project_completion_gree">
                                                            {{sprintf("%.2f",$efficiency).' %'}}
                                                        </span>
                                                    @else
                                                        <span class="project_completion_red">
                                                            {{sprintf("%.2f",$efficiency).' %'}}
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                @if($getProjectTaskVal->user_id == Auth::user()->id)
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" 
                                                    class="timeEntry" data-task-id="{{$getProjectTaskVal->project_id}}" data-id='{{$getProjectTaskVal->id}}'>
                                                     <i class="mdi mdi-alarm"></i>
                                                 </a>
                                                @endif
                                                @if((Auth::user()->hasRole(config('constant.developer_slug')) && 
                                                        (($getProjectData->project_name == 'Miscellaneous Tasks' || $getProjectData->project_name == 'Bench')))||
                                                    (Auth::user()->hasRole(config('constant.associative_project_manager_slug')) 
                                                        || Auth::user()->hasRole(config('constant.project_manager_slug'))
                                                        || Auth::user()->hasRole(config('constant.team_leader_slug')
                                                        ||Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                                            Auth::user()->hasRole(config('constant.admin_slug')))
                                                    ))
                                                    @if ((($userData) && $userData->status == 1)
                                                        && ((date('Y-m',strtotime($getProjectTaskVal->task_start_date)) >= date("Y-m",strtotime("-1 month")))
                                                            && (strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d")) || (date('Y-m',strtotime($getProjectTaskVal->task_start_date)) == date("Y-m"))))
                                                            && $getProjectTaskVal->task_start_date != '')

                                                        <a href="javascript:void(0);" data-toggle="tooltip"
                                                            data-placement="top" title="Edit Project Task"
                                                            class="editProjectTask"
                                                            data-task-id="{{$getProjectTaskVal->id}}">
                                                            <i class="mdi mdi-pencil"></i>
                                                        </a>
                                                        @if(in_array($getProjectTaskVal->id, $getDeletebleTaskId))
                                                            <a href="javascript:void(0);" data-toggle="tooltip"
                                                            data-placement="top" title="Delete Task"
                                                            class="delete deleteProjectTask"
                                                            data-task-id="{{$getProjectTaskVal->id}}">
                                                            <i class="mdi mdi-close"></i></a>
                                                        @endif
                                                    @endif
                                                    @if((Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                                        Auth::user()->hasRole(config('constant.admin_slug'))))
                                                        <a href="javascript:void(0);" data-toggle="tooltip"
                                                        data-placement="top" title="Delete Task"
                                                        class="delete deleteProjectTask"
                                                        data-task-id="{{$getProjectTaskVal->id}}">
                                                        <i class="mdi mdi-close"></i></a>
                                                    @endif
                                                @endif
                                                </td>
                                            </tr>
                                            @php
                                            $j++;
                                            @endphp
                                            @endforeach
                                            
                                            @if($oldEntry)
                                            @php 
                                                $oldLoggedHours = (isset($oldEntry->total_logged_hours) &&
                                                                $oldEntry->total_logged_hours!='')?$oldEntry->total_logged_hours:'0.00';
                                            @endphp
                                            <tr>
                                                <td>{{($j == '')?1:$j}}</td>
                                                <td><a href="{{url('view/project_entry/'.$oldEntry->project_id.'/'.Auth::user()->id)}}" title="View Project Entries"  class="a_clickable_link">Old Entry</a></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                                <td></td>
                                                @endif
                                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($oldEntry->total_logged_hours)}}</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                                <td></td>
                                                @endif
                                                <td><b>Total Hours</b></td>
                                                <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalHours)}}</b> </td>
                                                <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalLoggedHours+$oldLoggedHours)}}</b>  </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @else
                                            @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                                <tr>
                                                    <td colspan='8'>
                                                        <center>No Task Found</center>
                                                    </td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td colspan='9'>
                                                        <center>No Task Found</center>
                                                    </td>
                                                </tr>
                                            @endif
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if($getProjectTask && !empty($getProjectTask))
                                <div class="pt-3">{!! $getProjectTask->appends(\Request::except('page'))->render() !!}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@include('pms.task.modal.time_entry_modal')
@endsection
<div id="editProjectTaskModal" class="modal fade" role="dialog">

</div>
<div id="addProjectTaskModal" class="modal fade" role="dialog">

</div>
@section('javascript')
<script src="{{asset('js/projecttask.js?'.time())}}"></script>
<script src="{{asset('js/project.js?'.time())}}"></script>
<script src="{{asset('js/task-time-entry.js?'.time())}}"></script>
@endsection