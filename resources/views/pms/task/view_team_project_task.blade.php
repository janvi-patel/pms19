@extends('layouts.master')

@section('moduleName')
   Task Entries
@endsection

@php
    $pageRangArray = config('constant.page_range');
@endphp

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage"><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Team Project Entries</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Project Entry</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <?php // echo "<prE>";print_R($request->all());exit;?>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="" name="search_filter" id="search_filter">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">Task Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_log_from_date" id="search_by_log_from_date" value="{{(isset($request->search_by_log_from_date)?$request->search_by_log_from_date:'')}}" class="form-control" readonly />
                                    </div>
                                </div>
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">Task End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_log_to_date" id="search_by_log_to_date" value="{{(isset($request->search_by_log_to_date)?$request->search_by_log_to_date:'')}}" class="form-control" readonly />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary"
                                onclick="location.href ='{{url('view/user_project_entry/'.$projectId.'/'.$userId)}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-1 mb-3"> 
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive-md">
                                    <table id="taskEntryTable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@sortablelink('task_name','Task Name')</th>
                                                <th>@sortablelink('task_desc','Description')</th>
                                                <th>@sortablelink('task_start_date','Start Date')</th>
                                                <th>@sortablelink('task_end_date','End Date')</th>
                                                <th>@sortablelink('total_logged_hours','Logged Hours')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php  $total_logged = $oldLoggedHours = 0 ; @endphp
                                            @if(count($projectEntry) > 0 || $oldEntry)
                                                @php $j = $projectEntry->firstItem();@endphp
                                                @foreach($projectEntry as $index => $value)
                                                 @php  $total_logged = $total_logged + $value->total_logged_hours; @endphp
                                                    <tr id="{{$value->id}}">
                                                        <td>{{$j}}</td>
                                                        <td><a href="{{url('view/project_task_entry/'.$value->id)}}" title="View Task Entry"  class="a_clickable_link">{{$value->task_name}}</a></td>
                                                        <td>{{$value->task_desc}}</td>
                                                        <td>{{$value->task_start_date == '' ?"":date('d-m-Y',strtotime($value->task_start_date))}}</td>
                                                        <td>{{$value->task_end_date == '' ?"":date('d-m-Y',strtotime($value->task_end_date))}}</td>
                                                        <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($value->total_logged_hours)}}
                                                    </tr>
                                                    @php
                                                    $j++;
                                                    @endphp
                                                @endforeach
                                                @if($oldEntry)
                                                @php 
                                                    $oldLoggedHours = (isset($oldEntry->total_logged_hours) &&
                                                                $oldEntry->total_logged_hours!='')?$oldEntry->total_logged_hours:'0.00';
                                                @endphp
                                                <tr>
                                                    <td>{{($j == '')?1:$j}}</td>
                                                    <td><a href="{{url('view/project_entry/'.$oldEntry->project_id.'/'.$userId)}}" title="View Project Entries"  class="a_clickable_link">Old Entry</a></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($oldEntry->total_logged_hours)}}</td>
                                                </tr>
                                                @endif
                                                 <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><b>Total</b></td>
                                                    <td> {{(new \App\Helpers\CommonHelper)->displayTaskTime($total_logged+$oldLoggedHours)}}</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td colspan="7"><center>No Project Entry Found</center></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                @if($projectEntry && !empty($projectEntry))
                                    <div class="pt-3">
                                        {!! $projectEntry->appends(\Request::except('page'))->render() !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="editTaskEntryDiv">
    @include('pms.project_entry.modal.edit_task_time_entry')
</div>
@endsection

@section('javascript')
<script src="{{asset('js/task_entry.js?'.time())}}"></script>
@endsection