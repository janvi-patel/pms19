@extends('layouts.master')
@section('content')

@section('moduleName')
    Documents
@endsection

@php
$pageRangArray = config('constant.page_range');
$statusArray = config('constant.status');
@endphp
@include('sweet::alert')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pms Documents</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>     
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
 <section class="content">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">              
                    <div class="card-body">
                        <div class="row top-top-row">
                            <div class="col-sm-6 col-6 mb-3 pl-0 ">
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown">
                                        @if(is_array($pageRangArray))
                                        @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>                           
                            </div>
                            <div class="col-6">
                                <div class="text-right">
                                    <span class="btn btn-dark float-right techModal" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Add New Document">Upload New Document</span> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @if(count($data)>0)
                                    <table id="designation_listing" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                            <th style="width: 10px">No</th>
                                            <th>@sortablelink('name','File Name')</th>
                                            <th style="width: 159px;" class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                        $i = 1;
                                        $j = $data->firstItem();
                                        @endphp 
                                        @foreach($data as $val)
                                            <tr id ="{{$val['id']}}">
                                                <td >{{$j}}</td>
                                                <td class="text-capitalize"><a href="{{asset('upload/pms_docs/'.$val['file_name'])}}" data-toggle="tooltip" data-trigger="hover" data-content="Download" >{{$val['name']}}</a></td>
                                                <td align="center">
                                                    <span  data-toggle="tooltip" style="cursor: pointer;" data-placement="top" data-trigger="hover" docs-id ="{{$val['id']}}" class="editDocsModal" data-content="Update">
                                                        <i class="mdi mdi-pencil"></i>
                                                    </span>
                                                    <span data-toggle="tooltip" style="cursor: pointer;" data-placement="top" data-trigger="hover" docs-id="{{$val['id']}}" class='deleteTech' data-content="Delete">
                                                        <i class="mdi mdi-close"></i>
                                                    </span>
                                                </td>
                                            </tr>
                                            @php
                                                $i++;$j++;
                                            @endphp 
                                        @endforeach 
                                        </tbody>
                                    </table>
                                @else
                                <div class="border-top">
                                    <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                                </div> 
                                @endif
                            </div>
                        </div>
                        @if($data && !empty($data))
                        <div class="pt-4 ">{!! $data->appends(\Request::except('page'))->render() !!}</div>
                        @endif                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addDocsModal" role="dialog"></div>
    @include('pms.documents.modal.edit_documents_modal')
</section>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- this page js -->
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
@endsection

@section('javascript')
   <script src="{{asset('js/docs.js?'.time())}}"></script>
@endsection