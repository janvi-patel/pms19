<div class="modal fade" id="editDocsModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editDocsModal">Update Documents</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editDocs" name="editDocs">
                <div class="modal-body">
                    <input type="hidden" name="id" id='id'>
                    <div class="form-group">
                        <label for="edit_name">Name<span class="error">*</span></label><label class="ml-2 edit_name-error error" for="edit_name"></label>
                        <input type="text" name="edit_name" value="" class="form-control edit_name" id="edit_name">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-dark editDocuments" value="Update">
                    <span class="btn btn-secondary" data-dismiss="modal">Close</span>
                </div>
            </form>
        </div>
    </div>
</div>