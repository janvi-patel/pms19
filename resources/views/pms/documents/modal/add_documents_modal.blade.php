<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="addDocsModalLabel">Upload new documents</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('/upload/documents')}}" method='post' id="addDocuments" name="addDocuments" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="form-group fileInput">
                    <div class="custom-file">
                        <input type="file" name="modal_attachment[]" file-tag="uploadedFileName_1" id="modal_attachment" class="custom-file-input modal_attachment">
                        <span type="label" class="custom-file-label d-inline-block text-truncate" id="uploadedFileName_1">Drag and Drop file...</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-dark addDocsnology" value="Upload"> 
                <span class="btn btn-secondary" data-dismiss="modal">Close</span>
                <span class="btn btn-primary" id="addDocs">Add More Files</span>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#addDocs").on("click" , function(){
            var lastname_id = $('.fileInput input[type=file]:nth-child(1)').last().attr('file-tag');
            var split_id = lastname_id.split('_');
            var index = Number(split_id[1]) + 1;
            if(index <= 10)
            {
                var newel = $('.fileInput:last').clone(true);
                $(newel).find('input[type=file]:nth-child(1)').first().attr("file-tag","uploadedFileName_"+index).val("");
                $(newel).find('span[type=label]:nth-child(2)').first().attr("id","uploadedFileName_"+index).text('Drag and Drop file...');
                $(newel).insertAfter(".fileInput:last");
                return;
            }
            else{
                swal("Please select maximum 10 documents to upload","", "error");
            }
        });
        $('.modal_attachment').change(function(e){
            var Ids = $(e.target).attr('file-tag');
            var files =$(this).prop("files");   
            if(files.length !== 0){
                var ext = $("[file-tag|='"+Ids+"']").val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['pdf','xls','xlsx','doc','docx','ppt','pptx','pptm']) == -1) {
                    $(this).val("");
                    $("#"+Ids).text("Drag and Drop file...");
                    
                    swal("Only Pdf, Xls, Xlsx, Doc, Docx, Ppt,Pptx,Pptm files are accepted","", "error");
                }
                else{
                    var names  = $.map(files, function(val) { return val.name; });
                    $("#"+Ids).text(names);
                }
            }
            else{
                $("#"+Ids).text("Drag and Drop file...");
            }
        });
    });
</script>