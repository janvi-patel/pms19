@extends('layouts.master')
@section('content')
@php
$status = config('constant.estimation_status_id');
$pageRangArray = config('constant.page_range');
@endphp
@section('moduleName')
    Estimated Lead
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">On Going Estimation Lead</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Estimation Lead</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/lead/on_going')}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Lead Title</label>
                                    <?php asort($lead_name);?>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_lead_name" id="search_by_lead_name">
                                        <option value="">Select</option>
                                        @if(!empty($lead_name))
                                            @foreach(array_unique($lead_name) as $val)
                                                <option value="{{$val}}" {{(isset($request->search_by_lead_name) && ($request->search_by_lead_name == $val))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Technology</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_technology" id="search_by_technology">
                                        <option value="">Select</option>
                                        @if(!empty($technology))
                                            @foreach($technology as $val)
                                                <option value="{{$val['name']}}" {{(isset($request->search_by_technology) && ($request->search_by_technology == $val['name']))?'selected':''}}>{{$val['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Project Manager</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_manager" id="search_by_project_manager">
                                        <option value="">Select</option>
                                        @if(!empty($project_manager))      
                                            @foreach($project_manager as $val)
                                                <option value="{{$val['fullName']}}" {{(isset($request->search_by_project_manager) && ($request->search_by_project_manager == $val['fullName']))?'selected':''}}>{{$val['fullName']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Team Leader</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_team_leader" id="search_by_team_leader">
                                        <option value="">Select</option>
                                        @if(!empty($team_leader))      
                                            @foreach($team_leader as $val)
                                                <option value="{{$val['fullName']}}" {{(isset($request->search_by_team_leader) && ($request->search_by_team_leader == $val['fullName']))?'selected':''}}>{{$val['fullName']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            
                                <div class="form-group col-md-4 col-sm-6">
                                          <?php asort($added_by);?>
                                    <label class="">Added By</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_added" id="search_by_added">
                                        <option value="">Select</option>
                                        @if(!empty($added_by))
                                            @foreach(array_unique($added_by) as $val)
                                                <option value="{{$val}}" {{(isset($request->search_by_added) && ($request->search_by_added == $val))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Project Type</label>
                                    <select name="search_project_type" id="search_project_type" style="width: 100%; height:36px;" class="select2 form-control custom-select">
                                        <option value="">Select</option>
                                        @foreach($projectTypes as $projectStatusVal)
                                            <option value="{{$projectStatusVal->project_type_name}}" {{(isset($request->search_project_type) && ($request->search_project_type == $projectStatusVal->project_type_name))?'selected':''}}>{{$projectStatusVal->project_type_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label>Estimation Status</label>
                                    <select name="search_status" id="search_status" style="width: 100%; height:36px;"  class="select2 form-control custom-select">
                                        <option value="">Select</option>
                                        <option value="0" {{(isset($request->search_status) && ($request->search_status == 0))?'selected':''}}>All</option>
                                        @foreach($status as $key => $statusVal)
                                            <option value="{{$key}}" {{(isset($request->search_status) && ($request->search_status == $key))?'selected':''}}>{{$statusVal}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6">
                                    <label class="">Assigned to Company</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_company" id="search_by_company">
                                        <option value="">Select</option>
                                        @if(!empty($companies))     
                                            @foreach($companies as $val)
                                                <option value="{{$val->id}}" {{(isset($request->search_by_company) && ($request->search_by_company == $val->id))?'selected':''}}>{{$val->company_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/lead/on_going')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-6"> 
                                <div class="form-group col top-pagination">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <p class="mt-1 text-right">Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of total {{$data->total()}} entries</p>
                            </div>
                            <div class="col-2">
                                    @if(Auth::user()->hasRole('admin'))
                                    <div class="form-group col">
                                        <select class="select2 form-control custom-select update_checked_lead" name="update_checked_lead" id="update_checked_lead" style="width: 100%; height:36px;"  >
                                           <option value="">Change Status</option>
                                            <option value="1">Pending</option> 
                                            <option value="2">In progress</option> 
                                            <option value="3">completed</option>
                                        
                                            </select>
                                    </div> 
                                    @endif
                                </div>
                        </div>
                        @if (count($data)>0)
                        <div class="table-responsive">
                            <table id="holiday_listing" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                         <th>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" id="checkAll" class="custom-control-input">
                                                        <label class="custom-control-label" for="checkAll"></label>
                                                    </div>
                                                </th>
                                        <th>@sortablelink('lead_title','Lead Title')</th>
                                        <th>@sortablelink('technology','Technology')</th>
                                        <th>@sortablelink('project_manager_name','Project Manager')</th>
                                        <th>@sortablelink('team_leader_name','Team Leader')</th>
                                        <th>@sortablelink('added_by','Added By')</th>
                                        <th>@sortablelink('end_date','End Date')</th>
                                        <th>@sortablelink('source','Source')</th>
                                        <th>@sortablelink('project_type_name','Project Type')</th>
                                        <th>@sortablelink('status','Lead Status')</th>
                                        <th>@sortablelink('reviewer_end_date','Reviewer End Date')</th>
                                        <th>@sortablelink('created_at','Created Date')</th>
                                        <th>@sortablelink('updated_at','Updated Date')</th>
                                        <th>@sortablelink('company_name','Assigned to company')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    $j = $data->firstItem();
                                    @endphp
                                    @foreach($data as $lead)
                                        <tr id="{{$lead->id}}">
                                            <td>{{$j}}</td>
                                             <td>
                                                    <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="update_lead_row[]" id="update_lead_row_{{ $lead['id'] }}" value="{{$lead['id'] }}" class="custom-control-input update_lead_row">
                                                    <label class="custom-control-label" for="update_lead_row_{{ $lead['id'] }}"></label>
                                                    </div>
                                                </td>
                                            <td  class="text-capitalize">@if($lead->created_by == Auth::user()->id)<a href="{{url('/lead/details/'.$lead->id)}}" class="a_clickable_link" project_id="{{$lead->id}}"><span  data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="View In Details" >{{$lead->title}}</span></a>@else {{$lead->title}} @endif</td>
                                            <td class="text-capitalize">{{$lead->tech_name}}</td>
                                            <td>{{(isset($lead->project_manager_name) && $lead->project_manager_name!='')?$lead->project_manager_name:'N/A'}}</td>
                                            <td>{{(isset($lead->team_leader_name) && $lead->team_leader_name!='')?$lead->team_leader_name:'N/A'}}</td>
                                            <td>{{(isset($lead->added_by) && $lead->added_by!='')?$lead->added_by:'N/A'}}</td>
                                            <td>{{ (isset($lead['end_date']) && $lead['end_date']!='')?date('d-m-Y',strtotime($lead['end_date'])):'N/A' }}</td>
                                            <td>{{$lead->source == 1?"Up-Work":"N/A"}}</td>
                                            <td>{{$lead->project_type_name}}</td>
                                            @php
                                                $color = 'text-dark'
                                            @endphp
                                            @if(isset($status[$lead->status]))
                                                @php 
                                                    if($lead->status == 1)
                                                    {
                                                        $color = 'text-danger';
                                                    }
                                                    elseif ($lead->status == 2) {
                                                        $color = 'text-success';
                                                    }
                                                    else{
                                                        $color = 'text-secondary';
                                                    }
                                                @endphp
                                            @endif
                                            <td class="{{$color}}">{{isset($status[$lead->status])?$status[$lead->status]:"N/A"}}</td>
                                            <td>
                                                <span class="r_end_date" lead_id="{{$lead->id}}">
                                                    {{(isset($lead->reviewer_end_date) && $lead->reviewer_end_date!='')?date('d-m-Y',strtotime($lead->reviewer_end_date)):'N/A' }}
                                                </span>
                                            </td>
                                            <td>{{(isset($lead->created_at) && $lead->created_at!='')?date('d-M-Y (h:i A)',strtotime($lead->created_at)):'N/A'}}</td>
                                            <td>{{(isset($lead->updated_at) && $lead->updated_at!='')?date('d-M-Y (h:i A)',strtotime($lead->updated_at)):'N/A'}}</td>
                                            <td>{{(isset($lead->company_name) && $lead->company_name!='')?$lead->company_name:'N/A'}}</td>
                                        </tr>
                                        @php
                                        $i++;$j++;
                                        @endphp 
                                        @endforeach 
                                        @else
                                        <div class="border-top">
                                        <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                                    </div>
                                        @endif
                                </tbody>
                            </table>
                        </div>
                        
                        @if($data && !empty($data))
                        <div class="pt-4">{!! $data->appends(\Request::except('page'))->render() !!}</div>
                        @endif
                    </div>
                    </div>
               </div>
            </div>
        </div>
        <div id="lead_file" class="modal fade" role="dialog"></div>
</section>
@endsection
@section('javascript')
<script>
    $( document ).ready(function() {
        @if($errors->has('modal_attachment'))
            swal("{{$errors->first('modal_attachment')}}","", "error");
        @elseif($errors->has('modal_attachment.*'))
            swal("{{$errors->first('modal_attachment.*')}}","", "error");
        @else
        @endif
    });
</script>
<script src="{{asset('js/lead.js?'.time())}}"></script>
@endsection