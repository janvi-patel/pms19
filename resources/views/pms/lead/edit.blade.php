@extends('layouts.master')
@section('content')
@section('moduleName')
    Estimated Lead
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage" ><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Estimation Lead</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Estimation Lead</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('/lead/update/')}}" enctype="multipart/form-data" name="leadAdd" id="leadAdd">
                    @csrf
                        <input type="hidden" name="lead_id" value="{{$lead['id']}}">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="lead_title">Lead title<span class="error">*</span></label>
                                        <input type="text" name="lead_title" id="lead_title" autocomplete="off" value="{{ old('lead_title') != '' ? old('lead_title') : $lead['lead_title']}}" class="form-control">
                                        <label id="lead_title-error" class="error d-block" for="lead_title"></label>
                                        @if ($errors->has('lead_title'))
                                            <label class="error">{{ $errors->first('lead_title') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="client_name">Client Name<span class="error">*</span></label>
                                        <input type="text" name="client_name" id="client_name" autocomplete="off" value="{{ old('client_name') != '' ? old('client_name') : $lead['client_name']}}" class="form-control">
                                        <label id="client_name-error" class="error d-block" for="client_name"></label>
                                        @if ($errors->has('client_name'))
                                            <label class="error">{{ $errors->first('client_name') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="technology">Technology<span class="error">*</span></label>
                                        <select class="select2 form-control custom-select technology" name="technology" id="technology" style="width: 100%; height:36px;">
                                            <option value="">Select Technology</option>
                                            @if(isset($tech) && !empty($tech)) 
                                                @foreach($tech as $techVal)
                                                    <option value="{{$techVal->id}}" {{ old('technology') != '' ? (old('technology') == $techVal->id ? 'selected' : '') : ($lead['technology'] == $techVal->id ? 'selected' : '')}}>{{$techVal->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <label id="technology-error" class="error d-block" for="technology"></label>
                                        @if ($errors->has('technology'))
                                            <label class="error">{{ $errors->first('technology') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="company_id">Assign To Company<span class="error">*</span></label>
                                        <select class="select2 form-control custom-select company_id" name="company_id" id="company_id" style="width: 100%; height:36px;">
                                            <option value="">Select Copmpany</option>
                                            @if(isset($companies) && !empty($companies)) 
                                                @foreach($companies as $company)
                                                    <option value="{{$company->id}}" {{ old('company_id') != '' ? (old('company_id') == $company->id ? 'selected' : '') : ($lead['company_id'] == $company->id ? 'selected' : '')}}>{{$company->company_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <label id="company_id-error" class="error d-block" for="company_id"></label>
                                        @if ($errors->has('company_id'))
                                            <label class="error">{{ $errors->first('company_id') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="project_manager">Project Maneger<span class="error">*</span></label>
                                        <select class="select2 form-control custom-select project_manager" name="project_manager" id="project_manager" style="width: 100%; height:36px;">
                                            <option value="">Select Project Maneger</option>
                                            @if(isset($projectManagerList) && !empty($projectManagerList)) 
                                                @foreach($projectManagerList as $projectManagerListVal)
                                                    <option value="{{$projectManagerListVal['id']}}" {{ old('project_manager') != '' ? (old('project_manager') == $projectManagerListVal['id'] ? 'selected' : '') : ($lead['project_manager'] == $projectManagerListVal['id'] ? 'selected' : '')}}>{{$projectManagerListVal['fullName']}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <label id="project_manager-error" class="error d-block" for="project_manager"></label>
                                        @if ($errors->has('project_manager'))
                                            <label class="error">{{ $errors->first('project_manager') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="team_leader">Team Leader</label></label>
                                        <select class="select2 form-control custom-select team_leader" name="team_leader" id="team_leader" style="width: 100%; height:36px;">
                                            <option value="">Select Team Leader</option>
                                            @if(isset($team_leader) && !empty($team_leader)) 
                                                @foreach($team_leader as $teamleadval)
                                                    <option value="{{$teamleadval['id']}}" {{ old('team_leader') != '' ? (old('team_leader') == $teamleadval['id'] ? 'selected' : '') : ($lead['team_leader'] == $teamleadval['id'] ? 'selected' : '')}}>{{$teamleadval['fullName']}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <label id="team_leader-error" class="error d-block" for="team_leader"></label>
                                        @if ($errors->has('team_leader'))
                                            <label class="error">{{ $errors->first('team_leader') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="project_types">Project Type<span class="error">*</span></label>
                                        <select class="select2 form-control custom-select project_types" name="project_types" id="project_types" style="width: 100%; height:36px;">
                                            <option value="">Select Project Type</option>
                                            @if(isset($projectTypes) && !empty($projectTypes)) 
                                                @foreach($projectTypes as $projectType)
                                                    <option value="{{$projectType['id']}}" {{ old('project_types') != '' ? (old('project_types') == $projectType['id'] ? 'selected' : '') : ($lead['project_types'] == $projectType['id'] ? 'selected' : '')}}>{{$projectType['project_type_name']}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <label id="project_types-error" class="error d-block" for="project_types"></label>
                                        @if ($errors->has('project_types'))
                                            <label class="error">{{ $errors->first('project_types') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="estimation_status">Estimation Status<span class="error">*</span></label>
                                        <select class="select2 form-control custom-select estimation_status" name="estimation_status" id="estimation_status" style="width: 100%; height:36px;">
                                            @php
                                                $status = config('constant.estimation_status_id');
                                            @endphp
                                            
                                            @if(config('constant.estimation_status_id') && !empty(config('constant.estimation_status_id'))) 
                                                @foreach($status as $key => $projectStatusVal)
                                                    <option value="{{$key}}" {{ old('estimation_status') != '' ? (old('estimation_status') == $key ? 'selected' : '') : ($lead['estimation_status'] == $key ? 'selected' : '')}}>{{$projectStatusVal}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <label id="estimation_status-error" class="error d-block" for="estimation_status"></label>
                                        @if($errors->has('estimation_status'))
                                            <label class="error">{{ $errors->first('estimation_status') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-group">
                                        <label for="estimation_end_date">Estimation End Date</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="estimation_end_date" id="estimation_end_date" value="{{ old('estimation_end_date') != '' ? old('estimation_end_date') : ($lead['estimation_end_date'])?date('d-m-Y',strtotime($lead['estimation_end_date'])):''}}" class="form-control estimation_end_date" readonly="" />
                                        </div>
                                        <label id="estimation_end_date-error" class="error d-block" for="estimation_end_date"></label>
                                        <a href="javascript:void(0)" id="reset_end_date">Reset</a>
                                        @if($errors->has('estimation_end_date'))
                                            <label class="error">{{ $errors->first('estimation_end_date') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label for="description">Description<span class="error">*</span></label>
                                    <textarea class="form-control" name="description" id="description">{{ old('description') != '' ? old('description') : $lead['description']}}</textarea>
                                    @if($errors->has('description'))
                                        <label class="error">{{ $errors->first('description') }}</label>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input type="checkbox" id="source" name="source" class="form-check-input" {{ old('source') ? 'checked' : ($lead['source'] == 1? 'checked':'' ) }} style="width:auto !important;"> 
                                        <label for="source">Upwork</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Update" class="btn btn-primary btn-dark" >
                            <a href="{{url('/lead')}}"><button type="button" value="Cancel" class="btn btn-info btn-secondary">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascript')
<script src="{{asset('js/lead.js?'.time())}}"></script>
@endsection