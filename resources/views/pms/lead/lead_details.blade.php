@extends('layouts.master')
@section('content')
@php
$pageRangArray = config('constant.page_range');
@endphp
@section('moduleName')
    Project Lead Details
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="{{ url('/lead') }}" class="btn btn-primary btn-dark" id="BackPage" value="BackPage"><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/lead')}}">My Lead</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Project Lead Details</li>
                </ol>
            </div>
        </div>
    </div>
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @include('pms.lead.lead_details.lead_detail')
                @include('pms.lead.lead_details.chat')
            </div>
        </div>
    </div>
    <div id="lead_file" class="modal fade" role="dialog"></div>
    <div id="addLeadDocs" class="modal fade" role="dialog"></div>
    <div id="ViewEstimationDocs" class="modal fade" role="dialog"></div>
    <div id="AddEstimationDocs" class="modal fade" role="dialog"></div>
</section>
@endsection
@section('javascript')
<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js"></script>

<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-database.js"></script>

<script src="{{asset('js/lead.js?'.time())}}"></script>

<script>
    $(document).ready(function(){
        @if($errors->has('modal_attachment'))
            swal("{{$errors->first('modal_attachment')}}","", "error");
        @elseif($errors->has('modal_attachment.*'))
            swal("{{$errors->first('modal_attachment.*')}}","", "error");
        @else
        @endif
        @if($errors->has('other_type'))
            swal("{{$errors->first('other_type')}}","", "error");
        @endif
        window.location.href = "#last_chat";
    });
</script>

<script>
    var lead_id = "{{ $lead_id }}";
    var chat_id = "{{ (Session::get('chat_id'))?Session::get('chat_id'):'' }}";

    // Initialize Firebase
    var firebaseConfig = {
        apiKey: "AIzaSyA5fgCmHWNOF5gfoERekDdPiy-XhrEGI4I",
        authDomain: "testing-pms-ef531.firebaseapp.com",
        databaseURL: "https://testing-pms-ef531.firebaseio.com",
        projectId: "testing-pms-ef531",
        storageBucket: "testing-pms-ef531.appspot.com",
        messagingSenderId: "174840973190",
        appId: "1:174840973190:web:c73e018a7c015d69390e07",
        measurementId: "G-F04XQMTFZH"
    };
    firebase.initializeApp(firebaseConfig);
    var database = firebase.database();

    reloadPage(lead_id,chat_id);

    function reloadPage(lead_id,chat_id){
        firebase.database().ref('/lead/' + lead_id + '/chat_id').set(''+chat_id);
        firebase.database().ref('/lead/' + lead_id).on('value', function(snapshot2) {
            var v = snapshot2.val();
            if(v.chat_id) {
                $.ajax({
                    type : "GET",
                    url  : getsiteurl() + "/chat/listing",
                    data : {lead_id:lead_id},
                    success: function(response) {
                        $('.chat_screen').html(response);
                        firebase.database().ref('/lead/' + lead_id + '/chat_id').set('');
                    }
                });
            }
        });
    }

    function reloadRead(lead_id,chat_id){
        firebase.database().ref('/lead/' + lead_id + '/chat_id').set(''+chat_id);
        firebase.database().ref('/lead/' + lead_id).on('value', function(snapshot2) {
            var v = snapshot2.val();
            if(v.chat_id){
                $.ajax({
                    type : "GET",
                    url  : getsiteurl() + "/chat/listing",
                    data : {lead_id:lead_id},
                    success: function(response) {
                        $('.chat_screen').html(response);
                        firebase.database().ref('/lead/' + lead_id + '/chat_id').set('');
                    }
                });
            }
        });
    }
</script>

<script>
    $(document).ready(function(){
        $.ajax({
            type: "GET",
            url: "{{ url('/chat/listing') }}",
            data: {lead_id:lead_id},
            success: function(response) {
                $('.chat_screen').html(response);
            }
        });
    });
</script>

@endsection