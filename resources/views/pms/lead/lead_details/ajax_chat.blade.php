<div class="card card-primary">
    <div class="card-header bg-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseDiscussion" aria-expanded="true">
        <h3 class="card-title">Discussion
                @php
                    $msgCount = (isset($unreadMsg[$data['id']]) && $unreadMsg[$data['id']] !='')?$unreadMsg[$data['id']]:0;
                @endphp
                @if($msgCount>0)
                    <span class="badge badge-danger badge-pill" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="unread messages">
                        {{$msgCount}}    
                    </span>
                @endif
                </h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
        </div>
    </div>
    <div id="collapseDiscussion" class="panel-collapse in collapse show" style="">
        <div class="card w-100 h-auto">
            <div style="height: 300px; overflow:auto;"  class="w-100">
                @if(isset($chats) and count($chats)>0)
                    @php
                        $last = count($chats);
                        $j=1;
                    @endphp
                    @foreach ($chats as $key => $chat)
                        @php
                            $userName = (new \App\Helpers\CommonHelper)->getAnyUserById($chat['sender_id']); 
                            $userullName = $userName["first_name"]!=null?$userName["first_name"].' '.$userName["last_name"]:'N/A';
                            $reply_message_text = "";
                            $replyed_sender_name = "";
                            $reply_message_id = 0;
                            $list = "";
                            $read_array = explode(",",$chat['mark_as_read']);
                            $time = \Carbon\Carbon::parse($chat['created_at'])->diffForHumans();
                            if($chat['reply_id'] != ''){
                                $reply_message_id = isset($chats[$chat['reply_id']]['sender_id'])?$chats[$chat['reply_id']]['sender_id']:0;
                                $replyed_sender = (new \App\Helpers\CommonHelper)->getAnyUserById($reply_message_id);
                                $replyed_sender_name = $replyed_sender["first_name"]!=null?$replyed_sender["first_name"].' '.$replyed_sender["last_name"]:'';
                                $reply_message_text = isset($chats[$chat['reply_id']]['chat_text'])?  substr($chats[$chat['reply_id']]['chat_text'], 0, 5):"message deleted";
                            }
                        @endphp
                    <ul class="chat">
                        {{--  For Login User  --}}
                        @if($chat['sender_id'] == Auth::user()->id)
                            @if($chat['mark_as_read'] != null and count($read_array)>0)
                                @foreach ($read_array as $v)
                                    @php
                                        $userList = (new \App\Helpers\CommonHelper)->getAnyUserById($v); 
                                        if($userList["first_name"] != null)
                                        {
                                            $list .= $userList["first_name"].' '.$userList["last_name"]."<br />";
                                        }
                                        else {
                                            $list .= "----<br />";
                                        }
                                    @endphp
                                @endforeach
                            @else
                                @php $list = "----";@endphp
                            @endif
                            <li class="right clearfix" id={{($last == $j)?"last_chat": "".$chat['id']}}>
                                <div class="row w-100">
                                    <div class="chat-body mr-4 clearfix w-100">
                                        <div class="header">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="float-right">
                                                        <strong class="text-primary">{{$userullName}}</strong>
                                                        <span class="chat-img">
                                                            <img src="{{url('/img/profile.png')}}" class="img-fluid" alt="User Avatar">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                            @if($reply_message_text != '')
                                                <a href="#{{$chat['reply_id']}}" class="mr-2" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Replied message"><span class="fa fa-quote-left mr-1"></span>{{$reply_message_text}}...</a>
                                            @endif
                                            {{$chat['chat_text']}}
                                            <div class="header mt-1 mb-1"></div>
                                            <span data-trigger="hover" data-toggle="tooltip" data-placement="top" data-title="{{date('d-M-Y (D)', strtotime($chat['created_at']))}}" data-content="{{date('h:i:s A', strtotime($chat['created_at']))}}">
                                                <i class="mdi mdi-clock mr-1"></i>{{$time}}
                                            </span>
                                            <div class="float-right">
                                                <span class="text-dark mdi mdi-reply mr-1 reply_btn" style="cursor: pointer;" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Reply to this message" chat_id="{{$chat['id']}}" sender_name="{{$userullName}}"></span>
                                                @if(!in_array(Auth::user()->id,$read_array))
                                                    <span class="mdi mdi-checkbox-marked-circle-outline text-success mr-1" data-trigger="hover" data-toggle="tooltip" data-placement="top" title="Viewed By" data-content="{{$list}}" chat_id="{{$chat['id']}}"></span>
                                                @endif
                                                <span chat_id={{$chat['id']}} class="chat_file mdi mdi-eye ml-1 mr-1 text-primary" title="Attached files" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="{{$chat['total_file']}} files"></span>
                                                <span chat_id={{$chat['id']}} class="addChatFiles fa fa-paperclip ml-1 mr-1 text-primary" title="" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Attach New Files"></span>
                                                <span class="mdi mdi-pencil mr-1 edt_msg_btn" chat_id="{{$chat['id']}}" r_id="{{$chat['reply_id']}}" chat_text="{{$chat['chat_text']}}" sender_name="{{$replyed_sender_name}}" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Edit"></span>
                                                <span class="mdi mdi-close text-danger delete_msg" chat_id="{{$chat['id']}}" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Delete"></span>
                                            </div>
                                        </p>
                                    </div>
                                </div>
                            </li>
                        @else
                            {{--  For Other User  --}}
                            <li class="left clearfix" id={{($last == $j)?"last_chat": "".$chat['id']}}>
                                <div class="row w-100 @if(!in_array(Auth::user()->id,$read_array)) on_hover_read_message @endif" chat_id="{{ $chat['id'] }}">
                                    <div class="chat-body ml-4 clearfix w-100">
                                        <div class="header">
                                            <span class="chat-img pull-right">
                                                <img src="{{url('/img/profile.png')}}" class="img-fluid" alt="User Avatar">
                                            </span>
                                            <strong class="text-primary">{{$userullName}}</strong>
                                        </div>
                                        <p>
                                            @if($reply_message_text != '')
                                                <a href="#{{$chat['reply_id']}}" class="mr-2" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Replied message"><span class="fa fa-quote-left mr-1"></span>{{$reply_message_text}}...</a>
                                            @endif
                                            {{$chat['chat_text']}}
                                        </p>
                                        <div class="header mt-1 mb-1"></div>
                                        <span class="text-dark reply_btn mdi mdi-reply mr-1" style="cursor: pointer;" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Reply to this message" chat_id="{{$chat['id']}}" sender_name="{{$userullName}}"></span>
                                        @if($chat['total_file'] > 0)
                                            <span chat_id={{$chat['id']}} class="chat_file mdi mdi-eye mr-1 text-primary" title="Attached files" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="{{$chat['total_file']}} files"></span>
                                        @endif
                                        @if(!in_array(Auth::user()->id,$read_array))
                                            <span class="mark-readed mdi mdi-read text-success" style="cursor: pointer;" data-trigger="hover" data-toggle="tooltip" data-placement="top" data-content="Mark As Read" chat_id="{{$chat['id']}}"></span>
                                        @endif
                                        <div class="float-right" data-trigger="hover" data-toggle="tooltip" data-placement="top" data-title="{{date('d-M-Y (D)', strtotime($chat['created_at']))}}" data-content="{{date('h:i:s A', strtotime($chat['created_at']))}}">
                                            <i class="mdi mdi-clock mr-1"></i>{{ $time }}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endif               
                    </ul>
                    @php
                        $j++;
                    @endphp
                    @endforeach
                @else
                    <div class="d-flex justify-content-center align-items-center" style="height: 260px; background:rgba(184, 175, 175, 0.123);">No Message</div>
                @endif
            </div>
            <script src="{{asset('js/lead_chat.js?'.time())}}"></script>   