<link rel="stylesheet" href="{{asset('css/lead_chat.css')}}" />
<div class="row">
    <div class="col-md-12">
        {{-- data ajax_chat.blade.php --}}
            <div class="chat_screen"></div>
        {{-- data ajax_chat.blade.php --}}            
                    <hr>
                    <div class="card-body">
                        <form class="form-horizontal @if(old('edt_chat_id') != null) @else d-none @endif" method="post" action="{{url('edit/chat')}}" name="edit_message" id="edit_message">
                            @csrf
                            <input type="hidden" name="edt_lead_id" value="{{$data['id']}}">
                            <input type="hidden" name="edt_reply_id" class="edt_reply_id" id="edt_reply_id" value="{{ old('edt_reply_id') }}">
                            <input type="hidden" name="edt_chat_id" class="edt_chat_id" id="edt_chat_id"  value="{{ old('edt_chat_id') }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="edt_reply_to d-none text-primary"></label> <span class='edt_remove_to_replay d-none'><i class='mdi mdi-close text-danger' data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Remove" style='cursor: pointer;'></i></span>
                                        <div class="input-group">
                                            <textarea type="text" name="edt_chat_text" id="edt_chat_text" class="form-control edt_chat_text">{{ old('edt_chat_text') }}</textarea>
                                        </div>
                                        <label class="error" for="edt_chat_text" style="display : none;"></label>
                                        @if($errors->has('edt_chat_text'))
                                            <label class="error">{{ $errors->first('edt_chat_text') }}</label>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-check">
                                                <input type="checkbox" id="inform_to_email" name="inform_to_email" class="form-check-input" {{ old('inform_to_email') ? 'checked' : '' }} style="width:auto !important;"> 
                                                <label for="inform_to_email">inform to email</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group-prepend">
                                        <button class="btn btn-dark" data-toggle="tooltip" data-placement="right" data-trigger="hover" data-content="Update Message"><i class="mdi mdi-pencil"></i></button>
                                        <span class="btn btn-danger ml-2 cancel_msg" data-toggle="tooltip" data-placement="right" data-trigger="hover" data-content="Cancel Edit"><i class="mdi mdi-comment-remove-outline"></i></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form class="form-horizontal @if(old('edt_chat_id') != null) d-none @else  @endif" method="post" action="{{url('send/chat')}}" name="send_message" id="send_message" enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            <input type="hidden" name="lead_id" value="{{$data['id']}}">
                            <input type="hidden" name="reply_id" class="reply_id" id="reply_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="reply_to d-none text-primary"></label> <span class='remove_to_replay d-none'><i class='mdi mdi-close text-danger' data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Remove" style='cursor: pointer;'></i></span>
                                        <div class="input-group">
                                            <textarea type="text" name="chat_text" id="chat_text" placeholder="Type a message..." class="form-control">{{ old('chat_text') }}</textarea>
                                        </div>
                                        <label class="error" for="chat_text" style="display : none;"></label>
                                        @if($errors->has('chat_text'))
                                            <label class="error">{{ $errors->first('chat_text') }}</label>
                                        @endif
                                    </div>
                                    <div class="form-group chat_attachment_file {{($errors->has('chat_attachment') || $errors->first('chat_attachment.*'))? '' : 'd-none'}}">
                                        <div class="custom-file">
                                            <input type="file" name="chat_attachment[]" id="chat_attachment" class="custom-file-input" multiple="">
                                            <label class="custom-file-label d-inline-block text-truncate" id="uploadedFileName">Drag and Drop up to 10 files...</label>
                                        </div>
                                        @if($errors->has('chat_attachment'))
                                            <label class="error">{{ $errors->first('chat_attachment') }}</label>
                                        @elseif($errors->has('chat_attachment.*'))
                                            <label class="error">{{ $errors->first('chat_attachment.*') }}</label>
                                        @else
                                        @endif
                                    </div>
                                    <div class="input-group-prepend">
                                        <button class="btn btn-dark" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Send message"><i class="fa fa-paper-plane mr-1"></i></button>
                                        <span class="btn btn-primary ml-2 attach_docs" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Attach Docs with message"><i class="fa fa-paperclip"></i></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>