@php 
    $status = config('constant.estimation_status_id');
@endphp
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header bg-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseLeadDetails" aria-expanded="true">
                <h3 class="card-title">Project Lead Description</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="collapseLeadDetails" class="panel-collapse in collapse show" style="">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="table-responsive-md">
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width: 20%">Lead</th>
                                        <td colspan="2" class="text-capitalize">
                                            <b>Title</b> : {{$data['lead_title']}}
                                        </td>
                                        <td colspan="2">
                                            <b>Technology</b> : @php $tech_name = (new \App\Helpers\CommonHelper)->getTechnology($data['technology']); @endphp
                                            {{$tech_name["name"]!=null?$tech_name["name"]:'N/A'}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Client</th>
                                        <td colspan="2">
                                            <b>Name</b> : {{$data['client_name']}}
                                        </td>
                                        <td colspan="2">
                                            <b>Source</b> : {{$data['source']==1?"Up-Work":"N/A"}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Estimation</th>
                                        <td colspan="2">
                                            <b>Lead Code</b> : <span class="text-capitalize">{{ (isset($data['lead_code']) and $data['lead_code'] != '' ) ? $data['lead_code']:"N/A" }}<span>
                                        </td>
                                        <td colspan="2">
                                            <b>Status</b> : <span class="text-capitalize">{{ (isset($data['estimation_status']) and $data['estimation_status'] != '') ? $status[$data['estimation_status']]:"N/A" }}<span>
                                        </td>
                                    </tr>
                                    <tr class="w-100">
                                        <th>Account Manager</th>
                                        <td colspan="2">
                                            <b>Name</b> : 
                                            @php $added_by = (new \App\Helpers\CommonHelper)->getAnyUserById($data['created_by']); @endphp
                                            {{$added_by["first_name"]!=null?$added_by["first_name"].' '.$added_by["last_name"]:'N/A'}}
                                        </td>
                                        <td colspan="2">
                                            <b>End Date</b> : 
                                            {{(isset($data['estimation_end_date']) && $data['estimation_end_date'] != '') ?date('d-M-Y',strtotime($data['estimation_end_date'])):'N/A'}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Date</th>
                                        <td colspan="2">
                                            <b>Created Date</b> : {{(isset($data['created_at']) && $data['created_at'] != '') ?date('d-M-Y (h:i A)',strtotime($data['created_at'])):'N/A'}}
                                        </td>
                                        <td colspan="2">
                                            <b>Updated Date</b> : {{(isset($data['updated_at']) && $data['updated_at'] != '') ?date('d-M-Y (h:i A)',strtotime($data['updated_at'])):'N/A'}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Reviewer</th>
                                        <td colspan="2">
                                            <b>Project Manager</b> :
                                            @php $pm = (new \App\Helpers\CommonHelper)->getAnyUserById($data['project_manager']); @endphp
                                            {{$pm["first_name"]!=null?$pm["first_name"].' '.$pm["last_name"]:'N/A'}}
                                        </td>
                                        <td colspan="2">
                                            <b>Team Leader</b> :
                                            @php $tl = (new \App\Helpers\CommonHelper)->getAnyUserById($data['team_leader']); @endphp
                                            {{isset($tl["first_name"]) && $tl["first_name"]!=null?$tl["first_name"].' '.$tl["last_name"]:'N/A'}}
                                        </td>
                                    </tr>
                                    @if(isset($data['reviewer_end_date']) && isset($data['date_given_by']))
                                    <tr>
                                        <th>Reviewer Date</th>
                                        <td colspan="2">
                                            <b>End Date</b> :
                                            {{(isset($data['reviewer_end_date']) && $data['reviewer_end_date'] != '') ?date('d-M-Y',strtotime($data['reviewer_end_date'])):'N/A'}}
                                        </td>
                                        <td colspan="2">
                                            <b>Given By</b> :
                                            @php $added_by = (new \App\Helpers\CommonHelper)->getAnyUserById($data['date_given_by']); @endphp
                                                {{$added_by["first_name"]!=null?$added_by["first_name"].' '.$added_by["last_name"]:'N/A'}}
                                        </td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th>Attached Files</th>
                                        <td colspan="2"> <b>By Account Manager</b>
                                            ( {{$data['total_file']}} ) Files<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="View Files" class="lead-file" lead-id={{$data['id']}}>
                                                <i class="mdi mdi-eye ml-2"></i>
                                            </a>
                                            @if(Auth::user()->id == $data['created_by'])
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Attach New Documents" class="addLeadDocs" lead-id={{$data['id']}}>
                                                    <i class="fa fa-paperclip ml-2"></i>
                                                </a>
                                            @endif
                                        </td>
                                        <td colspan="2"> <b>By Project Manager</b>
                                            ( {{$data['etotal_file']}} ) Files<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="View Files" class="ViewEstimationDocs" lead-id={{$data['id']}}>
                                                <i class="mdi mdi-eye ml-2"></i>
                                            </a>
                                            @if(Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Attach New Documents" class="AddEstimationDocs" lead-id={{$data['id']}}>
                                                    <i class="fa fa-paperclip ml-2"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Lead Description</th>
                                        <td colspan="3">
                                            {{$data['description'] != ''?$data['description']:'N/A'}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>