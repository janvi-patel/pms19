<div class="modal-dialog modal-md">
    <div class="modal-content" id="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Upload Documentation</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="modal-body">
            @if(Auth::user()->id == $created_id)
                <div class="mt-2 mb-2 ">
                    <form action="{{url('/lead/update/docs')}}" method="post" class="model attach_new_docs_form" enctype="multipart/form-data">
                        @csrf
                        @if($type == "chat")
                            <input type="hidden" name="chat_id" value="{{$id}}">
                        @else
                            <input type="hidden" name="lead_id" value="{{$id}}">
                        @endif
                        <div class="form-group fileInput">
                            <div class="custom-file">
                                <input type="file" name="modal_attachment[]" file-tag="uploadedFileName_1" id="modal_attachment" class="custom-file-input modal_attachment">
                                <span type="label" class="custom-file-label d-inline-block text-truncate" id="uploadedFileName_1">Drag and Drop file...</span>
                            </div>
                            @if($errors->has('modal_attachment'))
                                <label class="error">{{ $errors->first('modal_attachment') }}</label>
                            @elseif($errors->has('modal_attachment.*'))
                                <label class="error">{{ $errors->first('modal_attachment.*') }}</label>
                            @else
                            @endif
                        </div>
                        <input type="submit" class="btn btn-dark" value="Upload" />
                        <span class="btn btn-primary" id="addDocs">Add More Files</span>
                    </form>
                </div>
            @endif
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').popover({html:true});
        $('.attach_new_docs').on('click',function () {
            $(".attach_new_docs_btn").addClass("d-none");
            $(".attach_new_docs_form").removeClass("d-none");
        });
        $('.modal_attachment').change(function(e){
            var Ids = $(e.target).attr('file-tag');
            var files =$(this).prop("files");
            if(files.length !== 0){
                var ext = $("[file-tag|='"+Ids+"']").val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['pdf','xls','xlsx','doc','docx','jpeg','png','jpg','psd','mp4','mkv']) == -1) {
                    $(this).val("");
                    $("#"+Ids).text("Drag and Drop file...");
                    swal("Only Pdf, Xls, Xlsx, Doc, Docx, Jpeg, Jpg, Png, Psd, mp4, mkv files are accepted","", "error");
                }
                else{
                    var names  = $.map(files, function(val) { return val.name; });
                    $("#"+Ids).text(names);
                }
            }
            else{
                $("#"+Ids).text("Drag and Drop file...");
            }
        });
    });
</script>