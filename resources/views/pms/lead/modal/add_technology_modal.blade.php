<div class="modal fade" id="addTechModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addTechModalLabel">Add New Technology</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addTech" name="addTech">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name<span class="error">*</span></label><label class="ml-2 name_error error" for="name"></label>
                        <input type="text" name="name" value="" class="form-control techName" id="name">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-dark addTechnology"> 
                    <span class="btn btn-secondary" data-dismiss="modal">Close</span>
                </div>
            </form>
        </div>
    </div>
</div>