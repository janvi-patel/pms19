<div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Documentation</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
            <div class="modal-body" id="modal-body">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="project_details_listing" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>File Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($leadFile))
                                    @foreach($leadFile as $leadFile_val)
                                        <tr class="docs{{$leadFile_val['id']}}">
                                            <td>{{$leadFile_val['file_name']}}</td>
                                            <td align="center">
                                                <a href="{{asset('upload/leads/'.$leadFile_val['file_name'])}}" data-toggle="tooltip" data-trigger="hover" data-content="Download" download>
                                                    <i class="mdi mdi-arrow-down-bold-circle-outline mdi-20px"></i>
                                                </a>
                                                @if(Auth::user()->id == $created_id)
                                                    <a href="javascript:void(0);" class="delete deleteDocs" data-toggle="tooltip" data-trigger="hover" data-content="Delete Docs" data-docs-id={{$leadFile_val['id']}}>
                                                        <i class="mdi mdi-close"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="2" align="center">No Documents</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if(Auth::user()->id == $created_id)
                        <div class="mt-2 mb-2 ">
                            <div class="text-right attach_new_docs_btn"> 
                                <span class="btn btn-dark attach_new_docs"  data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Attach New Docs"><i class="fa fa-paperclip"></i></span>
                            </div>
                            <form action="{{url('/lead/update/docs')}}" method="post" class="d-none model attach_new_docs_form" enctype="multipart/form-data">
                                @csrf
                                <h5 class="mt-4 text-bold">Upload Documentation</h5>
                                @if($type == "chat")
                                    <input type="hidden" name="chat_id" value="{{$id}}">
                                @else
                                    <input type="hidden" name="lead_id" value="{{$id}}">
                                @endif
                                <div class="form-group fileInput">
                                    <div class="custom-file">
                                        <input type="file" name="modal_attachment[]" file-tag="uploadedFileName_1" id="modal_attachment" class="custom-file-input modal_attachment">
                                        <span type="label" class="custom-file-label d-inline-block text-truncate" id="uploadedFileName_1">Drag and Drop file...</span>
                                    </div>
                                    @if($errors->has('modal_attachment'))
                                        <label class="error">{{ $errors->first('modal_attachment') }}</label>
                                    @elseif($errors->has('modal_attachment.*'))
                                        <label class="error">{{ $errors->first('modal_attachment.*') }}</label>
                                    @else
                                    @endif
                                </div>
                                <input type="submit" class="btn btn-dark" value="Upload" />
                                <span class="btn btn-primary" id="addDocs">Add More Files</span>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').popover({html:true});
        $('.attach_new_docs').on('click',function () {
            $(".attach_new_docs_btn").addClass("d-none");
            $(".attach_new_docs_form").removeClass("d-none");
        });
        $('.modal_attachment').change(function(e){
            var Ids = $(e.target).attr('file-tag');
            var files =$(this).prop("files");
            if(files.length !== 0){
                var ext = $("[file-tag|='"+Ids+"']").val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['pdf','xls','xlsx','doc','docx','jpeg','png','jpg','psd','mp4','mkv']) == -1) {
                    $(this).val("");
                    $("#"+Ids).text("Drag and Drop file...");
                    swal("Only Pdf, Xls, Xlsx, Doc, Docx, Jpeg, Jpg, Png, Psd, mp4, mkv files are accepted","", "error");
                }
                else{
                    var names  = $.map(files, function(val) { return val.name; });
                    $("#"+Ids).text(names);
                }
            }
            else{
                $("#"+Ids).text("Drag and Drop file...");
            }
        });
    });
</script>