@php
    $lead_docs_type = config('constant.lead_docs_type');
@endphp
<div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">View Documentation</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="modal-body">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="project_details_listing" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>File Name</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($leadFile))
                                @foreach($leadFile as $leadFile_val)
                                    <tr class="docs{{$leadFile_val['id']}}">
                                        <td>{{$leadFile_val['file_name']}}</td>
                                        @if($leadFile_val['docs_type'] == '5')
                                        <td>{{(isset($leadFile_val['other_type_name']) && $leadFile_val['other_type_name']!='') ? $leadFile_val['other_type_name'] : 'Other Type'}}</td>
                                        @else
                                        <td>{{$lead_docs_type[$leadFile_val['docs_type']]}}</td>
                                        @endif
                                        <td align="center">
                                            <a href="{{asset('upload/leads/'.$leadFile_val['file_name'])}}" data-toggle="tooltip" data-trigger="hover" data-content="Download" download>
                                                <i class="mdi mdi-arrow-down-bold-circle-outline mdi-20px"></i>
                                            </a>
                                            @if((!Auth::user()->hasRole(config('constant.account_manager_slug'))) && Auth::user()->id == $leadFile_val['created_by'])
                                                <a href="javascript:void(0);" class="delete deleteDocs" data-toggle="tooltip" data-trigger="hover" data-content="Delete Docs" data-docs-id={{$leadFile_val['id']}}>
                                                    <i class="mdi mdi-close"></i>
                                                </a>
                                                @if($leadFile_val['show_to_bde'] == 0)
                                                    <a href="javascript:void(0);" class="delete permission_to_bde" data-toggle="tooltip" data-trigger="hover" data-content="Share With Account Manager" data-docs-id={{$leadFile_val['id']}}>
                                                        <i class="mdi mdi-file-export"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3" align="center">No Documents</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                @if(Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))
                    <div class="mt-2 mb-2">
                        <div class="text-right attach_new_docs_btn"> 
                            <span class="btn btn-dark attach_new_docs"  data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Attach New Docs"><i class="fa fa-paperclip"></i></span>
                        </div>
                        <form action="{{url('/lead/update/estimation/docs')}}" method="post" id="attach_docs_form" class="d-none model attach_new_docs_form" enctype="multipart/form-data">
                            <h5 class="mt-4 text-bold">Upload Documentation</h5>
                            @csrf
                            <input type="hidden" name="lead_id" value="{{$id}}">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="type_select col-12">
                                            <label class="">Documentation Type</label>
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="docs_type" id="docs_type">
                                                <option value="">Select</option>
                                                @if(!empty($lead_docs_type))
                                                    @foreach($lead_docs_type as $key => $val)
                                                        <option value="{{$key}}">{{$val}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 d-none docs_name">
                                            <div class="form-group">
                                                <label for="other_type">Type <span class="error">*</span></label><label class="ml-2 other_type-error error" for="other_type"></label>
                                                <input type="text" name="other_type" value="" class="form-control" placeholder="Document type" id="other_type">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class='file_section d-none'>
                                <div class="form-group fileInput">
                                    <div class="custom-file">
                                        <input type="file" name="modal_attachment[]" file-tag="uploadedFileName_1" id="modal_attachment" class="custom-file-input modal_attachment">
                                        <span type="label" class="custom-file-label d-inline-block text-truncate" id="uploadedFileName_1">Drag and Drop file...</span>
                                    </div>
                                    @if($errors->has('modal_attachment'))
                                        <label class="error">{{ $errors->first('modal_attachment') }}</label>
                                    @elseif($errors->has('modal_attachment.*'))
                                        <label class="error">{{ $errors->first('modal_attachment.*') }}</label>
                                    @else
                                    @endif
                                </div>
                            </div>
                            <div class="form-group docs_to_bde d-none">
                                <div class="form-check">
                                    <input type="checkbox" id="show_to_bde" name="show_to_bde" class="form-check-input" {{ old('show_to_bde') ? 'checked' : '' }} style="width:auto !important;"> 
                                    <label for="show_to_bde">Share with account manager</label>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-dark" value="Upload" />
                            <span class="btn btn-primary" id="addDocs">Add More Files</span>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        if($('#docs_type').val() !== ""){
            $('.file_section').removeClass('d-none');
        }
        Form();
        $("#docs_type").on('change',function() {
            var file_section = $('#docs_type').val();
            if(file_section !== ""){
                if(file_section === "5"){
                    $('.type_select').addClass('col-lg-6 col-md-6 col-sm-12');
                    $('.type_select').removeClass('col-12');
                    $('.docs_name').removeClass('d-none');
                }
                else{
                    $('.type_select').removeClass('col-lg-6 col-md-6 col-sm-12');
                    $('.type_select').addClass('col-12');
                    $('.docs_name').addClass('d-none');
                }
                if(file_section === "2"){
                    $('.docs_to_bde').removeClass('d-none');
                }
                else{
                    $('.docs_to_bde').addClass('d-none');
                }
                $('.file_section').removeClass('d-none');
            }else{
                $('.docs_to_bde').addClass('d-none');
                $('.file_section').addClass('d-none');
            }
        });
        $('[data-toggle="tooltip"]').popover({html:true});
        $('.select2').select2();
        $('.attach_new_docs').on('click',function () {
            $(".attach_new_docs_btn").addClass("d-none");
            $(".attach_new_docs_form").removeClass("d-none");
        });
        $('.modal_attachment').change(function(e){
            var Ids = $(e.target).attr('file-tag');
            var files =$(this).prop("files");   
            if(files.length !== 0){
                var ext = $("[file-tag|='"+Ids+"']").val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['pdf','xls','xlsx','doc','docx','jpeg','png','jpg','psd','mp4','mkv']) == -1) {
                    $(this).val("");
                    $("#"+Ids).text("Drag and Drop file...");
                    swal("Only Pdf, Xls, Xlsx, Doc, Docx, Jpeg, Jpg, Png, Psd, mp4, mkv files are accepted","", "error");
                }
                else{
                    var names  = $.map(files, function(val) { return val.name; });
                    $("#"+Ids).text(names);
                }
            }
            else{
                $("#"+Ids).text("Drag and Drop file...");
            }
        });

    Form();
    function Form() {
        $("#attach_docs_form").validate({
            rules: {
                docs_type: "required",
                other_type:{required:function(element) {
                    return $('#docs_type').val() == 5;
                    }
                }
            },
            messages: {
                docs_type: "Please enter Documenation Type",
            },
        });
    }
    });
</script>