@extends('layouts.master')
@section('content')
@php
$leaveStatusArray = config('constant.leave_status');
$pageRangArray = config('constant.page_range');
@endphp
@section('moduleName')
    Work From Home
@endsection

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Work From Home Requests</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

 @include('sweet::alert')
 <?php 
 $search_class = "";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "show";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
       <div id="accordion">
             <div class="card">
                    <div class="card-header bg-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                        <h3 class="card-title">Work From Home Requests</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div id="collapseOne" class="panel-collapse in collapse <?php echo $search_class;?>" style="">
                        <form class="form-horizontal" method="get" action="{{url('/workfromhomeequests')}}" name="search_filter" id="search_filter">
                            <div class="card-body">
                                <div class="row">
                                    {{-- @if(!Auth::user()->hasRole('developer'))
                                    <div class="form-group col-lg col-md-6 col-sm-6">
                                        <label class="">User</label>
                                          <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee">
                                            <option value="">All</option>
                                            @foreach($userNameList as $userNameListVal)
                                                <option value="{{$userNameListVal->id}}" {{(isset($request->search_by_employee) && ($request->search_by_employee == $userNameListVal->id))?'selected':''}}>{{$userNameListVal->first_name.' '.$userNameListVal->last_name}}</option>
                                            @endforeach
                                        </select>  
                                    </div>
                                    @endif --}}
                                    <div class="form-group col-lg col-md-6 col-sm-6">
                                        <label class="">Approver</label>
                                          <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_approver" id="search_by_approver">
                                            <option value="">All</option>
                                            @foreach($userNameList as $userNameListVal)
                                                <option value="{{$userNameListVal->id}}" {{(isset($request->search_by_approver) && ($request->search_by_approver == $userNameListVal->id))?'selected':''}}>{{$userNameListVal->first_name.' '.$userNameListVal->last_name}}</option>
                                            @endforeach
                                        </select>  
                                    </div>
                                    <!--  </div>
                                      <div class="row"> -->
                                    <div class="form-group col-lg col-md-6 col-sm-6">
                                        <label>Start Date</label>
                                        <div class="input-group ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="search_start_date" id="search_start_date" value="{{ $start_date }}" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg col-md-6 col-sm-6">
                                        <label>End Date</label>
                                        <div class="input-group ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="search_end_date" id="search_end_date" value="{{  $end_date}}" class="form-control"/>
                                        </div>
                                    </div>
                            <!--    </div>
                               <div class="row"> -->
                                    <div class="form-group col-lg col-md-6 col-sm-6">
                                       <label class="">Status</label>
                                         <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_status" id="search_by_status">
                                           <option value="">All</option>
                                           @foreach ($leaveStatusArray as $key => $node)
                                               <option value="{{$key}}" {{(isset($request->search_by_status) && ($request->search_by_status == $key))?'selected':''}}> {{ $node }}</option>
                                           @endforeach

                                       </select>  
                                   </div>
                                </div>
                            </div>
                            {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                            <div class="card-footer">
                               <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                                   Search
                               </button>
                               <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/workfromhomeequests')}}'">
                                   Reset
                               </button>
                           </div>
                        </form>
                   </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-10 mb-3"> 
                                <div class="form-group col pl-0 top-pagination">
                                    <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="width: 100%; height:36px;"  >
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                                <p class="mt-1">Showing {{ $leavesData->firstItem() }} to {{ $leavesData->lastItem() }} of total {{$leavesData->total()}} entries</p>
                            </div>
                            <div class="col-md-2 mb-4 col-sm-3">
                            @if(!Auth::user()->hasRole(config('constant.superadmin_slug')))
                                <a href="{{url('/workfromhome/add')}}" title="Add Work From Home Request" class="btn btn-primary btn-dark btn-block padding-5"> Add WFH Request</a> 
                            @endif
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                            <div class="col-md-1"></div>
                        </div>


                        <div class="card-body">
                            @if (count($leavesData)>0) 
                            <div class="table-responsive-md">
                                <table id="leave_listing" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>@sortablelink('user.first_name','User')<i class="fa fa-sort"></i></th>
                                            <th>@sortablelink('start_date','Start Date')<i class="fa fa-sort"></i></th>
                                            <th>@sortablelink('end_date','End Date')<i class="fa fa-sort"></i></th>
                                            <th>@sortablelink('return_date','Return Date')<i class="fa fa-sort"></i></th>
                                            <th>@sortablelink('days','Days')<i class="fa fa-sort"></i></th>
                                            <th>@sortablelink('reason','Reason')<i class="fa fa-sort"></i></th>
                                            <th>@sortablelink('approver.first_name','Approver')<i class="fa fa-sort"></i></th>
                                            <th>@sortablelink('status','Status')<i class="fa fa-sort"></i></th>
                                            <th>Approved At</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $i = 1;
                                        $j = $leavesData->firstItem();
                                        @endphp 
                                        @foreach($leavesData as $leave)
                                          <tr id="{{$leave['id']}}">
                                            <td>{{$j}}</td>
                                            <td>{{$leave['user']['first_name'].' '.$leave['user']['last_name']}}</td>
                                            <td>{!! displayDate($leave['start_date']) !!}</td>
                                            <td>{!! displayDate($leave['end_date']) !!}</td>
                                            <td>{!! displayDate($leave['return_date']) !!}</td>
                                            <td>{{$leave['days']}}</td>
                                            <td>{{$leave['reason']}}</td>
                                            <td>
                                                {{$leave['approver']['first_name'].' '.$leave['approver']['last_name']}}
                                                @if($leave['is_adhoc'] == 1)
                                                    <span class="mdi mdi-alert-octagon mdi-20px text-primary" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-title="Adhoc Leave"></span>
                                                @endif
                                            </td>
                                            <td>
                                              {{$leaveStatusArray[$leave['status']]}}
                                            </td>
                                            <td>
                                              @if(!empty($leave['approver_date']))
                                                {{(new \App\Helpers\CommonHelper)->displayDate($leave['approver_date'])}}
                                              @endif
                                            </td>
                                            <td align="center"> 
                                                @if (Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                                     Auth::user()->hasRole(config('constant.admin_slug')))
                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteleave" data-task-entry-id={{$leave['id']}}>
                                                      <i class="mdi mdi-close"></i>
                                                    </a>
                                                    
                                                @elseif (date('Y-m-d') <= $leave['start_date'])
                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteleave" data-task-entry-id={{$leave['id']}}>
                                                      <i class="mdi mdi-close"></i>
                                                    </a>
                                                @endif
                                            </td>
                                          </tr>
                                          @php
                                          $i++;$j++;
                                          @endphp 
                                         @endforeach 
                                         @else
                                         <div class="border-top">
                                            <h4 align="center" style="padding : 20px;">You have not added any work form home requests yet.</h4>
                                        </div>
                                         @endif
                                    </tbody>
                                </table>
                            </div>
                            
                            @if($leavesData && !empty($leavesData))
                            <div class="pt-4">{!! $leavesData->appends(\Request::except('page'))->render() !!}</div>
                            @endif
                        </div>
                    </div>
               </div>
            </div>
        </div>
</section>
@endsection
@section('javascript')
<script src="{{asset('js/workfronhome.js?'.time())}}"></script>
@endsection