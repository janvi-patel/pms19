@extends('layouts.master')
@section('content')
@php
$leaveStatusArray = config('constant.leave_status');
$pageRangArray = config('constant.page_range');
$departmentArray = config('constant.department');
$internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp

@section('moduleName')
    Team Work From Home
@endsection
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet">
<style type="text/css">
    .beforeConfirmation{
        background-color:#ff7a7a !important;
    }
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6"><h1>Team Work From Home Requests</h1></div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Team Work From Home Requests</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
 <?php 
 $search_class = "";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "show";
    ?>
@endif
<section class="content">
     <div class="container-fluid"> 
         <div class="card">
            <div class="card-header bg-dark collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
               <h3 class="card-title">View Team Work From Home Requests</h3>
               <div class="card-tools">
                   <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
               </div>
            </div>
            <div id="collapseOne" class="panel-collapse in collapse <?php echo $search_class;?>" style="">
                <form class="form-horizontal" method="get" action="{{url('/teamworkfromhome')}}" name="search_filter" id="search_filter">
                    <div class="card-body">
                        <div class="row">
                            <!-- add by mohit -->
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                                @if(isset($internalCompany) && count($internalCompany) > 0)
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Department</label>
                                        <select class="select2 form-control" style="width: 100%; height:36px;" name="search_by_department[]" id="search_by_department" multiple="multiple">
                                            <option value="">Select Department</option>  
                                                @if(isset($departmentArray) && !empty($departmentArray))
                                                    @foreach($departmentArray as $key => $val)
                                                        @if(!isset($request->search_by_department))
                                                            <option selected="selected" value="{{ $key }}">{{ $val }}</option>
                                                        @else
                                                            <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Internal Company</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                            <option value="">Select Internal Company</option> 
                                            @if(isset($internalCompany) && !empty($internalCompany))
                                                @foreach($internalCompany as $key => $val)
                                                    <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                                @endforeach
                                            @endif
                                            <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                        </select>
                                    </div>
                                @endif
                            @endif
                            <!-- end by mohit -->
                            <div class="form-group  col-lg col-md-6 col-sm-6">
                                <label class="">User</label>
                                  <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee">
                                    <option value="">All</option>
                                    @if(count($userNameArray)>0)
                                    @foreach($userNameArray as $key => $value)
                                        <option value="{{$key}}" {{(isset($request->search_by_employee) && ($request->search_by_employee == $key))?'selected':''}}>{{$value}}</option>
                                    @endforeach
                                    @endif
                                </select>  
                            </div>
                             @if(Auth::user()->hasRole(config('constant.tl_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))
                            <div class="form-group col">
                                <label class="">Approver</label>
                                  <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_approver" id="search_by_approver">
                                    <option value="">All</option>
                                    @if(count($allUserListArray)>0)
                                    @foreach($allUserListArray as $key => $value)
                                        <option value="{{$key}}" {{(isset($request->search_by_approver) && ($request->search_by_approver == $key))?'selected':''}}>{{$value}}</option>
                                    @endforeach
                                    @endif
                                </select>  
                            </div>
                             @endif
    <!--                    </div>
                        <div class="row">-->
                            <div class="form-group  col-lg col-md-6 col-sm-6">
                                <label>Start Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="search_start_date" id="search_start_date" value="{{ $start_date }}" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <label>End Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="search_end_date" id="search_end_date" value="{{$end_date}}" class="form-control"/>
                                </div>
                            </div>
    <!--                    </div>
                        <div class="row">-->
                             <div class="form-group   col-lg col-md-6 col-sm-6">
                                <label class="">Status</label>
                                  <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_status" id="search_by_status">
                                    <option value="0">All</option>
                                    @foreach ($leaveStatusArray as $key => $node)
                                        <option value="{{$key}}" {{(isset($request->search_by_status) && ($request->search_by_status == $key) || !isset($request->search_by_status) && $key == 2)?'selected':''}}> {{ $node }}</option>
                                    @endforeach

                                </select>  
                            </div>
                        </div>
                       
                        <div class="row">
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                            <div class="form-group col-lg-3 col-md-6 col-sm-6">                                
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_leave_emp_company" id="search_by_leave_emp_company" >
                                    <option value="0">All Companies</option>
                                        @foreach($companyList as $companyDataVal)
                                            <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_leave_emp_company) && ($request->search_by_leave_emp_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                        @endforeach
                                </select>
                            </div>  
                            @endif
                            <!-- <div class="form-group col-lg-3 col-md-4 col-sm-6"> 
                                <div class="form-check">
                                    <input type="checkbox" id="adhoc_leave" name="adhoc_leave" class="form-check-input" style="width:auto !important;" {{(isset($request->adhoc_leave) && ($request->adhoc_leave == 'on'))?'checked':''}}> 
                                    <label for="reason">Adhoc Leave</label>
                                </div>
                            </div>                           -->
                        </div>
                    </div>
                    {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                    <div class="card-footer">
                        <button name="search_submit" type="submit" class="btn btn-primary  btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/teamworkfromhome')}}'">
                            Reset
                        </button>
                    </div>
                </form>
            </div>
            
         </div>
        <div class="row">
            
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                         <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex justify-content-between flex-sm-row flex-column"> 
                                <div class="left w-100">
                                    <div class="top-pagination">
                                        <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="width: 100%; height:36px;"  >
                                            @if(is_array($pageRangArray))
                                               @foreach ($pageRangArray as $key => $node)
                                                    <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                                @endforeach 
                                            @endif
                                        </select>
                                    </div>
                                    @if(count($leavesData)>0)
                                        <p class="mt-1">Showing {{ $leavesData->firstItem() }} to {{ $leavesData->lastItem() }} of total {{$leavesData->total()}} entries</p>
                                    @endif
                                </div>
                                <div class="right">   
                                    @if(Auth::check() && Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))    
                                        <select class="select2 form-control custom-select update_checked_leave" name="update_checked_leave" id="update_checked_leave" style="width: 100%; height:36px;"  >
                                        <option value="">Select Action</option>
                                            <option value="1">Approve</option>
                                            <option value="3">Reject</option>
                                            @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                                            <option value="0">Delete</option>
                                            @endif
                                        </select> 
                                    @endif                                   
                                </div>
                            </div>
                        </div>
                        @if (count($leavesData)>0)  
                        <div class="table-responsive">
                            <table id="leave_listing" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        @if(Auth::check() && Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                        <th>
                                             <div class="custom-control custom-checkbox">
                                                <input type="checkbox" id="checkAll" class="custom-control-input">
                                                <label class="custom-control-label" for="checkAll"></label>
                                            </div>
                                        </th>
                                        @endif
                                        <th>@sortablelink('user.first_name','User')</th>
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                            <th>@sortablelink('company_name','Company')</th>
                                        @endif
                                        <th>@sortablelink('start_date','Start Date')</th>
                  			            <th>@sortablelink('end_date','End Date')</th>
                                        <th>@sortablelink('return_date','Return Date')</th>
                                        <th>@sortablelink('days','Days')</th>
                                        <th>@sortablelink('reason','Reason')</th>
                                        <th>@sortablelink('user.first_name','Approver')</th>
                                        <th>@sortablelink('status','Status')</th>
                                        <th>@sortablelink('created_at','Created Date')</th>
                                        <th>Approved At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                 <tbody>
                                    @php
                                    $i = 1;
                                    $j = $leavesData->firstItem();
                                    @endphp 
                                    @foreach($leavesData as $leave)
                                     <?php if(strtotime($leave->user->confirmation_date) >= strtotime($leave->end_date)){
                                        $className = "beforeConfirmation";
                                    }else{
                                        $className = "afterConfirmation";
                                    } ?>
                                    <tr  id="{{$leave['id']}}" class="{{$className}}">
                                    <td>{{$j}}</td>
                                    @if(Auth::check() && Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="update_leave_row[]" id="update_leave_row_{{ $leave['id'] }}" value="{{$leave['id'] }}" class="custom-control-input update_leave_row">
                                                <label class="custom-control-label" for="update_leave_row_{{ $leave['id'] }}"></label>
                                        </div>
                                    </td>
                                    @endif
                                    <td>{{$leave['user']['first_name'].' '.$leave['user']['last_name']}}</td>
                                    @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                        <td>{{$leave['company_name']}}</td>
                                    @endif
                                    <td>{{(new \App\Helpers\CommonHelper)->displayDate($leave['start_date'])}}</td>
                                    <td>{{(new \App\Helpers\CommonHelper)->displayDate($leave['end_date'])}}</td>
                                    <td>{{(new \App\Helpers\CommonHelper)->displayDate($leave['return_date'])}}</td>
                                    <td>{{$leave['days']}}</td>
                                    
                                    <td>{{$leave['reason']}}</td>
                  		            <td>
                                        {{$leave['approver']['first_name'].' '.$leave['approver']['last_name']}}
                                        @if($leave['is_adhoc'] == 1)
                                            <span class="mdi mdi-alert-octagon mdi-20px text-primary" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-title="Adhoc Leave"></span>
                                        @endif
                                    </td>
                                    <td>
                                      {{$leaveStatusArray[$leave['status']]}}
                                    </td>
                                    <td>{{(new \App\Helpers\CommonHelper)->displayDate($leave['created_at'])}}</td>
                                    <td>
                                     @if(!empty($leave['approver_date']))
                                        {{(date('d-m-Y',strtotime($leave['approver_date'])))}}
                                    @endif
                                    </td>
                                    <td align="center">
                                            @if(Auth::check() && Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                            {{-- @if (Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug'))) --}}
                                              <a href="{{url('/workfromhome/approve/'.$leave['id'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update">
                                                  <i class="mdi mdi-pencil"></i>
                                              </a>
                                            @endif
                                            @if(Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug')))
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteleave" data-task-entry-id={{$leave['id']}}>
                                                    <i class="mdi mdi-close"></i>
                                                </a>
                                            @endif
                                    </td>
                                  </tr>
                                  @php
                                  $i++;$j++;
                                  @endphp 
                                 @endforeach 
                                @else
                                  <div class="border-top">
                                    <h4 align="center" style="padding : 20px;">No work from home request from your team.</h4>
                                  </div>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row w-100">
                            @if($leavesData && !empty($leavesData))
                           <div class="pt-4 col-md-6">{!! $leavesData->appends(\Request::except('page'))->render() !!}</div>
                           @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascript')
<script src="{{asset('js/workfronhome.js?'.time())}}"></script>
<script src="{{asset('js/reports.js?'.time())}}"></script>
@endsection