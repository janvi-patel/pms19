@extends('layouts.master')
@section('content')
    @php
        $leaveStatusArray = config('constant.leave_status');
        $pageRangArray = config('constant.page_range');
    @endphp

@section('moduleName')
    Team Work From Home
@endsection
<link href="{{ asset('css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<style type="text/css">
    .beforeConfirmation {
        background-color: #ff7a7a !important;
    }
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Work From Home Users</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Work From Home Users</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<?php
$search_class = '';
?>
@if (Request::get('search_submit'))
    <?php
    $search_class = 'show';
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header bg-dark collapsed" data-toggle="collapse" data-parent="#accordion"
                href="#collapseOne" aria-expanded="false">
                <h3 class="card-title">View Work From Home Users</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i
                            class="fa fa-chevron-down"></i></button>
                </div>
            </div>
            <div id="collapseOne" class="panel-collapse in collapse <?php echo $search_class; ?>" style="">
                <form class="form-horizontal" method="get" action="{{ url('/workfromhomeuser/show') }}"
                    name="search_filter" id="search_filter">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group  col-lg col-md-6 col-sm-6">
                                <label class="">User</label>
                                <select class="select2 form-control custom-select" style="width: 100%; height:36px;"
                                    name="search_by_employee" id="search_by_employee">
                                    <option value="">All</option>
                                    @if (count($userNameArray) > 0)
                                        @foreach ($userNameArray as $key => $value)
                                            <option value="{{ $key }}"
                                                {{ isset($request->search_by_employee) && $request->search_by_employee == $key ? 'selected' : '' }}>
                                                {{ $value }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            @if (Auth::user()->hasRole(config('constant.tl_slug')) ||
                                    Auth::user()->hasRole(config('constant.hr_slug')) ||
                                    Auth::user()->hasRole(config('constant.admin_slug')))
                                <div class="form-group col">
                                    <label class="">Approver</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;"
                                        name="search_by_approver" id="search_by_approver">
                                        <option value="">All</option>
                                        @if (count($allUserListArray) > 0)
                                            @foreach ($allUserListArray as $key => $value)
                                                <option value="{{ $key }}"
                                                    {{ isset($request->search_by_approver) && $request->search_by_approver == $key ? 'selected' : '' }}>
                                                    {{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            @endif
                            <!--                    </div>
                        <div class="row">-->
                            <div class="form-group  col-lg col-md-6 col-sm-6">
                                <label>Start Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="search_start_date" id="search_start_date"
                                        value="{{ $start_date }}" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <label>End Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="search_end_date" id="search_end_date"
                                        value="{{ $end_date }}" class="form-control" />
                                </div>
                            </div>
                            <!--                    </div>
                        <div class="row">-->
                        </div>

                        <div class="row">
                            @if (Auth::user()->hasRole(config('constant.superadmin_slug')))
                                <div class="form-group col-lg-3 col-md-6 col-sm-6">
                                    <label>Company</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;"
                                        name="search_by_leave_emp_company" id="search_by_leave_emp_company">
                                        <option value="0">All Companies</option>
                                        @foreach ($companyList as $companyDataVal)
                                            <option value="{{ $companyDataVal->id }}"
                                                {{ isset($request->search_by_leave_emp_company) && $request->search_by_leave_emp_company == $companyDataVal->id ? 'selected' : '' }}>
                                                {{ $companyDataVal->company_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <!-- <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                <div class="form-check">
                                    <input type="checkbox" id="adhoc_leave" name="adhoc_leave" class="form-check-input" style="width:auto !important;" {{ isset($request->adhoc_leave) && $request->adhoc_leave == 'on' ? 'checked' : '' }}>
                                    <label for="reason">Adhoc Leave</label>
                                </div>
                            </div>                           -->
                        </div>
                    </div>
                    {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                    <div class="card-footer">
                        <button name="search_submit" type="submit" class="btn btn-primary  btn-dark"
                            value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary"
                            onclick="location.href='{{ url('/workfromhomeuser/show') }}'">
                            Reset
                        </button>
                    </div>
                </form>
            </div>

        </div>
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex justify-content-between flex-sm-row flex-column">
                                <div class="left w-100">
                                    <div class="top-pagination">
                                        <select class="select2 form-control custom-select page_rang_dropdown"
                                            name="page_rang_dropdown" id="page_rang_dropdown"
                                            style="width: 100%; height:36px;">
                                            @if (is_array($pageRangArray))
                                                @foreach ($pageRangArray as $key => $node)
                                                    <option value="{{ $key }}"
                                                        {{ isset($request->page_range) && $request->page_range == $node ? 'selected' : '' }}>
                                                        {{ $node }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    @if (count($leavesData) > 0)
                                        <p class="mt-1">Showing {{ $leavesData->firstItem() }} to
                                            {{ $leavesData->lastItem() }} of total {{ $leavesData->total() }} entries
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if (count($leavesData) > 0)
                            <div class="table-responsive">
                                <table id="leave_listing" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>@sortablelink('user.first_name', 'User')</th>
                                            @if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry'))
                                                <th>@sortablelink('user.employee_id', 'Employee ID')</th>
                                            @endif
                                            @if (Auth::user()->hasRole(config('constant.superadmin_slug')))
                                                <th>@sortablelink('company_name', 'Company')</th>
                                            @endif
                                            <th>@sortablelink('start_date', 'Start Date')</th>
                                            <th>@sortablelink('end_date', 'End Date')</th>
                                            <th>@sortablelink('return_date', 'Return Date')</th>
                                            <th>@sortablelink('days', 'Days')</th>
                                            <th>@sortablelink('reason', 'Reason')</th>
                                            <th>@sortablelink('user.first_name', 'Approver')</th>
                                            <th>@sortablelink('status', 'Status')</th>
                                            <th>@sortablelink('created_at', 'Created Date')</th>
                                            <th>Approved At</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                            $j = $leavesData->firstItem();
                                        @endphp
                                        @foreach ($leavesData as $leave)
                                            <?php if (strtotime($leave->user->confirmation_date) >= strtotime($leave->end_date)) {
                                                $className = 'beforeConfirmation';
                                            } else {
                                                $className = 'afterConfirmation';
                                            } ?>
                                            <tr id="{{ $leave['id'] }}" class="{{ $className }}">
                                                <td>{{ $j }}</td>
                                                <td>{{ $leave['user']['first_name'] . ' ' . $leave['user']['last_name'] }}
                                                </td>
                                                @if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry'))
                                                    <td>{{ $leave['user']['employee_id'] }}</td>
                                                @endif
                                                @if (Auth::user()->hasRole(config('constant.superadmin_slug')))
                                                    <td>{{ $leave['company_name'] }}</td>
                                                @endif
                                                <td>{{ (new \App\Helpers\CommonHelper())->displayDate($leave['start_date']) }}
                                                </td>
                                                <td>{{ (new \App\Helpers\CommonHelper())->displayDate($leave['end_date']) }}
                                                </td>
                                                <td>{{ (new \App\Helpers\CommonHelper())->displayDate($leave['return_date']) }}
                                                </td>
                                                <td>{{ $leave['days'] }}</td>

                                                <td>{{ $leave['reason'] }}</td>
                                                <td>
                                                    {{ $leave['approver']['first_name'] . ' ' . $leave['approver']['last_name'] }}
                                                    @if ($leave['is_adhoc'] == 1)
                                                        <span class="mdi mdi-alert-octagon mdi-20px text-primary"
                                                            data-toggle="tooltip" data-placement="top"
                                                            data-trigger="hover" data-title="Adhoc Leave"></span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $leaveStatusArray[$leave['status']] }}
                                                </td>
                                                <td>{{ (new \App\Helpers\CommonHelper())->displayDate($leave['created_at']) }}
                                                </td>
                                                <td>
                                                    @if (!empty($leave['approver_date']))
                                                        {{ date('d-m-Y', strtotime($leave['approver_date'])) }}
                                                    @endif
                                                </td>
                                            </tr>
                                            @php
                                                $i++;
                                                $j++;
                                            @endphp
                                        @endforeach
                                    @else
                                        <div class="border-top">
                                            <h4 align="center" style="padding : 20px;">No work from home users from
                                                your team.</h4>
                                        </div>
                        @endif
                        </tbody>
                        </table>
                    </div>
                    <div class="row w-100">
                        @if ($leavesData && !empty($leavesData))
                            <div class="pt-4 col-md-6">{!! $leavesData->appends(\Request::except('page'))->render() !!}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

@section('javascript')
<script src="{{ asset('js/workfronhome.js?' . time()) }}"></script>
@endsection
