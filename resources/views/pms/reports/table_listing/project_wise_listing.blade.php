<table id="project_report_table" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Project ID</th>
            <th>Project Type</th>
            @if(Auth::user()->hasRole('developer') || isset($request->search_by_user_name) && $request->search_by_user_name != '')
                <th>User Name</th>
            @endif
            <th>@sortablelink('project_name','Project Name')</th>
            @if(!isset($request->search_by_user_name) && !Auth::user()->hasRole('developer'))
                <th>User Name</th>
            @endif
            <th>Task Name</th>
            <th>Assigned hours</th>
            <th>Logged Hours</th>
            <th>Total Logged Hours</th>
            @if(!Auth::user()->hasRole('developer'))
                <th>Total Assigned Hours</th>
            @endif
            <th>Project Hours</th>
            <th>Project Efficiency</th>
        </tr>
    </thead>
    <tbody>
        @php
            $j = 1;
            $non_billable = array();
            $is_billable = $is_non_billable = $totalBillableLoggedHours = $totalBillableTotalProjectAssignedHours = $totalBillableAssignedHours = $totalBillableProjectLoggedHours = $totalBillableProjectHours = $totalNonBillableLoggedHours = $totalNonBillableAssignedHours = $totalNonBillableProjectLoggedHours = $totalNonBillableProjectHours = $totalNonBillableTotalProjectAssignedHours = 0;
            $totalProHours = $totalEstimatedHours = $estimatedHours = 0;
            $overAllTaskHours = $overAllTotalHours = $overAllLoggedHours = $overAllProHours = $overAllAssignedHours = 0;
            $userRowCount = count($projectReportArray) * 2 ;
        @endphp
        @foreach($projectReportArray as $key => $projectReportVal)
            @if(!in_array($key,$non_billable_projects))
                @if(count($projectReportVal)>0)
                <?php
                    $userCount = isset($projectReportArray[$key]) && $projectReportArray[$key] > 0 ? count($projectReportArray[$key]) : 0;
                    $is_billable = 1;
                    $rowCount = $projectRowCount[$key] + ($userCount * 2);
                    $rowspan = 'rowspan='.$rowCount;
                    $projectData = (new \App\Helpers\CommonHelper)->getProjectData($key);
                    $projectType = (new \App\Helpers\CommonHelper)->getProjectType($key);
                    //echo "<pre>";print_r($projectType);exit;
                    $estimatedHours = (isset($projectData->pms_hours) && $projectData->pms_hours!='')?$projectData->pms_hours:'0.00';
                    $crHours = (isset($projectCrHours[$key]) && $projectCrHours[$key]!='')?$projectCrHours[$key]:'0';
                    $totalProHours = $crHours + $estimatedHours;
                ?>
                <tr class='{{$rowCount}}'>
                    <td <?php echo $rowCount>0?$rowspan:''?>>{{$key}}</td>
                    <td  <?php echo $rowCount>0?$rowspan:''?>>{{(isset($projectType->project_type_name) ? $projectType->project_type_name : '') }}</td>
                    @if(Auth::user()->hasRole('developer') || isset($request->search_by_user_name) && $request->search_by_user_name != '')
                        @php
                            $userName = (count(array_keys($projectReportArray[$key]))>0)?(new \App\Helpers\CommonHelper)->getAnyUserById(array_keys($projectReportArray[$key])[0]):'';
                        @endphp
                        <th <?php echo $rowCount>0?$rowspan:''?>>{{(isset($userName)?$userName->first_name.' '.$userName->last_name:'')}}</th>
                    @endif
                    <td <?php echo $rowCount>0?$rowspan:''?>>{{$projectData->project_name}}</td>

                    @php $totalHours = 0;$percentage = 0.00;$totalTaskHours =0;$subArray = array();  @endphp

                        @foreach($projectEmpLoggedArray[$key] as $k => $v)
                            @php    $subArray[$v['id']]['user_id'][] = $v['id'];
                                    $subArray[$v['id']]['task_name'][] = isset($v['task_name'])?$v['task_name']:'Old Entry';
                                    $subArray[$v['id']]['taskHours'][] = isset($v['taskHours'])?$v['taskHours']:'0.00';
                                    $subArray[$v['id']]['loggedHours'][] = $v['total_logged_hours'];
                                    $subArray[$v['id']]['taskId'][] = isset($v['taskId'])?$v['taskId']:'0';
                            @endphp
                        @endforeach
                        @if(count($subArray) == 0)
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            @if(!Auth::user()->hasRole('developer'))
                                <td></td>
                            @endif
                            <td></td>
                            <td></td>
                        @endif
                        @if(count($subArray)>0)
                            @foreach($subArray as $subArrayKey => $subArrayVal)
                                @php $userName = (new \App\Helpers\CommonHelper)->getAnyUserById($subArrayKey); @endphp
                                @if(!Auth::user()->hasRole('developer') && !isset($request->search_by_user_name))
                                    <td rowspan="{{count($subArrayVal['task_name']) + 1}}">{{(isset($userName)?$userName->first_name.' '.$userName->last_name:'')}}</td>
                                @endif
                                @php
                                    $userTaskHours = $userloggedHours = $userTotalTaskHr = 0 ;
                                @endphp
                                @for($i=0;$i<= count($subArrayVal['task_name']); $i++)
                                    @if($i != count($subArrayVal['task_name']))
                                        @php
                                            $totalTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                            $totalHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                            $getTotalTaskHr = (new \App\Helpers\CommonHelper)->getUserTaskToatalHr($key,$subArrayVal['user_id'][$i],$subArrayVal['taskId'][$i]);
                                            $userTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                            $userloggedHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                            $userTotalTaskHr += $getTotalTaskHr->user_logged_hours;
                                        @endphp
                                        <td>{{($subArrayVal['task_name'][$i] != '')?$subArrayVal['task_name'][$i]:'Old Entry'}}</td>
                                        <td>{{($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i]}}</td>
                                        <td>{{($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i]}}</td>
                                        <td>{{$getTotalTaskHr->user_logged_hours}}</td>
                                        @if(!Auth::user()->hasRole('developer'))
                                            <td></td>
                                        @endif
                                        <td></td>
                                        <td></td>
                                    @else
                                        <th class="text-right">Total</th>
                                        <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userTaskHours)}}</td>
                                        <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userloggedHours)}}</td>
                                        <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userTotalTaskHr)}}</td>

                                        @if(!Auth::user()->hasRole('developer'))
                                        <td></td>
                                        @endif
                                        <td></td>
                                        <td></td>
                                    @endif
                        </tr>
                                @endfor
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                            @endforeach
                        @endif
                    @php
                        $getTotalTaskHours = ($totalTaskHours != 0)?$totalTaskHours : 0;
                        $overAllTaskHours = $overAllTaskHours + $getTotalTaskHours;

                        $getTotalHours = ($totalHours != 0)?$totalHours : 0;
                        $overAllTotalHours = $overAllTotalHours + $getTotalHours;

                        $getLoggedHr = (isset($projectLoggedArray[$key][0]) && $projectLoggedArray[$key][0] != 0)?$projectLoggedArray[$key][0] : 0;
                        $overAllLoggedHours = $overAllLoggedHours + $getLoggedHr;
                        $getAssignedHr = (isset($projectEstimatedArray[$key][0]) && $projectEstimatedArray[$key][0] != 0) ? $projectEstimatedArray[$key][0] : 0;
                        $overAllAssignedHours = $overAllAssignedHours + $getAssignedHr;

                        $percentage = 0.00;

                        if($projectLoggedArray[$key][0] != 0 && $totalProHours != 0){
                            $percentage = ($projectLoggedArray[$key][0]/$totalProHours)*100;
                        }
                        $getProHr = ($totalProHours != 0)?sprintf("%.2f",$totalProHours):0;
                        $overAllProHours = $overAllProHours + $totalProHours;
                    @endphp
                    <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                        <th colspan="5" class="text-right">Total</th>
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalTaskHours)}}</b></td>
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalHours)}}</b></td>
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getLoggedHr)}}</b></td>
                        @if(!Auth::user()->hasRole('developer'))
                            <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getAssignedHr)}}</b></td>
                        @endif
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getProHr)}}</b></td>
                        <td class="text-bold">
                            @if(sprintf("%.2f",$percentage) <= 100)
                                <span class="project_completion_gree">
                                    {{sprintf("%.2f",$percentage).' %'}}
                                </span>
                            @else
                                <span class="project_completion_red">
                                    {{sprintf("%.2f",$percentage).' %'}}
                                </span>
                            @endif</b>
                        </td>
                    </tr>
                    @php
                        $totalBillableAssignedHours = $totalBillableAssignedHours + $getTotalTaskHours;
                        $totalBillableLoggedHours = $totalBillableLoggedHours + $getTotalHours;
                        $totalBillableProjectLoggedHours = $totalBillableProjectLoggedHours + $getLoggedHr;
                        $totalBillableTotalProjectAssignedHours = $totalBillableTotalProjectAssignedHours + $getAssignedHr;
                        $totalBillableProjectHours = $totalBillableProjectHours + $getProHr;
                    @endphp
                @php
                    $j++;
                @endphp
                @endif
            @else
                @php
                    $non_billable[$key] = $projectReportVal;
                @endphp
            @endif
        @endforeach
        @if($is_billable == 1)
            <tr><td colspan="11"></td></tr>
            <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                <td colspan="5" class="text-bold text-right">Total Billable Hours</td>
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableAssignedHours)}}</td>
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableLoggedHours)}}</td>
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableProjectLoggedHours)}}</td>
                @if(!Auth::user()->hasRole('developer'))
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableTotalProjectAssignedHours)}}</td>
                @endif
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableProjectHours)}}</td>
                <td></td>
            </tr>
            <tr><td colspan="11"></td></tr>
        @endif

{{-- Non Billable --}}
        @foreach($non_billable as $key => $projectReportVal)
            @if(count($projectReportVal)>0)
            <?php
                $userCount = isset($projectReportArray[$key]) && $projectReportArray[$key] > 0 ? count($projectReportArray[$key]) : 0;
                $is_non_billable = 1;
                $rowCount = $projectRowCount[$key] + ($userCount * 2);
                $rowspan = 'rowspan='.$rowCount;
                $projectData = (new \App\Helpers\CommonHelper)->getProjectData($key);
                $estimatedHours = (isset($projectData->pms_hours) && $projectData->pms_hours!='')?$projectData->pms_hours:'0.00';
                $crHours = (isset($projectCrHours[$key]) && $projectCrHours[$key]!='')?$projectCrHours[$key]:'0';
                $totalProHours = $crHours + $estimatedHours;
            ?>
            <tr class='{{$rowCount}}'>
                <td <?php echo $rowCount>0?$rowspan:''?>>{{$key}}</td>
                <td  <?php echo $rowCount>0?$rowspan:''?>>{{(isset($projectType->project_type_name) ? $projectType->project_type_name : '') }}</td>
                @if(Auth::user()->hasRole('developer') || isset($request->search_by_user_name) && $request->search_by_user_name != '')
                    @php
                        $userName = (count(array_keys($non_billable[$key]))>0)?(new \App\Helpers\CommonHelper)->getAnyUserById(array_keys($non_billable[$key])[0]):'';
                    @endphp
                    <th <?php echo $rowCount>0?$rowspan:''?>>{{(isset($userName)?$userName->first_name.' '.$userName->last_name:'')}}</th>
                @endif
                <td <?php echo $rowCount>0?$rowspan:''?>>{{$projectData->project_name}}</td>

                @php $totalHours = 0;$percentage = 0.00;$totalTaskHours =0;$subArray = array();  @endphp

                    @foreach($projectEmpLoggedArray[$key] as $k => $v)
                        @php    $subArray[$v['id']]['user_id'][] = $v['id'];
                                $subArray[$v['id']]['task_name'][] = isset($v['task_name'])?$v['task_name']:'Old Entry';
                                $subArray[$v['id']]['taskHours'][] = isset($v['taskHours'])?$v['taskHours']:'0.00';
                                $subArray[$v['id']]['loggedHours'][] = $v['total_logged_hours'];
                                $subArray[$v['id']]['taskId'][] = isset($v['taskId'])?$v['taskId']:'0';
                        @endphp
                    @endforeach
                    @if(count($subArray) == 0)
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        @if(!Auth::user()->hasRole('developer'))
                            <td></td>
                        @endif
                        <td></td>
                        <td></td>
                    @endif
                    @if(count($subArray)>0)
                        @foreach($subArray as $subArrayKey => $subArrayVal)
                            @php $userName = (new \App\Helpers\CommonHelper)->getAnyUserById($subArrayKey); @endphp
                            @if(!Auth::user()->hasRole('developer') && !isset($request->search_by_user_name))
                                <td rowspan="{{count($subArrayVal['task_name']) + 1}}">{{(isset($userName)?$userName->first_name.' '.$userName->last_name:'')}}</td>
                            @endif
                            @php
                                $userTaskHours = $userloggedHours = $userTotalTaskHr = 0 ;
                            @endphp
                            @for($i=0;$i<= count($subArrayVal['task_name']); $i++)
                                @if($i != count($subArrayVal['task_name']))
                                    @php
                                        $totalTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                        $totalHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                        $getTotalTaskHr = (new \App\Helpers\CommonHelper)->getUserTaskToatalHr($key,$subArrayVal['user_id'][$i],$subArrayVal['taskId'][$i]);
                                        $userTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                        $userloggedHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                        $userTotalTaskHr += $getTotalTaskHr->user_logged_hours;
                                    @endphp
                                    <td>{{($subArrayVal['task_name'][$i] != '')?$subArrayVal['task_name'][$i]:'Old Entry'}}</td>
                                    <td>{{($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i]}}</td>
                                    <td>{{($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i]}}</td>
                                    <td>{{$getTotalTaskHr->user_logged_hours}}</td>
                                    @if(!Auth::user()->hasRole('developer'))
                                        <td></td>
                                    @endif
                                    <td></td>
                                    <td></td>
                                @else
                                    <th class="text-right">Total</th>
                                    <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userTaskHours)}}</td>
                                    <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userTotalTaskHr)}}</td>
                                    <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($userTotalTaskHr)}}</td>
                                    @if(!Auth::user()->hasRole('developer'))
                                    <td></td>
                                    @endif
                                    <td></td>
                                    <td></td>
                                @endif
                    </tr>
                            @endfor
                            <tr>
                                <td colspan="8"></td>
                            </tr>
                        @endforeach
                    @endif
                @php
                    $getTotalTaskHours = ($totalTaskHours != 0)?$totalTaskHours : 0;
                    $overAllTaskHours = $overAllTaskHours + $getTotalTaskHours;

                    $getTotalHours = ($totalHours != 0)?$totalHours : 0;
                    $overAllTotalHours = $overAllTotalHours + $getTotalHours;

                    $getLoggedHr = (isset($projectLoggedArray[$key][0]) && $projectLoggedArray[$key][0] != 0)?$projectLoggedArray[$key][0] : 0;
                    $overAllLoggedHours = $overAllLoggedHours + $getLoggedHr;
                    $getAssignedHr = (isset($projectEstimatedArray[$key][0]) && $projectEstimatedArray[$key][0] != 0) ? $projectEstimatedArray[$key][0] : 0;
                    $overAllAssignedHours = $overAllAssignedHours + $getAssignedHr;

                    $percentage = 0.00;

                    if($projectLoggedArray[$key][0] != 0 && $totalProHours != 0){
                        $percentage = ($projectLoggedArray[$key][0]/$totalProHours)*100;
                    }
                    $getProHr = ($totalProHours != 0)?sprintf("%.2f",$totalProHours):0;
                    $overAllProHours = $overAllProHours + $totalProHours;
                @endphp
                <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                    <th colspan="5" class="text-right">Total</th>
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalTaskHours)}}</td>
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalHours)}}</td>
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getLoggedHr)}}</td>
                    @if(!Auth::user()->hasRole('developer'))
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getAssignedHr)}}</td>
                    @endif
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($getProHr)}}</td>
                    <td class="text-bold">
                        @if(sprintf("%.2f",$percentage) <= 100)
                            <span class="project_completion_gree">
                                {{sprintf("%.2f",$percentage).' %'}}
                            </span>
                        @else
                            <span class="project_completion_red">
                                {{sprintf("%.2f",$percentage).' %'}}
                            </span>
                        @endif
                    </td>
                </tr>
                @php
                    $totalNonBillableAssignedHours = $totalNonBillableAssignedHours + $getTotalTaskHours;
                    $totalNonBillableLoggedHours = $totalNonBillableLoggedHours + $getTotalHours;
                    $totalNonBillableProjectLoggedHours = $totalNonBillableProjectLoggedHours + $getLoggedHr;
                    $totalNonBillableTotalProjectAssignedHours = $totalNonBillableTotalProjectAssignedHours + $getAssignedHr;
                    $totalNonBillableProjectHours = $totalNonBillableProjectHours + $getProHr;
                @endphp
                @php
                    $j++;
                @endphp
            @endif
        @endforeach
            @if($is_non_billable == 1)
                <tr><td colspan="11"></td></tr>
                <tr style="background-color:rgba(0, 0, 0, .05) !important;">
                    <td colspan="5" class="text-bold text-right">Total Non Billable Hours</td>
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableAssignedHours)}}</td>
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableLoggedHours)}}</td>
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableProjectLoggedHours)}}</td>
                    @if(!Auth::user()->hasRole('developer'))
                        <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableTotalProjectAssignedHours)}}</td>
                    @endif
                    <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableProjectHours)}}</td>
                    <td class="text-bold"></td>
                </tr>
                <tr><td colspan="11"></td></tr>
            @endif
        <tr style="background-color:rgba(0, 0, 0, .05) !important;">
            <td colspan="5" class="text-bold text-right">Overall</td>
            <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTaskHours)}}</td>
            <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalHours)}}</td>
            <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllLoggedHours)}}</td>
            @if(!Auth::user()->hasRole('developer'))
                <td class="text-bold">{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllAssignedHours)}}</td>
            @endif
            <td class="text-bold">{{sprintf("%.2f",$overAllProHours)}}</td>
            <td class="text-bold"></td>
        </tr>
    </tbody>
</table>
