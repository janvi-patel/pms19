{{-- For Non Billable Hours--}}
@php
    $j = 1;
    $non_billable = array();
    $totalBillableLoggedHours = $is_billable = $is_non_billable = $totalBillableAssignedHours = $totalBillableProjectLoggedHours = $totalBillableProjectHours = 
    $totalNonBillableLoggedHours = $totalNonBillableAssignedHours = $totalNonBillableProjectLoggedHours = $totalNonBillableProjectHours = $estimatedHours = 0;
@endphp
@foreach($projectReportArray as $key => $projectReportVal)
    @if(!in_array($key,$non_billable_projects))
    <?php
        $is_billable = 1;
        $overAllLoggedHours = $overAllAssignedHours = $overAllTotalAssignedHr = $overAllTotalProjectHr = $overAllTotalLoggedHr = $estimatedHours = 0;
        $overAllTotalAssignedHr = isset($projectEstimatedArray[$key][0])?$projectEstimatedArray[$key][0]:0;
        $overAllTotalLoggedHr = isset($projectLoggedArray[$key][0])?$projectLoggedArray[$key][0]:0;
        $projectVal = (new \App\Helpers\CommonHelper)->getProjectData($key);
        if($projectVal){
            $estimatedHours = (isset($projectVal->pms_hours) && $projectVal->pms_hours!='')?$projectVal->pms_hours:'0.00'; 
        }
        $crHours = (isset($projectHoursArray[$key][0]) && $projectHoursArray[$key][0]!='')?$projectHoursArray[$key][0]:'0';
        $overAllTotalProjectHr = $crHours + $estimatedHours;
        $dateArray = $subArray = array();

        foreach($projectReportVal as $k => $v){ 
            $dateArray[] = $v['log_date'];
            $subArray[$v['log_date']]['user_id'][] = $v['user_id'];
            $subArray[$v['log_date']]['logged_hr'][] = $v['total_logged_hours'];
            $subArray[$v['log_date']]['task_name'][] = $v['task_name'];
            $subArray[$v['log_date']]['taskHours'][] = $v['taskHours'];
            $overAllAssignedHours = $overAllAssignedHours + $v['taskHours'];
            $overAllLoggedHours = $overAllLoggedHours + $v['total_logged_hours'];
        }
        $dateArray = array_count_values($dateArray);
        $rowCount = array_sum($dateArray);
        $rowspan = 'rowspan='.$rowCount;
    ?>
    <tr class='{{$rowCount}}'>
        <td <?php echo count($dateArray)>0?$rowspan:''?>>{{$j}}</td>
        <td <?php echo count($dateArray)>0?$rowspan:''?>>{{(new \App\Helpers\CommonHelper)->getProjectData($key)->project_name}}</td>
        @if(isset($rowCount)&& $rowCount == 0)
            <td></td>
            <td></td>
            <td></td>
            @else
            @foreach($subArray as $key => $val)
                <td rowspan="{{count($val['user_id'])}}">{{$key}}</td>
                @for($i=0;$i<count($val['user_id']);$i++)
                    <?php $userName = (new \App\Helpers\CommonHelper)->getAnyUserById($val['user_id'][$i]); ?>
                    <td>{{($val['task_name'][$i] != '')?$val['task_name'][$i]:'Old Entry'}}</td>
                    <td>{{(isset($userName)?$userName->first_name.' '.$userName->last_name:'')}}</td>
                    <td>{{($val['taskHours'][$i] != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($val['taskHours'][$i]):0}}</td>
                    <td>{{($val['logged_hr'][$i] != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($val['logged_hr'][$i]):0}}</td>
                    <td></td>
                    @if(!Auth::user()->hasRole('developer'))
                        <td></td>
                    @endif
                    <td></td>
        
        </tr>
                @endfor
            @endforeach
        <tr> 
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>Total</b></td>
            <td><b>{{($overAllLoggedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($overAllLoggedHours):0}}</b></td>
            <td><b>{{($overAllTotalLoggedHr != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalLoggedHr):0}}</b></td>
            @if(!Auth::user()->hasRole('developer'))
                <td><b>{{($overAllTotalAssignedHr != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalAssignedHr):0}}</b></td>
            @endif
            <td><b>{{($overAllTotalProjectHr != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalProjectHr):0}}</b></td>
            @php
                $totalBillableLoggedHours = $totalBillableLoggedHours + $overAllLoggedHours;
                $totalBillableProjectLoggedHours = $totalBillableProjectLoggedHours + $overAllTotalLoggedHr;
                $totalBillableAssignedHours = $totalBillableAssignedHours + $overAllTotalAssignedHr;
                $totalBillableProjectHours = $totalBillableProjectHours + $overAllTotalProjectHr;
            @endphp
        </tr>
        @endif
            @php
                $j++;
            @endphp
    @else
        @php
            $non_billable[$key] = $projectReportVal;
        @endphp
    @endif
@endforeach
@if($is_billable == 1)
    <tr><td colspan="10"></td></tr>
        <tr>
            <td colspan="6" class="text-bold text-right">Total Billable Hours</td>
            <td class="text-bold">{{($totalBillableLoggedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableLoggedHours):0}}</td>
            <td class="text-bold">{{($totalBillableProjectLoggedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableProjectLoggedHours):0}}</td>
            @if(!Auth::user()->hasRole('developer'))
                <td class="text-bold">{{($totalBillableAssignedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableAssignedHours):0}}</td>
            @endif
            <td class="text-bold">{{($totalBillableProjectHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalBillableProjectHours):0}}</td>
        </tr>
    <tr><td colspan="10"></td></tr>
@endif

{{-- For Non Billable Hours--}}

@foreach($non_billable as $key => $projectReportVal)
    <?php
        $is_non_billable = 1;
        $overAllLoggedHours = $overAllAssignedHours = $overAllTotalAssignedHr = $overAllTotalProjectHr = $overAllTotalLoggedHr = $estimatedHours = 0;
        $overAllTotalAssignedHr = isset($projectLoggedArray[$key][0])?$projectEstimatedArray[$key][0]:0;
        $overAllTotalLoggedHr = isset($projectLoggedArray[$key][0])?$projectLoggedArray[$key][0]:0;
        $projectVal = (new \App\Helpers\CommonHelper)->getProjectData($key);
        $estimatedHours = (isset($projectVal->pms_hours) && $projectVal->pms_hours!='')?$projectVal->pms_hours:'0.00'; 
        $crHours = (isset($projectHoursArray[$key][0]) && $projectHoursArray[$key][0]!='')?$projectHoursArray[$key][0]:'0';
        $overAllTotalProjectHr = $crHours + $estimatedHours;
        $dateArray = $subArray = array();
        foreach($projectReportVal as $k => $v){
            $dateArray[] = $v['log_date'];
            $subArray[$v['log_date']]['user_id'][] = $v['user_id'];
            $subArray[$v['log_date']]['logged_hr'][] = $v['total_logged_hours'];
            $subArray[$v['log_date']]['task_name'][] = $v['task_name'];
            $subArray[$v['log_date']]['taskHours'][] = $v['taskHours'];
            $overAllAssignedHours = $overAllAssignedHours + $v['taskHours'];
            $overAllLoggedHours = $overAllLoggedHours + $v['total_logged_hours'];
        }
        $dateArray = array_count_values($dateArray);
        $rowCount = array_sum($dateArray);
        $rowspan = 'rowspan='.$rowCount;
    ?>
    <tr class='{{$rowCount}}'>
        <td <?php echo count($dateArray)>0?$rowspan:''?>>{{$j}}</td>
        <td <?php echo count($dateArray)>0?$rowspan:''?>>{{(new \App\Helpers\CommonHelper)->getProjectData($key)->project_name}}</td>
        @if(isset($rowCount)&& $rowCount == 0)
        <td></td>
        <td></td>
        <td></td>
        @else
        @foreach($subArray as $key => $val)
            <td rowspan="{{count($val['user_id'])}}">{{$key}}</td>
            @for($i=0;$i<count($val['user_id']);$i++)
                <?php $userName = (new \App\Helpers\CommonHelper)->getAnyUserById($val['user_id'][$i]); ?>
                <td>{{($val['task_name'][$i] != '')?$val['task_name'][$i]:'Old Entry'}}</td>
                <td>{{(isset($userName)?$userName->first_name.' '.$userName->last_name:'')}}</td>
                <td>{{($val['taskHours'][$i] != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($val['taskHours'][$i]):0}}</td>
                <td>{{($val['logged_hr'][$i] != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($val['logged_hr'][$i]):0}}</td>
                <td></td>
                @if(!Auth::user()->hasRole('developer'))
                    <td></td>
                @endif
                <td></td>
    </tr>
            @endfor
        @endforeach
        <tr> 
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>Total</b></td>
            <td><b>{{($overAllLoggedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($overAllLoggedHours):0}}</b></td>
            <td><b>{{($overAllTotalLoggedHr != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalLoggedHr):0}}</b></td>
            @if(!Auth::user()->hasRole('developer'))
                <td><b>{{($overAllTotalAssignedHr != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalAssignedHr):0}}</b></td>
            @endif
            <td><b>{{($overAllTotalProjectHr != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalProjectHr):0}}</b></td>
            @php
                $totalNonBillableLoggedHours = $totalNonBillableLoggedHours + $overAllLoggedHours;
                $totalNonBillableProjectLoggedHours = $totalNonBillableProjectLoggedHours + $overAllTotalLoggedHr;
                $totalNonBillableAssignedHours = $totalNonBillableAssignedHours + $overAllTotalAssignedHr;
                $totalNonBillableProjectHours = $totalNonBillableProjectHours + $overAllTotalProjectHr;
            @endphp
        </tr>
    @endif
        @php
            $j++;
        @endphp
@endforeach
@if($is_non_billable == 1)
    <tr><td colspan="10"></td></tr>
    <tr>
        <td colspan="6" class="text-bold text-right">Total Non-Billable Hours</td>
        <td class="text-bold">{{($totalNonBillableLoggedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableLoggedHours):0}}</td>
        <td class="text-bold">{{($totalNonBillableProjectLoggedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableProjectLoggedHours):0}}</td>
        @if(!Auth::user()->hasRole('developer'))
            <td class="text-bold">{{($totalNonBillableAssignedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableAssignedHours):0}}</td>
        @endif
        <td class="text-bold">{{($totalNonBillableProjectHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalNonBillableProjectHours):0}}</td>
    </tr>
    <tr><td colspan="10"></td></tr>
@endif

@php
    $totalOverAllLoggedHours = $totalNonBillableLoggedHours + $totalBillableLoggedHours;
    $totalOverAllProjectLoggedHours = $totalNonBillableProjectLoggedHours + $totalBillableProjectLoggedHours;
    $totalOverAllAssignedHours = $totalNonBillableAssignedHours + $totalBillableAssignedHours;
    $totalOverAllProjectHours = $totalNonBillableProjectHours + $totalBillableProjectHours;
@endphp

<tr>
    <td colspan="6" class="text-bold text-right">Overall</td>
    <td class="text-bold">{{($totalOverAllLoggedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalOverAllLoggedHours):0}}</td>
    <td class="text-bold">{{($totalOverAllProjectLoggedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalOverAllProjectLoggedHours):0}}</td>
    @if(!Auth::user()->hasRole('developer'))
        <td class="text-bold">{{($totalOverAllAssignedHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalOverAllAssignedHours):0}}</td>
    @endif
    <td class="text-bold">{{($totalOverAllProjectHours != 0)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalOverAllProjectHours):0}}</td>
</tr>