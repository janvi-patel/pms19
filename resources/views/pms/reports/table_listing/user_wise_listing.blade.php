<table id="project_report_table" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>User Name</th>
            <th>Project Name</th>
            <th>Task Name</th>
            <th>Assigned hours</th>
            <th>Logged Hours</th>
            <th>Total Logged Hours</th>
            <th>Total Assigned Hours</th>
            <th>Project Hours</th>
        </tr>
    </thead>
    <tbody>
        @php
            $overAllTaskHours = $overAllTotalHours = $overAllLoggedHours = $overAllProHours = $overAllAssignedHours = 0;
            $test = array();
        @endphp
        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))
            @if(isset($request->search_by_reporting_to) && ($request->search_by_reporting_to != ''))
                @if(isset($req_user_team_name) && count($req_user_team_name)>0)
                    @php
                        $userIdListing = $req_user_team_name;
                    @endphp
                @else
                    @php
                        $userIdListing = $teamUserName;
                    @endphp
                @endif
            @else
                @php
                    $userIdListing = $teamUserName;
                @endphp
            @endif
        @else
            @php
                $userIdListing = $teamUserName;
            @endphp
        @endif
        @foreach ($userIdListing as $key => $val)
            @php
                $pro = 0;
                $row_count = 0;
                $userIdArray['project_id'] = [];      
                $getTotalProHours = $getLoggedHr = $getTotalHours = $getAssignedHr = $getTotalTaskHours = 0;
            @endphp
            @foreach ($projectReportArray as $pro_id => $pro_user)
                @php
                    if(in_array($key,array_keys($pro_user)))
                    {
                        if($pro == 0){
                            $userIdArray['project_id'][$key] = array($pro_id);
                        }
                        else{
                            $userIdArray['project_id'][$key] = array_merge($userIdArray['project_id'][$key],array($pro=>$pro_id));   
                        }
                        $pro++;
                    }
                @endphp
            @endforeach
            @if(in_array($key,array_keys($userIdArray['project_id'])))
                @foreach ($userIdArray['project_id'][$key] as $project_id)
                    @foreach($projectEmpLoggedArray[$project_id] as $keys => $values)
                        @if($values['id'] == $key)
                            @php $row_count++; @endphp
                        @endif
                    @endforeach
                @endforeach
            @else
                @php $row_count = 1; @endphp
            @endif
            <tr class="{{$row_count}}"> 
                <td rowspan='{{$row_count}}'>{{ $loop->iteration }}</td>
                <th rowspan='{{$row_count}}'>{{ $val}}</th>
                @php
                    if(in_array($key,array_keys($userIdArray['project_id']))){
                        if(is_array($userIdArray['project_id'][$key])){
                            sort($userIdArray['project_id'][$key]);
                        }
                    }
                @endphp
                @if(in_array($key,array_keys($userIdArray['project_id'])))
                    @foreach($userIdArray['project_id'][$key] as $projectDataId)
                        @php
                            $projectData = (new \App\Helpers\CommonHelper)->getProjectData($projectDataId);
                            $estimatedHours = (isset($projectData->pms_hours) && $projectData->pms_hours!='')?$projectData->pms_hours:'0.00'; 
                            $crHours = (isset($projectCrHours[$projectDataId]) && $projectCrHours[$projectDataId]!='')?$projectCrHours[$projectDataId]:'0';
                            $getTotalProHours = $crHours + $estimatedHours;
                        @endphp
                        @php $totalHours = 0;$percentage = 0.00;$totalTaskHours =0;$subArray = array();@endphp 
                        @foreach($projectEmpLoggedArray[$projectDataId] as $k => $v)
                            @if($v['id'] == $key)
                                @php    $subArray[$v['id']]['user_id'][] = $v['id'];
                                        $subArray[$v['id']]['task_name'][] = isset($v['task_name'])?$v['task_name']:'Old Entry';
                                        $subArray[$v['id']]['taskHours'][] = isset($v['taskHours'])?$v['taskHours']:'0.00';
                                        $subArray[$v['id']]['loggedHours'][] = $v['total_logged_hours']; 
                                        $subArray[$v['id']]['taskId'][] = isset($v['taskId'])?$v['taskId']:'0';
                                @endphp
                            @endif
                        @endforeach
                        @if(count($subArray)>0)
                            @foreach($subArray as $subArrayKey => $subArrayVal)
                                @php $row_task_count = count($subArrayVal['task_name']); @endphp
                            @endforeach
                        @endif
                        <td rowspan='{{$row_task_count}}'>{{$projectData->project_name}}</td>
                        @if(count($subArray) == 0)
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                        @if(count($subArray)>0)
                            @foreach($subArray as $subArrayKey => $subArrayVal)
                                @for ($i = 0; $i < count($subArrayVal['task_name']); $i++)
                                    @php
                                        $totalTaskHours += ($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i];
                                        $totalHours += ($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i];
                                        $getTotalTaskHr = (new \App\Helpers\CommonHelper)->getUserTaskToatalHr($projectDataId,$subArrayVal['user_id'][$i],$subArrayVal['taskId'][$i]); 
                                    @endphp
                                        <td>{{($subArrayVal['task_name'][$i] != '')?$subArrayVal['task_name'][$i]:'Old Entry'}}</td>
                                        <td>{{($subArrayVal['taskHours'][$i] == '')?'0.00':$subArrayVal['taskHours'][$i]}}</td>
                                        <td>{{($subArrayVal['loggedHours'][$i] == '')?'0.00':$subArrayVal['loggedHours'][$i]}}</td>
                                        <td>{{$getTotalTaskHr->user_logged_hours}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @endfor
                            @endforeach
                        @endif
                @php
                    // user wise hours
                        $getTotalTaskHours = $getTotalTaskHours + $totalTaskHours;
                        $getTotalHours = $getTotalHours + $totalHours;
                        $totalLogged = (isset($projectLoggedArray[$projectDataId][0]) && $projectLoggedArray[$projectDataId][0] != 0)?$projectLoggedArray[$projectDataId][0] : 0;
                        $getLoggedHr = $getLoggedHr + $totalLogged;
                        $totalAssigned = (isset($projectEstimatedArray[$projectDataId][0]) && $projectEstimatedArray[$projectDataId][0] != 0) ? $projectEstimatedArray[$projectDataId][0] : 0;
                        $getAssignedHr = $getAssignedHr + $totalAssigned;
                        $overAllProHours = $overAllProHours + $getTotalProHours;
                @endphp
                    </tr>
                @endforeach
                @php
                    //over all total hours
                        $overAllTaskHours = $overAllTaskHours + $getTotalTaskHours;
                        $overAllTotalHours = $overAllTotalHours + $getTotalHours;
                        $overAllLoggedHours = $overAllLoggedHours + $getLoggedHr;
                        $overAllAssignedHours = $overAllAssignedHours + $getAssignedHr;
                @endphp
                @else
                <td>---</td>
                <td>---</td>
                <td>0.0</td>
                <td>0.0</td>
                <td>0.0</td>
                <td></td>
                <td></td>
                @endif
                <tr><td colspan="9"></td></tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>Total</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalTaskHours)}}</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getTotalHours)}}</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getLoggedHr)}}</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getAssignedHr)}}</b></td>
                        <td><b>{{sprintf("%.2f",$getTotalProHours)}}</td>
                    </tr>
                <tr><td colspan="9"></td></tr>
                @php
                    $test[] = $userIdArray;
                @endphp
        @endforeach
            <tr><td colspan="9"></td></tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>Total</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTaskHours)}}</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalHours)}}</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllLoggedHours)}}</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllAssignedHours)}}</b></td>
                    <td><b>{{sprintf("%.2f",$overAllProHours)}}</td>
                </tr>
            <tr><td colspan="9"></td></tr>
    </tbody>
</table>