@extends('layouts.master')
@section('moduleName')
    Report
@endsection
@section('content')
@php
    $departmentArray = config('constant.department');
    $pageRangArray = config('constant.page_range');
    $internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Leave Report</h1> -->
            </div>
            <div class="col-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Project Hours Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<?php 
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <form class="form-horizontal" method="get" action="{{url('/reports/project_hours')}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search Project</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>            
                <div class="card-body">
                    <div class="row">
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                            @if(isset($internalCompany) && count($internalCompany) > 0)        
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Department</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_department[]" id="search_by_department" multiple="multiple">
                                        <option value="">Select Department</option>  
                                            @if(isset($departmentArray) && !empty($departmentArray))
                                                @foreach($departmentArray as $key => $val)
                                                    @if(!isset($request->search_by_department))
                                                        <option selected="selected" value="{{ $key }}">{{ $val }}</option>
                                                    @else
                                                        <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Internal Company</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                        <option value="">Select Internal Company</option> 
                                        @if(isset($internalCompany) && !empty($internalCompany))
                                            @foreach($internalCompany as $key => $val)
                                                <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                            @endforeach
                                        @endif
                                        <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                    </select>
                                </div>
                            @endif
                        @endif
                        @if(!Auth::user()->hasRole('developer'))
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">Project Name</label>
                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_name" id="search_by_project_name">
                                <option value="">Select</option>
                                @if(!empty($projectData))      
                                    @foreach($projectData as $projectDataVal)
                                        <option value="{{$projectDataVal->id}}" {{(isset($request->search_by_project_name) && ($request->search_by_project_name == $projectDataVal->id))?'selected':''}}>{{$projectDataVal->project_name}}</option>
                                    @endforeach
                                @endif
                            </select>  
                        </div>  
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">User Name</label>
                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_user_name" id="search_by_user_name">
                                <option value="">Select</option>
                                @if(!empty($teamUserName))      
                                    @foreach($teamUserName as $key => $val)
                                        <option value="{{$key}}" {{(isset($request->search_by_user_name) && ($request->search_by_user_name == $key))?'selected':''}}>{{$val}}</option>
                                    @endforeach
                                @endif
                            </select>  
                        </div>
                        @endif
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">Month</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_month" id="search_month">
                                @foreach(config('constant.months') as $key => $value)
                                @if(isset($request->search_month)&& $request->search_month != '')
                                    <option class="1" value="{{$key}}" {{((isset($request->search_month) && $request->search_month == $key))?'selected':''}}>{{$value}} </option>
                                @else
                                     <option class="2"  value="{{$key}}" {{((date('m') == $key))?'selected':''}}>{{$value}} </option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">Year</label>
                            <select name="financial_year" id="financial_year"
                                class="form-control w-85 select2 custom-select" style="width: 100%; height:36px;">
                                <option value="">Select Year </option>
                                @if(count($financialYear)>0)
                                @foreach($financialYear as $key => $year)
                                <option value="{{$year}}"
                                        <?PHP echo (isset($request->financial_year) && $year == $request->financial_year )|| ((count($financialYear)-1) && !isset($request->financial_year))?'selected':''?>>
                                    {{$year}}
                                </option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))
                        <div class="form-group col-md-3 col-sm-6">                                
                            <label>Reporting To</label>
                            <?php asort($reportingToList);?>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_reporting_to" id="search_by_reporting_to" >
                                <option value="">Select Reporting Person</option>  
                                    @if(isset($reportingToList) && !empty($reportingToList))
                                        @foreach($reportingToList as $reportingDataVal)
                                            @if(isset($reportingDataVal['id']) && $reportingDataVal['id'] != "")
                                                <option value="{{ $reportingDataVal['id'] }}" {{(isset($request->search_by_reporting_to) && ($request->search_by_reporting_to == $reportingDataVal['id']))?'selected':''}}>{{ $reportingDataVal['first_name'].' '.$reportingDataVal['last_name'] }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                            </select>
                        </div>
                        @endif
                        @if(!(Auth::user()->hasRole('developer')))
                            <div class="form-group col-md-3 col-sm-6">
                                <label>Hours Type</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_project_hours_type" id="search_by_project_hours_type">
                                    <option value="">Select All</option>
                                    <option value="billable_hours" {{(isset($request->search_by_project_hours_type) && ($request->search_by_project_hours_type == "billable_hours"))?'selected':''}}>Billable Hours</option>
                                    <option value="non_billable_hours" {{(isset($request->search_by_project_hours_type) && ($request->search_by_project_hours_type == "non_billable_hours"))?'selected':''}}>Non Billable Hours</option> 
                                </select>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/project_hours')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row"> 
                                <div class="col-md-12">                        
                                <div class="table-responsive">
                                    @if (count($projectReportArray)>0)
                                    <table id="project_report_table" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@sortablelink('project_name','Project Name')</th>
                                                <th>Logged Date</th>
                                                <th>Task Name</th>
                                                <th>User Name</th>
                                                <th>Assigned hours</th>
                                                <th>Logged Hours</th>
                                                <th>Total Logged Hours</th>
                                                @if(!Auth::user()->hasRole('developer'))
                                                    <th>Total Assigned Hours</th>
                                                @endif
                                                <th>Project Hours</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @include('pms.reports.table_listing.project_hours')
                                        </tbody>
                                    </table>
                                    @else
                                    <div class="border-top">
                                        <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                        
    </div>
    </div>
</section>

@endsection

@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
   
@endsection
