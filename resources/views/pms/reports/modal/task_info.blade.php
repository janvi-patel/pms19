<div class="modal-dialog modal-lg" id="taskInfoModal">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Task Details</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>
            <div class="modal-body" id="modal-body">
                <div class="card-body">
                    <div class="table-responsive">
                        @if(!$taskInfo->isEmpty())
                        <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-4">
                                Showing {{ $taskInfo->firstItem() }} to {{ $taskInfo->lastItem() }} of total {{$taskInfo->total()}} entries
                            </div>
                        </div>
                        <table id="" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Task</th>
                                    <th>Task Hours</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i= $taskInfo->firstItem();$totalTaskHours = 0.00; 
                                @endphp
                            @foreach($taskInfo  as $val)
                                @php
                                    $totalTaskHours = $totalTaskHours + $val->estimated_hours;
                                @endphp
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$val->task_name}}</td>
                                    <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($val->estimated_hours)}}</td>
                                </tr>
                                @php $i++; @endphp
                            @endforeach
                            <tr>
                                <td></td>
                                <td><b>Total</b></td>
                                <td>{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalTaskHours)}}</td>
                            </tr>
                            </tbody>
                        </table>
                        @else
                        <span>No Data Found!</span>
                        @endif
                    </div>
                    @if(!$taskInfo->isEmpty())
                        <div class="pt-3">{!! $taskInfo->appends(\Request::except('page'))->render() !!}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
