@extends('layouts.master')
@section('moduleName')
    Report
@endsection
@section('content')
@php
    $pageRangArray = config('constant.page_range');
    $departmentArray = config('constant.department');
    $internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Leave Report</h1> -->
            </div>
            <div class="col-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Project Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<?php
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <form class="form-horizontal" method="get" action="{{url('/reports/projects')}}" name="search_filter" id="search_filter">
            <input type="hidden" id="submit_type" value="search" name="submit_type">
            <div class="card <?php echo $search_class;?>" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search Project</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                            @if(isset($internalCompany) && count($internalCompany) > 0)
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Department</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_department[]" id="search_by_department" multiple="multiple">
                                        <option value="">Select Department</option>  
                                            @if(isset($departmentArray) && !empty($departmentArray))
                                                @foreach($departmentArray as $key => $val)
                                                    @if(!isset($request->search_by_department))
                                                        <option selected="selected" value="{{ $key }}">{{ $val }}</option>
                                                    @else
                                                        <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Internal Company</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                        <option value="">Select Internal Company</option> 
                                        @if(isset($internalCompany) && !empty($internalCompany))
                                            @foreach($internalCompany as $key => $val)
                                                <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                            @endforeach
                                        @endif
                                        <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                    </select>
                                </div>
                            @endif
                        @endif
                        @if(!(Auth::user()->hasRole('developer')))
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">Project Name</label>
                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_name" id="search_by_project_name">
                                <option value="">Select</option>
                                @if(!empty($projectData))
                                    @foreach($projectData as $projectDataVal)
                                        <option value="{{$projectDataVal->id}}" {{(isset($request->search_by_project_name) && ($request->search_by_project_name == $projectDataVal->id))?'selected':''}}>{{$projectDataVal->project_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">User Name</label>
                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_user_name" id="search_by_user_name">
                                <option value="">Select</option>
                                @if(!empty($teamUserName))
                                    @foreach($teamUserName as $key => $val)
                                        <option value="{{$key}}" {{(isset($request->search_by_user_name) && ($request->search_by_user_name == $key))?'selected':''}}>{{$val}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @endif
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">Project Start Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_by_start_date" id="search_by_start_date" value="{{(isset($request->search_by_start_date)?$request->search_by_start_date:date('Y-m-01'))}}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">Project End Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_by_end_date" id="search_by_end_date" value="{{(isset($request->search_by_end_date)?$request->search_by_end_date:date('Y-m-t'))}}"  class="form-control" readonly />
                            </div>
                        </div>
                        @if(!(Auth::user()->hasRole('developer')))
                        <div class="form-group col-md-3 col-sm-6">
                            <label>Project Status</label>
                            <select name="search_project_status" id="search_project_status" class="form-control">
                                <option value="">Select Project Status</option>
                                <option value="0" <?php echo (!isset($request->search_project_status) || $request->search_project_status == 0)?'selected':''?>>All</option>
                                <?php
                                $pstatus = config('constant.project_status');
                                asort($pstatus);
                                ?>
                                @if(config('constant.project_status') && !empty(config('constant.project_status')))
                                @foreach($pstatus as $key => $projectStatusVal)
                                <option value="{{$key}}" <?PHP echo (isset($request->search_project_status) && $key == $request->search_project_status )?'selected':''?>>{{$projectStatusVal}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        @endif
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))
                        <div class="form-group col-md-3 col-sm-6">
                            <label>Reporting To</label>
                            <?php asort($reportingToList);?>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_reporting_to" id="search_by_reporting_to" >
                                <option value="">Select Reporting Person</option>
                                    @if(isset($reportingToList) && !empty($reportingToList))
                                        @foreach($reportingToList as $reportingDataVal)
                                            @if(isset($reportingDataVal['id']) && $reportingDataVal['id'] != "")    
                                                <option value="{{ $reportingDataVal['id'] }}" {{(isset($request->search_by_reporting_to) && ($request->search_by_reporting_to == $reportingDataVal['id']))?'selected':''}}>{{ $reportingDataVal['first_name'].' '.$reportingDataVal['last_name'] }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                            </select>
                        </div>
                        @endif
                        @if(!(Auth::user()->hasRole('developer')))
                        <div class="form-group col-md-3 col-sm-6">
                            <label>Hours Type</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_project_hours_type" id="search_by_project_hours_type">
                                <option value="">Select All</option>
                                <option value="billable_hours" {{(isset($request->search_by_project_hours_type) && ($request->search_by_project_hours_type == "billable_hours"))?'selected':''}}>Billable Hours</option>
                                <option value="non_billable_hours" {{(isset($request->search_by_project_hours_type) && ($request->search_by_project_hours_type == "non_billable_hours"))?'selected':''}}>Non Billable Hours</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label>Listing Type</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_listing_type" id="search_by_listing_type">
                                <option value="">Default</option>
                                <option value="project_wise" {{(isset($request->search_by_listing_type) && ($request->search_by_listing_type == "project_wise"))?'selected':''}}>Project Wise</option>
                                <option value="user_wise" {{(isset($request->search_by_listing_type) && ($request->search_by_listing_type == "user_wise"))?'selected':''}}>User Wise</option>
                            </select>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/projects')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex justify-content-between">
                                @if (count($projData)>0)
                                    <div class="left w-100 d-flex mb-3 mb-sm-0">
                                        <div class="pl-3">

                                        </div>
                                    </div>
                                    <div class="right">
                                       <a href="#" title="Download Excel" id="export_to_excel">
                                        @if(isset($request->search_by_listing_type) && ($request->search_by_listing_type == "user_wise"))
                                        @else
                                            <button type="button" class="btn btn-dark">Export to Excel</button>
                                        @endif
                                       </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    @if (count($projectReportArray)>0)
                                        @if(!(Auth::user()->hasRole('developer')) && isset($request->search_by_listing_type) && ($request->search_by_listing_type == "user_wise") && !(isset($request->search_by_user_name)))
                                            @include('pms.reports.table_listing.user_wise_listing')
                                        @else
                                            @include('pms.reports.table_listing.project_wise_listing')
                                        @endif
                                    @else
                                    <div class="border-top">
                                        <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
<div id="taskInfo" class="modal fade" role="dialog">

</div>
@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
@endsection
