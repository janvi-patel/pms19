<div class="card-body">
    <div class="table-responsive">
        <table id="holiday_listing" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th><b>Sr_No</b></th>
                    <th><b>Emp_Code</b></th>
                    <th><b>Name</b></th>
                    @if(count($dates)>0)
                        @foreach($dates as $v)
                            <th><b>{{$v}}</b></th>
                        @endforeach
                    @endif
                    <th><b>Total No of day</b></th>
                    <th><b>Week Off</b></th>
                    <th><b>Total Working days</b></th>
                    <th><b>Total Worked days</b></th>
                    <th><b>Credit Leave C/F</b></th>
                    <th><b>Leave for the month</b></th>
                    <th><b>Balance Leave</b></th>
                    <th><b>Comp Off</b></th>
                    <th><b>Balance Leave After Compoff</b></th>
                    <th><b>This month LWP</b></th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th><b>Day</b></th>
                    @if(count($days)>0)
                        @foreach($days as $d)
                            <th><b>{{$d}}</b></th>
                        @endforeach
                    @endif
                    <th>-</th>
                    <th>-</th>
                    <th>-</th>
                    <th>-</th>
                    <th>-</th>
                    <th>-</th>
                    <th>-</th>
                    <th>-</th>
                    <th>-</th>
                    <th>-</th>
                </tr>
            </thead>
            <tbody>
            @php $i = 1; @endphp
            @if(count($excelData)>0)
                @foreach($excelData as $emp_code => $data)
                <tr>
                    <td><b>{{$i}}</b></td>
                    <td><b>{{$data['empCode']}}</b></td>
                    <td><b>{{$data['name']}}</b></td>
                    @php $totalWorkedDay = 0; $leaveOfMonth = 0; @endphp
                    @for($j =0 ;$j<count($dates); $j++)
                        @if((new \App\Helpers\CommonHelper)->is_holiday($dates[$j]) != true)
                            @if(isset($data[$dates[$j]]))
                                @php $weekDay = date('w', strtotime($dates[$j])); @endphp
                                    @if($weekDay != 0 && $weekDay != 6)
                                        @if($data[$dates[$j]]['full_day'] == 1)
                                            @php $totalWorkedDay = $totalWorkedDay + 1; @endphp
                                        <td>1.0</td>
                                        @elseif($data[$dates[$j]]['incorrect_entry'] == 1)
                                            @php $leaveOfMonth = $leaveOfMonth + 1; @endphp
                                            <td style="background-color: #ffa600;">0</td>
                                        @elseif ($data[$dates[$j]]['half_day'] == 1)
                                            @php 
                                                $totalWorkedDay = $totalWorkedDay + 0.5;
                                                $leaveOfMonth = $leaveOfMonth + 0.5; 
                                            @endphp
                                            <td>0.5</td>
                                        @elseif ($data[$dates[$j]]['absent'] == 1)
                                        @php $leaveOfMonth = $leaveOfMonth + 1; @endphp
                                            <td>0.0</td>
                                        @endif
                                    @else
                                        <td>Off</td>
                                    @endif
                                @else
                                <td>0.0</td>
                                @endif
                            @else
                            <td>Off</td>
                            @endif
                    @endfor
                        <td><b>{{count($days)}}</b></td>
                        <td><b>{{$weekOff}}</b></td>
                        <td><b>{{count($days)- $weekOff}}</b></td>
                        <td><b>{{$totalWorkedDay}}</b></td>
                        @if($userLeaveData[$emp_code]['creditLeave']<=0)
                        <td style="background-color: #FFFF00;"><b>{{$userLeaveData[$emp_code]['creditLeave']}}</b></td>
                        @else
                            <td><b>{{$userLeaveData[$emp_code]['creditLeave']}}</b></td>
                        @endif
                        <td><b>{{$leaveOfMonth}}</b></td>
                         @if($userLeaveData[$emp_code]['creditLeave'] - $leaveOfMonth<=0)
                        <td style="background-color: #FFFF00;"><b>{{($userLeaveData[$emp_code]['creditLeave'] - $leaveOfMonth)}}</b></td>
                          @else
                           <td><b>{{($userLeaveData[$emp_code]['creditLeave'] - $leaveOfMonth)}}</b></td>
                        @endif
                        <td><b>{{$userLeaveData[$emp_code]['compoffDay']}}</b></td>
                        <td><b>{{($userLeaveData[$emp_code]['creditLeave'] >= $leaveOfMonth)?(($userLeaveData[$emp_code]['creditLeave'] - $leaveOfMonth)+ $userLeaveData[$emp_code]['compoffDay']):'0.00'}}</b></td>
                        @if ((($userLeaveData[$emp_code]['creditLeave'] - $leaveOfMonth) + $userLeaveData[$emp_code]['compoffDay']) < 0)
                        <td style="background-color: #FFFF00;"><b>{{ ($userLeaveData[$emp_code]['creditLeave'] - $leaveOfMonth) + $userLeaveData[$emp_code]['compoffDay'] }}</b></td>
                        @endif
                </tr>
                @php $i++; @endphp
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
