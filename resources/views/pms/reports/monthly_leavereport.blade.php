@extends('layouts.master')
@section('moduleName')
    Report
@endsection
@section('content')
@php
$pageRangArray = config('constant.page_range');
$departmentArray = config('constant.department');
$internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Leave Report</h1> -->
            </div>
            <div class="col-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Monthly Leave Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <form class="form-horizontal" method="get" action="{{url('/reports/monthly-leave')}}" name="search_filter" id="search_filter">
            <input type="hidden" id="submit_type" value="search" name="submit_type">
            <div class="card" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search</h3>
                </div> 
                <div class="card-body">
                    <div class="row">
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                            @if(isset($internalCompany) && count($internalCompany) > 0)
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Department</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_department[]" id="search_by_department" multiple="multiple">
                                        <option value="">Select Department</option>  
                                            @if(isset($departmentArray) && !empty($departmentArray))
                                                @foreach($departmentArray as $key => $val)
                                                    @if(!isset($request->search_by_department))
                                                        <option selected="selected" value="{{ $key }}">{{ $val }}</option>
                                                    @else
                                                        <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Internal Company</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                        <option value="">Select Internal Company</option> 
                                        @if(isset($internalCompany) && !empty($internalCompany))
                                            @foreach($internalCompany as $key => $val)
                                                <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                            @endforeach
                                        @endif
                                        <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                    </select>
                                </div>
                            @endif
                        @endif
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_company" id="search_by_company" >
                                    <option value="">All Companies</option>
                                        @foreach($companyList as $companyDataVal)
                                            <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_company) && ($request->search_by_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                        @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Month</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_month" id="search_by_month" >
                                    @foreach($months as $key => $month)
                                    <option value="{{ $key }}" 
                                    @if($request->has('search_by_month') && $request->search_by_month != '')
                                    {{ (isset($request->search_by_month) && ( $key == $request->search_by_month))?'selected':''  }}
                                    @else 
                                    {{ $key == $now['month']?'selected':''}}
                                    @endif >{{ $month }}</option>
                                    @endforeach
                            </select>
                        </div> 
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Year</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_year" id="search_by_year" >
                                    @foreach($years as $key => $year)
                                        <option value="{{ $key }}" 
                                    @if($request->has('search_by_year') && $request->search_by_year != '')
                                    {{ (isset($request->search_by_year) && ( $key == $request->search_by_year))?'selected':''  }}
                                    @else 
                                    {{ $key == $now['year']?'selected':''}}
                                    @endif> {{ $year }} </option>
                                    @endforeach
                            </select>
                        </div>
                         <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Select Report Type</label>
                            <select class="form-control select2 search_report_type" style="width: 100%; height:36px;" name="search_report_type" id="search_report_type" >
                                    <option value="time_sheet_report">Time Sheet Report</option>
                                    <option value="monthly_leave_report">Monthly Leave Report</option>
                            </select>
                        </div>
                    </div>
                </div>
                {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/monthly-leave')}}'">
                            Reset
                        </button>
                        <span class="btn btn-primary btn-process d-none sync_leave_button" data-btn-process="Sync Leave">
                            Sync Leave
                        </span>
                    </div>
                </div>    
            </div>
        </form>
    </div>
</section>

@endsection

@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
   <script>
    $(document).ready(function(){
        $(document).on("change",".search_report_type",function() {
            var selectedVal = $(this).val();
            if(selectedVal == "monthly_leave_report") {
                $(".sync_leave_button").removeClass("d-none");
            } else {
                $(".sync_leave_button").addClass("d-none");
            }
        });
    });

    $('.sync_leave_button').click(function(e) {
        e.preventDefault();
        $.ajax({
            type: 'get',
            url: "{{ route('monthly-leave-sync') }}",
            data: $("#search_filter").serialize(),
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            beforeSend: function() {
                $('.btn-process').attr("disabled", true).html('Processing...');
            },
            success: function(data) {
                if(data.status == 'success'){
                    swal(data.message,"", "success");
                    $(".sync_leave_button").addClass("d-none");
                } else if(data.status == 'error'){
                    swal(data.message,"", "error");
                }
            },
            error: function(data) {
                swal("Something went wrong!","", "error");
            },
            complete: function() {
                $('.btn-process').attr("disabled", false).html($('.btn-process').data('btn-process'));
            }
        });
    });
   </script>
@endsection
