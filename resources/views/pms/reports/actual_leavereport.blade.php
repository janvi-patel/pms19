@extends('layouts.master')
@section('moduleName')
    Report
@endsection
@section('content')
@php
$leaveStatusArray = config('constant.leave_status');
$pageRangArray = config('constant.page_range');
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Leave Report</h1> -->
            </div>
            <div class="col-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Actual Leave Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<?php 
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        @if(!Auth::user()->hasRole(config('constant.developer_slug')))
            <form class="form-horizontal" method="get" action="{{url('/reports/actual-leave')}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search Leave</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>            
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-4 col-sm-6">
                            <label class="">User</label>
                              <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee">
                                <option value="">All</option>
                                @foreach($userNameArray as $key => $userNameListVal)
                                    <option value="{{$key}}" {{(isset($request->search_by_employee) && ($request->search_by_employee == $key))?'selected':''}}>{{$userNameListVal}}</option>
                                @endforeach
                            </select>  
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-6">                        
                            <label>Leave Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_leave_date" id="search_leave_date" value="{{ $leave_start_date }}" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-6">                        
                            <label>Leave Date Rang</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_leave_start_date" id="search_leave_start_date" value="{{ $start_date }}" class="form-control"/>
                                <input type="text" name="search_leave_end_date" id="search_leave_end_date" value="{{$end_date}}" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-12">                        
                                <label>Filter Type</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_type" id="search_by_type" >
                                    <option value="">Select Column</option>
                                    <option value="0"  {{(isset($request->search_by_type) && ($request->search_by_type == 0) || !isset($request->search_by_type))?'selected':''}}>Name</option>
                                    <option value="1" {{(isset($request->search_by_type) && ($request->search_by_type == 1))?'selected':''}}>Leave Date</option>
                                </select>
                            </div>
                        
                        @if(Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin')))
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-12">
                            <label class="">Month</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_month" id="search_month">
                                <option value="">Select Month</option>   
                                @foreach(config('constant.months') as $key => $value)
                                    <option value="{{$key}}" {{((isset($request->search_month) && $request->search_month == $key) || (isset($currentMonth) && $currentMonth == $key))?'selected':''}}>
                                        {{$value}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                        <div class="form-group col-lg-4 col-md-4 col-sm-4 col-12">                        
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_company" id="search_by_company" >
                                    <option value="0">All Companies</option>
                                        @foreach($companyList as $companyDataVal)
                                            <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_company) && ($request->search_by_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                        @endforeach
                                </select>
                        </div>
                        @endif                       
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/actual-leave')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>
         @endif
        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 d-flex justify-content-between">
                            <div class="left"> 
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="width: 100%; height:36px;"  >
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div> 
                            </div>
                            
                             @if (count($leavesData)>0)                             
                                <div class="right show_total_no">
                                    Showing {{ $leavesData->firstItem() }} to {{ $leavesData->lastItem() }} of total {{$leavesData->total()}} entries
                                </div>
                             @endif
                        </div>
                        </div>
                        <div class="row"> 
                        <div class="col-md-12">                        
                        <div class="table-responsive">
                            @if (count($leavesData)>0)
                            <table id="upcoming_leave_listing" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        @if (isset($request->search_by_type) && ($request->search_by_type == 1))
                                            <th>@sortablelink('entry_date','Leave Date')</th>
                                            <th>@sortablelink('emp_name','Name')</th>
                                        @else
                                            <th>@sortablelink('emp_name','Name')</th>
                                            <th>@sortablelink('entry_date','Leave Date')</th>
                                        @endif
                                        <th>@sortablelink('half_day','Day')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    $j = $leavesData->firstItem();
                                    @endphp   
                                    
                                    @foreach($leavesData as  $key => $leaveDetail)
                                    <?php $rowCount = count($leaveDetail);
                                            $rowspan = 'rowspan='.$rowCount;?>
                                    <tr>
                                        <td <?php echo count($leaveDetail)>1?$rowspan:''?>>{{$j}}</td>
                                        <td <?php echo count($leaveDetail)>1?$rowspan:''?>> {{$key}}</td>
                                        @if(isset($rowCount)&& $rowCount == 0)
                                            <td></td>
                                            <td></td>
                                        @else
                                            @foreach($leaveDetail as $leaveDetailkey => $Val)
                                                @if (isset($request->search_by_type) && ($request->search_by_type == 1))
                                                    <td class="">{{$Val['emp_name']}}</td>
                                                @else
                                                    <td>{{(new \App\Helpers\CommonHelper)->displayDate($Val['entry_date'])}}</td>
                                                @endif
                                                <td>{{isset($Val['half_day'])?'Half Day':'Full Day'}}</td>
                                        </tr>
                                            @endforeach 
                                        @endif
                                    </tr>
                                    @php
                                    $i++;$j++;
                                    @endphp                             
                                    @endforeach 
                                </tbody>
                            </table>
                            @else
                            <div class="border-top">
                                <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                            </div>
                            @endif
                            @if($leavesData && !empty($leavesData))
                            <div class="pt-4" >{!! $leavesData->appends(\Request::except('page'))->render() !!}</div>
                             @endif 
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>                        
        </div>
    </div>
</section>

@endsection
@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
@endsection