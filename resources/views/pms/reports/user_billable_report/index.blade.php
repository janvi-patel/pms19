@extends('layouts.master')
@section('moduleName')
    Report
@endsection
@section('content')
@php
    $designationInclude = config('constant.designation');
    $pageRangArray = config('constant.page_range');
    $internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Leave Report</h1> -->
            </div>
            <div class="col-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Billable Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<?php 
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <form class="form-horizontal" method="get" action="{{url('/reports/billable_report')}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search Report</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>            
                <div class="card-body">
                    <div class="row">
                        @if ((Auth::check() &&
                                        (Auth::user()->hasRole('admin') ||
                                            Auth::user()->hasRole('hr') ||
                                            Auth::user()->hasRole(config('constant.superadmin_slug')))))

                                    
                                    <div class="form-group col-lg-5 col-md-6 col-sm-12">
                                        <label>Designation</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;"
                                                name="search_designation[]" id="search_designation" multiple="multiple">
                                            <option value="">Select Designation</option>
                                            
                                            @if (isset($designationList) && !empty($designationList))
                                                @foreach ($designationList as $key => $val)
                                                    @if (Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin')))
                                                            @if (in_array($val->id, $designationInclude) && !isset($request->search_designation))
                                                                <option selected="selected" value="{{ $val->id }}">
                                                                    {{ $val->designation_name }}</option>
                                                            @else
                                                                <option value="{{ $val->id }}"
                                                                    {{ isset($request->search_designation) && in_array($val->id, $request->search_designation) ? 'selected' : '' }}>
                                                                    {{ $val->designation_name }}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{ $val->id }}"
                                                                {{ isset($request->search_designation) && $request->search_designation == $val->id ? 'selected' : '' }}>
                                                                {{ $val->designation_name }}</option>
                                                        @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                @endif
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug'))) 
                            @if(isset($internalCompany) && count($internalCompany) > 0)
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Internal Company</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                        <option value="">Select Internal Company</option> 
                                        @if(isset($internalCompany) && !empty($internalCompany))
                                            @foreach($internalCompany as $key => $val)
                                                <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                            @endforeach
                                        @endif
                                        <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                    </select>
                                </div>
                            @endif
                        @endif
                        @if(!Auth::user()->hasRole('developer'))
                            <div class="form-group col-md-3 col-sm-6">
                                <label class="">User Name</label>
                                <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_user_name" id="search_by_user_name">
                                    <option value="">Select</option>
                                    @if(!empty($data['team_user_name']))      
                                        @foreach($data['team_user_name'] as $key => $val)
                                            <option value="{{$key}}" {{(isset($request->search_by_user_name) && ($request->search_by_user_name == $key))?'selected':''}}>{{$val}}</option>
                                        @endforeach
                                    @endif
                                </select>  
                            </div>
                        @endif
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))
                            <div class="form-group col-md-3 col-sm-6">                              
                                <label>Reporting To</label>
                                <?php asort($data['reporting_to_list']);?>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_reporting_to" id="search_by_reporting_to" >
                                    <option value="">Select Reporting Person</option>  
                                        @if(isset($data['reporting_to_list']) && !empty($data['reporting_to_list']))
                                            @foreach($data['reporting_to_list'] as $reportingDataVal)
                                                @if(isset($reportingDataVal['id']) && $reportingDataVal['id'] != "")
                                                    <option value="{{ $reportingDataVal['id'] }}" {{(isset($request->search_by_reporting_to) && ($request->search_by_reporting_to == $reportingDataVal['id']))?'selected':''}}>{{ $reportingDataVal['first_name'].' '.$reportingDataVal['last_name'] }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                </select>
                            </div>
                        @endif
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">Start Date</label><span class="ml-2 text-primary mdi mdi-information-outline information" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" data-title="It will consider fisrt date of selected month!"></span>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_by_start_date" id="search_by_start_date" value="{{(isset($request->search_by_start_date)?$request->search_by_start_date:date('Y-m-01'))}}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="form-group col-md-3 col-sm-6">
                            <label class="">End Date</label><span class="ml-2 text-primary mdi mdi-information-outline information" data-toggle="tooltip" data-toggle="tooltip" data-placement="top" data-title="It will consider last date of selected month!"></span>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_by_end_date" id="search_by_end_date" value="{{(isset($request->search_by_end_date)?$request->search_by_end_date:date('Y-m-t'))}}"  class="form-control" readonly />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/billable_report')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-4 mb-0"> 
                                <div class="form-group col pl-0 top-pagination">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div> 
                                @if(!empty($data['result']))
                                    <p class="mt-sm-1 mt-0">Showing {{ $data['result']->firstItem() }} to {{ $data['result']->lastItem() }} of total {{$data['result']->total()}} entries</p>
                                @endif
                            </div>
                            <div class="col-md-8 mb-4"> 
                                <div class="row justify-content-end">
                                    <div class="col-lg-2 pr-0 top_update_checked_user">
                                        <a title="Download Excel" id="export_to_excel">
                                            <button type="button" class="btn btn-primary btn-dark btn-block padding-5">Export to Excel</button>
                                        </a>
                                    </div>
                                </div>
                            </div>                  
                        </div> 
                        <div class="row"> 
                                <div class="col-md-12">                        
                                <div class="table-responsive">
                                    <table id="project_report_table" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@sortablelink('first_name','User Name')</th>
                                                <th>@sortablelink('employee_id','User Code')</th>
                                                <th>@sortablelink('billable_hour','Billable Hours')</th>
                                                <th>@sortablelink('logged_hours','Billable Logged Hours')</th>
                                                <th>@sortablelink('non_billable_hours','Non-Billable Hours')</th>
                                                <th>@sortablelink('non_billable_logged_hours','Non-Billable Logged Hours')</th>
                                                <th>Total Hours</th>
                                                <th>Total Logged Hours</th>
                                                <th>Total Working Days</th>
                                                <th>Total Hours as per Working Days</th>
                                                <th>@sortablelink('billability_percentage', 'Billability Percentage')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($data['result']) > 0)
                                            <?php  $j = $data['result']->firstItem(); ?>
                                                @foreach ($data['result'] as $result)
                                                    <tr>
                                                        <td>{{ $j }}</td>
                                                        <td>{{ $result->first_name ." ".$result->last_name }}</td>
                                                        <td>{{ $result->company->emp_short_name ."". $result->employee_id }}</td>
                                                        <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result->billable_hour) }}</td>
                                                        <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result->logged_hours) }}</td>
                                                        <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result->non_billable_hours) }}</td>
                                                        <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result->non_billable_logged_hours) }}</td>
                                                        <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result->billable_hour + $result->non_billable_hours) }}</td>
                                                        <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result->logged_hours + $result->non_billable_logged_hours) }}</td>
                                                        <td>{{ $result->working_days }}</td>
                                                        <td>{{ ($result->working_days*8) }}</td>
                                                        @if(($result->working_days*8) > 0)
                                                            <td>{{ number_format($result->billability_percentage,2).' %' }}</td>
                                                        @else
                                                            <td>--</td>
                                                        @endif
                                                    </tr>
                                                    @php
                                                        $j++;   
                                                    @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="12" class="text-center">No Records found!</td>
                                                </tr>
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                    @if(isset($data['result']) && count($data['result']) > 0)
                                        <div class="pt-4">{!! $data['result']->appends(\Request::except('page'))->render() !!}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                        
    </div>
    </div>
</section>

@endsection

@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
   <script>
        $( document ).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });

        $('#export_to_excel').on('click', function () {
            var link  = window.location.pathname;
            var query_string = window.location.search;
            if(query_string != '')
            {
                var redirectLink = link+query_string+'&'+'submit_type=export_excel';
            }
            else{
                var redirectLink = link +'?'+'submit_type=export_excel';
            }
            window.location = redirectLink;
        });

        $('#page_range_dropdown').change(function(){
            var limit = $(this).val();
            var link  = window.location.pathname;
            var query_string = window.location.search;

            var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
            if(limit != '' && query_string.indexOf('search_submit') != -1){
                if(query_string.indexOf('page_range')==1){
                    var page_range = query_string.slice(0,query_string.indexOf('='));
                    var srch_str = query_string.slice(query_string.indexOf('&'));
                    var redirectLink = link +page_range+'='+limit+srch_str;
                }else{
                    var redirectLink = link +'?page_range='+limit + '&'+search_str;
                }
            }else{
                var redirectLink = link +'?page_range='+limit;
            }
            window.location = redirectLink;

        });
   </script>
   
@endsection
