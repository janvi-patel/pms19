<div class="card-body">
    <div class="table-responsive">
        <table id="holiday_listing" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>User Name</th>
                    <th>User Code</th>
                    <th>Billable Hours</th>
                    <th>Billable Logged Hours</th>
                    <th>Non-Billable Hours</th>
                    <th>Non-Billable Logged Hours</th>
                    <th>Total Hours</th>
                    <th>Total Logged Hours</th>
                    <th>Total Working Days</th>
                    <th>Total Hours as per Working Days</th>
                    <th>Billability Percentage</th>
                </tr>
            </thead>
            <tbody>
                @if(count($data['result']) > 0)
                    @foreach ($data['result'] as $result)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $result['first_name'] ." ".$result['last_name'] }}</td>
                            <td>{{ $result['company']['emp_short_name'] ."". $result['employee_id'] }}</td>
                            <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result['billable_hour']) }}</td>
                            <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result['logged_hours']) }}</td>
                            <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result['non_billable_hours']) }}</td>
                            <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result['non_billable_logged_hours']) }}</td>
                            <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result['billable_hour'] + $result['non_billable_hours']) }}</td>
                            <td>{{ (new \App\Helpers\CommonHelper)->displayTaskTime($result['logged_hours'] + $result['non_billable_logged_hours']) }}</td>
                            <td>{{ $result['working_days'] }}</td>
                            <td>{{ ($result['working_days']*8) }}</td>
                            <?php 
                                $totalAsssignHours = $result['billable_hour'];
                                $totalLoggedHours = $result['logged_hours'] + $result['non_billable_logged_hours'];
                                $totalWorkingDaysHours = $result['working_days']*8;
                                $totalDividedHours = ($totalLoggedHours > $totalWorkingDaysHours)?$totalLoggedHours: $totalWorkingDaysHours;
                            ?>
                            @if($totalDividedHours > 0)
                            <td>{{ number_format(($totalAsssignHours * 100) / $totalDividedHours,2).' %' }}</td>
                            @else
                            <td>--</td>
                            @endif
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="12" class="text-center">No Records found!</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
