@extends('layouts.master')
@section('moduleName')
    Report
@endsection
@section('content')
@php
$leaveStatusArray = config('constant.leave_status');
$userStatusArray = config('constant.user_status');
$pageRangArray = config('constant.page_range');
$departmentArray = config('constant.department');
$internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Leave Report</h1> -->
            </div>
            <div class="col-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Detail Leave Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<?php
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        @if(!Auth::user()->hasRole(config('constant.developer_slug')))
            <form class="form-horizontal" method="get" action="{{url('/reports/detail-leave')}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search Leave</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                            @if(isset($internalCompany) && count($internalCompany) > 0)
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Department</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_department[]" id="search_by_department" multiple="multiple">
                                        <option value="">Select Department</option>  
                                            @if(isset($departmentArray) && !empty($departmentArray))
                                                @foreach($departmentArray as $key => $val)
                                                    @if(!isset($request->search_by_department))
                                                        <option selected="selected" value="{{ $key }}">{{ $val }}</option>
                                                    @else
                                                        <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Internal Company</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                        <option value="">Select Internal Company</option> 
                                        @if(isset($internalCompany) && !empty($internalCompany))
                                            @foreach($internalCompany as $key => $val)
                                                <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                            @endforeach
                                        @endif
                                        <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                    </select>
                                </div>
                            @endif
                        @endif
                        <div class="form-group col-lg-4 col-md-4 col-sm-6">
                            <label class="">User</label>
                              <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee">
                                <option value="">All</option>
                                @foreach($userNameArray as $key => $userNameListVal)
                                    <option value="{{$key}}" {{(isset($request->search_by_employee) && ($request->search_by_employee == $key))?'selected':''}}>{{$userNameListVal}}</option>
                                @endforeach
                            </select>
                        </div>
                    <div class="form-group col-lg-4 col-md-4 col-sm-6">
                        <label>Status</label>
                        <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_status" id="search_by_status" >
                            <option value="">Select status</option>
                                @if(isset($userStatusArray) && !empty($userStatusArray))
                                    @foreach($userStatusArray as $key => $userStatusVal)
                                        <option value="{{ $key }}" {{(isset($request->search_by_status) && ($request->search_by_status == $key) ||
                                            !isset($request->search_by_status) && $key == 1)?'selected':''}}>{{ $userStatusVal }}</option>
                                    @endforeach
                                @endif
                        </select>
                    </div>
                    </div>
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/detail-leave')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
            </div>
        </form>
         @endif
        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 d-flex justify-content-between">
                            <div class="left">
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="width: 100%; height:36px;"  >
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                             @if (count($leavesData)>0)
                                <div class="right show_total_no">
                                    Showing {{ $leavesData->firstItem() }} to {{ $leavesData->lastItem() }} of total {{$leavesData->total()}} entries
                                </div>
                             @endif
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                        <div class="table-responsive">
                            @if (count($leavesData)>0)
                            <table id="upcoming_leave_listing" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>@sortablelink('emp_name','Name')</th>
                                        <th>@sortablelink('available_leaves','Total Leaves')</th>
                                        <th>@sortablelink('used_leaves','Leaves Taken')</th>
                                        <th>Leaves Remaining</th>
                                        <th>Comp-Off</th>
                                        <th>Total Leave After Comp-off</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $j = $leavesData->firstItem();
                                    @endphp
                                    @foreach($leavesData as  $key => $leaveDetail)
                                    <tr class="edit_leaves" id="{{$leaveDetail->id}}">
                                        <td>{{$j}}</td>
                                        <td>{{$leaveDetail->emp_name.'  ('.$leaveDetail->emp_short_name.$leaveDetail->employee_id.')'}}</td>
                                        <td><span class="available_leaves">{{$leaveDetail->available_leaves}}</span>
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                                    <i class="fa fa-edit edit_available_leaves" id="edit_available_leaves" data-id='{{$leaveDetail->id}}'></i>
                                            @endif
                                        </td>
                                        <td><span class="used_leaves">{{getTakenLeaveCount($leaveDetail->id,$leaveDetail->confirmation_date)}}</span>
                                            <!-- @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                                <i class="fa fa-edit edit_used_leaves"  id="edit_used_leaves" data-id='{{$leaveDetail->id}}'></i>
                                            @endif -->
                                        </td>
                                        <td>
                                            @php
                                                $remainingLeave = 0;
                                                $remainingLeave = $leaveDetail->available_leaves - getTakenLeaveCount($leaveDetail->id,$leaveDetail->confirmation_date);
                                            @endphp
                                            <span class="remaining_leaves">{{$remainingLeave<0?0:$remainingLeave}}</span>
                                        </td>
                                        <td>
                                            @php
                                                $startDate = getUsersFinancialYearStartDate($leaveDetail->id);
                                                $compoffdata = \App\CompOff::where('user_id',$leaveDetail->id)->where('compoff_start_date', '>=', $startDate)->where('compoff_marked_as',2)->orderBy('id','desc')->get();
                                                $compOff = getUsersCompoffData($compoffdata);
                                            @endphp
                                            {{ $compOff }}
                                        </td>
                                        <td>
                                            @php
                                                $totalLeaveRemaining = $remainingLeave<0?0:$remainingLeave;
                                            @endphp
                                            {{ ($totalLeaveRemaining + $compOff) }}
                                        </td>

                                    </tr>
                                    @php
                                        $j++;
                                    @endphp
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="border-top">
                                <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                            </div>
                            @endif
                            @if($leavesData && !empty($leavesData))
                            <div class="pt-4" >{!! $leavesData->appends(\Request::except('page'))->render() !!}</div>
                             @endif
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
@endsection
