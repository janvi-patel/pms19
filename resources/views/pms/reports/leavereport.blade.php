@extends('layouts.master')
@section('moduleName')
    Report
@endsection
@section('content')
@php
$leaveStatusArray = config('constant.leave_status');
$pageRangArray = config('constant.page_range');
$departmentArray = config('constant.department');
$internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);

@endphp
<style type="text/css">
    .beforeConfirmation{
        background-color:#ff7a7a !important;
    }
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Leave Report</h1> -->
            </div>
            <div class="col-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Leave Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<?php 
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <form class="form-horizontal" method="get" action="{{url('/reports/leave')}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search Leave</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>            
                <div class="card-body">
                    <div class="row">
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                            @if(isset($internalCompany) && count($internalCompany) > 0)
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Department</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_department[]" id="search_by_department" multiple="multiple">
                                        <option value="">Select Department</option>  
                                            @if(isset($departmentArray) && !empty($departmentArray))
                                                @foreach($departmentArray as $key => $val)
                                                    @if(!isset($request->search_by_department))
                                                        <option selected="selected" value="{{ $key }}">{{ $val }}</option>
                                                    @else
                                                        <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                    </select>
                                </div>
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Internal Company</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                        <option value="">Select Internal Company</option> 
                                        @if(isset($internalCompany) && !empty($internalCompany))
                                            @foreach($internalCompany as $key => $val)
                                                <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                            @endforeach
                                        @endif
                                        <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                    </select>
                                </div>
                            @endif
                        @endif
                        <div class="form-group col-lg-3 col-md-4 col-sm-6">
                            <label class="">User</label>
                              <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee">
                                <option value="">All</option>
                                @if(!empty($userNameArray))      
                                    @foreach($userNameArray as $key => $val)
                                        <option value="{{$key}}" {{(isset($request->search_by_employee) && ($request->search_by_employee == $key))?'selected':''}}>{{$val}}</option>
                                    @endforeach
                                @endif
                            </select>  
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-6">
                            <label class="">Approver</label>
                              <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_approver" id="search_by_approver">
                                <option value="">All</option>
                                @if(!empty($userNameArray))      
                                    @foreach($userNameArray as $key => $val)
                                        <option value="{{$key}}" {{(isset($request->search_by_approver) && ($request->search_by_approver == $key))?'selected':''}}>{{$val}}</option>
                                    @endforeach
                                @endif
                            </select>  
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-6">                        
                            <label>Leave Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_leave_start_date" id="search_leave_start_date" value="{{ $start_date }}" class="form-control"/>
                            </div>
                        </div>
                    
                        <div class="form-group col-lg-3 col-md-4 col-sm-6">                        
                            <label class="">Leave Status</label>
                              <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_leave_status" id="search_by_leave_status">
                                <option value="">All</option>
                                @foreach ($leaveStatusArray as $key => $node)
                                    <option value="{{$key}}" {{(isset($request->search_by_leave_status) && ($request->search_by_leave_status == $key))?'selected':''}}> {{ $node }}</option>
                                @endforeach

                            </select>  
                        </div>
                        @if(Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole('admin'))
                        <div class="form-group col-lg-3 col-md-4 col-sm-6">
                            <div class="form-group col-lg col-md-4 col-sm-4">
                                <label class="">Month</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_month" id="search_month">
                                    <option value="">Select Month</option>   
                                    @foreach(config('constant.months') as $key => $value)
                                        <option value="{{$key}}" {{((isset($request->search_month) && $request->search_month == $key) || (isset($currentMonth) && $currentMonth == $key))?'selected':''}}>
                                            {{$value}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif
                        @if(Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole('admin'))
                        <div class="form-group col-lg-3 col-md-4 col-sm-6">
                            <div class="form-group col-lg col-md-4 col-sm-4">
                                <label class="">Year</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_year" id="search_year">
                                    <option value="">Select Year</option>   
                                     @foreach($years as $key => $value)
                                        <option value="{{$key}}" {{((isset($request->search_year) && $request->search_year == $key) || (isset($currentYear) && $currentYear == $key))?'selected':''}}>
                                            {{$value}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                        <div class="form-group col-lg-3 col-md-4 col-sm-6">                        
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_company" id="search_by_company" >
                                    <option value="0">All Companies</option>
                                        @foreach($companyList as $companyDataVal)
                                            <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_company) && ($request->search_by_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                        @endforeach
                                </select>
                        </div>
                        @endif
                        <div class="form-group col-lg-3 col-md-4 col-sm-6"> 
                            <div class="form-check" style="padding-top:35px;">
                                <input type="checkbox" id="adhoc_leave" name="adhoc_leave" class="form-check-input" style="width:auto !important;" {{(isset($request->adhoc_leave) && ($request->adhoc_leave == 'on'))?'checked':''}}> 
                                <label for="reason">Adhoc Leave</label>
                            </div>
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-6"></div>
                    </div>
                </div>
                {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/leave')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex justify-content-between">
                                <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="height:36px;">
                                    @if(is_array($pageRangArray))
                                       @foreach ($pageRangArray as $key => $node)
                                            <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                        @endforeach 
                                    @endif
                                </select>
                              
                                @if (count($leavesData)>0)                             
                                    <div class="right show_total_no text-right">
                                        Showing {{ $leavesData->firstItem() }} to {{ $leavesData->lastItem() }} of total {{$leavesData->total()}} entries
                                    </div> 
                                 @endif
                             </div>
                        </div>
                        <div class="row"> 
                        <div class="col-md-12">                        
                        <div class="table-responsive">
                            @if (count($leavesData)>0)
                            <table id="upcoming_leave_listing" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>@sortablelink('user.first_name','User')</th>
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                        <th>@sortablelink('company_name','Company')</th>
                                        @endif
                                        <th>@sortablelink('leave_start_date','Start Date')</th>
                                        <th>@sortablelink('leave_end_date','End Date')</th>
                                        <th>@sortablelink('return_date','Return Date')</th>
                                        <th>@sortablelink('leave_days','Leave Days')</th>
                                        <th>@sortablelink('reason','Reason')</th>
                                        <th>@sortablelink('user.first_name','Approver')</th>
                                        <th>@sortablelink('leave_status','Leave Status')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    $j = $leavesData->firstItem();
                                    @endphp 
                                    @foreach($leavesData as $leaveDetail)
                                    <?php if(strtotime($leaveDetail->confirmation_date) >= strtotime($leaveDetail->leave_end_date)){
                                        $className = "beforeConfirmation";
                                    }else{
                                        $className = "afterConfirmation";
                                    } ?>
                                    <tr class="{{$className}}">
                                        <td>{{$j}}</td>
                                        <td>{{$leaveDetail['user']['first_name'].' '.$leaveDetail['user']['last_name']}}</td>
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                            <td>{{$leaveDetail['company_name']}}</td>
                                        @endif
                                        <td>{{(new \App\Helpers\CommonHelper)->displayDate($leaveDetail['leave_start_date'])}}</td>
                                        <td >{{(new\App\Helpers\CommonHelper)->displayDate($leaveDetail['leave_end_date'])}}</td>
                                        <td>{!! displayDate($leaveDetail['return_date']) !!}</td>
                                        <td>{{$leaveDetail['leave_days']}}</td>
                                        <td class="leave_reasone">{{$leaveDetail['reason']}}</td>
                                        <td>
                                            {{$leaveDetail['approver']['first_name'].' '.$leaveDetail['approver']['last_name']}}
                                            @if($leaveDetail['is_adhoc'] == 1)
                                                <span class="mdi mdi-alert-octagon mdi-20px text-primary" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-title="Adhoc Leave"></span>
                                            @endif
                                        </td>
                                        <td>{{$leaveStatusArray[$leaveDetail['leave_status']]}}</td>
                                    </tr>
                                    @php
                                    $i++;$j++;
                                    @endphp                             
                                    @endforeach 
                                    @if(count($attance)>0)
                                        @foreach($attance as $a)
                                        <tr>
                                            <td>{{$j}}</td>
                                            <td>{{$a->emp_name}}</td>
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                                <td>{{$a->emp_comp}}</td>
                                            @endif
                                            <td>{{(new \App\Helpers\CommonHelper)->displayDate($a->entry_date)}}</td>
                                            <td>{{(new \App\Helpers\CommonHelper)->displayDate($a->entry_date)}}</td>
                                            <td>{!! displayDate($a->entry_date) !!}</td>
                                            <td>{{($a->half_day == 1)?'0.5':'1'}}</td>
                                            <td class="leave_reasone">Auto Generated Leave</td>
                                            <td>{{($a->approver_full_name)}}</td>
                                            <td>Approved</td>
                                        </tr>
                                        @php
                                            $j++;
                                        @endphp  
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            @else
                            <div class="border-top">
                                <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                            </div>
                            @endif
                            @if($leavesData && !empty($leavesData))
                            <div class="pt-4" >{!! $leavesData->appends(\Request::except('page'))->render() !!}</div>
                             @endif 
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>                        
        </div>
    </div>
</section>

@endsection
@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
@endsection