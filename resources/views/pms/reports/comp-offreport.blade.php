@extends('layouts.master')
@section('content')
@section('moduleName')
    Report
@endsection

@php
$compOffStatusArray = config('constant.compoff_status');
$compoffMarkedAsArray = config('constant.compoff_marked_as');
$pageRangArray = config('constant.page_range');
@endphp
<link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet">
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Comp-off</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<?php 
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <form class="form-horizontal" method="get" action="{{url('/reports/comp-offs')}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search Comp-off</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label class="">User</label>
                              <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee">
                                <option value="">All</option>
                                @foreach($userNameList as $userNameListVal)
                                    <option value="{{$userNameListVal->id}}" {{(isset($request->search_by_employee) && ($request->search_by_employee == $userNameListVal->id))?'selected':''}}>{{$userNameListVal->first_name.' '.$userNameListVal->last_name}}</option>
                                @endforeach
                            </select>  
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label class="">Approver</label>
                              <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_approver" id="search_by_approver">
                                <option value="">All</option>
                                @foreach($userNameList as $userNameListVal)
                                    <option value="{{$userNameListVal->id}}" {{(isset($request->search_by_approver) && ($request->search_by_approver == $userNameListVal->id))?'selected':''}}>{{$userNameListVal->first_name.' '.$userNameListVal->last_name}}</option>
                                @endforeach
                            </select>   
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Start Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_compoff_start_date" id="search_compoff_start_date" value="{{ $request->search_compoff_start_date }}" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>End Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input type="text" name="search_compoff_end_date" id="search_compoff_end_date" value="{{$request->search_compoff_end_date}}" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">                    
                            <label>Comp-off Status</label>
                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_compoff_status" id="search_by_compoff_status">
                                <option value="">All</option>
                                @foreach ($compOffStatusArray as $key => $node)
                                    <option value="{{$key}}" {{(isset($request->search_by_compoff_status) && ($request->search_by_compoff_status == $key))?'selected':''}}> {{ $node }}</option>
                                @endforeach

                            </select> 
                        </div>
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug'))|| Auth::user()->hasRole(config('constant.admin')))
                            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                                <label>Marked-as Status</label>
                                <select class="select2 form-control custom-select" name="search_by_marked_status" id="search_by_marked_status" style="width: 100%">
                                    <option value="">All</option>
                                    @foreach($compoffMarkedAsArray as $key => $val)
                                    <option value="{{$key}}" {{ ($key == request()->input('search_by_marked_status'))? 'selected' : ''  }}>{{$val}}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_company" id="search_by_company" >
                                    <option value="0">All Companies</option>
                                        @foreach($companyList as $companyDataVal)
                                            <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_company) && ($request->search_by_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                        @endforeach
                                </select>
                            @endif
                        </div>
                        
                        <div class="form-group col-lg col-md-6 col-sm-6"></div>
                        <div class="form-group col-lg col-md-6 col-sm-6"></div>                    
                    </div>
                </div>
                {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/comp-offs')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex justify-content-between">
                                <div class="left">
                                    <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="width: 100%; height:36px;"  >
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>   
                                </div>                             
                                @if (count($compOffData)>0) 
                                <div class="right show_total_no text-right">
                                    Showing {{ $compOffData->firstItem() }} to {{ $compOffData->lastItem() }} of total {{$compOffData->total()}} entries
                                </div>                             
                                @endif
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-12">                          
                            @if (count($compOffData)>0)  
                            <div class="table-responsive">
                                <table id="compoff_listing" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>@sortablelink('user.first_name','User')</th>
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                                <th>@sortablelink('company_name','Company')</th>
                                            @endif 
                                            <th>@sortablelink('compoff_start_date','Start Date')</th>
                      			            <th>@sortablelink('compoff_end_date','End Date')</th>
                                            <th>@sortablelink('compoff_days','Comp-off Days')</th>
                                            <th>@sortablelink('compoff_description','Comp-off Description')</th>
                                            <th>@sortablelink('user.first_name','Approver')</th>
                                            <th>@sortablelink('compoff_status','Comp-off Status')</th>
                                            <th>@sortablelink('compoff_marked_as','Encashed / Leave Added')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $i = 1;
                                        $j = $compOffData->firstItem();
                                        @endphp 
                                        @foreach($compOffData as $compoff)
                                        <tr>
                                            <td>{{$j}}</td>
                                            <td>{{$compoff['user']['first_name'].' '.$compoff['user']['last_name']}}</td>
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                                <td>{{$compoff['company_name']}}</td>
                                            @endif
                                            <td data-sort='{{ $compoff['compoff_start_date'] }}'>{{(new \App\Helpers\CommonHelper)->displayDate($compoff['compoff_start_date'])}}</td>
                                            <td data-sort='{{ $compoff['compoff_end_date'] }}'>{{(new \App\Helpers\CommonHelper)->displayDate($compoff['compoff_end_date'])}}</td>

                                            <td>{{$compoff['compoff_days']}}</td>
                                            <td class="comp-offs-desc">{{$compoff['compoff_description']}}</td>
                                            <td>{{$compoff['approver']['first_name'].' '.$compoff['approver']['last_name']}}</td>
                                            <td>
                                                {{$compOffStatusArray[$compoff['compoff_status']]}}
                                            </td>
                                            <td>
                                                @if($compoff['compoff_marked_as'] != '')  
                                                {{$compoffMarkedAsArray[$compoff['compoff_marked_as']]}}
                                                @else
                                                -
                                                @endif
                                            </td>
                                        </tr>
                                        @php
                                        $i++;$j++;
                                        @endphp 
                                        @endforeach 
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <div class="border-top">
                                <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                            </div>
                            @endif
                            @if($compOffData && !empty($compOffData))
                            <div class="pt-4" >{!! $compOffData->appends(\Request::except('page'))->render() !!}</div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
@endsection