<div class="card-body">
    <div class="table-responsive">
        <table id="holiday_listing" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th><b>Sr_No</b></th>
                    <th><b>Emp_Code</b></th>
                    <th><b>Name</b></th>
                    @if(count($dates)>0)
                        @foreach($dates as $v)
                            <th><b>{{$v}}</b></th>
                        @endforeach
                    @endif
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th><b>Day</b></th>
                    @if(count($days)>0)
                        @foreach($days as $d)
                            <th><b>{{$d}}</b></th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
            @php $i = 1; @endphp
            @if(count($excelData)>0)
                @foreach($excelData as $k => $e)
                <tr>
                    <td><b>{{$i}}</b></td>
                    <td><b>{{$e['employee_id']}}</b></td>
                    <td><b>{{$e['first_name']}} {{$e['last_name']}}</b></td>
                    @php $totalWorkedDay = 0; $leaveOfMonth = 0; @endphp
                    @for($j =0 ;$j<count($dates); $j++)
                        @if((new \App\Helpers\CommonHelper)->is_holiday($dates[$j]) != true)
                            
                                <?php 
                                    $getUserDetailForDate = getUserWorkingHoursDetails($dates[$j],$e['employee_id'],$companyName);
                                    if(!empty($getUserDetailForDate)){
                                            if($getUserDetailForDate->incorrect_entry == 1){ ?>
                                                <td style="background-color: #cc3300;">00:00:00</td>
                                            <?php }else { 
                                                if($getUserDetailForDate->early_going == 1){
                                                    $className = "#ff9933";
                                                }elseif($getUserDetailForDate->half_day == 1){
                                                    $className = "#6666ff";
                                                }elseif($getUserDetailForDate->full_day == 1){
                                                    $className = "#009933";
                                                }elseif($getUserDetailForDate->absent == 1){
                                                    $className = "#ff0000";
                                                    $getUserDetailForDate->working_hours = '00:00:00';
                                                }else{
                                                    $className = "";
                                                }
                                            ?>
                                            <td style="background-color: {{$className}};">{{$getUserDetailForDate->working_hours}}</td>
                                    <?php 
                                            }
                                    }else{ ?>
                                        <td >Off</td>
                                   <?php }
                                
                                ?>
                        @else
                        <td style="background-color: #ffc107;">Off</td>    
                        @endif
                    @endfor
                       
                </tr>
                @php $i++; @endphp
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
