@extends('layouts.master')
@section('content')
@section('moduleName')
    Report
@endsection
@php
$pageRangArray = config('constant.page_range');
@endphp
<?php 
 $search_class = "collapsed-card";
?>
@if(Request::get('search_submit'))
    <?php 
        $search_class = "";
    ?>
@endif
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Late Come Report</h1> -->
            </div>
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active">Late Comers Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>  
<section class="content-header">
    <div class="container-fluid"> 
        <form class="form-horizontal" method="get" action="{{url('/reports/late-comer')}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>" >
                <div class="card-header bg-dark" data-widget="collapse">
                    <h3 class="card-title">Search Late Comers</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                            <div class="form-group col-lg col-md-6 col-sm-6">
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_company" id="search_by_company" >
                                    <option value="">All Companies</option>
                                        @foreach($companyList as $companyDataVal)
                                            <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_company) && ($request->search_by_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                        @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Month</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_month" id="search_by_month" >
                                    @foreach($months as $key => $month)
                                    <option value="{{ $key }}" 
                                    @if($request->has('search_by_month') && $request->search_by_month != '')
                                    {{ (isset($request->search_by_month) && ( $key == $request->search_by_month))?'selected':''  }}
                                    @else 
                                    {{ $key == $now['month']?'selected':''}}
                                    @endif >{{ $month }}</option>
                                    @endforeach
                            </select>
                        </div> 
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Year</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_year" id="search_by_year" >
                                    @foreach($years as $key => $year)
                                        <option value="{{ $key }}" 
                                    @if($request->has('search_by_year') && $request->search_by_year != '')
                                    {{ (isset($request->search_by_year) && ( $key == $request->search_by_year))?'selected':''  }}
                                    @else 
                                    {{ $key == $now['year']?'selected':''}}
                                    @endif> {{ $year }} </option>
                                    @endforeach
                            </select>
                        </div>                                                                        
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>User</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_employee" id="search_by_employee" >
                                    <option value="">All Users</option>
                                    @foreach($userDeatailsDropDown as $key => $user)
                                        <option value="{{ $user['employee_id'] }}"
                                        {{(isset($request->search_by_employee) && ( $request->search_by_employee == $user['employee_id']))?'selected':'' }}
                                        > {{ $user['first_name'].' '.$user['last_name'] }} </option>
                                    @endforeach
                            </select>
                        </div>
                        @if(!Auth::user()->hasRole(config('constant.superadmin_slug')))<div class="form-group col-lg col-md-6 col-sm-6"></div>@endif
                    </div>
                </div>
                {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" type="submit" class="btn btn-primary btn-dark" value="1">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href='{{url('/reports/late-comer')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>                 
        <div class="card">
            <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 d-flex justify-content-between mb-3 "> 
                                <div class="left">
                                    <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown" style="width: 100%; height:36px;"  >
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div> 
                            
                            
                             @if (count($lateComeData)>0) 
                            
                                <div class="right show_total_no text-right">
                                    Showing {{ $lateComeData->firstItem() }} to {{ $lateComeData->lastItem() }} of total {{$lateComeData->total()}} entries
                                </div> 
                            
                             @endif
                             </div>
                        </div>                
                <div class="row">
<!--                     <div class="col-md-4 mb-4 pl-2">
                        <h5 class="card-title m-b-1">
                            Late Comers In 

                            @if($request->has('search_by_month') && $request->search_by_month != '')
                                {{ $months[$request->search_by_month]}}
                            @else 
                                {{ $months[$now['month']] }}
                            @endif                                     

                            -

                            @if($request->has('search_by_year') && $request->search_by_year != '')
                                {{ $request->search_by_year }}
                            @else 
                                {{ $now['year'] }}
                            @endif                    

                        </h5>
                </div> -->
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="thisMonth">
                        @if(count($lateComeData)>0)
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="late_come_report">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">@sortablelink('emp_name','User Name')</th>
                                        <th scope="col">@sortablelink('emp_comp','Company')</th>
                                        <th scope="col">Total full Days</th>
                                        <th scope="col">Total Half Days</th>
                                        <th scope="col">Late Days</th>
                                    </tr>
                                </thead>
                                <tbody>
                            @php
                                $i=1;
                                $j = $lateComeData->firstItem();
                            @endphp
                            @foreach($lateComeData as $lateComer)
                                <tr>
                                    <td>{{$j }}</td>
                                    <td>{{$lateComer['emp_name']}}</td>
                                    <td>{{$lateComer['emp_comp']}}</td>
                                    <td>{{$lateComer['full_Day']}}</td>
                                    <td>{{$lateComer['half_Day']}}</td>
                                    <td>{{$lateComer['late_comer']}}</td>
                                </tr>
                            @php
                                $i++;$j++;
                            @endphp                                
                            @endforeach 
                                </tbody>
                            </table> 
                        </div>
                        @else
                        <div class="border-top">
                            <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                        </div>                              
                        @endif
                        @if($lateComeData && !empty($lateComeData))
                        <div class="pt-4" >{!! $lateComeData->appends(\Request::except('page'))->render() !!}</div>
                        @endif                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
   <script src="{{asset('js/reports.js?'.time())}}"></script>
@endsection
