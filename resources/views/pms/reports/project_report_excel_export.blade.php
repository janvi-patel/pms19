<div class="card-body">
    <div class="table-responsive">
        <table id="holiday_listing" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    @if(isset($request->search_by_user_name)&& $request->search_by_user_name != '')
                        <th>User Name</th>
                    @endif
                    <th>Project Name</th>
                    @if(!isset($request->search_by_user_name))
                        <th>User Name</th>
                    @endif
                    <th>Task Name</th>
                    <th>Assigned hours</th>
                    <th>Logged Hours</th>
                    <th>Total Logged Hours</th>
                    @if(!Auth::user()->hasRole('developer'))
                        <th>Total Assigned Hours</th>
                    @endif
                    <th>Project Hours</th>
                    <th><b>Project Efficiency</b></th>
                </tr>
            </thead>
            <tbody>
            @if (count($projectReportArray)>0)  
                @php
                    $i = 1;
                    $totalProHours = $totalEstimatedHours = $estimatedHours = 0;
                    $overAllTaskHours = $overAllTotalHours = $overAllLoggedHours = $overAllProHours = $overAllAssignedHours = 0;
                @endphp 
                @foreach($projectReportArray as $key => $projectReportVal)
                    @php 
                        $rowCount = $projectRowCount[$key];
                        $rowspan = 'rowspan='.$rowCount;
                        $projectData = (new \App\Helpers\CommonHelper)->getProjectData($key);
                        $estimatedHours = (isset($projectData->pms_hours) && $projectData->pms_hours!='')?$projectData->pms_hours:'0.00'; 
                        $crHours = (isset($projectCrHours[$key]) && $projectCrHours[$key]!='')?$projectCrHours[$key]:'0';
                        $totalProHours = $crHours + $estimatedHours;
                        $totalHours = 0;$totalTaskHours = 0; $subArray = array();$j = $k = 0;$percentage = 0.00;
                    @endphp  
                
                    @foreach($projectEmpLoggedArray[$key] as $projectEmpLoggedArrayk => $v)
                        @php    $subArray[$v['id']]['user_id'][] = $v['id'];
                                $subArray[$v['id']]['task_name'][] = isset($v['task_name'])?$v['task_name']:'Old Entry';
                                $subArray[$v['id']]['taskHours'][] = isset($v['taskHours'])?$v['taskHours']:'0.00';
                                $subArray[$v['id']]['loggedHours'][] = $v['total_logged_hours']; 
                        @endphp
                    @endforeach

                    @if(count($subArray)>0)
                        @php $k = 0;@endphp
                        @foreach($subArray as $subArrayKey => $subArrayVal)
                            @php $userName = (new \App\Helpers\CommonHelper)->getAnyUserById($subArrayKey); @endphp
                            @for($z = 0;$z < count($subArrayVal['task_name']);$z++)
                            @php
                                $totalTaskHours += ($subArrayVal['taskHours'][$z] == '')?'0.00':$subArrayVal['taskHours'][$z];
                                $totalHours += ($subArrayVal['loggedHours'][$z] == '')?'0.00':$subArrayVal['loggedHours'][$z];

                            @endphp
                                @if($z == 0) 
                                 <tr class='{{$rowCount}}'>
                                    @if($k != 0)
                                        <td></td>
                                        <td></td>
                                    @else
                                        <td>{{$i}}</td>
                                        @if(isset($request->search_by_user_name) && $request->search_by_user_name != '')
                                            <td>{{(isset($userName)?$userName->first_name.' '.$userName->last_name:'')}}</td>
                                        @else
                                            <td>{{$projectData->project_name}}</td>
                                        @endif
                                        @php $i++;@endphp
                                    @endif
                                    @if(!isset($request->search_by_user_name))
                                        <td>{{(isset($userName)?$userName->first_name.' '.$userName->last_name:'')}}</td>
                                    @else
                                        <td>{{$projectData->project_name}}</td>
                                    @endif
                                    <td>{{($subArrayVal['task_name'][$z] != '')?$subArrayVal['task_name'][$z]:'Old Entry'}}</td>
                                    <td>{{($subArrayVal['taskHours'][$z] == '')?'0.00':$subArrayVal['taskHours'][$z]}}</td>
                                    <td>{{($subArrayVal['loggedHours'][$z] == '')?'0.00':$subArrayVal['loggedHours'][$z]}}</td>
                                    <td></td>
                                    @if(!Auth::user()->hasRole('developer'))
                                        <td></td>
                                    @endif
                                    <td></td>
                                    <td></td>
                                </tr>
                                @else 
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{($subArrayVal['task_name'][$z] != '')?$subArrayVal['task_name'][$z]:'Old Entry'}}</td>
                                    <td>{{($subArrayVal['taskHours'][$z] == '')?'0.00':$subArrayVal['taskHours'][$z]}}</td>
                                    <td>{{($subArrayVal['loggedHours'][$z] == '')?'0.00':$subArrayVal['loggedHours'][$z]}}</td>
                                    <td></td>
                                    @if(!Auth::user()->hasRole('developer'))
                                        <td></td>
                                    @endif
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endif
                            @endfor
                            @php $k++; @endphp
                        @endforeach 
                    @endif
                    @php
                        $percentage = 0.00;
                        $overAllTaskHours = $overAllTaskHours + $totalTaskHours;
                        $overAllTotalHours = $overAllTotalHours + $totalHours;
                        $getLoggedHr = (isset($projectLoggedArray[$key][0]) && $projectLoggedArray[$key][0] != 0)?$projectLoggedArray[$key][0] : 0;
                        $overAllLoggedHours = $overAllLoggedHours + $getLoggedHr;
                        $getAssignedHr = (isset($projectEstimatedArray[$key][0]) && $projectEstimatedArray[$key][0] != 0) ? $projectEstimatedArray[$key][0] : 0;
                        $overAllAssignedHours = $overAllAssignedHours + $getAssignedHr;
                        $overAllProHours = $overAllProHours + $totalProHours;
                        if($getLoggedHr != 0 && $totalProHours != 0){
                            $percentage = ($getLoggedHr/$totalProHours)*100;
                        }
                    @endphp
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>Total</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalTaskHours)}}</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($totalHours)}}</b></td>
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getLoggedHr)}}</b></td>
                        @if(!Auth::user()->hasRole('developer'))
                            <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($getAssignedHr)}}</b></td>
                        @endif
                        <td><b>{{sprintf("%.2f",$totalProHours)}}</b></td>
                        <td><b>@if(sprintf("%.2f",$percentage) <= 100)
                                <span class="project_completion_gree" style="color: green;">
                                    {{sprintf("%.2f",$percentage).' %'}}
                                </span>
                            @else
                                <span class="project_completion_red" style="color: red;">
                                    {{sprintf("%.2f",$percentage).' %'}}
                                </span>
                            @endif</b>
                        </td>
                    </tr>
                @endforeach
                <tr></tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>Overall</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTaskHours)}}</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllTotalHours)}}</b></td>
                    <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllLoggedHours)}}</b></td>
                    @if(!Auth::user()->hasRole('developer'))
                        <td><b>{{(new \App\Helpers\CommonHelper)->displayTaskTime($overAllAssignedHours)}}</b></td>
                    @endif
                    <td><b>{{sprintf("%.2f",$overAllProHours)}}</b></td>
                    <td></td>
                </tr>
            @else
            <tr>
                <td colspan='9'>No Employee Found.</td>
            </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
