@extends('layouts.master')

@section('moduleName')
    Salary Mail logss
@endsection

@php
    $pageRangArray = config('constant.page_range');
@endphp

@section('content')
@include('sweet::alert')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">        
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Salary Mail logs</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Logs</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/old/salary/logs')}}" name="search_submit" id="search_submit">
                        <div class="card-body">
                            <div class="row">
                                    <div class="form-group col-md-4 col-sm-6">
                                        <label class="">Status</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_status" id="search_by_status">
                                            <option value="">Select Status</option>   
                                                <option value="1" {{(isset($request->search_by_status) && ($request->search_by_status == '1'))?'selected':''}}>Suceess</option>
                                                <option value="2" {{(isset($request->search_by_status) && ($request->search_by_status == '2'))?'selected':'' }}>Skipped</option>
                                                <option value="3" {{(isset($request->search_by_status) && ($request->search_by_status == '3'))?'selected':'' }}>Failed</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4 col-sm-6">
                                        <label class="">Entry Date</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="search_by_entry_date" id="search_by_entry_date" value="{{(isset($request->search_by_entry_date)?$request->search_by_entry_date:'')}}"  class="form-control" readonly />
                                        </div>
                                        <a href="javascript:void(0)" id="reset_entry_date">Reset</a>
                                    </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/old/salary/logs')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-12 mb-3 d-flex align-items-center justify-content-between">
                                <div class="col-6">
                                    <select class="select2 form-control custom-select page_rang_dropdown" name="page_rang_dropdown" id="page_rang_dropdown">
                                        @if(is_array($pageRangArray))
                                        @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                                <div class="show_total_no float-right">
                                    Showing {{ $logs->firstItem() }} to {{ $logs->lastItem() }} of total {{$logs->total()}} entries
                                </div> 
                                <div class="right col-2">
                                    <a id="export_to_excel">
                                        <button type="button" class="btn btn-dark">Export Failed Record</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="leave_listing" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Employee Code</th>
                                        <th>Employee</th>
                                        <th>Company Name</th>
                                        <th>Entry Date</th>
                                        <th>Used Email</th>
                                        <th>Uploaded By</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($logs)>0)
                                        @php 
                                            $j = $logs->firstItem();
                                        @endphp
                                        @foreach($logs as $log)
                                        <tr>
                                            <td>{{$j}}</td>
                                            <td>{{$log['emp_id'] != 0 ?$log['emp_id'] :'N/A'}}</td>
                                            <td>{{$log['user_id'] != null ?$log['user_id'] :'N/A'}}</td>
                                            <td>{{$log['emp_comp']!= null ?$log['emp_comp'] :'N/A'}}</td>
                                            <td>{{date('d-M-Y',strtotime($log['entry_date']))}}</td>
                                            <td>{{$log['email']!= null ?$log['email']:'N/A'}}</td>
                                            <td>{{$log['uploaded_by']}}</td>
                                            <td>
                                                @if($log['status'] == "1")
                                                    <span class="badge badge-success w-75">Success</span>
                                                @elseif($log['status'] == "2")
                                                    <span class="badge badge-warning w-75">Skipped</span>
                                                @else
                                                    <span class="badge badge-danger w-75">Failed</span>
                                                @endif
                                                @if($log['reason'] != '')
                                                    <span data-trigger="hover" data-toggle="tooltip" data-placement="right" title="Status Reason" data-content="{{$log['reason']}}">
                                                        <i class="mdi mdi-information-outline text-primary mdi-18px"></i>
                                                    </span>
                                                @endif
                                            </td>
                                        </tr>
                                        @php
                                            $j++;
                                        @endphp  
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan='10'>
                                                <center>
                                                    {{'No Record Found'}}
                                                </center>
                                            </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row w-100">
                            @if($logs && !empty($logs))
                                <div class="pt-4 col-md-6">
                                    {!! $logs->appends(\Request::except('page'))->render() !!}
                                </div>
                            @endif
                                @if(count($logs) >0)
                                <div class="show_total_no_bottom col-md-6"> 
                                    <div class="card-body float-right">
                                        Showing {{ $logs->firstItem() }} to {{ $logs->lastItem() }} of total {{$logs->total()}} entries
                                    </div>
                                </div> 
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script src="{{asset('js/email_log.js?'.time())}}"></script>
@endsection
