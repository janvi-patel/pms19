<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Employee Code</th>
                    <th>Employee</th>
                    <th>Company Name</th>
                    <th>Entry Date</th>
                    <th>Used Email</th>
                    <th>Failed Reason</th>
                    <th>Uploaded By</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($logs) && count($logs)>0)
                    @foreach($logs as $log)
                        <tr>
                            <td>{{$log['emp_id'] != 0 ?$log['emp_id'] :'N/A'}}</td>
                            <td>{{$log['user_id'] != null ?$log['user_id'] :'N/A'}}</td>
                            <td>{{$log['emp_comp']!= null ?$log['emp_comp'] :'N/A'}}</td>
                            <td>{{date('d-m-Y',strtotime($log['entry_date']))}}</td>
                            <td>{{$log['email']!= null ?$log['email']:'N/A'}}</td>
                            <td>{{$log['reason']}}</td>
                            <td>{{$log['uploaded_by']}}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
