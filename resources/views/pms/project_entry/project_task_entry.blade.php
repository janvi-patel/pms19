@extends('layouts.master')

@section('moduleName')
   Task Entries
@endsection

@php
    $pageRangArray = config('constant.page_range');
@endphp

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a href="javascript:history.back()" class="btn btn-primary btn-dark" id="BackPage" value="BackPage"><i class="fa fa-angle-left"> Back</i></a>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Project Task Entries</li>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Project Entry</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/view/project_task_entry/'.$taskId)}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_log_from_date" id="search_by_log_from_date" value="{{(isset($request->search_by_log_from_date)?$request->search_by_log_from_date:'')}}" class="form-control" autocomplete="off" readonly />
                                    </div>
                                </div>
                                <div class="form-group col-md col-md-4 col-sm-4">
                                    <label class="">End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_log_to_date" id="search_by_log_to_date" value="{{(isset($request->search_by_log_to_date)?$request->search_by_log_to_date:'')}}" class="form-control" autocomplete="off" readonly />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/view/project_task_entry/'.$taskId)}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-1 mb-3"> 
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div>
                            </div>
                            @if(count($projectEntry)>0)
                                @if($projectEntry[0]->user_id == Auth::user()->id)
                                <div class="col-md-3 offset-md-8 mb-4"> 
                                    <div class="form-group col">
                                        <select class="select2 form-control custom-select" name="taskEntryDropdown" id="taskEntryDropdown">
                                            <option value="">Select Action</option>
                                            <option value="1">Delete</option>
                                        </select>
                                    </div>
                                </div>
                                @endif
                            @endif
                        </div>
                        @if(count($projectEntry)>0)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive-md">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                            <tr>
                                                <th style="width: 20%">Project Name</th>
                                                <td>{{$projectHours->project_name}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th style="width: 20%">Task Name</th>
                                                <td>{{$projectHours->task_name}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Task Assigned Hours</th>
                                                <td>
                                                {{isset($projectEntry)?(new \App\Helpers\CommonHelper)->displayTaskTime($projectEntry[0]->taskHours):'00:00'}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Total Loggged Hours</th>
                                                <td>
                                                    @php $totalLogged = 0; @endphp
                                                    @if(count($projectEntry)>0)
                                                        @foreach($projectEntry as $val)
                                                            @php $totalLogged += $val['log_hours']; @endphp
                                                        @endforeach
                                                    @endif
                                                    {{isset($projectHours)?(new \App\Helpers\CommonHelper)->displayTaskTime($totalLogged):'00:00'}}
                                                </td>
                                            </tr> 
                                            <tr>
                                                <th>Total Description</th>
                                                <td>
                                                    {{isset($projectHours)?$projectHours->task_desc:''}}
                                                </td>
                                            </tr> 

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive-md">
                                    <table id="taskEntryTable" class="table table-striped table-bordered">
                                        <thead>
                                            @if(count($projectEntry)>0)
                                                <tr>
                                                    @if($projectEntry[0]->user_id == Auth::user()->id)
                                                    <th>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" id="checkAll" class="custom-control-input">
                                                            <label class="custom-control-label" for="checkAll"></label>
                                                        </div>
                                                    </th>
                                                    @endif
                                                <th>@sortablelink('first_name','User Name')</th>
                                                <th>@sortablelink('log_date','Date')</th>
                                                <th>@sortablelink('log_hours','Hours')</th>
                                                <th>Description</th>
                                                @if($projectEntry[0]->user_id == Auth::user()->id)
                                                <th>Action</th>
                                                @endif
                                                </tr>
                                            @endif
                                        </thead>
                                        <tbody>
                                            <?php $total_logged = 0; ?>
                                            @if(count($projectEntry) > 0)
                                                @foreach($projectEntry as $index => $value)
                                                <?php 
                                                    $total_logged = $total_logged + $value->log_hours;
                                                ?>
                                                    <tr id="{{$value->id}}">
                                                        @if($value->user_id == Auth::user()->id)
                                                        <td>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="chboxTaskEntry[]" id="chboxTaskEntry_{{$value->id}}" value="{{$value->id}}" class="custom-control-input chboxTaskEntry">
                                                                    <label class="custom-control-label" for="chboxTaskEntry_{{$value->id}}"></label>
                                                            </div>
                                                        </td>
                                                        @endif
                                                        <td>
                                                            {{$value->first_name.' '.$value->last_name}}
                                                        </td>
                                                        <td>
                                                            {{date('d-m-Y',strtotime($value->log_date))}}
                                                        </td>
                                                        <td>
                                                            {{(new \App\Helpers\CommonHelper)->displayTaskTime($value->log_hours)}}
                                                        </td>
                                                        <td>
                                                            {{$value->desc}}
                                                        </td>
                                                        @if($value->user_id == Auth::user()->id)
                                                        <td>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update" data-task-entry-id="{{$value->id}}" class="updateTaskEntry"
                                                                data-class="project_task_entry">
                                                                    <i class="mdi mdi-pencil"></i>
                                                            </a>
                                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteTaskEntry" data-task-entry-id="{{$value->id}}">
                                                                <i class="mdi mdi-close"></i>
                                                            </a> 
                                                        </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    @if($projectEntry[0]->user_id == Auth::user()->id)
                                                    <td></td>
                                                    @endif
                                                    <td></td>
                                                    <td><b>Total</b></td>
                                                    <td> {{(new \App\Helpers\CommonHelper)->displayTaskTime($total_logged)}}</td>
                                                    <td></td>
                                                    @if($projectEntry[0]->user_id == Auth::user()->id)
                                                    <td></td>
                                                    @endif
                                                </tr>
                                            @else
                                                <tr>
                                                    <td colspan="5"><center>No Project Entry Found</center></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row w-100">
                                @if($projectEntry && !empty($projectEntry))
                                    <div class="pt-3 col-md-6">
                                        {!! $projectEntry->appends(\Request::except('page'))->render() !!}
                                    </div>
                                @endif
                                @if(count($projectEntry) >0)
                                    <div class="show_total_no_bottom col-md-6"> 
                                        <div class="card-body float-right">
                                            Showing {{ $projectEntry->firstItem() }} to {{ $projectEntry->lastItem() }} of total {{$projectEntry->total()}} entries
                                        </div>
                                    </div> 
                                 @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="editTaskEntryDiv">
    @include('pms.project_entry.modal.edit_task_time_entry')
</div>
@endsection

@section('javascript')
<script src="{{asset('js/task_entry.js?'.time())}}"></script>
@endsection