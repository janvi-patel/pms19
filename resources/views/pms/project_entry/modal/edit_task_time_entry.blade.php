<div id="editTaskTimeEntryModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Update Project entry</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>
            <form method="post" action="{{url('update/project_entry')}}" id="updateTaskEntry">
                @csrf
                <input type="hidden" name="editProjectId" id="editProjectId" value="{{(isset($projectEntry['project_id']))?$projectEntry['project_id']:''}}">

                <input type="hidden" name="editProjectEntryId" id="editProjectEntryId" value="{{(isset($projectEntry['id']))?$projectEntry['id']:''}}">
                <input type="hidden" name="editTaskStartDate" id="editTaskStartDate" value="{{(isset($projectEntry['project_start_date']))?$projectEntry['project_start_date']:''}}">
                <input type="hidden" name="editProjectTaskStartDate" id="editProjectTaskStartDate" value="{{(isset($projectEntry['task_start_date']))?$projectEntry['task_start_date']:''}}">
                <input type="hidden" name="editProjectTaskEndDate" id="editProjectTaskEndDate" value="{{(isset($projectEntry['task_end_date']))?$projectEntry['task_end_date']:''}}">
                <input type="hidden" name="editTaskId" id="editTaskId" value="{{(isset($projectEntry['task_id']))?$projectEntry['task_id']:''}}">
                <input type="hidden" name="pageLink" id="pageLink" value="">

                <div class="modal-body assign_to_team_records" id="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task Date<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="editTaskDate" class="form-control" id="editTaskDate" autocomplete="off" readonly="readonly" style="width: 220px;" value="{{(isset($projectEntry["log_date"]))?date('d-m-Y',strtotime($projectEntry["log_date"])):''}}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php
                            $hoursMinutes = array();
                            $storedHours = $storedMinutes = ''; 
                            if(isset($projectEntry) && isset($projectEntry['log_hours']) && $projectEntry['log_hours'] != ''){                                
                                $hoursMinutes   = explode(".",$projectEntry['log_hours']);
                                $storedHours    = isset($hoursMinutes[0])?$hoursMinutes[0]:0;
                                $storedMinutes  = isset($hoursMinutes[1])?$hoursMinutes[1]:0;
                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Work Time<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="editWorkTimeHours" id="editWorkTimeHours" style="width: 90px;">
                                    <option value="">Hours</option>
                                    <option value="0" <?php if(isset($projectEntry) && $storedHours == 0){ echo "selected"; } ?>>0</option>
                                    
                                    <option value="1" <?php if(isset($projectEntry) && $storedHours == 1){ echo "selected"; } ?>>1</option>
                                    
                                    <option value="2" <?php if(isset($projectEntry) && $storedHours == 2){ echo "selected"; } ?>>2</option>
                                    
                                    <option value="3" <?php if(isset($projectEntry) && $storedHours == 3){ echo "selected"; } ?>>3</option>
                                    
                                    <option value="4" <?php if(isset($projectEntry) && $storedHours == 4){ echo "selected"; } ?>>4</option>
                                    
                                    <option value="5" <?php if(isset($projectEntry) && $storedHours == 5){ echo "selected"; } ?>>5</option>
                                    
                                    <option value="6" <?php if(isset($projectEntry) && $storedHours == 6){ echo "selected"; } ?>>6</option>
                                    
                                    <option value="7" <?php if(isset($projectEntry) && $storedHours == 7){ echo "selected"; } ?>>7</option>
                                    
                                    <option value="8" <?php if(isset($projectEntry) && $storedHours == 8){ echo "selected"; } ?>>8</option>
                                    
                                    <option value="9" <?php if(isset($projectEntry) && $storedHours == 9){ echo "selected"; } ?>>9</option>
                                    
                                    <option value="10" <?php if(isset($projectEntry) && $storedHours == 10){ echo "selected"; } ?>>10</option>
                                    
                                    <option value="11" <?php if(isset($projectEntry) && $storedHours == 11){ echo "selected"; } ?>>11</option>
                                    
                                    <option value="12" <?php if(isset($projectEntry) && $storedHours == 12){ echo "selected"; } ?>>12</option>
                                </select>
                                <label id="editWorkTimeHours-error" class="error" for="editWorkTimeHours"></label>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="editWorkTimeMinutes" id="editWorkTimeMinutes" style="width: 90px">
                                    <option value="">Minutes</option>
                                    <option value="0" <?php if(isset($projectEntry) && $storedMinutes == 0){ echo "selected"; } ?>>0</option>
                                    <option value="15" <?php if(isset($projectEntry) && $storedMinutes == 25){ echo "selected"; } ?>>15</option>
                                    <option value="30" <?php if(isset($projectEntry) && ($storedMinutes == 5  || $storedMinutes == 50)){ echo "selected"; } ?>>30</option>
                                    <option value="45" <?php if(isset($projectEntry) && $storedMinutes == 75){ echo "selected"; } ?>>45</option>
                                </select>
                                <label id="editWorkTimeMinutes-error" class="error" for="editWorkTimeMinutes"></label>
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-group task_listing_block">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-6">
                                <select name="task_list" class="form-control select2" id="task_list">
                                    <option value="0">Select Task</option>
                                </select>
                                 <label id="task_list-error" class="error" for="task_list"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Description<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <textarea rows="10" cols="10" name="editTaskDescription" id="editTaskDescription" autocomplete="off" class="form-control">{{(isset($projectEntry['desc'])?$projectEntry['desc']:'')}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" value="Update"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
