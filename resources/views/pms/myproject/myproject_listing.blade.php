<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="holiday_listing" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        @php
                            $colspan = 6;
                            $NoRowColspan = 9;
                        @endphp
                        <th><center>No</center></th>
                        <th>@sortablelink('project_name','Project Name')</th>
                        <th>@sortablelink('project_type_name','Project Type')</th>
                    @if(Auth::user()->hasRole(config('constant.developer_slug')))
                    @php $NoRowColspan = 12;@endphp
                        <th>Team Leader</th>
                    @endif
                        <th>@sortablelink('project_start_date','Start Date')</th>
                    @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                        @php $colspan = 6; $NoRowColspan = 12;@endphp
                        <th>@sortablelink('client_name','Client Name')</th>
                        <th>@sortablelink('account_manager_name','Account Manager')</th>
                        @if(!Auth::user()->hasRole(config('constant.team_leader_slug')) && Auth::user()->designation_id != 16)
                            <th>@sortablelink('project_manager_name','Project Manager')</th>
                        @endif
                    @endif
                    @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                        <th>@sortablelink('project_status','Status')</th>
                    @endif
                        <th>Total Project Hours</th>
                        <th>Total Assigned Hours</th>
                        @if(Auth::user()->hasRole(config('constant.developer_slug')))
                        <th>Your Task Hours</th>
                        @endif
                        <th>Your Project Hours</th>
                        <th>@sortablelink('total_logged_hours','Total Logged Hours')</th>
                        <th style="padding-right:0px !Important">Project Completed</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($data)>0)
                        @php
                            $i = 1;
                            $j = $data->firstItem();
                            $totalEstimatedHours = $totalUserLoggedHours = $totalLoggedHours = $totalCrHours = $totalHours = $sumOfTotalHours= $sumOfTaskHours = 0.00;
                        @endphp
                        @foreach($data as $dataVal)
                            <?php
                                $estimatedHours = (isset($dataVal->pms_hours) && $dataVal->pms_hours!='')?$dataVal->pms_hours:'0.00';
                                $totalLoggedHr = ($dataVal->total_logged_hours)?$dataVal->total_logged_hours:'0.00';
                                $userLoggedHr = (new \App\Helpers\CommonHelper)->getLoggedHours($dataVal->id,Auth::user()->id);
                                $crHours = (isset($projectCrHours[$dataVal->id]) && $projectCrHours[$dataVal->id]!='')?$projectCrHours[$dataVal->id]:'0';
                                $totalWithCrandEst = isset($dataVal->total_logged_hours)?$dataVal->total_logged_hours:'0.00';

                                $totalEstimatedHours = $totalEstimatedHours + $estimatedHours;
                                $totalLoggedHours    = $totalLoggedHours + $totalLoggedHr;
                                $totalUserLoggedHours    = $totalUserLoggedHours + (($userLoggedHr)?$userLoggedHr->user_logged_hours:'0.00');
                                $totalCrHours = $totalCrHours + $crHours;
                                $totalHours = $estimatedHours + $crHours;
                                $sumOfTotalHours = $totalHours + $sumOfTotalHours;

                                $percentage = 0.00;
                                if($totalLoggedHr != 0 && $totalHours != 0){
                                    $percentage = ($totalLoggedHr/$totalHours)*100;
                                }
                            ?>

                            <tr>
                                <td><center>{{$j}}</center></td>

                                <td>
                                    @if(Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))
                                        <a href="{{url('view/task/list/'.$dataVal->id)}}" data-toggle="tooltip" data-placement="top"
                                           title="" data-original-title="View" class="project-task-info a_clickable_link" project_id="{{$dataVal->id}}">
                                            {{(isset($dataVal->project_name) && $dataVal->project_name!='')?$dataVal->project_name:'N/A'}}</a>&nbsp;
                                    @else
                                        <a href="{{url('view/task/list/'.$dataVal->id)}}" title="View Project Entries"
                                           class="a_clickable_link">
                                            {{(isset($dataVal->project_name) && $dataVal->project_name!='')?$dataVal->project_name:'N/A'}}
                                        </a>
                                    @endif
                                </td>

                                <td>
                                    {{(isset($dataVal->project_type_name) && $dataVal->project_type_name!='')?$dataVal->project_type_name:'N/A'}}
                                </td>
                                @if(Auth::user()->hasRole(config('constant.developer_slug')))
                                <td>
                                    {{$dataVal->teamleader}}
                                </td>
                                @endif
                                <td>
                                    {{(isset($dataVal->project_start_date) && $dataVal->project_start_date!='')?date('d-m-Y',strtotime($dataVal->project_start_date)):'N/A'}}
                                </td>
                                @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                <td>
                                    {{(isset($dataVal->client_name) && $dataVal->client_name!='')?$dataVal->client_name:'N/A'}}
                                </td>
                                <td>
                                    {{(isset($dataVal->account_manager_name) && $dataVal->account_manager_name!='')?$dataVal->account_manager_name:'N/A'}}
                                </td>
                                @if(!Auth::user()->hasRole(config('constant.team_leader_slug')) && Auth::user()->designation_id != 16)
                                    <td>
                                        {{(isset($dataVal->project_manager_name) && $dataVal->project_manager_name!='')?$dataVal->project_manager_name:'N/A'}}
                                    </td>
                                    @endif
                                @endif
                                @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                <td>
                                    @if(isset($dataVal->project_status) && $dataVal->project_status == 1)
                                        {{'Open'}}
                                    @elseif(isset($dataVal->project_status)  && $dataVal->project_status == 2)
                                        {{'On Hold'}}
                                    @elseif(isset($dataVal->project_status)  && $dataVal->project_status == 3)
                                        {{'Closed'}}
                                    @elseif(isset($dataVal->project_status)  && $dataVal->project_status == 4)
                                        {{'Completed'}}
                                    @else
                                        {{'N/A'}}
                                    @endif
                                </td>
                                @endif
                                <td>
                                    {{sprintf("%.2f",$totalHours)}}
                                        <a href="javascript;" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" class="project-info" project_id="{{$dataVal->id}}"><i class="mdi mdi-information-outline"></i></a>
                                </td>
                                    <?php
                                        $totalTaskHours = $myTaskTotalHours= 0 ;
                                         if(Auth::user()->hasRole(config('constant.developer_slug'))){
                                            $myTaskHour = (new \App\Helpers\CommonHelper)->getTaskHours($dataVal->id,Auth::user()->id);
                                         }else{
                                            $myTaskHour = (new \App\Helpers\CommonHelper)->getTaskHours($dataVal->id,'');
                                         }
                                        $myTaskTotalHours = $myTaskTotalHours + $myTaskHour->total_task_hours;
                                        $taskHour = (new \App\Helpers\CommonHelper)->getTaskHours($dataVal->id,'');
                                        $totalTaskHours = $totalTaskHours + $taskHour->total_task_hours;
                                        $sumOfTaskHours = $sumOfTaskHours + $totalTaskHours;
                                    ?>
                                @if(Auth::user()->hasRole(config('constant.developer_slug')))
                                <td class="{{((sprintf("%.2f",$totalHours))<(sprintf("%.2f",$totalTaskHours)))?'project_completion_red':''}}">
                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime($totalTaskHours)}}
                                </td>
                                @endif
                                <td class="">
                                    {{(new \App\Helpers\CommonHelper)->displayTaskTime($myTaskTotalHours)}}
                                </td>
                                <td>{{($userLoggedHr)?(new \App\Helpers\CommonHelper)->displayTaskTime($userLoggedHr->user_logged_hours):'0:00'}}</td>
                                <td>
                                    @if($totalWithCrandEst)
                                        {{(new \App\Helpers\CommonHelper)->displayTaskTime($totalWithCrandEst)}}
                                    @else
                                        {{$totalWithCrandEst}}
                                    @endif
                                </td>
                                <td>
                                    @if(sprintf("%.2f",$percentage) <= 100)
                                        <span class="project_completion_gree">
                                            {{sprintf("%.2f",$percentage).' %'}}
                                        </span>
                                    @else
                                        <span class="project_completion_red">
                                            {{sprintf("%.2f",$percentage).' %'}}
                                        </span>
                                    @endif
                                </td>
                                <td align="center">
                                    @if(Auth::check() && Auth::user()->hasPermission('projects.task.listing') || ($dataVal->project_name == 'Bench' || $dataVal->project_name == 'Miscellaneous Tasks'))
                                        <a href="javascript:void(0);" title="Add Task" class="addProjectTaskBtn" data-project-id="{{$dataVal->id}}"><i class="fa fa-list-alt" aria-hidden="true"></i></a>
                                    @endif
                                    @if($dataVal->project_name != 'Bench' && $dataVal->project_name != 'Miscellaneous Tasks')
                                        @if((Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->designation_id == 16))
                                            <a href="javascript:void(0);" class="createTeam" id="{{$dataVal->id}}" data-assiged-to-companyid="{{$dataVal->assigned_to}}" title="Create Team"><i class="fa fa-users" aria-hidden="true"></i></a>
                                        @endif
                                        <a href="{{url('view/project/notes/'.$dataVal->id)}}" data-toggle="tooltip" data-placement="top" class="viewNotes" data-task-id="{{$dataVal->id}}" title="View Notes"><i class="mdi mdi-book-open-page-variant"></i></a>
                                    @endif
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Task Entry" class="timeEntry" data-task-id="{{$dataVal->id}}"><i class="mdi mdi-alarm"></i></a>
                                </td>
                            </tr>
                            @php
                                $i++;$j++;
                            @endphp
                        @endforeach

                        @if(count($data)>0)
                            <tr>
                                <td></td>
                                @if(Auth::user()->hasRole(config('constant.developer_slug')))
                                <td></td>
                                @endif
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                    @if(!Auth::user()->hasRole(config('constant.team_leader_slug')) && Auth::user()->designation_id != 16)
                                        <td></td>
                                        <td></td>
                                    @endif
                                @endif
                                <td><b>Total</b></td>
                                @if(Auth::user()->hasRole(config('constant.team_leader_slug')) ||
                                (Auth::user()->hasRole(config('constant.project_manager_slug'))) ||
                                Auth::user()->designation_id == 16)
                                <td><b>{{sprintf("%.2f",$sumOfTotalHours)}}</b></td>
                                @endif
                                @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                <td><b>{{sprintf("%.2f",$sumOfTaskHours)}}</b></td>
                                @endif
                                <td><b>{{sprintf("%.2f",$totalUserLoggedHours)}}</b></td>
                                <td><b>{{sprintf("%.2f",$totalLoggedHours)}}</b></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    @else
                        <tr>
                            <td colspan='<?php echo $NoRowColspan;?>'><center>No Project Found</center></td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if($data && !empty($data))
            <div class="pt-4">{!! $data->appends(\Request::except('page'))->render() !!}</div>
        @endif
    </div>
</div>
<div id="project_cost" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Project Info</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"><a href="" class='view_cr_info'>View</a></button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="createTeamModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post" action="{{url('project/save_team')}}" id="saveTeamForm">
            @csrf
                <div class="modal-header card-header bg-dark">
                    <h4 class="modal-title">Assign To Team</h4>
                    <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
                </div>
                <div class="modal-body assign_to_team_records" id="modal-body">
                    @include('pms.project.assign_to_team')
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark" id="saveTeam" value="Create Team">
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="taskTimeEntryModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header card-header bg-dark">
                <h4 class="modal-title">Add project Entry</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
            </div>

            <form method="post" action="{{url('save/project/entry')}}" id="saveTaskEntry">
                @csrf
                <input type="hidden" name="projectId" id="projectId" value="">

                @if(!is_null($projectName))
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="true" />
                @else
                    <input type="hidden" name="redirectSameScreen" id="redirectSameScreen" value="false" />
                @endif

                <input type="hidden" name="projectTaskId" id="projectTaskId" value="" />

                <div class="modal-body" id="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Log Date<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="taskDate" class="form-control" id="taskDate" autocomplete="off" readonly="readonly" style="width: 220px;" value="{{$log_date}}">
                                @if ($errors->has('taskDate'))
                                    <div class="error">{{'Please select date'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Work Time<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="workTimeHours" id="workTimeHours" style="width: 90px;">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8" selected>8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                                <label id="workTimeHours-error" class="error" for="workTimeHours"></label>
                                @if ($errors->has('workTimeHours'))
                                    <div class="error">{{'Please select hours'}}</div>
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control select2" name="workTimeMinutes" id="workTimeMinutes" style="width: 90px">
                                    <option value="0">0</option>
                                    <option value="15">15</option>
                                    <option value="30" selected>30</option>
                                    <option value="45">45</option>
                                </select>
                                <label id="workTimeMinutes-error" class="error" for="workTimeMinutes"></label>
                                @if($errors->has('workTimeMinutes'))
                                    <div class="error">{{'Please select minutes'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Task<span class="error">*</span></label>
                            </div>
                            <div class="col-sm-6">
                                <select name="task_list" class="form-control select2" id="task_list">
                                    <option value="0">Select Task</option>
                                </select>
                                 <label id="task_list-error" class="error" for="task_list"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="col-form-label">Description</label>
                            </div>
                            <div class="col-sm-9">
                                <textarea rows="10" cols="10" name="taskDescription" id="taskDescription" autocomplete="off" class="form-control"></textarea>
                                @if($errors->has('taskDescription'))
                                    <div class="error">{{'Please enter task description'}}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary btn-dark projectTaskEntry" value="Save"/>
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="addProjectTaskModal" class="modal fade" role="dialog">

</div>
@section('javascript')
<script src="{{asset('js/moment-with-locales.js?'.time())}}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.min.js?'.time())}}"></script>
<script src="{{asset('js/select2.min.js?'.time())}}"></script>
<script src="{{asset('js/myprojects.js?'.time())}}"></script>
<script src="{{asset('js/misc_task.js?'.time())}}"></script>
@endsection
