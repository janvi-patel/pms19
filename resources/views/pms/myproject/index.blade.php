@extends('layouts.master')

@section('moduleName')
    My Projects
@endsection

@section('style')
    <!--<link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.css')}}">-->
@endsection

@section('content')

@php
    $pageRangArray = config('constant.page_range');
@endphp

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">My Projects</li>
                </ol>
            </div>
        </div>
    </div>
</section>
@include('sweet::alert')

<section class="content">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header bg-dark {{(!$request->has('search_submit'))?'collapsed':''}}" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
                    <h3 class="card-title">Filter Project</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div id="collapseOne" class="panel-collapse in {{(!$request->has('search_submit'))?'collapse':'collapse show'}}">
                    <form class="form-horizontal" method="get" action="{{url('/view/myproject/')}}" name="search_filter" id="search_filter">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Project Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_project_name" id="search_by_project_name">
                                        <option value="">Select</option>
                                        @if(!empty($project_name))
                                            @foreach($project_name as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_project_name) && ($request->search_by_project_name == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label>Project Status</label>
                                        <select name="search_project_status" id="search_project_status" class="form-control">
                                            <option value="">Select Project Status</option>
                                            <option value="0" <?php echo (isset($request->search_project_status) &&  $request->search_project_status == 0)?'selected':''?>>All</option>
                                            <?php
                                            $pstatus = config('constant.project_status');
                                            asort($pstatus);
                                            ?>
                                            @if(config('constant.project_status') && !empty(config('constant.project_status')))
                                            @foreach($pstatus as $key => $projectStatusVal)
                                            <option value="{{$key}}" <?PHP echo (isset($request->search_project_status) && $key == $request->search_project_status || ($key == 1 && !isset($request->search_project_status)))?'selected':''?>>{{$projectStatusVal}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                </div>
                                 @if(!Auth::user()->hasRole(config('constant.developer_slug')))
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Client Name</label>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="search_by_client" id="search_by_client">
                                        <option value="">Select</option>
                                        @if(!empty($client_name))
                                            @foreach($client_name as $key => $val)
                                                <option value="{{$key}}" {{(isset($request->search_by_client) && ($request->search_by_client == $key))?'selected':''}}>{{$val}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                 @endif
                            </div>

                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Project Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_start_date" id="search_by_start_date" value="{{(isset($request->search_by_start_date)?$request->search_by_start_date:'')}}" class="form-control" autocomplete="off" readonly />
                                    </div>
                                </div>
                                @if(Auth::user()->hasRole(config('constant.developer_slug')))
                                <div class="form-group col-lg col-md-6 col-sm-6"></div>
                                <div class="form-group col-lg col-md-6 col-sm-6"></div>
                                @else
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Project End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_end_date" id="search_by_end_date" value="{{(isset($request->search_by_end_date)?$request->search_by_end_date:'')}}"  class="form-control" autocomplete="off" readonly />
                                    </div>
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <label class="">Created Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <input type="text" name="search_by_created_date" id="search_by_created_date" value="{{(isset($request->search_by_created_date)?$request->search_by_created_date:'')}}" class="form-control" autocomplete="off" readonly />
                                    </div>
                                </div>
                            @endif
                            </div>
                            <div class="row">
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <a href="javascript:void(0)" id="reset_startdate">Reset</a>
                                </div>
                                @if(Auth::user()->hasRole(config('constant.developer_slug')))
                                <div class="form-group col-lg col-md-6 col-sm-6"></div>
                                <div class="form-group col-lg col-md-6 col-sm-6"></div>
                                @else
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <a href="javascript:void(0)" id="reset_enddate">Reset</a>
                                </div>
                                <div class="form-group col-lg col-md-6 col-sm-6">
                                    <a href="javascript:void(0)" id="reset_createddate">Reset</a>
                                </div>
                                @endif
                            </div>
                        </div>
                        {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                        <div class="card-footer">
                            <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                                Search
                            </button>
                            <button name="search_reset" type="reset" class="btn btn-info btn-secondary" onclick="location.href ='{{url('/view/myproject')}}'">
                                Reset
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-1 ">
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-7"></div>
                            <div class="col-md-4">
                                <div class="card-body show_total_no">
                                    Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of total {{$data->total()}} entries
                                </div>
                            </div>
                        </div>
                        @include('pms.myproject.myproject_listing')
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection

