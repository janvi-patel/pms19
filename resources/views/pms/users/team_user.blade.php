@extends('layouts.master')
@section('moduleName')
    User
@endsection
@section('content')
@php
$statusArray = config('constant.status');
$departmentArray = config('constant.department');
$pageRangArray = config('constant.page_range');
@endphp
@include('sweet::alert')

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
             <!--    <h1>User Management</h1>
 -->            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Team User Management</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
<?php
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <form class="form-horizontal" method="get" action="{{url('teamuser')}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>">
                <div class="card-header bg-dark " data-widget="collapse">
                    <h3 class="card-title">Search User</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="search_by_first_name" id="search_by_first_name" value="{{$request->search_by_first_name}}" placeholder="First Name">
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="search_by_last_name" id="search_by_last_name" value="{{$request->search_by_last_name}}" placeholder="Last Name">
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label class="">Email</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                     <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                 </div>
                                <input type="text" class="form-control" name="search_by_email" id="search_by_email" value="{{$request->search_by_email}}" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label class="">User Code</label>
                            <input type="text" class="form-control" name="search_by_employee_code" id="search_by_employee_code" value="{{$request->search_by_employee_code}}" placeholder="User Code">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Designation</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_designation" id="search_by_designation" >
                                <option value="">Select Designation</option>
                                    @if(isset($searchFilter['designationList']) && !empty($searchFilter['designationList']))
                                        @foreach($searchFilter['designationList'] as $designationDataVal)
                                            <option value="{{ $designationDataVal->id }}" {{(isset($request->search_by_designation) && ($request->search_by_designation == $designationDataVal->id))?'selected':''}}>{{ $designationDataVal->designation_name }}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Reporting To</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_reporting_to" id="search_by_reporting_to" >
                                <option value="">Select Reporting Person</option>
                                    @if(isset($searchFilter['reportingToList']) && !empty($searchFilter['reportingToList']))
                                        @foreach($searchFilter['reportingToList'] as $reportingDataVal)
                                            <option value="{{ $reportingDataVal->id }}" {{(isset($request->search_by_reporting_to) && ($request->search_by_reporting_to == $reportingDataVal->id))?'selected':''}}>{{ $reportingDataVal->first_name.' '.$reportingDataVal->last_name }}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_company" id="search_by_company" >
                                    <option value="0">All Companies</option>
                                        @if(isset($searchFilter['companyList']) && !empty($searchFilter['companyList']))
                                            @foreach($searchFilter['companyList'] as $companyDataVal)
                                                <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_company) && ($request->search_by_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                            @endforeach
                                        @endif
                                </select>
                            @endif
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                        </div>
                    </div>
                </div>
                {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href ='{{url('teamuser')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- Listing -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-1 mb-3 pl-0">
                                <div class="form-group col">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown" style="width: 100%; height:36px;"  >
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                Auth::user()->hasRole(config('constant.hr_slug')))
                            <div class="col-md-2 offset-md-7 mb-3">
                                <div class="form-group col update_checked_user_disabled">
                                    <select class="select2 form-control custom-select update_checked_user" name="update_checked_user" id="update_checked_user" style="width: 100%; height:36px;"  >
                                        <option value="">Select Action</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                        <option value="2">Delete</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 mb-4 col-sm-3">
                                <a href="{{url('users/create')}}" title="Add Employee">
                                    <button type="button" class="btn btn-primary btn-dark btn-block padding-5">Add User</button>
                                </a>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            @if (count($usersData)>0)
                            <div class="table-responsive">
                                <table id="user_data" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                            <th>
                                               <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" id="checkAll" class="custom-control-input">
                                                    <label class="custom-control-label" for="checkAll"></label>
                                                </div>
                                            </th>
                                            @endif
                                            <th>@sortablelink('first_name','First Name')</th>
                                            <th>@sortablelink('last_name','Last Name')</th>
                                            <th>@sortablelink('email','Email')</th>
                                            <th>@sortablelink('employee_id','User Code')</th>
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                            <th>@sortablelink('company.company_name','Company')</th>
                                            @endif
                                            <th>@sortablelink('designation.designation_name','Designation')</th>
                                            <th>@sortablelink('department','Department')</th>
                                            <th>@sortablelink('reportingto.first_name','Reporting To')</th>
                                            <th>Shift Time</th>
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                            <th>Status</th>
                                            <th>Action</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php  $j = $usersData->firstItem(); ?>
                                    @foreach($usersData as $user)
                                    <tr id="{{$user->id}}">
                                        <td> <?php  echo $j;?></td>
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                            Auth::user()->hasRole(config('constant.admin_slug')) ||
                                            Auth::user()->hasRole(config('constant.hr_slug')))
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="update_user_row[]" id="update_user_row_{{ $user['id'] }}" value="{{$user['id'] }}" class="custom-control-input update_user_row">
                                                <label class="custom-control-label" for="update_user_row_{{ $user['id'] }}"></label>
                                            </div>
                                        </td>
                                        @endif
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td class="user_email_index">{{$user->email}}</td>
                                        <!-- <td>{{$user->employee_id}}</td> -->
                                        <td>
                                            {{
                                                $user->getCompanyShortName($user->company_id).sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $user->employee_id)))
                                            }}
                                        </td>
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                            <td>@isset ($user->company->company_name){{$user->company->company_name}}@endisset</td>
                                        @endif
                                        <td>@isset ($user->designation->designation_name){{$user->designation->designation_name}}@endisset</td>
                                        <td>@isset ($user->department){{$departmentArray[$user->department]}}@endisset</td>
                                        <td nowrap="nowrap" class="reporting_to" id="reporting_to_{{$user->id}}">
                                            <span class="reporting_edit" id="{{$user->id}}" data-reportingid="{{ (isset($user->reportingto) && !empty($user->reportingto)) ? $user->reportingto->id : ''}}">
                                                @if(isset($user->reportingto->first_name) && isset($user->reportingto->last_name))
                                                {{$user->reportingto->first_name.' '.$user->reportingto->last_name}}
                                                @endif
                                            </span>
                                             @if(Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                                                Auth::user()->hasRole(config('constant.admin_slug')))
                                                <select class="form-control select2 edit_reporting_to" style="width: 100%; height:36px;display:none" name="edit_reporting_to" id="edit_reporting_to">

                                                    @foreach($editReporting as $reportingDataVal)
                                                        @php $selected = (isset($user->reportingto->id) && isset($reportingDataVal['id']) && $user->reportingto->id == $reportingDataVal['id']) ? "selected" : ""; @endphp
                                                        <option value="{{ $reportingDataVal['id'] }}" {{ $selected }}>{{ $reportingDataVal['first_name'].' '.$reportingDataVal['last_name'] }}</option>
                                                    @endforeach
                                                </select>
                                                <i class="fa fa-edit edit_reporting_form" style="cursor: context-menu;"></i>
                                            @endif
                                        </td>

                                        <td style='width: 11%;'>
                                            <?php
                                            $from_shift = ($user->from_shift != '')?(date("g:iA", strtotime($user->from_shift ))):'N/A';
                                            $to_shift = ($user->to_shift != '')?date("g:iA", strtotime($user->to_shift )):'N/A';
                                            echo $from_shift.'-'.$to_shift;
                                            ?>
                                        </td>
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                            Auth::user()->hasRole(config('constant.admin_slug')) ||
                                            Auth::user()->hasRole(config('constant.hr_slug')))
                                        <td>{{$statusArray[$user->status]}}</td>
                                        <td align="center">
                                                <a href="{{url('users/edit/'.$user->id)}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update">
                                                    <i class="mdi mdi-pencil"></i>
                                                </a>
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteUser" data-user-id="{{$user->id}}">
                                                    <i class="mdi mdi-close"></i>
                                                </a>
                                        </td>
                                        @endif
                                    </tr>
                                     <?php $j++;?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="border-top">
                            <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                        </div>
                        @endif
                        @if($usersData && !empty($usersData))
                        <div class="pt-4">{!! $usersData->appends(\Request::except('page'))->render() !!}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection
@section('javascript')
   <script src="{{asset('js/user.js?'.time())}}"></script>
@endsection
