@extends('layouts.master')
@section('content')
@section('moduleName')
    User
@endsection
@php
    $internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
@include('sweet::alert')
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 no-block align-items-center">
                <div class="text-left">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('users')}}">User Management</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create User</li>
                    </ol>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header bg-dark">
                <h3 class="card-title">Create User</h3>
            </div>
            <form class="form-horizontal error_fix_table" method="post" action="{{url('users/store')}}" name="user_create" id="user_create">
            @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                    Company
                                    <span class="error">*</span>
                                </label>
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                                    <select name="company_id" id="company_id" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                        <option value="">Select Company</option>
                                        @foreach($company as $value)
                                            <option value="{{$value['id']}}" {{ old('company_id') == $value['id'] ? 'selected' : ''}}>{{$value['company_name']}}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <input type="hidden" name="company_id" id="company_id" value="{{$company[0]['id']}}"  class="form-control" />
                                    <input type="text" name="company_id1" id="company_id1" value="{{$company[0]['company_name']}}"  placeholder="{{ $company[0]['company_name'] }}" class="form-control" readonly />
                                @endif
                            </div>
                        </div>
                        @if(isset($internalCompany) && count($internalCompany) > 0)
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="">
                                        Internal Company
                                    </label>
                                    <select name="internal_company_id" id="internal_company_id" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                        <option value="">Select Internal Company</option>
                                        @foreach($internalCompany as $value)
                                            <option value="{{$value['id']}}" {{ old('internal_company_id') == $value['id'] ? 'selected' : ''}}>{{$value['company_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="first_name" class="">
                                    First Name
                                    <span class="error">*</span>
                                </label>
                                    <input type="text" onkeypress="return validateKeyStrokesAlphaOnly(event)" name="first_name" id="first_name" value="{{ old('first_name') }}" class="form-control"   />
                                    @if ($errors->has('first_name'))
                                        <div class="error">{{ $errors->first('first_name') }}</div>
                                    @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="last_name" class="">
                                    Last Name
                                    <span class="error">*</span>
                                </label>
                                <input type="text" onkeypress="return validateKeyStrokesAlphaOnly(event)" name="last_name" id="last_name" value="{{ old('last_name') }}" class="form-control"  />
                                @if ($errors->has('last_name'))
                                <div class="error">{{ $errors->first('last_name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="email" class="">
                                    Company Email
                                    <span class="error">*</span>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control"  />
                                </div>
                                @if ($errors->has('email'))
                                    <div class="error">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="password" class="">
                                    Password
                                    <span class="error">*</span>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-key"></i></span>
                                    </div>
                                    <input type="password" name="password" id="password" value="{{ old('password') }}" class="form-control"  />
                                </div>
                                @if ($errors->has('password'))
                                <div class="error">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="confirm_password" class="">
                                    Confirm Password
                                    <span class="error">*</span>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-key"></i></span>
                                    </div>
                                    <input type="password" name="confirm_password" id="confirm_password" value="{{ old('confirm_password') }}" class="form-control"  />
                                </div>
                                @if ($errors->has('confirm_password'))
                                <div class="error">{{ $errors->first('confirm_password') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="phone" class="">
                                    Contact Number
                                    <span class="error">*</span>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                    </div>
                                    <input type="text" name="phone" onkeypress="return validateKeyStrokesNumberOnly(event)" id="phone" value="{{ old('phone') }}" class="form-control"  />
                                </div>
                                @if ($errors->has('phone'))
                                <div class="error">{{ $errors->first('phone') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">Date Of Birth</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="dob" id="dob" value="{{ old('dob') }}" class="form-control"/>
                                </div>
                                @if ($errors->has('dob'))
                                <div class="error">{{ $errors->first('dob') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="personal_email" class="">Personal Email</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <input type="personal_email" name="personal_email" id="personal_email" value="{{ old('personal_email') }}" class="form-control" />
                                </div>
                                @if ($errors->has('personal_email'))
                                <div class="error">{{ $errors->first('personal_email') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="employee_id" class="">
                                    Employee ID
                                </label>
                                <input type="text" name="employee_id" id="employee_id" value="{{ old('employee_id') }}"  placeholder="" class="form-control" readonly />
                                @if ($errors->has('employee_id'))
                                <div class="error">{{ $errors->first('employee_id') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">Department<span class="error">*</span></label>
                                <?php   $dep = config('constant.department');
                                    asort($dep);?>
                                <select name="department" id="department" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                    <option value="">Select Department</option>
                                    @foreach($dep as $key => $value)
                                    <option value="{{$key}}" {{ old('department') == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                    Designation
                                    <span class="error">*</span>
                                </label>
                                <select class="select2 form-control custom-select" name="designation_id" id="designation_id" style="width: 100%; height:36px;" >
                                    <option value="">Select Designation</option>
                                    @foreach($designationData as $designation)
                                    <option value="{{$designation['id']}}" {{ old('designation_id') == $designation['id'] ? 'selected' : ''}}>{{$designation['designation_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                    Role
                                    <span class="error">*</span>
                                </label>
                                <select class="select2 form-control custom-select" name="role_id" id="role_id" style="width: 100%; height:36px;" >
                                    <option value="">Select Role</option>
                                    @foreach($roleList as $role)
                                    <option value="{{$role['id']}}" {{ old('role_id') == $role['id'] ? 'selected' : ''}}>{{$role['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                    Reporting To
                                    <span class="error">*</span>
                                </label>
                                <select class="select2 form-control custom-select" name="reporting_to" id="reporting_to" style="width: 100%; height:36px;" >
                                    <option value="">Select</option>
                                    @foreach($reportingToList as $reportingTo)
                                    <option value="{{$reportingTo['id']}}" {{ old('reporting_to') == $reportingTo['id'] ? 'selected' : ''}}>{{$reportingTo['first_name'].' '.$reportingTo['last_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bank_name" class="">
                                    Bank Name
                                </label>
                                <input type="text" name="bank_name"  onkeypress="return validateKeyStrokesAlphaOnly(event)"  id="bank_name" value="{{ old('bank_name') }}" class="form-control" />
                                @if ($errors->has('bank_name'))
                                <div class="error">{{ $errors->first('bank_name') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bank_account_no" class="">
                                    Bank Account Number
                                </label>
                                <input type="text" name="bank_account_no"  onkeypress="return /[0-9]/i.test(event.key)" id="bank_account_no" value="{{ old('bank_account_no') }}" class="form-control" />
                                @if ($errors->has('bank_account_no'))
                                <div class="error">{{ $errors->first('bank_account_no') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="ifsc_code" class="">
                                   IFSC Code
                                </label>
                                <input type="text" name="ifsc_code"  id="ifsc_code" value="{{ old('ifsc_code') }}" class="form-control ifsc_code" />
                                <span class=" error ifsc_err" ></span>
                                @if ($errors->has('ifsc_code'))
                                <div class="error ifsc_err" >{{ $errors->first('ifsc_code') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="pan_no" class="">
                                    PAN Number
                                </label>
                                <input type="text" name="pan_no" id="pan_no" value="{{ old('pan_no') }}" class="form-control" />
                                <span class="pan_no" style="color:red;"></span>
                                @if ($errors->has('pan_no'))
                                <div class="error">{{ $errors->first('pan_no') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                    Joining Date
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="joining_date" id="joining_date" value="{{ old('joining_date') }}" class="form-control rounded-right" />
                                    <span class="dob_error" style="color:red;"></span>
                                </div>
                                @if ($errors->has('joining_date'))
                                <div class="error">{{ $errors->first('joining_date') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group bootstrap-timepicker timepicker">
                                <label class="">
                                    From Shift
                                    <span class="error">*</span>
                                </label>
                                <div class="input-group ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-clock"></i></span>
                                    </div>
                                    <input type="text" name="from_shift" id="from_shift" value="{{ old('from_shift') }}" class="form-control" />
                                </div>
                                @if ($errors->has('from_shift'))
                                <div class="error">{{ $errors->first('from_shift') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group bootstrap-timepicker timepicker">
                                <label class="">
                                    To Shift
                                    <span class="error">*</span>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-clock"></i></span>
                                    </div>
                                    <input type="text" name="to_shift" id="to_shift" value="{{ old('to_shift') }}" class="form-control" />
                                </div>
                                @if ($errors->has('to_shift'))
                                <div class="error">{{ $errors->first('to_shift') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="available_leaves" class="">
                                    Total Leaves
                                    <span class="error">*</span>
                                </label>
                                    <input type="text" name="available_leaves" id="available_leaves" onkeypress="return validateKeyStrokesDecimal(event)"  value="{{ old('available_leaves') }}" class="form-control" />
                                    @if ($errors->has('available_leaves'))
                                        <div class="error">{{ $errors->first('available_leaves') }}</div>
                                    @endif

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group emp_status">
                                <label class="">
                                    Employee Status
                                    <span class="error">*</span>
                                </label>
                                <select name="employee_status" id="employee_status" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                    <option value="">Select Status</option>
                                    @foreach(config('constant.employee_status') as $key => $value)
                                    <option value="{{$key}}" {{ old('employee_status') == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 status_date d-none">
                            <div class="form-group">
                                <label class="">
                                    Confirmation Date
                                    <span class="error">*</span>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="confirmation_date" id="confirmation_date" value="" class="form-control" readonly="readonly">
                                </div>
                                <label class="confirmation_date_error error" for="confirmation_date"></label>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex align-items-center justify-content-center">
                            <div class="form-group mb-md-0 mb-2 input-group">
                                <input type="checkbox" id="work_home" name="work_home">
                                <label for="reason" class="mb-0 pl-4">Is Doing work from home?</label>
                            </div>
                        </div>
                        <div class="col-md-3 work_home_start_date">
                            <div class="form-group">
                                <label class="">
                                    Start Date
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="work_start_date" id="work_start_date" value="" class="form-control" readonly="readonly">
                                    <span class="wsd_error" style="color:red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 work_home_end_date">
                            <div class="form-group">
                                <label class="">
                                    End Date
                                </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <input type="text" name="work_end_date" id="work_end_date" value="" class="form-control" readonly="readonly">
                                    <span class="wed_error" style="color:red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group emp_gratuity">
                                <label class="">
                                    Gratuity
                                    <span class="error">*</span>
                                </label>
                                <select name="gratuity" id="gratuity" class="form-control" style="width: 100%; height:36px;" >
                                    <option value="">Select Gratuity</option>
                                    @foreach(config('constant.gratuity') as $key => $value)
                                    <option value="{{$key}}" {{ old('gratuity') == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('gratuity'))
                                        <div class="error">{{ $errors->first('gratuity') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group emp_provident_fund">
                                <label class="">
                                    Provident Fund
                                    <span class="error">*</span>
                                </label>
                                <select name="provident_fund" id="provident_fund" class="form-control" style="width: 100%; height:36px;" >
                                    <option value="">Select Provident Fund</option>
                                    @foreach(config('constant.provident_fund') as $key => $value)
                                    <option value="{{$key}}" {{ old('provident_fund') == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('provident_fund'))
                                        <div class="error">{{ $errors->first('provident_fund') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                   Bonus Applicable ?
                                            <span class="error">*</span>
                                 </label>
                                  <select name="bonus_applicable" id="bonus_applicable" class="form-control" style="width: 100%; height:36px;" >
                                  @foreach(config('constant.bonus_applicable') as $key => $value)
                                    <option value="{{$key}}" {{ old('bonus_applicable') == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                   Salary Structure
                                 </label>
                                  <select name="salary_structure" id="salary_structure" class="form-control" style="width: 100%; height:36px;" >
                                  <option value="">--- Please Select ---</option>
                                  @foreach(config('constant.salary_structure') as $key => $value)
                                    <option value="{{$key}}" {{ old('salary_structure') == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                    Punch Consideration
                                 </label>
                                  <select name="punch_consideration" id="punch_consideration" class="form-control" style="width: 100%; height:36px;" >
                                  <option value="">--- Please Select ---</option>
                                  @foreach(config('constant.punch_consideration') as $key => $value)
                                    <option value="{{$key}}" {{ old('punch_consideration') == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                   Employee Join As
                                 </label>
                                  <select name="employee_join_as" id="employee_join_as" class="form-control" style="width: 100%; height:36px;" >
                                  <option value="">--- Please Select ---</option>
                                  @foreach(config('constant.employee_join_as') as $key => $value)
                                    <option value="{{$key}}" {{ old('employee_join_as') == $key ? 'selected' : ''}}>{{$value}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="">
                                   Employee Join As Trainee / Probation Months
                                 </label>
                                  <input type="text" name="trainee_probation_month" id="trainee_probation_month" value="{{ old('trainee_probation_month') }}" class="form-control"/>
                                   @if ($errors->has('trainee_probation_month'))
                                    <div class="error">{{ $errors->first('trainee_probation_month') }}</div>
                                    @endif

                            </div>
                        </div>
                        <div class="col-md-3 provident_fund_amount" style="display:none;">
                            <div class="form-group">
                                <label class="">Provident Fund Amount<span class="error">*</span></label>
                                <input type="text" name="provident_fund_amount" id="provident_fund_amount" value="" class="form-control">
                                @if ($errors->has('provident_fund_amount'))
                                    <div class="error">{{ $errors->first('provident_fund_amount') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 align-items-center mb-0 mt-3 justify-content-center">
                            <div class="form-group input-group mb-0">
                                <input type="checkbox" id="exclude_user" name="exclude_user">
                                <label for="exclude_user" class="pl-4">Want to Exclude?</label>
                            </div>
                            <div class="form-group input-group mb-0">
                                <input type="checkbox" id="want_to_exclude_for_salary" name="want_to_exclude_for_salary">
                                <label for="want_to_exclude_for_salary" class="pl-4">Want to Exclude From Salary Slip?</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="Save" class="btn btn-primary btn-dark">
                    <a href="{{url('users')}}"><button type="button" value="Cancel" class="btn btn-info btn-secondary">Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</section>

<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
@endsection

@section('javascript')
   <script src="{{asset('js/user.js?'.time())}}"></script>
   @if(!Auth::user()->hasRole(config('constant.superadmin_slug')))
   <script type="text/javascript">
    createEmployeeCode({{ $company[0]['id'] }});
   </script>
   @endif
@endsection
