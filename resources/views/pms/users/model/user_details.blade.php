<div class="modal-dialog userdetailsmodalwidth modal-md">
    <div class="modal-content">
        <div class="modal-header card-header bg-dark">
            <h4 class="modal-title">User Details</h4>
            <button type="button" class="close" data-dismiss="modal"
                style="color: #fff; opacity: 1;">&times;</button>
        </div>
        <div class="modal-body">
             <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                     <table class="table table-striped table-bordered">
                                        <tr>
                                            <th style="width: 20%">First Name</th>
                                            <td>
                                                {{ $userDetails['first_name']}}
                                            </td>
                                            <th>Last Name</th>
                                            <td>
                                                {{$userDetails['last_name']}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Company Email</th>
                                            <td>
                                                {{ $userDetails['email']}}
                                            </td>
                                            <th>Personal Email</th>
                                            <td>
                                                {{$userDetails['personal_email']}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Contact Number</th>
                                            <td>
                                                {{ $userDetails['phone']}}
                                            </td>
                                            <th>Date Of Birth</th>
                                            <td>
                                                {{$userDetails['birthdate']}}
                                            </td>
                                        </tr>
                                         <tr>
                                            <th style="width: 20%">Employee ID</th>
                                            <td>
                                                {{ $userDetails['employee_id'] }}
                                            </td>
                                            <th>Designation</th>
                                            <td>
                                                 @foreach($designationData as $designation)
                                                
                                                 {{ ($userDetails['designation_id'] == $designation['id'] ? $designation['designation_name'] : '')}}
                                                @endforeach
                                                
                                            </td>
                                        </tr>
                                         <tr>
                                            <th style="width: 20%">Department</th>
                                            <td>
                                                   <?php   $dep = config('constant.department');
                                    asort($dep);?>
                                               @foreach($dep as $key => $value)
                                             {{  ($userDetails['department'] == $key ? $value : '')}}
                                            @endforeach
                                            </td>
                                            <th>Reporting To</th>
                                            <td>
                                                @foreach($reportingToList as $reportingTo)
                                           {{ ($userDetails['reporting_to'] == $reportingTo['id'] ? $reportingTo['first_name'].' '.$reportingTo['last_name'] : '')}}
                                            @endforeach
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Bank Name</th>
                                            <td>
                                                {{$userDetails['bank_name']}}
                                            </td>
                                            <th>Bank Account Number</th>
                                            <td>
                                                {{$userDetails['bank_account_no']}} 
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">IFSC Code</th>
                                            <td>
                                                {{$userDetails['ifsc_code']}}
                                            </td>
                                            <th>PAN Number</th>
                                            <td>
                                                {{$userDetails['pan_no']}} 
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Joining Date</th>
                                            <td>
                                                {{$userDetails['joining_date']}}
                                            </td>
                                            <th>From Shift</th>
                                            <td>
                                                {{$userDetails['from_shift']}} 
                                            </td>
                                        </tr>
                                         <tr>
                                            <th style="width: 20%">To Shift</th>
                                            <td>
                                                {{$userDetails['to_shift'].' p'}}
                                            </td>
                                            <th>Total Leave</th>
                                            <td>
                                                {{$userDetails['available_leaves']}} 
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Used Leave</th>
                                            <td>
                                                {{$userDetails['used_leaves']}}
                                            </td>
                                            <th>User Status</th>
                                            <td>
                                                @foreach(config('constant.status') as $key => $value)
                                            {{ ($userDetails['status'] == $key ? $value : '')}}
                                            @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Employment Status</th>
                                            <td>
                                                @foreach(config('constant.employee_status') as $key => $value)
                                            {{ ($userDetails['employee_status'] == $key ? $value : '')}}
                                            @endforeach
                                            </td>
                                            <th>Confirmation Date</th>
                                            <td>
                                              {{ ($userDetails['confirmation_date'])?date('d-m-Y',strtotime($userDetails['confirmation_date'])):''}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Role</th>
                                            <td>
                                                @foreach($roleList as $role)
                                            {{((isset($userDetails['roleuser']['role_id']) ? $userDetails['roleuser']['role_id'] : '') == $role['id'] ? $role['name'] : '')}}
                                            @endforeach
                                            </td>
                                            <th>Gratuity Status</th>
                                            <td>
                                              @foreach(config('constant.gratuity') as $key => $value)
                                            {{($userDetails['gratuity'] == $key ? $value : '')}}
                                            @endforeach
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Provident Fund</th>
                                            <td>
                                              @foreach(config('constant.provident_fund') as $key => $value)
                                            {{((isset($userDetails['provident_fund']) ? $userDetails['provident_fund'] : '') == $key ? $value : '')}}
                                            @endforeach
                                            </td>
                                            <th>Salary Structure</th>
                                            <td>
                                               @foreach(config('constant.salary_structure') as $key => $value)
                                            {{ ((isset($userDetails['salary_structure']) ? $userDetails['salary_structure'] : '') == $key ? $value : '')}}
                                            @endforeach
                                            </td>
                                        </tr>
                                         <tr>
                                            <th style="width: 20%">Employee Join As</th>
                                            <td>
                                               @foreach(config('constant.employee_join_as') as $key => $value)
                                           {{ ((isset($userDetails['employee_join_as']) ? $userDetails['employee_join_as'] : '') == $key ? $value : '')}}
                                            @endforeach
                                            </td>
                                            <th>Provident Fund Amount</th>
                                            <td>
                                              {{$userDetails['provident_fund_amount']}}
                                            </td>
                                        </tr>
                                         <tr>
                                            <th style="width: 20%">Is Doing work from home?</th>
                                            <td>
                                              {{$workFromHome?'Yes':'No'}}
                                            </td>
                                           
                                         <th>Want to Exclude?</th>
                                            <td>
                                              {{$userDetails['want_to_exclude']?'Yes':'No'}}
                                            </td>
                                           
                                        </tr>
                                        <tr>
                                            <th style="width: 20%">Want to Exclude From Salary Slip?</th>
                                            <td>
                                              {{isset($userDetails['want_to_exclude_for_salary']) && $userDetails['want_to_exclude_for_salary'] == 1?'Yes':'No'}}
                                            </td>
                                             <?php if($userDetails['employee_status'] == 3){?>
                                                     <th>Last Date</th>
                                                    <td>
                                                      {{$userDetails['last_date']}}
                                                    </td>
                                            <?php }?>
                                           
                                        </tr>
                                        <?php if($workFromHome){?>
                                            <tr>
                                            <th style="width: 20%">Start Date</th>
                                            <td>
                                              {{$workFromHome?$workFromHome->start_date:''}}
                                            </td>
                                            <th>End Date</th>
                                            <td>
                                              {{$workFromHome?$workFromHome->end_date:''}}
                                            </td>
                                        </tr>
                                            
                                        <?php }?>
                                        

                        </table>
                  
                               
                                
                                
                               

                                   

                                
                                
                                
                                 
                                
                                
                              
                </div>
            </div>
        </div>
    </div>
        
    </div>
</div>
