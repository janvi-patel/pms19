@extends('layouts.master')
@section('content')
@include('sweet::alert') 
<style type="text/css">
    .disable{
        display:none;
    }
</style>
   <section class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 no-block align-items-center">
                <div class="text-left">                
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('users')}}">User Management</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Update Fileds</li>
                    </ol>
                </div>   
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
    
<section class="content">
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header bg-dark">
                <h3 class="card-title">Update Fileds</h3>
            </div>                    
            <form class="form-horizontal error_fix_table" method="post" action="{{url('dynamicFields/store')}}" name="user_create" id="user_create">
            @csrf
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                      <thead>
                          <h4 class="card-title">Update Fileds</h4>
                          <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Sort Number</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if(count($final_data)>0)
                          <?php $Fileds='';?>
                         
                          @foreach($final_data as $k => $v)
                         
                          <tr>
                            <td>
                             <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="update_user_row[]" id="update_user_row_{{ $k }}" value="{{$k}}"  class="custom-control-input update_user_row"  @if(in_array($k,$field_id)){{'checked'}}@endif>
                                    <label class="custom-control-label" for="update_user_row_{{ $k }}"></label>
                            </div>         
                            </td>
                            <td><?php print_r(ucfirst($v));?></td>
                            <td><input type="text" id="sort_column_{{$k}}" name="sort_column_{{$k}}" value="{{isset($sort_data[$k])?$sort_data[$k]:''}}" class="{{(in_array($k,$field_id))?:'disable'}}"></td>
                          </tr>
                          @endforeach
                          @else
                            <tr>
                              <td colspan="8">
                              <center><h4 class="card-title">No DataField Found</h4></center>
                              </td>
                            </tr>
                          @endif
                         
                      </tbody> 
                    </table>
                  </div>
                </div>                  
                  <button type="submit" class="btn btn-dark mr-2">Update</button>
            </form>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</section>
@endsection
@section('javascript')
  
        <script>
          $(document).ready(function() {
             $(".update_user_row").change(function(){
                var id = $(this).val();
                if ($(this).prop("checked")){
                    $("#sort_column_"+id).removeClass("disable");
                }else{
                    $("#sort_column_"+id).addClass("disable");
                }
                
            });
          });
        </script>
@endsection

                                                   
                           
                            
                                                     
                          
                         
                         
                           
                      
                         
                        
                         
                           
                     
                         
                            
                          
                         
                         
                          
                          
                         
                         
                   
           