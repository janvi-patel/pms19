<div class="card-header border-0" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">
    <h3 class="card-title">add employee template</h3>
    <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapsed">
            <i class="fa fa-chevron-down text-dark"></i>
        </button>
    </div>
</div>
<div id="collapseOne" class="panel-collapse in collapse">
    <div class="card-body">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>keyword</th>
                        <th>use</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>[first_name]</td>
                        <td>user's first name</td>
                    </tr>
                    <tr>
                        <td>[full_name]</td>
                        <td>user's full name</td>
                    </tr>
                    <tr>
                        <td>[user_name]</td>
                        <td>use for login in pms</td>
                    </tr>
                    <tr>
                        <td>[password]</td>
                        <td>user's password</td>
                    </tr>
                    <tr>
                        <td>[company_name]</td>
                        <td>company name</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div> 

<div class="card-header border-0" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false">
    <h3 class="card-title">update employment status</h3>
    <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapsed">
            <i class="fa fa-chevron-down text-dark"></i>
        </button>
    </div>
</div>
<div id="collapseTwo" class="panel-collapse in collapse">
    <div class="card-body">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>keyword</th>
                        <th>use</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>[first_name]</td>
                        <td>user's first name</td>
                    </tr>
                    <tr>
                        <td>[full_name]</td>
                        <td>user's full name</td>
                    </tr>
                    <tr>
                        <td>[company_name]</td>
                        <td>company name</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div> 

<div class="card-header border-0" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false">
    <h3 class="card-title">update designation</h3>
    <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapsed">
            <i class="fa fa-chevron-down text-dark"></i>
        </button>
    </div>
</div>
<div id="collapseThree" class="panel-collapse in collapse">
    <div class="card-body">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>keyword</th>
                        <th>use</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>[first_name]</td>
                        <td>user's first name</td>
                    </tr>
                    <tr>
                        <td>[full_name]</td>
                        <td>user's full name</td>
                    </tr>
                    <tr>
                        <td>[old_designation]</td>
                        <td>use's old designation</td>
                    </tr>
                    <tr>
                        <td>[new_designation]</td>
                        <td>user's new designation</td>
                    </tr>
                    <tr>
                        <td>[company_name]</td>
                        <td>company name</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div> 

<div class="card-header border-0" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false">
    <h3 class="card-title">update appraisal mail</h3>
    <div class="card-tools">
        <button type="button" class="btn btn-tool" data-widget="collapsed">
            <i class="fa fa-chevron-down text-dark"></i>
        </button>
    </div>
</div>
<div id="collapseFour" class="panel-collapse in collapse">
    <div class="card-body">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th>keyword</th>
                        <th>use</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>[first_name]</td>
                        <td>user's first name</td>
                    </tr>
                    <tr>
                        <td>[full_name]</td>
                        <td>user's full name</td>
                    </tr>
                    <tr>
                        <td>[from_date]</td>
                        <td>effective date from</td>
                    </tr>
                    <tr>
                        <td>[to_date]</td>
                        <td>next increment date</td>
                    </tr>
                    <tr>
                        <td>[new_ctc]</td>
                        <td>new ctc amount</td>
                    </tr>
                    <tr>
                        <td>[designation]</td>
                        <td>user's new designation</td>
                    </tr>
                    <tr>
                        <td>[appraisal_amount]</td>
                        <td>user's appraisal amount</td>
                    </tr>
                    <tr>
                        <td>[company_name]</td>
                        <td>company name</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div> 