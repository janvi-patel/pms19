@extends('layouts.master')
@section('moduleName')
    User
@endsection
@section('content')
@php
$statusArray = config('constant.status');
$pageRangArray = config('constant.page_range');
$departmentArray = config('constant.department');
$userStatusArray = config('constant.user_status');
$EmployentStatusArray = config('constant.employee_status');

$departmentExclude = array(5,8,9);
$internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
@include('sweet::alert')
<style>
    @keyframes spinner-border {
        to {transform: rotate(360deg);}
    }
    .spinner-border {
        position:fixed;
        top:50%;
        right:50%;
        width:50%;
        height:50%;
        z-index:10000000;
        opacity: 0.7;
        filter: alpha(opacity=40);
        display: inline-block;
        width: 2rem;
        height: 2rem;
        vertical-align: text-bottom;
        border: .25em solid currentColor;
        border-right-color: transparent;
        border-radius: 50%;
        -webkit-animation: spinner-border .75s linear infinite;
        animation: spinner-border 1s linear infinite;
    }
    .disabletext{
       color:blue;
    }
    .disableLabel{
     /*   opacity:0.5;*/
        pointer-events: none;
    }
    .userdetailsmodalwidth
    {
        max-width:850px !important;
        width: 100% !important;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.css')}}" />

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <!-- <div class="col-sm-6">
                <h1>User Management</h1>
            </div> -->
            <div class="col-sm-12">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">User Management</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
<?php 
 $search_class = "collapsed-card";
 ?>
 @if(Request::get('search_submit'))
    <?php 
        $search_class = "";
    ?>
@endif
<section class="content">
    <div class="container-fluid">
        <form class="form-horizontal" method="get" action="{{url('/users/'.$request->search_by_company)}}" name="search_filter" id="search_filter">
            <div class="card <?php echo $search_class;?>">
                <div class="card-header bg-dark " data-widget="collapse">
                    <h3 class="card-title">Search User</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="search_by_first_name" id="search_by_first_name" value="{{$request->search_by_first_name}}" placeholder="First Name">
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="search_by_last_name" id="search_by_last_name" value="{{$request->search_by_last_name}}" placeholder="Last Name">
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">
                            <label class="">Email</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                     <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                 </div>                                  
                                <input type="text" class="form-control" name="search_by_email" id="search_by_email" value="{{$request->search_by_email}}" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group col-lg col-md-6 col-sm-6">                                
                            <label class="">User Code</label>
                            <input type="text" class="form-control" name="search_by_employee_code" id="search_by_employee_code" value="{{$request->search_by_employee_code}}" placeholder="User Code">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label>Designation</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_designation" id="search_by_designation" >
                                <option value="">Select Designation</option>  
                                    @if(isset($searchFilter['designationList']) && !empty($searchFilter['designationList']))
                                        @foreach($searchFilter['designationList'] as $designationDataVal)
                                            <option value="{{ $designationDataVal->id }}" {{(isset($request->search_by_designation) && ($request->search_by_designation == $designationDataVal->id))?'selected':''}}>{{ $designationDataVal->designation_name }}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </div>                            
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">                                
                            <label>Reporting To</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_reporting_to" id="search_by_reporting_to" >
                                <option value="">Select Reporting Person</option>  
                                    @if(isset($searchFilter['reportingToList']) && !empty($searchFilter['reportingToList']))
                                        @foreach($searchFilter['reportingToList'] as $reportingDataVal)
                                            <option value="{{ $reportingDataVal->id }}" {{(isset($request->search_by_reporting_to) && ($request->search_by_reporting_to == $reportingDataVal->id))?'selected':''}}>{{ $reportingDataVal->first_name.' '.$reportingDataVal->last_name }}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </div>
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">                                
                                <label>Company</label>
                                <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_company" id="search_by_company" >
                                    <option value="0">All Companies</option>
                                        @if(isset($searchFilter['companyList']) && !empty($searchFilter['companyList']))
                                            @foreach($searchFilter['companyList'] as $companyDataVal)
                                                <option value="{{ $companyDataVal->id }}" {{(isset($request->search_by_company) && ($request->search_by_company == $companyDataVal->id))?'selected':''}}>{{ $companyDataVal->company_name }}</option>
                                            @endforeach
                                        @endif
                                </select>
                            </div>
                            @endif
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label>Department</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_department[]" id="search_by_department" multiple="multiple">
                                <option value="">Select Department</option>  
                                    @if(isset($departmentArray) && !empty($departmentArray))
                                        @foreach($departmentArray as $key => $val)
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin')))
                                                @if(!isset($request->search_by_department))
                                                    <option selected="selected" value="{{ $key }}">{{ $val }}</option>
                                                @else
                                                    <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $key }}" {{(isset($request->search_by_department) && (in_array($key,$request->search_by_department)))?'selected':''}}>{{ $val }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                            </select>
                        </div> 

                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                            @if(isset($internalCompany) && count($internalCompany) > 0)
                                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                    <label>Internal Company</label>
                                    <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_internal_company_id[]" id="search_by_internal_company_id" multiple="multiple">
                                        <option value="">Select Internal Company</option> 
                                        @if(isset($internalCompany) && !empty($internalCompany))
                                            @foreach($internalCompany as $key => $val)
                                                <option value="{{ $val['id'] }}" {{(isset($request->search_by_internal_company_id) && (in_array($val['id'],$request->search_by_internal_company_id)))?'selected':''}}>{{ $val['company_name'] }}</option>
                                            @endforeach
                                        @endif
                                        <option value="parent_company" {{(isset($request->search_by_internal_company_id) && (in_array('parent_company',$request->search_by_internal_company_id)))?'selected':''}}>Parent Company</option>
                                    </select>
                                </div>
                            @endif
                        @endif
                        
                        @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                   
                          
                        <div class="form-group col-lg col-md-6 col-sm-6">                                
                            <label>Status</label>
                            <select class="form-control select2" style="width: 100%; height:36px;" name="search_by_status" id="search_by_status" >
                                <option value="">Select status</option>
                                    @if(isset($userStatusArray) && !empty($userStatusArray))
                                        @foreach($userStatusArray as $key => $userStatusVal)
                                            <option value="{{ $key }}" {{(isset($request->search_by_status) && ($request->search_by_status == $key) || !isset($request->search_by_status) && $key == 1)?'selected':''}}>{{ $userStatusVal }}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </div>

                        @endif
                        <div class="form-group col-lg col-md-6 col-sm-6">
                                        <label>Joining Date</label>
                                        <div class="input-group ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="daterange" id="daterange"  class="form-control daterange" value="{{$request->daterange}}"/>
                                        </div>
                                       
                                    </div>
                    </div>
                </div>
                {{-- <input type="hidden" value="{{(isset($request->page_range))?$request->page_range:''}}" name="page_range" id="page_range"> --}}
                <div class="card-footer">
                    <div class="float-left">
                        <button name="search_submit" value="1" type="submit" class="btn btn-primary btn-dark">
                            Search
                        </button>
                        <button name="search_reset" type="button" class="btn btn-info btn-secondary" onclick="location.href ='{{url('users')}}'">
                            Reset
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- Listing -->
        <!-- ============================================================== -->
        <div class="row">
            <span class="hidden spinner-border"></span>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row update_checked_data_row">
                            <div class="col-md-4 mb-0"> 
                                <div class="form-group col pl-0 top-pagination">
                                    <select class="select2 form-control custom-select page_range_dropdown" name="page_range_dropdown" id="page_range_dropdown">
                                        @if(is_array($pageRangArray))
                                           @foreach ($pageRangArray as $key => $node)
                                                <option value="{{$key}}" {{(isset($request->page_range) && ($request->page_range == $node))?'selected':''}}> {{ $node }}</option>
                                            @endforeach 
                                        @endif
                                    </select>
                                </div> 
                                @if(!empty($usersData))
                                <p class="mt-sm-1 mt-0">Showing {{ $usersData->firstItem() }} to {{ $usersData->lastItem() }} of total {{$usersData->total()}} entries</p>
                                @endif
                            </div>
                            <div class="col-md-8 mb-2"> 
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                    Auth::user()->hasRole(config('constant.admin_slug')) ||
                                    Auth::user()->hasRole(config('constant.hr_slug')))
                                    <div class="row justify-content-end">

                                        @if(isset($getConfirmDatePendingList) && count($getConfirmDatePendingList)>0)                                    
                                            <span class="mr-1 text-danger mdi mdi-information-outline mdi-24px information" data-toggle="modal" data-target="#userListModel" data-toggle="tooltip" data-placement="top" data-title="Confirmation Date Pending"></span>
                                        @endif                    
                                        <div class="col-lg-2 top_update_checked_user">
                                            <div class="form-group col update_checked_user_disabled p-0">
                                                <select class="select2 form-control custom-select update_checked_user" name="update_checked_user" id="update_checked_user" style="height:36px;"  >
                                                    <option value="">Select Action</option>
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                    <option value="2">Delete</option>
                                                    <option value="3">Confirmed</option>
                                                </select>                                
                                            </div> 
                                        </div>
                                        <div class="col-lg-2 pr-0 top_update_checked_user">
                                            <a href="{{url('users/email/update/')}}" title="Email Templates">
                                                <button type="button" class="btn btn-primary btn-dark btn-block padding-5">Email Templates</button>
                                            </a>
                                        </div>
                                        <div class="col-lg-2 pr-0 top_update_checked_user">
                                            <a href="{{url('users/create')}}" title="Add Employee">
                                                <button type="button" class="btn btn-primary btn-dark btn-block padding-5">Add User</button>
                                            </a>  
                                        </div>
                                        <div class="col-lg-2 pr-0 top_update_checked_user">
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                                <a title="Download Excel" id="export_to_excel">
                                                    <button type="button" class="btn btn-primary btn-dark btn-block padding-5">Export to Excel</button>
                                                </a>
                                            @endif  
                                        </div>
                                         <div class="col-lg-2 pr-0 top_update_checked_user">
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                               <span class="btn btn-dark  techModal" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Add New Document">Import Excel</span> 
                                                   <!--  <span class="btn btn-primary btn-dark btn-block  techModal padding-5" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Add New Document">Import Excel</span> -->
                                                   <!--  <button class="btn btn-dark float-right techModal padding-5" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-content="Add New Document">Import Excle</button> -->
                                             
                                            @endif  
                                        </div>
                                         <!--  -->
                                    </div>
                                @endif
                            </div>                       
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                            @if (count($usersData)>0)  
                            <div class="table-responsive">
                                <table id="user_data" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>

                                            @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                                             @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                            <th>
                                               <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" id="checkAll" class="custom-control-input">
                                                    <label class="custom-control-label" for="checkAll"></label>
                                                </div>
                                            </th>
                                            @endif
                                             @foreach($fielddata as $key=>$value)
                                            <?php echo "<th>".ucwords($value).'</th>';?>
                                            @endforeach
                                            <th>Action</th>
                                            @else
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                            <th>
                                               <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" id="checkAll" class="custom-control-input">
                                                    <label class="custom-control-label" for="checkAll"></label>
                                                </div>
                                            </th>
                                            @endif
                                            <th>@sortablelink('first_name','First Name')</th>
                                            <th>@sortablelink('email','Email')</th>
                                            <th>@sortablelink('employee_id','User Code')</th>
                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                            <th>@sortablelink('company.company_name','Company')</th>
                                            @endif
                                            <th>@sortablelink('designation.designation_name','Designation')</th>
                                            <th>@sortablelink('department','Department')</th>
                                            <th>@sortablelink('reportingto.first_name','Reporting To')</th>
                                           
                                            <th>@sortablelink('joining_date','Joining Date')</th>

                                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                                Auth::user()->hasRole(config('constant.hr_slug')))
                                            <th>@sortablelink('employee_status','Employment Status')</th>
                                            <th>@sortablelink('status','Status')</th>
                                            <th>Action</th>
                                            @endif
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php  $j = $usersData->firstItem(); ?>
                             
                                    @foreach($usersData as $user)
                                    <tr id="{{$user->id}}" class="{{($user->want_to_exclude==1)?'disabletext':''}}">
                                        <td> <?php  echo $j;?></td>
                                         @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                            Auth::user()->hasRole(config('constant.admin_slug')) ||
                                            Auth::user()->hasRole(config('constant.hr_slug')))
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="update_user_row[]" id="update_user_row_{{ $user['id'] }}" value="{{$user['id'] }}" class="custom-control-input update_user_row">
                                                <label class="custom-control-label" for="update_user_row_{{ $user['id'] }}"></label>
                                            </div>
                                        </td>   
                                        @endif
                                        @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                                         @foreach($fields as $key=>$value)
                                         @php   $fieldName =$value['field_name'] @endphp
                                          <?php if($fieldName == 'name'){?>
                                            <td>{{$user->first_name.' '.$user->last_name}}</td>
                                          <?php } elseif($fieldName == 'designation_id'){ ?>
                                             <td>@isset ($user->designation->designation_name){{$user->designation->designation_name}}@endisset</td>
                                          <?php }elseif($fieldName == 'department'){ ?>
                                              <td>@isset ($user->department){{isset($departmentArray[$user->$fieldName]) ? $departmentArray[$user->department] : ""}}@endisset</td>
                                          <?php }elseif($fieldName == 'employee_status'){ ?>
                                            <td>@isset ($user->$fieldName) {{$user->$fieldName == 1? "Confirmed" : ($user->$fieldName == 2 ? 'Probation' : 'Resign')}}@endisset</td>
                                          <?php }elseif($fieldName == 'gratuity'){ ?>
                                                <?php if($user->$fieldName == 0){ echo "<td>".'No'.'</td>';
                                                 }else{ echo "<td>".'Yes'.'</td>';}?>
                                          <?php } elseif($fieldName == 'provident_fund'){ ?>
                                                <?php if($user->$fieldName == 0){ echo "<td>".'No'.'</td>';
                                                 }else{ echo "<td>".'Yes'.'</td>';}?>
                                          <?php }elseif($fieldName == 'status'){ ?>
                                                <?php if($user->$fieldName == 0){ echo "<td>".'In-Active'.'</td>';
                                                 }else{ echo "<td>".'Active'.'</td>';}?>
                                          <?php }elseif($fieldName == 'want_to_exclude'){ ?>
                                                <?php if($user->$fieldName == 0){ echo "<td>".'For not excluded user'.'</td>';
                                                 }else{ echo "<td>".'For excluded user'.'</td>';}?>
                                          <?php }elseif($fieldName == 'want_to_exclude_for_salary'){ ?>
                                                <?php if($user->$fieldName == 0){ echo "<td>".'In salary'.'</td>';
                                                 }else{ echo "<td>".'Exclude from salary'.'</td>';}?>
                                          <?php }elseif($fieldName == 'bonus_applicable'){ ?>
                                                <?php if($user->$fieldName == 0){ echo "<td>".'Not Applicable'.'</td>';
                                                 }else{ echo "<td>".'Applicable'.'</td>';}?>
                                          <?php }elseif($fieldName == 'salary_structure'){ ?>
                                                <?php if($user->$fieldName == 0){ echo "<td>".'Salaried'.'</td>';
                                                 } elseif($user->$fieldName == 1){ echo "<td>".'Stipend'.'</td>'; }elseif($user->$fieldName == 2){ echo "<td>".'Contractor TDS'.'</td>'; }else{ echo "<td>".'No Stipend'.'</td>';}?>
                                          <?php }elseif($fieldName == 'employee_join_as'){ ?>
                                                <?php if($user->$fieldName == 0){ echo "<td>".'Trainee'.'</td>';
                                                 }else{ echo "<td>".'Probation'.'</td>';}?>
                                          <?php }elseif($fieldName == 'reporting_to'){ ?>
                                                <td nowrap="nowrap" class="reporting_to" id="reporting_to_{{$user->id}}">   
                                                <span class="reporting_edit" id="{{$user->id}}" data-reportingid="{{ (isset($user->reportingto) && !empty($user->reportingto)) ? $user->reportingto->id : ''}}">
                                                    @if(isset($user->reportingto->first_name) && isset($user->reportingto->last_name))
                                                    {{$user->reportingto->first_name.' '.$user->reportingto->last_name}}
                                                    @endif
                                                </span>
                                                 @if(Auth::user()->hasRole(config('constant.project_manager_slug')) || 
                                                    Auth::user()->hasRole(config('constant.admin_slug')))
                                                    <select class="form-control select2 edit_reporting_to" style="width: 100%; height:36px;display:none" name="edit_reporting_to" id="edit_reporting_to" >
                                                        
                                                        @foreach($editReporting as $reportingDataVal)
                                                            <option value="{{ $reportingDataVal['id'] }}" >{{ $reportingDataVal['first_name'].' '.$reportingDataVal['last_name'] }}</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="fa fa-edit edit_reporting_form" style="cursor: context-menu;"></i>
                                                @endif
                                            </td>
                                          <?php }else{?>
                                            
                                            <td>{{$user->$fieldName}}</td>
                                          <?php } ?>
                                         @endforeach
                                          <td align="center">
                                                <a href="{{url('users/edit/'.$user->id)}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update">
                                                    <i class="mdi mdi-pencil"></i>
                                                </a>
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteUser" data-user-id="{{$user->id}}">
                                                    <i class="mdi mdi-close"></i>
                                                </a>
                                                 <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="User Details" class="userDetailsModal" data-id="{{$user->id}}">
                                                    <i class="mdi mdi-account"></i>
                                                </a>
                                                                                             
                                        </td>
                                        @else
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                            Auth::user()->hasRole(config('constant.admin_slug')) ||
                                            Auth::user()->hasRole(config('constant.hr_slug')))
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="update_user_row[]" id="update_user_row_{{ $user['id'] }}" value="{{$user['id'] }}" class="custom-control-input update_user_row">
                                                <label class="custom-control-label" for="update_user_row_{{ $user['id'] }}"></label>
                                            </div>
                                        </td>   
                                        @endif
                                        <td>{{$user->first_name.' '.$user->last_name}}</td>
                                        <td class="user_email_index">{{$user->email}}</td>
                                        <!-- <td>{{$user->employee_id}}</td> -->
                                        <td>
                                            {{
                                                $user->getCompanyShortName($user->company_id).sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $user->employee_id)))
                                            }}                                            
                                        </td>
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                            <td>@isset ($user->company->company_name){{$user->company->company_name}}@endisset</td>
                                        @endif
                                        <td>@isset ($user->designation->designation_name){{$user->designation->designation_name}}@endisset</td>
                                        <td>@isset ($user->department){{isset($departmentArray[$user->department]) ? $departmentArray[$user->department] : ""}}@endisset</td>
                                        <td nowrap="nowrap" class="reporting_to" id="reporting_to_{{$user->id}}">   
                                            <span class="reporting_edit" id="{{$user->id}}" data-reportingid="{{ (isset($user->reportingto) && !empty($user->reportingto)) ? $user->reportingto->id : ''}}">
                                                @if(isset($user->reportingto->first_name) && isset($user->reportingto->last_name))
                                                {{$user->reportingto->first_name.' '.$user->reportingto->last_name}}
                                                @endif
                                            </span>
                                             @if(Auth::user()->hasRole(config('constant.project_manager_slug')) || 
                                                Auth::user()->hasRole(config('constant.admin_slug')))
                                                <select class="form-control select2 edit_reporting_to" style="width: 100%; height:36px;display:none" name="edit_reporting_to" id="edit_reporting_to" >
                                                    
                                                    @foreach($editReporting as $reportingDataVal)
                                                        <option value="{{ $reportingDataVal['id'] }}" >{{ $reportingDataVal['first_name'].' '.$reportingDataVal['last_name'] }}</option>
                                                    @endforeach
                                                </select>
                                                <i class="fa fa-edit edit_reporting_form" style="cursor: context-menu;"></i>
                                            @endif
                                        </td>
                                        
                                         
                                            <?php 
                                        $joining_date = '';
                                        if(isset($user->joining_date) && !empty($user->joining_date)){
                                            $joining_date = date('d-m-Y',strtotime($user->joining_date));} ?>
                                               <td>{{$joining_date}}</td>
                                    
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || 
                                            Auth::user()->hasRole(config('constant.admin_slug')) ||
                                            Auth::user()->hasRole(config('constant.hr_slug')))
                                        <td>@isset ($user->employee_status) {{$user->employee_status == 1? "Confirmed" : ($user->employee_status == 2 ? 'Probation' : 'Resign')}}@endisset</td>
                                        <td>{{$statusArray[$user->status]}}</td>
                                        <td align="center">
                                                <a href="{{url('users/edit/'.$user->id)}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update">
                                                    <i class="mdi mdi-pencil"></i>
                                                </a>
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete deleteUser" data-user-id="{{$user->id}}">
                                                    <i class="mdi mdi-close"></i>
                                                </a>                                            
                                        </td>
                                        @endif
                                        @endif
                                    </tr>
                                     <?php $j++;?>
                                    @endforeach 
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="border-top">
                            <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                        </div>   
                        @endif
                        @if($usersData && !empty($usersData))
                        <div class="pt-4">{!! $usersData->appends(\Request::except('page'))->render() !!}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(isset($getConfirmDatePendingList) && count($getConfirmDatePendingList)>0)
        <div class="modal fade" id="userListModel" tabindex="-1" role="dialog" aria-labelledby="userListModelLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="userListModelLabel">Confirmation date is pending</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table id="project_details_listing" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Employees Name</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($getConfirmDatePendingList as $list)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        <td>{{$list['user_name']}}</td>
                                        <td class="text-center">
                                            <a href="{{url('users/edit/'.$list['id'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update">
                                                <i class="mdi mdi-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
<div class="modal fade" id="addDocsModal" role="dialog"></div>
<div class="modal fade" id="userDetailsModal" role="dialog"></div>
  
</section>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
@endsection
@section('javascript')

   <script src="{{asset('js/moment.min.js')}}"></script>
   <script src="{{asset('js/daterangepicker.js')}}"></script>
   <script src="{{asset('js/user.js?'.time())}}"></script>
   <script>
     $('.daterange').daterangepicker({
        locale: { format: 'YYYY-MM-DD',cancelLabel: 'Clear' },
        drops: 'down',
        opens: 'right',
        
    });
    $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
        $(this).data('daterangepicker').setStartDate(new Date());
        $(this).data('daterangepicker').setEndDate(new Date());
        $(this).val('');
    });
    @if(!(isset($request->daterange) && $request->daterange != ''))
    $('.daterange').val('');
    @endif
   </script>
@endsection