@extends('layouts.master')
@section('content')

@section('moduleName')
    User
@endsection
@php
    $internalCompany = getInternalCompanyList([ "select" => "*","where" =>["parent_company_id" => auth()->user()->company_id]]);
@endphp
@include('sweet::alert')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('users')}}">User Management</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Edit User</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('users/update/'.$userDetails['id'])}}" name="user_edit" id="user_edit">
                        @csrf
                        <div class="card-body edit_user">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                            Company
                                            <span class="error">*</span>
                                        </label>
                                        @if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                                        <select name="company_id" id="company_id" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                            <option value="">Select Company</option>
                                            @foreach($company as $value)
                                                <option value="{{$value['id']}}" @if(old('company_id') != '') @if(old('company_id') == $value['id']) selected @endif @elseif(isset($userDetails['company_id']) && $userDetails['company_id'] == $value['id']) selected @endif>{{$value['company_name']}}</option>
                                            @endforeach
                                        </select>
                                        @else
                                            <input type="hidden" name="company_id" id="company_id" value="{{$company[0]['id']}}"  class="form-control" />
                                            <input type="text" name="company_id1" id="company_id1" value="{{$company[0]['company_name']}}"  placeholder="{{ $company[0]['company_name'] }}" class="form-control" readonly />
                                        @endif
                                    </div>
                                </div>
                                @if(isset($internalCompany) && count($internalCompany) > 0)
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="">
                                                Internal Company
                                            </label>
                                            <select name="internal_company_id" id="internal_company_id" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                                <option value="">Select Internal Company</option>
                                                @foreach($internalCompany as $value)
                                                    <option value="{{$value['id']}}" @if(old('internal_company_id') != '') @if(old('internal_company_id') == $value['id']) selected @endif @elseif(isset($userDetails['internal_company_id']) && $userDetails['internal_company_id'] == $value['id']) selected @endif>{{$value['company_name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="first_name" class="">
                                            First Name
                                            <span class="error">*</span>
                                        </label>
                                        <input type="text" name="first_name"  onkeypress="return validateKeyStrokesAlphaOnly(event)" id="first_name" value="{{ old('first_name') != '' ? old('first_name') : $userDetails['first_name']}}" class="form-control"  />
                                        @if ($errors->has('first_name'))
                                        <div class="error">{{ $errors->first('first_name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="last_name" class="">
                                            Last Name
                                            <span class="error">*</span>
                                        </label>
                                        <input type="text" onkeypress="return validateKeyStrokesAlphaOnly(event)" name="last_name" id="last_name" value="{{ old('last_name') != '' ? old('last_name') : $userDetails['last_name']}}" class="form-control"  />
                                        @if ($errors->has('last_name'))
                                        <div class="error">{{ $errors->first('last_name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="email" class="">
                                            Company Email
                                            <span class="error">*</span>
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="email" name="email" id="email" value="{{ old('email') != '' ? old('email') : $userDetails['email']}}" class="form-control"  />
                                        </div>
                                            @if ($errors->has('email'))
                                            <div class="error">{{ $errors->first('email') }}</div>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="personal_email" class="">
                                        Personal Email</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input type="personal_email" name="personal_email" id="personal_email" value="{{ old('personal_email') != '' ? old('personal_email') : $userDetails['personal_email']}}" class="form-control" />
                                        </div>
                                            @if ($errors->has('personal_email'))
                                        <div class="error">{{ $errors->first('personal_email') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone" class="">
                                            Contact Number
                                            <span class="error">*</span>
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                            </div>
                                            <input type="text" name="phone" onkeypress="return validateKeyStrokesNumberOnly(event)" id="phone" value="{{ old('phone') != '' ? old('phone') : $userDetails['phone']}}" class="form-control"  />
                                        </div>
                                        @if ($errors->has('phone'))
                                        <div class="error">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">Date Of Birth</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="dob" id="dob" value="{{ old('dob') != '' ? old('dob') : $userDetails['birthdate']}}" class="form-control"/>
                                        </div>
                                        @if ($errors->has('dob'))
                                        <div class="error">{{ $errors->first('dob') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="employee_id" class="">Employee ID</label>
                                        <input type="text" class="form-control"name="employee_id" id="employee_id" value="{{ old('employee_id') != '' ? old('employee_id') : $userDetails['employee_id']}}" class="form-control"/>
                                        @if ($errors->has('employee_id'))
                                        <div class="error">{{ $errors->first('employee_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                            Department
                                            <span class="error">*</span>
                                        </label>
                                         @php  $dep = config('constant.department'); asort($dep); @endphp
                                        <select name="department" id="department" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                            <option value="">Select Department</option>
                                            @foreach($dep as $key => $value)
                                            <option value="{{$key}}" @if(old('department') != '') @if(old('department') == $key) selected @endif @elseif(isset($userDetails['department']) && $userDetails['department'] == $key) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                            Designation
                                            <span class="error">*</span>
                                        </label>
                                        <select class="select2 form-control custom-select" name="designation_id" id="designation_id" style="width: 100%; height:36px;" >
                                            <option value="">Select Designation</option>

                                            @foreach($designationData as $designation)
                                            <option value="{{$designation['id']}}" @if(old('designation_id') != '') @if(old('designation_id') == $designation['id']) selected @endif @elseif(isset($userDetails['designation_id']) && $userDetails['designation_id'] == $designation['id']) selected @endif>{{$designation['designation_name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                            Role
                                            <span class="error">*</span>
                                        </label>
                                        <select class="select2 form-control custom-select role_id" name="role_id" id="role_id" style="width: 100%; height:36px;" >
                                            <option value="">Select Role</option>
                                            @foreach($roleList as $role)
                                            <option value="{{$role['id']}}" @if(old('role_id') != '') @if(old('role_id') == $role['id']) selected @endif @elseif(isset($userDetails['roleuser']['role_id']) && $userDetails['roleuser']['role_id'] == $role['id']) selected @endif>{{$role['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                            Reporting To
                                            <span class="error">*</span>
                                        </label>
                                        <select class="select2 form-control custom-select" name="reporting_to" id="reporting_to" style="width: 100%; height:36px;" >
                                            <option value="">Select</option>
                                            @foreach($reportingToList as $reportingTo)
                                            <option value="{{$reportingTo['id']}}" @if(old('reporting_to') != '') @if(old('reporting_to') == $reportingTo['id']) selected @endif @elseif(isset($userDetails['reporting_to']) && $userDetails['reporting_to'] == $reportingTo['id']) selected @endif>{{$reportingTo['first_name'].' '.$reportingTo['last_name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="bank_name" class="">Bank Name</label>
                                        <input type="text" onkeypress="return validateKeyStrokesAlphaOnly(event)" name="bank_name" id="bank_name" value="{{ old('bank_name') != '' ? old('bank_name') : $userDetails['bank_name']}}" class="form-control" />
                                        @if ($errors->has('bank_name'))
                                        <div class="error">{{ $errors->first('bank_name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="bank_account_no" onkeypress="return validateKeyStrokesNumberOnly(event)" class="">Bank Account Number</label>
                                        <input type="text" name="bank_account_no" id="bank_account_no" value="{{ old('bank_account_no') != '' ? old('bank_account_no') : $userDetails['bank_account_no']}}" class="form-control" />
                                        @if ($errors->has('bank_account_no'))
                                        <div class="error">{{ $errors->first('bank_account_no') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="ifsc_code"  class="">IFSC Code</label>
                                        <input type="text" name="ifsc_code" id="ifsc_code" value="{{ old('ifsc_code') != '' ? old('ifsc_code') : $userDetails['ifsc_code']}}" class="form-control" />
                                        @if ($errors->has('ifsc_code'))
                                        <div class="error">{{ $errors->first('ifsc_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="pan_no" class="">PAN Number</label>
                                        <input type="text" name="pan_no" id="pan_no" value="{{ old('pan_no') != '' ? old('pan_no') : $userDetails['pan_no']}}" class="form-control" />
                                        <span class="pan_no" style="color:red;"></span>
                                        @if ($errors->has('pan_no'))
                                        <div class="error">{{ $errors->first('pan_no') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="joining_date" class="">Joining Date</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="joining_date" id="joining_date" value="{{ old('joining_date') != '' ? old('joining_date') : $userDetails['joining_date']}}" class="form-control" />
                                            <span class="dob_error" style="color:red;"></span>
                                        </div>
                                        @if ($errors->has('joining_date'))
                                        <div class="error">{{ $errors->first('joining_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="from_shift" class="">
                                            From Shift
                                            <span class="error">*</span>
                                        </label>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-clock"></i></span>
                                            </div>
                                            <input type="text" name="from_shift" id="from_shift" value="{{ old('from_shift') != '' ? old('from_shift') : $userDetails['from_shift']}}" class="form-control" />
                                            @if ($errors->has('from_shift'))
                                            <div class="error">{{ $errors->first('from_shift') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="to_shift" class="">
                                            To Shift
                                            <span class="error">*</span>
                                        </label>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-clock"></i></span>
                                            </div>
                                            <input type="text" name="to_shift" id="to_shift" value="{{ old('to_shift') != '' ? old('to_shift').' p' : $userDetails['to_shift'].' p'}}" class="form-control" />
                                            @if ($errors->has('to_shift'))
                                            <div class="error">{{ $errors->first('to_shift') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="total_leaves" class="">
                                                Total Leave
                                                <span class="error">*</span>
                                            </label>
                                                <input type="text" name="available_leaves" onkeypress="return validateKeyStrokesDecimal(event)" id="available_leaves" value="{{ old('available_leaves') != '' ? old('available_leaves') : $userDetails['available_leaves']}}" class="form-control" />
                                                @if ($errors->has('available_leaves'))
                                                    <div class="error">{{ $errors->first('available_leaves') }}</div>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="used_leaves" class="">
                                                Used Leave
                                                <span class="error">*</span>
                                            </label>
                                                <input type="text" name="used_leaves" onkeypress="return validateKeyStrokesDecimal(event)" id="used_leaves" value="{{ old('used_leaves') != '' ? old('used_leaves') : $userDetails['used_leaves']}}" class="form-control" />
                                                @if ($errors->has('used_leaves'))
                                                    <div class="error">{{ $errors->first('used_leaves') }}</div>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                            User Status
                                        <span class="error">*</span>
                                        </label>
                                        <select name="user_status" id="user_status" class="select2 border border-danger form-control custom-select" style="width: 100%; height:36px;" required onchange="changeUserStatus(this.value)">
                                            <option value="">Select Status</option>
                                            @foreach(config('constant.status') as $key => $value)
                                            <option value="{{$key}}" @if(old('user_status') != '') @if(old('user_status') == $key) selected @endif @elseif(isset($userDetails['status']) && $userDetails['status'] == $key) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 emp_status">
                                    <div class="form-group">
                                        <label class="">
                                            Employment Status
                                            <span class="error">*</span>
                                        </label>
                                        <select name="employee_status" id="employee_status" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                            <option value="">Select Employement Status</option>
                                            @foreach(config('constant.employee_status') as $key => $value)
                                            <option value="{{$key}}" @if(old('employee_status') != '') @if(old('employee_status') == $key) selected @endif @elseif($userDetails['employee_status'] == $key) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 status_date d-none">
                                    <div class="form-group">
                                        <label class="">
                                            Confirmation Date
                                            <span class="error">*</span>
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="confirmation_date" id="confirmation_date" value="@if(old('confirmation_date') != '') {{ old('confirmation_date') }} @elseif($userDetails['confirmation_date']) {{ date('d-m-Y',strtotime($userDetails['confirmation_date'])) }} @endif" class="form-control" readonly="readonly">
                                        </div>
                                        <label class="confirmation_date_error error" for="confirmation_date"></label>
                                        @if ($errors->has('confirmation_date'))
                                            <div class="error">{{ $errors->first('confirmation_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3 d-flex align-items-center justify-content-center">
                                    <div class="form-group mb-md-0 mb-2 input-group">
                                        <input type="checkbox" id="work_home" name="work_home" {{$workFromHome?'checked':''}}>
                                        <label for="reason" class="mb-0 pl-4">Is Doing work from home?</label>
                                    </div>
                                </div>
                                <div class="col-md-3 work_home_start_date">
                                    <div class="form-group">
                                        <label class="">
                                            Start Date
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="work_start_date" id="work_start_date" value="{{$workFromHome?$workFromHome->start_date:''}}" class="form-control rounded-right" readonly="readonly">
                                            <span class="wsd_error" style="color:red;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 work_home_end_date">
                                    <div class="form-group">
                                        <label class="">
                                            End Date
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                            <input type="text" name="work_end_date" id="work_end_date" value="{{$workFromHome?$workFromHome->end_date:''}}" class="form-control rounded-right" readonly="readonly">
                                            <span class="wed_error" style="color:red;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 emp_gratuity">
                                    <div class="form-group">
                                        <label class="">
                                            Gratuity Status
                                            <span class="error">*</span>
                                        </label>
                                        <select name="gratuity" id="gratuity" class="select2 form-control custom-select" style="width: 100%; height:36px;" >
                                            <option value="">Select Gratuity</option>

                                            @foreach(config('constant.gratuity') as $key => $value)
                                            <option value="{{$key}}" @if(old('gratuity') != '') @if(old('gratuity') == $key) selected @endif @elseif($userDetails['gratuity'] == $key) selected @endif>{{$value}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group emp_provident_fund">
                                        <label class="">
                                            Provident Fund
                                            <span class="error">*</span>
                                        </label>
                                        <select name="provident_fund" id="provident_fund" class="form-control" style="width: 100%; height:36px;" >
                                            <option value="">Select Provident Fund</option>
                                            @foreach(config('constant.provident_fund') as $key => $value)
                                            <option value="{{$key}}"  @if(old('provident_fund') != '') @if(old('provident_fund') == $key) selected @endif @elseif($userDetails['provident_fund'] == $key) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('provident_fund'))
                                                <div class="error">{{ $errors->first('provident_fund') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                           Bonus Applicable ?
                                                    <span class="error">*</span>
                                         </label>
                                          <select name="bonus_applicable" id="bonus_applicable" class="form-control" style="width: 100%; height:36px;" >
                                          @foreach(config('constant.bonus_applicable') as $key => $value)
                                            <option value="{{$key}}" @if(old('bonus_applicable') != '') @if(old('bonus_applicable') == $key) selected @endif @elseif(isset($userDetails['bonus_applicable']) && $userDetails['bonus_applicable'] == $key) selected @endif >{{$value}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                           Salary Structure
                                         </label>
                                          <select name="salary_structure" id="salary_structure" class="form-control" style="width: 100%; height:36px;" >
                                          <option value="">Please Select</option>
                                          @foreach(config('constant.salary_structure') as $key => $value)
                                            <option value="{{$key}}" @if(old('salary_structure') != '') @if(old('salary_structure') == $key) selected @endif @elseif(isset($userDetails['salary_structure']) && $userDetails['salary_structure'] == $key) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                           Punch Consideration
                                         </label>
                                          <select name="punch_consideration" id="punch_consideration" class="form-control" style="width: 100%; height:36px;" >
                                          <option value="">--- Please Select ---</option>
                                          @foreach(config('constant.punch_consideration') as $key => $value)
                                            <option value="{{$key}}" @if(old('punch_consideration') != '') @if(old('punch_consideration') == $key) selected @endif @elseif(isset($userDetails['punch_consideration']) && $userDetails['punch_consideration'] == $key) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                            Employee Join As
                                         </label>
                                          <select name="employee_join_as" id="employee_join_as" class="form-control" style="width: 100%; height:36px;" >
                                          <option value="">Please Select</option>
                                          @foreach(config('constant.employee_join_as') as $key => $value)
                                            <option value="{{$key}}" @if(old('employee_join_as') != '') @if(old('employee_join_as') == $key) selected @endif @elseif(isset($userDetails['employee_join_as']) && $userDetails['employee_join_as'] == $key) selected @endif>{{$value}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="">
                                           Employee Join As Trainee / Probation Months
                                         </label>
                                          <input type="text" name="trainee_probation_month" id="trainee_probation_month" value="{{ old('trainee_probation_month') != '' ? old('trainee_probation_month') : $userDetails['trainee_probation_month']}}" class="form-control"/>
                                           @if ($errors->has('trainee_probation_month'))
                                            <div class="error">{{ $errors->first('trainee_probation_month') }}</div>
                                            @endif

                                    </div>
                                </div>
                                <div class="col-md-3 provident_fund_amount" {{($userDetails['provident_fund'] != 1) ? 'style=display:none' : ''}}>
                                    <div class="form-group">
                                        <label class="">
                                            Provident Fund Amount
                                            <span class="error">*</span>
                                        </label>
                                        <input type="text" name="provident_fund_amount" id="provident_fund_amount" value="{{$userDetails['provident_fund_amount']}}" class="form-control">
                                        @if ($errors->has('provident_fund_amount'))
                                                <div class="error">{{ $errors->first('provident_fund_amount') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3 user_last_date" {{($userDetails['employee_status'] != 3) ? 'style=display:none' : ''}}>
                                    <div class="form-group">
                                        <label class="">
                                            Last Date
                                            <span class="error">*</span>
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>

                                        <input type="text" name="user_last_date" id="user_last_date" value="{{ old('user_last_date') != '' ? old('user_last_date') : $userDetails['last_date']}}" class="form-control">
                                        </div>
                                        <!-- @if ($errors->has('user_last_date'))
                                                <div class="error">{{ $errors->first('user_last_date') }}</div>
                                        @endif -->
                                    </div>

                                </div>
                                <div class="col-md-3 align-items-center mb-0 justify-content-center">
                                    <label class=""></label>
                                    <div class="form-group input-group mb-0">
                                        <input type="checkbox" id="exclude_user" name="exclude_user" {{$userDetails['want_to_exclude']?'checked':''}}>
                                        <label for="exclude_user" class="pl-4">Want to Exclude?</label>
                                    </div>
                                    <div class="form-group input-group mb-0">
                                        <input type="checkbox" id="want_to_exclude_for_salary" name="want_to_exclude_for_salary" {{isset($userDetails['want_to_exclude_for_salary']) && $userDetails['want_to_exclude_for_salary'] == 1?'checked':''}}>
                                        <label for="want_to_exclude_for_salary" class="pl-4">Want to Exclude From Salary Slip?</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="hidden" name="old_role_id" id="old_role_id" value="{{isset($userDetails['roleuser']['role_id']) ? $userDetails['roleuser']['role_id'] : ''}}" class="btn btn-primary">
                            <input type="submit" value="Save" class="btn btn-primary btn-dark">
                            <a href="{{url('users')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row" id="edit_password">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header bg-dark">
                        <h3 class="card-title">Reset Password</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="{{url('users/update/password/'.$userDetails['id'])}}" name="user_password" id="user_password">
                        @csrf
                        <div class="card-body edit_user">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="user_name" class="">
                                            User Name
                                        </label>
                                        <input type="text" id="user_name" value="{{$userDetails['first_name']. ' ' .$userDetails['last_name']}}" class="form-control"  readonly />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="employee_code" class="">Employee ID</label>
                                        <input type="text" class="form-control" id="employee_code" value="{{$userDetails['employee_id']}}" class="form-control" readonly />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="password" class="">
                                            Password
                                            <span class="error">*</span>
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input type="password" name="password" id="password" value="{{ old('password') }}" class="form-control"  />
                                        </div>
                                        @if ($errors->has('password'))
                                        <div class="error">{{ $errors->first('password') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="confirm_password" class="">
                                            Confirm Password
                                            <span class="error">*</span>
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input type="password" name="confirm_password" id="confirm_password" value="{{ old('confirm_password') }}" class="form-control"  />
                                        </div>
                                        @if ($errors->has('confirm_password'))
                                        <div class="error">{{ $errors->first('confirm_password') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Update Password" class="btn btn-primary btn-dark">
                            <a href="{{url('users')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
@endsection

@section('javascript')
    @if ($errors->has('confirm_password') || $errors->has('confirm_password'))
        <script>
          $(document).ready(function() {
              window.location.href = "#edit_password";
          });
        </script>
    @endif
    <script src="{{asset('js/user.js?'.time())}}"></script>
@endsection
