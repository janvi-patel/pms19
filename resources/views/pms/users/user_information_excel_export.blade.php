@php
$statusArray = config('constant.status');
$departmentArray = config('constant.department');
$employee_join_as = config('constant.employee_join_as');
@endphp
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Personal Email</th>
                    <th>Contact Number</th>
                    <th>Date Of Birth</th>
                    <th>User Code</th>
                    @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                    <th>Company</th>
                    @endif
                    <th>Designation</th>
                    <th>Department</th>
                    <th>Reporting To</th>
                    <th>Bank Name</th>
                    <th>Bank Account Number</th>
                    <th>PAN Number</th>
                    <th>Joining Date</th>
                    <th>Confirmation Date</th>
                    <th>Shift Time</th>
                    <th>Total Leave</th>
                    <th>Used Leave</th>
                    <th>Employee Join As</th>
                    <th>Employment Status</th>
                    <th>User Status</th>
                    {{-- <th>Role</th> --}}
                </tr>
            </thead>
            <tbody>
                @if(isset($usersData) && count($usersData)>0)
                    @foreach($usersData as $user)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$user->first_name.' '.$user->last_name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->personal_email}}</td>
                            <td>{{$user->phone}}</td>
                            <td>{{date('d-M-Y',strtotime($user->birthdate))}}</td>
                            <td>
                                {{
                                    $user->getCompanyShortName($user->company_id).sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $user->employee_id)))
                                }}
                            </td>
                            @if(Auth::user()->hasRole(config('constant.superadmin_slug')))
                                <td>@isset ($user->company->company_name){{$user->company->company_name}}@endisset</td>
                            @endif
                            <td>@isset ($user->designation->designation_name){{$user->designation->designation_name}}@endisset</td>
                            <td>@isset ($user->department){{$departmentArray[$user->department]}}@endisset</td>
                            <td>{{$user->reportingto->first_name.' '.$user->reportingto->last_name}}</td>
                            <td>{{$user->bank_name}}</td>
                            <td>{{$user->bank_account_no}}</td>
                            <td>{{$user->pan_no}}</td>
                            <td>{{date('d-M-Y',strtotime($user->joining_date))}}</td>
                            <td>@if(isset($user->confirmation_date) && $user->confirmation_date != "") {{date('d-M-Y',strtotime($user->confirmation_date))}} @endif</td>
                            <td>
                                <?php
                                    $from_shift = ($user->from_shift != '')?(date("g:iA", strtotime($user->from_shift ))):'N/A';
                                    $to_shift = ($user->to_shift != '')?date("g:iA", strtotime($user->to_shift )):'N/A';
                                    echo $user->from_shift.'-'.$user->to_shift;
                                ?>
                            </td>
                            <td>{{$user->available_leaves}}</td>
                            <td>{{$user->used_leaves}}</td>
                            <td>@isset ($user->employee_join_as){{$employee_join_as[$user->employee_join_as]}}@endisset</td>
                            <td>@isset ($user->employee_status) {{$user->employee_status == 1? "Confirmed" : "Probation"}}@endisset</td>
                            <td>{{$statusArray[$user->status]}}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
