@extends('layouts.master')
@section('content')

@section('moduleName')
    Update Templates
@endsection

@php
$pageRangArray = config('constant.page_range');
$statusArray = config('constant.status');
@endphp
@include('sweet::alert')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <!-- <h1>Designation Management</h1> -->
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Templates Management</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>     
 <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12 col-12 mb-4 text-right"> 
            <div class="float-right">
                <span class="btn btn-dark viewKeyword">view keyword</span>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header bg-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseAddUser" aria-expanded="true">
                        <h3 class="card-title">Add Employee Template</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div id="collapseAddUser" class="panel-collapse in collapse @if($errors->any() && old('form_type') == "add_user") show @endif" style="">
                        <div class="col-md-12">
                            <form method='post' action="{{url('email/update')}}">
                                @csrf
                                <input type="hidden"  name="form_type" value="add_user"/>
                                <div class="row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Send Mail To</label>
                                        <input type="text" name="send_to" id="send_to" value="Employee" class="form-control rounded-right" readonly="readonly">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Send to As a CC</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;" name="send_cc[]" id="send_cc"  multiple="multiple">
                                            <option value="">Select</option>
                                                @if(!empty(config('constant.send_mail_to_cc')))
                                                    @foreach(config('constant.send_mail_to_cc') as $key => $sendToCC)
                                                        <option value="{{ $key }}" {{(isset($dataArray['add_user']['send_cc']) && (in_array($key,str_split($dataArray['add_user']['send_cc']))))?'selected':''}}>{{ $sendToCC }}</option>
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <label>Subject</label>
                                        <input type="text" name="subject" id="subject" value="{{(isset($dataArray['add_user']['subject'])) ? $dataArray['add_user']['subject']:''}}" class="form-control rounded-right">
                                        @if ($errors->has('subject') && old('form_type') == "add_user")
                                            <div class="error">{{ $errors->first('subject') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Mail Format</label>
                                    <textarea class="form-control" name="email_format" id="template_for_add_user">{{(isset($dataArray['add_user']['email_format']))?$dataArray['add_user']['email_format']:''}}</textarea>
                                    @if($errors->has('email_format') && old('form_type') == "add_user")
                                        <div class="error">{{ $errors->first('email_format') }}</div>
                                    @endif
                                </div>
                                <div class="card-footer pl-0">
                                    <input type="submit" value="Update Template" class="btn btn-primary btn-dark">
                                    <a href="{{url('users/email/update')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header bg-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateDesignation" aria-expanded="true">
                        <h3 class="card-title">Update Designation</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div id="collapseUpdateDesignation" class="panel-collapse in collapse @if($errors->any() && old('form_type') == "update_designation") show @endif" style="">
                        <div class="col-md-12">
                            <form method='post' action="{{url('email/update')}}">
                                @csrf
                                <input type="hidden"  name="form_type" value="update_designation"/>
                                <div class="row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Send Mail To</label>
                                        <input type="text" name="send_to" id="send_to" value="Employee" class="form-control rounded-right" readonly="readonly">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Send to As a CC</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;" name="send_cc[]" id="send_cc"  multiple="multiple">
                                            <option value="">Select</option>
                                                @if(!empty(config('constant.send_mail_to_cc')))
                                                    @foreach(config('constant.send_mail_to_cc') as $key => $sendToCC)
                                                        <option value="{{ $key }}" {{(isset($dataArray['update_designation']['send_cc']) && (in_array($key,str_split($dataArray['update_designation']['send_cc']))))?'selected':''}}>{{ $sendToCC }}</option>
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <label>Subject</label>
                                        <input type="text" name="subject" id="subject" value="{{(isset($dataArray['update_designation']['subject'])) ? $dataArray['update_designation']['subject']:''}}" class="form-control rounded-right">
                                        @if($errors->has('subject') && old('form_type') == "update_designation")
                                            <div class="error">{{ $errors->first('subject') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Mail Format</label>
                                    <textarea class="form-control" name="email_format" id="template_for_update_designation">{{(isset($dataArray['update_designation']['email_format'])) ? $dataArray['update_designation']['email_format']:''}}</textarea>
                                    @if($errors->has('email_format') && old('form_type') == "update_designation")
                                        <div class="error">{{ $errors->first('email_format') }}</div>
                                    @endif
                                </div>
                                <div class="card-footer pl-0">
                                    <input type="submit" value="Update Template" class="btn btn-primary btn-dark">
                                    <a href="{{url('users/email/update')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header bg-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateEmp" aria-expanded="true">
                        <h3 class="card-title">Update Employment Status</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div id="collapseUpdateEmp" class="panel-collapse in collapse @if($errors->any() && old('form_type') == "update_employment") show @endif" style="">
                        <div class="col-md-12">
                            <form method='post' action="{{url('email/update')}}">
                                @csrf
                                <input type="hidden"  name="form_type" value="update_employment"/>
                                <div class="row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Send Mail To</label>
                                        <input type="text" name="send_to" id="send_to" value="Employee" class="form-control rounded-right" readonly="readonly">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Send to As a CC</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;" name="send_cc[]" id="send_cc"  multiple="multiple">
                                            <option value="">Select</option>
                                                @if(!empty(config('constant.send_mail_to_cc')))
                                                    @foreach(config('constant.send_mail_to_cc') as $key => $sendToCC)
                                                        <option value="{{ $key }}" {{(isset($dataArray['update_employment']['send_cc']) && (in_array($key,str_split($dataArray['update_employment']['send_cc']))))?'selected':''}}>{{ $sendToCC }}</option>
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <label>Subject</label>
                                        <input type="text" name="subject" id="subject" value="{{(isset($dataArray['update_employment']['subject'])) ? $dataArray['update_employment']['subject']:''}}" class="form-control rounded-right">
                                        @if($errors->has('subject') && old('form_type') == "update_employment")
                                            <div class="error">{{ $errors->first('subject') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Mail Format</label>
                                    <textarea class="form-control" name="email_format" id="template_for_update_employment">{{(isset($dataArray['update_employment']['email_format'])) ? $dataArray['update_employment']['email_format']:''}}</textarea>
                                    @if($errors->has('email_format') && old('form_type') == "update_employment")
                                        <div class="error">{{ $errors->first('email_format') }}</div>
                                    @endif
                                </div>
                                <div class="card-footer pl-0">
                                    <input type="submit" value="Update Template" class="btn btn-primary btn-dark">
                                    <a href="{{url('users/email/update')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header bg-dark" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateAppraisal" aria-expanded="true">
                        <h3 class="card-title">Update Appraisal Mail</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div id="collapseUpdateAppraisal" class="panel-collapse in collapse @if($errors->any() && old('form_type') == "update_appraisal") show @endif" style="">
                        <div class="col-md-12">
                            <form method='post' action="{{url('email/update')}}">
                                @csrf
                                <input type="hidden"  name="form_type" value="update_appraisal"/>
                                <div class="row">
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Send Mail To</label>
                                        <input type="text" name="send_to" id="send_to" value="Employee" class="form-control rounded-right" readonly="readonly">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                        <label>Send to As a CC</label>
                                        <select class="form-control select2" style="width: 100%; height:36px;" name="send_cc[]" id="send_cc"  multiple="multiple">
                                            <option value="">Select</option>
                                                @if(!empty(config('constant.send_mail_to_cc')))
                                                    @foreach(config('constant.send_mail_to_cc') as $key => $sendToCC)
                                                        <option value="{{ $key }}" {{(isset($dataArray['update_appraisal']['send_cc']) && (in_array($key,str_split($dataArray['update_appraisal']['send_cc']))))?'selected':''}}>{{ $sendToCC }}</option>
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <label>Subject</label>
                                        <input type="text" name="subject" id="subject" value="{{(isset($dataArray['update_appraisal']['subject'])) ? $dataArray['update_appraisal']['subject']:''}}" class="form-control rounded-right">
                                        @if($errors->has('subject') && old('form_type') == "update_appraisal")
                                            <div class="error">{{ $errors->first('subject') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Mail Format</label>
                                    <textarea class="form-control" name="email_format" id="template_for_update_appraisal">{{(isset($dataArray['update_appraisal']['email_format'])) ? $dataArray['update_appraisal']['email_format']:''}}</textarea>
                                    @if($errors->has('email_format') && old('form_type') == "update_appraisal")
                                        <div class="error">{{ $errors->first('email_format') }}</div>
                                    @endif
                                </div>
                                <div class="card-footer pl-0">
                                    <input type="submit" value="Update Template" class="btn btn-primary btn-dark">
                                    <a href="{{url('users/email/update')}}"><button type="button" value="Cancel" class="btn btn-secondary btn-dark">Cancel</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="createTeamModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-small">
            <div class="modal-content">
                <div class="modal-header card-header bg-dark">
                    <h4 class="modal-title">Keyword List</h4>
                    <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity: 1;">&times;</button>
                </div>
                <div class="modal-body assign_to_team_records" id="modal-body">  
                    @include('pms.users.keywordListing')                                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
 </section>     
@endsection
@section('javascript')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    $('.viewKeyword').click(function(){
        $('#createTeamModal').modal();
    });
    $('.select2').select2();
    CKEDITOR.replace( 'template_for_add_user');
    CKEDITOR.replace( 'template_for_update_employment');
    CKEDITOR.replace( 'template_for_update_appraisal');
    CKEDITOR.replace( 'template_for_update_designation');
</script>
@endsection