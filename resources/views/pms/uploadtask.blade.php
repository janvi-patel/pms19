@extends('layouts.master')
@section('content')
@section('moduleName')
    Upload Task xlsx
@endsection
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Upload Task xlsx</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
@include('sweet::alert')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if (Session::has('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!!session('success')!!}</strong>
                </div>
                @endif    
                @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!!session('error')!!}</strong>
                </div>
                @endif 
                <div class="card">
                    <div class='card-body download_text'><a href="{{asset('doc/project_task.xlsx')}}"><i class="fa fa-file-excel" aria-hidden="true"></i><b> please download sample file.</b></a></div>
                </div>
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{url('/uploadtask')}}" name="upload_csv" id="upload_csv" enctype="multipart/form-data">
                    @csrf 
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-4">File Upload(Allow to upload .xlsx file)</label>
                                <div class="custom-file">
                                    <input type="file" name="csvfile" id="validatedCustomFile" class="custom-file-input" required/>
                                    <label class="custom-file-label" for="validatedCustomFile" id="uploadedFileName">Choose file...</label>
                                </div>
                            </div> 
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button name="submit" type="submit" class="btn btn-primary btn-dark">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            $('#uploadedFileName').text(fileName);
    });
});
</script>
@endsection