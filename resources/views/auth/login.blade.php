@extends('layouts.master') 

@section('moduleName')
    Login
@endsection

@section('content')
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
    <div class="auth-box bg-dark border-top border-secondary">
        <div class="login-box">
    <div class="login-logo">
    
        <div class="text-center p-t-20 p-b-20">
                        <span class="db"><img src="{{url('images/admin_images/logo_inverse.png')}}" width="350" alt="logo"></span>
                    </div>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">{{ __('Login') }}</p>

            @if(Session::has('employeeStatus'))
                <div class="alert alert-danger" role="alert">
                    {{Session::get('employeeStatus')}}
                </div>
                {{Session::forget('employeeStatus')}}
                {{Session::put('employeeStatus','')}}
            @endif

            <form action="{{ route('login') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input id="email" type="email" placeholder="Email Address" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    <div class="input-group-append">
                        <span class="fa fa-envelope input-group-text"></span> @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span> @endif
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                    <div class="input-group-append">
                        <span class="mdi mdi-key input-group-text"></span> @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                        <div class="checkbox icheck">
                            <label class="password_label">
                                <input type="checkbox"> Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                        <button type="submit" class="btn btn-orange btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
    </div>
        </div>
    </div>
</div>
<!-- /.login-box -->
@endsection