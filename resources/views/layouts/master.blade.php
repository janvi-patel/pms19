<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content={{csrf_token()}}>

        <title>PMS | @yield('moduleName')</title>
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <!--<link href="{{asset('dist/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">-->
        <link href="{{asset('dist/plugins/font-awesome/css/font-awesome.5.2.css')}}" rel="stylesheet">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('dist/plugins/select2/select2.min.css')}}">

        <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{asset('dist/plugins/iCheck/flat/blue.css')}}">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{{asset('dist/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
        <!-- Time Picker -->
        <link rel="stylesheet" href="{{asset('css/bootstrap-timepicker.min.css')}}">


        <link href="{{asset('css/icons/material-design-iconic-font/css/materialdesignicons.min.css')}}" rel="stylesheet">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{asset('dist/plugins/daterangepicker/daterangepicker-bs3.css')}}">
        <link rel="stylesheet" href="{{asset('css/morris.css')}}">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{{asset('dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        @yield('style')
        <script src="{{asset('js/sweetalert.min.js')}}"></script>
        <script type="text/javascript">      
            function getsiteurl()
            {
              var BASE_URL = {!! json_encode(url('/')) !!}

              return BASE_URL;
            }
       </script>
    </head>

    <body class="hold-transition sidebar-mini sidebar-collapse">
        <div id="custom_sidebar-overlay"></div>
        @guest @yield('content') @else
        <div class="wrapper" id="app">
            <!-- Header -->
            @include('layouts.header')
            <!-- Sidebar -->
            @include('layouts.sidebar') 
            <div class="content-wrapper">
                @yield('content')
                
            </div>
            <!-- Footer -->
            @include('layouts.footer')
        </div>
        <!-- ./wrapper -->
        @endguest 

        <script src="{{asset('js/jquery.min.js')}}"></script>
        {{--  <script type="text/javascript">
            if($('.swal-overlay')[0]) {
                setTimeout(function () {
                    location.reload(true);
                }, 2000);
            }
        </script>  --}}
        <script src="{{asset('js/jquery-ui.min.js')}}"></script>
        <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('js/bootstrap-timepicker.min.js')}}"></script>
        <!-- <script src="{{asset('js/moment.min.js')}}"></script>  -->
        <!-- <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script> -->
        <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('js/select2.full.min.js')}}"></script>
        <script src="{{asset('js/adminlte.min.js')}}"></script>

        <script src="{{asset('js/DataTables/datatables.min.js')}}"></script>
        <script src="{{asset('js/jquery.validate.min.js')}}"></script>

        <script src="{{asset('js/additional-methods.js')}}"></script>
        <script src="{{asset('js/common.js')}}"></script>
        @yield('javascript')
        <script type="text/javascript">
         $( document ).ready(function() {
            $('li.menu-open ul').css('display','block');
            $('li.menu-open ul ul').css('display','none');
            if($("li ul.third-sub-menu li").hasClass("menu-open")){
                $("li ul.third-sub-menu .menu-open ul").css('display','block');
            }
            if ($(window).width() <= 991){
                $('.sidebar-mini').removeClass('sidebar-collapse');
            } 

            if ($(window).width() > 991){
                $('.sidebar-mini').addClass('sidebar-collapse');
            }
            
            $(window).on('resize', function(){
                var win = $(this);
                if (win.width() <= 991) {
                    $('.sidebar-mini').removeClass('sidebar-collapse');
                }
                if (win.width() > 991) {
                    $('.sidebar-mini').addClass('sidebar-collapse');
                }
            });
            
         });
        </script>
</body>
</html>