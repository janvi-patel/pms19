<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/dashboard')}}" class="brand-link">
        <img src="{{url('/img/logo.png')}}" alt="PMS" class="brand-image">
    <span class="brand-text font-weight-light">PMS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->

        <nav class="mt-2 user-panel">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview {{ Request::is('viewprofile')|| Request::is('changepassword')|| Request::is('logout')? ' menu-open' : '' }}">
                    <a  href="javascript:void(0);" class="align-items-center line-h-0 nav-link {{ Request::is('viewprofile')|| Request::is('changepassword')|| Request::is('logout')? ' active' : '' }}">
                        <i class="username-panel" aria-hidden="true"><img src="{{url('/img/profile.png')}}" class="img-circle elevation-2" alt="User Image"></i>
                        <p>
                          <span>
                          {{auth()->user()->first_name!=null ? auth()->user()->first_name ." ".auth()->user()->last_name  : "Administrator"}}
                          </span>
                           <i class="right fa fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview" style="display:none">
                         <li class="nav-item has-treeview ">
                            <a href="{{url('/viewprofile')}}" class="nav-link {{ Request::is('viewprofile') ? 'active' : '' }}">
                                <i class="fas fa-user-circle"></i>
                                <p>View Profile</p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview ">
                            <a href="{{url('changepassword')}}" class="nav-link {{ Request::is('changepassword') ? 'active' : '' }}">
                                <i class="fas fa-key"></i>
                                <p>Change Password</p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview">
                            <a href="{{url('/logout')}}" class="nav-link {{ Request::is('logout') ? 'active' : '' }}">
                                <i class="fas fa-sign-out-alt"></i>
                                <p>Logout</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview {{ Request::is('dashboard')? ' menu-open' : '' }}">
                    <a href="{{url('dashboard')}}" class="nav-link {{ Request::is('dashboard')? 'active' : '' }}">
                        <i class="fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                    Auth::user()->hasRole(config('constant.admin_slug')) ||
                    Auth::user()->hasRole(config('constant.hr_slug'))||
                    Auth::user()->hasRole(config('constant.project_manager_slug')))
                <li class="nav-item has-treeview  {{ Request::is('users') ||Request::is('designation') ? 'menu-open' : '' }}">
                    <a href="javascript:void(0);" class="nav-link {{ Request::is('users') ||Request::is('designation')? 'active' : '' }}">
                      <i class="fa fa-users"></i>
                      <p>
                        User Management
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview" style="display:none">
                      <li class="nav-item">
                        <a href="{{url('users')}}" class="nav-link  {{ Request::is('users') ? 'active' : '' }}">
                          <i class="fas fa-user"></i>
                          <p>Users</p>
                        </a>
                      </li>
                      @if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')))
                      <li class="nav-item">
                        <a href="{{url('dynamicFields')}}" class="nav-link  {{ Request::is('dynamicFields') ? 'active' : '' }}">
                          <i class="fas fa-user"></i>
                          <p>User Module Fields</p>
                        </a>
                      </li>
                      @endif
                      @if(!Auth::user()->hasRole(config('constant.project_manager_slug')))
                      <li class="nav-item">
                        <a href="{{url('users/leave/upload/view')}}" class="nav-link {{ Request::is('users/leave/upload/view') ? 'active' : '' }}">
                          <i class="mdi mdi-note-outline"></i>
                          <p>Upload Leave</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{url('designation')}}" class="nav-link {{ Request::is('designation') ? 'active' : '' }}">
                          <i class="mdi mdi-note-outline"></i>
                          <p>Designations</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{url('appraisal')}}" class="nav-link {{ Request::is('appraisal') ? 'active' : '' }}">
                          <i class="mdi mdi-note-outline"></i>
                          <p>Appraisal</p>
                        </a>
                      </li>
                      @endif
                    </ul>
                </li>
                @endif
                <li class="nav-item has-treeview">
                    <a href="{{route('/pms/viewtimesheet')}}" class="nav-link {{ Request::is('pms/viewtimesheet') || Request::is('viewsummary') ? 'active' : '' }}">
                        <i class="fas fa-clock"></i>
                        <p>View Time Sheet</p>
                    </a>
                </li>
                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                    Auth::user()->hasRole(config('constant.admin_slug')) ||
                    Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                    Auth::user()->hasRole(config('constant.account_manager_slug')) ||
                    Auth::user()->hasRole(config('constant.team_leader_slug')) ||
                    Auth::user()->hasRole(config('constant.sbdm_slug')) ||
                    Auth::user()->hasRole('ba'))
                    <li class="nav-item has-treeview {{Request::is('lead*') ? 'menu-open' : '' }}">
                        <a href="javascript:void(0);" class="nav-link {{ Request::is('lead*') ? 'active' : '' }}">
                            <i class="mdi mdi-timer"></i>
                            <p>
                              Project Lead<i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display:none">
                          @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                          Auth::user()->hasRole(config('constant.admin_slug')) ||
                          Auth::user()->hasRole(config('constant.account_manager_slug')))
                          <li class="nav-item">
                              <a href="{{url('/lead/on_going')}}" class="nav-link {{ Request::is('lead/on_going') ? 'active' : '' }}">
                                  <i class="mdi mdi-av-timer mdi-18px"></i>
                                  <p>On Going Lead</p>
                              </a>
                          </li>
                          @endif
                          <li class="nav-item">
                              <a href="{{url('/lead')}}" class="nav-link {{ Request::is('lead') ? 'active' : '' }}">
                                  <i class="mdi mdi-folder-account mdi-18px"></i>
                                  <p>My Lead</p>
                              </a>
                          </li>
                          @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                          Auth::user()->hasRole(config('constant.admin_slug')))
                            <li class="nav-item">
                              <a href="{{url('/technology')}}" class="nav-link {{ Request::is('lead') ? 'active' : '' }}">
                                  <i class="mdi mdi-console mdi-18px"></i>
                                  <p>Project Technology</p>
                              </a>
                            </li>
                          @endif
                      </ul>
                    </li>
                @endif
                @if(!Auth::user()->hasRole(config('constant.hr_slug')) && !Auth::user()->hasRole(config('constant.account_manager_slug')))
                   <input type="hidden" name="hidden_superadmin_slug" value="{{Auth::user()->hasRole(config('constant.superadmin_slug'))}}">

                    <input type="hidden" name="hidden_admin_slug" value="{{Auth::user()->hasRole(config('constant.admin_slug'))}}">

                    <input type="hidden" name="hidden_project_manager_slug" value="{{Auth::user()->hasRole(config('constant.project_manager_slug'))}}">

                     <input type="hidden" name="hidden_constant_superadmin_slug" value="{{config('constant.superadmin_slug')}}">

                      <input type="hidden" name="hidden_constant_admin_slug" value="{{config('constant.admin_slug')}}">

                        <input type="hidden" name="hidden_constant_project_manager_slug" value="{{config('constant.project_manager_slug')}}">

                    @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                        Auth::user()->hasRole(config('constant.project_manager_slug')))

                        <li class="nav-item has-treeview {{ (Request::is('view/projects') || Request::is('view/crs')) || Request::is('uploadtask') ? 'menu-open' : '' }}">

                            <a href="javascript:void(0);" class="nav-link  {{ Request::is('view/projects') || Request::is('view/crs') || Request::is('uploadtask')? 'active' : '' }}">
                                <i class="fa fa-cubes"></i>
                                <p>
                                    Project Management
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>

                            <ul class="nav nav-treeview" style="display:none">
                                <li class="nav-item">
                                    <a href="{{url('/view/projects')}}" class="nav-link {{ Request::is('view/projects') ? 'active' : '' }}">
                                        <i class="fa fa-bars"></i>
                                        <p>My Projects</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('view/crs')}}" class="nav-link {{ Request::is('view/crs') ? 'active' : '' }}">
                                        <i class="fa fa-bars"></i>
                                        <p>My CR</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('view/mytask/list')}}" class="nav-link {{ Request::is('view/mytask/list') ? 'active' : '' }}">
                                        <i class="fas fa-bars"></i>
                                        <p>My Tasks</p>
                                    </a>
                                </li>
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                    Auth::user()->hasRole(config('constant.admin_slug')) ||
                                    Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                                    Auth::user()->hasRole(config('constant.team_leader_slug')))
                                    <li class="nav-item has-treeview">
                                        <a href="{{url('/uploadtask')}}" class="nav-link {{ Request::is('uploadtask') || Request::is('uploadtask') ? 'active' : '' }}">
                                            <i class="fa fa-tasks"></i>
                                            <p>Upload Task</p>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @else

                        <li class="nav-item has-treeview {{ Request::is('view/myproject') || Request::is('view/task/*') ||
                                    Request::is('view/task') || Request::is('view/mytask/list') || Request::is('view/task_entry/*') ||
                                    Request::is('uploadtask')? 'menu-open' : '' }}">

                            <a href="javascript:void(0);" class="nav-link
                               {{ Request::is('view/myproject') || Request::is('view/task/*') || Request::is('view/task') ||
                                Request::is('view/mytask/list')||Request::is('view/task_entry/*') || Request::is('uploadtask')? 'active' : '' }}">
                                <i class="fa fa-cubes"></i>
                                <p>
                                    Project Management
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>

                            <ul class="nav nav-treeview" style="display:none">
                                <li class="nav-item">
                                    <a href="{{url('view/myproject')}}" class="nav-link {{ Request::is('view/myproject') ? 'active' : '' }}">
                                        <i class="fas fa-user-shield"></i>
                                        <p>My Projects</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{url('view/mytask/list')}}" class="nav-link {{ Request::is('view/mytask/list') ? 'active' : '' }}">
                                        <i class="fas fa-user-shield"></i>
                                        <p>My Tasks</p>
                                    </a>
                                </li>
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                    Auth::user()->hasRole(config('constant.admin_slug')) ||
                                    Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                                    Auth::user()->hasRole(config('constant.team_leader_slug')))
                                    <li class="nav-item has-treeview">
                                        <a href="{{url('/uploadtask')}}" class="nav-link {{ Request::is('uploadtask') || Request::is('uploadtask') ? 'active' : '' }}">
                                            <i class="fa fa-tasks"></i>
                                            <p>Upload Task</p>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                @endif

                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                    Auth::user()->hasRole(config('constant.admin_slug')) ||
                    Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole('administration'))
                    <li class="nav-item has-treeview {{ Request::is('uploadcsv') || Request::is('holiday') || Request::is('upload/entries') || Request::is('uploadDocuments')? 'menu-open' : '' }}">
                    <a href="javascript:void(0);" class="nav-link {{ Request::is('uploadcsv') || Request::is('holiday')|| Request::is('upload/entries') || Request::is('uploadDocuments')? 'active' : '' }}">
                      <i class="mdi mdi-receipt"></i>
                      <p>
                         PMS Management
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview" style="display:none">
                        @if(!Auth::user()->hasRole('administration'))
                      <!-- <li class="nav-item">
                        <a href="{{url('/uploadcsv')}}" class="nav-link {{ Request::is('uploadcsv') ? 'active' : '' }}">
                          <i class="fas fa-money"></i>
                          <p>Upload Salary</p>
                        </a>
                      </li> -->
                      @endif
                      <li class="nav-item">
                      <a href="{{ url('upload/entries') }}" class="nav-link  {{ Request::is('upload/entries') ? 'active' : '' }}">
                          <i class="fas fa-hourglass-end"></i>
                          <p>Manage Time Sheet</p>
                        </a>
                      </li>
                      <li class="nav-item">
                      <a href="{{ route ('workfromhomeuserlist') }}" class="nav-link  {{ Request::is('workfromhomeuserlist') ? 'active' : '' }}">
                          <i class="fas fa-hourglass-end"></i>
                          <p>Work From Home User</p>
                        </a>
                      </li>
                      <li class="nav-item">
                      <a href="{{ url('check_entries') }}" class="nav-link  {{ Request::is('check_entries') ? 'active' : '' }}">
                          <i class="fas fa-hourglass-end"></i>
                          <p>Check Time Entry</p>
                        </a>
                      </li>
                      @if(!Auth::user()->hasRole('administration'))
                      <li class="nav-item">
                        <a href="{{url('/uploadProjectEntry')}}" class="nav-link {{ Request::is('uploadProjectEntry') ? 'active' : '' }}">
                          <i class="fas fa-money"></i>
                          <p>Upload Project Entry</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{url('uploadDocuments')}}" class="nav-link {{ Request::is('uploadDocuments') ? 'active' : '' }}">
                          <i class="mdi mdi-book-plus"></i>
                          <p>Upload Documents</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{url('/holiday')}}" class="nav-link {{ Request::is('holiday') ? 'active' : '' }}">
                          <i class="fa fa-bullhorn"></i>
                          <p>Holidays</p>
                        </a>
                      </li>
                      @endif
                    </ul>
                </li>
                @endif
                @if(!Auth::user()->hasRole(config('constant.superadmin_slug')))
                <li class="nav-item has-treeview {{ Request::is('leaverequests') || Request::is('compoffrequests') || Request::is('workfromhomeequests') || Request::is('earlyLeaveRequest')? 'menu-open' : '' }}">
                    <a href="javascript:void(0);" class="nav-link {{ Request::is('leaverequests') || Request::is('compoffrequests') || Request::is('workfromhomeequests')|| Request::is('earlyLeaveRequest') ? 'active' : '' }}">
                      <i class="mdi mdi-receipt"></i>
                      <p>
                        General
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview" style="display:none" >
                        <li class="nav-item">
                            <a href="{!! route('leaverequests') !!}" class="nav-link {{ Request::is('leaverequests') ? 'active' : '' }}">
                                <i class="mdi mdi-note-outline"></i>
                                <p>Leave Requests</p>
                            </a>
                        </li>
                        <li class="nav-item">
                          <a href="{!! route('compoffrequests') !!}" class="nav-link {{ Request::is('compoffrequests') ? 'active' : '' }}">
                            <i class="mdi mdi-note-outline"></i>
                            <p>Comp-off Requests</p>
                          </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('workfromhomeequests') !!}" class="nav-link {{ Request::is('workfromhomeequests') ? 'active' : '' }}">
                                <i class="mdi mdi-note-outline"></i>
                                <p>WFH Requests</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('earlyLeaveRequest') !!}" class="nav-link {{ Request::is('earlyLeaveRequest') ? 'active' : '' }}">
                                <i class="mdi mdi-note-outline"></i>
                                <p>Early Leave Requests</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
               @php $isteam = (new \App\Helpers\CommonHelper)->CheckReprtingPerson(auth()->user()->id);

                @endphp
                @if($isteam == 1)
                   <li class="nav-item has-treeview {{Request::is('teamleaves') || Request::is('teamcompoffs')
                    || Request::is('teamtask') || Request::is('teamviewtimesheet') || Request::is('teamuser') || Request::is('teamworkfromhome') || Request::is('teamEarlyLeave') ?'menu-open':''}}">
                    <a href="javascript:void(0);" class="nav-link {{Request::is('teamleaves') || Request::is('teamcompoffs')
                        || Request::is('teamtask') || Request::is('teamviewtimesheet') || Request::is('teamuser') ||  Request::is('teamworkfromhome') || Request::is('teamTaskList') || Request::is('teamEarlyLeave')  ?'active':''}}">
                      <i class="mdi mdi-receipt"></i>
                      <p>
                        My Team
                        <i class="right fa fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview" style="display:none">

                          <li class="nav-item">
                            <a href="{!! route('teamTaskList') !!}" class="nav-link {{ Request::is('teamTaskList') ? 'active' : '' }}">
                              <i class="mdi mdi-note-outline"></i>
                              <p>Team Tasks</p>
                            </a>
                          </li>

                        <li class="nav-item">
                          <a href="{!! route('teamLeaveRequests') !!}" class="nav-link {{ Request::is('teamleaves') ? 'active' : '' }}">
                            <i class="mdi mdi-note-outline"></i>
                            <p>Leave Requests</p>
                          </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('teamCompOff') !!}" class="nav-link {{ Request::is('teamcompoffs') ? 'active' : '' }}">
                              <i class="mdi mdi-note-outline"></i>
                              <p>Comp-off Requests</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('teamWorkFromHomeRequests') !!}" class="nav-link {{ Request::is('teamworkfromhome') ? 'active' : '' }}">
                              <i class="mdi mdi-note-outline"></i>
                              <p>WFH Requests</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{!! route('teamEarlyLeave') !!}" class="nav-link {{ Request::is('teamEarlyLeave') ? 'active' : '' }}">
                              <i class="mdi mdi-note-outline"></i>
                              <p>Early Leave Requests</p>
                            </a>
                        </li>



                            <li class="nav-item">
                                <a href="{!! route('teamUser') !!}" class="nav-link {{ Request::is('teamuser') ? 'active' : '' }}">
                                  <i class="mdi mdi-note-outline"></i>
                                  <p>User Listing</p>
                                </a>
                            </li>

                    </ul>
                   </li>
                @endif
                @if(!(Auth::user()->designation_id == 12))
                  <li class="nav-item has-treeview  {{Request::is('reports/actual-leave') || Request::is('reports/leave') || Request::is('reports/comp-offs')|| Request::is('reports/late-comer') || Request::is('reports/project_hours') ? 'menu-open' : '' }}">
                  <a href="javascript:void(0);" class="nav-link {{ Request::is('reports/actual-leave') ||Request::is('reports/leave') || Request::is('reports/comp-offs')|| Request::is('reports/late-comer')|| Request::is('reports/project_hours') ? 'active' : '' }}">
                    <i class="mdi mdi-receipt"></i>
                    <p>
                      Reports
                      <i class="right fa fa-angle-left"></i>
                    </p>
                  </a>
                  @if(!Auth::user()->hasRole('developer'))
                    <ul class="nav nav-treeview third-sub-menu">
                        <li class="nav-item has-treeview  {{Request::is('reports/actual-leave') || Request::is('reports/leave')? 'menu-open' : '' }}">
                            <a href="javascript:void(0);" class="nav-link {{ Request::is('reports/actual-leave') ||Request::is('reports/leave')? 'active' : '' }}">
                              <i class="mdi mdi-receipt"></i>
                              <p>
                               Leave Reports
                                <i class="right fa fa-angle-left"></i>
                              </p>
                            </a>
                            <ul class="nav nav-treeview" >
                              <!--<li class="nav-item"   >
                                    <a href="{{url('reports/actual-leave')}}" class="nav-link  {{ Request::is('reports/actual-leave') ? 'active' : '' }}">
                                      <i class="mdi mdi-receipt"></i>
                                      <p>Actual Leaves</p>
                                    </a>
                                </li>-->
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                Auth::user()->hasRole(config('constant.admin_slug')) ||
                                Auth::user()->hasRole(config('constant.project_manager_slug'))||
                                Auth::user()->hasRole(config('constant.hr_slug')) ||
                                Auth::user()->hasRole(config('constant.team_leader_slug'))||
                                Auth::user()->designation_id == 16  || Auth::user()->designation_id == 26)
                                <li class="nav-item">
                                    <a href="{{url('reports/leave')}}" class="nav-link  {{ Request::is('reports/leave') ? 'active' : '' }}">
                                      <i class="mdi mdi-receipt"></i>
                                      <p>Applied Leaves</p>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a href="{{url('reports/detail-leave')}}" class="nav-link  {{ Request::is('reports/detail-leave') ? 'active' : '' }}">
                                      <i class="mdi mdi-receipt"></i>
                                      <p>Balance/Used Leaves</p>
                                    </a>
                                 </li>
                                 @endif
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                    Auth::user()->hasRole(config('constant.admin_slug')) ||
                                    Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole('administration'))
                                    <li class="nav-item">
                                    <a href="{{url('reports/monthly-leave')}}" class="nav-link  {{ Request::is('reports/monthly-leave') ? 'active' : '' }}">
                                      <i class="mdi mdi-receipt"></i>
                                      <p>Monthly Leave Report</p>
                                    </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                  @endif
                    @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                        Auth::user()->hasRole(config('constant.project_manager_slug'))||
                        Auth::user()->hasRole(config('constant.hr_slug')) ||
                        Auth::user()->hasRole(config('constant.team_leader_slug'))||
                        Auth::user()->hasRole('developer') ||
                        Auth::user()->designation_id == 16 || Auth::user()->designation_id == 26
                        )
                    <ul class="nav nav-treeview  third-sub-menu">
                        <li class="nav-item has-treeview  {{ Request::is('reports/comp-offs')|| Request::is('reports/late-comer') || Request::is('reports/project_hours') ? 'menu-open' : '' }}">
                            <a href="javascript:void(0);" class="nav-link {{  Request::is('reports/comp-offs')|| Request::is('reports/late-comer')|| Request::is('reports/project_hours') ? 'active' : '' }}">
                              <i class="mdi mdi-receipt"></i>
                              <p>
                               Other Reports
                                <i class="right fa fa-angle-left"></i>
                              </p>
                            </a>
                            <ul class="nav nav-treeview" style="display:none" >
                                @if(!Auth::user()->hasRole('developer'))
                                 <li class="nav-item">
                                    <a href="{{url('reports/comp-offs')}}" class="nav-link {{ Request::is('reports/comp-offs') ? 'active' : '' }}">
                                      <i class="mdi mdi-receipt"></i>
                                      <p>Comp-Offs Report</p>
                                    </a>
                                 </li>
                                 <li class="nav-item">
                                    <a href="{{url('reports/late-comer')}}" class="nav-link  {{ Request::is('reports/late-comer') ? 'active' : '' }}">
                                      <i class="mdi mdi-receipt"></i>
                                      <p>Late Comer</p>
                                    </a>
                                 </li>
                                @endif
                                  @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                                        Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                                        Auth::user()->hasRole(config('constant.team_leader_slug')) ||
                                        Auth::user()->hasRole('developer') ||
                                        Auth::user()->department == 4 )
                                    <li class="nav-item">
                                        <a href="{{url('reports/projects')}}" class="nav-link  {{ Request::is('reports/projects') ? 'active' : '' }}">
                                          <i class="mdi mdi-receipt"></i>
                                          <p>Projects</p>
                                        </a>
                                    </li>
                                @endif
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                                        Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                                        Auth::user()->hasRole('developer') ||
                                        Auth::user()->hasRole(config('constant.team_leader_slug')))
                                    <li class="nav-item">
                                        <a href="{{url('reports/project_hours')}}" class="nav-link  {{ Request::is('reports/project_hours') ? 'active' : '' }}">
                                          <i class="mdi mdi-receipt"></i>
                                          <p>Projects Hours</p>
                                        </a>
                                    </li>

                                @endif
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                                        Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                                        Auth::user()->hasRole(config('constant.team_leader_slug')))
                                    <li class="nav-item">
                                        <a href="{{url('reports/project_calculation')}}" class="nav-link  {{ Request::is('reports/project_calculation') ? 'active' : '' }}">
                                          <i class="mdi mdi-receipt"></i>
                                          <p>Projects Calculation</p>
                                        </a>
                                    </li>

                                @endif
                                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                                        Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                                        Auth::user()->hasRole(config('constant.team_leader_slug')))
                                    <li class="nav-item">
                                        <a href="{{url('reports/billable_report')}}" class="nav-link  {{ Request::is('reports/billable_report') ? 'active' : '' }}">
                                          <i class="mdi mdi-receipt"></i>
                                          <p>Billable Report</p>
                                        </a>
                                    </li>

                                @endif
                            </ul>
                        </li>
                    </ul>
                    @endif
                  </li>
                @endif
                @if(Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                        Auth::user()->hasRole(config('constant.admin_slug')))
                    <li class="nav-item has-treeview {{ Request::is('setting')? ' menu-open' : '' }}">
                        <a href="{{url('/view/setting')}}" class="nav-link {{ Request::is('setting')? 'active' : '' }}">
                            <i class="fas fa-cogs"></i>
                            <p>Settings</p>
                        </a>
                    </li>
                @endif
                <li class="nav-item has-treeview {{ Request::is('documents')? ' menu-open' : '' }}">
                  <a href="{{url('/documents')}}" class="nav-link {{ Request::is('documents') ? 'active' : '' }}">
                    <i class="mdi mdi-file-pdf"></i>
                    <p>Documents</p>
                  </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

