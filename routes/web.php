<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


 Route::get('/', function () {
        return view('home');
    })->middleware('auth');

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['middleware'=>['auth']],function(){

    //dashboard and home routes
    Route::get('/', 'DashboardController@versionone')->name('home');
    Route::get('/dashboard', 'DashboardController@versionone');
    Route::get('/getBreakTimeUser', 'DashboardController@getBreakTimeUser');
    Route::get('/dashboard/empTotalWorkDays', 'DashboardController@empTotalWorkDays');

    Route::get('/dashboard/v2', 'DashboardController@versiontwo')->name('v2');
    Route::get('/dashboard/v3', 'DashboardController@versionthree')->name('v3');


    //user profile action route
    Route::match(['get','post'],'/viewprofile','DashboardController@viewProfile');
    Route::match(['get','post'],'/changepassword','DashboardController@changePassword');

    // Create User
    Route::match(['get','post'],'/users/create','UserController@create');
    Route::post('/users/store','UserController@store');
    Route::get('/users/updateuserajax','UserController@updateCheckedUser');
    Route::get('/users/updatereportingajax','UserController@updateReporting');
    Route::get('/users/create_employee_code','UserController@createEmployeeCode');
    Route::match(['get','post'],'users/{serach_by_company?}','UserController@index');
    Route::get('/users/edit/{id}','UserController@edit');
    Route::post('/users/update/{id}','UserController@update');
    Route::get('users/email/update','UserController@EmailTemplate');
    Route::post('email/update','UserController@updateEmail');
    Route::post('/users/update/password/{id}','UserController@updatePassword');
    Route::delete('/users/delete','UserController@deleteUser');
    Route::post('/users/getreportingtolist','UserController@getReportingToAjax');
    Route::match(['get','post'],'/teamuser','UserController@teamUser')->name('teamUser');
    Route::get('/get_role/{designation_id}','UserController@getRoleDesignation');
     Route::get('/importuser_modal','UserController@ImportuserModal');
     Route::post('/show_user_details','UserController@showUserDetails');
     Route::post('/store_import_data','UserController@StoreImportUserData');



    // designation crud
    Route::get('/designation','DesignationController@index');
    Route::get('/designation/edit/{id}','DesignationController@edit');
    Route::post('/designation/update/{id}','DesignationController@update');
    Route::delete('/designation/destroy','DesignationController@destroy');
    Route::match(['get','post'],'/designation/create','DesignationController@create');
    Route::post('/designation/store','DesignationController@store');


    // User Leave
    Route::get('/users/leave/upload/view','UserController@LeaveUploadView')->name('userLeaveUpload');
    Route::post('upload/user_leave_file','UserController@UserLeaveUpload');

    // Appraisal
    Route::get('/appraisal','AppraisalController@index');
    Route::get('/get_designation/{userId}','AppraisalController@getDesignation');
    Route::post('update_appraisal','AppraisalController@updateAppraisal');
    Route::post('/upload/appraisal_file','AppraisalController@uploadAppraisal');
    //Leave route
    Route::match(['get','post'],'/leaverequests','LeaveController@index')->name('leaverequests');
    Route::match(['get','post'],'/leaves/add','LeaveController@create')->name('leaveAdd');
    Route::post('/leaves/store','LeaveController@store')->name('leaveStore');
    Route::get('/leaves/approve/{id}','LeaveController@edit')->name('leaveApprove');
    Route::post('/leaves/update/{id}','LeaveController@update')->name('leaveUpdate');
    Route::match(['get','post'],'/teamleaves','LeaveController@teamLeaveRequests')->name('teamLeaveRequests');
    Route::delete('/leave_entry/delete','LeaveController@leaveDalete');
    Route::get('/leaves/updatecheckedleave','LeaveController@updateCheckedLeaveAjax');

    //work form home
    Route::match(['get','post'],'/workfromhomeequests','WorkFromHomeController@index')->name('workfromhomeequests');
    Route::match(['get','post'],'/workfromhome/add','WorkFromHomeController@create')->name('workFromHomeAdd');
    Route::post('/workfromhome/store','WorkFromHomeController@store')->name('workFromHomeStore');
    Route::match(['get','post'],'/teamworkfromhome','WorkFromHomeController@teamWorkFromHomeRequests')->name('teamWorkFromHomeRequests');
    Route::get('/workfromhome/approve/{id}','WorkFromHomeController@edit')->name('workFromHomeApprove');
    Route::post('/workfromhome/update/{id}','WorkFromHomeController@update')->name('workFromHomeUpdate');
    Route::get('/workfromhomeuser/show','WorkFromHomeController@workFromHomeUserList')->name('workfromhomeuserlist');
    Route::delete('/workfromhome/delete','WorkFromHomeController@workFromHomeDelete');
    Route::get('/workfromhome/updatecheckedworkfromhome','WorkFromHomeController@updateCheckedWorkFromHomeAjax');


    //early leave
    Route::match(['get','post'],'/earlyLeaveRequest','EarlyLeaveController@index')->name('earlyLeaveRequest');
    Route::match(['get','post'],'/earlyLeave/add','EarlyLeaveController@create')->name('earlyLeaveAdd');
    Route::post('/earlyLeave/store','EarlyLeaveController@store')->name('earlyLeaveStore');
    Route::match(['get','post'],'/teamEarlyLeave','EarlyLeaveController@teamEarlyLeaveRequests')->name('teamEarlyLeave');
    Route::get('/earlyLeave/approve/{id}','EarlyLeaveController@edit')->name('earlyLeavepprove');
    Route::post('/earlyLeave/update/{id}','EarlyLeaveController@update')->name('earlyLeaveUpdate');
    Route::delete('/earlyLeave/delete','EarlyLeaveController@earlyLeaveDelete');
    Route::get('/earlyLeave/updateCheckedEarlyLeave','EarlyLeaveController@updateCheckedEarlyLeaveAjax');
    //Comp-off route
    Route::match(['get','post'],'/compoffrequests','CompOffController@index')->name('compoffrequests');
    Route::match(['get','post'],'/compoffs/add','CompOffController@create')->name('compoffsAdd');
    Route::post('/compoffs/store','CompOffController@store')->name('compOffStore');
    Route::delete('/compoffs/delete','CompOffController@destroy');
    Route::match(['get','post'],'/teamcompoffs','CompOffController@teamCompoffRequests')->name('teamCompOff');
    Route::match(['get','post'],'/compoffs/markedas/{id}','CompOffController@editMarkedAs')->name('editMarkedAs');
    Route::match(['get','post'],'/compoffs/updateMarkedAs/{id}','CompOffController@updateMarkedAs')->name('updateMarkedAs');
    Route::get('/compoffs/approve/{id}','CompOffController@edit')->name('compOffApprove');
    Route::post('/compoffs/update/{id}','CompOffController@update')->name('compOffUpdate');
    Route::get('/compoffs/updatecheckedcompoff','CompOffController@updateCheckedCompoffAjax');

    //TimeSheet Route
    Route::match(['get','post'],'/pms/viewtimesheet','TimeSheetController@viewTimesheet')->name('/pms/viewtimesheet');
    Route::match(['get','post'],'/viewsummary','TimeSheetController@viewSummary');
    Route::post('/update/time/entry','TimeSheetController@updateEntry');
    Route::post('/add/auto/leave','TimeSheetController@addAutoLeave');
    Route::match(['get','post'],'/pms/gettimeentry','TimeSheetController@getTimeEntry');
    Route::post('/pms/updatetimeentry','TimeSheetController@updateTimeEntry');
    Route::get('/punch_data','TimeSheetController@getPunchData');
    Route::get('/add_punch_data','TimeSheetController@addPunchData');
    Route::get('/get_old_punch_data','TimeSheetController@getOldPunchData');
    Route::get('/get_punch_data','TimeSheetController@getUserPunchData');
    Route::get('/get_punch_data_today','TimeSheetController@getUserPunchDataToday');
    Route::get('/validate_punch_data','TimeSheetController@validateUserPunchData');
    Route::delete('/delete/punch_data','TimeSheetController@deletePunchData');
    Route::match(['get','post'],'/detailSummary/{attendance_id}','TimeSheetController@DetailTimeEntry');
    Route::match(['get','post'],'/upload/old/entries','TimeSheetController@oldUploadedEntries');

    Route::delete('/delete/time_entries','TimeSheetController@timeEntryDalete');

    //upload Task sheet
    Route::match(['get','post'],'/uploadtask','UploadTaskController@uploadtask');

    //pms route
    Route::match(['get','post'],'/uploadcsv','UploadcsvController@uploadcsv');
    Route::get('/old/salary/logs','UploadcsvController@oldSalaryLogs');
    Route::match(['get','post'],'/uploadProjectEntry','UploadProjectEntryController@uploadProjectEntry');
    Route::match(['get','post'],'/upload/entries','TimeSheetController@upload');
    Route::post('/upload/time_entries_using_cron','TimeSheetController@timeEntriesUsingCron');
    Route::match(['get','post'],'/check_entries','CheckTimeEntryController@upload');
    Route::delete('/delete/old/entries','TimeSheetController@oldEntriesDestroy');
    Route::match(['get','post'],'/admin/viewtimesheet','TimeSheetController@viewTimesheet')->name('/admin/viewtimesheet');
    Route::match(['get','post'],'/admin/viewsummary','TimeSheetController@viewSummary');
    Route::get('/documents','UploadDocumentsController@index');
    Route::get('/uploadDocuments','UploadDocumentsController@uploadDocuments');
    Route::get('/docs_modal','UploadDocumentsController@docsModal');
    Route::get('/docs_name/edit/{id}','UploadDocumentsController@editDocs');
    Route::post('/upload/documents','UploadDocumentsController@uploadDocs');
    Route::post('/docs_name/update','UploadDocumentsController@updateDocs');
    Route::delete('/documents/delete','UploadDocumentsController@deleteDocs');
    // user module dynamics
    Route::get('/dynamicFields','UserController@dynamicFields')->name('dynamicFields');
    Route::post('/dynamicFields/store','UserController@dynamicFieldsStore')->name('dynamicFieldsStore');
    // Holiday Route
    Route::get('/holiday','HolidayController@index')->name('holiday');
    Route::post('/save/holiday','HolidayController@store')->name('holidaySave');
    Route::post('/get/holiday/month_wise','HolidayController@getMonthWiseHoliday');
    Route::post('/update/holiday','HolidayController@update');
    Route::delete('/delete/holiday','HolidayController@destroy');

    //Reports Routes
    Route::get('/reports/leave','ReportController@leaveReport')->name('reports/leave');
    Route::get('/reports/actual-leave','ReportController@actualLeaveReport')->name('reports/actual-leave');
    Route::get('/reports/detail-leave','ReportController@detailLeaveReport')->name('reports/detail-leave');
    Route::get('/reports/updateLeave','ReportController@updateLeave');
    Route::get('/reports/comp-offs','ReportController@compOffsReport')->name('reports/comp-offs');
    Route::get('/reports/late-comer','ReportController@lateComerReport')->name('reports/late-comer');
    Route::get('/reports/projects','ReportController@projectReport')->name('reports/projects');
    Route::get('/reports/get_task_details','ReportController@getTaskDetails');
    Route::get('/reports/project_calculation','ReportController@projectCalculation');
    Route::get('/reports/project_hours','ReportController@projectHours');
    Route::get('/reports/monthly-leave','ReportController@monthlyLeaveReport')->name('reports/monthly-leave');
    Route::get('/reports/monthly-leave-sync','ReportController@syncLeaveBalances')->name('monthly-leave-sync');
    Route::get('/reports/billable_report','ReportController@billableReport');

    // Project
    Route::get('/view/projects/{company?}','ProjectController@index');
    Route::get('/projects/{company?}','ProjectController@index');
    Route::get('/project/assign_to_team','ProjectController@assignToTeam');
    Route::get('/project/remove_team_member','ProjectController@removeTeamMember');
    Route::post('/project/save_team','ProjectController@saveTeam');
    Route::get('/view/myproject','ProjectController@myProjects');
    Route::get('get/project/details','ProjectController@getProjectDetail');
    Route::get('/project/get_project_cr_details/{projectId}','ProjectController@getCrInfo');
    Route::get('/project/project_info/{projectId}','ProjectController@viewCrInfo');
    Route::get('get/project/{id}','ProjectController@getTaskDetails');
    Route::post('save/project/entry','ProjectEntryController@saveTaskEntry');
    Route::get('view/project_entry/{projectId}/{userId}','ProjectEntryController@viewTaskEntry');
    Route::get('view/user_project_entry/{projectId}/{userId}','ProjectEntryController@viewProjectEntry');
    Route::post('update/project_entry/entry','ProjectEntryController@updateTaskEntry');
    Route::get('edit/project_entry','ProjectEntryController@editTaskEntry');
    Route::post('update/project_entry','ProjectEntryController@updateTaskEntry');
    Route::delete('project_entry/multiple/delete','ProjectEntryController@deleteMultipleTaskEntry');
    Route::delete('project_entry/delete','ProjectEntryController@deleteTaskEntry');
    Route::get('remainingProjectEntry','ProjectEntryController@remainingProEntry');
    Route::get('view/project_task_entry/{taskId}','ProjectEntryController@viewProjectTaskEntry');
    Route::get('view/project_task_entry/old_entry/{projectId}','ProjectEntryController@viewProjectOldTaskEntry');
    Route::get('view/get_task_entry_details/{taskId}','ProjectEntryController@viewProjectTaskEntryModal');
    Route::get('/view/project/notes/{id}','ProjectController@viewProjectNotes');
    Route::get('/add/addProjectNoteModal','ProjectController@addProjectNoteModal');
    Route::post('add/addProjectNotes','ProjectController@storeProjectNotes');
    Route::post('edit/editProjectNotes','ProjectController@updateProjectNote');
    Route::get('/edit/editProjectNoteModal','ProjectController@editProjectNoteModal');
    Route::delete('/project_note/delete','ProjectController@deleteNotes');

    //Upload Project Task
    Route::get('move/moveProjectTask','ProjectTaskController@moveProjectTask');
    Route::post('/move/task/entry','ProjectTaskController@moveTaskEntry');
    Route::get('view/task/list/{id}','ProjectTaskController@getTaskList');
    Route::delete('project_task/delete','ProjectTaskController@deleteProjectTask');
    Route::get('edit/editProjectTaskModal','ProjectTaskController@editProjectTaskModal');
    Route::post('update/updateProjectTask','ProjectTaskController@updateProjectTaskModal');
    Route::get('add/addProjectTaskModal','ProjectTaskController@addProjectTaskModal');
    Route::post('add/addProjectTask','ProjectTaskController@storeProjectTask');
    Route::get('/view/mytask/list','ProjectTaskController@myTaskList');
    Route::post('/getChangesProjectCr','ProjectTaskController@getChangesProjectCr');
    Route::get('/view/teamTask/list','ProjectTaskController@teamTaskList')->name('teamTaskList');

    Route::get('/get/project/task_list/{id}/{date}','ProjectController@getProjectTask');
    Route::get('/get/project/new_task_list/{id}/{taskid}/{date}','ProjectController@newGetProjectTask');

    // Task Time Entry
    Route::get('view/task_entry/{taskId}','TaskEntryController@viewTaskEntry');
    Route::delete('task_entry/delete','TaskEntryController@deleteTaskEntry');
    Route::post('update/task/entry','TaskEntryController@updateTaskEntry');
    Route::delete('task_entry/multiple/delete','TaskEntryController@deleteMultipleTaskEntry');
    Route::get('check/time_entry','ProjectEntryController@checkTimeEntry');

    // CR
    Route::get('/view/crs/{company?}','CrController@index');

    //Setting
    Route::get('/view/setting','SettingController@index')->name('setting');
    Route::post('/setting/taskEntryDayUpdate','SettingController@TaskEntryDayUpdate');
    Route::post('/setting/lateComerTimeUpdate','SettingController@LateComeTimeUpdate');
    Route::post('/setting/workingHourUpdate','SettingController@workingHourUpdate');
    Route::post('/setting/probationTimeUpdate','SettingController@probationTimeUpdate');
    Route::post('/setting/loggedHourUpdate','SettingController@loggedHourUpdate');
    Route::post('/setting/taskOperationDaysUpdate','SettingController@taskOperationDaysUpdate');
    Route::post('/setting/gratuityUpdate','SettingController@gratuityUpdate');
    Route::post('/setting/professionalTaxUpdate','SettingController@professionalTaxUpdate');
    Route::post('/setting/houseRantUpdate','SettingController@houseRantUpdate');
    Route::post('/setting/BasicSalaryUpdate','SettingController@BasicSalaryUpdate');
    Route::post('/setting/performance_allowanceUpdate','SettingController@performance_allowanceUpdate');
    Route::post('/setting/other_allowncesUpdate','SettingController@other_allowncesUpdate');
    Route::post('/setting/startTimeforFirstHalf','SettingController@startTimeforFirstHalf');
    Route::post('/setting/startTimeforSecondtHalf','SettingController@startTimeforSecondtHalf');
    Route::post('/setting/startTimeforfullday','SettingController@startTimeforfullday');
    Route::post('/setting/bonus_applicable_percentage','SettingController@bonus_applicable_percentage');
    Route::post('/setting/cr_select_time_restriction','SettingController@crSelectTimeRestriction');
    Route::post('/setting/email_for_server','SettingController@emailForServer');


    //Lead
    Route::get('/lead','ProjectLeadController@index')->name('lead');
    Route::get('/lead/on_going','ProjectLeadController@onGoingLead')->name('onGoing');
    Route::get('/lead/create','ProjectLeadController@create');
    Route::get('/chat/listing/','ProjectLeadController@chatListing');
    Route::get('/lead/edit/{leadId}','ProjectLeadController@edit');
    Route::get('/lead_attachment/get_file_details/{leadId}','ProjectLeadController@getLeadFiles');
    Route::get('/lead_attachment/add_file_details/{leadId}','ProjectLeadController@addLeadFiles');
    Route::get('/lead_estimation/get_file_details/{leadId}','ProjectLeadController@getEstimationFiles');
    Route::get('/lead_estimation/add_file_details/{leadId}','ProjectLeadController@addEstimationFiles');
    Route::get('/lead_chat/get_file_details/{leadId}','ProjectLeadController@getChatFiles');
    Route::get('/lead_chat/add_file_details/{leadId}','ProjectLeadController@addChatFiles');
    Route::get('/lead/details/{leadId}','ProjectLeadController@getLeadDetails');
    Route::get('/get_tl_list/{id}','ProjectLeadController@getTlList');
    Route::get('/get_pm_list/{id}','ProjectLeadController@getPmList');
    Route::get('/chat/mark_as_read/{ChatId}','ProjectLeadController@markAsRead');
    Route::get('/lead/update/reviewer_end_date','ProjectLeadController@updateReviewerEndDate');
    Route::get('/lead/update/team_leader_by_pm','ProjectLeadController@updateTeamLeaderByPm');
    Route::get('/lead/estimation_docs/permission_to_bde','ProjectLeadController@updateDocsPermission');
    Route::get('/getTechnology','ProjectLeadController@getTechnology');
    Route::post('/lead/update','ProjectLeadController@update');
    Route::post('send/chat','ProjectLeadController@storeChats');
    Route::post('/addLeadDate','ProjectLeadController@store');
    Route::post('edit/chat','ProjectLeadController@editChat');
    Route::post('/lead/update/docs','ProjectLeadController@updateDocs');
    Route::post('/lead/update/estimation/docs','ProjectLeadController@updateEstimationDocs');
    Route::delete('/docs/delete','ProjectLeadController@deleteDocs');
    Route::delete('/lead/delete','ProjectLeadController@deleteLeads');
    Route::delete('/chat/delete','ProjectLeadController@deleteChat');
    Route::delete('/docs/estimation/delete','ProjectLeadController@deleteEstimationDocs');
     Route::get('/lead/updatecheckedleads','ProjectLeadController@updateCheckedLeadsAjax');

    //Technology
    Route::get('/technology','ProjectLeadController@listTech')->name('technology');
    Route::get('/technology/edit/{id}','ProjectLeadController@editTech');
    Route::post('/technology/add','ProjectLeadController@addTech');
    Route::post('/technology/update','ProjectLeadController@updateTech');
    Route::delete('/technology/delete','ProjectLeadController@deleteTech');
});
