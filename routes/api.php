<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST,GET,OPTIONS,HEAD,PUT,PATCH,DELETE");
header("Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization");

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
    
});*/
Route::get('/users/{id?}','Api\ApiController@getUsers');
Route::get('/general_settings/{company_id}','Api\ApiController@getGeneralSettings');
Route::get('/reports/monthly-leave/{month?}/{year?}','Api\ApiController@monthlyLeaveReport');
Route::get('/attendnce/{month?}/{year?}','Api\ApiController@attendnce');
Route::get('getcompany/{company_id?}','Api\ApiController@getCompanyDetails');
Route::get('/getActivaUser','Api\ApiController@getActiveUser');
Route::get('/project_calculation','Api\ApiController@projectCalculation');

 /*Route::get('/users/{id?}', 'Api\ApiController@getUsers')->name('api.get.user');*/

