import json
# static response
response = {"status": 1, "message": "Success", "data": {
    "user": {
        "name": "Pankit",
        "account": "34jkdfi5nww",
        "login_id": "Pankit@123"
    },
    "captcha": {"code": 168989, "expires_in": 60}
}}


# declared function
def provide_resp():
    print(json.dumps(response))


# calling
provide_resp()
