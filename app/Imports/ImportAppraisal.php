<?php

namespace App\Imports;

use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\Jobs\MailJob;
use App\Designation;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Session;
use App\EmailTemplate;
use jeremykenedy\LaravelRoles\Models\Role;


class ImportAppraisal implements ToCollection, WithStartRow
{
    public function startRow(): int {
        return 2;
    }
    
    
    public function collection(Collection $rows)
    {
        set_time_limit(0);
        $sortArr = [];
        $success = 0;
        $uploadedCompanyId = Auth::user()->company_id;
        $designation = new Designation;
        $designation_list_mix = $designation->whereNull('deleted_at')->get()->pluck('designation_name','id')->toArray();
        $designationList = array_map('strtolower', $designation_list_mix);
        $userObj = new User();
        $inValidRecord = array();
        $data = array();
        foreach ($rows as $key => $row) {
            if($row[0] != '' && $row[1] != '' && $row[3] != '' && $row[4] != ''
            && is_numeric($row[3]) && is_numeric($row[4]) && $row[3]>0 && $row[4]>0){
                $sortArr[$key]['empcode'] = $row[0];
                $sortArr[$key]['company'] = $row[1];
                $sortArr[$key]['designation'] = $row[2];
                $sortArr[$key]['appraisal_amount'] = $row[3];
                $sortArr[$key]['new_ctc'] = $row[4];
            }
            else{
                if($row[0] != '' || $row[1] != '' || $row[2] != '' || $row[3] != '' || $row[4] != '')
                {
                    $inValidRecord[$key]['empcode'] = $row[0];
                    $inValidRecord[$key]['company'] = $row[1];
                    $inValidRecord[$key]['designation'] = $row[2];
                    $inValidRecord[$key]['appraisal_amount'] = $row[3];
                    $inValidRecord[$key]['new_ctc'] = $row[4];
                }
            }
        }
        if(count($sortArr) > 0){
            foreach($sortArr as $key => $val){
                $company = $val['company'];
                $companyId = 2;
                if($company){
                    $companyArray = config('constant.company');
                    foreach($companyArray AS $cmpName => $companyVal){
                        if($cmpName == lcfirst($company)){
                            $companyId = $companyVal;
                        }
                    }
                }

                if($companyId){
                     $companyObj = Company::find($companyId);
                     $empcode =  str_replace($companyObj->emp_short_name,'',$val['empcode']);
                }
                $getUserId = $userObj->getUserDataByEmpCode($empcode,$uploadedCompanyId);
                if($getUserId!=null){
                    $userDetails = $userObj->getUserDetails($getUserId['id']);
                    $old_designation = $userDetails['designation_id'];
                    if($val['designation'] == '')
                    {
                        $this->sendMail($userDetails['id'],$val['appraisal_amount'],$val['new_ctc'],$designationList[$old_designation]);
                        $success++;
                    }
                    else if(in_array(strtolower($val['designation']),$designationList))
                    {
                        $new_designation = array_search(strtolower($val['designation']),$designationList);
                        if($old_designation != $new_designation)
                        {
                            $updateUser = User::find($getUserId['id']);
                            $updateUser->designation_id = $new_designation;
                            $updateUser->save();
                            $role = Role::where('id', '=', $userDetails['roleuser']['id'])->first();
                            $updateUser->detachRole($role);
                            $role_details = Designation::find($new_designation);
                            $role = Role::where('id', '=', $role_details->role_id)->first();
                            $updateUser->attachRole($role);
                        }
                        $this->sendMail($userDetails['id'],$val['appraisal_amount'],$val['new_ctc'],$val['designation']);
                        $success++;
                    }
                    else{
                        $inValidRecord[$key]['empcode'] = $val['empcode'];
                        $inValidRecord[$key]['company'] = $val['company'];
                        $inValidRecord[$key]['designation'] = $val['designation'];
                        $inValidRecord[$key]['appraisal_amount'] = $val['appraisal_amount'];
                        $inValidRecord[$key]['new_ctc'] = $val['new_ctc'];
                    }
                }
                else{
                    $inValidRecord[$key]['empcode'] = $val['empcode'];
                    $inValidRecord[$key]['company'] = $val['company'];
                    $inValidRecord[$key]['designation'] = $val['designation'];
                    $inValidRecord[$key]['appraisal_amount'] = $val['appraisal_amount'];
                    $inValidRecord[$key]['new_ctc'] = $val['new_ctc'];
                }
            }
        }
        $data['success']=$success;
        $data['skipped']=$inValidRecord;
        Session::push('AppraisalImportError',$data);
    }

    public function sendMail($userId,$amt,$ctc,$designation_name)
    {
        $userObj = new User();
        $userDetails = $userObj->getUserDetails($userId);
        $company     = new Company();
        $userCompanyMailData  = $company->getCompanyViseMailData($userDetails['company_id'])[0];
        $data['regards'] = $userCompanyMailData['company_name'];
        $from['email'] = $userCompanyMailData['from_address'];
        $from['name']  = $userCompanyMailData['company_name'];
        $mailTemplate = new EmailTemplate();
        $to = $userDetails['email'];
        // $to = "trushaptridhyatech@gmail.com";
        $cc = "";
        $template = 'emails.employee_appraisal_changed';
        $mail_subject = '';
        $data['full_name'] = $userDetails['first_name'].' '.$userDetails['last_name'];
        $data['first_name'] = $userDetails['first_name'];
        $subject = 'Employee Appraisal';
        $module = 'update_appraisal';
        $mailTemp = $mailTemplate->emailTemplate($userDetails['company_id'],$module);
        $data['temp'] = "";
        $data['from_date'] = '';
        $data['to_date'] = '';
        $data['designation'] = $designation_name;
        $data['new_ctc'] = $ctc;
        $data['appraisal_amount'] = $amt;
        if($mailTemp != null)
        {
            $keyword = [
                'first_name'        => $data['first_name'],
                'full_name'         => $data['full_name'],
                'company_name'      => $data['regards'],
                'new_ctc'           => $data['new_ctc'],
                'designation'       => $data['designation'],
                'from_date'         => $data['from_date'],
                'to_date'           => $data['to_date'],
                'appraisal_amount'  => $data['appraisal_amount']
            ];
            $subject = $mailTemp['subject'];
            $data['temp'] = $mailTemplate->replaceKeyword($mailTemp['email_format'],$keyword);
            $cc = $mailTemplate->sendToCC($userDetails['company_id'],$mailTemp['send_cc'],$userDetails['reporting_to']);
        }
        $subject = $subject != ''?$subject:$mail_subject;
        MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []);
    }
}
