<?php

namespace App\Imports;

use App\Project;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use App\MiscBeanchTask;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Company;
use Illuminate\Support\Facades\Input;
use App\Designation;
use App\MiscBeanchTaskEntry;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\ProjectEntry;
use App\ProjectTasks;
use App\Http\Controllers\UserController;
class UserDataImport implements ToCollection, WithStartRow
{
    public function startRow(): int {return 1; }
    
    public function collection(Collection $rows)
    {  
        $UserArr = [];
        $user_controller = new UserController;
        foreach ($rows  as $key => $row) {
          if($key > 0){
            if(empty($row[0])){
                  break;
              }
              if(!isset($row[9]))
              {
                alert()->error('Please select appropriate file.')->persistent('close')->autoclose("3600");
                  break;
              }
             $UserDataobj = User::with('designation', 'reportingto', 'company')->where('employee_id',str_replace($row[19],"",$row[6]))->select('id')->first();
              $companyId = Company::where('emp_short_name',$row[19])->select('id')->first();

              if(empty($UserDataobj)){
                $UserDataobj = new User();
              }
              
              $getEmployeeId = User::leftjoin('companies','users.company_id','companies.id')->select('users.employee_id','companies.id as company_id')->where('users.employee_id',str_replace($row[19],"",$row[6]))->first();
              if(isset($getEmployeeId) && isset($getEmployeeId->employee_id)){

                $UserDataobj->employee_id = str_replace($row[19],"",$row[6]);
              }
              else
              {
                $employeeCode = $user_controller->createEmployeeCodeNew($companyId->id);
                $UserDataobj->employee_id = $user_controller->getNumFromEmployeeCode($employeeCode,$companyId->id);
              }  
           
              if(empty($row[6])) 
              {
                  $checkEmail = User::where('email',$row[2])->where('company_id',$companyId->id)->count();
              }
              else{
                $checkEmail = User::where('email',$row[2])->where('employee_id','!=',str_replace($row[19],"",$row[6]))->where('company_id',$companyId->id)->count();
              }
              if($checkEmail > 0)
              {
                alert()->error('Email already in use.')->persistent('close')->autoclose("3600");
                break;
              }
              $getdesignationId = Designation::where('designation_name',$row[7])->select('id')->first();
                     
              if(isset($getdesignationId) && isset($getdesignationId->id)){

                $UserDataobj->designation_id = $getdesignationId->id;
              }
              else
              {
                $UserDataobj->designation_id = '';  
              }   
              $reportingtoname = (explode(" ",$row[9]));

              $reportingtoId = User::where('first_name',$reportingtoname[0])->where('last_name',$reportingtoname[1])->where('company_id',$companyId->id)->select('id')->first();

              if(isset($reportingtoId) && isset($reportingtoId->id)){

                $UserDataobj->reporting_to  = $reportingtoId->id; 

              }else
              {

                $UserDataobj->reporting_to  = 0;
              }
        
                    
              $dep = config('constant.department');
              foreach($dep as $key => $value){

                if(strtolower($value)==strtolower($row[8])){

                  $UserDataobj->department = $key;
                }
              }

              $employmentstatus = config('constant.employee_status');
              foreach($employmentstatus as $key1 => $value1){ 

                if($value1==$row[17]){
                
                  $UserDataobj->employee_status = $key1;
                }
              }
              $status = config('constant.status');
                    
              foreach($status as $key2=> $value2){  
                
                if($value2==$row[18]){
                      
                  $UserDataobj->status = $key2;
                }
              }
                    
               $name = (explode(" ",$row[1]));
               $UserDataobj->first_name = $name[0];
               $UserDataobj->last_name = $name[1]; 
               $UserDataobj->email = $row[2];
               $UserDataobj->personal_email = $row[3];
               $UserDataobj->phone = (string) $row[4];
                if(str_contains($row[5], '-')){
                      //  echo \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[13], 'YYYY-MM-DD');
                        //  // echo \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[13], 'YYYY-MM-DD');exit;
                       
                    $UserDataobj->birthdate = date('Y-m-d',strtotime($row[5]));
                }
                else
                {
                        $UserDataobj->birthdate = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[5], 'YYYY-MM-DD');
                }
                   
                 $UserDataobj->bank_name = $row[10]; 
                 $UserDataobj->bank_account_no = $row[11];
                 $UserDataobj->pan_no = $row[12];
                 if(str_contains($row[13], '-')){
                      //  echo \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[13], 'YYYY-MM-DD');
                        //  // echo \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[13], 'YYYY-MM-DD');exit;
                       
                        $UserDataobj->joining_date = date('Y-m-d',strtotime($row[13]));
                  }else{
                        
                        $UserDataobj->joining_date = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[13], 'YYYY-MM-DD');
                      }
                      $shift_time = (explode("-",$row[14]));
                      $UserDataobj->from_shift = $shift_time[0];
                      $UserDataobj->to_shift = $shift_time[1];
                      $UserDataobj->available_leaves =$row[15];
                      $UserDataobj->used_leaves =$row[16];
                      $UserDataobj->company_id =$companyId->id;
                      if(!empty($row[20])){

                      $UserDataobj->password = Hash::make($row[20]);
                      }
                      $UserDataobj->save();
                                 
                        
          }
        }  
      }


}
                     
  




                    
                   


                   




