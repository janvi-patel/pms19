<?php

namespace App\Imports;

use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\Jobs\MailJob;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Session;
use App\Mail\SalaryslipMail;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\SalaryMailLog;
class UploadSalary implements ToCollection, WithStartRow
{
    public function startRow(): int {return 1; }
    public $re_upload = false;
    public function  __construct($re_upload)
    {
        $this->re_upload= $re_upload;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $re_upload = $this->re_upload;
        $success = 0;
        foreach ($rows  as $key => $value) {
           
            $companyObj = new Company();
            $getAllCompanyName = $companyObj->getAllCompanyName();
            $employeeStatusArray = config('constant.employee_status');
            $departmentArray = config('constant.department');
            $arr = array();
            $logsRecord = array();
            $logData = array();
            if($key > 0){
                if(!$re_upload)
                {
                    $logsRecord = SalaryMailLog::where('entry_date',date('y-m-d'))->where('status',1)->pluck('user_id')->toArray();
                }
                $userDetails = array();
                $empCode = (isset($value[0]) && $value[0]) ? $value[0] : 0;
                $company = (isset($value[1]) && $value[1]) ? $value[1] : 0;
                if($company){
                    if(in_array($company,$getAllCompanyName))
                    {
                        $cmp_id = $companyObj->getcompanyIdbyName($company);
                        $userCompanyMailData  = $companyObj->getCompanyViseMailData($cmp_id)[0];    
                        $getCompanyDataById  = $companyObj->getCompanyDataById($cmp_id);
                        if ($empCode) {
                            $user = new User();
                            $userDetails = $user->getUserDataByEmpCode($empCode,$cmp_id);
                            if($userDetails){
                                $balanceCF = $basicSalaryCtc = $houseRentAllowCtc = $otherAllowCtc = $elEncashAmount = $leaveWithoutPayAmount = $performanceAllowCtc =
                                $performanceAllowSalAmount = $basicSalaryAmount = $houseRentAllowSalAmount = $otherAllowSalAmount = $sumSalAmount = $sumDeduction = $netAmount = 0;
                                
                                $balanceCF = $value[7] - $value[8] + $value[10];
                                $ctcAmount = (isset($value[15]) && $value[15])?$value[15]:0;
                                $workingDay = (isset($value[12]) && $value[12]) ? ($value[12]) : 0;
                                $presentDay = (isset($value[13]) && $value[13]) ? ($value[13]) : 0;
                                $elEncashNo = (isset($value[9]) && $value[9]) ? ($value[9]) : 0;
                                $leaveWithoutPayNo = (isset($value[14]) && $value[14]) ? ($value[14]) : 0;
                                if($ctcAmount != 0){
                                    $basicSalaryCtc = $value[16];
                                    $houseRentAllowCtc = $value[17];
                                    $otherAllowCtc = $value[18];
                                    $performanceAllowCtc = $value[19];
                                }
                                if($ctcAmount != 0 && $workingDay != 0 && $elEncashNo != 0){
                                    $elEncashAmount = round(($ctcAmount/$workingDay)*$elEncashNo);
                                }
                                if($ctcAmount != 0 && $workingDay != 0 && $leaveWithoutPayNo != 0){
                                    $leaveWithoutPayAmount = round(($ctcAmount/$workingDay)*$leaveWithoutPayNo);
                                }
                                if($workingDay != 0 && $presentDay != 0){
                                    $basicSalaryAmount = round(($basicSalaryCtc/$workingDay)*$presentDay);
                                    $houseRentAllowSalAmount = round(($houseRentAllowCtc/$workingDay)*$presentDay);
                                    $otherAllowSalAmount = round(($otherAllowCtc/$workingDay)*$presentDay);
                                    $performanceAllowSalAmount = round(($performanceAllowCtc/$workingDay)*$presentDay);
                                }
                                $arr = [
                                        'user_id' =>(isset($userDetails['id']) && $userDetails['id']) ? $userDetails['id'] : 0,
                                        'month_year' => (isset($value[2]) && $value[2] && $value[28]) ? $value[2].' - '.$value[28] : '',
                                        'emp_code' => (isset($value[0]) && $value[0]) ? ((!empty($getCompanyDataById))?$getCompanyDataById->emp_short_name:'').$value[0] : '',
                                        'emp_name' => (isset($userDetails['first_name']) && isset($userDetails['last_name'])) ? $userDetails['first_name'] . ' ' . $userDetails['last_name'] : '',
                                        'emp_email' => (isset($userDetails['email']) && $userDetails['email']) ? $userDetails['email'] : '',
                                        'personal_email' => (isset($userDetails['personal_email']) && $userDetails['personal_email']) ? $userDetails['personal_email'] : $userDetails['email'],
                                        'department' => (isset($departmentArray[$userDetails['department']]) && $departmentArray[$userDetails['department']]) ? $departmentArray[$userDetails['department']] : '',
                                        'designation' => (isset($userDetails['designation_name']) && $userDetails['designation_name']) ? $userDetails['designation_name'] : '',
                                        'bank_name' => (isset($userDetails['bank_name']) && $userDetails['bank_name']) ? $userDetails['bank_name'] : '',
                                        'bank_account_no' => (isset($userDetails['bank_account_no']) && $userDetails['bank_account_no']) ? $userDetails['bank_account_no'] : '',
                                        'joining_date' => (isset($userDetails['joining_date']) && $userDetails['joining_date']) ? date('d-m-Y', strtotime($userDetails['joining_date'])) : '',
                                        'pan_no' => (isset($userDetails['pan_no']) && $userDetails['pan_no']) ? $userDetails['pan_no'] : '',
                                        'status' => (isset($employeeStatusArray[$userDetails['employee_status']]) && $employeeStatusArray[$userDetails['employee_status']]) ? $employeeStatusArray[$userDetails['employee_status']] : '',
                                        'uan_no' => (isset($value[3]) && $value[3]) ? ($value[3]) : '',
                                        'esic_no' => (isset($value[4]) && $value[4]) ? ($value[4]) : '',
                                        'pf_no' => (isset($value[5]) && $value[5]) ? ($value[5]) : '',
                                        'leave_issue' => (isset($value[6]) && $value[6]) ? ($value[6]) : 0,
                                        'leave_balance' => (isset($value[7]) && $value[7]) ? ($value[7]) : 0,
                                        'leave_taken' => (isset($value[8]) && $value[8]) ? ($value[8]) : 0,
                                        'el_encash_no' => $elEncashNo,
                                        'adjusted' => (isset($value[10]) && $value[10]) ? ($value[10]) : 0,
                                        'balance_cf' => $balanceCF,
                                        'total_days' => (isset($value[11]) && $value[11]) ? ($value[11]) : 0,
                                        'working_days' => $workingDay,
                                        'present_days' => $presentDay,
                                        'leave_without_pay_no' => $leaveWithoutPayNo,
                                        'ctc' => $ctcAmount,
                                        'basic_salary_ctc' =>  $basicSalaryCtc,
                                        'house_rent_allowance_ctc' => $houseRentAllowCtc,
                                        'other_allownces_ctc' => $otherAllowCtc,
                                        'basic_salary_amount' =>  $basicSalaryAmount,
                                        'house_rent_allowance_salary' => $houseRentAllowSalAmount,
                                        'other_allownces_salary' => $otherAllowSalAmount,
                                        'performance_allowance' =>  $performanceAllowSalAmount,
                                        'performance_allowance_ctc' =>  $performanceAllowCtc,
                                        'bonus' =>  (isset($value[20]) && $value[20]) ? ($value[20]) : 0,
                                        'other' =>  (isset($value[21]) && $value[21]) ? ($value[21]) : 0,
                                        'el_encash_amount' =>  $elEncashAmount,
                                        'professional_tax' =>  (isset($value[22]) && $value[22]) ? ($value[22]) : 0,
                                        'income_tax' =>  (isset($value[23]) && $value[23]) ? ($value[23]) : 0,
                                        'gratuity' =>  (isset($value[24]) && $value[24]) ? ($value[24]) : 0,
                                        'pf_employee_contribution' =>  (isset($value[25]) && $value[25]) ? ($value[25]) : 0,
                                        'esi_employee_contribution' =>  (isset($value[26]) && $value[26]) ? ($value[26]) : 0,
                                        'other_deduction' =>  (isset($value[27]) && $value[27]) ? ($value[27]) : 0,
                                        'leave_without_pay_amount' =>  $leaveWithoutPayAmount,
                                    ];
                
                                if (!empty($arr)) {
                                    $sumDeduction = round($arr['professional_tax'] + $arr['income_tax'] + $arr['gratuity'] + $arr['pf_employee_contribution'] +
                                                        $arr['esi_employee_contribution']+ $arr['other_deduction'] + $arr['leave_without_pay_amount']) ;
                                    $sumSalAmount = round($basicSalaryAmount + $houseRentAllowSalAmount + $otherAllowSalAmount + $arr['performance_allowance'] +
                                                    $arr['bonus'] + $arr['other'] + $arr['el_encash_amount']);
                                    $netAmount =  round($sumSalAmount - $sumDeduction);
                                    $arr['total_deduction'] = $sumDeduction;
                                    $arr['net_amount'] = $netAmount;
                                    $arr['sumSalAmount'] = $sumSalAmount;
                                    $arr['from_email'] = $userCompanyMailData['from_address'];
                                    $arr['from_name']  = $userCompanyMailData['company_name'];
                                    $arr['comp_name']  = (!empty($getCompanyDataById))?$getCompanyDataById->company_name:'';
                                    $arr['comp_address']  = (!empty($getCompanyDataById))?$getCompanyDataById->address:'';
                                    $arr['comp_website']  = (!empty($getCompanyDataById))?$getCompanyDataById->website:'';
                                    $arr['comp_logo']  = (!empty($getCompanyDataById))?$getCompanyDataById->logo:'';
                                    $arr['comp_contact']  = (!empty($getCompanyDataById))?$getCompanyDataById->mobile_number:'';
                                    $arr['comp_email']  = (!empty($getCompanyDataById))?$getCompanyDataById->email_address:'';
                                    if(!in_array($arr['user_id'],$logsRecord))
                                    {
                                        $pdf = PDF::loadView('admin.salaryPDF', $arr);
                                        $pdf->setPaper('A4', 'portrait');
                                        $pdf->output();
                                        try{
                                            Mail::send(new SalaryslipMail($pdf,$arr));
                                            $success++;
                                            $logData = [
                                                'user_id' => $arr['user_id'],
                                                'emp_id' => $empCode,
                                                'emp_comp' => $cmp_id,
                                                'status' => 1,
                                                'reason' => '',
                                                'email' => $arr['personal_email']
                                            ];
                                            $this->storeLogs($logData);
                                        }
                                        catch(\Exception $e){
                                            $logData = [
                                                'user_id' => $arr['user_id'],
                                                'emp_id' => $empCode,
                                                'emp_comp' => $cmp_id,
                                                'status' => 3,
                                                'reason' => 'The issue in the Mail system. issue is =>'.$e->getMessage(),
                                                'email' => $arr['personal_email']
                                            ];
                                            $this->storeLogs($logData);
                                        }
                                    }else{
                                        $logData = [
                                            'user_id' => $arr['user_id'],
                                            'emp_id' => $empCode,
                                            'emp_comp' => $cmp_id,
                                            'status' => 2,
                                            'reason' => '',
                                            'email' => $arr['personal_email']
                                        ];
                                        $this->storeLogs($logData);
                                    }
                                }
                            }else{
                                $logData = [
                                    'user_id' => 0,
                                    'emp_id' => $empCode,
                                    'emp_comp' => $cmp_id,
                                    'status' => 3,
                                    'reason' => 'The employee details not found',
                                    'email' => ''
                                ];
                                $this->storeLogs($logData);
                            }
                        }else{
                            $logData = [
                                'user_id' => 0,
                                'emp_id' => $empCode,
                                'emp_comp' => $cmp_id,
                                'status' => 3,
                                'reason' => 'The employee code is empty',
                                'email' => ''
                            ];
                            $this->storeLogs($logData);
                        }
                    }
                    else{
                        $logData = [
                            'user_id' => 0,
                            'emp_id' => $empCode,
                            'emp_comp' => 0,
                            'status' => 3,
                            'reason' => 'The ( '.$company.' ) company was not found',
                            'email' => ''
                        ];
                        $this->storeLogs($logData);
                    }
                }
                else{
                    if($value[0] != '' || $value[1] != '' || $value[2] != '' || $value[3] != '' || $value[4] != '')
                    {
                        $logData = [
                            'user_id' => 0,
                            'emp_id' => $empCode,
                            'emp_comp' => 0,
                            'status' => 3,
                            'reason' => 'The company name was empty',
                            'email' => ''
                        ];
                        $this->storeLogs($logData);
                    }
                }
            }
        }
        Session::push('salarySheetImportError',$success);
    }

    public function storeLogs($logData)
    {
        $empCode = (int)$logData['emp_id'];
        try{
            $log = new SalaryMailLog();
            $log->user_id = $logData['user_id'];
            $log->emp_id = $empCode;
            $log->emp_comp = $logData['emp_comp'];
            $log->entry_date = date('Y-m-d');
            $log->status = $logData['status']; //  1 - success, 2 - skipped, 3 - failed
            $log->reason = substr($logData['reason'], 0, 250);
            $log->email = $logData['email'];
            $log->uploaded_by = Auth::user()->id;
            $log->uploaded_by_comp = Auth::user()->company_id;
            $log->save();
        }catch(\Exception $e){}
    }
}
