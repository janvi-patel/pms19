<?php

namespace App\Imports;
use App\User;
use App\Company;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Session;

class UsersLeaveImport implements ToCollection, WithStartRow
{
    public function startRow(): int {return 1; }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $success = $inValidRecord = 0;
        $data = array();
        $companyError = array();
        
        foreach ($rows  as $key => $row) {
            if($key > 0 && $row[0] != ''){
                $error = 0;
               if(($error == 0) && $row[1]=='')
                        {
                          $companyError[] =  'Comapny Name missing for record '.$key .'.'; 
                           $error = 1; 
                        } 
                if($error == 0){
                $company = $row[1];
                    $companyId = 2;
                    if($company){
                        $companyArray = config('constant.company');
                        foreach($companyArray AS $cmpName => $companyVal){
                            if($cmpName == lcfirst($company)){
                                $companyId = $companyVal;
                            }
                        }
                    }

                    if($companyId){
                         $companyObj = Company::find($companyId);
                         $empcode =  str_replace($companyObj->emp_short_name,'',$row[0]);
                    }
                    $userObj = new User();
                    $getUserId = $userObj->getUserDataByEmpCode($empcode,$companyId);
                    //dd($getUserId);
                    if($getUserId!=null){
                        $updateUser = User::find($getUserId['id']);
                        $updateUser->available_leaves = $row[2];
                        $updateUser->used_leaves	= $row[3];
                        $updateUser->save();
                        $success++;
                    }else{
                        $inValidRecord++;
                    }
                }    
            }
        }
        $data['success']=$success;
        $data['skipped']= $inValidRecord;
        $data['companyError']  = array_filter($companyError);
        Session::push('UserLeaveImportError',$data);
    }
}
