<?php

namespace App\Imports;

use App\Attendance;
use App\AttendanceDetail;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\Setting;
use App\Leave;
use App\Jobs\MailJob;
use App\UserWorkFromHome;
// use Illuminate\Contracts\Queue\ShouldQueue;
// use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\TimeEntryStatus;
use Session;

class CheckTimeEntryFile implements ToCollection, WithStartRow
{
    private $sheetDate = "";

    public function  __construct($sheetDate)
    {
        $this->sheetDate = $sheetDate;
    }

    public function startRow(): int {
        return 2;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        set_time_limit(0);
        $sortArr = array();
      
        if(isset($rows) && count($rows)>0){
            foreach ($rows as $key => $row) {
                $time_entry_status = checkTimeEntryFileHelper($row[2]);
                if(count($time_entry_status) > 0) {
                    $sortArr[$key]['empcode'] = str_replace(" ","",$row[0]);
                    $sortArr[$key]['name'] = $row[1];
                    $sortArr[$key]['time'] = $time_entry_status;
                }               
            }
          
        }
        Session::push('TimeEntryData',$sortArr);
     
    } 
}
