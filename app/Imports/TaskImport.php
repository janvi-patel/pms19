<?php

namespace App\Imports;

use App\Attendance;
use App\AttendanceDetail;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\Project;
use App\Setting;
use App\ProjectTasks;
use App\Jobs\MailJob;
use App\Cr;
use App\ProjectTeam;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Session;
use DateTime;

class TaskImport implements ToCollection, WithStartRow
{
    public function startRow(): int {return 1; }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $setting = new Setting();
        $days = $setting->getSettingTaskOperationData();
        $company_id = Auth::user()->company_id;
        $created_by = Auth::user()->id;
        $errorData = array();
        $success = 0;
        $projectHour = 0;
        $remainingHours = 0;
        $projectError = $userError = $projectCostError = $projectStartDateError = $projectEndDateError = $getUserId = $getProject = $projectDateError = $tasknameError= $estimatedHours = array();
        foreach ($rows  as $key => $row) {
            if($key > 0){
  
               
              
                $getUserId = User::where('employee_id',$row[2])->where('company_id',$company_id)->select('id')->first();
                $getProject = Project::where('project_name',$row[3])
                                ->where(function($q) use($company_id) {
                                    $q->where('projects.company_id', $company_id)
                                    ->orWhere('projects.assigned_to', $company_id);
                                })
                                ->select('id','project_type','project_start_date')->first();
                if(isset($getUserId) && isset($getProject)){
                    if(date('d-m-Y',strtotime(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[4], 'YYYY-MM-DD'))) != \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[4], 'DD-MM-YYYY')){
                        $projectStartDateError[] = $row[4];
                    }
                    else if(date('d-m-Y',strtotime(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[5], 'YYYY-MM-DD'))) != \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[5], 'DD-MM-YYYY'))
                    {
                        $projectEndDateError[] = $row[5];
                    }else{
                        $error          = 0;
                        $start_date     = date('Y-m-d',strtotime(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[4], 'YYYY-MM-DD')));
                        $end_date       = date('Y-m-d',strtotime(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[5], 'YYYY-MM-DD')));
                        $startMonth     = date('Y-m',strtotime(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[4], 'YYYY-MM-DD')));
                        $endMonth       = date('Y-m',strtotime(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[5], 'YYYY-MM-DD')));
                        if(($error == 0) &&  $end_date < $start_date)
                        {
                            $projectDateError[] = '"'.$row[0] . '" task\'s end-date must be after the start-date';
                            $error = 1;
                        }
                        if(($error == 0) && strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d")) && date('Y-m-d',strtotime($start_date)) < date("Y-m-d",strtotime("first day of previous month"))){
                            $projectDateError[] = '"'.$row[0] . '" task\'s start-date must be after the '.date("d-M-Y",strtotime("first day of previous month")).'!';
                            $error = 1;
                        }
                        else if(($error == 0) && !(strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d"))) && date('Y-m') > $startMonth){
                            $projectDateError[] = '"'.$row[0] . '" task\'s start-date must be after the '.date("01-M-Y").'!';
                            $error = 1;
                        }
                        else{}
                        if(($error == 0) && $startMonth != $endMonth){
                            $projectDateError[] = '"'.$row[0] . '" task\'s start-date and end-date with same month';
                            $error = 1;
                        }
                        if(($error == 0) && $getProject['project_start_date'] > date('Y-m-d',strtotime($start_date))){
                            $projectDateError[] = '"'.$row[0] . '" task\'s start-date must be after the project start date';
                            $error = 1;
                        }
                        $projectHours = $this->checkTaskHours($getProject,$getProject['id'],'');
                        if(($error == 0) && $projectHours < $row[6] && $getProject['project_type'] != 3){
                            $projectDateError[] = '"'.$row[0] . '" task\'s hours is more than project hours';
                            $error = 1;
                        }
                        if(($error == 0) && $row[0]==''){
                           $tasknameError[] =  'Task Name missing for record '.$key . '.'; 
                           $error = 1; 
                        }
                        
                        if((($error == 0) && !in_array(fmod($row[6], 1),['0','0.25','0.75','0.50'])) || $row[6] <= 0 ){
                            $estimatedHours[] =  'Please insert hours in proper format for record '.$key . '.'; 
                            $error = 1; 
                         }
                            
                                
                           
                        if($error == 0){
                            $ProjectTasks                   = new ProjectTasks;
                            $ProjectTasks->task_name        = $row[0];
                            $ProjectTasks->task_desc        = $row[1];
                            $ProjectTasks->created_by       = $created_by;
                            $ProjectTasks->user_id          = (isset($getUserId)&&$getUserId['id'])?$getUserId['id']:'';
                            $ProjectTasks->project_id       = (isset($getProject)&&$getProject['id'])?$getProject['id']:'';
                            $ProjectTasks->task_start_date  = $start_date;
                            $ProjectTasks->task_end_date    = $end_date;
                            $ProjectTasks->estimated_hours  = $row[6];

                            $ProjectTasks->save();
                            $createTeam = new ProjectTeam();
                            $createTeam->createTeam($ProjectTasks->project_id,$ProjectTasks->user_id);
                            $success++;
                        }
                    }
                }else{
                    if(empty($getProject['id'])){
                        $projectError[] = $row[3];
                    }
                    elseif(empty($getUserId['id'])){
                        $userError[] = $row[2];
                    }
                }
            }
        }
        $errorData['success']           = $success;
        $errorData['userId']            = array_filter($userError);
        $errorData['taskname']          = array_filter($tasknameError);
        $errorData['estimatedHours']          = array_filter($estimatedHours);
        
        $errorData['projectId']         = array_filter($projectError);
        $errorData['projectCost']       = array_filter($projectCostError);
        $errorData['projectStartDate']  = array_filter($projectStartDateError);
        $errorData['projectEndDate']    = array_filter($projectEndDateError);
        $errorData['projectDateError']  = array_filter($projectDateError);
        Session::push('taskImportError',$errorData);

    }
    public function checkProjectHours($projectId) {
        $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                        ->select('projects.pms_hours','projects.id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                        ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                        ->whereNull('cr.deleted_at')
                        ->where('projects.id',$projectId)
                        ->groupBy('projects.id')->first();
        return $projectCrHours;
    }

    public function getProjectPmsHours($projectId) {
        $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                        ->select('projects.pms_hours','projects.id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                        ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                        ->whereNull('cr.deleted_at')
                        ->where('projects.id',$projectId)
                        //->groupBy('projects.id')
                        ->first(); 
        return $projectCrHours;
    }

    public function checkTaskHours($getProject,$projectId,$taskId) {
        $remainingHours = 0;
        if($getProject['project_type'] != 3){
            $getProjectHr = $this->getProjectPmsHours($projectId);
            $projectHour =  sprintf ("%.2f",$getProjectHr['pms_hours'] + $getProjectHr['total_cr_hours']);
            
            $taskHour = ProjectTasks::where('project_id',$projectId);
            if($taskId != ''){
                $taskHour->where('id','!=',$taskId);
            }
            $taskHour =  $taskHour->select(DB::raw("sum(`estimated_hours`) as total_task_hours"))->first();
            if($projectHour > $taskHour['total_task_hours']){
                $remainingHours = $projectHour - $taskHour['total_task_hours'];
            }
        }
        return $remainingHours;
    }

}
