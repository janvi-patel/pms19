<?php

namespace App\Imports;

use App\Project;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use App\MiscBeanchTask;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\MiscBeanchTaskEntry;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\ProjectEntry;
use App\ProjectTasks;
class ImportProjectEntry implements ToCollection, WithStartRow
{
    public function startRow(): int {return 1; }
    
    public function collection(Collection $rows)
    {
        $company_id = Auth::user()->company_id;
        $created_by = Auth::user()->id;
        $errorData = array();
        $success = 0;
        $miscError = 0;
        $projectHour = 0;
        $remainingHours = 0;
        $projectError = array();
        $userError = array();
        $miscTaskError = array();
        $otherTaskError = array();
        $getUserId = $getProject = array();
        foreach ($rows  as $key => $row) {
            if($key > 0){
                $getUserId = User::where('employee_id',$row[0])->where('company_id',$company_id)->select('id')->first();
                $getProject = Project::where('project_name',$row[1])
                                ->where(function($q) use($company_id) {
                                    $q->where('projects.company_id', $company_id)
                                    ->orWhere('projects.assigned_to', $company_id);
                                })->select('id','project_type')->first();
                if(isset($getUserId) && isset($getProject)){
                    if($row[1] == 'Miscellaneous Tasks' || $row[1] == 'Bench'){
                        $task_start_date =  \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[4], 'YYYY-MM-DD');
                        $task_end_date =  \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[5], 'YYYY-MM-DD');
                        $getMiscBeanchTask = MiscBeanchTask::where('project_id',$getProject['id'])
                                                ->where('user_id',$getUserId['id'])
                                                ->where('task_title',trim($row[2]))
                                                ->where('task_start_date',$task_start_date)
                                                ->where('task_end_date',$task_end_date)->first();
                        if(!isset($getMiscBeanchTask)){
                            if($row[2] == '' ||$row[4] == '' || $row[5] == '' ){
                                $miscTaskError[] = $row[1];
                                $miscError ++ ; 
                            }else{
                                $miscBeanchTaskObj = new MiscBeanchTask();
                                $miscBeanchTaskObj->task_title = $row[2];
                                $miscBeanchTaskObj->project_id = $getProject['id'];
                                $miscBeanchTaskObj->user_id = $getUserId['id'];
                                $miscBeanchTaskObj->task_start_date = $task_start_date;
                                $miscBeanchTaskObj->task_end_date = $task_end_date;
                                $miscBeanchTaskObj->save();
                                $miscTaskId = $miscBeanchTaskObj->id;
                            }
                        }else{
                            $miscTaskId = $getMiscBeanchTask['id'];
                        }
                        if($miscError == 0){
                            $projectEntry = new ProjectEntry; 
                            $projectEntry->project_id = (isset($getProject)&&$getProject['id'])?$getProject['id']:'';
                            $projectEntry->user_id = (isset($getUserId)&&$getUserId['id'])?$getUserId['id']:'';
                            $projectEntry->log_hours = $row[6];
                            $projectEntry->desc = $row[3];
                            $projectEntry->log_date = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[7], 'YYYY-MM-DD');
                            $projectEntry->save();
                            if($row[1] == 'Miscellaneous Tasks' || $row[1] == 'Bench'){
                                $miscBeanchTaskEntryObj = new MiscBeanchTaskEntry();
                                $miscBeanchTaskEntryObj->project_entries_id = $projectEntry->id;
                                $miscBeanchTaskEntryObj->misc_beanch_task_id  = $miscTaskId;
                                $miscBeanchTaskEntryObj->save();
                            }
                            $success++;
                        }
                    }else{
                        if($row[2] == '' ||$row[4] == '' || $row[5] == '' ){
                            $miscTaskError[] = $row[1];
                        }else{
                            $projectTasks = ProjectTasks::where('project_id',$getProject->id)->where('task_name',$row[2])->first();
                            if(!isset($projectTasks)){
                                $otherTaskError[] = " ".$row[2]." tasks name is not valid for ". $row[1] ." Project";
                            }else{
                                $task_start_date =  \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[4], 'YYYY-MM-DD');
                                $task_end_date =  \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[5], 'YYYY-MM-DD');
                                if($projectTasks->task_start_date != $task_start_date || $projectTasks->task_end_date != $task_end_date){
                                    $otherTaskError[] = " in ".$row[2]." tasks date starting date or ending date is not valid ";
                                }
                                else{
                                    if($projectTasks->user_id != $getUserId->id){
                                        $otherTaskError[] = " in ".$row[2].' tasks employee code '. $row[0] ." is not valid ";
                                    }
                                    else{
                                        $projectEntry = new ProjectEntry; 
                                        $projectEntry->project_id = $getProject->id;
                                        $projectEntry->user_id = $getUserId->id;
                                        $projectEntry->log_hours = $row[6];
                                        $projectEntry->task_id = $projectTasks->id;
                                        $projectEntry->desc = $row[3];
                                        $projectEntry->log_date = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::toFormattedString($row[7], 'YYYY-MM-DD');
                                        $projectEntry->save();
                                        $success++;
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if(empty($getProject['id'])){ 
                        $projectError[] = $row[1];
                    }
                    elseif(empty($getUserId['id'])){
                        $userError[] = $row[0];
                    }
                }
            }
        }
        $errorData['success'] = $success;
        $errorData['userId'] = array_filter($userError);
        $errorData['projectId'] = array_filter($projectError);
        $errorData['miscTask'] = array_filter($miscTaskError);
        $errorData['otherTaskError'] = array_filter($otherTaskError);
        Session::push('projectEntryImportError',$errorData);
    }
}
