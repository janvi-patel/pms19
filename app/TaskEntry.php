<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Http\Controllers\AttendancesController;
use Illuminate\Support\Facades\Auth;
use App\ProjectEntry;
use App\Setting;
use App\Holiday;
use App\User;
use App\ProjectTasks;
class TaskEntry extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'task_entries';

    public $sortable = ['log_date', 'log_hours'];

    public function getTaskEntryList($taskID = 0){
    	$taskEntry = TaskEntry::query();

        if ($taskID) {
            $taskEntry->where('task_id', $taskID);
        }

        $taskEntryData = $taskEntry->orderBy('id','ASC')->get()->toArray();
        
        return $taskEntryData;
    }

    public function getTaskEntryDetail($id = 0){
    	$taskEntry = TaskEntry::query();

        if ($id) {
            $taskEntry->where('id', $id);
        }

        $taskEntryData = $taskEntry->orderBy('id','ASC')->first()->toArray();
        
        return $taskEntryData;
    }

    public function getTeamTaskEntries($searchFilter = array()){

        //$query = TaskEntry::select("task_entries.*", "p.project_name", "p.id as project_id","t.task_name", DB::raw("CONCAT(u.first_name,' ',u.last_name) as user_name"), DB::raw("SUM(task_entries.log_hours) as total_log_hours"));
        $query = TaskEntry::select("task_entries.*", "p.project_name", "p.id as project_id","t.task_name", DB::raw("CONCAT(u.first_name,' ',u.last_name) as user_name"), 
                DB::raw("floor(sum(floor(task_entries.`log_hours`)) + sum(task_entries.`log_hours` - floor(task_entries.`log_hours`)) * 100.0/60) +((sum(floor(task_entries.`log_hours`)) + sum(task_entries.`log_hours` - floor(task_entries.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_log_hours"));
        $query->groupBy('task_entries.log_date');
        $query->join('tasks as t', 'task_entries.task_id', '=', 't.id');
        $query->leftJoin('users as u', 't.user_id', '=', 'u.id');
        $query->leftJoin('projects as p', 'p.id', '=', 't.project_id');
        $query->groupBy('p.project_name');

        if (isset($searchFilter['employee_id']) && isset($searchFilter['year']) && isset($searchFilter['month'])) {
            $query->where('t.user_id', $searchFilter['employee_id'])
                  ->whereYear('task_entries.log_date', '=', $searchFilter['year'])
                  ->whereMonth('task_entries.log_date', '=', $searchFilter['month']);
        }

        $taskEntryData = $query->orderBy('p.project_name','ASC')->orderBy('task_entries.log_date','ASC')->get();
        
        return $taskEntryData;
    }

    public function getTaskLogHours($searchFilter = array()){

        //$query = TaskEntry::select("task_entries.*", DB::raw("SUM(task_entries.log_hours) as total_log_hours"));
        $query = TaskEntry::select("task_entries.*",DB::raw("floor(sum(floor(task_entries.`log_hours`)) + sum(task_entries.`log_hours` - floor(task_entries.`log_hours`)) * 100.0/60) +((sum(floor(task_entries.`log_hours`)) + sum(task_entries.`log_hours` - floor(task_entries.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_log_hours"));
        $query->join('tasks as t', 'task_entries.task_id', '=', 't.id');

        if (isset($searchFilter['userID']) && isset($searchFilter['year']) && isset($searchFilter['month'])) {
            $query->where('t.user_id', $searchFilter['userID'])
                  ->whereYear('task_entries.log_date', '=', $searchFilter['year'])
                  ->whereMonth('task_entries.log_date', '=', $searchFilter['month']);
        }
        $query->groupBy('task_entries.log_date');
        $taskEntryData = $query->orderBy('task_entries.log_date','ASC')->get()->toArray();
        return $taskEntryData;
    }
    public function getTeamTaskEntriesByProject($project_id = 40, $searchFilter = array()){

        //$query = TaskEntry::select("task_entries.*", "p.project_name", "p.id as project_id","t.task_name", DB::raw("CONCAT(u.first_name,' ',u.last_name) as user_name"), DB::raw("SUM(task_entries.log_hours) as task_total_log_hours"));
        $query = TaskEntry::select("task_entries.*", "p.project_name", "p.id as project_id","t.task_name", DB::raw("CONCAT(u.first_name,' ',u.last_name) as user_name"), 
                DB::raw("floor(sum(floor(task_entries.`log_hours`)) + sum(task_entries.`log_hours` - floor(task_entries.`log_hours`)) * 100.0/60) +((sum(floor(task_entries.`log_hours`)) + sum(task_entries.`log_hours` - floor(task_entries.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as task_total_log_hours"));
        $query->groupBy('task_entries.log_date');
        $query->join('tasks as t', 'task_entries.task_id', '=', 't.id');
        $query->leftJoin('users as u', 't.user_id', '=', 'u.id');
        $query->leftJoin('projects as p', 'p.id', '=', 't.project_id');
        $query->groupBy('task_entries.task_id');
        $query->where('p.id', $project_id);

        if (isset($searchFilter['employee_id']) && isset($searchFilter['year']) && isset($searchFilter['month'])) {
            $query->where('t.user_id', $searchFilter['employee_id'])
                  ->whereYear('task_entries.log_date', '=', $searchFilter['year'])
                  ->whereMonth('task_entries.log_date', '=', $searchFilter['month']);
        }

        $taskEntryDataByPro = $query->orderBy('p.project_name','ASC')->orderBy('task_entries.log_date','ASC')->get();
        
        return $taskEntryDataByPro;
    }
    public function getLoggedHours($userId,$month,$year){
        $query = TaskEntry::select(DB::raw("AVG(task_entries.log_hours) as total_log_hours"));
        $query->join('tasks as t', 'task_entries.task_id', '=', 't.id');
        $query->where('t.user_id', $userId);
        $query->where('task_entries.log_date', 'like', '%' . $year.'-'.sprintf("%02d",$month).'%');
        
        $loggedHour = $query->get()->toArray();
        return $loggedHour;
    }

    public function task() {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }

    public function getTaskEntryDate($emp_code,$id,$comapny_name,$company_id){
        $allowDays = Setting::where('module','TaskEntry')->where('company_id',$company_id)->select('days')->first();
        $diffToCheck = $allowDays->days * 86400;
//        $get2DaysDate = date('Y-m-d', strtotime('-2 day'));
        $get2DaysDate = '2019-09-23';
        $bufferSec =  '00:15:00';
        $attCont = new AttendancesController();
        $lastAttendDate = Attendance::where(function($q){
                                $q->where('attendances.absent', NULL)
                                ->where(function($q){
                                    $q->where('first_in','!=','00:00:00')
                                    ->orwhere('last_out','!=','00:00:00');
                                });
                            })
                            ->where('emp_code', $emp_code)
                            ->where('emp_comp', $comapny_name)
                            ->where('entry_date',$get2DaysDate)->first();
                            
        if(!empty($lastAttendDate)){
            $projectEntry = ProjectEntry::select(DB::raw("sum(`log_hours`) as total_logged_hours"))
                    ->where('user_id','=',$id)
                    ->where('log_date',$get2DaysDate)
                    ->first();
            if(!empty($projectEntry)){
                $time_seconds =  $this->timeToSec($projectEntry->total_logged_hours);
                $avg_log_hr = gmdate("H:i:s", $time_seconds);
                $avg_log_limit = $this->findAvgLoggedTime($comapny_id);
                if(strtotime($avg_log_limit) > strtotime($avg_log_hr)){
                    $query = AttendanceDetail::select(DB::raw('TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))), "%H:%i:%s") AS working_hours'))
                            ->leftJoin('attendances', 'attendances.id', '=', 'attendance_details.attendance_id')
                            ->where('attendances.absent', NULL)
                            ->where('attendances.emp_code', $emp_code)
                            ->where('attendances.emp_comp', $comapny_name)
                            ->where('attendances.entry_date',$get2DaysDate)
                            ->groupBy('attendance_details.attendance_id')->first();
                    if(!empty($query)){
                        $avg_workign_hr =  $query->working_hours;
                        $workDiffLog =  strtotime($avg_workign_hr) - strtotime($avg_log_hr);
                        if(strtotime(gmdate("H:i:s", $workDiffLog)) > strtotime($bufferSec)){
                            return $get2DaysDate;
                        }
                    }
                }
            }else{
                return false;
            }
        }else{
            return false;
        }

    }      
    public function timeToSec($time) {
        $avg_log_hr = '00:00:00';
        $logged_hours = (new \App\Helpers\CommonHelper)->displayTaskTime($time);
        $hoursMinutes = (explode(".",$logged_hours));
        $hours = $minutes = '00';

        if(isset($hoursMinutes[0])){
            $hours = $hoursMinutes[0]; 
            $hours = (strlen($hours) == 1)?'0'.$hours:$hours;
        }

        if(isset($hoursMinutes[1])){
            $minutes = $hoursMinutes[1];
            $minutes = (strlen($minutes) == 1)?$minutes.'0':$minutes;
        }

        $str_time = $hours.':'.$minutes.':00';

        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

        $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
        return $time_seconds;
    }
    public function userLoggedHoursForMail($user_id,$month,$year,$date) {
      
        $loggedHours = ProjectEntry::select('users.employee_id',
                        DB::raw("sum(project_entries.`log_hours`) as total_logedHours"))
                        ->leftjoin('users','users.id','user_id')
                        ->where(function($q) use ($month,$year){
                            $q->where('project_entries.log_date', 'like', '%' . $year.'-'.sprintf("%02d",$month).'%');
                        })
                        ->where('project_entries.user_id',$user_id)
                     ->groupBy('project_entries.user_id')
                    ->pluck('total_logedHours','users.employee_id')->toArray();
        
        return $loggedHours;
    }
    public function userAttendDataForMail($companyName,$emp_code,$month,$year,$company) {
        $query = Attendance::selectRaw('emp_code, emp_name, SUM(full_day) as full_days, SUM(half_day) as half_days, SUM(early_going) as early_going_days,SUM(absent) as absent_days,
                SUM(late_comer) as late_days, SUM(incorrect_entry) as incorrect_days,users.reporting_to,users.department,users.company_id');
                $query->leftJoin('users','emp_code','users.employee_id');
                $query->where('users.status',1);
                $query->where('emp_comp',$companyName );
                $query->where('company_id',$company );
                $query->where('emp_code', $emp_code);
                $query->whereMonth('entry_date', $month);
                $query->whereYear('entry_date', $year);
        $records = $query->groupBy('emp_code')->get();
        return $records;
    }
    public function userWeeklyLoggedHoursForMail($user_id,$lastWeekStartDate,$lastWeekEndDate) {
      
        $loggedHours = ProjectEntry::select('users.employee_id',
                        DB::raw("sum(project_entries.`log_hours`) as total_logedHours"))
                        ->leftjoin('users','users.id','user_id')
                        ->where(function($q) use ($lastWeekStartDate,$lastWeekEndDate){
                            $q->whereBetween('project_entries.log_date', [$lastWeekStartDate, $lastWeekEndDate]);
                        })
                        ->where('project_entries.user_id',$user_id)
                     ->groupBy('project_entries.user_id')
                    ->pluck('total_logedHours','users.employee_id')->toArray();
        
        return $loggedHours;
    }

    public function userDailyLoggedHoursForMail($user_id,$date) {
      
        $loggedHours = ProjectEntry::select('users.employee_id',
                        DB::raw("sum(project_entries.`log_hours`) as total_logedHours"))
                        ->leftjoin('users','users.id','user_id')
                        ->where(function($q) use ($date){
                            $q->where('project_entries.log_date', $date);
                        })
                        ->where('project_entries.user_id',$user_id)
                     ->groupBy('project_entries.user_id')
                    ->pluck('total_logedHours','users.employee_id')->toArray();
        
        return $loggedHours;
    }
    
    public function userWeeklyAttendDataForMail($companyName,$emp_code,$lastWeekStartDate,$lastWeekEndDate,$company_id) {
        $query = Attendance::selectRaw('emp_code, emp_name, SUM(full_day) as full_days, SUM(half_day) as half_days, SUM(early_going) as early_going_days,SUM(absent) as absent_days,
                SUM(late_comer) as late_days, SUM(incorrect_entry) as incorrect_days,users.reporting_to,users.department,users.company_id');
                $query->leftJoin('users','emp_code','users.employee_id');
                $query->where('users.status',1);
                $query->where('emp_comp',$companyName );
                $query->where('company_id',$company_id );
                $query->where('emp_code', $emp_code);
                $query->whereBetween('entry_date', [$lastWeekStartDate, $lastWeekEndDate]);
        $records = $query->groupBy('emp_code')->get();
        return $records;
    }

    public function userDailyAttendDataForMail($companyName,$emp_code,$date,$company_id) {
        $query = Attendance::selectRaw('emp_code, emp_name, SUM(full_day) as full_days, SUM(half_day) as half_days, SUM(early_going) as early_going_days,SUM(absent) as absent_days,
                SUM(late_comer) as late_days, SUM(incorrect_entry) as incorrect_days,users.reporting_to,users.department,users.company_id');
                $query->leftJoin('users','emp_code','users.employee_id');
                $query->where('users.status',1);
                $query->where('emp_comp',$companyName );
                $query->where('company_id',$company_id );
                $query->where('emp_code', $emp_code);
                $query->where('entry_date', $date);
        $records = $query->groupBy('emp_code')->get();
        return $records;
    }
    public function dashboardTimeDetails($companyName,$month,$year, $authUserEmpcode = "") {
        $user          = new User();
      
        $authUser      = Auth::user();
        $authUserId      = Auth::user()->id;
        if($authUserEmpcode == ""){
            $authUserEmpcode      = Auth::user()->employee_id;
        }
        $comapny_id = '';
        $current_years = $year;
        $current_month  =  $month;
        $userListing   = $user->getUserDropDown();
        for ($y = config('constant.Start_year'); $y <= $current_years; $y++) {
            $years[$y] = $y;
        }
        $data['employee']      = $authUserEmpcode;
        $data['request_month'] = $month;
        $data['request_year']  = $year;

        $query = Attendance::selectRaw('emp_code, emp_name, SUM(full_day) as full_days,
                    SUM(half_day) as half_days, SUM(early_going) as early_going_days,
                    SUM(late_comer) as late_days, SUM(incorrect_entry) as incorrect_days');
        $query->where('emp_comp',$companyName );
        $query->where('emp_code', $authUserEmpcode);
        $query->whereMonth('entry_date', $month);
        $query->whereYear('entry_date', $year);
        
        $records = $query->groupBy('emp_code')->get();
        $loggedHours = ProjectEntry::select('users.employee_id',DB::raw("floor(sum(floor(project_entries.`log_hours`)) + sum(project_entries.`log_hours` - floor(project_entries.`log_hours`)) * 100.0/60) +
                            ((sum(floor(project_entries.`log_hours`)) + sum(project_entries.`log_hours` - floor(project_entries.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_logged_hours_bkp"),DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                        ->leftjoin('users','users.id','user_id')
                        ->where(function($q) use ($month,$year){
                            $q->where('project_entries.log_date', 'like', '%' . $year.'-'.sprintf("%02d",$month).'%');
                        })
                     ->where('users.company_id',Auth::user()->company_id)
                     ->groupBy('project_entries.user_id')
                    ->pluck('total_logged_hours','users.employee_id')->toArray();
        $loggedHours = array_filter($loggedHours);
        if(!empty($records)) {
            $total_days_in_month = date('t', mktime(0, 0, 0, $data['request_month'], 1, $data['request_year']));
            // $total_days_in_month = cal_days_in_month(CAL_GREGORIAN, $data['request_month'], $data['request_year']);
            
            $total_week_days = totalWeekDays($data['request_month'], $data['request_year']);

            $queryAttend = AttendanceDetail::selectRaw('attendances.emp_code, TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))), "%H:%i:%s") AS working_hours')
                        ->rightJoin('attendances', 'attendances.id', '=', 'attendance_details.attendance_id')
                        ->where('attendances.absent', NULL)
                        ->where('first_in','!=','00:00:00')
                        ->where('last_out','!=','00:00:00')
                        ->where('attendances.incorrect_entry', NULL);
            $queryAttend->where('attendances.emp_code', $authUserEmpcode);
            $queryAttend->where('attendances.emp_comp',$companyName );
            $queryAttend->whereMonth('attendances.entry_date', $month);
            $queryAttend->whereYear('attendances.entry_date', $year);
            $queryAttend->groupBy('attendance_details.attendance_id');

            $working_details = $queryAttend->get()->toArray();
            
            $total_days_qry = Attendance::select('entry_date');
            if (isset($request['search_month'])  || isset($request['search_month'])){
                if($request['search_month']) {
                    $total_days_qry->whereMonth('entry_date', $request['search_month']);
                }
                if($request['search_year']) {
                    $total_days_qry->whereYear('entry_date', $request['search_year']);
                }
            }else{
                $total_days_qry->where('entry_date','LIKE','%'.$current_years.'-'.$current_month.'%');
            }
            if($data['employee']){
                $total_days_qry->where('emp_code', $data['employee']);
            }
            $total_days_qry->where('emp_comp',$companyName );
            $total_days_qry->groupby('entry_date');
        }
        $total_days_array = $total_days_qry->get()->toArray();
        $total_days =0;
        foreach($total_days_array as $total_days_array_val){
            if(!Holiday::getHolidayDateList($total_days_array_val['entry_date'])){
                // $timestamp = strtotime($total_days_array_val['entry_date']);
                $date = $total_days_array_val['entry_date'];
                $isWeekend = Holiday::getWeekendDateList($date);
                // $weekday= date("l", $timestamp );
                // if ($weekday !="Saturday" && $weekday !="Sunday") {
                if(count($isWeekend) == 0){
                    $total_days++;
                }
            }
        }
        $resultArray = array();
        $resultArray['working_details'] = $working_details;
        $resultArray['records'] = $records;
        $resultArray['loggedHours'] = $loggedHours;
        return $resultArray;
        //echo "<pre>";print_R($resultArray);exit;
    }

    public function teamNonBillableLoggedHours($getTeamMember,$request)
    {
        $teamUserArray = array();
        foreach ($getTeamMember as $userListingVal){
            $teamUserArray[] = $userListingVal['id'];
        }
        $firstDate = date('Y-m-01');
        $lastDate = date('Y-m-d',strtotime("last day of this month"));
        if ($request->has('search_by_start_date') && $request->search_by_start_date != '') {
            $firstDate = $request->search_by_start_date;
        }
        if ($request->has('search_by_end_date') && $request->search_by_end_date != '') {
            $lastDate = $request->search_by_end_date;
        }
        $projectQuery = Project::leftJoin('project_tasks as pt','projects.id','pt.project_id')
                                ->whereIn('projects.id',[458,517,516,518])
                                ->groupBy('pt.id');
        
        $projectQuery->where('pt.task_start_date',  '>=', $firstDate);
        $projectQuery->where('pt.task_end_date',  '<=', $lastDate);
        $projectQuery->whereNull('pt.deleted_at');
        $projectQuery->whereIn('pt.user_id',  $teamUserArray);
        if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
            $projectQuery->where('pt.user_id',$request->search_by_user_name);
        }
        $projectQuery->leftjoin('project_entries as pe','pt.id','pe.task_id')
        ->whereNull('pe.deleted_at')
        ->select('pt.id','pt.task_name','pt.task_start_date','pt.task_end_date','projects.project_name','pt.estimated_hours','pt.user_id',DB::raw("sum(pe.`log_hours`) as total_logged_hours"));
        $projData = $projectQuery->sortable()->orderBy('pt.task_start_date', 'desc')->get();
        return $projData;
    }

    public function getNonBillableLoggedHours($getTeamMember)
    {
        $teamUserArray = array();
        foreach ($getTeamMember as $userListingVal){
            $teamUserArray[] = $userListingVal['id'];
        }
        $firstDate = date('Y-m-d',strtotime("first day of this month"));
        $lastDate = date('Y-m-d',strtotime("last day of this month"));
        $projectQuery = ProjectEntry::whereIn('user_id',$teamUserArray)
                                ->whereIn('project_id',[458,517,516,518])
                                ->whereNull('deleted_at')
                                ->where('log_date',  '>=', $firstDate)
                                ->where('log_date',  '<=', $lastDate)
                                ->select(DB::raw("sum(log_hours) as log_hours"))->pluck('log_hours')->toArray();
            $loggedHours = $projectQuery[0] != 0?$projectQuery[0]:0;
            return $loggedHours;
        
    }

    public function getBillableAssignedHours($getTeamMember)
    {
        $teamUserArray = array();
        foreach ($getTeamMember as $userListingVal){
            $teamUserArray[] = $userListingVal['id'];
        }
        $firstDate = date('Y-m-d',strtotime("first day of this month"));
        $lastDate = date('Y-m-d',strtotime("last day of this month"));
        $projectQuery = ProjectTasks::whereIn('user_id',$teamUserArray)
                                ->whereNotIn('project_id',[458,517,516,518])
                                ->whereNull('deleted_at')
                                ->where('task_start_date',  '>=', $firstDate)
                                ->where('task_end_date',  '<=', $lastDate)
                                ->select(DB::raw("sum(estimated_hours) as estimated_hours"))->pluck('estimated_hours')->toArray();
            $AssignedHours = $projectQuery[0] != 0?$projectQuery[0]:0;
            return $AssignedHours;
        
    }

    public function findAvgLoggedTime($comapny_id)
    {
        $setting = new Setting();
        $avg_log_time = null;
        $average_logged_time = $setting->getSettingAvgLoggedTime('average_logged_time',$comapny_id);
        if($average_logged_time != null){
            $avg_log_time = $average_logged_time;
        }
        return $avg_log_time != null ? $avg_log_time : config('constant.avg_log_limit');
    }

    public function findAvgWorkingTime($comapny_id)
    {
        $setting = new Setting();
        $avg_working_time = null;
        $average_working_time = $setting->getSettingAvgWorkingTime('average_working_time',$comapny_id);
        if($average_working_time != null){
            $avg_working_time = $average_working_time;
        }
        return $avg_working_time != null ? $avg_working_time : config('constant.avg_working_time');
    }
}
