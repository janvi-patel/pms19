<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TimeEntryStatus extends Model
{
    use SoftDeletes;
    protected $table = "time_entry_status";
}
