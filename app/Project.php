<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Project extends BaseModel
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'projects';

    public $sortable = ['project_name',
        'project_start_date',
        'project_status',
        'client.first_name',
        'accountmanager.first_name',
        'projectmanager.first_name',
        'company.company_name',
        'assignedCompany.company_name',
        'project_end_date',
        'created_at',
        'updated_at',
        'invoice_status',
        'task_entries.log_hours'];

    public function projecttype() {
        return $this->hasOne(ProjectType::class, 'id', 'project_type');
    }

    public function accountmanager() {
        return $this->hasOne(User::class, 'id', 'account_manager')->select(array('*', DB::raw('CONCAT(first_name, " ", last_name) as account_manager_name')));
    }

    public function projectmanager() {
        return $this->hasOne(User::class, 'id', 'project_manager')->select(array('*', DB::raw('CONCAT(first_name, " ", last_name) as project_manager_name')));
    }

    public function company() {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function currency() {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function assignedCompany() {
        return $this->hasOne(Company::class, 'id', 'assigned_to');
    }

    public function projectuser() {
        return $this->hasMany(ProjectUser::class, 'project_id', 'id');
    }
    public function projectcost() {
        return $this->hasMany(ProjectCostInfo::class, 'project_id', 'id');
    }
     public function projectcr() {
        return $this->hasMany(Cr::class, 'project_id', 'id')->with('crcostinfo');
    }

    public function getProjectCr() {
        return $this->hasMany(Cr::class, 'project_id', 'id');
    }

    public function client() {
        return $this->hasOne(Client::class, 'id', 'client_id')->select(array('*', DB::raw('CONCAT(first_name, " ", last_name) as client_name')));
    }

    public function projecttask() {
        return $this->hasMany(Task::class, 'project_id', 'id');
    }

    public function getProjectList($searchFilter = array()) {
        //$query = Project::select("projects.*", "project_types.project_type_name", DB::raw("CONCAT(u1.first_name,' ',u1.last_name) as account_manager_name"), DB::raw("CONCAT(u2.first_name,' ',u2.last_name) as project_manager_name"), "te.log_hours", DB::raw("SUM(te.log_hours) As total_logged_hours"));
        $query = Project::select("projects.*", "project_types.project_type_name", DB::raw("CONCAT(u1.first_name,' ',u1.last_name) as account_manager_name"), DB::raw("CONCAT(u2.first_name,' ',u2.last_name) as project_manager_name"), "te.log_hours",
                    DB::raw("floor(sum(floor(te.`log_hours`)) + sum(te.`log_hours` - floor(te.`log_hours`)) * 100.0/60) +((sum(floor(te.`log_hours`)) + sum(te.`log_hours` - floor(te.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_logged_hours"));
        $query->join('project_types', 'projects.project_type', '=', 'project_types.id');
        $query->leftJoin('users as u1', 'projects.account_manager', '=', 'u1.id');
        $query->leftJoin('users as u2', 'projects.project_manager', '=', 'u2.id');
        $query->leftJoin('tasks as t', 'projects.id', '=', 't.project_id');
        $query->leftJoin('task_entries as te', 'te.task_id', '=', 't.id');

        if (isset($searchFilter['project_type'])) {
            $query->where('project_type', $searchFilter['project_type']);
        }
        if (isset($searchFilter['project_status'])) {
            $query->where('project_status', $searchFilter['project_status']);
        }
        $userIDs = isset($searchFilter['user_ids']) ? $searchFilter['user_ids'] : array();
        if (isset($userIDs) && !empty($userIDs)) {
            $query->join("project_users", function($join) use($userIDs) {
                $join->on("project_users.project_id", "=", "projects.id")
                        ->whereIn("project_users.user_id", $userIDs);
            });
        }
        $query->groupBy('projects.id');
        $projectData = $query->get()->toArray();

        return $projectData;
    }

    public function getUserProjects($searchFilter = array()) {
        $projects = array();
        $userIDs = isset($searchFilter['user_ids']) ? $searchFilter['user_ids'] : array();
        if (isset($userIDs) && !empty($userIDs)) {
            //$query = Project::select("projects.*", "project_users.user_id as user_id", "project_types.project_type_name", DB::raw("CONCAT(u1.first_name,' ',u1.last_name) as account_manager_name"), DB::raw("CONCAT(u2.first_name,' ',u2.last_name) as project_manager_name"), "te.log_hours", DB::raw("SUM(te.log_hours) As total_logged_hours"));
            $query = Project::select("projects.*", "project_users.user_id as user_id", "project_types.project_type_name", DB::raw("CONCAT(u1.first_name,' ',u1.last_name) as account_manager_name"), DB::raw("CONCAT(u2.first_name,' ',u2.last_name) as project_manager_name"), "te.log_hours",
                    DB::raw("floor(sum(floor(te.`log_hours`)) + sum(te.`log_hours` - floor(te.`log_hours`)) * 100.0/60) +((sum(floor(te.`log_hours`)) + sum(te.`log_hours` - floor(te.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_logged_hours"));
            $query->join("project_users", function($join) use($userIDs) {
                $join->on("project_users.project_id", "=", "projects.id")
                        ->whereIn("project_users.user_id", $userIDs);
            });
            $query->join('project_types', 'projects.project_type', '=', 'project_types.id');
            $query->leftJoin('users as u1', 'projects.account_manager', '=', 'u1.id');
            $query->leftJoin('users as u2', 'projects.project_manager', '=', 'u2.id');
            $query->leftJoin('tasks as t', 'projects.id', '=', 't.project_id');
            $query->leftJoin('task_entries as te', 'te.task_id', '=', 't.id');
            $query->whereNull('project_users.deleted_at');

            if (isset($searchFilter['project_type'])) {
                $query->where('project_type', $searchFilter['project_type']);
            }
            if (isset($searchFilter['project_status'])) {
                $query->where('project_status', $searchFilter['project_status']);
            }

            $query->groupBy('projects.id');
            $projects = $query->get()->toArray();
        }
        return $projects;
    }

    public function getProjectDetails($projectID) {
        $project = Project::query()->with('projectuser');
        if ($projectID) {
            $project->where('id', $projectID);
        }
        $projectDetail = $project->first()->toArray();

        return $projectDetail;
    }

    public function getProjectData($projectID) {
        $project = Project::select("projects.*", "project_types.project_type_name", DB::raw("CONCAT(u1.first_name,' ',u1.last_name) as account_manager_name"), DB::raw("CONCAT(u2.first_name,' ',u2.last_name) as project_manager_name"))->with('projectuser');
        $project->join('project_types', 'projects.project_type', '=', 'project_types.id');
        $project->leftJoin('users as u1', 'projects.account_manager', '=', 'u1.id');
        $project->leftJoin('users as u2', 'projects.project_manager', '=', 'u2.id');
        if ($projectID) {
            $project->where('projects.id', $projectID);
        }
        $projectDetail = $project->first()->toArray();

        return $projectDetail;
    }

    public function delete() {
        $this->projectuser()->delete();
        $this->projecttask()->delete();

        return parent::delete();
    }
    public function getTotalActiveProject($company_id = ''){
        $query = Project::select(DB::raw('COUNT(id) AS totalProject'));
        if(isset($company_id) && $company_id){
            $query->where('company_id','=',$company_id);
        }
         $query->whereNull('deleted_at');
         $query->where('project_status','=',1);
         $projects = $query->get()->toArray();
        return $projects;
    }
    public function getTotalClosedProject($company_id = ''){
        $query = Project::select(DB::raw('COUNT(id) AS totalProject'));
        $query->whereNull('deleted_at');
        if(isset($company_id) && $company_id){
            $query->where('company_id','=',$company_id);
        }
        $query->where('project_status','=',3);
        $projects = $query->get()->toArray();
        return $projects;
    }

    public function getOpenProjectList($company_id = ''){
        $query = Project::select('projects.project_name','project_types.project_type_name');
        $query->join('project_types', 'projects.project_type', '=', 'project_types.id');
        if(isset($company_id) && $company_id){
            $query->where('projects.company_id','=',$company_id);
        }
        $query->whereNull('projects.deleted_at');
        $query->where('projects.project_status','=',1);
        $projects = $query->get()->toArray();
        return $projects;
    }

}
