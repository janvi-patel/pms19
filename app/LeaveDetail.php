<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveDetail extends BaseModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "leave_group_id", "user_id", "approver_id", "is_adhoc", "reason", "approver_comment", "approver_date", "leave_status", "return_date", "leave_date", "leave_type", "leave_days", "created_at", "updated_at", "deleted_at",
    ];

    function createLeaveDetail($leave) {
        $leaveData = array();
        $leaveDetail = static::where("leave_group_id",$leave['id'])->get()->toArray();
        if(count($leaveDetail) <= 0) {
            $obj_leave = new Leave;
            $leave_start_date = $obj_leave->getNextWorkingDate(["date" => $leave['leave_start_date']]);
            $leaveData = [
                "leave_group_id"   => $leave['id'],
                "user_id"          => $leave['user_id'],
                "approver_id"      => $leave['approver_id'],
                "is_adhoc"         => $leave['is_adhoc'],
                "reason"           => $leave['reason'],
                "approver_comment" => $leave['approver_comment'],
                "approver_date"    => $leave['approver_date'],
                "leave_status"     => $leave['leave_status'],
                "created_at"       => $leave['created_at'],
                "updated_at"       => $leave['updated_at'],
                "deleted_at"       => $leave['deleted_at'],
                "return_date"      => $leave_start_date,
            ];
            if($leave['leave_days'] && $leave['leave_days'] > 1) {
                for($i = 1; $i <= ceil($leave['leave_days']); $i++ ) {
                    $summary = [
                        'start_date'      => date("Y-m-d",strtotime($leaveData['return_date'])),
                        'end_date'        => date("Y-m-d",strtotime($leaveData['return_date'])),
                        'start_type'      => 0,
                        'end_type'        => 0,
                    ];
                    if($leave['leave_start_type'] != 0 && $i == 1) {
                        $summary['start_type']   = $leave['leave_start_type'];
                        $summary['end_type']     = $leave['leave_start_type'];
                    } else if($leave['leave_end_type'] != 0 && $i == ceil($leave['leave_days'])) {
                        $summary['start_type']  = $leave['leave_end_type'];
                        $summary['end_type']    = $leave['leave_end_type'];
                    }
                    $summaryDetails = getLeaveSummary($summary);
                    $leaveData["leave_date"]    = $leaveData['return_date'];
                    $leaveData['return_date']   = date("Y-m-d", strtotime($summaryDetails['return_date']));
                    $leaveData["leave_type"]    = $summary['start_type'];
                    $leaveData["leave_days"]    = $summaryDetails['leave_days'] > 0 ? $summaryDetails['leave_days'] : 0;
                    static::create($leaveData);
                }
            } else {
                if($leave['leave_days'] > 0) {
                    $summary = [
                        'start_date'      => date("Y-m-d",strtotime($leaveData['return_date'])),
                        'end_date'        => date("Y-m-d",strtotime($leaveData['return_date'])),
                        'start_type'      => 0,
                        'end_type'        => 0,
                    ];
                    if($leave['leave_start_type'] != 0) {
                        $summary['start_type']   = $leave['leave_start_type'];
                    } else if($leave['leave_end_type'] != 0) {
                        $summary['end_type']    = $leave['leave_end_type'];
                    } else {
                        if($leave['leave_days'] < 1 && $leave['leave_days'] > 0){
                            $summary['start_type'] = 1;
                            $summary['end_type'] = 1;
                            $leave['leave_start_type'] = 1;
                        }
                    }
                    $summaryDetails = getLeaveSummary($summary);
                    $leaveData["leave_date"]        = $leaveData['return_date'];
                    $leaveData["leave_type"]        = $leave['leave_start_type'];
                    $leaveData["leave_days"]        = $summaryDetails['leave_days'] > 0 ? $summaryDetails['leave_days'] : 0;
                    $leaveData["return_date"]       = date("Y-m-d", strtotime($summaryDetails['return_date']));;
                    static::create($leaveData);
                }
            }
        }

        return true;
    }

    function updateLeaveDetail($leave) {
        $leaveDetail = static::where("leave_group_id",$leave['id'])->get()->toArray();
        if(count($leaveDetail) > 0) {
            $leaveData = array(
                'approver_comment' => $leave['approver_comment'],
                'leave_status' => $leave['leave_status'],
                'approver_id' => $leave['approver_id'],
                'approver_date' => $leave['approver_date'],
            );
            static::where("leave_group_id",$leave['id'])->update($leaveData);
        } else {
          //  dd($leave);
            $this->createLeaveDetail($leave);
        }

        return true;
    }
}
