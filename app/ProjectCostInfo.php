<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ProjectCostInfo extends Model
{
    use Sortable;
    public $table = 'project_cost_info';
}
