<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Http\Controllers\AttendancesController;
use Illuminate\Support\Facades\Auth;

class ProjectEntry extends BaseModel
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'project_entries';

    public function projectDetails() {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    public function users() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    //public $sortable = ['log_date', 'log_hours','first_name'];

    public function isTaskEntryIsValid($task_id,$project_id,$log_date)
    {
        $task = ProjectTasks::where('id',$task_id)
                    ->where('project_id',$project_id)
                    ->where('user_id',Auth::user()->id)
                    ->whereNotNull('task_start_date')
                    ->where('task_start_date','<=',$log_date)
                    ->where('task_end_date','>=',$log_date)
                    ->get();

        if(count($task)>0)
        {
            return true;
        }
        else{
            return false;
        }
    }
    
}
