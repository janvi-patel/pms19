<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Auth;
class LeadAttachment extends Model
{
    use SoftDeletes;
    use Sortable;
    protected $table = "lead_attachments";

    public function leadAttachedFileCount($id = ''){
        $leadCount = LeadAttachment::where('lead_id',$id)
        ->whereNull('deleted_at')
        ->whereNull('chat_id')
        ->whereNull('docs_type')->count();
        return $leadCount;
    }

    public function leadEstimationFileCount($id = ''){
        $leadCount = LeadAttachment::where('lead_id',$id)
        ->whereNull('deleted_at')
        ->whereNull('chat_id');
        if(Auth::user()->hasRole(config('constant.account_manager_slug'))){
            $leadCount = $leadCount->where('show_to_bde',1);
        }
        $leadCount = $leadCount->whereNotNull('docs_type')->count();
        return $leadCount;
    }
}