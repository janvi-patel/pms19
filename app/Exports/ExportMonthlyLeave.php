<?php

namespace App\Exports;

use App\Project;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportMonthlyLeave implements FromView,WithEvents,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($dates,$days,$weekOff,$excelData,$userLeaveData) {
        $this->dates = $dates;
        $this->days = $days;
        $this->weekOff = $weekOff;
        $this->excelData = $excelData;
        $this->userLeaveData = $userLeaveData; 
       
    }
    public function view(): View
    {
        return view('pms.reports.monthly_leave_excel_export', [
            'dates'=>$this->dates,
            'days'=>$this->days,
            'weekOff'=>$this->weekOff, 
            'excelData'=>$this->excelData,
            'userLeaveData'=>$this->userLeaveData,
            
        ]);
    }
    public function registerEvents(): array
    {
       
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:AZ100',
                    [
                        //Set font style
                        'font' => [
                            'size'      =>  10,
                        ],
                    ]
                );
            },
        ];
    }
}
