<?php

namespace App\Exports;

use App\Project;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportFailedTimeEntryStatus implements FromView,WithEvents,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($exportStatus) {
        $this->exportStatus = $exportStatus;
    }
    public function view(): View
    {
        return view('pms.timesheet.csv_export_time_sheet_status', [
            'exportStatus'=>$this->exportStatus,
        ]);
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:AZ100',
                    [
                        //Set font style
                        'font' => [
                            'size'      =>  10,
                        ],
                    ]
                );
            },
        ];
    }
}
