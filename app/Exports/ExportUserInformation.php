<?php

namespace App\Exports;

use App\Project;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportUserInformation implements FromView,WithEvents,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($usersData, $getAllUserDataArray, $searchFilter, $request, $companyId, $editReporting) {
        $this->usersData = $usersData;
        $this->getAllUserDataArray = $getAllUserDataArray;
        $this->searchFilter = $searchFilter;
        $this->companyId = $companyId;
        $this->editReporting = $editReporting;
        $this->request = $request;
    }
    public function view(): View
    {
        return view('pms.users.user_information_excel_export', [
            'usersData' => $this->usersData,
            'getAllUserDataArray'=> $this->getAllUserDataArray,
            'searchFilter'=> $this->searchFilter,
            'companyId' => $this->companyId,
            'editReporting' => $this->editReporting,
            'request' => $this->request,
        ]);
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:AZ100',
                    [
                        //Set font style
                        'font' => [
                            'size'      =>  10,
                        ],
                    ]
                );
            },
        ];
    }
}
