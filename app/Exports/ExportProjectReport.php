<?php

namespace App\Exports;

use App\Project;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportProjectReport implements FromView,WithEvents,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
     public function __construct($projectRowCount,$projectLoggedArray,$projectReportArray,$projectCrHours,$projectEmpLoggedArray,$request,$projectEstimatedArray) {
        $this->projectLoggedArray = $projectLoggedArray;
        $this->projectReportArray = $projectReportArray;
        $this->projectCrHours = $projectCrHours;
        $this->projectEmpLoggedArray = $projectEmpLoggedArray;
        $this->projectRowCount = $projectRowCount;
        $this->request = $request;
        $this->projectEstimatedArray = $projectEstimatedArray;
    }
    public function view(): View
    {
        return view('pms.reports.project_report_excel_export', [
            'projectLoggedArray' => $this->projectLoggedArray,
            'projectReportArray'=> $this->projectReportArray,
            'projectCrHours'=> $this->projectCrHours,
            'projectEmpLoggedArray' => $this->projectEmpLoggedArray,
            'projectRowCount' => $this->projectRowCount,
            'projectEstimatedArray' => $this->projectEstimatedArray,
            
        ]);
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:H1',
                    [
                        //Set font style
                        'font' => [
                            'size'      =>  13,
                            'bold'      =>  true,
                        ],
                    ]
                );
            },
        ];
    }
}
