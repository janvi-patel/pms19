<?php

namespace App\Exports;

use App\Project;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportTimeSheet implements FromView,WithEvents,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($dates,$days,$excelData,$companyName) {
        $this->dates = $dates;
        $this->days = $days;
        $this->excelData = $excelData; 
        $this->companyName = $companyName;
    }
    public function view(): View
    {
        return view('pms.reports.time_sheet_excel_export', [
            'dates'=>$this->dates,
            'days'=>$this->days,
            'excelData'=>$this->excelData,
            'companyName' => $this->companyName,
            
        ]);
    }
    public function registerEvents(): array
    {
       
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:AZ100',
                    [
                        //Set font style
                        'font' => [
                            'size'      =>  10,
                        ],
                    ]
                );
            },
        ];
    }
}
