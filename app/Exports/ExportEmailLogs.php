<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class ExportEmailLogs implements FromView,WithEvents,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($logs,$request) {
        $this->logs = $logs;
        $this->request = $request;
    }
    public function view(): View
    {
        return view('pms.salary_email_log.logs_excel_export', [
            'logs' => $this->logs,
            'request' => $this->request,
        ]);
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:AZ100',
                    [
                        //Set font style
                        'font' => [
                            'size'      =>  10,
                        ],
                    ]
                );
            },
        ];
    }
}
