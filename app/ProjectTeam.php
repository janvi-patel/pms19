<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Jobs\MailJob;
use Auth;
use App\Helpers\CommonHelper;
use DB;
use Exception;
class ProjectTeam extends Model
{
	use SoftDeletes;
	
    protected $table = 'project_team';

    protected $fillable = [
        'project_id', 'user_id'
    ];

    public function createTeam($pro_id,$user_id){
        if(Auth::check() && Auth::user()->hasPermission('create.project.team')){
            $companyId = Auth::user()->company_id;
            $team = ProjectTeam::where('project_id',$pro_id)->get()->pluck('user_id')->toArray();
            if(in_array($user_id,$team))
            {
                return true;
            }
            else{
                try{
                    $obj             = new ProjectTeam();
                    $obj->project_id = $pro_id;
                    $obj->company_id = $companyId;
                    $obj->user_id    = $user_id;
                    $obj->save();
                    //email to devloper
                    $projectData = Project::where('projects.id',$pro_id)
                    ->leftJoin('users as teamLeader','teamLeader.id','=','projects.team_leader')
                    ->select('project_name','project_start_date','project_end_date',DB::raw('CONCAT(teamLeader.first_name, " ", teamLeader.last_name) as team_lead_name'))->first()->toArray();
                    if(count($projectData)>0){
                        $data['team_lead_name']=isset($projectData['team_lead_name'])?$projectData['team_lead_name']:"";                       
                        $data['project_name']=$projectData['project_name'];
                        $data['project_start_date']=$projectData['project_start_date'];
                        $data['project_end_date']=$projectData['project_end_date'];
                        $userData = User::select('email','first_name')
                        ->where('id','=',$user_id)->first()->toArray();
                        if(count($userData)>0){
                            $companyEmailData =  CommonHelper::getCompanyDetails($companyId);                  
                            $to = $userData['email'];
                            $template = 'emails.project_assigned_developer';
                            $subject = 'Project assigned';
                            $from['email'] = $companyEmailData->from_address;
                            $from['name'] = $companyEmailData->company_name;
                            $data['first_name'] = $userData['first_name'];
                            $data['regards'] = $companyEmailData->company_name;
                            MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []);
                        }
                    }
                    return true;                
                }catch(Exception $e){
                    return false;
                }
            }
        }else{
           return false;
        }
    }
}
