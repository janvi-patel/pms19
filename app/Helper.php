<?php

use App\User;
use App\Leave;
use App\AttendanceDetail;
use App\UserWorkFromHome;
use App\Company;
use App\Attendance;
use App\Project;
use App\CompOff;

if (!function_exists('getLeaveSummary')) {
    function getLeaveSummary($data) {
        $datediff = 0;
        $startDate = isset($data['start_date']) ? date("Y-m-d", strtotime($data['start_date'])) : '';
        $endDate = isset($data['end_date']) ? date("Y-m-d", strtotime($data['end_date'])) : '';

        $leave = new Leave;
        $LeaveTotalDates = (new \App\Helpers\CommonHelper)->getAllDates($data['start_date'],$data['end_date']);

        foreach($LeaveTotalDates as $LeaveDate) {
            $isHoliday = $leave->isHoliday($LeaveDate);
            if(!$isHoliday) {
                $datediff++;
            }
        }

        $leaveDays = $datediff;
        $startType = isset($data['start_type']) ? intval($data['start_type']) : '';
        $endType = isset($data['end_type']) ? intval($data['end_type']) : '';
        if($datediff == 1 && ($startType != 0 || $endType != 0)) {
            $leaveDays = abs($leaveDays - 0.5);
        } else {
            if($startType != 0) {
                $leaveDays = $leaveDays - 0.5;
            }
            if($endType != 0) {
                $leaveDays = $leaveDays - 0.5;
            }
        }
        if($endType == 1) {
            $returnDate = date('Y-m-d', strtotime($endDate));
        } else {
            $returnDate = $leave->getNextWorkingDate(["date" => $endDate,"type" => "next_working_day"]);
        }
        $returnData['return_date'] = $returnDate;
        $returnData['leave_days'] = $leaveDays;

        return $returnData;
    }
}

if(!function_exists('getUsersFinancialYearStartDate')) {
    function getUsersFinancialYearStartDate($userId = 0){
        $userDetails = User::find($userId);
        $start_date = date('Y-m-d',strtotime(date('01-04-Y')));
        if(date('m-d',strtotime($start_date)) > date('m-d'))
        {
            $year = date("Y", strtotime("-1 years"));
            $start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
        }
        //  if(isset($userDetails->confirmation_date) && (strtotime($start_date) < strtotime($userDetails->confirmation_date)))
        // {
        //     $start_date = $userDetails->confirmation_date;
        // }
        if(!isset($userDetails->confirmation_date) && $userDetails->employee_status == 2) {
            $start_date = date('Y-m-d',strtotime($userDetails->joining_date));
        } else if(isset($userDetails->confirmation_date) && (strtotime($start_date) < strtotime($userDetails->confirmation_date))) {
            $start_date = $userDetails->confirmation_date;
        }
        return $start_date;
    }
}

if(!function_exists('getUsersCompoffData')) {
function getUsersCompoffData($compoffdata){
    $compoffleave = 0;

        if(!empty($compoffdata)){

            foreach($compoffdata as $key=>$compoff){
                if($compoff->leave_used != null){
                    $comma = ',';
                    if(strpos($compoff->leave_used,$comma) == true ) {

                        $compoffArry = explode(",",$compoff->leave_used);
                        // dd($compoffArry);
                        $compoffArryCount = count($compoffArry);
                        $i = 0;
                            foreach($compoffArry as $key=>$cof){
                                if($cof == '0'){
                                if($i == 0){
                                    if($compoff->compoff_start_type == 0){
                                        $compoffleave++;
                                    } else {
                                        $compoffleave = $compoffleave + 0.5;
                                    }
                                }else if($key == ($compoffArryCount-1)){
                                    if($compoff->compoff_end_type == 0){
                                        $compoffleave++;
                                    }else {
                                    $compoffleave = $compoffleave + 0.5;
                                    }
                                }else{
                                   $compoffleave++;
                                }
                            }
                            $i++;
                        }
                    }else{
                        if($compoff->leave_used==0){
                            $compoffleave++;
                        }
                    }
                }
            }
        }
        return $compoffleave;
    }
}

if(!function_exists('getUsersAvailableLeave')) {
    function getUsersAvailableLeave($array = []) {
        $userData = User::find($array['where']['id']);
        if(!(isset($userData['confirmation_date']) && $userData['confirmation_date'] != '' && strtotime(date("Y-m",strtotime($userData['confirmation_date']))) <= strtotime(date("Y-m",strtotime($array['where']['start_date']))))) {
            if($userData['employee_join_as'] == 0) {
                $to = \Carbon\Carbon::createFromFormat('Y-m', date("Y-m",strtotime($userData['joining_date'])));
                $from = \Carbon\Carbon::createFromFormat('Y-m', date("Y-m",strtotime($userData['confirmation_date'])));
                $diffInMonths = $to->diffInMonths($from);
                if($diffInMonths <= 6 && $diffInMonths > 0){
                    $userData['available_leaves'] = config("constant.assign_leave.trainee")[$diffInMonths];
                } else {
                    $userData['available_leaves'] = config("constant.assign_leave.trainee")[6];
                }
            } else if($userData['employee_join_as'] == 1) {
                $userData['available_leaves'] = config("constant.assign_leave.probation");
            }
        }
        return $userData['available_leaves'];
    }
}

if(!function_exists('displayDate')){
    function displayDate($date) {
        $returnDate = date('d-m-Y', strtotime($date));
        return $returnDate;
    }
}

if(!function_exists('totalWeekDays')){
    function totalWeekDays($month,$year)
    {
        $sundays=0;
        // echo date('m') . '==' . $month;exit;
        if(date('m') == $month)
            $total_days=date('d');
        else
            // $total_days=cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $total_days = date('t', mktime(0, 0, 0, $month, 1, $year));
        // echo $total_days;exit;
            for($i=1;$i<=$total_days;$i++)
        if(date('N',strtotime($year.'-'.$month.'-'.$i))==6 || date('N',strtotime($year.'-'.$month.'-'.$i))==7)
        $sundays++;
        return $sundays;
    }
}

if(!function_exists('getTimeEntryCompanyFromHelper')){
    function getTimeEntryCompanyFromHelper()
    {
        return App\Company::whereNull('deleted_at')->pluck('company_name','id')->toArray();
    }
}

if(!function_exists('checkTimeEntryFileHelper')){
    function checkTimeEntryFileHelper($row = '')
    {

        $entries = (explode(",",$row));
        $punchKey = 0;
        $dataArray =  array();
        if(isset($entries) && count($entries) > 0){
            $last_key = count($entries) - 1;
            foreach($entries as $key => $val)
            {
                if($val != ""){
                    if($key == 0){
                        if (strpos($val, 'Exit') !== false) {
                            $status = 'Exit';
                            $dataArray[$key]['incorrect_entry'] = $val;
                            $dataArray[$key]['incorrect_reason'] = "message";
                        } else {
                            $status = 'Entry';
                        }
                    } else {
                        $entry = (strpos($val, 'Entry') !== false) == true ? "Entry" : "Exit";
                        if ( $entry == $status ) {
                            $dataArray[$key]['incorrect_entry'] = $val;
                            $dataArray[$key]['incorrect_reason'] = "incorrect $entry.";
                        }
                        $status = $entry;
                    }
                    if($key != 0 && $last_key == $key){
                        if (strpos($val, 'Entry') !== false) {
                            $dataArray[$key]['incorrect_entry'] = $val;
                            $dataArray[$key]['incorrect_reason'] = "message";
                        }
                    }
                }
            }
        }
        return $dataArray;


    }
}

if(!function_exists('getUserInfo')){
    function getUserInfo($array = []){
        $user_info = User::with(['roles'=> function($q){$q->select('name','slug')->first();},'designation' => function($q){$q->select('id','designation_name');},'company' => function($q){$q->select('id','company_name','emp_short_name');}])->where($array['where'])->select($array['select'])->first();
        if(isset($user_info) && !empty($user_info)){
            return $user_info->toArray();
        }
        return array();
    }
}

if(!function_exists('checkReprtingPerson')){
    function checkReprtingPerson($user_id = ""){
        $isReprtingPerson = User::where('reporting_to',$user_id)->whereStatus('1')->get();
        if(!($isReprtingPerson->isEmpty()))
        {
            return 1;
        }else{
            return 0;
        }
    }
}

if(!function_exists('getTeamUserId')){
    function getTeamUserId($user_id = "",$status = ""){
        $getTeamUserId = [];
        $user = new User();
        $teamList = $user->getMyTeamList($user_id,$status);
        if(isset($teamList) && count($teamList) > 0){
            foreach($teamList AS $teamListVal){
                $getTeamUserId[] = $teamListVal['id'];
            }
        }
        return $getTeamUserId;
    }
}

if(!function_exists('getEmpListHasCompletedProbation')){
    function getEmpListHasCompletedProbation($company_id = '')
    {
        $EmpList = array();
        $user = User::query()->with('designation');
        if($company_id){
            $user->where('company_id',$company_id);
        }
        $user->where(['employee_status'=>2,'status'=>'1']);
        $user->whereNull('deleted_at');
        $user = $user->select('id','first_name','last_name','employee_status','designation_id','joining_date')->orderBy('joining_date', 'ASC')->get()->toArray();
        $setting = new App\Setting();
        $settingData = $setting->getSettingDetails(['where' => ['module' => 'probation_time'],'select' => ['constant_int_value']]);
        $probation_time = config("constant.probation_period");
        if(isset($settingData->constant_int_value) && $settingData->constant_int_value != "") {
            $probation_time = $settingData->constant_int_value;
        }
        if(isset($user) && count($user) > 0) {
            foreach($user as $key => $value) {
                $JoinDate = date('d',strtotime($value['joining_date']));
                if($JoinDate > "10"){
                    $month = $probation_time + 1;
                    $date = date('Y-m-d', strtotime('first day of - '.$month.' months'));
                    if(date('Y-m-d') > date('Y-m-d', strtotime('first day of '.$value['joining_date'].' + '.$month.' months'))){
                        $EmpList[$key] = [
                            'id' => $value['id'],
                            'first_name' => $value['first_name'],
                            'last_name' => $value['last_name'],
                            'employee_status' => $value['employee_status'],
                            'designation_id' => $value['designation_id'],
                            'joining_date' => $value['joining_date'],
                            'designation' => $value['designation']
                        ];
                    }
                } else {
                    $month = $probation_time;
                    $date = date('Y-m-d', strtotime('first day of + '.$month.' months'));
                    if(date('Y-m-d') > date('Y-m-d', strtotime('first day of '.$value['joining_date'].' + '.$month.' months'))){
                        $EmpList[$key] = [
                            'id' => $value['id'],
                            'first_name' => $value['first_name'],
                            'last_name' => $value['last_name'],
                            'employee_status' => $value['employee_status'],
                            'designation_id' => $value['designation_id'],
                            'joining_date' => $value['joining_date'],
                            'designation' => $value['designation']
                        ];
                    }
                }
            }
        }
        return $EmpList;
    }
}

if(!function_exists('getTakenLeaveCount')){
    
    function getTakenLeaveCount($user_id = "",$confirmation_date = ''){
        $userDetails = User::find($user_id);
        $start_date = date('Y-m-d',strtotime(date('01-04-Y')));
        if(date('m-d',strtotime($start_date)) > date('m-d'))
        {
            $year = date("Y", strtotime("-1 years"));
            $start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
        }
        //  if(isset($confirmation_date) && (strtotime($start_date) < strtotime($confirmation_date)))
        // {

        //         $start_date = $confirmation_date;
        // }
        if(!isset($userDetails->confirmation_date) && $userDetails->employee_status == 2) {
            $start_date = date('Y-m-d',strtotime($userDetails->joining_date));
        } else if(isset($userDetails->confirmation_date) && (strtotime($start_date) < strtotime($userDetails->confirmation_date))) {
            $start_date = $userDetails->confirmation_date;
        }
        $taken_leaves = Leave::where(['user_id' => $user_id,'leave_status' => 1])->where('leave_start_date' ,'>=', $start_date)->whereNull('deleted_at')->sum('leave_days');
        return $taken_leaves;
    }
}
if(!function_exists('getUserLeaveDetails')){
    function getUserLeaveDetails($array = []){
        $leave = Leave::whereNull('deleted_at');
        if(isset($array['where'])){
            $leave = $leave->where($array['where']);
        }
        $leave = $leave->first();
       if(!empty($leave)){
            return $leave;
       }
        return null;
    }
}
if(!function_exists('getUserPunchDataEntry')){
    function getUserPunchDataEntry($attendanceId){

        $attendanceDetail = AttendanceDetail::where('attendance_id', $attendanceId)->get();

        if (count($attendanceDetail) > 0) {
            return $attendanceDetail;
        } else {
            return array();
        }
    }
}
if(!function_exists('getUserWorkingHoursDetails')){
    function getUserWorkingHoursDetails($date,$empcode,$company_id){

        if(!empty($date) && !empty($empcode) && !empty($company_id)){
            $userAttendanceDetails = AttendanceDetail::rightjoin('attendances', 'attendance_details.attendance_id', '=', 'attendances.id')
                                    ->where('attendances.emp_code', $empcode)
                                    ->where('attendances.emp_comp', $company_id)
                                    ->where('attendances.entry_date', 'like', '%' . $date . '%')
                                    ->groupBy(['attendances.entry_date', 'attendance_details.attendance_id'])
                                    ->select(
                                            'attendance_details.attendance_id', 'attendances.id', 'attendances.first_in', 'attendances.last_out', 'attendances.late_comer', 'attendances.incorrect_entry', 'attendances.entry_date', 'attendances.full_day', 'attendances.half_day', 'attendances.early_going', 'attendances.absent',
                                    //DB::raw('TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))), "%h:%i:%s") AS working_hours'),
                                    //DB::raw('SUBTIME(TIMEDIFF( attendances.last_out, attendances.first_in), TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))), "%h:%i:%s")) as break_hours')
                                            DB::raw('SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))) AS working_hours'), DB::raw('SUBTIME(TIMEDIFF( attendances.last_out, attendances.first_in),SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time))))) as break_hours')
                                    )->first();
                                  // echo "fsfds<pre>";print_r($userAttendanceDetails);exit;
            return $userAttendanceDetails;
        }else{
            return null;
        }
    }
}
if(!function_exists('getUserLwpLeave')){
    function getUserLwpLeave($startMonth = '',$startYear = '', $userId = ''){
        if(!empty($startYear) && !empty($startMonth) && !empty($userId)){
            $startDate = '01-'.$startMonth.'-'.$startYear;
            $endDate = date("Y-m-t", strtotime($startDate));


            $lwpLeaveDetails = Leave::where('user_id',$userId)->where('leave_status','!=',1)->where('leave_end_date' ,'<=', $endDate)->where('leave_start_date','>=', $startDate)->whereNull('deleted_at')->get();
            return count($lwpLeaveDetails);
        } else {
            $start_date = date('Y-m-d',strtotime(date('01-04-Y')));
            if(date('m-d',strtotime($start_date)) > date('m-d'))
            {
                $year = date("Y", strtotime("-1 years"));
                $start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
            }

            $query = Leave::where('leave_status','!=',1)->where('leave_end_date' ,'<', date('Y-m-d'))->where('leave_start_date','>', $start_date)->whereNull('deleted_at');
            if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug'))){
                $query = $query->with(['user'])->whereHas('user', function($q) {  $q->where(['company_id'=> Auth::user()->company_id,'status' => 1]); });
            } else {
                $query = $query->where('user_id',Auth::user()->id);
            }
            $lwpLeaveDetails = $query->get();
            $countLeaveDays = $query->sum('leave_days');
            $data['lwpLeaveDetails'] = $lwpLeaveDetails;
            $data['lwpLeaveCount'] = $countLeaveDays;
            return $data;
        }
    }
}

if(!function_exists('getUsersPm')) {
    function getUsersPm($id = "") {
        static $pm_email = false;
        if($id != "" || $id != "0"){
            $query = User::find($id);
            if(isset($query->reporting_to) && $query->reporting_to != "") {
                $is_pm = isProjectManager($query->reporting_to);
                if($is_pm == false){
                    getUsersPm($query->reporting_to);
                } else {
                    $pm_email = $is_pm;
                }
            }
        }
        return $pm_email;
    }
}

if(!function_exists('isProjectManager')) {
    function isProjectManager($user_id) {
        $query = User::select('users.email','rs.role_id as role_id','rl.slug','users.id as uid')->leftJoin('role_user As rs','rs.user_id','=','users.id')->leftJoin('roles as rl','rl.id','=','rs.role_id')->where('users.id',$user_id)->first();
        if(isset($query['slug']) && $query['slug'] == "pm") {
            return $query['email'];
        } else {
            return false;
        }
    }
}

if(!function_exists('getUserMonthlyLeaveDetails')){
    function getUserMonthlyLeaveDetails($array =  []) {
        $getleave = Leave::where($array['where'])->where(function ($q) use ($array) {
            $q->where(function ($q) use ($array) {
                $q->where('leave_start_date', '>=', $array['between']['start_date'])
                ->where('leave_start_date', '<=', $array['between']['end_date']);
            })->orWhere(function ($q) use ($array) {
                $q->where('leave_end_date', '<=', $array['between']['end_date'])
                    ->where('leave_end_date', '>=', $array['between']['start_date']);

            });
        })->get();
       $approvedLeave = $pendingLeave = $lwp = 0;

        foreach ($getleave as $key => $value) {
            for($i = strtotime($value->leave_start_date) ; $i <= strtotime($value->leave_end_date) ;$i = $i + 86400 ) {
                $thisDate = date( 'Y-m-d', $i );
                if(strtotime($thisDate) >= strtotime($array['between']['start_date']) && strtotime($thisDate) <= strtotime($array['between']['end_date'])){

                    if(strtotime($thisDate) == strtotime($value->leave_start_date)){
                        if($value->leave_start_type != 0){
                            $number = 0.5;
                        }else{
                            $number = 1;
                        }
                    }elseif (strtotime($thisDate) == strtotime($value->leave_end_date)) {
                        if($value->leave_end_type != 0){
                            $number = 0.5;
                        }else{
                            $number = 1;
                        }
                    }else{
                        $number = 1;
                    }

                    if(strtotime($thisDate) < strtotime(date('Y-m-d'))){
                        $leave = new Leave();
                        $data = $leave->isHoliday($thisDate);
                        if($data !=1){
                            if($value->leave_status == 1){
                                $approvedLeave = $approvedLeave + $number;
                            }
                            if($value->leave_status == 2){
                               $lwp = $lwp + $number;
                            }
                        }
                    }else{
                        $leave = new Leave();
                        $data = $leave->isHoliday($thisDate);
                        if($data !=1){
                            if($value->leave_status == 1){
                                $approvedLeave = $approvedLeave + $number;
                            }
                            if($value->leave_status == 2){
                                $pendingLeave = $pendingLeave + $number;
                            }
                        }
                    }
                }
            }
        }
        $mailData['approvedLeave'] = $approvedLeave;
        $mailData['pendingLeave'] = $pendingLeave;
        $mailData['lwp'] = $lwp;
        return $mailData;

    }
}
if(!function_exists('isWorkingFromHome')){
    function isWorkingFromHome($user_id,$date) {
        $data = UserWorkFromHome::where('user_id',$user_id)->where('start_date','<=',$date)->where('end_date','>=',$date)->get()->toArray();
        if(count($data) > 0){
            return true;
        }
        return false;
    }
}
if(!function_exists('userListingWithJoiningDateOnly')){
    function userListingWithJoiningDateOnly($company_id = '') {
        $currentMonth = date('m');
        $currentDay = date('d');
        $month = ($currentMonth == 12)?0:$currentMonth;
        $total = 15;
        $currenMonthBrd = User::select('joining_date','id','first_name','last_name')
                    ->where('company_id',$company_id)->where('status',1)
                    ->whereMonth('joining_date','=',$currentMonth)->whereDay('joining_date', '>=', $currentDay)
                    ->whereNotNull('joining_date')
                    ->orderByRaw('DATE_FORMAT(joining_date, "%m-%d")')->get()->toArray();

        $upcomingBrd = User::select('joining_date','id','first_name','last_name')
                        ->where('company_id',$company_id)
                        ->whereMonth('joining_date','>',$month)->whereNotNull('joining_date')
                        ->where('status',1)->orderByRaw('DATE_FORMAT(joining_date, "%m-%d")')->get()->toArray();

        $userList = array_slice(array_merge($currenMonthBrd,$upcomingBrd),0,$total) ;
        return $userList;
    }
}

if(!function_exists('getProjectPmsHours')){
    function getProjectPmsHours($projectId) {
        return Project::leftjoin('cr',function($leftJoin)
            {
                $leftJoin->on('projects.id', '=', 'cr.project_id')->whereNull('cr.deleted_at');
            })
        ->select('projects.pms_hours','projects.id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
        ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
        ->where('projects.id',$projectId)
        ->first();
    }

}

if(!function_exists('getLeaveCount')){
    function getLeaveCount($array = []) {
        $leave = new App\LeaveDetail();
        return $leave->getSum($array);
    }
}
if(!function_exists('getEarlyLeave')){
    function getEarlyLeave($array = []) {
        $leave = new App\EarlyLeave();
        return $leave->getList($array);
    }
}
if(!function_exists('getProjectTaskSum')){
    function getProjectTaskSum($array = []) {
        $task = new App\ProjectTasks();
        return $task->getSum($array);
    }
}

if(!function_exists('getProjectTaskList')){
    function getProjectTaskList($array = []) {
        $task = new App\ProjectTasks();
        return $task->getList($array);
    }
}

if(!function_exists('getSettingModules')){
    function getSettingModules($array = []) {
        $setting = new App\Setting();
        return $setting->getDetails($array);
    }
}

if(!function_exists('getProjectEntrySum')){
    function getProjectEntrySum($array = []) {
        $projectEntry = new App\ProjectEntry();
        return $projectEntry->getSum($array);
    }
}

if(!function_exists('getCompOffCount')){
    function getCompOffCount($array = []) {
        $compOffCount = new CompOff();
        return $compOffCount->getSum($array);
    }
}

if(!function_exists('syncMontlyBalanceLeave')){
    function syncMontlyBalanceLeave($array = []) {
        $monthly_leave = new App\MonthlyLeaveBalance();
        $monthly_leave->createMontlyBalanceData($array);
    }
}

if(!function_exists('syncMontlyBillableReport')) {
    function syncMontlyBillableReport($array = []) {
        $monthly_leave = new App\BillableReport();
        $monthly_leave->createMontlyBillableReport($array);
    }
}

if(!function_exists('getProjectDetails')){
    function getProjectDetails($array = []) {
        $project = new App\Project();
        return $project->getDetails($array);
    }
}

if(!function_exists('getCrDetails')){
    function getCrDetails($array = []) {
        $cr = new App\Cr();
        return $cr->getDetails($array);
    }
}

if(!function_exists('getInternalCompanyList')){
    function getInternalCompanyList($array = []) {
        $cr = new App\InternalCompany();
        return $cr->getList($array);
    }
}