<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Kyslik\ColumnSortable\Sortable;
use Response;
//use Laravel\Passport\HasApiTokens;
use DB;
use Auth;
use App\RoleUser;
use App\Company;
use App\Jobs\MailJob;
use App\EmailTemplate;
use App\Holiday;
use Carbon\Carbon;

class User extends Authenticatable {

    use Sortable;
    use Notifiable;
    use SoftDeletes;
    use HasRoleAndPermission;
  //  use HasApiTokens;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $sortable = ['first_name', 'last_name','company_id', 'employee_id', 'email',
    'company.company_name', 'designation.designation_name', 'reportingto.first_name',
    'created_at', 'updated_at','department','employee_status','status','from_shift','confirmation_date','joining_date','billable_hour'];

    public $sortableAs = ['billable_hour','logged_hours','non_billable_hours','non_billable_logged_hours','billability_percentage'];

    public function designation() {
        return $this->hasOne(Designation::class, 'id', 'designation_id');
    }

    public function reportingto() {
        return $this->hasOne(User::class, 'id', 'reporting_to');
    }

    public function roleuser() {
        return $this->hasOne(RoleUser::class, 'user_id', 'id');
    }

    public function company() {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function getUserList() {
        $user = User::query()->with('designation', 'reportingto');
        $usersData = $user->orderBy('id', 'ASC')->get();

        return $usersData;
    }

    public function getUserName() {
        return  User::select('id', 'first_name', 'last_name')->orderBy('first_name', 'ASC')->get();
    }

    public function getUserNameByCompany($company_id= '') {
        $user = User::query();

        if(isset($company_id) && $company_id){
            $user->where('company_id','=', $company_id);
            $user->whereNull('deleted_at');
        }
        if(Auth::check() && (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('hr'))){
            $user->where('status','=', 1);
        }
        if(Auth::check() && (Auth::user()->hasRole('admin') || Auth::user()->hasRole('hr') || Auth::user()->hasRole('super_admin'))){
            $user->where(function ($query) {
            $query->where('status',1);
            $query->orWhere(function ($q) {
            $q->where('status',0);
            $q->whereBetween('last_date',[Carbon::now()->subMonth(2)->toDateString("Y-m-d"), Carbon::now()->toDateString("Y-m-d")]);
            });
        });
        }


        return $user->orderBy('first_name', 'ASC')->get();
    }

    public function getUserCompanyById($user_id) {
        return User::select('company_id')->where('id','=', $user_id)->get()->toArray();
    }

    public function getUserListByDesignation($designationID) {
        $user = User::query()->with('designation');
        if ($designationID) {
            $user->where('users.designation_id', $designationID);
        }
        $projectmanagerList = $user->orderBy('first_name', 'ASC')->get();
        return $projectmanagerList;
    }

    public function getTraineeList($company_id = '')
    {
        $user = User::query()->with('designation');
        if($company_id){
            $user->where('company_id',$company_id);
        }
        $user->where('designation_id',9);
        $user->whereNull('deleted_at');
        $user->where('status',1);
        $EmpList = $user->select('first_name','last_name','employee_status','designation_id','joining_date')->orderBy('first_name', 'ASC')->get()->toArray();
        return $EmpList;
    }

    public function getEmpListOnProbation($company_id = '')
    {
        $user = User::query()->with('designation');
        if($company_id){
            $user->where('company_id',$company_id);
        }
        $user->where('employee_status',2);
        $user->where('designation_id','!=',9);
        $user->whereNull('deleted_at');
        $user->where('status',1);
        $EmpList = $user->select('first_name','last_name','employee_status','designation_id','joining_date')->orderBy('first_name', 'ASC')->get()->toArray();
        return $EmpList;
    }

    public function getUserDetails($userID) {
        $user = User::query()->with('roleuser');
        if ($userID) {
            $user = $user->where('id', $userID)->first();
            if($user){
                $userDetail = $user->toArray();
                return $userDetail;
            }
        }
    }

    public function updateYearlyLeaves() {
        $user = User::query();
        $user->where('employee_status','=',1)
        ->update([ 'available_leaves' =>config('constant.yearlyTotalLeave'),'used_leaves'=>config('constant.yearlyTotalLeave')]);
    }

    public function updateUserUsedLeave($userId,$leaves){
        $user = User::query();
        $user->where('id', $userId)
        ->update(['used_leaves' => $leaves]);
    }

    public function updateUserAssignedLeave($userId,$leaves){
        $user = User::query();
        $user->where('id', $userId)
        ->update(['available_leaves' => $leaves]);
    }

    public function updateCompoff_RemainnigLeave($userId,$remaining_leave,$compoff_leave){
        $user = User::query();
        $user->where('id', $userId)
        ->update(['remaining_leave'=> $remaining_leave,'comp_off_leave'=> $compoff_leave]);
    }

    public function updateRemainnigLeave($userId,$remaining_leave){
        $user = User::query();

        $user->where('id', $userId)

        ->update(['remaining_leave'=> $remaining_leave]);
    }

    public function getUserDataByEmpCode($empCode,$cmp_id) {
        $user = User::select('users.id','users.first_name','users.last_name','users.email','users.personal_email','users.department','users.bank_name','users.bank_account_no','users.joining_date','users.pan_no','users.employee_status','d.designation_name');
        $user->leftJoin('designation as d', 'd.id','=','users.designation_id');
        if ($empCode) {
            $user->where('employee_id', $empCode);
        }
        if($cmp_id){
            $user->where('company_id',$cmp_id);
        }
        $user->where('status',1);
        $userDetail = $user->first();
        if ($userDetail) {
            $userDetail = $userDetail->toArray();
        }
        return $userDetail;
    }

    public function getReportingToList($userID = '') {
        $user = User::query();
        if ($userID) {
            $user->where('id', '!=', $userID);
        }
        $companyId = Auth::user()->company_id;
        if(!(Auth::user()->hasRole(config('constant.superadmin_slug')))){
            $user->where('company_id' , $companyId);
        }
        $usersData = $user->where('status',1)->orderBy('first_name', 'ASC')->get()->toArray();
        return $usersData;
    }

    public function getReportingToListWithOnlyTeamLeader($userID = '') {
        static $teamLeader;
        $teamLeader         = User::find($userID)->reporting_to;
        return $teamLeader;
    }

    public function getReportingToName(){
        if(Auth::user()->hasRole(config('constant.superadmin_slug'))){
            return User::select('id', 'first_name', 'last_name')->whereNotIn('designation_id',array(15))->where('status',1)
            ->orderBy('first_name', 'ASC')->get();
        }else{
            $companyId = Auth::user()->company_id;
            return User::select('id', 'first_name', 'last_name')
            ->where('users.company_id',$companyId)
            ->where('status',1)
            ->whereNotIn('designation_id',array(1,15))
            ->orderBy('first_name', 'ASC')
            ->get();
        }
    }

    public function getProjectUserList() {
        $user = User::query();

        $usersData = $user->orderBy('first_name', 'ASC')->get()->toArray();

        return $usersData;
    }

    public function getMyTeamList($userID,$status = "") {
        static $teamArray;
        $user = User::query()->with('designation');
        $user->where('reporting_to', '=', $userID);
        $user->where('company_id', '=', Auth::user()->company_id);
        if((Auth::check() && (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('hr'))) && $status == ""){
            $user->where('status',1);
            $user->whereNull('deleted_at');
        }
        $usersData = $user->orderBy('id', 'ASC')->get()->toArray();

        if (count($usersData) > 0) {
            foreach ($usersData as $userDetail) {
                $teamArray[$userDetail['id']]['id'] = $userDetail['id'];
                $teamArray[$userDetail['id']]['first_name'] = $userDetail['first_name'];
                $teamArray[$userDetail['id']]['last_name'] = $userDetail['last_name'];
                $teamArray[$userDetail['id']]['employee_id'] = $userDetail['employee_id'];
                $teamArray[$userDetail['id']]['reporting_to'] = $userDetail['reporting_to'];
                $teamArray[$userDetail['id']]['email'] = $userDetail['email'];
                $teamArray[$userDetail['id']]['phone'] = $userDetail['phone'];
                $teamArray[$userDetail['id']]['designation_id'] = $userDetail['designation_id'];
                $teamArray[$userDetail['id']]['designation_name'] = (isset($userDetail['designation']['designation_name']))?$userDetail['designation']['designation_name']:'';
                $this->getMyTeamList($userDetail['id']);
            }
        }

        $teamArray = array_sort($teamArray, 'first_name', SORT_ASC);
        return $teamArray;
    }

    public function getMyTeamLeader($userID) {
        static $teamArray;
        $user = User::query();
        $user->where('reporting_to', '=', $userID)->where('status',1);
        $usersData = $user->orderBy('id', 'ASC')->get()->toArray();
        if (count($usersData) > 0) {
            foreach ($usersData as $key => $userDetail) {
                $teamArray[$userDetail['id']]['id'] = $userDetail['id'];
                $this->getMyTeamLeader($userDetail['id']);
            }
        }

        return array_keys($teamArray);
    }

    public function getUserDropDown() {
        $user = User::query();
        if(Auth::check() && (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('hr'))){
            $user->whereNull('deleted_at');
        }
        $usersData = $user->orderBy('first_name', 'ASC')->get()->toArray();

        return $usersData;
    }

    public function getUserDetailsByFilter($filter = array()) {
        $user = User::query();
        if (isset($filter['id']) && $filter['id']) {
            $user->where('id', $filter['id']);
        }
        if (isset($filter['employee_id']) && $filter['employee_id']) {
            $user->where('employee_id', $filter['employee_id']);
        }

        $userDetail = $user->first()->toArray();

        return $userDetail;
    }

    public function getUserDetailsByEmail($email) {
        $user = User::query();
        if ($email) {
            $user->where('email', $email);
        }
        $userDetail = $user->first()->toArray();

        return $userDetail;
    }

    public function getAllUserList($userId = '') {
        $user = User::query();
        if($userId){
            $user->where('id',$userId);
        }
        $usersData = $user->orderBy('id', 'ASC')->get();
        return $usersData;
    }

    public function getAllUserListByCompany($company_id = '') {
        $user = User::query();
        if($company_id){
            $user->where('company_id',$company_id);
        }

        $usersData = $user->where('status',1)->orderBy('first_name', 'ASC')->get();
        return $usersData;
    }


    /**
     * calculate number of months between 2 dates
     * @param type $joiningDate
     * @param type $nextMonthDate
     * @return int total months
     */

    // get all asigned project Role (TL|PM);
    public function assignedProjectList(){
        return $this->hasMany('App\ProjectUser','user_id','id')->with('projectList');
    }

    // checked user ajax action
    public function checkedDelete($userIdArray) {

        $user = User::query()->whereIn('id',$userIdArray)->delete();
        return $user;

    }
    public function checkedUpdate($userIdArray,$action) {
        $user = User::query()->whereIn('id',$userIdArray)->update(['status' => $action]);
        return $user;
    }

    public function changeEmploymentStatus($userIdArray,$status){
        $count = 0;
        $userArray = array();
        if(isset($userIdArray) && (count($userIdArray)>0))
        {
            $user = User::query()->whereIn('id',$userIdArray)->where('employee_status','!=',$status)->get();
            foreach($user as $key => $userDetails)
            {
                $userDetails->employee_status = $status;
                $userDetails->save();
                if($status == 1){
                    $userArray[$key] = $userDetails;
                }
                $count++;
            }
        }
        if(count($userArray) > 0)
        {
            $this->sendEmpStatusConfirmMail($userArray);
        }
        return $count;
    }

    public function sendEmpStatusConfirmMail($userArray)
    {
        foreach($userArray as $userDetails)
        {
            $company     = new Company();
            $mailTemplate = new EmailTemplate();
            $userCompanyMailData  = $company->getCompanyViseMailData($userDetails['company_id'])[0];
            $data['regards'] = $userCompanyMailData['company_name'];
            $from['email'] = $userCompanyMailData['from_address'];
            $from['name']  = $userCompanyMailData['company_name'];
            $to = $userDetails['email'];
            $cc = "";
            $template = 'emails.employeement_status_changed';
            $mail_subject = 'Confirmation about you employeement status';
            $data['first_name'] = $userDetails['first_name'];
            $data['full_name'] = $userDetails['first_name'].' '.$userDetails['last_name'];
            $subject = '';
            $module = 'update_employment';
            $mailTemp = $mailTemplate->emailTemplate($userDetails['company_id'],$module);
            $data['temp'] = "";
            if($mailTemp != null)
            {
                $keyword = [
                    'first_name'        => $data['first_name'],
                    'full_name'         => $data['full_name'],
                    'company_name'      => $data['regards']
                ];
                $subject = $mailTemp['subject'];
                $data['temp'] = $mailTemplate->replaceKeyword($mailTemp['email_format'],$keyword);
            }
            $subject = $subject != ''?$subject:$mail_subject;
            MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []);
        }
    }
    public function getUserwithRole() {
        $query = User::select('users.id','users.employee_id','users.email','users.first_name', 'users.last_name','users.company_id');
        $query->leftJoin('role_user As rs','rs.user_id','=','users.id');

        $query->whereIn('rs.role_id',[3,4,5,7,9,10,11,12,13,14,15,17,20,22,23]);
        $query->where('users.status',1);
        $userIdArray = $query->orderBy('users.id', 'ASC')->get()->toArray();
        return $userIdArray;
    }

     public function getUserwithDepartment() {
        $query = User::select('users.id','users.employee_id','users.email','users.first_name', 'users.last_name','users.company_id');
        $query->whereIn('users.department',[1,3,6,7]);
        $userIdArray = $query->orderBy('users.id', 'ASC')->get()->toArray();
        return $userIdArray;
    }

    public function getUserwithRoleForLateComerReport() {
        $query = User::select('users.id','users.employee_id','users.email','users.first_name', 'users.last_name','users.company_id');
        $query->leftJoin('role_user As rs','rs.user_id','=','users.id');
        $query->where('users.status',1);
        $query = $query->whereIn('rs.role_id',[3,4,5,10,11,12,13,17,18]);
        return $query;
    }

    public function getReportingToEmail($id) {
        $query = User::select('reporting_to');
        $query = $query->where('id',$id)->get()->first();
        if($query){
            $tlId= $query->toArray();
            $tlId = $tlId['reporting_to'];
            $tlEmail = User::select('email')->where('id',$tlId)->get()->first();
            if($tlEmail){
                $tlEmail->toArray();
                return $tlEmail;
            }
        }
    }

    public function getPmTlEmail($id) {
        $query = User::select('reporting_to');
        $query = $query->where('id',$id)->get()->first();

        if($query){
            $tlId= $query->toArray();
            $tlId = $tlId['reporting_to'];
            $tlEmail = User::select('users.email','rs.role_id as role_id','rl.slug','users.id as uid')->leftJoin('role_user As rs','rs.user_id','=','users.id')->leftJoin('roles as rl','rl.id','=','rs.role_id')->where('users.id',$tlId)->get()->first();

            if($tlEmail['slug'] == 'pm')
            {

                $email['tl'] = Null;
                $email['pm'] = $tlEmail['email'];
                 return $email;
            }
            elseif($tlEmail['slug'] == 'tl')
            {

                 $pm_tl_Email =  User::select('reporting_to')->where('id',$tlEmail['uid'])->get()->first();
                  if($pm_tl_Email){
                    $pmId= $pm_tl_Email->toArray();
                    $pmId = $pmId['reporting_to'];
                    $pmEmail = User::select('email')->where('id',$pmId)->get()->first();
                    }
                  if($pmEmail){
                    $pmEmail->toArray();
                    $email['pm'] = $pmEmail['email'];
                    $email['tl'] = $tlEmail['email'];

                    return $email;
                  }
            }
            else
            {
                  $email['pm'] = Null;
                  $email['tl'] = Null;

                  return $email;

            }

        }
    }
    public function getUserForMailJobMonthly($companyId) {
        $query = User::select('joining_date','first_name','last_name','employee_status','company_id')->where('company_id',$companyId);
        $query = $query->get();
        if($query){
            $userData= $query->toArray();
            return $userData;
        }
    }

    public function getCompanyShortName ($companyId){
        $query = Company::select('emp_short_name')->where('id',$companyId)->get()->first();
        if($query){
            $CompanyShortName = $query->toArray()['emp_short_name'];
            return $CompanyShortName;
        }else{
            return '';
        }
    }

    public function getAllCompanyShortName (){
        $query = Company::select('emp_short_name')->get();
        if($query){
            $CompanyShortName = $query->pluck('emp_short_name')->toArray();
            return $CompanyShortName;
        }else{
            return '';
        }
    }

    public function getUsersHrAndAdminEmailWithCompany($companyId){
        $HrAndAdminEmail = Company::select('companies.hr_email_address','users.email')
                            ->leftjoin('users','companies.id','users.company_id')
                            ->where('companies.id',$companyId)
                            ->where('users.designation_id',1)->first();
        return $HrAndAdminEmail;
    }

    public function getUserByCompanyName($companyName){
        $user = User::leftjoin('companies','companies.id','users.company_id')
        ->where('companies.company_name',$companyName)
        ->where('users.status',1)
        ->whereNull('users.deleted_at')
        ->pluck('users.id')->toArray();
        return $user;
    }

    public function getUsersBirthDayOnPreviousHoliday($company_id = '')
    {
        $currentMonth = date('m');
        $currentDay = date('d');
        $month = ($currentMonth == 12)?0:$currentMonth;
        $total = 15;
        $currenMonthBrd = User::select('birthdate','id','first_name','last_name')
                    ->where('company_id',$company_id)->where('status',1)
                    ->whereMonth('birthdate','=',$currentMonth)->whereDay('birthdate', '>=', $currentDay)
                    ->whereNotNull('birthdate')
                    ->orderByRaw('DATE_FORMAT(birthdate, "%m-%d")')->get()->toArray();

        $upcomingBrd = User::select('birthdate','id','first_name','last_name')
                        ->where('company_id',$company_id)
                        ->whereMonth('birthdate','>',$month)->whereNotNull('birthdate')
                        ->where('status',1)->orderByRaw('DATE_FORMAT(birthdate, "%m-%d")')->get()->toArray();

        $userList = array_slice(array_merge($currenMonthBrd,$upcomingBrd),0,$total) ;
        return $userList;

    }
}
