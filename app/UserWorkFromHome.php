<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Support\Facades\Auth;

class UserWorkFromHome extends Model
{
    use SoftDeletes;

    protected $table = 'user_work_from_home';


}
