<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class SalaryMailLog extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = "upload_salary_email_log";
}
