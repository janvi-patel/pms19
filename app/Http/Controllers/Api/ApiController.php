<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Controllers\Api\BaseController as BaseController;
use JsValidator;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserCollection;
use App\Designation;
use App\Company;
use App\RoleUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use App\Imports\UsersLeaveImport;
use Excel;
use Sortable;
use App\Notifications\EmpStatusChange;
use Alert;
use Response;
use App\Jobs\MailJob;
use App\Project;
use App\ProjectTeam;
use App\UserWorkFromHome;
use App\EmailTemplate;
use App\Holiday;
use App\Leave;
use App\CompOff;
use App\Setting;
use App\Attendance;
use DateTime;
use DateInterval;
use DatePeriod;
use Carbon\Carbon;
use App\Exports\ExportUserInformation;
use DB;
use Illuminate\Support\Facades\Storage;
use App\ProjectEntry;
use App\ProjectTasks;

class ApiController extends BaseController
{
   public  function getActiveUser(){
    if(request()->ip() != '66.198.240.31'){
        $getdata = [];
        return $this->sendResponse($getdata,'Invalid domain request');
    }
    $user = User::select(DB::raw("CONCAT(first_name,' ',last_name) as name"),'joining_date','confirmation_date','employee_id')->where(['status' =>1, 'company_id' => 2])->where('want_to_exclude_for_salary',0)->get();
    //dd($user);
    return $user;
   }
    public function getUsers(Request $request,$id = '')
    {
        if(request()->ip() != '66.198.240.31'){
            $getdata = [];
            return $this->sendResponse($getdata,'Invalid domain request');
        }
    $statusArray = config('constant.status');
    $departmentArray = config('constant.department');
        if($id!='')
        {
            $display=User::leftjoin('designation as d','d.id','users.designation_id')
                          ->leftjoin('users as r','r.id','users.reporting_to')
                         ->leftjoin('companies as c','c.id','users.company_id');

            $getdata=$display->select('users.ifsc_code','users.password as userpassword','users.first_name','users.last_name','c.company_name','c.id as company_id','r.first_name as reporting_user','users.email as company_email','users.personal_email as personal_email','users.phone as contact_number','users.birthdate','users.employee_id','d.designation_name','users.department','users.bank_name','users.bank_account_no','users.pan_no','users.joining_date','users.from_shift','users.to_shift','users.status','users.available_leaves','users.used_leaves','users.employee_status','users.confirmation_date','users.gratuity','users.provident_fund','users.provident_fund_amount','users.bonus_applicable','users.salary_structure')->where('users.status',1)->where('users.company_id',2)->where('users.employee_id',$id)->where('want_to_exclude_for_salary',0)->get();
            if(isset($getdata) && !empty($getdata))
            {
                foreach($getdata as $val)
                {
                    $val->status=$statusArray[$val->status];
                    $val->department=$departmentArray[$val->department];
                    $val->employee_status= $val->employee_status == 1? "Confirmed" : "Probation";
                    $val->code=$val->getCompanyShortName($val->company_id).sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $val->employee_id)));
                }
            }

        }else
        {
             // $month = date('m');
            
             $display=User::leftjoin('designation as d','d.id','users.designation_id')
                            ->leftjoin('companies as c','c.id','users.company_id')
                            ->leftjoin('users as r','r.id','users.reporting_to');
                               // ->leftjoin('attendances', 'attendances.emp_code', 'users.employee_id')
                               // ->whereMonth('attendances.entry_date','<=',$month-1)
                            // ->whereMonth('users.joining_date','<=',$month-1);

                          

                            $getdata=$display->select('users.ifsc_code','users.password as userpassword','users.first_name','users.last_name','c.company_name','c.id as company_id','r.first_name as reporting_user','users.email as company_email','users.personal_email as personal_email','users.phone as contact_number','users.birthdate','users.employee_id','d.designation_name','users.department','users.bank_name','users.bank_account_no','users.pan_no','users.joining_date','users.from_shift','users.to_shift','users.status','users.available_leaves','users.used_leaves','users.employee_status','users.confirmation_date','users.joining_date','users.gratuity','users.provident_fund','users.provident_fund_amount','users.bonus_applicable','users.salary_structure')->where('users.company_id',2)->where('users.want_to_exclude_for_salary',0)
                                ->where(function($qry){
      
                                   $qry->where(function($q){
                                  $q->where('users.status',0);
                                   $q->orWhere('users.status',1);
                              });
                                    })->get();
                            
             if(isset($getdata) && !empty($getdata))
             {
                foreach($getdata as $val)
                {
                    $val->status=$statusArray[$val->status];
                    $val->department=$departmentArray[$val->department];
                    $val->employee_status= $val->employee_status == 1? "Confirmed" : "Probation";
                     $val->code=$val->getCompanyShortName($val->company_id).sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $val->employee_id)));
                }
             }
           
       }
         return $this->sendResponse($getdata,'data get');   
    }

    public function monthlyLeaveReport(Request $request,$month='',$year='')
    {
        
        if(request()->ip() != '66.198.240.31'){
            $getdata = [];
            return $this->sendResponse($getdata,'Invalid domain request');
        }
            $company         = new Company();
            $holiday          = new Holiday();
            $companyList      = $company->getCompanyName();
            $now['month']     = Carbon::now()->month;
            $now['year']      = Carbon::now()->year;  
            $comapny_id = 2;
            $is_date_wise = 0;
            $companyName = $company->getCompanyNameById($comapny_id)->company_name;
            for($m=1; $m<=12; ++$m){$months[$m] = date('F', mktime(0, 0, 0, $m, 1));}//month's array
            for($y=Carbon::now()->year; $y>=Carbon::now()->year-2;--$y){$years[$y] = $y;}//years array
            $weekOff = 0;
            
            if(date('d-m-Y',strtotime($month)) == $month && date('d-m-Y',strtotime($year)) == $year){
                $is_date_wise = 1;
            }
            if(isset($month) && !empty($month) && isset($year) && !empty($year))
            {
                $serachMonth = $month;
                $serachYear = $year;

                 // $getUserExtraLeave = Attendance::whereMonth('entry_date',  ">", $serachMonth)
                 //                                    ->whereYear('entry_date',  ">=", $serachYear)->where('emp_code','059')->orderBy('id','DESC')->get();
                // echo "<pre>";print_r($serachMonth);
                // echo "<pre>";print_r($serachYear);
             //   echo "<pre>";print_r($getUserExtraLeave);exit;

                if($is_date_wise){
                    $start_date = $serachMonth;
                } else {
                    $start_date = "01-".$serachMonth."-".$serachYear;
                }
                $start_time = strtotime($start_date);
                if($is_date_wise){
                    $end_time = strtotime("+1 day",strtotime($serachYear));
                } else {
                    $end_time = strtotime("+1 month", $start_time);
                }
                for($i=$start_time; $i<$end_time; $i+=86400)
                {
                    $dates[] = date('Y-m-d', $i);
                    $isWeekend = $holiday->getWeekendDateList($dates);
                    $days[] = date('D', $i);
                    // if((date('D', $i)== 'Sat') || (date('D', $i) == 'Sun') )
                    if(count($isWeekend) > 0)
                    {
                      $weekOff++;
                    }

                    
                }

                 $total_days_qry = Attendance::select('entry_date');
                    $total_days_qry->whereMonth('entry_date', $month);
                  $total_days_qry->whereYear('entry_date', $year);
                // $total_days_qry->where('entry_date', 'LIKE', '%' . $year . '-01%');
                // /$total_days_qry->where('emp_code','006');
                $total_days_qry->groupby('entry_date');
                $total_days_array = $total_days_qry->get()->toArray();
       

                            $total_days = 0;
                foreach ($total_days_array as $total_days_array_val) {
                    if (!Holiday::getHolidayDateList($total_days_array_val['entry_date'])) {
                        // $timestamp = strtotime($total_days_array_val['entry_date']);
                        // $weekday = date("l", $timestamp);
                        $date1 = $total_days_array_val['entry_date'];
                        $isWeekend1 = Holiday::getWeekendDateList($date1);
                        // if ($weekday != "Saturday" && $weekday != "Sunday") {
                        if(count($isWeekend1) == 0){
                            $total_days++;
                        }
                    }
                }


                $excelData = $userLeaveData =  array();
                $getUserList = User::where('users.company_id', $comapny_id)->whereNull('deleted_at');
                            if($comapny_id == 2)
                                {
                                    $getUserList = $getUserList->whereNotIn('employee_id',array('000','004'));
                                }   
                               // $getUserList->where("employee_id","297");
                               $getUserList =  $getUserList->where('users.want_to_exclude_for_salary',0)->orderBy('users.employee_id','asc')->pluck('employee_id');

               
                foreach($getUserList as $val){
                    $getLeave = Attendance::leftJoin('users', 'attendances.emp_code', 'users.employee_id')
                                        ->leftJoin('companies', 'users.company_id','companies.id' )
                                        ->where('attendances.emp_comp', $companyName)
                                        ->where('users.company_id', $comapny_id)
                                        ->where('users.employee_id', $val);
                                        if($is_date_wise){
                                            $getLeave = $getLeave->where('attendances.entry_date',  '>=', date('Y-m-d',strtotime($serachMonth)))
                                            ->where('attendances.entry_date','<=', date('Y-m-d',strtotime($serachYear)));
                                        }else {
                                            $getLeave = $getLeave->whereMonth('attendances.entry_date',  sprintf("%02d",$serachMonth))
                                            ->whereYear('attendances.entry_date', $serachYear);
                                        }
                                        
                                        $getLeave = $getLeave->whereNull('attendances.deleted_at')
                                        ->select(DB::raw('CONCAT(users.first_name," ",users.last_name) as name'),
                                                    DB::raw('CONCAT(companies.emp_short_name,users.employee_id) as empCode'),'users.employee_id',
                                                    'attendances.full_day','attendances.half_day','attendances.absent'
                                                ,'attendances.entry_date','users.id as userID')
                                        ->get()->toArray();
 
                    if(count($getLeave)>0){
                        foreach($getLeave as  $v){
                            
                             $query1 = CompOff::where(['user_id' => $v['userID'], 'compoff_marked_as' => '1','compoff_status' => 1]);

                            if($is_date_wise){
                                $query1 = $query1->where('compoff_start_date',  ">=", $serachMonth)
                                                ->where('compoff_start_date',"<=", $serachYear);
                            }else {
                                $query1 = $query1->whereMonth('compoff_start_date', sprintf("%02d",$serachMonth))
                                            ->whereYear('compoff_start_date', $serachYear);
                            }
                            $excelData[$val]['incased_compoff'] = $query1->whereNull('deleted_at')->sum('compoff_days');
                            $excelData[$val]['name'] = $v['name'];
                            $excelData[$val]['userId'] = $v['userID'];
                            $excelData[$val]['name'] = $v['name'];
                            $excelData[$val]['empCode'] = $v['employee_id'];
                            $excelData[$val][$v['entry_date']] = $v;
                        }
                    } 
                    $getCreatedLeave = User::leftjoin('comp_offs as cf', function($leftJoin)use($serachMonth,$serachYear,$is_date_wise)
                                            {
                                                $leftJoin->on('users.id', '=','cf.user_id')
                                                ->whereNull('cf.deleted_at')->where('cf.compoff_marked_as',2);
                                                if($is_date_wise){
                                                    $leftJoin = $leftJoin->where('cf.compoff_start_date',  ">=", $serachMonth)
                                                    ->where('cf.compoff_start_date',"<=", $serachYear);
                                                }else {
                                                    $leftJoin = $leftJoin->whereMonth('cf.compoff_start_date', sprintf("%02d",$serachMonth))
                                                    ->whereYear('cf.compoff_start_date', $serachYear);
                                                }
                                            })
                                        ->where('employee_id',$val)
                                        ->where('users.company_id', $comapny_id)
                                        ->select(DB::raw('SUM(cf.compoff_days) AS compoffDay'),'users.employee_id',
                                                    'users.available_leaves','users.used_leaves','users.employee_id','users.id','users.confirmation_date','users.employee_join_as','users.joining_date')
                                        ->orderBy('users.id')
                                        ->get();

                                        // echo '<pre>'; print_r($getCreatedLeave);exit;
                    if(count($getCreatedLeave)>0){
                        foreach($getCreatedLeave as  $userData){
                             $getUserExtraLeave = Attendance::whereMonth('entry_date',  ">=", $serachMonth)
                                                    ->whereYear('entry_date',  ">=", $serachYear)->where('emp_code',$userData['employee_id'])->orderBy('id','DESC')->get();
                            $count = 0;
                            if(!empty($getUserExtraLeave)){
                                
                                foreach ($getUserExtraLeave as $ka => $va) {
                                    if($va['absent'] == 1){
                                        $count += 1;
                                    }
                                    if($va['half_day'] == 1){
                                         $count += 0.5;
                                    }
                                }
                            }

                             // $getleave = Leave::where('user_id','=',$userData['id'])
                             //  ->where('leave_status',1)
                             //    ->where(function ($query) use ($serachYear, $serachMonth) {
                             //     $query->where(function ($q) use ($serachMonth, $serachYear) {
                             //            $q->whereMonth('leave_start_date', '>=', $serachMonth)
                             //               ->whereYear('leave_start_date', '>=', $serachYear);
                             //           })->orWhere(function ($q) use ($serachYear, $serachMonth) {
                             //            $q->whereMonth('leave_end_date', '>=', $serachMonth)
                             //                ->whereYear('leave_end_date', '>=', $serachYear);

                             //        });



                             //     })->get();
                               $getleave = Leave::where('user_id','=',$userData['uid'])->where('leave_status',1)->where('leave_end_date', '>=', date("Y-m-d",strtotime("$serachYear-$serachMonth-01")))->get();
                               $count = 0;
                            $array = array();
                            if(!empty($getleave)){
                             
                                foreach ($getleave as $ka => $va) {
                              
                                    if($va['leave_start_date']< date('Y-m-d', strtotime($serachYear.'-'.date('m', $serachMonth).'-01'))){
                                           
                                        for($currentDate = date('Y-m-d', strtotime($serachYear.'-'.date('m', $serachMonth).'-01')); $currentDate <= $va['leave_end_date']; $currentDate  = date('Y-m-d', strtotime($currentDate . ' +1 day'))) {
                                    
                                                
                                                $Store = date('Y-m-d', strtotime($currentDate));
                                                $leave = new Leave();
                                                $data = $leave->isHoliday($Store);
                                                $array[$Store]['data'] = $data;
                                                $array[$Store]['date'] = $Store;

                                             }
                                               
                                            foreach($array as $key=>$value){
                                                    
                                            
                                                   
                                                if($value['data'] !=1 ){
                                                    if($value['date']==$va['leave_end_date'])
                                                    {
                                                        if($va['leave_end_type']!=0){
                                                            $count += 0.5;
                                                        }else{
                                                            $count += 1;
                                                        }
                                                     
                                                    
                                                    }else{
                                                         $count += 1;
                                                    }
                                                }
                                            
                                                
                                              
                                            }
                                             
                                    }else{
                                        $count += $va['leave_days'];
                                    }
                                    
                                        
                                   
                                   

                                }
                                  

                            }
                            $remainingLeave = $userData['used_leaves'] - $count;
                            $userLeaveData[$val]['compoffDay'] = $userData['compoffDay'];
                            $userLeaveData[$val]['used_leaves'] = $remainingLeave;
                            $userLeaveData[$val]['used_leaves_test'] = $userData['used_leaves'];
                            $userLeaveData[$val]['count'] = $count;
                            if($remainingLeave != 0 || $remainingLeave != 0.0){
                                $leaveForUser = $remainingLeave;
                            }else{
                                $leaveForUser = 0.0;
                            }

                             if(isset($userData['confirmation_date']) && $userData['confirmation_date'] != '' && strtotime(date("Y-m",strtotime($userData['confirmation_date']))) <= strtotime(date("Y-m",strtotime("$serachYear-$serachMonth-01")))){
                                $userData['available_leaves'] = $userData['available_leaves'];
                            }else{
                                if($userData['employee_join_as'] == 0){
                                    $userData['available_leaves'] = 10;
                                }else if($userData['employee_join_as'] == 1){
                                    $userData['available_leaves'] = 3;
                                }else{
                                    $userData['available_leaves'] = $userData['available_leaves'];
                                }
                               
                            }

                             if(strtotime(date("Y-m",strtotime($userData['confirmation_date']))) > strtotime(date("Y-m",strtotime("$serachYear-$serachMonth-01")))){

                                $lastLeaveDate = date('Y-m-t',strtotime("$serachYear-$serachMonth-01"));
                                $userData['used_leaves'] = Leave::where('user_id',$userData['id'])->where('leave_start_date','>=',$userData['joining_date'])->where('leave_end_date','<=',$userData['confirmation_date'])->where('leave_status',1)->sum('leave_days');
                                $count = Leave::where('user_id',$userData['id'])->where('leave_end_date','>',$lastLeaveDate)->where('leave_status',1)->sum('leave_days');
                            }
                            $userLeaveData[$val]['creditLeave'] = $userData['available_leaves'] - ($userData['used_leaves'] - $count);


                            if(isset($userData['joining_date']) && $userData['joining_date'] != "" && strtotime(date('Y-m',strtotime($userData['joining_date']))) == strtotime(date('Y-m',strtotime("$serachYear-$serachMonth")))) {
                                $userLeaveData[$val]['creditLeave'] = $userData['available_leaves'];
                            }
                            if($userLeaveData[$val]['creditLeave']<=0){
                                $userLeaveData[$val]['creditLeave']=0;
                            }
                        }
                    }
                } 
           
              //echo "<pre>";print_r($userLeaveData);exit;
               $getEmployeeLeaves=array();
               if(count($excelData)>0)
               {
               foreach($excelData as $emp_code => $data)
               {
                $totalWorkedDay = 0; $leaveOfMonth = 0; 
                    for($j =0 ;$j<count($dates); $j++){
                       if(isset($data[$dates[$j]])){
                         $weekDay = date('w', strtotime($dates[$j]));
                            if($weekDay != 0 && $weekDay != 6){
                                if($data[$dates[$j]]['full_day'] == 1){
                                    $totalWorkedDay = $totalWorkedDay+1;
                                }
                                elseif($data[$dates[$j]]['half_day'] == 1){
                                        $totalWorkedDay = $totalWorkedDay+0.5;
                                        $leaveOfMonth = $leaveOfMonth+0.5; 
                                }
                                elseif ($data[$dates[$j]]['absent'] == 1){
                                $leaveOfMonth = $leaveOfMonth+1; 
                                }
                            }else{
                                
                            }
                        }
                       else{
                       }
                    }
               //     dd($data);
                    // echo "<pre>";print_r($serachYear);
                    // echo "<pre>";print_r($serachMonth);exit;

                $currentlyUsedLeavesBeforeThisMonth  = $userLeaveData[$emp_code]['used_leaves'];
               // $userLeaveData[$emp_code]['creditLeave'] = ($userLeaveData[$emp_code]['creditLeave'] > $currentlyUsedLeavesBeforeThisMonth)?($userLeaveData[$emp_code]['creditLeave'] - $currentlyUsedLeavesBeforeThisMonth):'0.00';
                $getEmployeeLeaves[$emp_code]['name']=$data['name'];
                $getEmployeeLeaves[$emp_code]['lwpCount']=getUserLwpLeave($serachMonth,$serachYear,$data['userId']);
                $getEmployeeLeaves[$emp_code]['leaveOfMonth']=$leaveOfMonth;
                $getEmployeeLeaves[$emp_code]['empCode']=$data['empCode'];
                $getEmployeeLeaves[$emp_code]['company_id']=$comapny_id;
                $getEmployeeLeaves[$emp_code]['total_number_of_days']=count($days);
                $getEmployeeLeaves[$emp_code]['weekOff']=$weekOff;
                $getEmployeeLeaves[$emp_code]['total_working_days']=$total_days;
                $getEmployeeLeaves[$emp_code]['total_worked_days']=$totalWorkedDay;
                $getEmployeeLeaves[$emp_code]['credit_leave_cf']= $userLeaveData[$emp_code]['creditLeave'];
                $getEmployeeLeaves[$emp_code]['leave_of_month']=$leaveOfMonth;
                $getEmployeeLeaves[$emp_code]['balance_leave']=($userLeaveData[$emp_code]['creditLeave'] > $leaveOfMonth)?($userLeaveData[$emp_code]['creditLeave'] - $leaveOfMonth):'0.00';
                $getEmployeeLeaves[$emp_code]['comp_off']=$userLeaveData[$emp_code]['compoffDay'];
                $getEmployeeLeaves[$emp_code]['incase_comp_off']=$data['incased_compoff'];
                $getEmployeeLeaves[$emp_code]['balance_leave_after_companyoff']=($userLeaveData[$emp_code]['creditLeave'] > $leaveOfMonth)?(($userLeaveData[$emp_code]['creditLeave'] - $leaveOfMonth)+ $userLeaveData[$emp_code]['compoffDay']):'0.00';
               /* $excelData[$key]->creditLeave=$userLeaveData[$val]->creditLeave;*/
               }
               }
                //echo "<pre>";print_r($getEmployeeLeaves);exit;
               return $this->sendResponse($getEmployeeLeaves,'data get'); 
            }else
            {
                return $this->sendError('Invalid Arguments',[]);
            }

    }
      public function attendnce(Request $request,$month='',$year='')
    {
        if(request()->ip() != '66.198.240.31'){
            $getdata = [];
            return $this->sendResponse($getdata,'Invalid domain request');
        }
          
         
            if(isset($month) && !empty($month) && isset($year) && !empty($year))
            {
                $serachMonth = $month;
                $serachYear = $year;
               

                 $total_days = Attendance::leftJoin('users', 'attendances.emp_code', 'users.employee_id')->whereMonth('attendances.entry_date', $serachMonth)->whereYear('attendances.entry_date', $serachYear)->where('attendances.emp_comp','Tridhya Tech')->where('users.want_to_exclude_for_salary',0)->groupby('attendances.emp_code')->get();
                 // echo '<pre>';print_r($total_days);exit;
                $getattendences=array();
                  foreach($total_days as $key => $data)
                   { 
                                  
                     
          
              
               
                        $getattendences[$key]['entry_date']=$data['entry_date'];
                        $getattendences[$key]['emp_code']=$data['emp_code'];
                        $getattendences[$key]['emp_name']=$data['emp_name'];
                        $getattendences[$key]['emp_comp']=$data['emp_comp'];
                        $getattendences[$key]['first_in']=$data['first_in'];
                        $getattendences[$key]['last_out']=$data['last_out'];
                        $getattendences[$key]['late_comer']=$data['late_comer'];
                        $getattendences[$key]['full_day']= $data['full_day'];
                        $getattendences[$key]['early_going']=$data['early_going'];
                        $getattendences[$key]['absent']=$data['absent'];
                         
               
               }
               // }
             
               return $this->sendResponse($getattendences,'data get'); 
            }else
            {
                return $this->sendError('Invalid Arguments',[]);
            }

    }
    public function getGeneralSettings($company_id){
        if(request()->ip() != '66.198.240.31'){
            $getdata = [];
            return $this->sendResponse($getdata,'Invalid domain request');
        }
        $data = Setting::where('module','gratuity')->orWhere('module','professional_tax')->orWhere('module','basic_salary')->orWhere('module','other_allownces')->orWhere('module','house_rent')->orWhere('module','performance_allowance')->orWhere('module','bonus_applicable_percentage')->where('company_id',$company_id)->get();

        return $this->sendResponse($data,'data get');
    }

      public function getCompanyDetails(Request $request){
        if(request()->ip() != '66.198.240.31'){
            $getdata = [];
            return $this->sendResponse($getdata,'Invalid domain request');
        }
        $data = Company::where('id',$request->company_id)->first();
        return $this->sendResponse($data,'data get');
    }

     public function projectCalculation(Request $request){
            $company        = new Company();
            $companyList    = $company->getCompanyName();
            $companyId      = 2;
            $user           = new User();
            $userReq           = new User();
            $userListingForTL  = array();
            $team_user_id = array();
            $req_user_team_name = array();
            $reportingToList = $reportingId = array();
            $non_billable_projects = Project::where('is_non_billable',1)->pluck('id')->toArray();
            
                $reportingList = User::select('id','first_name','last_name','reporting_to')
                                    ->where('company_id',$companyId)
                                    ->whereNull('deleted_at')
                                    ->where('status',1)->groupBy('id')->get()->toArray();
                                    // echo '<pre>'; print_r(reportingList)
                foreach ($reportingList as $key => $reportingListVal) {
                    if (!in_array($reportingListVal['reporting_to'], $reportingId)) {
                        $reportingId[] = $reportingListVal['reporting_to'];
                        $reportingToList[] = User::where('id', $reportingListVal['reporting_to'])->select('first_name', 'last_name', 'id')->first();
                    }
                }
                if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                    if($request->search_by_reporting_to != 0){
                        $req_user_team = $userReq->getMyTeamList($request->search_by_reporting_to);
                        foreach ($req_user_team as $key => $reqVal){
                            $team_user_id[$key] = $reqVal['id'];
                            $req_user_team_name[$reqVal['id']] = $reqVal['first_name'].' '.$reqVal['last_name'];
                        }
                    }
                }
            
                if($companyId){
                    $user =   $user->where('company_id',$companyId);
                }
                if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                    $user =   $user->whereIn('users.department',($request->search_by_department));
                }
                if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                    $param = $request->search_by_internal_company_id;
                    if(in_array("parent_company",$param) && count($param) >= 2){
                        $user =   $user->where(function ($q) use ($param) {
                            $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                        });
                    } else if (in_array("parent_company",$param)) {
                        $user =   $user->WhereNull('internal_company_id');
                    } else {
                        $user =   $user->whereIn('internal_company_id', ($param));
                    }
                }
                $userListing = $user->where('users.status',1)->where('want_to_exclude_for_salary',0)->orderBy('first_name', 'ASC')->get();

            $teamUserArray = $teamUserName = $teamDesignationId = array();
            foreach ($userListing as $userListingVal){
                $teamUserArray[] = $userListingVal['id'];
                $teamDesignationId[$userListingVal['id']] = $userListingVal['designation_id'];
                $teamUserName[$userListingVal['id']] = $userListingVal['first_name'].' '.$userListingVal['last_name'];
            }

            $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                            ->select('cr.project_id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                            ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                            ->whereNull('cr.deleted_at');
            if($request->has('search_by_project_hours_type') && $request->search_by_project_hours_type != '')
            {
                if($request->search_by_project_hours_type == "billable_hours")
                {
                    $projectCrHours = $projectCrHours->whereNotIn('projects.id',$non_billable_projects);
                }
                if($request->search_by_project_hours_type == "non_billable_hours")
                {
                    $projectCrHours = $projectCrHours->whereIn('projects.id',$non_billable_projects);
                }
            }
            $projectCrHours= $projectCrHours->groupBy('cr.project_id')
                            ->pluck('total_cr_hours','cr.project_id')->toArray();
            
                $firstDate = date('Y-m-01');$lastDate = date('Y-m-t');
                 if ($request->has('search_by_start_date') && $request->search_by_start_date != '') {
                    $firstDate = $request->search_by_start_date;
                }
                if ($request->has('search_by_end_date') && $request->search_by_end_date != '') {
                    $lastDate = $request->search_by_end_date;
                }
                
                        $projectQuery = Project::leftJoin('project_entries as pe','projects.id','pe.project_id');
                                        
                                            $projectQuery->where('projects.assigned_to',$companyId);
                                        
                                    $projectQuery = $projectQuery->groupBy('projects.id')
                                            ->select('projects.id','projects.project_name');
                    
                    $allproject = clone $projectQuery;
                    $projectData =  $allproject->orderBy('projects.project_name', 'asc')->get();
                    $projectQuery->where('pe.log_date',  '>=', $firstDate);
                    $projectQuery->where('pe.log_date',  '<=', $lastDate);
                    $projectQuery->whereIn('pe.user_id',  $teamUserArray);
                    
                    
                        if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                            $projectQuery->where('projects.id',$request->search_by_project_name);
                        }
                        if($request->has('search_by_project_hours_type') && $request->search_by_project_hours_type != '')
                        {
                            if($request->search_by_project_hours_type == "billable_hours")
                            {
                                $projectQuery = $projectQuery->whereNotIn('projects.id',$non_billable_projects);
                            }
                            if($request->search_by_project_hours_type == "non_billable_hours")
                            {
                                $projectQuery = $projectQuery->whereIn('projects.id',$non_billable_projects);
                            }
                        }
                        if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                            $projectQuery->where('pe.user_id',$request->search_by_user_name);
                        }

                        if ($request->has('search_project_status') && $request->search_project_status != '') {
                            if($request->search_project_status != 0){
                                $projectQuery->where('projects.project_status', $request->search_project_status);
                            }
                        }
                       
                            if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                                if($request->search_by_reporting_to != 0){
                                    $projectQuery->whereIn('pe.user_id',$team_user_id);
                                }
                            }
                        
                    
                    if ($request->has('sort') && $request->input('sort') != '') {
                         $projData = $projectQuery->sortable()->orderBy($request->input('sort'), $request->input('direction'))->get();//->paginate($page_limit);
                     } else {
                         $projData = $projectQuery->sortable()->orderBy('projects.id', 'desc')->get();//->paginate($page_limit);
                    }
                    $projectReportArray = $projectLoggedArray = $projectEmpLoggedArray = $projectRowCount = $projectoldEntry = $projectEstimatedArray = array();

                    foreach($projData as $projectDataVal){
                        $loggedHours = ProjectEntry::with(["projectDetails" => function($q) { $q->with("projecttype");},"users" => function($q) {$q->select(DB::raw('CONCAT(first_name," ",last_name) AS user_name'),'id');}])->leftjoin('users','users.id','user_id');
                        $AssignedHours = ProjectTasks::leftjoin('users','users.id','user_id');
                        $allLoggedHours = clone $loggedHours;
                        $allAssigndHours = clone $AssignedHours;
                        $empLoggedHours = ProjectTasks::with(["users" => function($q) {$q->select(DB::raw('CONCAT(first_name," ",last_name) AS user_name'),'id');}])->leftjoin('project_entries', function($leftJoin)use($firstDate,$lastDate)
                                            {
                                                $leftJoin->on('project_tasks.id', '=','project_entries.task_id')
                                                        ->whereNull('project_entries.deleted_at')
                                                        ->where('project_entries.log_date',  '>=', $firstDate)
                                                        ->where('project_entries.log_date',  '<=', $lastDate);
                                            });
                        $oldEntry = ProjectEntry::where('project_id',$projectDataVal['id'])
                                                ->where('log_date',  '>=', $firstDate)
                                                ->where('log_date',  '<=', $lastDate)
                                                ->whereNull('deleted_at');
                        $allLoggedHours =  $allLoggedHours->select(DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                                                            ->where('project_id',$projectDataVal['id'])
                                                            ->groupBy('project_entries.project_id')
                                                            ->pluck('total_logged_hours');
                        $allAssigndHours =  $allAssigndHours->select(DB::raw("sum(project_tasks.`estimated_hours`) as total_estimated_hours"))
                                                            ->where('project_id',$projectDataVal['id'])
                                                            ->groupBy('project_tasks.project_id')
                                                            ->pluck('total_estimated_hours');
                                $loggedHours = $loggedHours->where('project_entries.log_date',  '>=', $firstDate)
                                                            ->where('project_entries.log_date',  '<=', $lastDate);
                                
                               
                                    if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                        $loggedHours = $loggedHours->where('user_id',$request->search_by_user_name);
                                    }
                                    
                                        if($request->search_by_reporting_to != 0){
                                            $loggedHours = $loggedHours->whereIn('user_id',$team_user_id);
                                        }
                                if (($projectDataVal['project_name'] == 'Miscellaneous Tasks' || $projectDataVal['project_name'] == 'Bench')){
                                        $loggedHours = $loggedHours->whereIn('user_id',$teamUserArray);
                                        $empLoggedHours = $empLoggedHours->whereIn('project_tasks.user_id',$teamUserArray);
                                        $oldEntry = $oldEntry->whereIn('user_id',$teamUserArray);
                                }
                                $loggedHours =  $loggedHours->where('project_id',$projectDataVal['id']);
                                $query_task_id = $loggedHours->pluck("task_id")->toArray();
                                $loggedHours =  $loggedHours->select('users.id',"project_entries.user_id",'project_id',DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                                                            ->groupBy('project_entries.user_id');

                                if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                   $empLoggedHours =  $empLoggedHours->where('project_tasks.user_id',$request->search_by_user_name);
                                   $oldEntry = $oldEntry->where('user_id',$request->search_by_user_name);
                                }
                               

                              
                                    if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                                        if($request->search_by_reporting_to != 0){
                                            $empLoggedHours =  $empLoggedHours->whereIn('project_tasks.user_id',$team_user_id);
                                            $oldEntry = $oldEntry->whereIn('user_id',$team_user_id);
                                        }
                                    }
                                

                                $empLoggedHours = $empLoggedHours->where('project_tasks.project_id',$projectDataVal['id'])->whereIn("project_tasks.id",$query_task_id)
                                                            // ->where(function($q)use($firstDate,$lastDate){
                                                            //     $q->where('task_start_date',  '>=', date("Y-m-01",strtotime($firstDate)))
                                                            //         ->where('task_end_date',  '<=', date("Y-m-t",strtotime($lastDate)));
                                                            // })
                                                            ->select('project_tasks.user_id as id','project_tasks.user_id','project_tasks.id as taskId',
                                                                    DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"),
                                                                    'project_tasks.task_name','project_tasks.estimated_hours as taskHours')
                                                            ->groupBy('project_tasks.user_id','project_tasks.id')
                                                            ->get()->toArray();
                                $oldEntry = $oldEntry->select('user_id as id',DB::raw("sum(`log_hours`) as total_logged_hours"))
                                            ->whereNull('task_id')->groupBy('user_id')
                                            ->get()->toArray();
                        $totalCount = 0 ;

                        if(isset($loggedHours) && count($loggedHours->get()->toArray()) > 0){
                            $projectReportArray[$projectDataVal['id']] = [$loggedHours->pluck('total_logged_hours','users.id')->toArray(), $loggedHours->first()->toArray()];
                            $projectLoggedArray[$projectDataVal['id']] = $allLoggedHours;
                            $projectEmpLoggedArray[$projectDataVal['id']] =array_merge($empLoggedHours,$oldEntry);
                            $projectEstimatedArray[$projectDataVal['id']] = $allAssigndHours;
                            $totalCount = $totalCount + count($empLoggedHours) + count($oldEntry);
                            $projectRowCount[$projectDataVal['id']] = $totalCount;
                        }
                    }
                return [
                    'projectRowCount' => $projectRowCount,
                    'projectLoggedArray' => $projectLoggedArray,
                    'projectReportArray' => $projectReportArray,
                    'projectEstimatedArray' => $projectEstimatedArray,
                    'projectEmpLoggedArray' => $projectEmpLoggedArray,
                    'projData' => $projData,
                    'projectData' => $projectData,
                    'request' => $request,
                    'teamUserName' => $teamUserName,
                    'teamDesignationId' => $teamDesignationId,
                    'projectCrHours' => $projectCrHours,
                    'reportingToList' => $reportingToList,
                    'req_user_team_name' => $req_user_team_name,
                    'non_billable_projects' => $non_billable_projects
                ];
    }

}
