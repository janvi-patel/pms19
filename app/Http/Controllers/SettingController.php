<?php

namespace App\Http\Controllers;


use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use DB;
use Alert;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $setting = new Setting();
            $settingData = $setting::where('company_id',Auth::user()->company_id)->get();
            $dataArray = array();
            foreach($settingData as $settingDataVal){
                $dataArray[$settingDataVal['module']] = $settingDataVal;
            }
            return view('pms.setting.index', compact('dataArray'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function TaskEntryDayUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $timeentry_day = $request->input()['timeentry_day'];
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','TaskEntry')
                            ->update(['days' => $timeentry_day]);

            Alert::success('Allow timeentry days Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function LateComeTimeUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $timeentry_day = $request->input()['lateComeTime'];
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','LateComeTime')
                            ->update(['constant_str_value' => $timeentry_day]);

            Alert::success('LateComer Time Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function workingHourUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $working_hour = $request->input()['working_hour'];
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','average_working_time')
                            ->update(['constant_str_value' => $working_hour]);

            Alert::success('Daily Working Time Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function probationTimeUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $probation_time = $request->input()['probation_time'];
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','probation_time')
                            ->update(['constant_int_value' => $probation_time]);

            Alert::success('Probation Time Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function loggedHourUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $logged_hour = $request->input()['logged_hours_time'].":".$request->input()['logged_minute_time'].":00";
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','average_logged_time')
                            ->update(['constant_time' => $logged_hour]);

            Alert::success('Daily Logged Time Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function taskOperationDaysUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $days = $request->input()['task_operation_day'];
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','task_operation')
                            ->update(['days' => $days]);

            Alert::success('Task operation days successfully updated');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
public function gratuityUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $gratuity_text = $request->input()['gratuity_text'];
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','gratuity')
                            ->update(['gratuity' => $gratuity_text]);

            Alert::success('Gratuity Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function professionalTaxUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $professional_tax_text = $request->input()['professional_tax_text'];
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','professional_tax')
                            ->update(['professional_tax' => $professional_tax_text]);

            Alert::success('Professional Tax Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function houseRantUpdate(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $house_rent_text = $request->input()['house_rent_text'];

            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','house_rent')
                            ->update(['house_rent' => $house_rent_text]);
                // echo '<pre>';print_r(Auth::user()->company_id);exit;
            Alert::success('House Rent Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

          public function BasicSalaryUpdate(Request $request) {
           if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $basic_salary_text = $request->input()['basic_salary_text'];

            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','basic_salary')
                            ->update(['basic_salary' => $basic_salary_text]);
                // echo '<pre>';print_r(Auth::user()->company_id);exit;
            Alert::success('Basic Salary Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

      public function performance_allowanceUpdate(Request $request) {
           if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $performance_allowance_text = $request->input()['performance_allowance_text'];

            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','performance_allowance')
                            ->update(['performance_allowance' => $performance_allowance_text]);
                // echo '<pre>';print_r(Auth::user()->company_id);exit;
            Alert::success('Performance Allowance Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

       public function other_allowncesUpdate(Request $request) {
           if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $other_allownces_text = $request->input()['other_allownces_text'];

            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','other_allownces')
                            ->update(['other_allownces' => $other_allownces_text]);
                // echo '<pre>';print_r(Auth::user()->company_id);exit;
            Alert::success('Other Allownces Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function bonus_applicable_percentage(Request $request) {
           if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            $bonus_applicable_percentage_text = $request->input()['bonus_applicable_percentage_text'];
         //   echo $bonus_applicable_percentage_text;exit;
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','bonus_applicable_percentage')
                            ->update(['constant_float_value' => $bonus_applicable_percentage_text]);
                // echo '<pre>';print_r(Auth::user()->company_id);exit;
            Alert::success('Bonus Applicable Percentage Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
     public function startTimeforFirstHalf(Request $request) {
           if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
            //$start_time_for_first_half = $request->input()['start_time_for_first_half'];
            $start_time_for_first_half = $request->input()['first_half_hours_time'].":".$request->input()['first_half_minute_time'].":00";
            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','max_first_half_start_time')
                            ->update(['constant_time' => $start_time_for_first_half]);
                // echo '<pre>';print_r(Auth::user()->company_id);exit;
            Alert::success('First Half time Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function startTimeforSecondtHalf(Request $request) {
           if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
           // $start_time_for_second_half = $request->input()['start_time_for_second_half'];
            $start_time_for_second_half = $request->input()['second_half_hours_time'].":".$request->input()['second_half_minute_time'].":00";

            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','max_second_half_start_time')
                            ->update(['constant_time' => $start_time_for_second_half]);
                // echo '<pre>';print_r(Auth::user()->company_id);exit;
            Alert::success('Second Half time Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function startTimeforfullday(Request $request) {
           if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))){
           // $start_time_for_second_half = $request->input()['start_time_for_second_half'];
            $start_time_for_second_half = $request->input()['full_day_hours_time'].":".$request->input()['full_day_minute_time'].":00";

            $settingUpdate = DB::table('settings')
                            ->where('company_id',Auth::user()->company_id)
                            ->where('module','max_full_day_start_time')
                            ->update(['constant_time' => $start_time_for_second_half]);
                // echo '<pre>';print_r(Auth::user()->company_id);exit;
            Alert::success('Full Day time Successfully Updated!');
            return Redirect::to('/view/setting');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function crSelectTimeRestriction(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))) {
            $cr_operation_month = $request->cr_operation_month;

            $settingUpdate = DB::table('settings')->where('company_id',Auth::user()->company_id)->where('module','cr_operation')->update(['constant_int_value' => $cr_operation_month]);
            Alert::success('Select cr / project to create a task restriction successfully updated!');
            return Redirect::to('/view/setting');
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function emailForServer(Request $request) {
        if(Auth::check() && Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))) {
            $email_for_server = isset($request->email_for_server) && $request->email_for_server == 1 ? 1 : 0 ;

            $settingUpdate = DB::table('settings')->where('company_id',Auth::user()->company_id)->where('module','email_for_server')->update(['constant_int_value' => $email_for_server]);
            Alert::success('Email setting successfully updated!');
            return Redirect::to('/view/setting');
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

}
