<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Documents;
use Auth;
use Alert;
use Response;
use Redirect;
use Validator;
class UploadDocumentsController extends Controller
{

    public function index(Request $request){
        $page       = $request->get('page');
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug'))) ? config('constant.company.shaligram') : (Auth::user()->company_id);
        $docs = Documents::where('company_id',$companyId);
        if ($request->has('sort') && $request->input('sort') != '') {
            $data = $docs->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
        } else {
            $data = $docs->sortable()->orderBy('id', 'desc')->paginate($page_limit);
        }
        return view('pms.documents.index',compact('data','request'));
    }

    public function uploadDocuments(Request $request){
        if (Auth::check() && Auth::user()->hasPermission('pms.upload.docs')) {
            $page       = $request->get('page');
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug'))) ? config('constant.company.shaligram') : (Auth::user()->company_id);
            $docs = Documents::where('company_id',$companyId);
            if ($request->has('sort') && $request->input('sort') != '') {
                $data = $docs->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $data = $docs->sortable()->orderBy('id', 'desc')->paginate($page_limit);
            }
            return view('pms.documents.upload_documents',compact('data','request'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::back();
        }
    }
    public function docsModal(){
        if (Auth::check() && Auth::user()->hasPermission('pms.upload.docs')) {
            echo view('pms.documents.modal.add_documents_modal');
        }else{
            return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
        }
    }
    public function uploadDocs(Request $request){
        if (Auth::check() && Auth::user()->hasPermission('pms.upload.docs')) {
            $requestData = $request->toArray();
            $rules['modal_attachment'] = 'max:10';
            $rules['modal_attachment.*'] = 'mimes:pdf,xls,xlsx,doc,docx,ppt,pptx,pptm';
            $messages = [
                'modal_attachment.max' => "Please select maximum 10 documents to upload",
                'modal_attachment.mimes' => "Please select pdf,xls,xlsx,doc,docx,ppt,pptx,pptm documents type to upload",
            ];
            $validator = Validator::make($requestData, $rules, $messages);
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else{
                try{
                    $destinationPath = public_path('/upload/pms_docs');
                    if ($request->hasFile('modal_attachment') && isset($request->modal_attachment) && !empty($request->modal_attachment)) {
                        $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug'))) ? config('constant.company.shaligram') : (Auth::user()->company_id);
                        foreach ($request->modal_attachment as $key => $attachmentFile) {
                            $documents = new Documents();
                            $fullName = $attachmentFile->getClientOriginalName();
                            $name = explode('.', $fullName)[0];
                            $extension = explode('.', $fullName)[1];
                            $pmsFile = $name.'_document_'.rand().'.'.$extension;
                            $attachmentFile->move($destinationPath, $pmsFile);
                            $documents->name = $name;
                            $documents->file_name = $pmsFile;
                            $documents->created_by = Auth::user()->id;
                            $documents->company_id = $companyId;
                            $documents->save();
                        }
                    }
                    Alert::success('New Documents Successfully Added.'); 
                    return Redirect::back();
                }catch(Exception $e){
                    Alert::error('Some thing was wrong please try again.');
                    return Redirect::back();
                }
            }
        }
        else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::back();
        }
    }

    public function editDocs($id){
        if (Auth::check() && Auth::user()->hasPermission('pms.upload.docs')) {
            $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug'))) ? config('constant.company.shaligram') : (Auth::user()->company_id);
            $docs = Documents::where('id',$id)->where('company_id',$companyId)->select('id','name')->first()->toArray();
            if(isset($docs)){
                return $docs;
            }
            else{
                return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::back();
        }
    }

    public function updateDocs(Request $request){
        if (Auth::check() && Auth::user()->hasPermission('pms.upload.docs')) {
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'edit_name' => 'required',
            ]);
            if ($validator->fails()) {
                return Response::json(array('message'=>$validator->errors()->all(),'status'=>'error'));
            }
            else{
                $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug'))) ? config('constant.company.shaligram') : (Auth::user()->company_id);
                $docs = Documents::find($request->id);
                if(isset($docs) && ($docs->company_id == $companyId)){
                    try{
                        $docs->name = $request->edit_name;
                        $docs->updated_by = Auth::user()->id;
                        $docs->save();
                        return Response::json(array('message'=>'Documents Name Successfully Updated','status'=>'success'));
                    }catch(Exception $e){
                        return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
                    }
                }else{
                    return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
                }
            }
        }else{
            return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
        }
    }

    public function deleteDocs(Request $request)
    {
        if (Auth::check() && Auth::user()->hasPermission('pms.upload.docs')) {
            if(isset($request->docsId) && $request->docsId != '')
            {
                $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug'))) ? config('constant.company.shaligram') : (Auth::user()->company_id);
                $docsId = Documents::find($request->docsId);
                if(isset($docsId) && ($docsId->company_id == $companyId))
                {
                    try{
                        $docsId->deleted_by = Auth::user()->id;
                        $docsId->save();
                        $docsId->delete();
                        return Response::json(array('message'=>'Documents Deleted Successfully','status'=>'success'));
                    }catch(Exception $e){
                        return Response::json(array('message'=>'Documents Not Deleted Successfully','status'=>'error'));
                    }
                }else{
                    return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
                }
            }
        }else{
            return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
        }
    }
}
