<?php

namespace App\Http\Controllers;

use App\Leave;
use App\User;
use Alert;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Response;
use Log;
use App\CompOff;
use App\Jobs\MailJob;
use App\Company;
use App\LeaveDetail;
use App\Attendance;
use App\WorkFromHome;
use App\Http\Controllers\TimeSheetController;

class WorkFromHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userIdArray = array();
        $page       = $request->get('page');
        $authUser = Auth::user();
        $userName = new User();
        $company_id = '';
        if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
            $company_id = $authUser->company_id;
        }
        $approver_id = WorkFromHome::where('user_id','=',$authUser->id)->pluck("approver_id")->toArray();
        $userNameList = User::select('id','first_name','last_name')->whereIn('id',$approver_id)->get();

        $start_date = $request->search_start_date;
        $end_date = $request->search_end_date;
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

        $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
        if($loggedInUserID){
            $userIdArray = array($loggedInUserID);
        }

        $leave = WorkFromHome::with(['user', 'approver']);
        if (isset($request->id)) {
            $leaveStatus = $request->id;
        }else{
            $leaveStatus =0;
        }
        if ($userIdArray) {
            if ($leaveStatus != 0) {
                $leave->whereIn('status', [$leaveStatus]);
            }
            $leave->whereIn('user_id', $userIdArray);
        }
        if (!($request->has('search_start_date'))) {
            $start_date = date('d-m-Y',strtotime(date('01-04-Y')));
            if(date('m-d',strtotime($start_date)) > date('m-d'))
            {
                $year = date("Y", strtotime("-1 years"));
                $start_date = date('d-m-Y',strtotime(date('1-04-'.$year)));
            }
            $leave->where('start_date',  '>=',  date("Y-m-d", strtotime($start_date)));
        }
        if (!($request->has('search_end_date'))) {
            $end_date = date('d-m-Y',strtotime(date('31-12-Y')));
            $leave->where('end_date',  '<=', date("Y-m-d", strtotime($end_date)));
        }
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                $leave->where('user_id',  '=', $request->search_by_employee);
            }
            if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                $leave->where('approver_id',  '=', $request->search_by_approver);
            }
            if ($request->has('search_start_date') && $request->search_start_date != '') {
                $leave->where('start_date',  '>=', date("Y-m-d", strtotime($request->search_start_date)));
            }
            if ($request->has('search_end_date') && $request->search_end_date != '') {
                $leave->where('end_date',  '<=', date("Y-m-d", strtotime($request->search_end_date)));
            }
            if ($request->has('search_by_status') && $request->search_by_status != '') {
                $leave->where('status',  '=', $request->search_by_status);
            }
        }
        $leavesData = $leave->sortable()->orderBy('start_date', 'desc')->paginate($page_limit);
        return view('pms.work_from_home.index', compact('leavesData', 'loggedInUserID','page','userNameList','request','start_date','end_date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if (Auth::check()) {
            $authUser = Auth::user();
            $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
            $user = new User();
            $userDetails = $user->getUserDetails($loggedInUserID);
            $reportingTo = $user->getReportingToListWithOnlyTeamLeader($loggedInUserID);
            return view('pms.work_from_home.create', compact('userDetails', 'reportingTo'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r($request->all());
        // exit;
        $user = new User();

        if (Auth::check() && Auth::user()->hasPermission('leave.create')) {
            $getUid = $request->user()->id;
            $total_days = 0;
            $postData = $request->input();
            $validationRule['reason'] = 'required|string|min:6';
            $validationRule['start_date'] = 'required|date_format:Y-m-d|before_or_equal:end_date';
            $validationRule['start_date'] = 'required|date_format:Y-m-d|after_or_equal:joinning_date';
            $validationRule['end_date'] = 'required|date_format:Y-m-d|after_or_equal:start_date';


            $validator = Validator::make(Input::all(), $validationRule);

            // if validation fails
            if ($validator->fails()) {
                // Alert::error('Oops, Form has some error!');
                return Redirect::to('workfromhome/add')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                if( ($request->start_date==$request->end_date)  && ($request->start_type== 2) && ($request->end_type== 0 || $request->end_type == 1) ){
                    alert()->info('You can only select second half as end type.')->persistent('close')->autoclose("3600");
                    return Redirect::to('workfromhome/add');
                }
                $start = $request->start_date;
                $end = $request->end_date;
                $leavecheck = Leave::where('user_id','=',$getUid)
                    ->where(function ($query) use ($start, $end) {
                    $query->where(function ($q) use ($start, $end) {
                            $q->where('leave_start_date', '>=', $start)
                            ->where('leave_start_date', '<', $end);

                        })->orWhere(function ($q) use ($start, $end) {
                            $q->where('leave_start_date', '<=', $start)
                            ->where('leave_end_date', '>', $end);

                        })->orWhere(function ($q) use ($start, $end) {
                            $q->where('leave_end_date', '>=', $start)
                            ->where('leave_end_date', '<=', $end);

                        })->orWhere(function ($q) use ($start, $end) {
                            $q->where('leave_start_date', '>=', $start)
                            ->where('leave_end_date', '<=', $end);
                        });

                    })->first();

                    if(!empty($leavecheck)){
                        if(!($leavecheck->reason == "Auto Generated Leave Adhoc Leave" && $leavecheck->leave_status == 2)){
                            alert()->info('Your already have leave on that day.')->persistent('close')->autoclose("3600");
                            return Redirect::to('workfromhomeequests');
                        }
                    }
                $checkLeave = WorkFromHome::where('user_id','=',$getUid)
                                ->where(function ($query) use ($start, $end) {
                                 $query->where(function ($q) use ($start, $end) {
                                        $q->where('start_date', '>=', $start)
                                           ->where('start_date', '<', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('start_date', '<=', $start)
                                           ->where('end_date', '>', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('end_date', '>=', $start)
                                           ->where('end_date', '<=', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('start_date', '>=', $start)
                                           ->where('end_date', '<=', $end);
                                    });

                                })->count();

                if($checkLeave > 0){
                   alert()->info('Your already have work from home on that day.')->persistent('close')->autoclose("3600");
                   return Redirect::to('workfromhomeequests');
                }
                else{
                     $checkcompOff = CompOff::where('user_id','=',$getUid)
                                ->where(function ($query) use ($start, $end) {
                                 $query->where(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '>=', $start)
                                           ->where('compoff_start_date', '<', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '<=', $start)
                                           ->where('compoff_end_date', '>', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_end_date', '>=', $start)
                                           ->where('compoff_end_date', '<=', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '>=', $start)
                                           ->where('compoff_end_date', '<=', $end);
                                    });

                                })->count();
                      if($checkcompOff>0)
                      {
                          alert()->info('Your already have comp off on that day.')->persistent('close')->autoclose("3600");
                         return Redirect::to('workfromhomeequests');
                      }else{
                        $leave = new WorkFromHome;


                    $summary['start_date'] = $postData['start_date'];
                    $summary['end_date'] = $postData['end_date'];
                    $summary['start_type'] = $postData['start_type'];
                    $summary['end_type'] = $postData['end_type'];
                    //echo "<pre>";print_r($summary);
                    $summaryDetails = getLeaveSummary($summary);
                    //echo "<pre>";print_r($summaryDetails);exit;
                    if($summaryDetails < 0.5){
                        Alert::error('Oops, Something went wrong please try again!');
                        return Redirect::to('workfromhome/add');
                    }
                    if($summaryDetails['leave_days']==0 || $summaryDetails['leave_days']== -1){
                        // alert()->info('Your not allowed to add weekend as leave.')->persistent('close')->autoclose("3600");
                            Alert::error('Your not allowed to add weekend as leave.');
                           return Redirect::to('workfromhomeequests');
                       }
                    $leave->user_id = Auth::user()->id;
                    $leave->approver_id =  $user->getReportingToListWithOnlyTeamLeader(Auth::user()->id);
                    $leave->start_date = date("Y-m-d", strtotime(Input::get('start_date')));
                    $leave->end_date = date("Y-m-d", strtotime(Input::get('end_date')));
                    $leave->return_date = date("Y-m-d", strtotime($summaryDetails['return_date']));
                    $leave->days = $summaryDetails['leave_days'];
                    $leave->start_type = Input::get('start_type');
                    $leave->end_type = Input::get('end_type');
                    $leave->reason = Input::get('reason');
                    $leave->approver_comment = '';
                    $leave->status = 2;
                    if(isset($postData['adhoc_leave']) && $postData['adhoc_leave'] == 'on'){
                        $leave->is_adhoc = 1;
                    }else{
                        $leave->is_adhoc = 0;
                    }
                    // if((!isset($postData['adhoc_leave']))  &&  date('Y-m-d') > $leave->start_date){
                    //     Alert::error('You can not add leave of pervious days');
                    //     return Redirect::to('workfromhomeequests');
                    // }


                    $leave->save();
                    $leaveID = $leave->id;
                     $leaveDetails = $leave->getLeaveDetails($leaveID);

                    // $obj_leave_detail = new LeaveDetail();
                    // $obj_leave_detail->createLeaveDetail($leaveDetails);


                    $user = new User();
                    $update_leave = 0;
                    // if((isset($postData['adhoc_leave']) && $postData['adhoc_leave'] == 'on')&& $leaveDetails['leave_status'] == 1){
                    //     $userDetails = $user->getUserDetails($leaveDetails['user']['id']);
                    //     $update_leave = $userDetails['used_leaves'] + $leaveDetails['leave_days'];
                    //     $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                    // }

                    $data = array();
                    $data['id'] = $leaveID;
                    $data['reporting_name'] = $leaveDetails['approver']['first_name'].' '.$leaveDetails['approver']['last_name'];
                    $data['employee_name'] = $leaveDetails['user']['first_name'].' '.$leaveDetails['user']['last_name'];
                    $data['leave_start_date'] = date('jS F, Y', strtotime($leaveDetails['start_date']));
                    $data['leave_end_date'] = date('jS F, Y', strtotime($leaveDetails['end_date']));
                    $data['return_date'] = date('jS F, Y', strtotime($leaveDetails['return_date']));
                    $data['leave_days'] = $leaveDetails['days'];
                    $data['reason'] = $leaveDetails['reason'];


                    $authUser = Auth::user();
                    $company_id = $authUser->company_id;
                    $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany($company_id);
                    $cc = $bcc = '';
                    $cc_array = array($hr_admin_email['hr_email_address']);
                    $pm_email = getUsersPm(Auth::user()->id);
                    if($pm_email != false && $pm_email != $leaveDetails['approver']['email']){
                        $cc_array[] = $pm_email;
                    }
                    $cc = $cc_array;
                    $bcc = $hr_admin_email['email'];

                    $company     = new Company();
                    $userCompanyMailData  = $company->getCompanyViseMailData($company_id)[0];
                    $data['regards'] = $userCompanyMailData['company_name'];
                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name'];

                    $to = $leaveDetails['approver']['email'];
                    $template = 'emails.work_from_home_request';
                    $subject = 'Work From Home Application Of '.$data['employee_name'];
                    MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);

                    // redirect
                    Alert::success('Work From Home Request Successfully Created!');
                    return Redirect::to('workfromhomeequests');
                      }


                }
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
            if ((Auth::user()->hasPermission('team.leaves.listing') || checkReprtingPerson(auth()->user()->id)) ) {
                $authUser = Auth::user();
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;

                $leave = new WorkFromHome();
                $leaveDetails = $leave->getLeaveDetails($id);

                if(empty($leaveDetails)){
                    Alert::error('This request has been removed');
                    return Redirect::to('workfromhomeequests');
                }
                if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || in_array($leaveDetails['user_id'],getTeamUserId(auth()->user()->id))){
                    $user = new User();
                    $reportingToList = $user->getReportingToList();
                    return view('pms.work_from_home.edit', compact('leaveDetails', 'reportingToList'));
                } else {
                    Alert::error('You do not have permission to perform this action!');
                    return Redirect::to('dashboard');
                }
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check() && Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug'))) {
            $leave = WorkFromHome::find($id);
            $loggedInUserID = isset(auth()->user()->id) ? intval(auth()->user()->id) : 0;

            if(empty($leave)){
                Alert::error('This request has been removed');
                return Redirect::to('teamworkfromhome');
            }

            if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || in_array($leave['user_id'],getTeamUserId(auth()->user()->id))) {
                $postData = $request->input();

                $validationRule['status'] = 'required|integer';
                $validationRule['approver_comment'] = 'required|string|min:6';

                $validator = Validator::make(Input::all(), $validationRule);

                if ($validator->fails()) {
                    return Redirect::to('workfromhome/approve/'.$id)
                        ->withErrors($validator)
                        ->withInput();
                } else {
                    $beforeUpdate = $leave->getLeaveDetails($id);
                    $leave->approver_comment = Input::get('approver_comment');
                    $leave->status = Input::get('status');
                    $leave->approver_id = $loggedInUserID;
                    $leave->approver_date = date('Y-m-d');
                    $leave->save();

                    $this->updateAttendanceDetail([$leave->id]);

                    $leave = new WorkFromHome();
                    $leaveDetails = $leave->getLeaveDetails($id);


                    $leaveStatusArray = config('constant.leave_status');

                    $user = new User();
                    $update_leave = 0;

                    $data = array();
                    $data['id'] = $id;
                    $data['reporting_name'] = $leaveDetails['approver']['first_name'].' '.$leaveDetails['approver']['last_name'];
                    $data['employee_name'] = $leaveDetails['user']['first_name'].' '.$leaveDetails['user']['last_name'];
                    $data['leave_start_date'] = date('jS F, Y', strtotime($leaveDetails['start_date']));
                    $data['leave_end_date'] = date('jS F, Y', strtotime($leaveDetails['end_date']));
                    $data['leave_status'] = $leaveStatusArray[$leaveDetails['status']];

                    $company     = new Company();
                    $userCompanyMailData  = $company->getCompanyViseMailData($leaveDetails['user']['company_id'])[0];
                    $data['regards'] = $userCompanyMailData['company_name'];
                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name'];

                    $to = $leaveDetails['user']['email'];
                    $template = 'emails.work_from_home_approve';
                    $subject = 'Your Work From Home Application has been '.$data['leave_status'];

                    MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []);
                    // redirect
                    Alert::success('Work From Home Request Successfully Updated!');
                    return Redirect::to('teamworkfromhome');
                }
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function teamWorkFromHomeRequests(Request $request)
    {
       // dd("dsa");
        if (Auth::check()) {
            if ((Auth::user()->hasPermission('team.leaves.listing') || checkReprtingPerson(auth()->user()->id))) {

                 $year_start_date = date('Y-m-d',strtotime(date('01-04-Y')));
                    if(date('m-d',strtotime($year_start_date)) > date('m-d'))
                    {
                        $year = date("Y", strtotime("-1 years"));
                        $year_start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
                    }
                $company     = new Company();
                $companyList     = $company->getCompanyName();
                $leaveStatusSelected = '';
                if (isset($request->search_by_status) && $request->search_by_status != 2    ) {
                    $leaveStatusSelected = $request->search_by_status;
                }else{
                    $leaveStatusSelected = '2';
                }
                $userIdArray = $leavesData = $userNameArray = array();
                $authUser = Auth::user();
                $user = new User();

                $page       = $request->get('page');
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
                $start_date = $request->search_start_date;
                $end_date = $request->search_end_date;
                if (Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                    $company_id = '';
                    if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
                        $company_id = $authUser->company_id;
                    }
                    $allUserList = $userNameList = $user->getUserNameByCompany($company_id);
                } else {
                    $allUserList = $user->getUserNameByCompany($authUser->company_id);
                    $userNameList = $user->getMyTeamList($loggedInUserID);
                }

                $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
                if(!empty($userNameList)){
                    foreach ($userNameList as $userDetail) {
                        $userIdArray[] = $userDetail['id'];
                        $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
                if(!empty($allUserList)){
                    foreach ($allUserList as $userDetail) {
                        $allUserListArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
                $search_cmp = '';
                if ($request->has('search_submit') && $request->search_submit != '') {
                    if ($request->has('search_by_leave_emp_company') && $request->search_by_leave_emp_company != '') {
                        $search_cmp = $request->search_by_leave_emp_company;
                    }
                }

                $leave = WorkFromHome::with(['user','approver']);
                $leave->leftJoin('users as user','user.id','work_from_home.user_id');
                $leave->leftJoin('companies as c','c.id','user.company_id');
                $leave->where('user.status',1);
                $leave->whereIn('user_id', $userIdArray);
                if($search_cmp){
                    $leave->where('user.company_id','=',$search_cmp);
                }

                if(!empty($userIdArray)){
                    if ($userIdArray) {
                        if ($leaveStatusSelected != '' && $leaveStatusSelected != 0) {
                            $leave->whereIn('work_from_home.status', [$leaveStatusSelected]);
                        }
                    }
                }
                if ($request->has('search_submit') && $request->search_submit != '') {
                    // add by mohit
                    if ($request->has('search_by_department') && count($request->search_by_department) > 0 ){
                        $leave =   $leave->whereIn('user.department',($request->search_by_department));
                    }
                    if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                        $param = $request->search_by_internal_company_id;
                        if(in_array("parent_company",$param) && count($param) >= 2){
                            $leave =   $leave->where(function ($q) use ($param) {
                                $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                            });
                        } else if (in_array("parent_company",$param)) {
                            $leave =   $leave->WhereNull('internal_company_id');
                        } else {
                            $leave =   $leave->whereIn('internal_company_id', ($param));
                        }
                    }
                    // end by mohit
                    if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                        $leave->where('work_from_home.user_id',  '=', $request->search_by_employee);
                    }
                    if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                        $leave->where('work_from_home.approver_id',  '=', $request->search_by_approver);
                    }
                    if($request->has('adhoc_leave') && $request->adhoc_leave == 'on'){
                        $leave->where('work_from_home.is_adhoc',  '=', 1);
                    }
                    if ($request->has('search_start_date') && $request->search_start_date != '') {
                        $leave->where('work_from_home.start_date',  '>=', date("Y-m-d", strtotime($request->search_start_date)));
                    }
                    if ($request->has('search_end_date') && $request->search_end_date != '') {
                        $leave->where('work_from_home.end_date',  '<=',  date("Y-m-d", strtotime($request->search_end_date)));
                    }
                    if ($request->has('search_by_status') && $request->search_by_status != '' && $request->search_by_status != 0) {
                        $leave->where('work_from_home.status',  '=', $request->search_by_status);
                    }
                    if((!($request->has('search_start_date') && $request->search_start_date != '')) && (!($request->has('search_end_date') && $request->search_end_date != ''))){
                        //echo "dsa";exit;
                        $leave->where('work_from_home.start_date',  '>=', $year_start_date);
                    }

                }else{
                   // echo "dsa";exit;
                    $leave->where('work_from_home.start_date',  '>=', $year_start_date);
                }
                $leave->select('work_from_home.*','c.company_name');
                if ($request->has('sort') && $request->input('sort') != '') {
                    $leavesData = $leave->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
                } else {
                    $leavesData = $leave->sortable()->orderBy('work_from_home.start_date', 'DESC')->paginate($page_limit);
                }
                if (empty($userIdArray) && (Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->designation_id == 16 || Auth::user()->designation_id == 26 )){
                    $leavesData = array();
                }
              //  echo "<pre>";print_r($leavesData);exit;
                return view('pms.work_from_home.team_leave_requests', compact('allUserListArray','leavesData','companyList', 'loggedInUserID', 'leaveStatusSelected','page','userNameArray','request','start_date','end_date'));
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function updateCheckedWorkFromHomeAjax(Request $request) {
        $action = $request['action'];
        $user = new User();
        $update_leave = 0;
        if($action == 0){
            if(Auth::user()->hasRole(config('constant.admin_slug'))
            || Auth::user()->hasRole(config('constant.hr_slug'))
            || Auth::user()->hasRole(config('constant.superadmin_slug')))
            {
                $leave = new WorkFromHome();
                $leaveArray = $request['leaveIdArray'];
                foreach($leaveArray as $val){
                    $leaveEntry = WorkFromHome::find($val);
                    $leaveEntry->delete();

                }
                return Response::json(array('message'=>'Work From Home Entry Deleted Successfully','status'=>'success'));
            }else{
                return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
            }
        }else{
            $leave = new WorkFromHome();
            $leave = WorkFromHome::query()->whereIn('id',$request['leaveIdArray'])->update(['status' => $action,'approver_date' => date('Y-m-d')]);
            if(count($request['leaveIdArray']) > 0 && $action == 1){
                foreach($request['leaveIdArray'] as $val){
                    $this->updateAttendanceDetail([$val]);
                }
            }
        }
    }
    public function workFromHomeDelete(Request $request)
    {
        try{

            $update_leave = 0;
            $leaveId = $request->input('leaveId');
            $leaveEntry = WorkFromHome::find($leaveId);
            if(isset($leaveEntry))
            {
                if((!Auth::user()->hasRole(config('constant.admin_slug')))
                && (!Auth::user()->hasRole(config('constant.hr_slug')))
                && (!Auth::user()->hasRole(config('constant.superadmin_slug'))))
                {
                    if($leaveEntry->user_id != Auth::user()->id || date('Y-m-d') > $leaveEntry->start_date)
                    {
                        return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
                    }
                }

                $leaveEntry->delete();


                return Response::json(array('message'=>'Work From Home Entry Deleted Successfully','status'=>'success'));
            }else{
                return Response::json(array('message'=>'Something was wrong!','status'=>'error'));
            }
        }catch(Exception $e){
            return Response::json(array('message'=>'Work From Home Entry Deleted Successfully','status'=>'error'));
        }
    }

    public function isLeave($user_id,$date){
        $leave = WorkFromHome::query();
        $date = date("Y-m-d", strtotime($date));
        $leave->whereDate('leaves.start_date','<=', $date);
        $leave->whereDate('leaves.end_date','>=', $date);
        $leave->where('status', 1);   //Approved
        $leave->where('user_id', $user_id);   //Approved
        $leaveData = $leave->get();
        if(isset($leaveData[0])){
            return true;
        }else{
            return false;
        }
    }

    public function workFromHomeUserList(Request $request){

        if (Auth::check()) {
        if (Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) ||
        Auth::user()->hasRole(config('constant.superadmin_slug')) ||
        Auth::user()->hasRole('administration')) {

        $year_start_date = date('Y-m-d',strtotime(date('01-04-Y')));
        if(date('m-d',strtotime($year_start_date)) > date('m-d'))
        {
        $year = date("Y", strtotime("-1 years"));
        $year_start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
        }
        $company = new Company();
        $companyList = $company->getCompanyName();
        $leaveStatusSelected = '1';
        $userIdArray = $leavesData = $userNameArray = array();
        $authUser = Auth::user();
        $user = new User();

        $page = $request->get('page');
        $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
        $start_date = $request->search_start_date;
        $end_date = $request->search_end_date;
        if (Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) ||
        Auth::user()->hasRole(config('constant.superadmin_slug')) ||
        Auth::user()->hasRole('administration')) {
        $company_id = '';
        if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
        $company_id = $authUser->company_id;
        }
        $allUserList = $userNameList = $user->getUserNameByCompany($company_id);
        } else {
        $allUserList = $user->getUserNameByCompany($authUser->company_id);
        $userNameList = $user->getMyTeamList($loggedInUserID);
        }

        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        if(!empty($userNameList)){
        foreach ($userNameList as $userDetail) {
        $userIdArray[] = $userDetail['id'];
        $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
        }
        }
        if(!empty($allUserList)){
        foreach ($allUserList as $userDetail) {
        $allUserListArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
        }
        }
        $search_cmp = '';
        if ($request->has('search_submit') && $request->search_submit != '') {
        if ($request->has('search_by_leave_emp_company') && $request->search_by_leave_emp_company != '') {
        $search_cmp = $request->search_by_leave_emp_company;
        }
        }

        $leave = WorkFromHome::with(['user','approver']);
        $leave->leftJoin('users as user','user.id','work_from_home.user_id');
        $leave->leftJoin('companies as c','c.id','user.company_id');
        $leave->where('user.status',1);
        $leave->whereIn('user_id', $userIdArray);
        if($search_cmp){
        $leave->where('user.company_id','=',$search_cmp);
        }

        if(!empty($userIdArray)){
        if ($userIdArray) {
        if ($leaveStatusSelected != '' && $leaveStatusSelected != 0) {
        $leave->whereIn('work_from_home.status', [$leaveStatusSelected]);
        }
        }
        }
        if ($request->has('search_submit') && $request->search_submit != '') {
        if ($request->has('search_by_employee') && $request->search_by_employee != '') {
        $leave->where('work_from_home.user_id', '=', $request->search_by_employee);
        }
        if ($request->has('search_by_approver') && $request->search_by_approver != '') {
        $leave->where('work_from_home.approver_id', '=', $request->search_by_approver);
        }
        if($request->has('adhoc_leave') && $request->adhoc_leave == 'on'){
        $leave->where('work_from_home.is_adhoc', '=', 1);
        }
        if ($request->has('search_start_date') && $request->search_start_date != '') {
        $leave->where('work_from_home.start_date', '>=', date("Y-m-d", strtotime($request->search_start_date)));
        }
        if ($request->has('search_end_date') && $request->search_end_date != '') {
        $leave->where('work_from_home.end_date', '<=', date("Y-m-d", strtotime($request->search_end_date)));
            }
            if ($request->has('search_by_status') && $request->search_by_status != '' && $request->search_by_status !=
            0) {
            $leave->where('work_from_home.status', '=', $request->search_by_status);
            }
            if((!($request->has('search_start_date') && $request->search_start_date != '')) &&
            (!($request->has('search_end_date') && $request->search_end_date != ''))){
            //echo "dsa";exit;
            $leave->where('work_from_home.start_date', '>=', $year_start_date);
            }

            }else{
            // echo "dsa";exit;
            $leave->where('work_from_home.start_date', '>=', $year_start_date);
            }
            $leave->select('work_from_home.*','c.company_name');
            if ($request->has('sort') && $request->input('sort') != '') {
            $leavesData = $leave->sortable()->orderBy($request->input('sort'),
            $request->input('direction'))->paginate($page_limit);
            } else {
            $leavesData = $leave->sortable()->orderBy('work_from_home.start_date', 'DESC')->paginate($page_limit);
            }
            if (empty($userIdArray) && (Auth::user()->hasRole(config('constant.team_leader_slug')) ||
            Auth::user()->designation_id == 16 || Auth::user()->designation_id == 26 )){
            $leavesData = array();
            }
            // echo "
            // <pre>";print_r($leavesData);exit;
                return view('pms.work_from_home.show', compact('allUserListArray','leavesData','companyList', 'loggedInUserID', 'leaveStatusSelected','page','userNameArray','request','start_date','end_date'));
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function updateAttendanceDetail($array = []){
        if(isset($array) && count($array) > 0){
            $add = array();
            foreach($array as $value){
                $workFromHome = WorkFromHome::find($value);

                if(isset($workFromHome->status) && $workFromHome->status == 1 && $workFromHome->start_date < date("Y-m-d")){
                    if($workFromHome->days <= 1){
                        $date = date("Y-m-d",strtotime("$workFromHome->start_date"));
                            $leave = new Leave();
                            $isHoliday = $leave->isHoliday($date);
                            if($isHoliday != true){
                               
                                    $firstTime = "10:00:00";
                                    $lastTime = "18:30:00";
                                    if($workFromHome['start_type'] != "0" || $workFromHome['end_type'] != "0"){
                                        if($workFromHome['start_type'] == "1") {
                                             $lastTime = "14:30:00";
                                        } else {
                                            $firstTime = "15:00:00";
                                             $lastTime = "19:30:00";
                                        }
                                    }
                                    $attendance = Attendance::where(['user_id' => $workFromHome->user_id, 'entry_date' => date('Y-m-d',strtotime($date))])->whereNull('deleted_at')->orderBy('id','desc')->first();
                                    if(isset($attendance->id) && $attendance->id != ""){    
                                        $request = new \Illuminate\Http\Request();
                                        $request->replace([
                                            'hidenTotalElement' => 1,
                                            'hiddenAttendanceId' => $attendance->id,
                                            'inEntry_1' => $firstTime,
                                            'outEntry_1' => $lastTime,
                                            'request_for' => "work_from_home",
                                            'user_id' => $workFromHome->user_id
                                        ]);
                                        $result = (new TimeSheetController)->updateEntry($request);
                                    }                               
                            }    
                    }else{
                        $days = $workFromHome->days;
                        $checkDays = 0;
                        $leave = new Leave();
                        while($checkDays <= $days){
                            $date = date("Y-m-d",strtotime("$workFromHome->start_date +".$checkDays." days"));
                            $isHoliday = $leave->isHoliday($date);
                            if($isHoliday != true){
                                if($date < date("Y-m-d")){
                                    $attendance = Attendance::where(['user_id' => $workFromHome->user_id, 'entry_date' => date('Y-m-d',strtotime($date))])->whereNull('deleted_at')->orderBy('id','desc')->first();
                                    if(isset($attendance->id) && $attendance->id != ""){
                                        $request = new \Illuminate\Http\Request();
                                        $request->replace([
                                            'hidenTotalElement' => 1,
                                            'hiddenAttendanceId' => $attendance->id,
                                            'inEntry_1' => '10:00:00',
                                            'outEntry_1' => '18:30:00',
                                            'request_for' => "work_from_home",
                                            'user_id' => $workFromHome->user_id
                                        ]);
                                        $result = (new TimeSheetController)->updateEntry($request);
                                    }
                                }
                            } else {
                                $days+=1;
                            }
                            $checkDays++;
                        }
                    }
                }
            }
        }
    }
}
