<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;
use Session;

class AdminController extends Controller
{
    public function login(Request $request){
        return redirect('/pms');
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'is_admin' => '1'])) {
                Session::put('adminSession',$data['email']);
                return redirect('/admin/dashboard');
            } else {
                return redirect('/admin')->with('flash_message_error', 'Invalid Email or Password');
            }
        }
        return view('admin.admin_login');
        
        
    }
    public function dashboard(){
        if(Session::has('adminSession')){

        }
        else{
            return redirect('/admin')->with('flash_message_error', 'Please login to access');
        }
        return view('admin.dashboard');   
    }
    public function settings(){
        return view('admin.settings');
    }
    public function logout(){
        Session::flush();  
        return redirect('/admin')->with('flash_message_success', 'Logged out Successfully'); 
    }
}
