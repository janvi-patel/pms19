<?php

namespace App\Http\Controllers;

use App\ProjectEntry;
use App\Task;
use App\Project;
use App\User;
use Carbon\Carbon;
use Excel;
use App\MiscBeanchTask;
use App\Attendance;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Exception;
use Alert;
use Illuminate\Support\Facades\URL;
use Response;
use App\Setting;
use App\ProjectTasks;
use App\CompOff;

class ProjectEntryController extends Controller
{ 
    public $totalOffDay = 0;
    public function viewTaskEntry(Request $request,$projectId,$userId)
    {
        if(Auth::check() && Auth::user()->hasPermission('task.entry.listing')){
            $projectEntry = ProjectEntry::where('project_id',$projectId);
                $projectEntry->where('user_id',$userId);
            if(Auth::user()->hasRole(config('constant.developer_slug'))){
            }
            $getProjectData = (new \App\Helpers\CommonHelper)->getProjectData($projectId);
            if($request->has('search_submit') && $request->search_submit != '') {
                if($request->search_by_log_from_date != '' && $request->search_by_log_to_date != ''){
                    $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                    $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                    $projectEntry->whereBetween('log_date', [$fromDate, $toDate]);
                }else{
                   if($request->has('search_by_log_from_date') && $request->search_by_log_from_date != '') {
                        $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                        $projectEntry->where('log_date', 'like', '%' . $fromDate . '%');
                    }
                    if ($request->has('search_by_log_to_date') && $request->search_by_log_to_date != '') {
                        $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                        $projectEntry->where('log_date', 'like', '%' . $toDate . '%');
                    }  
                }
            }

            $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            
            $projectEntry = $projectEntry->whereNull('task_id')->sortable()->orderBy('log_date', 'desc')->paginate($page_limit);

            return view('pms.project_entry.index')->with('getProjectData',$getProjectData)
                    ->with('projectEntry',$projectEntry)->with('projectId',$projectId)->with('userId',$userId)
                    ->with('request',$request);
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function viewProjectTaskEntry(Request $request,$taskId) {
        if(Auth::check() && Auth::user()->hasPermission('task.entry.listing')){
          
            $projectEntry = ProjectEntry::select('project_entries.*','pt.estimated_hours as taskHours','u.first_name','u.last_name')
                                    ->leftJoin('project_tasks as pt','project_entries.task_id','pt.id')
                                    ->leftJoin('users as u','project_entries.user_id','u.id')
                                    ->where('pt.id',$taskId);
            $projectHours = ProjectEntry::select('p.project_name',DB::raw("p.`pms_hours` as total_project_hours"),'pt.task_desc',
                                            DB::raw("sum(cr.`pms_hours`) as total_cr_hours"),'pt.task_name')
                                    ->leftJoin('project_tasks as pt','project_entries.task_id','pt.id')
                                    ->leftJoin('users as u','project_entries.user_id','u.id')
                                    ->leftJoin('projects as p','project_entries.project_id','p.id')
                                    ->leftjoin('cr','p.id','cr.project_id')->whereNull('cr.deleted_at')
                                    ->where('pt.id',$taskId)->first();
            if(Auth::user()->hasRole(config('constant.developer_slug'))){
                $projectEntry->where('project_entries.user_id',Auth::user()->id);
            }
            if($request->has('search_submit') && $request->search_submit != '') {
                if($request->search_by_log_from_date != '' && $request->search_by_log_to_date != ''){
                    $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                    $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                    $projectEntry->whereBetween('log_date', [$fromDate, $toDate]);
                }else{
                   if($request->has('search_by_log_from_date') && $request->search_by_log_from_date != '') {
                        $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                        $projectEntry->where('log_date', 'like', '%' . $fromDate . '%');
                    }
                    if ($request->has('search_by_log_to_date') && $request->search_by_log_to_date != '') {
                        $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                        $projectEntry->where('log_date', 'like', '%' . $toDate . '%');
                    }  
                }
            }
            $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $projectEntry = $projectEntry->sortable()->orderBy('project_entries.id', 'desc')->paginate($page_limit);
            return view('pms.project_entry.project_task_entry')->with('projectEntry',$projectEntry)->with('taskId',$taskId)->with('request',$request)
                    ->with('projectHours',$projectHours);
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function viewProjectTaskEntryModal($taskId) {
        $projectEntry = ProjectEntry::select('project_entries.*','pt.estimated_hours as taskHours','u.first_name','u.last_name')
                                ->leftJoin('project_tasks as pt','project_entries.task_id','pt.id')
                                ->leftJoin('users as u','project_entries.user_id','u.id')
                                ->where('pt.id',$taskId)
                                ->orderBy('project_entries.id', 'desc')->get()->toArray();
        echo view('dashboard.task_entry_modal',compact('projectEntry','taskId'));
    }

    public function viewProjectOldTaskEntry(Request $request,$projectId) {
        if(Auth::check() && Auth::user()->hasPermission('task.entry.listing')){
            $projectEntry = ProjectEntry::select('project_entries.*')
                    ->leftJoin('project_tasks as pt','project_entries.project_id','pt.id')
                    ->where('pt.id',$projectId)->where('task_id',NULL);
            
            if(Auth::user()->hasRole(config('constant.developer_slug'))){
                $projectEntry->where('project_entries.user_id',Auth::user()->id);
            }
            if($request->has('search_submit') && $request->search_submit != '') {
                if($request->search_by_log_from_date != '' && $request->search_by_log_to_date != ''){
                    $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                    $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                    $projectEntry->whereBetween('log_date', [$fromDate, $toDate]);
                }else{
                   if($request->has('search_by_log_from_date') && $request->search_by_log_from_date != '') {
                        $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                        $projectEntry->where('log_date', 'like', '%' . $fromDate . '%');
                    }
                    if ($request->has('search_by_log_to_date') && $request->search_by_log_to_date != '') {
                        $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                        $projectEntry->where('log_date', 'like', '%' . $toDate . '%');
                    }  
                }
            }
            $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            
            $projectEntry = $projectEntry->sortable()->orderBy('project_entries.id', 'desc')->paginate($page_limit);
            return view('pms.project_entry.project_old_entry')->with('projectEntry',$projectEntry)->with('projectId',$projectId)->with('request',$request);
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    
    public function viewProjectEntry(Request $request,$projectId,$userId)
    {
        if(Auth::check() && Auth::user()->hasPermission('task.entry.listing')){
            $projectEntry = ProjectTasks::leftjoin('project_entries as pe', function($leftJoin)
                            {
                                $leftJoin->on('project_tasks.id', '=','pe.task_id')
                                ->whereNull('pe.deleted_at');
                            }) 
                                ->where('project_tasks.project_id',$projectId)
                                ->where('project_tasks.user_id',$userId)
                                ->groupby('project_tasks.id')->whereNull('pe.deleted_at')
                                ->select('project_tasks.*',DB::raw("sum(pe.`log_hours`) as total_logged_hours"));
            
            $oldEntry = ProjectEntry::where('project_id',$projectId)->where('user_id',$userId);
            
            if($request->has('search_submit') && $request->search_submit != '') {
                if($request->search_by_log_from_date != '' && $request->search_by_log_from_date != ''){
                    $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                    $projectEntry->where('task_start_date', '=',$fromDate );
                }
                if ($request->has('search_by_log_to_date') && $request->search_by_log_to_date != '') {
                    $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                    $projectEntry->where('task_end_date','=',$toDate);
                }  
            }
            $oldEntry = $oldEntry->select('project_id',DB::raw("sum(`log_hours`) as total_logged_hours"))->whereNull('deleted_at')->whereNull('task_id')->first();
            $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $projectEntry = $projectEntry->sortable()->orderBy('project_tasks.id', 'desc')->paginate($page_limit);
            return view('pms.task.view_team_project_task')->with('projectEntry',$projectEntry)
                        ->with('oldEntry',$oldEntry)->with('userId',$userId)
                        ->with('projectId',$projectId)->with('request',$request);
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    
    public function saveTaskEntry(Request $request)
    {
        try{
            if(isset($request->projectType) && $request->projectType == 'misc_bench'){
                $rules = array(
                    'taskDate'        => 'required',
                    'workTimeHours'   => 'required',
                    'workTimeMinutes' => 'required',
                );
            }else{
                $rules = array(
                    'taskDate'        => 'required',
                    'workTimeHours'   => 'required',
                    'workTimeMinutes' => 'required',
                    'task_list' => 'required',
                );
            }
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                Alert::error('Time Entry Not Successfully Added, Please enter all required field!');
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
           
    
         
            // $findTimeEntry = Attendance::where('emp_code',Auth::User()->employee_id)->where('entry_date',date('Y-m-d',strtotime($request->input('taskDate'))))->first();
              
            // if(empty($findTimeEntry)){
            //       $dt= date('Y-m-d',strtotime($request->input('taskDate')));
            //       $isCompOffAvailble = DB::table('comp_offs')->where('compoff_status',1)->where('user_id',Auth::User()->id)
            //     ->whereRaw('"'.$dt.'" between `compoff_start_date` and `compoff_end_date`')->whereNull('deleted_at')
            //     ->get();
                
            //        if($isCompOffAvailble->isEmpty())
            //             {

            //                 alert()->info('your time entry is pending for this day by HR.')->persistent('close')->autoclose("3600");
            //                     return Redirect::back();
            //             }
            // }
            $taskId = $request->input('projectTaskId');
            $taskEntryDate = date('Y-m-d',strtotime($request->input('taskDate')));
            
            $workTimeMinutes = (new \App\Helpers\CommonHelper)->inserTaskTime($request->input('workTimeMinutes'));
            $obj                  = new ProjectEntry();
            $obj->project_id      = $request->input('projectId');
            $obj->log_date        = date('Y-m-d',strtotime($request->input('taskDate')));
            $obj->log_hours       = $request->input('workTimeHours').'.'.$workTimeMinutes;
            $obj->task_id         = (isset($request->projectType) && $request->projectType == 'misc_bench')?
                                        NULL:$request->input('task_list');
            $obj->desc            = $request->input('taskDescription');
            $obj->user_id         = Auth::user()->id;
            $projEntry = new ProjectEntry();
            $verifyTaskDate = $projEntry->isTaskEntryIsValid($obj->task_id,$obj->project_id,$obj->log_date);
            if($verifyTaskDate)
            {
                /* check taskentry validation for days */
                $attCont = new AttendancesController();
                $leave = new LeaveController();
                $setting = new Setting();
                $userId = Auth::user()->id;
                $settingData = $setting->getSettingData('TaskEntry')[0];
                $days=$settingData['days'];
                for($i=1;$i<=$days;$i++){
                    $checkDay =date('d-m-Y',strtotime("-".$i."days"));
                    if($attCont->isHoliday($checkDay)){
                        $days++;
                    }else{
                        if($leave->isLeave($userId, $checkDay)){
                            $days++;
                        }
                    }     
                }
                $entryDate = date('d-m-Y',strtotime($obj->log_date));
                $validDate = date('d-m-Y',strtotime("-".$days."days"));
                if(strtotime($entryDate) < strtotime($validDate)){
                    Alert::error('You can only add time Entry up to '.$settingData['days'].' days back.');
                    return Redirect::back();
                }else{
                    $obj->save();
                    Alert::success('Time Entry Successfully Added');
                    return Redirect::back();
                }
            }
            else{
                Alert::error('Something Was Wrong !!');
                return Redirect::back();
            }
        }catch(Exception $e){
            Alert::error('Time Entry Not Successfully Added');
            return Redirect::back();
        }
    }

    public function deleteTaskEntry(Request $request)
    {
        try{
            $projectEntryId = $request->input('projectEntryId');
            $taskEntry = ProjectEntry::find($projectEntryId);
            
            $attCont = new AttendancesController();
            $leave = new LeaveController();
            $setting = new Setting();
            $userId = $taskEntry->user_id;
            $settingData = $setting->getSettingData('TaskEntry')[0];
            $days=$settingData['days'];
            for($i=1;$i<=$days;$i++){
                $checkDay =date('d-m-Y',strtotime("-".$i."days"));
                if($attCont->isHoliday($checkDay)){
                    $days++;
                }else{
                    if($leave->isLeave($userId, $checkDay)){
                        $days++;
                    }
                }     
            }
            $entryDate = date('d-m-Y',strtotime($taskEntry->log_date));
            $validDate = date('d-m-Y',strtotime("-".$days."days"));
            if(strtotime($entryDate) < strtotime($validDate)){
                return Response::json(array('message'=>'You can only delete entries up to '.$settingData['days'].' days back.','status'=>'error'));
            }else{
                $taskEntry->delete();
            }
            return Response::json(array('message'=>'Project Entry Deleted Successfully','status'=>'success'));
        }catch(Exception $e){
            return Response::json(array('message'=>'Project Entry Not Deleted Successfully','status'=>'error'));
        }
    }
    
    public function editTaskEntry(Request $request)
    {
        $projectEntryId = $request->input('projectEntryId');
        $projectEntry = ProjectEntry::find($projectEntryId)->toArray();
        $projectEntry['project_start_date'] = Project::find($projectEntry['project_id'])->project_start_date;
        return view('pms.project_entry.modal.edit_task_time_entry',compact('projectEntry'));
    }

    public function updateTaskEntry(Request $request)
    {
        $projectId = $request->input('editProjectId');
        $editProjectEntryId = $request->input('editProjectEntryId');
        $editTaskId = $request->input('editTaskId');
        $pageIdentifier = $request->input('pageLink');
        $redirectLink = 'view/project_entry/'.$projectId;
        if(strpos($pageIdentifier, 'view/user_project_entry/') !== false ){
            $redirectLink =  'view/user_project_entry/'.$projectId.'/'.Auth::user()->id;
        }if(strpos($pageIdentifier, 'view/project_task_entry') !== false){
            $redirectLink =  'view/project_task_entry/'.$editTaskId;
        }if(strpos($pageIdentifier, 'view/project_misc_task_entry') !== false){
            $redirectLink =  'view/project_misc_task_entry/'.substr($pageIdentifier, strrpos($pageIdentifier, '/') + 1);
        }if(strpos($pageIdentifier, 'detailSummary') !== false){
            $redirectLink =  'detailSummary/'.substr($pageIdentifier, strrpos($pageIdentifier, '/') + 1);
        }
        try{
            $obj = ProjectEntry::find($editProjectEntryId);

            if(!$obj){
                Alert::error('No Record Found!');
                return Redirect::to($redirectLink);
            }
            $projectName = Project::find($projectId);
            if($projectName != ''){
                if($projectName->project_name != 'Miscellaneous Tasks' && $projectName->project_name != 'Bench'){
                    if(Auth::user()->company_id != 1){
                        $myTaskTotalHours = 0;
                        $myTaskHour = (new \App\Helpers\CommonHelper)->getTaskHours($projectId,Auth::user()->id);
                        $myTaskTotalHours = $myTaskTotalHours + $myTaskHour->total_task_hours;
                        if($myTaskTotalHours <= 0 ){
                            Alert::error('Please ask your project team leader to create task for this project.'); 
                            return Redirect::back();
                        }
                    }
                }
            }
            
            $attCont = new AttendancesController();
            $leave = new LeaveController();
            $setting = new Setting();
            $userId = $obj->user_id;
            $settingData = $setting->getSettingData('TaskEntry')[0];
            $days=$settingData['days'];
            for($i=1;$i<=$days;$i++){
                $checkDay =date('d-m-Y',strtotime("-".$i."days"));
                if($attCont->isHoliday($checkDay)){
                    $days++;
                }else{
                    if($leave->isLeave($userId, $checkDay)){
                        $days++;
                    }
                }     
            }
            $entryDate = $request->input('editTaskDate');
            $validDate = date('d-m-Y',strtotime("-".$days."days"));
            if(strtotime($entryDate) < strtotime($validDate)){
                Alert::error('You can only update entries up to '.$settingData['days'].' days back.');
                return Redirect::back();
            }else{
                $workTimeMinutes = (new \App\Helpers\CommonHelper)->inserTaskTime($request->input('editWorkTimeMinutes'));
                $obj->log_date        = date('Y-m-d',strtotime($request->input('editTaskDate')));
                $obj->log_hours       = $request->input('editWorkTimeHours').'.'.$workTimeMinutes;
                $obj->desc            = $request->input('editTaskDescription');
                if(isset($request->projectType) && $request->projectType == 'misc_bench'){
                    $obj->task_id         = NULL;
                }else{
                    $obj->task_id         = ($request->input('task_list'))?$request->input('task_list'):NULL;
                }
                $projEntry = new ProjectEntry();
                $isTaskEntryIsValid = $projEntry->isTaskEntryIsValid($obj->task_id,$obj->project_id,$obj->log_date);
                if($isTaskEntryIsValid)
                {
                    $obj->save();
                    if(($projectName != '')){
                        if($projectName->project_name == 'Miscellaneous Tasks' || $projectName->project_name == 'Bench'){
                            Alert::success('Project Entry Successfully Updated');
                            return back();
                        }
                    }
                    Alert::success('Project Entry Successfully Updated');
                    return Redirect::to($redirectLink);
                }
                else{
                    Alert::error('Something Was Wrong !!');
                    return Redirect::back();
                } 
            }
        }catch(Exception $e){
            Alert::error('Project Entry Not Successfully Updated!');
            return Redirect::to($redirectLink);
        }
    }

    public function deleteMultipleTaskEntry(Request $request)
    {
        try{
            $errorMsg = array();
            $taskEntryIdsArray = $request->input('taskEntryIdsArray');
            $attCont = new AttendancesController();
            $leave = new LeaveController();
            $setting = new Setting();
           
            $settingData = $setting->getSettingData('TaskEntry')[0];
            foreach($taskEntryIdsArray as $value){
                $taskEntry = ProjectEntry::find($value);
                if($taskEntry){
                    $userId = $taskEntry->user_id;
                    $days=$settingData['days'];
                    for($i=1;$i<=$days;$i++){
                        $checkDay =date('d-m-Y',strtotime("-".$i."days"));
                        if($attCont->isHoliday($checkDay)){
                            $days++;
                        }else{
                            if($leave->isLeave($userId, $checkDay)){
                                $days++;
                            }
                        }     
                    }
                    $entryDate = date('d-m-Y',strtotime($taskEntry->log_date));
                    $validDate = date('d-m-Y',strtotime("-".$days."days"));
                    if(strtotime($entryDate) < strtotime($validDate)){
                        $errorMsg[] = $taskEntry->id;
                    }
                }
            }
            if(!empty($errorMsg)){
                return Response::json(array('message'=>'You can only delete entries up to '.$settingData['days'].' days back.','status'=>'error'));
            }else{
                foreach($taskEntryIdsArray as $value){
                    $taskEntry = ProjectEntry::find($value);

                    if($taskEntry){
                        $taskEntry->delete();
                    }
                }

                return Response::json(array('message'=>'Project Entry Deleted Successfully','status'=>'success'));
            }
        }catch(Exception $e){
            return Response::json(array('message'=>'Project Entry Not Deleted Successfully','status'=>'error'));
        }
    }
    
    public function remainingProEntry() {
        $projectLogeedHours = ProjectEntry::where('log_date',date('Y-m-d',strtotime("-1 days")))->where('user_id',Auth::user()->id)
                ->select(DB::raw("floor(sum(floor(`log_hours`)) + sum(`log_hours` - floor(`log_hours`)) * 100.0/60) +
                                ((sum(floor(`log_hours`)) + sum(`log_hours` - floor(`log_hours`)) * 100.0/60) % 1) * 60.0/100  as logged_hours"))
                ->groupBy('log_date')
                ->first();
        $result = array();
        if($projectLogeedHours != ''){
            $result['hours'] = '0';
            $result['min'] = '0';
            $start = Carbon::parse(sprintf("%.2f",$projectLogeedHours->logged_hours)); //$projectLogeedHours->logged_hours
            $end = Carbon::parse(config('constant.project_entry'));
            if($end > $start){
                $duration = $end->diff($start);
                $result['hours'] = $duration->h;
                $result['min'] = $duration->i;
            }
        }
        return $result;
    }
    
    public function checkTimeEntry(){
        $setting = new Setting();
        $settingData = $setting->getSettingData('TaskEntry')[0];
        $attCont = new AttendancesController();
        $leave = new LeaveController();
        $userId = Auth::user()->id;
        $days=$settingData['days'];
        for($i=1;$i<=$days;$i++){
            $checkDay =date('d-m-Y',strtotime("-".$i."days"));
            if($attCont->isHoliday($checkDay)){
                $days++;
            }else{
                if($leave->isLeave($userId, $checkDay)){
                    $days++;
                }
            }     
        }
        $validDate = date('d-m-Y',strtotime("-".$days."days"));
        return $validDate;
    }
    
    public function checkDay($checkDay,$day){
        $attCont = new AttendancesController();
        $leave = new LeaveController();
        $userId = Auth::user()->id;
        $count = 0;
        if($attCont->isHoliday($checkDay)){
            $day+=86400;
            $count++;
            $this->totalOffDay++;
        }else{
            if($leave->isLeave($userId, $checkDay)){
                $day+=86400;
                $count++;
                $this->totalOffDay++;
            }
        }
        $day = $day;
        if($count != 0){
           $this->checkDay(date('d.m.Y',strtotime("-".$day/(24 * 3600)."days")),$day);
        }
        return  $this->totalOffDay;
    }
}
