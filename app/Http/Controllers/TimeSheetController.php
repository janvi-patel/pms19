<?php

namespace App\Http\Controllers;

use App\pms;
use App\User;
use App\Holiday;
use App\TaskEntry;
use App\ProjectEntry;
use App\Task;
use App\LateFrom;
use App\Company;
use Excel;
use Illuminate\Http\Request;
use Config;
use Validator;
use App\Leave;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
Use Alert;
use App\Jobs\UploadTimeEntries;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\LeaveController;
use App\Imports\TimeEntriesImport;
use App\AttendanceDetail;
use App\Attendance;
use App\Designation;
use App\Setting;
use App\UserWorkFromHome;
use DateTime;
use Exception;
use Response;
use Carbon\Carbon;
use App\Jobs\MailJob;
use App\TimeEntryStatus;
use App\Exports\ExportFailedTimeEntryStatus;
use TimeSheetHelper;
use App\LeaveDetail;
class TimeSheetController extends Controller {

    public function viewTimesheet(Request $request) {
        // dd($request->all());
        $user = new User();
        $company = new Company();
         $designation = new Designation();
        $authUser = Auth::user();
        $comapny_id = '';
        $comp_id = Auth::user()->company_id;
        $comp_name = $company->getCompanyDataById($comp_id)->company_name;
        $taskEntry = new TaskEntry();
        $current_years = date('Y');
        $current_month = date('m');
        $departmentArray = array(5, 8, 9);
        $myTeamUserEmpIdArray = array();
        $myTeamUserDesiIdArray = array();

        $userListing = $user->getUserDropDown();


        if (Auth::check() && (Auth::user()->hasRole('admin') || Auth::user()->hasRole('hr') || Auth::user()->hasRole(config('constant.superadmin_slug'))) || Auth::user()->hasRole('Networking') || Auth::user()->hasRole('administration')) {
            $company_id = $authUser->company_id;
            $userListing = $user->getUserNameByCompany($company_id)->toArray();
        } else {
            $myTeamUserEmpIdArray[] = auth()->user()->employee_id;
            $userListing = $user->getMyTeamList(auth()->user()->id);
        }
        if(isset($userListing) && count($userListing) > 0) {
            foreach ($userListing as $myTeamUserEmpIdVal) {
                $myTeamUserEmpIdArray[] = $myTeamUserEmpIdVal['employee_id'];
                $myTeamUserDesiIdArray[] = $myTeamUserEmpIdVal['designation_id'];
            }
        }
        $designationList = Designation::whereIn('id', array_unique($myTeamUserDesiIdArray))->get();
        for ($y = config('constant.Start_year'); $y <= $current_years; $y++) {
            $years[$y] = $y;
        }
        $data = $request->all();
        $data['employee'] = isset($data['search_employee']) ? $data['search_employee'] : Auth::User()->employee_id;
        $data['request_month'] = isset($data['search_month']) ? (($data['search_month'] <= 9) ? '0' . $data['search_month'] : $data['search_month']) : date('m');
        $data['request_year'] = isset($data['search_year']) ? $data['search_year'] : date('Y');

        $companyName = $company->getCompanyNameById(Auth::user()->company_id)->company_name;
        $query = Attendance::selectRaw('emp_code, concat(`users`.`first_name`," ",`users`.`last_name`) as emp_name,
        SUM(full_day) as full_days, SUM(half_day) as half_days, SUM(early_going) as early_going_days,SUM(adjustment) as
        adjustment,SUM(absent) as absent_days,
        SUM(late_comer) as late_days, SUM(incorrect_entry) as incorrect_days,users.reporting_to,users.department,users.company_id,users.id as userId, users.designation_id');
        $query->leftJoin('users', 'emp_code', 'users.employee_id');
        if (Auth::check() && (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('hr'))) {
            $query->where('users.status', 1);
        }
        $query->where('emp_comp', $companyName);
        $query->where('users.company_id', Auth::user()->company_id);
        $query->whereIn('emp_code', $myTeamUserEmpIdArray);

        $reportingList = clone $query;
        // Search

        if (Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin'))) {
            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_department') && $request->search_department != '') {
                    $query->whereIn('department', ($request->search_department));
                }
            } else {
                $query->whereNotIn('department', $departmentArray);
            }
        }
        if ($request->has('search_submit') && $request->search_submit != '') {
            $currentYear = $currentMonth = '';
            if ($request->has('search_employee') && $request->get('search_employee') != '') {
                $query->where('emp_code', $request->get('search_employee'));
            }

            if ($request->has('search_by_designation') && $request->get('search_by_designation') != '') {
                $query->where('users.designation_id', $request->get('search_by_designation'));
            }

            $query->whereMonth('entry_date', sprintf("%02d", $request->get('search_month')));
            $query->whereYear('entry_date', $request->get('search_year'));

            if (($request->has('search_reporting_to')) && $request->search_reporting_to != '') {
                $userListing = $user->getMyTeamList($request->search_reporting_to);
                $teamUserArray = $teamUserName = array();
                foreach ($userListing as $userListingVal){
                    $teamUserArray[] = $userListingVal['id'];
                }
                $query->whereIn('users.id',$teamUserArray);
            }
            if (($request->has('search_department')) && $request->search_department != '' && !(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin')))) {
                $query->where('users.department', $request->search_department);
            }
            //
            $firstDateOfMonth = $request->get('search_year') . '-' . sprintf("%02d", $request->get('search_month')) . '-' . '01';
            $lastDateOfMonth = new \DateTime($request->get('search_year') . '-' . sprintf("%02d", $request->get('search_month')));
            $lastDateOfMonth->modify('last day of this month');
            $lastDateOfMonth = $lastDateOfMonth->format('Y-m-d');
        } else {
            $currentYear = date('Y');
            $currentMonth = date('m');

            if (isset($data['request_month']) && $data['request_month']) {
                $query->whereMonth('entry_date', $data['request_month']);
                $firstDateOfMonth = $data['request_year'] . '-' . $data['request_month'] . '-' . '01';
            }
            if (isset($data['request_year']) && $data['request_year']) {
                $query->whereYear('entry_date', $data['request_year']);
                $lastDateOfMonth = new \DateTime($data['request_year'] . '-' . $data['request_month']);
                $lastDateOfMonth->modify('last day of this month');
                $lastDateOfMonth = $lastDateOfMonth->format('Y-m-d');
            }
        }
        $records = $query->sortable()->groupBy('emp_code')->get();
        $timeEntry = array();
        $key = 1;
        foreach($records as $rec){
            if($rec->emp_code == auth()->user()->employee_id){
                $timeEntry[0] = $rec;
            } else {
                $timeEntry[$key] = $rec;
                $key++;
            }
        }
        ksort($timeEntry);
        $records = $timeEntry;
        $loggedHours = ProjectEntry::select('users.id', DB::raw("floor(sum(floor(project_entries.`log_hours`)) + sum(project_entries.`log_hours` - floor(project_entries.`log_hours`)) * 100.0/60) +
        ((sum(floor(project_entries.`log_hours`)) + sum(project_entries.`log_hours` - floor(project_entries.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_logged_hours_bkp"), DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                        ->leftjoin('users', 'users.id', 'user_id')
                        ->where('users.status',1)
                        ->where(function($q) use ($firstDateOfMonth, $lastDateOfMonth) {
                            $q->whereBetween('project_entries.log_date', [$firstDateOfMonth, $lastDateOfMonth]);
                        })
                        ->groupBy('project_entries.user_id')
                        ->pluck('total_logged_hours', 'users.id')->toArray();
        $loggedHours = array_filter($loggedHours);
        $total_days_array = $working_details =  $total_days_qry = array();
        if (!empty($records)) {
            foreach ($records as $key => $value) {
                    if(isset($request->search_month) && !empty($request->search_month)){
                        $detailMonth = $request->search_month;
                    }else{
                        $detailMonth = $currentMonth;
                    }
                    if(isset($request->search_year) && !empty($request->search_year)){
                        $detailYear = $request->search_year;
                    }else{
                        $detailYear = $currentYear;
                    }
                    $startdatestring = "01-".$detailMonth."-".$detailYear;
                    $date1 = 'first day of '.date("M",strtotime($startdatestring));
                    $date2 = 'last day of '.date("M",strtotime($startdatestring));
                    $startMonthDate = $newDate = date('Y-m-d', strtotime($date1));
                    $lastMonthDate = $newDate = date('Y-m-d', strtotime($date2));
                    $value['leaveDetails'] = getUserMonthlyLeaveDetails(["where" => ['user_id' => $value->userId ],"between" => ["start_date" => $startMonthDate, "end_date" => $lastMonthDate]]);
            }
            if (date('m') == $data['request_month']) {
                $total_days_in_month = date('d');
            } else {
                $total_days_in_month = date('t', mktime(0, 0, 0, $data['request_month'], 1, $data['request_year']));
            }
            $total_week_days = totalWeekDays($data['request_month'], $data['request_year']);

            $query_atte = AttendanceDetail::selectRaw('attendances.emp_code, SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))) AS working_hours')
                    ->rightJoin('attendances', 'attendances.id', '=', 'attendance_details.attendance_id')
                    ->where('attendances.absent', NULL)
                    ->where('first_in', '!=', '00:00:00')
                    ->where('last_out', '!=', '00:00:00')
                    ->where('emp_comp',$comp_name)
                    ->where('attendances.incorrect_entry', NULL);
           // if (Auth::check() && (Auth::user()->hasRole('admin') || Auth::user()->hasRole('hr') || Auth::user()->hasRole('super_admin')) || Auth::user()->hasRole('pm')  || Auth::user()->hasRole('tl') || Auth::user()->designation_id == 16 || Auth::user()->hasRole('administration')) {
                $data['employee'] = '';
                if ($data['employee']) {
                    $query_atte->where('emp_code', $data['employee']);
                }
            // } elseif (isset(Auth::User()->employee_id)) {
            //     $query_atte->where('emp_code', Auth::User()->employee_id);
            // }
            if (isset($data['request_month']) && $data['request_month']) {
                $query_atte->whereMonth('entry_date', $data['request_month']);
            }
            if (isset($data['request_year']) && $data['request_year']) {
                $query_atte->whereYear('entry_date', $data['request_year']);
            }
            $query_atte->groupBy('attendance_details.attendance_id');
            $working_details = $query_atte->get()->toArray();
            $total_days_qry = Attendance::select('entry_date');
            if (isset($request['search_month']) || isset($request['search_month'])) {
                if ($request['search_month']) {
                    $total_days_qry->whereMonth('entry_date', $request['search_month']);
                }
                if ($request['search_year']) {
                    $total_days_qry->whereYear('entry_date', $request['search_year']);
                }
            } else {
                $total_days_qry->where('entry_date', 'LIKE', '%' . $current_years . '-' . $current_month . '%');
            }
            $total_days_qry->groupby('entry_date');
            $total_days_array = $total_days_qry->get()->toArray();
        }

        $total_days = 0;
        foreach ($total_days_array as $total_days_array_val) {
            if (!Holiday::getHolidayDateList($total_days_array_val['entry_date'])) {
                $date = $total_days_array_val['entry_date'];
                $isWeekend = Holiday::getWeekendDateList($date);
                if(count($isWeekend) == 0){
                    $total_days++;
                }
            }
        }

        $userAttendanceDetails = AttendanceDetail::rightjoin('attendances', 'attendance_details.attendance_id', '=', 'attendances.id')
                ->where('attendances.emp_code', Auth::User()->employee_id);
        if (isset($request['search_month']) || isset($request['search_month'])) {
            if ($request['search_month']) {
                $userAttendanceDetails = $userAttendanceDetails->whereMonth('entry_date', $request['search_month']);
            }
            if ($request['search_year']) {
                $userAttendanceDetails = $userAttendanceDetails->whereYear('entry_date', $request['search_year']);
            }
        } else {
            $userAttendanceDetails = $userAttendanceDetails->where('entry_date', 'LIKE', '%' . $current_years . '-' . $current_month . '%');
        }
        $userAttendanceDetails = $userAttendanceDetails->groupBy(['attendances.entry_date', 'attendance_details.attendance_id'])
                        ->select(
                                'attendance_details.attendance_id', 'attendances.first_in', 'attendances.last_out', 'attendances.late_comer', 'attendances.incorrect_entry', 'attendances.entry_date', 'attendances.full_day', 'attendances.half_day', 'attendances.early_going', 'attendances.absent', DB::raw('SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))) AS working_hours'), DB::raw('SUBTIME(TIMEDIFF( attendances.last_out, attendances.first_in), TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))), "%h:%i:%s")) as break_hours')
                        )->get();

        $absentDays = 0;
        foreach ($userAttendanceDetails as $userAttendanceDetailsVal) {
            if ($userAttendanceDetailsVal->absent == 1 || $userAttendanceDetailsVal->half_day == 1 || $userAttendanceDetailsVal->incorrect_entry == 1) {
                $absentDays = $absentDays + ($userAttendanceDetailsVal->absent + $userAttendanceDetailsVal->half_day / 2 + $userAttendanceDetailsVal->incorrect_entry);
            }
        }

        $reportingList = $reportingList->select('users.reporting_to')->sortable()->groupBy('emp_code')->get()->toArray();

        $reportingUserArray = $reportingId = array();
        foreach ($reportingList as $reportingListVal) {
            if (!in_array($reportingListVal['reporting_to'], $reportingId)) {
                $reportingId[] = $reportingListVal['reporting_to'];
                $reporting = User::where('id', $reportingListVal['reporting_to'])->select('first_name', 'last_name', 'id')->first();

                if(isset($reporting['first_name']) && $reporting['first_name'] != ""){
                    $reportingUserArray[$reporting['id']] = $reporting['first_name'] . ' ' . $reporting['last_name'];
                } else {
                    $reportingUserArray[$reportingListVal['reporting_to']] =  '';
                }

            }
        }
        $searchFilter['designationList'] = $designationList;
        $avg_working_time_in_sec = $taskEntry->findAvgWorkingTime($comp_id);
        $avg_log_limit = $taskEntry->findAvgLoggedTime($comp_id);
       // echo "<pre>";print_r($working_details);exit;
        return view('pms.timesheet.viewTimeSheet', compact('reportingUserArray','designationList', 'absentDays', 'total_days', 'working_details', 'records', 'data', 'years', 'userListing', 'request', 'currentMonth', 'currentYear', 'loggedHours','avg_working_time_in_sec','avg_log_limit'));
    }

    public function addAutoLeave(Request $request){
        try{
            if($request->auto_leave && $request->auto_leave == 'on'){
                if(isset($request->type) && $request->type == "addNewTimeEntrie"){
                    if(isset($request->entry_date) && $request->entry_date != "" && isset($request->emp_id) && $request->emp_id != "") {
                        $get_user_info = getUserInfo(['select' => ['id','first_name','designation_id','last_name','employee_id','company_id'],'where' => ['status' => 1, 'employee_id' => $request->emp_id,'company_id' => auth()->user()->company_id]]);
                        if(isset($get_user_info) && count($get_user_info) > 0) {
                            $attendance = Attendance::where(['emp_code' => $request->emp_id, 'entry_date' => date('Y-m-d',strtotime($request->entry_date)), 'emp_comp' => $get_user_info['company']['company_name']])->whereNull('deleted_at')->orderBy('id','desc')->first();
                            if(!(isset($attendance->id) && $attendance->id != "")){
                                $insert = [];
                                $insert['entry_date'] = date('Y-m-d',strtotime($request->entry_date));
                                $insert['emp_code'] = $request->emp_id;
                                $insert['emp_name'] = $get_user_info['first_name']." ".$get_user_info['last_name'];
                                $insert['emp_comp'] = $get_user_info['company']['company_name'];
                                $insert['first_in'] = "00:00:00";
                                $insert['last_out'] = "00:00:00";
                                $insert['user_id'] = $get_user_info['id'];
                                $attendance = Attendance::create($insert);
                                $attendance->full_day = 1;
                                $attendance->save();
                            }
                        }
                    }
                }
                $days = 0;
                $operation = "plus";
                $leaveDays = 0;
                $attendanceId = $request->hiddenAtteIdToLeave;
                if(isset($attendance->id) && $attendance->id != ""){
                    $attendanceId = $attendance->id;
                }
                $oldObj = Attendance::find($attendanceId);
                if($oldObj->half_day != null && $oldObj->half_day == 1){
                    $leaveDays=0.5;
                    $operation = "plus";
                }
                if($oldObj->full_day != null && $oldObj->full_day == 1){
                    $leaveDays=1;
                    $operation = "plus";
                }
                if($oldObj->incorrect_entry != null && $oldObj->incorrect_entry == 1){
                    $leaveDays=1;
                    $operation = "plus";
                }
                $obj = Attendance::find($attendanceId);
                $obj->first_in = '00:00:00';
                $obj->last_out = '00:00:00';
                $obj->incorrect_entry = null;
                $obj->full_day = null;
                $obj->half_day = null;
                $obj->early_going = null;
                $obj->adjustment = 1;
                $obj->absent = 1;
                $obj->late_comer = null;
                if($obj->save()){
                    $company = new Company();
                    $companyId = $company->getCompanyDataByName($obj->emp_comp)->id;
                    $userId = $this->getUserId($obj->emp_code,$companyId);
                    $status = $this->updateLeaveData($userId,$obj->entry_date,$days);
                    if($status == "1")
                    {
                        $this->updateTakenLeave($userId,$leaveDays,$operation,$obj->entry_date);
                    }
                }
                AttendanceDetail::where('attendance_id', $attendanceId)->delete();
                $insert_attendance = new AttendanceDetail();
                $insert_attendance->attendance_id = $attendanceId;
                $insert_attendance->in_time = '00:00:00';
                $insert_attendance->out_time = '00:00:00';
                $insert_attendance->save();
                if($status == "1"){
                    Alert::success('Leave is successfully added!');
                } else if ($status == "2") {
                    Alert::success('Absent entry successfully added!');
                } else {
                    Alert::error('Oops, Leave is already added!');
                }
            }
            return Redirect::back();
        } catch (Exception $ex) {
            Alert::error('Oops, Leave not successfully added!');
            return Redirect::back();
        }
    }

    public function calculateSummary($pms_records) {
        $total_days = 0;
        $working_days = 0;
        $present_days = 0;
        $absent_days = 0;
        $late_days = 0;
        $early_going_days = 0;
        $average_total = 0;
        $average = '00:00';

        foreach ($pms_records as $pms_record) {
            $pmsDate = $pms_record['year'] . '-' . $pms_record['month'] . '-' . $pms_record['date'];
            // $isWeekend = $this->isWeekend($pmsDate);
            $isHoliday = $this->isHoliday($pmsDate);
            if (!$isHoliday) {
                $total_days++;
            }

            //if punch data is not empty then process the data
            if (!empty($pms_record['punch_data'])) {
                $all_in_times = array();
                $all_out_times = array();
                $pms_record['total_hours'] = '-';
                $pms_record['working_hours'] = '-';
                $pms_record['break_hours'] = '-';

                if (strpos($pms_record['punch_data'], ';')) {
                    $time_array = array_filter(explode(";", $pms_record['punch_data']));
                    foreach ($time_array as $key => $value) {
                        if ($key % 2 == 0) {
                            $all_in_times[] = strstr($value, '(', true);
                        } else {
                            $all_out_times[] = strstr($value, '(', true);
                        }
                    }
                } else {
                    $time_array = str_replace(":in", ":00", $pms_record['punch_data']);
                    $time_array = str_replace(":out", ":00", $time_array);
                    if (strpos($time_array, ':(')) {
                        $time_array = str_replace(":(", ":00(", $time_array);
                    }
                    $time_array = array_filter(explode("(Sales) ", $time_array));
                    foreach ($time_array as $key => $value) {
                        if ($key % 2 == 0) {
                            $all_in_times[] = $value;
                        } else {
                            $all_out_times[] = $value;
                        }
                    }
                }

                $pms_record['all_in_times'] = $all_in_times;
                $pms_record['all_out_times'] = $all_out_times;
                $pms_record['missing_entry'] = (sizeof($all_in_times) == sizeof($all_out_times)) ? 0 : 1;
                $in_out_count = sizeof($all_in_times);
                if ($pms_record['missing_entry'] == 0) {
                    $total_working_hours = 0;
                    for ($i = 0; $i < $in_out_count; $i++) {
                        $total_working_hours += strtotime($all_out_times[$i]) - strtotime($all_in_times[$i]);
                    }
                    $total_hours = strtotime($pms_record['last_out']) - strtotime($pms_record['first_in']);
                    $total_break_hours = $total_hours - $total_working_hours;
                    $pms_record['total_hours'] = date('H:i:s', $total_hours);
                    $pms_record['working_hours'] = date('H:i:s', $total_working_hours);
                    $pms_record['break_hours'] = date('H:i:s', $total_break_hours);

                    if ($pms_record['working_hours'] >= (config('constant.half_day_from')) && $pms_record['working_hours'] < (config('constant.half_day_to'))) {
                        $working_days += 0.5;

                        if (!$isHoliday) {
                            $present_days++;

                            $timeParts = explode(":", $pms_record['working_hours']); //if you know its safe
                            $seconds = ($timeParts[0] * 60 * 60 + $timeParts[1] * 60 + $timeParts[2]);
                            $tempWorkingHours = gmdate("H:i:s", ($seconds * 2));
                            $average_total += strtotime($tempWorkingHours);
                        }
                    } elseif (isset($pms_record['working_hours']) && $pms_record['working_hours'] != '00:00:00' && $pms_record['working_hours'] >= (config('constant.half_day_to'))) {
                        $working_days++;
                        if (!$isHoliday) {
                            $present_days++;
                            $average_total += strtotime($pms_record['working_hours']);
                        }
                        $lateFrom = $this->getLateFromValue($pmsDate);
                        if ($pms_record['first_in'] > $lateFrom && !$isHoliday) {
                            $late_days++;
                        }
                    }

                    /* if($pms_record['first_in']>(config('constant.late_from')) && !$isHoliday && strtotime('2019-1-15') < strtotime($pmsDate)){
                      $late_days++;
                      } */
                    if ($pms_record['last_out'] < (config('constant.shift_to')) && !$isHoliday) {
                        $early_going_days++;
                    }
                }
            }
        }

        if ($total_days >= $working_days) {
            $absent_days = $total_days - $working_days;
        }

        if ($average_total && $present_days) {
            $average = date('H:i:s', intval($average_total / $present_days));
        }

        $summary = [
            'total_days' => $total_days,
            'working_days' => $working_days,
            'absent_days' => $absent_days,
            'late_days' => $late_days,
            'early_going_days' => $early_going_days,
            'average' => $average,
            'emp_code' => $pms_record['emp_code'],
            'emp_name' => $pms_record['emp_name'],
            'month' => date('F', mktime(0, 0, 0, $pms_record['month'], 10)),
            'year' => $pms_record['year'],
        ];

        return $summary;
    }

    public function isHoliday($date) {
        $session = session()->all();
        $date = date("Y-m-d", strtotime($date));
        $searchFilter['request_year'] = date('Y', strtotime($date));

        if ((date('N', strtotime($date)) >= 6)) {
            return true;
        } else if (in_array($date, $session['holidays'])) {
            return true;
        }
        return false;
    }

    public function isWeekend($date) {
        return (date('N', strtotime($date)) >= 6);
    }

    public function getLateFromValue($date) {
        $lateFromArr = LateFrom::where('affected_from_date', '<=', $date)->orderBy('affected_from_date', 'desc')->first();
        if (!empty($lateFromArr)) {
            $lateFromValue = $lateFromArr->late_from;
            return $lateFromValue;
        }
        throw new \Exception("Value for late_from is not defined in database table named 'late_from_settings' for " . $date . ".");
    }

    public function viewSummary(Request $request) {
        if (Auth::check() && !(Auth::user()->hasPermission('pms.uploadtimeentry'))) {
            $teamUserArray = array(auth()->user()->employee_id);
            $user = new User();
            $userListing = $user->getMyTeamList(Auth::user()->id);
            $emp_code = (isset($request->employee) && $request->employee != "") ? $request->employee : "";
            if(isset($userListing) && count($userListing) > 0) {
                foreach ($userListing as $userListingVal){
                    $teamUserArray[] = $userListingVal['employee_id'];
                }
            }
            if (Auth::check() && !in_array($emp_code,$teamUserArray)) {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }
        $userDetail = $userAttendanceDetails = $taskLogHours = array();
        $monthNameYear = '';

        if ($request->has('month') && $request->has('year') && $request->has('employee') && $request->get('month') != '' && $request->get('year') != '' && $request->get('employee') != '') {
            $month = $request->get('month');
            $year = $request->get('year');
            $date = $year . '-' . $month . '-';
            $employeeCode = $request->get('employee');
            $company_id = Auth::user()->company_id;

            $company = new Company();
            $taskEntry = new TaskEntry();
            $monthName = DateTime::createFromFormat('!m', $month);
            $monthNameYear = $monthName->format('F') . ', ' . $year;

            $userDetail = User::where('employee_id', $employeeCode)->where('company_id', $company_id)->first();

            $emp_company = $company->getCompanyDataById($company_id)->company_name;

            $userAttendanceDetails = AttendanceDetail::rightjoin('attendances', 'attendance_details.attendance_id', '=', 'attendances.id')
                            ->where('attendances.emp_code', $employeeCode)
                            ->where('attendances.emp_comp', $emp_company)
                            ->where('attendances.entry_date', 'like', '%' . $date . '%')
                            ->groupBy(['attendances.entry_date', 'attendance_details.attendance_id'])
                            ->select(
                                    'attendance_details.attendance_id', 'attendances.id', 'attendances.first_in', 'attendances.last_out', 'attendances.late_comer', 'attendances.incorrect_entry', 'attendances.entry_date', 'attendances.full_day', 'attendances.half_day', 'attendances.early_going', 'attendances.absent','attendances.adjustment','attendances.content',
                            //DB::raw('TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))), "%h:%i:%s") AS working_hours'),
                            //DB::raw('SUBTIME(TIMEDIFF( attendances.last_out, attendances.first_in), TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))), "%h:%i:%s")) as break_hours')
                                    DB::raw('SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time)))) AS working_hours'), DB::raw('SUBTIME(TIMEDIFF( attendances.last_out, attendances.first_in),SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time))))) as break_hours')
                            )->orderBy('attendances.entry_date','asc')->get();
                            // dd($userAttendanceDetails);
           $userLeaveDetails = Attendance::selectRaw('attendances.emp_code, attendances.emp_name, SUM(attendances.full_day) as full_days, SUM(attendances.half_day) as half_days, SUM(attendances.early_going) as early_going_days,SUM(attendances.adjustment) as adjustment,SUM(attendances.absent) as absent_days,
           SUM(attendances.late_comer) as late_days, SUM(attendances.incorrect_entry) as incorrect_days')->where('attendances.emp_code', $employeeCode)->where('attendances.emp_comp', $emp_company)->where('attendances.entry_date', 'like', '%' . $date . '%')->first();
           $total_days_qry = Attendance::select('entry_date')->where('entry_date', 'LIKE', '%' . $date . '%');
           $total_days_qry->groupby('entry_date');
           $total_days_array = $total_days_qry->get()->toArray();

            $total_days = 0;
            foreach ($total_days_array as $total_days_array_val) {
                if (!Holiday::getHolidayDateList($total_days_array_val['entry_date'])) {
                    // $timestamp = strtotime($total_days_array_val['entry_date']);
                    // $weekday = date("l", $timestamp);
                    $date = $total_days_array_val['entry_date'];
                    $isWeekend = Holiday::getWeekendDateList($date);
                    // if ($weekday != "Saturday" && $weekday != "Sunday") {
                    if(count($isWeekend) == 0){
                        $total_days++;
                    }
                }
            }
          // dd($date,$employeeCode,$emp_company);
          //echo "<pre>";print_r($total_days);exit;

            $firstDateOfMonth = $year . '-' . $month . '-' . '01';
            $lastDateOfMonth = new \DateTime($year . '-' . sprintf("%02d", $month));
            $lastDateOfMonth->modify('last day of this month');
            $lastDateOfMonth = $lastDateOfMonth->format('Y-m-d');

            if ($userDetail) {
                //,DB::raw("floor(sum(floor(`log_hours`)) + sum(`log_hours` - floor(`log_hours`)) * 100.0/60) +
                //((sum(floor(`log_hours`)) + sum(`log_hours` - floor(`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_logged_hours")
                $taskLogHours = ProjectEntry::select('log_date', DB::raw("sum(`log_hours`) as total_logged_hours"))
                                ->where('user_id', $userDetail->id)
                                ->where(function($q) use ($firstDateOfMonth, $lastDateOfMonth) {
                                    $q->whereBetween('log_date', [$firstDateOfMonth, $lastDateOfMonth]);
                                })
                                ->whereNull('deleted_at')
                                ->groupBy('log_date')
                                ->pluck('total_logged_hours', 'log_date')->toArray();
                $taskLogHours = array_filter($taskLogHours);
            }
        }

        $setting = new Setting();
        $full_day_time = config('constant.max_full_day_start_time');
        $average_full_day_time = $setting->getSettingEntryTime('max_full_day_start_time',$company_id);
        if($average_full_day_time != null){
            $full_day_time = $average_full_day_time;
        }
        $first_half_time = config('constant.max_first_half_start_time');
        $average_first_half_time = $setting->getSettingEntryTime('max_first_half_start_time',$company_id);
        if($average_first_half_time != null){
            $first_half_time = $average_first_half_time;
        }
        $second_half_time = config('constant.max_second_half_start_time');
        $average_second_half_time = $setting->getSettingEntryTime('max_second_half_start_time',$company_id);
        if($average_second_half_time != null){
            $second_half_time = $average_second_half_time;
        }
        $totalWorkingDay = 0;
        $late_days = 0;
        $avg_log_hr  = '00:00:00';
        $userAttendanceData =  $taskEntry->dashboardTimeDetails($emp_company,$month,$year,$employeeCode);
        foreach($userAttendanceData['records'] as $userSummaryDetail){
            $totalWorkingDay = $userSummaryDetail['full_days'] + ($userSummaryDetail['half_days'] / 2);
            $late_days = $userSummaryDetail['late_days'];
            if(array_key_exists($userSummaryDetail['emp_code'],$userAttendanceData['loggedHours'])){
                //$logged_hours = sprintf('%0.2f',$userAttendanceData['loggedHours'][$userSummaryDetail['emp_code']]);
                $logged_hours = (new \App\Helpers\CommonHelper)->displayTaskTime($userAttendanceData['loggedHours'][$userSummaryDetail['emp_code']]);
                $hoursMinutes = (explode(".",$logged_hours));
                $hours = $minutes = '00';
                if(isset($hoursMinutes[0])){
                    $hours = $hoursMinutes[0];
                    $hours = (strlen($hours) == 1)?'0'.$hours:$hours;
                }

                if(isset($hoursMinutes[1])){
                    $minutes = $hoursMinutes[1];
                    $minutes = (strlen($minutes) == 1)?$minutes.'0':$minutes;
                }

                $str_time = $hours.':'.$minutes.':00';

                sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

                $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
                if($totalWorkingDay != 0){

                    $avg_log_hr = gmdate("H:i:s", $time_seconds/$totalWorkingDay);
                }
            }
            else{
               $avg_log_hr  = '00:00:00';
            }
        }
        $seconds = 0 ;
        $avg_working_hr = '00:00:00';

        foreach($userAttendanceData['working_details'] as $working_details_val){
            if($working_details_val['emp_code'] == $userSummaryDetail['emp_code']){
                $parsed = date_parse($working_details_val['working_hours']);
                $seconds = $seconds + $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
            }
        }
        if($totalWorkingDay != 0){
            $avg_working_hr = gmdate("H:i:s", $seconds/$totalWorkingDay);
        }

        $startdatestring = "01-".$month."-".$year;
        $date1 = 'first day of '.date("M",strtotime($startdatestring));
        $date2 = 'last day of '.date("M",strtotime($startdatestring));
        //   dd($date1);
        $startMonthDate = $newDate = date('Y-m-d', strtotime($date1));
        $lastMonthDate = $newDate = date('Y-m-d', strtotime($date2));
        // dd($lastMonthDate);
        //dd($startMonthDate);
        $leaveDetails = getUserMonthlyLeaveDetails(["where" => ['user_id' => $userDetail->id ],"between" => ["start_date" => $startMonthDate, "end_date" => $lastMonthDate]]);
     //   echo $avg_working_hr;exit;
        //echo "<pre>";print_r($userDetail);exit;
        $toDayDate = date('Y-m');
        $searchMonth = $year . '-' . $month;
        $todayAttendance = $removeText = [];
        $working_hours = $break_hours = '00:00:00';
        $class = "full_day_color";
        $currentTime = date("H:i");
        if($currentTime < '23:45'){
            if($toDayDate == $searchMonth){
                        $todayAttendance = Attendance::where('entry_date',date('Y-m-d'))->where('emp_comp',$emp_company)->where('emp_code',$employeeCode)->first();
                         if(!empty($todayAttendance->content)){
                            $workingHoursArray = $this->timeDiff($todayAttendance->content);
                            if(isset($workingHoursArray['working_time'])){
                                $working_hours = $workingHoursArray['working_time'];
                                $break_hours = $workingHoursArray['break_hours'];
                                $class = $workingHoursArray['class'];
                            }
                        }
            }
        }
       // echo "<pre>";print_r($userAttendanceDetails);exit;
        return view('pms.timesheet.summary', compact('userDetail', 'class', 'monthNameYear', 'userAttendanceDetails', 'taskLogHours','full_day_time','first_half_time','second_half_time','userLeaveDetails','total_days','avg_working_hr','avg_log_hr','leaveDetails','todayAttendance','working_hours','break_hours'));
    }

    public function timeDiff($time = null)
    {
        date_default_timezone_set("Asia/Kolkata");
        $last_punch = "out";
        if($time != ""){
            $time = explode(",",$time);
        } else {
            $time = [];
        }

        if(count($time) <= 0){
            return ["working_time" => "00:00:00", "break_hours" => "00:00:00", "last_punch" => "no punch"];
        } else {
            $working_time = 0;
            $total_time = 0;
            if(count($time)%2 !=0){
                $time[count($time)] = date("H:i:s");
                $last_punch = "in";
            }
            foreach($time as $key => $val){
                if($key == 0){
                    $total_time -= strtotime(strtok($val,' '));
                }
                if($key%2 == 0){
                    $working_time -= strtotime(strtok($val,' '));
                } else {
                    $working_time += strtotime(strtok($val,' '));
                }
                if((count($time) - 1) == $key){
                    $total_time += strtotime(strtok($val,' '));
                }
            }
            if($working_time < 16200) {
                $class = "absent_day_color";
            } else if ($working_time < 25200) {
                $class = "half_day_color";
            } else if ($working_time < 27000) {
                $class = "early_going_color";
            } else {
                $class = "full_day_color";
            }
            return ["working_time" => gmdate("H:i:s", $working_time),"class" => $class, "break_hours" => gmdate("H:i:s", ($total_time - $working_time)),"last_punch" => $last_punch];
        }
    }
    public function updateTimeEntry(Request $request) {

        if ($request->isMethod('post')) {
            $postData = $request->post();

            $count = 0;
            if (!empty($postData)) {
                foreach ($postData as $key => $value) {
                    $result = preg_match("#^entry(.*)$#i", $key);
                    if ($result != 0) {
                        $count++;
                    }
                }
            }
            $timeEntryID = isset($postData['time_entry_id']) ? intval($postData['time_entry_id']) : 0;

            if ($count > 0) {
                $timeEntryString = '';

                for ($i = 1; $i <= $count; $i++) {
                    if ($i % 2 == 1) {
                        $timeEntryString = $timeEntryString . '' . trim($postData['entry' . $i]) . '(in);';
                    } else {
                        $timeEntryString = $timeEntryString . '' . trim($postData['entry' . $i]) . '(out);';
                    }
                }

                if ($timeEntryString && $timeEntryID) {
                    $pms = pms::find($timeEntryID);
                    $pms->first_in = trim($postData['entry1']);
                    $pms->last_out = trim($postData['entry' . $count]);
                    $pms->punch_data = trim($timeEntryString);
                    $pms->save();

                    echo 'success';
                    exit;
                }
            }
        }
        echo 'error';
        exit;
    }

    public function getTimeEntry(Request $request) {

        if ($request->isMethod('post')) {
            if ($request->has('id')) {
                $timeEntryID = $request->post('id');

                $pms = new pms();
                $timeEntryDetail = $pms->getTimeEntryByID($timeEntryID);

                if (!empty($timeEntryDetail)) {
                    if (strpos($timeEntryDetail['punch_data'], ';')) {
                        $punchArray = preg_split("/[;]/", $timeEntryDetail['punch_data']);
                    } else {
                        $punchArray = str_replace(":in", ":00", $timeEntryDetail['punch_data']);
                        $punchArray = str_replace(":out", ":00", $punchArray);
                        if (strpos($punchArray, ':(')) {
                            $punchArray = str_replace(":(", ":00(", $punchArray);
                        }
                        $punchArray = preg_split("/[(Sales) ]/", $punchArray);
                    }
                    $punchArray = array_filter($punchArray);
                    echo view('pms.timesheet.edit_timeentry', compact('punchArray', 'timeEntryID'));
                    exit;
                }
            }
        }
        echo 'error';
        exit;
    }

    /**
     * Upload time entry of users (Only excepts xls format).
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry')) {
            $method = strtolower($request->method());
            if ($method == 'post') {
                $validator = Validator::make($request->all(), [
                    // 'xlsfile' => 'required|mimes:csv',
                    'timeEntryDate' => 'required|date',
                ]);

                if ($validator->fails()) {
                    Alert::error('Oops, Form has some error!');
                    return Redirect::back()->withErrors($validator)->withInput();
                } else {
                    $file = $request->file('xlsfile');
                    $extension = $file->getClientOriginalExtension();
                    $filename = 'attendance_' . time() . '.' . $extension;
                    if ($request) {
                        Storage::disk('local')->putFileAs('', $file, $filename);
                    }
                    Excel::import(new TimeEntriesImport($request->timeEntryDate), $filename);

                    $importStatus = array();
                    $importStatus = Session::get('statusOfTimeEntry');
                    Alert::success('Time Entries Successfully Uploaded. Suceess Record => '.$importStatus[0]['success'].' Skipped Record => '.$importStatus[0]['skipped'].' Failed Record => '.$importStatus[0]['failed']);
                    $importStatus = Session::forget('statusOfTimeEntry');
                    return Redirect::to('/upload/old/entries');
                }
            } else {
                return view('pms.upload_entries');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function oldUploadedEntries(Request $request)
    {
        $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry')) {
            $company = new Company();
            $comp_id = Auth::user()->company_id;
            $comp_name = $company->getCompanyDataById($comp_id)->company_name;
            $entryStatus = new TimeEntryStatus();
            $entryStatus = $entryStatus->where('emp_comp',$comp_id)->whereNull('deleted_at');
            if(isset($request->search_by_entry_date) && !empty($request->search_by_entry_date)){
                $entryStatus = $entryStatus->where('entry_date',date('Y-m-d',strtotime($request->search_by_entry_date)));
            }
            if(isset($request->search_by_status) && !empty($request->search_by_status)){
                $entryStatus = $entryStatus->where('status',$request->search_by_status);
            }
            $exportStatus = $entryStatus;
            $records =  $entryStatus->orderBy('entry_date','DESC')->orderBy('status','asc')->paginate($page_limit);
            if($request->submit_type == 'export_excel'){
                $exportStatus = $exportStatus->where('status','failed')->get()->toArray();
                return Excel::download(new ExportFailedTimeEntryStatus($exportStatus), 'in_valid_time_entry.csv');
            }else{
                return view('pms.timesheet.time_sheet_status', compact('request','records'));
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function oldEntriesDestroy(Request $request)
    {
        if(Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry')){
            $entryStatus = TimeEntryStatus::find($request->statusId);
            try{
                if($entryStatus){
                    $entryStatus->delete();
                    return Response::json(array('message'=>'Time Entry Status Deleted Successfully','status'=>'success'));
                }else{
                    return Response::json(array('message'=>'Time Entry Status Not Deleted Successfully','status'=>'error'));
                }
            }catch(Exception $e){
                return Response::json(array('message'=>'Time Entry Status Not Deleted Successfully','status'=>'error'));
            }
        }else{
            return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
        }
    }

    public function getUserPunchData(Request $request) {
        $attendanceId = $request->input('attendanceId');
        $attendanceDetail = AttendanceDetail::where('attendance_id', $attendanceId)->get();

        if (count($attendanceDetail) > 0) {
            return Response::json(array('status' => 'success', "result" => $attendanceDetail));
        } else {
            return Response::json(array('status' => 'error', "result" => array()));
        }
    }
    public function getUserPunchDataToday(Request $request) {
        $attendanceId = $request->input('attendanceId');
        $attendanceDetail = Attendance::find($attendanceId);
        $mainArray = [];
        if(!empty($attendanceDetail) && !empty($attendanceDetail->content)){
            $attendanceArray = explode(",",$attendanceDetail->content);
            $i = 0;
            $j=0;
            foreach ($attendanceArray as $key => $value) {
                if($i % 2 == 0){
                    $mainArray[$j]['in'] = $value;
                }else{
                    $mainArray[$j]['out'] = $value;
                    $j++;

                }
                 $i++;

            }
        }

       // echo "<pre>";print_r($mainArray);exit;
        if (count($mainArray) > 0) {
            return Response::json(array('status' => 'success', "result" => $mainArray));
        } else {
            return Response::json(array('status' => 'error', "result" => array()));
        }
    }

    public function getPunchData(Request $request) {
        $user_name = isset(auth()->user()->first_name) ? auth()->user()->first_name : "";
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry')) {
            $data['attendanceId'] = $request->input('attendanceId');
            $data['attendanceDetail'] = AttendanceDetail::where('attendance_id', $data['attendanceId'])->get();
            $html = view('pms.timesheet.model.update_time_entry', compact('data'))->render();
            return Response::json(array('status' => 'success', "html" => $html));
        } else {
            return Response::json(array('message' => 'Hello! '.$user_name.' Your time-entry update\'s request has been passed to the respective person! please wait for his response...','status'=>'error'));
        }
    }

    public function addPunchData(Request $request) {
        $user_name = isset(auth()->user()->first_name) ? auth()->user()->first_name : "";
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry')) {
            $selectedMonthDate = isset($request->selectedMonthDate) && $request->selectedMonthDate != "" ? $request->selectedMonthDate : date('d-m-Y');
            $html = view('pms.timesheet.model.add_time_entry',compact('selectedMonthDate'))->render();
            return Response::json(array('status' => 'success', "html" => $html));
        } else {
            return Response::json(array('message' => 'Hello! '.$user_name.' Your time-entry update\'s request has been passed to the respective person! please wait for his response...','status'=>'error'));
        }
    }

    public function getOldPunchData(Request $request) {
        $user_name = isset(auth()->user()->first_name) ? auth()->user()->first_name : "";
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry')) {
            $data = [];
            if((isset($request->entry_date) && $request->entry_date != "") && (isset($request->emp_id) && $request->emp_id != "")){
                $entry_date = date('Y-m-d',strtotime($request->entry_date));
                $data['entry_date'] = $entry_date;
                $data['emp_id'] = $request->emp_id;
                $company = Company::find(auth()->user()->company_id);
                $company_name = (isset($company->company_name) && $company->company_name != "") ? $company->company_name : "";
                $attendance = Attendance::where(['emp_code' => $request->emp_id,'entry_date' => $entry_date, 'emp_comp' => $company_name])->whereNull('deleted_at')->orderBy('id','desc')->first();
                if(isset($attendance->id) && $attendance->id != ""){
                    $data['attendanceId'] = $attendance->id;
                    $data['attendanceDetail'] = AttendanceDetail::where('attendance_id', $data['attendanceId'])->get();
                }
                $html = view('pms.timesheet.model.time_entry_punch_data',compact('data'))->render();
                return Response::json(array('status' => 'success', "html" => $html));
            } else {
                return Response::json(array('message' => 'something went wrong!','status'=>'error'));
            }
        } else {
            return Response::json(array('message' => 'Hello! '.$user_name.' Your time-entry update\'s request has been passed to the respective person! please wait for his response...','status'=>'error'));
        }
    }

    public function updateEntry(Request $request) {
        $is_work_from_home = (isset($request->request_for) && $request->request_for == "work_from_home") ? true : false;
        $is_valid_request = (isset($request->user_id) && in_array($request->user_id,getTeamUserId(auth()->user()->id))) ? true : false;
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry') || ($is_work_from_home && $is_valid_request)) {

            $company = new Company();
            try {
                if(isset($request->type) && $request->type == "addNewTimeEntrie"){
                    if(isset($request->entry_date) && $request->entry_date != "" && isset($request->emp_id) && $request->emp_id != "") {
                        $get_user_info = getUserInfo(['select' => ['id','first_name','designation_id','last_name','employee_id','company_id'],'where' => ['status' => 1, 'employee_id' => $request->emp_id,'company_id' => auth()->user()->company_id]]);
                        if(isset($get_user_info) && count($get_user_info) > 0) {
                            $attendance = Attendance::where(['emp_code' => $request->emp_id, 'entry_date' => date('Y-m-d',strtotime($request->entry_date)), 'emp_comp' => $get_user_info['company']['company_name']])->whereNull('deleted_at')->orderBy('id','desc')->first();
                            if(!(isset($attendance->id) && $attendance->id != "")){
                                $insert = [];
                                $insert['entry_date'] = date('Y-m-d',strtotime($request->entry_date));
                                $insert['emp_code'] = $request->emp_id;
                                $insert['emp_name'] = $get_user_info['first_name']." ".$get_user_info['last_name'];
                                $insert['emp_comp'] = $get_user_info['company']['company_name'];
                                $insert['first_in'] = "00:00:00";
                                $insert['last_out'] = "00:00:00";
                                $insert['user_id'] = $get_user_info['id'];
                                $attendance = Attendance::create($insert);
                                $attendance->full_day = 1;
                                $attendance->save();
                            }
                        }
                    }
                }
                $element = $request->input('hidenTotalElement');
                $time_entry_array = array();
                foreach ($request->all() as $key => $value) {
                    $explode = explode('_', $key);
                    if (!empty($explode) && $explode[0] == "inEntry") {
                        $time_entry_array[] = $explode[1];
                    }
                }
                $adjustments = 0;

                $attendanceId = $request->input('hiddenAttendanceId');
                if(isset($attendance->id) && $attendance->id != ""){
                    $attendanceId = $attendance->id;
                } else {
                    if(Auth::user()->hasRole('admin') && !isset($request->adjustment)){
                        $adjustments = 0;
                    }else{

                        $adjustments = 1;
                    }
                }

                $attendanceDate = Attendance::find($attendanceId);

                $userid = User::where('employee_id',$attendanceDate->emp_code)->where('company_id',auth()->user()->company_id)->value('id');
               // dd($userid);
                $isWorkfromHome = isWorkingFromHome($userid,$attendanceDate->entry_date);
              //  dd($isWorkfromHome);
                if($isWorkfromHome){
                    $adjustments = 0;
                 }
                $attendanceDate->adjustment = $adjustments;
                $attendanceDate->save();
                $oldEntryData = getUserPunchDataEntry($attendanceId);
              //  echo "<pre>";print_r($oldEntryData);
                $firstIn = $request->input('inEntry_' . $time_entry_array[0]);

                AttendanceDetail::where('attendance_id', $attendanceId)->delete();
                for ($i = 0; $i < count($time_entry_array); $i++) {
                    $inTime = $this->formatTime($request->input('inEntry_' . $time_entry_array[$i]));
                    $outTime = $this->formatTime($request->input('outEntry_' . $time_entry_array[$i]));
                    if ($i == 0) {
                        Attendance::where('id', $attendanceId)->update(['first_in' => $inTime]);
                    }

                    if ($inTime != '' && $outTime != '') {
                        $lastOut = $outTime;
                        $obj = new AttendanceDetail;
                        $obj->attendance_id = $attendanceId;
                        $obj->in_time = $inTime;
                        $obj->out_time = $outTime;
                        $obj->save();
                    }
                    if ($i == count($time_entry_array) - 1) {
                        $update_att = Attendance::where('id', $attendanceId)->update(['last_out' => $outTime]);
                    }
                }


                if ($firstIn != '' && $lastOut != '') {
                    if ($firstIn != '00:00:00' && $lastOut != '00:00:00') {
                        // $obj = Attendance::find($attendanceId);
                        // $obj->incorrect_entry = null;
                        // $obj->save();

                        $this->manageUserDays($attendanceId);
                    } else if ($firstIn == '00:00:00' && $lastOut == '00:00:00') {
                        $obj = Attendance::find($attendanceId);
                        if($obj != null)
                        {
                            $isAbsent = $obj->absent == 1?1:null;
                            $isHalfDay = $obj->half_day == 1?1:null;
                            $isFullDay = $obj->full_day == 1?1:null;
                        }
                        $obj->incorrect_entry = null;
                        $obj->full_day = null;
                        $obj->half_day = null;
                        $obj->early_going = null;
                        $obj->absent = 1;
                        if($obj->save())
                        {
                            if(isset($isFullDay) && $isFullDay == 1)
                            {
                                $operation = "plus";
                                $leaveDays = 1;
                                $days = 0;
                                $company = new Company();
                                $companyId = $company->getCompanyDataByName($obj->emp_comp)->id;
                                $userId = $this->getUserId($obj->emp_code,$companyId);
                                $status = $this->updateLeaveData($userId,$obj->entry_date,$days);
                                if( $status == "1"){
                                    $this->updateTakenLeave($userId,$leaveDays,$operation,$obj->entry_date);
                                }
                            }
                            if(isset($isHalfDay) && $isHalfDay == 1)
                            {
                                $operation = "plus";
                                $leaveDays = 0.5;
                                $days = 0;
                                $company = new Company();
                                $companyId = $company->getCompanyDataByName($obj->emp_comp)->id;
                                $userId = $this->getUserId($obj->emp_code,$companyId);
                                $status = $this->updateLeaveData($userId,$obj->entry_date,$days);
                                if( $status == "1"){
                                    $this->updateTakenLeave($userId,$leaveDays,$operation,$obj->entry_date);
                                }
                            }
                        }
                    }
                    else{
                        $obj = Attendance::find($attendanceId);
                        $obj->incorrect_entry = 1;
                        $obj->full_day = null;
                        $obj->half_day = null;
                        $obj->early_going = null;
                        $obj->absent = null;
                        $obj->save();
                    }
                }
                $newEntryData = getUserPunchDataEntry($attendanceId);
               // echo "<pre>";print_r($newEntryData);exit;
                $company     = new Company();
                $userCompanyMailData  = $company->getCompanyViseMailData(Auth::user()->company_id)[0];
                $from['email'] = $userCompanyMailData['from_address'];
                $from['name']  = $userCompanyMailData['company_name'];
                $data['regards'] = $userCompanyMailData['company_name'];
                $template = 'emails.time_entry_update';
                $subject = 'Update Time Entry';
                $user = new User();
                $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany(Auth::user()->company_id);
                $to = $hr_admin_email['hr_email_address'];
                $cc = $hr_admin_email['email'];

                $data['oldEntryData'] = $oldEntryData;
                $data['newEntryData'] = $newEntryData;
                $data['username'] = Auth::user()->first_name. ' '.Auth::user()->last_name;
                $data['date'] = ($attendanceDate->entry_date)?$attendanceDate->entry_date:'';
                $data['empname'] = ($attendanceDate->emp_name)?$attendanceDate->emp_name:'';
                $bcc = '';
                MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);

              //  dd($newEntryData);
                Alert::success('Time entry is successfully updated');
                return Redirect::back();
            } catch (Exception $ex) {
                dd($ex->getMessage());
                Alert::error('Oops, Time entry not successfully updated!');
                return Redirect::back();
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function manageUserDays($attendanceId) {
        $attendance = Attendance::find($attendanceId);
        if($attendance != null)
        {
            $isAbsent = $attendance->absent == 1?1:null;
            $isHalfDay = $attendance->half_day == 1?1:null;
            $isFullDay = $attendance->full_day == 1?1:null;
        }

        $attendance_detail = AttendanceDetail::selectRaw('SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))) AS working_hours')
                ->where('attendance_id', $attendanceId)
                ->first();

        $parsed = date_parse($attendance_detail->working_hours);
        $total_working = (int) $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

        if($attendance_detail->working_hours == "00:00:00")
                    {
                        $total_working = 1;
                    }
        $attendance->incorrect_entry = ($total_working <= 0) ? 1 : null;
        $attendance->full_day = ($total_working >= config('constant.full_day_seconds')) ? 1 : null;
        $attendance->half_day = ($total_working >= config('constant.half_day_seconds') && $total_working < config('constant.full_day_seconds')) ? 1 : null;
        $attendance->early_going = ($total_working >= config('constant.full_day_seconds') && $total_working < config('constant.early_leave_end_seconds')) ? 1 : null;
        $attendance->absent = ($total_working > 0 && $total_working < config('constant.half_day_seconds')) ? 1 : null;
        $company = new Company();
        $companyId = $company->getCompanyDataByName($attendance->emp_comp)->id;
        $user = User::where('employee_id', $attendance->emp_code)->where('company_id',$companyId)
                ->first();

        if ($user) {
            $parsed = date_parse($user->from_shift);
            $shift_start = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

            $parsed = date_parse($attendance->first_in);
            $first_in_seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

            $setting = new Setting();
            if (!empty($setting->getSettingData('LateComeTime'))) {
                $settingData = $setting->getSettingData('LateComeTime')[0];
                $attendance->late_comer = ($first_in_seconds > $shift_start + $settingData['constant_str_value'] && $attendance->half_day != 1) ? 1 : null;
            }

            if($attendance->full_day == 1)
            {
                $autoLeave = Leave::where('leave_start_date',$attendance->entry_date)->where('leave_end_date',$attendance->entry_date)->where('user_id',$user->id)->whereNull('deleted_at')->first();
                if($autoLeave != null){
                    Leave::destroy($autoLeave->id);
                    LeaveDetail::where('leave_group_id', $autoLeave->id)->delete();
                    $destroyTakenLeave = 1;
                }
                if(isset($isAbsent) && $isAbsent == 1)
                {
                    $operation = "minus";
                    $leaveDays = 1;
                    $days = 0;
                    if(isset($destroyTakenLeave) && $destroyTakenLeave == 1 ){
                        $this->updateTakenLeave($user->id,$leaveDays,$operation,$attendance->entry_date);
                    }
                }
                if(isset($isHalfDay) && $isHalfDay == 1)
                {
                    $operation = "minus";
                    $leaveDays = 0.5;
                    $days = 0;
                    if(isset($destroyTakenLeave) && $destroyTakenLeave == 1 ){
                        $this->updateTakenLeave($user->id,$leaveDays,$operation,$attendance->entry_date);
                    }
                }
            }

            if($attendance->half_day == 1)
            {

                if(isset($isAbsent) && $isAbsent == 1)
                {
                    $days = 1;
                    $status = $this->updateLeaveData($user->id,$attendance->entry_date,$days);
                    $operation = "minus";
                    $leaveDays = 0.5;
                    if( $status == "1" ){
                        $this->updateTakenLeave($user->id,$leaveDays,$operation,$attendance->entry_date);
                    }
                }

                if(isset($isFullDay) && $isFullDay == 1)
                {
                    $days = 1;
                    $status = $this->updateLeaveData($user->id,$attendance->entry_date,$days);
                    $operation = "plus";
                    $leaveDays = 0.5;
                    if( $status == "1" ){
                        $this->updateTakenLeave($user->id,$leaveDays,$operation,$attendance->entry_date);
                    }
                }
            }
            if($attendance->absent == 1)
            {
                if(isset($isFullDay) && $isFullDay == 1)
                {
                    $days = 0;
                    $status = $this->updateLeaveData($user->id,$attendance->entry_date,$days);
                    $operation = "plus";
                    $leaveDays = 1;
                    if( $status == "1" ){
                        $this->updateTakenLeave($user->id,$leaveDays,$operation,$attendance->entry_date);
                    }
                }
                if(isset($isHalfDay) && $isHalfDay == 1)
                {
                    $days = 0;
                    $status = $this->updateLeaveData($user->id,$attendance->entry_date,$days);
                    $operation = "plus";
                    $leaveDays = 0.5;
                    if( $status == "1" ){
                        $this->updateTakenLeave($user->id,$leaveDays,$operation,$attendance->entry_date);
                    }
                }
            }
        }

        $attendance->save();

        return true;
    }

    public function formatTime($time) {
        if (empty($time)) {
            return $time;
        } else {
            $array = explode(':', $time);

            if (count($array) == 3) {
                return $time;
            } elseif (count($array) == 2) {
                return $time . ':00';
            } else {
                return $time . ':00:00';
            }
        }
    }

    public function getUserId($empCode,$cmp_id="")
    {
        $user = new User();
        $userId = $user->getUserDataByEmpCode($empCode,$cmp_id);
        return $userId['id'];
    }

    public function updateLeaveData($userId,$entryDate,$days)
    {
        if(!(new \App\Helpers\CommonHelper)->is_holiday(date('Y-m-d',strtotime($entryDate)))) {
            if($days == 0){$leaveDays = 1;}
            if($days == 1){$leaveDays = 0.5;}

            $autoLeave = Leave::where('leave_start_date','<=', date('Y-m-d',strtotime($entryDate)))->where('leave_end_date','>=',date('Y-m-d',strtotime($entryDate)))->where('user_id',$userId)->whereNull('deleted_at')->first();

            if($autoLeave == null)
            {
                $this->addNewLeaves($userId,$entryDate,$days);
                return 1;
            }
            else if($autoLeave->leave_days != $leaveDays && $autoLeave->leave_days <= 1 ){
                $this->updateLeaves($userId,$entryDate,$days,$autoLeave->id);
                return 1;
            }
            else{
                return 0;
            }
        } else {
            return 2;
        }
    }

    public function addNewLeaves($userId,$entryDate,$days)
    {
        if(!(new \App\Helpers\CommonHelper)->is_holiday(date('Y-m-d',strtotime($entryDate)))) {
            $userData= User::find($userId);
            $user = new User();
            $summary['start_date'] = $entryDate;
            $summary['end_date'] = $entryDate;
            $summary['start_type'] = $days;
            $summary['end_type'] = $days;
            $summaryDetails  = $this->getLeaveSummary($summary);
            $userDetails = $user->getUserDetails($userId);
            $leave = new Leave;
            $leave->user_id = $userData->id;
            $leave->approver_id = $userData->reporting_to;
            $leave->leave_start_date = $entryDate;
            $leave->leave_end_date = $entryDate;
            $leave->return_date = date("Y-m-d", strtotime($summaryDetails['return_date']));
            $leave->leave_days = $summaryDetails['leave_days'];
            if($leave->leave_days == 0.5){
            $leave->leave_start_type = 1;
            $leave->leave_end_type = 1;
            }else{
            $leave->leave_start_type = 0;
            $leave->leave_end_type = 0;
            }
            $leave->is_adhoc = 1;
            $leave->reason = "Auto Generated Leave Adhoc Leave";
            $leave->approver_comment = '';
            $leave->leave_status = 2;
            $leave->save();
            $leaveID = $leave->id;
            $leave1 = new Leave();
            $leaveDetails = $leave1->getLeaveDetails($leave->id);

            $obj_leave_detail = new LeaveDetail();
            $obj_leave_detail->updateLeaveDetail($leaveDetails);
            $company     = new Company();
            $userCompanyMailData  = $company->getCompanyViseMailData($userData->company_id)[0];

            $data['regards'] = $userCompanyMailData['company_name'];
            $data['start_date'] = $entryDate;
            $data['employee_name'] = $userData->first_name.' '.$userData->last_name;
            $data['leave_days'] = $summaryDetails['leave_days'];
            $data['reason'] = $leave->reason;
            $data['id'] = $leaveID;
            $from['email'] = $userCompanyMailData['from_address'];
            $from['name']  = $userCompanyMailData['company_name'];

            $approverData = (new \App\Helpers\CommonHelper)->getUserById($userData->reporting_to);
            $to = $userData->email;
            $cc = isset($approverData['email']) ? $approverData['email'] : '';
            $template = 'emails.auto_leave_request';
            $subject = 'Auto Adhoc Leave Apply Of '.$entryDate;

            $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany($userData->company_id);
            $cc = $hr_admin_email['hr_email_address'];
            $bcc = $hr_admin_email['email'];

            MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);
        }
    }

    public function updateLeaves($userId,$entryDate,$days,$leaveId)
    {
        if(!(new \App\Helpers\CommonHelper)->is_holiday(date('Y-m-d',strtotime($entryDate)))) {
            $userData= User::find($userId);
            $summary['start_date'] = $entryDate;
            $summary['end_date'] = $entryDate;
            $summary['start_type'] = $days;
            $summary['end_type'] = $days;
            $summaryDetails  = $this->getLeaveSummary($summary);

            $leave = Leave::find($leaveId);
            $leave->leave_start_date = $entryDate;
            $leave->leave_end_date = $entryDate;
            $leave->return_date = date("Y-m-d", strtotime($summaryDetails['return_date']));
            $leave->leave_days = $summaryDetails['leave_days'];
            if($leave->leave_days == 0.5){
            $leave->leave_start_type = 1;
            $leave->leave_end_type = 1;
            }else{
            $leave->leave_start_type = 0;
            $leave->leave_end_type = 0;
            }
            $leave->is_adhoc = 1;
            $leave->reason = "Auto Generated Leave Adhoc Leave";
            $leave->approver_comment = '';
            $leave->leave_status = 2;
            $leave->save();
            $leave = new Leave();
            $leaveDetails = $leave->getLeaveDetails($leaveId);
            LeaveDetail::where('leave_group_id', $leaveId)->forceDelete();
            $obj_leave_detail = new LeaveDetail();
            $obj_leave_detail->createLeaveDetail($leaveDetails);
        }
    }

    public function deletePunchData(Request $request) {
        // try {
        //     $attendanceDetailId = $request->input('attendanceDetailId');

        //     $attendanceDetail = AttendanceDetail::find($attendanceDetailId);
        //     $attendanceId = $attendanceDetail->attendance_id;
        //     $attendanceDetail->delete();

        //     $punchData = AttendanceDetail::where('attendance_id', $attendanceId)->get();

        //     if (count($punchData) > 0) {
        //         $firstIn = (isset($punchData[0])) ? $this->formatTime($punchData[0]['in_time']) : '00:00:00';
        //         $lastOut = (isset($punchData[count($punchData) - 1])) ? $this->formatTime($punchData[count($punchData) - 1]['out_time']) : '00:00:00';

        //         if ($firstIn != '' && $lastOut != '') {
        //             if ($firstIn != '00:00:00' && $lastOut != '00:00:00') {
        //                 $oldObj = Attendance::find($attendanceId);
        //                 $obj = Attendance::find($attendanceId);
        //                 $obj->incorrect_entry = null;
        //                 if($obj->save()){
        //                     if(count($punchData) == 1){
        //                         $days = 0;
        //                         $leaveDays = 0;
        //                         $operation = "plus";
        //                         if($oldObj->half_day != null && $oldObj->half_day == 1){
        //                             $leaveDays=0.5;
        //                             $operation = "plus";
        //                         }
        //                         if($oldObj->full_day != null && $oldObj->full_day == 1){
        //                             $leaveDays=1;
        //                             $operation = "plus";
        //                         }
        //                         $company = new Company();
        //                         $companyId = $company->getCompanyDataByName($obj->emp_comp)->id;
        //                         $userId = $this->getUserId($obj->emp_code,$companyId);
        //                         $this->updateLeaveData($userId,$obj->entry_date,$days);
        //                         $this->updateTakenLeave($userId,$leaveDays,$operation,$obj->entry_date);
        //                     }
        //                 }
        //             }
        //         }
        //     } else if (count($punchData) == 0) {
        //         $leaveDays=0;
        //         $days = 0;
        //         $operation = "plus";
        //         $oldObj = Attendance::find($attendanceId);
        //         if($oldObj->half_day != null && $oldObj->half_day == 1){
        //             $days = 0;
        //             $leaveDays=0.5;
        //             $operation = "plus";
        //         }
        //         if($oldObj->full_day != null && $oldObj->full_day == 1){
        //             $leaveDays=1;
        //             $operation = "plus";
        //         }
        //         $obj = Attendance::find($attendanceId);
        //         $obj->first_in = '00:00:00';
        //         $obj->last_out = '00:00:00';
        //         $obj->incorrect_entry = null;
        //         $obj->full_day = null;
        //         $obj->half_day = null;
        //         $obj->early_going = null;
        //         $obj->absent = 1;
        //         if($obj->save()){
        //             $company = new Company();
        //             $companyId = $company->getCompanyDataByName($obj->emp_comp)->id;
        //             $userId = $this->getUserId($obj->emp_code,$companyId);
        //             $this->updateLeaveData($userId,$obj->entry_date,$days);
        //             $this->updateTakenLeave($userId,$leaveDays,$operation,obj->entry_date);
        //         }
        //         $insert_attendance = new AttendanceDetail();
        //         $insert_attendance->attendance_id = $attendanceId;
        //         $insert_attendance->in_time = '00:00:00';
        //         $insert_attendance->out_time = '00:00:00';
        //         $insert_attendance->save();
        //     }

        //     return Response::json(array('message' => 'Time Entry Deleted Successfully', 'status' => 'success'));
        // } catch (Exception $ex) {
        //     return Response::json(array('message' => 'Time Entry Not Deleted Successfully', 'status' => 'error'));
        // }
        return Response::json(array('message' => 'Time Entry Deleted Successfully', 'status' => 'success'));
    }

    public function DetailTimeEntry($attendance_id, Request $request) {
        $companyObj = new Company();
        $attendance_data = Attendance::find($attendance_id);
        $company_id = $companyObj->getcompanyIdbyName($attendance_data->emp_comp);
        $getProjectData = array();
        if (($attendance_data) != NULL) {
            $getProjectData = ProjectEntry::leftjoin('users', 'project_entries.user_id', 'users.id')
                    ->leftJoin('projects', 'project_entries.project_id', 'projects.id')
                    ->leftJoin('misc_beanch_task_entries as mbte', 'project_entries.id', 'mbte.project_entries_id')
                    ->leftJoin('misc_beanch_task as mbt', 'mbte.misc_beanch_task_id', 'mbt.id')
                    ->where('project_entries.log_date', $attendance_data->entry_date)
                    ->where('users.employee_id', $attendance_data->emp_code)
                    ->where('users.company_id', $company_id)
                    ->where('users.status',1)
                    ->select('projects.project_name', 'project_entries.log_hours', 'project_entries.desc', 'project_entries.user_id', 'project_entries.id', 'mbt.task_title');

            $page_limit = ($request['page_range']) ? $request['page_range'] : config('constant.recordPerPage');
            if ($request->has('sort') && $request->input('sort') != '') {
                $getProjectData = $getProjectData->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $getProjectData = $getProjectData->sortable()->orderBy('id', 'desc')->paginate($page_limit);
            }
        }
        echo view('pms.timesheet.detailSummary', compact('attendance_data', 'getProjectData'));
    }

    public function timeEntryDalete(Request $request) {
        try {
            $attendance = new Attendance();
            $attendanceDetail = new AttendanceDetail();
            $attendanceIdArray = array();
            $update_leave = 0;
            $companyId = auth()->user()->company_id;
            $entryDate = date('Y-m-d',strtotime($request->input('entryDate')));
            $timeEnterCompanyArray = getTimeEntryCompanyFromHelper();
            $companyName = $timeEnterCompanyArray[$companyId];
            $user = new User();
            $userId = $user->getUserByCompanyName($companyName);
            $selectData = $attendance->where('entry_date', $entryDate)->where('emp_comp', $companyName)->get();
            if (!$selectData->isEmpty()) {
                foreach ($selectData as $val) {
                    $attendanceIdArray[] = $val->id;
                }
            }
            if (!empty($attendanceIdArray)) {
                $attendance->whereIn('id', $attendanceIdArray)->forceDelete();
                $attendanceDetail->whereIn('attendance_id', $attendanceIdArray)->forceDelete();
                Leave::whereIn('user_id',$userId)->where('leave_start_date',$entryDate)->where('leave_end_date',$entryDate)->where('reason','Auto Generated Leave Adhoc Leave')->delete();
                LeaveDetail::whereIn('user_id',$userId)->where('leave_date',$entryDate)->where('reason','Auto Generated Leave Adhoc Leave')->delete();
                return Response::json(array('message' => 'Time Entry Deleted Successfully', 'status' => 'success'));
            } else {
                return Response::json(array('message' => 'Data Not Exist', 'status' => 'error'));
            }
        } catch (Exception $e) {
            return Response::json(array('message' => 'Time Entry Not Deleted Successfully', 'status' => 'error'));
        }
    }

    public function updateTakenLeave($userId,$leaveDays,$operation,$entryDate){
        if(!(new \App\Helpers\CommonHelper)->is_holiday(date('Y-m-d',strtotime($entryDate)))) {
            $userData= User::find($userId);
            $user = new User();
            $userDetails = $user->getUserDetails($userId);
            if($operation == "plus")
            {
                $update_leave = $userDetails['used_leaves'] + $leaveDays;
            }
            if($operation == "minus"){
                $update_leave = $userDetails['used_leaves'] - $leaveDays;
            }
            $user->updateUserUsedLeave($userData->id,$update_leave);
        }
    }

    public function getLeaveSummary($data){
        $datediff = 0;
        $startDate = isset($data['start_date']) ? date("Y-m-d", strtotime($data['start_date'])) : '';
        $endDate = isset($data['end_date']) ? date("Y-m-d", strtotime($data['end_date'])) : '';

        $leave = new Leave;
        $LeaveTotalDates = (new \App\Helpers\CommonHelper)->getAllDates($data['start_date'],$data['end_date']);
        foreach($LeaveTotalDates as $LeaveDate){
            $isHoliday = $leave->isHoliday($LeaveDate);
            if(!$isHoliday){
                $datediff++;
            }
        }
        $leaveDays = $datediff;

        $startType = isset($data['start_type']) ? $data['start_type'] : '';
        $endType = isset($data['end_type']) ? $data['end_type'] : '';

        if($startType != 0){
            $leaveDays = $leaveDays - 0.5;
        }
        if($endType != 0){
            $leaveDays = $leaveDays - 0.5;
        }
        if($datediff == 1 && ($startType != 0 || $endType != 0)){
            $leaveDays = abs($leaveDays - 0.5);
        }
        if($endType == 1){
            $returnDate = date('Y-m-d', strtotime($endDate));
        }else{
            $returnDate = date('Y-m-d', strtotime($endDate . ' +1 Weekday'));
        }
        $returnData['return_date'] = $returnDate;
        $returnData['leave_days'] = $leaveDays;

        return $returnData;
    }

    public function validateUserPunchData(Request $request){
        $rules = [];
        $message = [];
        if(isset($request->getEnteryKey) && $request->getEnteryKey != ""){
            $punchKey = explode(',', $request->getEnteryKey);
            if(count($punchKey) > 0){
                foreach($punchKey as $key => $value){
                    if($key == 0){
                        $prevKey = $key;
                        $rules['inEntry_'.$value] = ['required','date_format:H:i:s','after:00:00:00'];
                    } else {
                        $prevKey = $key -1;
                        $rules['inEntry_'.$value] = ['required','date_format:H:i:s','after:00:00:00','after_or_equal:outEntry_'.$punchKey[$prevKey]];
                    }

                    $rules['outEntry_'.$value] = ['required','date_format:H:i:s','after:00:00:00','after_or_equal:inEntry_'.$value];
                    $message['inEntry_'.$value.'.required'] = "Please enter in time.";
                    $message['outEntry_'.$value.'.required'] = "Please enter out time.";
                    $message['inEntry_'.$value.'.date_format'] = "Time entry format must be HH:MM:SS.";
                    $message['outEntry_'.$value.'.date_format'] = "Time entry format must be HH:MM:SS.";
                    $message['inEntry_'.$value.'.after'] = "Please enter time after 00:00:00.";
                    $message['outEntry_'.$value.'.after'] = "Please enter time after 00:00:00.";
                    $message['inEntry_'.$value.'.after_or_equal'] = "Please enter time after or equal previous text box.";
                    $message['outEntry_'.$value.'.after_or_equal'] = "Please enter time after or equal previous text box.";
                }
            }
        } else {
            return Response::json(array('message' => 'Please add proper time entry and try again...', 'status' => 'error'));
        }

        $this->validate($request, $rules,$message);
        return response()->json(['status' => 'success']);
    }

     public function updateLeave($absetIdArrayVal,$absentDate,$type)
    {
        $userObj = new User();
        $uploadedCompanyId = Auth::user()->company_id;
        $userName = (new \App\Helpers\CommonHelper)->getUserByEmpCodeWithCompany($absetIdArrayVal,$uploadedCompanyId);
            $exclude = array();
           $excludeobj = User::where('want_to_exclude',1)->select('first_name','last_name')->get();
                            foreach($excludeobj as $key=>$value)
                            {
                                $exclude[] = $value->first_name.' '.$value->last_name;
                            }
                            // $exclude = (array)$excludeobj;
                            // $exclude = implode(', ', $excludeArray);
        if(isset($userName)){
            $companyName = (new \App\Helpers\CommonHelper)->getCompanyDetails($userName['company_id']);
            $emp_name = isset($userName['first_name']) ? $userName['first_name'].' '.$userName['last_name'] : '';
            // $exclude = array('RameshSir','Ramesh Marand','Sikandar Belim(Rajubhai)','Jaimit Modi','VinaySir','Shreya Shah','Mauli Doshi','Super Admin');
            if(!in_array($emp_name,$exclude)){
                $leave = new Leave;
                $checkLeave = $leave->where('user_id',$userName['id'])
                                ->whereDate('leaves.leave_start_date','<=', $absentDate)
                                ->whereDate('leaves.leave_end_date','>=', $absentDate)->first();

                $user = new User();
                $summary['start_date'] = $absentDate;
                $summary['end_date'] = $absentDate;
                $summary['start_type'] = $type;
                $summary['end_type'] = $type;
                $summaryDetails = $this->getLeaveSummary($summary);
                $userDetails = $user->getUserDetails($userName['id']);
                $update_leave = $userDetails['used_leaves'] + $summaryDetails['leave_days'];

                if(!isset($checkLeave)){
                    $leave->user_id = $userName['id'];
                    $leave->approver_id = $userName['reporting_to'];
                    $leave->leave_start_date = $absentDate;
                    $leave->leave_end_date = $absentDate;
                    $leave->return_date = date("Y-m-d", strtotime($summaryDetails['return_date']));
                    $leave->leave_days = $summaryDetails['leave_days'];
                    $leave->leave_start_type = 0;
                    $leave->leave_end_type = 0;
                    $leave->is_adhoc = 1;
                    $leave->reason = "Auto Generated Leave Adhoc Leave";
                    $leave->approver_comment = '';
                    $leave->leave_status = 2;
                    $leave->save();
                    $leaveID = $leave->id;

                    $user->updateUserUsedLeave($userName['id'],$update_leave);

                    $company     = new Company();
                    $userCompanyMailData  = $company->getCompanyViseMailData($userName['company_id'])[0];

                    $data['regards'] = $userCompanyMailData['company_name'];
                    $data['start_date'] = $absentDate;
                    $data['employee_name'] = $emp_name;
                    $data['leave_days'] = $summaryDetails['leave_days'];
                    $data['reason'] = $leave->reason;
                    $data['id'] = $leaveID;
                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name'];


                    $approverData = (new \App\Helpers\CommonHelper)->getUserById($userName['reporting_to']);
                    $to = $userName['email'];
                    $cc = isset($approverData['email']) ? $approverData['email'] : '';
                    $template = 'emails.auto_leave_request';
                    $subject = 'Auto Adhoc Leave Apply Of '.$absentDate;

                    $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany($userName['company_id']);
                    $cc = $hr_admin_email['hr_email_address'];
                    $bcc = $hr_admin_email['email'];

                    MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);
                }
            }
        }
    }
     public function createStatus($status)
    {
        TimeEntryStatus::where(['emp_name' => $status['name'], 'emp_comp' => Auth::user()->company_id, 'entry_date' => date("Y-m-d", strtotime($status['sheetDate']))])->delete();
        $timeEntryStatus = new TimeEntryStatus();
        $timeEntryStatus->emp_code = $status['empcode'];
        $timeEntryStatus->emp_name = $status['name'];
        $timeEntryStatus->punch_time = $status['punch_time'];
        $timeEntryStatus->emp_comp = Auth::user()->company_id;
        $timeEntryStatus->entry_date = date("Y-m-d", strtotime($status['sheetDate']));
        $timeEntryStatus->status = $status['status'];
        $timeEntryStatus->created_by = Auth::user()->id;
        $timeEntryStatus->created_date = date("Y-m-d");
        $timeEntryStatus->save();
    }
    public function timeEntriesUsingCron(Request $request)
    {
        $date = date('Y-m-d',strtotime($request->entryDate));
        $company = new Company();
        $comp_id = Auth::user()->company_id;
        $loginUserCompanyCode = $company->getCompanyDataById($comp_id)->emp_short_name;
        $loginUserCompanyName = $company->getCompanyDataById($comp_id)->company_name;
        $exitingData = Attendance::where("emp_comp",$loginUserCompanyName)->where('entry_date',$date)->get()->toArray();
        if(count($exitingData) > 0) {
            return response()->json(['status' => "error", 'message' => "There are some entry exist for $date! please delete the entry and try again!"]);
        } else {
            TimeSheetHelper::importTimeEntries(["company_id" => $comp_id,"date" => $date]);
            return response()->json(['status' => "success", 'message' => "Time Entry Uploaded Successfully for $date"]);
        }
    }

}
