<?php

namespace App\Http\Controllers;

use Excel;
use Illuminate\Http\Request;
use Config;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
Use Alert;
use App\Jobs\projectEntry;
use Illuminate\Support\Facades\Storage;
use App\Imports\ImportProjectEntry;
use Illuminate\Support\Facades\Session;
use DateTime;
use Exception;
use Response;

class UploadProjectEntryController extends Controller {

    public function uploadProjectEntry(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadprojectentry')) {
            if ($request->hasFile('csvfile')) {      
                $method = strtolower($request->method());
                if ($method == 'post') {
                    $validator=Validator::make($request->all(),[
                        'csvfile'=>'required|mimes:xlsx,xls'
                    ]); 
                    // if validation fails
                    if ($validator->fails()) {
                        
                     Alert::error('Oops, Invalid file type or file seems to be missing!');
                        return Redirect::to('uploadProjectEntry')
                        ->withErrors($validator)
                        ->withInput();
                    }
                    else {
                        $file = $request->file('csvfile');
                        $extension = $file->getClientOriginalExtension();
                        $filename = 'project_entry_'.time().'.'.$extension;
                        if ($request) {
                            Storage::disk('local')->putFileAs('', $file, $filename);
                        }
                        $importError = array();
                        Excel::import(new ImportProjectEntry, $filename);
                        $importError = Session::get('projectEntryImportError');
                           
                        $type = 'success';
                        if(count($importError)>0){
                            $message = '';
                            if(count($importError[0]['userId'])>0){
                                $message .= implode(",",$importError[0]['userId']).' Employee code is not valid. ';
                                $type = 'warning';
                            }if(count($importError[0]['projectId'])>0){
                                $message .= implode(",",$importError[0]['projectId']).' project name is not valid. ';
                                $type = 'warning';
                            }if(count($importError[0]['miscTask'])>0){
                                $message .= implode(",",$importError[0]['miscTask']).' Project task name,tast start date and task end date require. ';
                                $type = 'warning';
                            }if(count($importError[0]['otherTaskError'])>0){
                                $message .= implode(",",$importError[0]['otherTaskError'])." ";
                                $type = 'warning';
                            } if($importError[0]['success'] != ''){
                                $message .= $importError[0]['success'].' Project Entry Successfully Uploaded.';
                            }     
                            Alert::$type($message)->persistent("Close");
                        }
                        $importError = Session::forget('projectEntryImportError');
                        return Redirect::to('uploadProjectEntry');
                    }
                }
                else {
                    return view('pms.uploadProjectEntry');
                }
            }
            return view('pms.uploadProjectEntry');
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
}
