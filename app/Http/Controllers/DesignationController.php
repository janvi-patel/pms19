<?php

namespace App\Http\Controllers;

use App\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Alert;
use Response;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        if (Auth::check() && Auth::user()->hasPermission('designation.listing')) {
            $designationData = array();
            $designation = new Designation();
            $page       = $request->get('page');
            $designationData = $designation->getDesignationList($request);
            
            
            return view('pms.designations.index', compact('request','designationData', 'page'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check() && Auth::user()->hasPermission('designation.create')) {
            $designation = new Designation();
            $designationData = $designation->getDesignationList();

            return view('pms.designations.create', compact('designationData'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check() && Auth::user()->hasPermission('designation.create')) {
            $rules = array(
                'designation_name'       => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

             if ($validator->fails()) {
                Alert::error('Oops, Form has some error!');
                return Redirect::to('designation/create')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                // store
                $designation = new Designation;
                $designation->designation_name = Input::get('designation_name');
                $designation->save();

                // redirect
                Alert::success('Designation Successfully Created!');
                return Redirect::to('designation');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Designation  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Designation  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check() && Auth::user()->hasPermission('designation.edit')) {
            $designation = new Designation();

            $designationData = $designation->getDesignationList();
            
            $designationDetails = $designation->getDesignationDetails($id);
            return view('pms.designations.edit', compact('designationData', 'designationDetails'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Designation  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        if (Auth::check() && Auth::user()->hasPermission('designation.edit')) {
            $rules = array(
                'designation_name'      => 'required',
             );
            $validator = Validator::make(Input::all(), $rules);

            // if validation fails
            if ($validator->fails()) {
                Alert::error('Oops, Form has some error!');
                return Redirect::to('designation/edit/'.$id)
                    ->withErrors($validator)
                    ->withInput();
            } else {
                // store
                $designation = Designation::find($id);
                $designation->designation_name = Input::get('designation_name');
                $designation->save();

                // redirect
                Alert::success('Designation Successfully Updated!');
                return Redirect::to('designation');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Designation  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $id = $request->id; 
        if (Auth::check() && Auth::user()->hasPermission('designation.delete')) {
            $designation = Designation::find($id);
            $designation->delete();

            return Response::json(array('message'=>'Designation Deleted Successfully','status'=>'success'));
            
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

}
