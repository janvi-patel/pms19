<?php

namespace App\Http\Controllers;

use App\Leave;
use App\User;
use Alert;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Response;
use Log;
use App\CompOff;
use App\Jobs\MailJob;
use App\Company;
use App\LeaveDetail;
use App\Attendance;
use App\WorkFromHome;
use App\EarlyLeave;
class EarlyLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userIdArray = array();
        $page       = $request->get('page');
        $authUser = Auth::user();
        $userName = new User();
        $company_id = '';
        if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
            $company_id = $authUser->company_id;
        }
        $approver_id = EarlyLeave::where('user_id','=',$authUser->id)->pluck("approver_id")->toArray();
        $userNameList = User::select('id','first_name','last_name')->whereIn('id',$approver_id)->get();

        $start_date = $request->search_date;
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

        $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
        if($loggedInUserID){
            $userIdArray = array($loggedInUserID);
        }

        $leave = EarlyLeave::with(['user', 'approver']);
        if (isset($request->id)) {
            $leaveStatus = $request->id;
        }else{
            $leaveStatus =0;
        }
        if ($userIdArray) {
            if ($leaveStatus != 0) {
                $leave->whereIn('status', [$leaveStatus]);
            }
            $leave->whereIn('user_id', $userIdArray);
        }
        if (!($request->has('search_date'))) {
            $start_date = date('d-m-Y',strtotime(date('01-04-Y')));
            if(date('m-d',strtotime($start_date)) > date('m-d'))
            {
                $year = date("Y", strtotime("-1 years"));
                $start_date = date('d-m-Y',strtotime(date('1-04-'.$year)));
            }
            $leave->where('date',  '>=',  date("Y-m-d", strtotime($start_date)));
        }
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                $leave->where('user_id',  '=', $request->search_by_employee);
            }
            if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                $leave->where('approver_id',  '=', $request->search_by_approver);
            }
            if ($request->has('search_date') && $request->search_date != '') {
                $leave->where('date', date("Y-m-d", strtotime($request->search_date)));
            }
            if ($request->has('search_by_status') && $request->search_by_status != '') {
                $leave->where('status',  '=', $request->search_by_status);
            }
        }
        $leavesData = $leave->sortable()->orderBy('date', 'desc')->paginate($page_limit);
        return view('pms.early_leave.index', compact('leavesData', 'loggedInUserID','page','userNameList','request','start_date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if (Auth::check()) {
            $authUser = Auth::user();
            $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
            $user = new User();
            $userDetails = $user->getUserDetails($loggedInUserID);
            $reportingTo = $user->getReportingToListWithOnlyTeamLeader($loggedInUserID);
            return view('pms.early_leave.create', compact('userDetails', 'reportingTo'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r($request->all());
        // exit;
        $user = new User();

        if (Auth::check() && Auth::user()->hasPermission('leave.create')) {
            $getUid = $request->user()->id;
            $total_days = 0;
            $postData = $request->input();
            $validationRule['reason'] = 'required|string|min:6';
            $validationRule['date'] = 'required|date_format:Y-m-d';


            $validator = Validator::make(Input::all(), $validationRule);

            // if validation fails
            if ($validator->fails()) {
                // Alert::error('Oops, Form has some error!');
                return Redirect::to('earlyLeave/add')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $start = $request->date;
                
                $monthlyEarlyLeave = getEarlyLeave(["select" => "id","where" =>["user_id"=>$getUid],"whereBetween" => ["column" => "date", "query" => [date("Y-m-01",strtotime($start)),date("Y-m-t",strtotime($start))]]]);
                //dd(count($monthlyEarlyLeave));
                if(count($monthlyEarlyLeave) >= 2){
                    alert()->info('you have already applied 2 leaves in that month.')->persistent('close')->autoclose("3600");
                    return Redirect::to('earlyLeaveRequest');
                }
                $checkLeave1 = EarlyLeave::where('user_id','=',$getUid)
                    ->where('date',date("Y-m-d",strtotime($start)))->count();

                if($checkLeave1 > 0){
                alert()->info('Your already have early leave on that day.')->persistent('close')->autoclose("3600");
                return Redirect::to('earlyLeaveRequest');
                }
               
                    
                    $leave = new EarlyLeave;
                    $leave->user_id = Auth::user()->id;
                    $leave->approver_id =  $user->getReportingToListWithOnlyTeamLeader(Auth::user()->id);
                    $leave->date = date("Y-m-d", strtotime(Input::get('date')));
                    $leave->reason = Input::get('reason');
                    $leave->approver_comment = '';
                    $leave->status = 2;
                    
                    $leave->save();
                    $leaveID = $leave->id;
                    $leaveDetails = $leave->getLeaveDetails($leaveID);
                    

                    // $obj_leave_detail = new LeaveDetail();
                    // $obj_leave_detail->createLeaveDetail($leaveDetails);


                    $user = new User();
                    $update_leave = 0;
                    // if((isset($postData['adhoc_leave']) && $postData['adhoc_leave'] == 'on')&& $leaveDetails['leave_status'] == 1){
                    //     $userDetails = $user->getUserDetails($leaveDetails['user']['id']);
                    //     $update_leave = $userDetails['used_leaves'] + $leaveDetails['leave_days'];
                    //     $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                    // }

                    $data = array();
                    $data['id'] = $leaveID;
                    $data['reporting_name'] = $leaveDetails['approver']['first_name'].' '.$leaveDetails['approver']['last_name'];
                    $data['employee_name'] = $leaveDetails['user']['first_name'].' '.$leaveDetails['user']['last_name'];
                    $data['date'] = date('jS F, Y', strtotime($leaveDetails['date']));
                    $data['reason'] = $leaveDetails['reason'];


                    $authUser = Auth::user();
                    $company_id = $authUser->company_id;
                    $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany($company_id);
                    $cc = $bcc = '';
                    $cc_array = array($hr_admin_email['hr_email_address']);
                    $pm_email = getUsersPm(Auth::user()->id);
                    if($pm_email != false && $pm_email != $leaveDetails['approver']['email']){
                        $cc_array[] = $pm_email;
                    }
                    $cc = $cc_array;
                    $bcc = $hr_admin_email['email'];

                    $company     = new Company();
                    $userCompanyMailData  = $company->getCompanyViseMailData($company_id)[0];
                    $data['regards'] = $userCompanyMailData['company_name'];
                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name'];

                    $to = $leaveDetails['approver']['email'];
                    $template = 'emails.early_leave_request';
                    $subject = 'Early Leave Application Of '.$data['employee_name'];
                     MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);
//dd("dsdsa");
                    // redirect
                    Alert::success('Early Leave Request Successfully Created!');
                    return Redirect::to('earlyLeaveRequest');
                      


                
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
            if ((Auth::user()->hasPermission('team.leaves.listing') || checkReprtingPerson(auth()->user()->id)) ) {
                $authUser = Auth::user();
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;

                $leave = new EarlyLeave();
                $leaveDetails = $leave->getLeaveDetails($id);
            //    dd($id);
                if(empty($leaveDetails)){
                    Alert::error('This request has been removed');
                    return Redirect::to('earlyLeaveRequest');
                }
                if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || in_array($leaveDetails['user_id'],getTeamUserId(auth()->user()->id))){
                    $user = new User();
                    $reportingToList = $user->getReportingToList();
                    return view('pms.early_leave.edit', compact('leaveDetails', 'reportingToList'));
                } else {
                    Alert::error('You do not have permission to perform this action!');
                    return Redirect::to('dashboard');
                }
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            if ((Auth::user()->hasPermission('team.leaves.listing') || checkReprtingPerson(auth()->user()->id))) {
                $leave = EarlyLeave::find($id);
                $loggedInUserID = isset(auth()->user()->id) ? intval(auth()->user()->id) : 0;

                if(empty($leave)){
                    Alert::error('This request has been removed');
                    return Redirect::to('teamworkfromhome');
                }

                if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || in_array($leave['user_id'],getTeamUserId(auth()->user()->id))) {
                    $postData = $request->input();

                    $validationRule['status'] = 'required|integer';
                    $validationRule['approver_comment'] = 'required|string|min:6';

                    $validator = Validator::make(Input::all(), $validationRule);

                    if ($validator->fails()) {
                        return Redirect::to('earlyLeave/approve/'.$id)
                            ->withErrors($validator)
                            ->withInput();
                    } else {
                        $beforeUpdate = $leave->getLeaveDetails($id);
                        $leave->approver_comment = Input::get('approver_comment');
                        $leave->status = Input::get('status');
                        $leave->approver_id = $loggedInUserID;
                        $leave->approver_date = date('Y-m-d');
                        $leave->save();

                        $leave = new EarlyLeave();
                        $leaveDetails = $leave->getLeaveDetails($id);
                        

                        $leaveStatusArray = config('constant.leave_status');

                        $user = new User();
                        $update_leave = 0;

                        $data = array();
                        $data['id'] = $id;
                        $data['reporting_name'] = $leaveDetails['approver']['first_name'].' '.$leaveDetails['approver']['last_name'];
                        $data['employee_name'] = $leaveDetails['user']['first_name'].' '.$leaveDetails['user']['last_name'];
                        $data['date'] = date('jS F, Y', strtotime($leaveDetails['date']));
                       // $data['leave_end_date'] = date('jS F, Y', strtotime($leaveDetails['end_date']));
                        $data['status'] = $leaveStatusArray[$leaveDetails['status']];

                        $company     = new Company();
                        $userCompanyMailData  = $company->getCompanyViseMailData($leaveDetails['user']['company_id'])[0];
                        $data['regards'] = $userCompanyMailData['company_name'];
                        $from['email'] = $userCompanyMailData['from_address'];
                        $from['name']  = $userCompanyMailData['company_name'];

                        $to = $leaveDetails['user']['email'];
                        $template = 'emails.early_leave_approve';
                        $subject = 'Your Early Leave Application has been '.$data['status'];

                        MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []);
                        // redirect
                        Alert::success('Early Leave Request Successfully Updated!');
                        return Redirect::to('teamEarlyLeave');
                    }
                } else {
                    Alert::error('You do not have permission to perform this action!');
                    return Redirect::to('dashboard');
                }
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function teamEarlyLeaveRequests(Request $request)
    {
       // dd("dsa");
        if (Auth::check()) {
            if ((Auth::user()->hasPermission('team.leaves.listing') || checkReprtingPerson(auth()->user()->id))) {

                 $year_start_date = date('Y-m-d',strtotime(date('01-04-Y')));
                    if(date('m-d',strtotime($year_start_date)) > date('m-d'))
                    {
                        $year = date("Y", strtotime("-1 years"));
                        $year_start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
                    }
                $company     = new Company();
                $companyList     = $company->getCompanyName();
                $leaveStatusSelected = '';
                if (isset($request->search_by_status) && $request->search_by_status != 2    ) {
                    $leaveStatusSelected = $request->search_by_status;
                }else{
                    $leaveStatusSelected = '2';
                }
                $userIdArray = $leavesData = $userNameArray = array();
                $authUser = Auth::user();
                $user = new User();

                $page       = $request->get('page');
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
                $start_date = $request->search_date;
              // $end_date = $request->search_end_date;
                if (Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                    $company_id = '';
                    if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
                        $company_id = $authUser->company_id;
                    }
                    $allUserList = $userNameList = $user->getUserNameByCompany($company_id);
                } else {
                    $allUserList = $user->getUserNameByCompany($authUser->company_id);
                    $userNameList = $user->getMyTeamList($loggedInUserID);
                }

                $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
                if(!empty($userNameList)){
                    foreach ($userNameList as $userDetail) {
                        $userIdArray[] = $userDetail['id'];
                        $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
                if(!empty($allUserList)){
                    foreach ($allUserList as $userDetail) {
                        $allUserListArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
                $search_cmp = '';
                if ($request->has('search_submit') && $request->search_submit != '') {
                    if ($request->has('search_by_leave_emp_company') && $request->search_by_leave_emp_company != '') {
                        $search_cmp = $request->search_by_leave_emp_company;
                    }
                }

                $leave = EarlyLeave::with(['user','approver']);
                $leave->leftJoin('users as user','user.id','early_leaves.user_id');
                $leave->leftJoin('companies as c','c.id','user.company_id');
                $leave->where('user.status',1);
                $leave->whereIn('user_id', $userIdArray);
                if($search_cmp){
                    $leave->where('user.company_id','=',$search_cmp);
                }

                if(!empty($userIdArray)){
                    if ($userIdArray) {
                        if ($leaveStatusSelected != '' && $leaveStatusSelected != 0) {
                            $leave->whereIn('early_leaves.status', [$leaveStatusSelected]);
                        }
                    }
                }
                if ($request->has('search_submit') && $request->search_submit != '') {
                    if ($request->has('search_by_department') && count($request->search_by_department) > 0 ){
                        $leave =   $leave->whereIn('user.department',($request->search_by_department));
                    }
                    if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                        $param = $request->search_by_internal_company_id;
                        if(in_array("parent_company",$param) && count($param) >= 2){
                            $leave =   $leave->where(function ($q) use ($param) {
                                $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                            });
                        } else if (in_array("parent_company",$param)) {
                            $leave =   $leave->WhereNull('internal_company_id');
                        } else {
                            $leave =   $leave->whereIn('internal_company_id', ($param));
                        }
                    }
                    if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                        $leave->where('early_leaves.user_id',  '=', $request->search_by_employee);
                    }
                    if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                        $leave->where('early_leaves.approver_id',  '=', $request->search_by_approver);
                    }
                    
                    if ($request->has('search_date') && $request->search_date != '') {
                        $leave->where('early_leaves.date', date("Y-m-d", strtotime($request->search_date)));
                    }
                    
                    if ($request->has('search_by_status') && $request->search_by_status != '' && $request->search_by_status != 0) {
                        $leave->where('early_leaves.status',  '=', $request->search_by_status);
                    }
                    
                 
                }else{
                   // echo "dsa";exit;
                    $leave->where('early_leaves.date',  '>=', $year_start_date);
                }
                $leave->select('early_leaves.*','c.company_name');
                if ($request->has('sort') && $request->input('sort') != '') {
                    $leavesData = $leave->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
                } else {
                    $leavesData = $leave->sortable()->orderBy('early_leaves.date', 'DESC')->paginate($page_limit);
                }
                if (empty($userIdArray) && (Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->designation_id == 16 || Auth::user()->designation_id == 26 )){
                    $leavesData = array();
                }
              //  echo "<pre>";print_r($leavesData);exit;
                return view('pms.early_leave.team_early_leave_requests', compact('allUserListArray','leavesData','companyList', 'loggedInUserID', 'leaveStatusSelected','page','userNameArray','request','start_date'));
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function updateCheckedEarlyLeaveAjax(Request $request) {
        $action = $request['action'];
        $user = new User();
        $update_leave = 0;
        if($action == 0){
            if(Auth::user()->hasRole(config('constant.admin_slug'))
            || Auth::user()->hasRole(config('constant.hr_slug'))
            || Auth::user()->hasRole(config('constant.superadmin_slug')))
            {
                $leave = new EarlyLeave();
                $leaveArray = $request['leaveIdArray'];
                foreach($leaveArray as $val){
                    $leaveEntry = EarlyLeave::find($val);
                    $leaveEntry->delete();
                   
                }
                return Response::json(array('message'=>'Early Leave Entry Deleted Successfully','status'=>'success'));
            }else{
                return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
            }
        }else{
            $leave = new EarlyLeave();
            $leave = EarlyLeave::query()->whereIn('id',$request['leaveIdArray'])->update(['status' => $action,'approver_date' => date('Y-m-d')]);
        }
    }
    public function earlyLeaveDelete(Request $request)
    {
        try{
           
            $update_leave = 0;
            $leaveId = $request->input('leaveId');
            $leaveEntry = EarlyLeave::find($leaveId);
            if(isset($leaveEntry))
            {
                if((!Auth::user()->hasRole(config('constant.admin_slug')))
                && (!Auth::user()->hasRole(config('constant.hr_slug')))
                && (!Auth::user()->hasRole(config('constant.superadmin_slug'))))
                {
                    if($leaveEntry->user_id != Auth::user()->id || date('Y-m-d') > $leaveEntry->date)
                    {
                        return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
                    }
                }

                $leaveEntry->delete();
            
               
                return Response::json(array('message'=>'Early Leave Entry Deleted Successfully','status'=>'success'));
            }else{
                return Response::json(array('message'=>'Something was wrong!','status'=>'error'));
            }
        }catch(Exception $e){
            return Response::json(array('message'=>'Early Leave Entry Deleted Successfully','status'=>'error'));
        }
    }

    public function isLeave($user_id,$date){
        $leave = EarlyLeave::query();
        $date = date("Y-m-d", strtotime($date));
        $leave->whereDate('date', $date);
        $leave->where('status', 1);   //Approved
        $leave->where('user_id', $user_id);   //Approved
        $leaveData = $leave->get();
        if(isset($leaveData[0])){
            return true;
        }else{
            return false;
        }
    }

    
}
