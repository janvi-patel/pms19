<?php

namespace App\Http\Controllers;

use Excel;
use Illuminate\Http\Request;
use Config;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
Use Alert;
use App\Jobs\UploadTask;
use Illuminate\Support\Facades\Storage;
use App\Imports\TaskImport;
use Illuminate\Support\Facades\Session;
use DateTime;
use Exception;
use Response;


class UploadTaskController extends Controller {

    public function uploadtask(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadtask')) {
            if ($request->hasFile('csvfile')) {
                $method = strtolower($request->method());
                if ($method == 'post') {
                    $validator=Validator::make($request->all(),[
                        'csvfile'=>'required|mimes:xlsx,xls'
                    ]);
                    // if validation fails
                    if ($validator->fails()) {
                        Alert::error('Oops, Invalid file type or file seems to be missing!');
                        return Redirect::to('uploadtask')
                        ->withErrors($validator)
                        ->withInput();
                    }
                    else {
                        $file = $request->file('csvfile');
                        $extension = $file->getClientOriginalExtension();
                        $filename = 'task_'.time().'.'.$extension;
                        if ($request) {
                            Storage::disk('local')->putFileAs('', $file, $filename);
                        }
                        $importError = array();
                        Excel::import(new TaskImport, $filename);
                        $importError = Session::get('taskImportError');

                        $type = 'success';
                        if(count($importError)>0){
                          
                            $message = '';
                            if($importError[0]['success'] != ''){
                                $message .= $importError[0]['success'].' Task Successfully Uploaded.    ';
                            }
                            if(count($importError[0]['taskname'])>0){
                                $message .= implode(", ",$importError[0]['taskname']);
                                $type = 'warning';
                            }
                            if(count($importError[0]['userId'])>0){
                                $message .= implode(", ",$importError[0]['userId']).' Employee code is not valid. ';
                                $type = 'warning';
                            }if(count($importError[0]['projectId'])>0){
                                $message .= implode(", ",$importError[0]['projectId']).' project name is not valid. ';
                                $type = 'warning';
                            }if(count($importError[0]['projectCost'])>0){
                                $message .= implode(", ",$importError[0]['projectCost']).' Project task hours is more than PMS hours. ';
                                $type = 'warning';
                            }if(count($importError[0]['projectStartDate'])>0){
                                $message .= implode(", ",$importError[0]['projectStartDate']).' Task start date format must be DD-MM-YYYY. ';
                                $type = 'warning';
                            } if(count($importError[0]['projectEndDate'])>0){
                                $message .= implode(", ",$importError[0]['projectEndDate']).' Task end date format must be DD-MM-YYYY. ';
                                $type = 'warning';
                            } if(count($importError[0]['projectDateError'])>0){
                                $message .= implode(", ",$importError[0]['projectDateError']);
                                $type = 'warning';
                            }
                            if(count($importError[0]['estimatedHours'])>0){
                                $message .= implode(", ",$importError[0]['estimatedHours']).' Estimated Hours is not valid. ';
                                $type = 'warning';
                            }
                            
                            Alert::$type($message)->persistent("Dismiss");
                        }
                        $importError = Session::forget('taskImportError');
                        return Redirect::to('uploadtask');
                    }
                }
                else {
                    return view('pms.uploadtask');
                }
            }
            return view('pms.uploadtask');
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
}
