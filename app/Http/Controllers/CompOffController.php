<?php

namespace App\Http\Controllers;

use App\CompOff;
use App\User;
use Mail;
use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Response;
use Log;
use DB;
use App\Jobs\MailJob; 
use App\Company;
use App\Leave;

class CompOffController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $userIdArray = array();
        $authUser = Auth::user();
        $userName = new User();
        $company_id = '';
        if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
            $company_id = $authUser->company_id;
        }
        $approver_id = CompOff::where('user_id','=',$authUser->id)->pluck("approver_id")->toArray();
        $userNameList = User::select('id','first_name','last_name')->whereIn('id',$approver_id)->get();
        $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
        $start_date = $request->search_compoff_start_date;
        $end_date = $request->search_compoff_end_date;
        $page       = $request->get('page');
        if ($loggedInUserID) {
            $userIdArray = array($loggedInUserID);
        }
        $compOffData = array();

        $compoff = new CompOff();
        $compOffData = $compoff->getCompOffIndexListing($userIdArray,$request);
        return view('pms.compoffs.index', compact('compOffData','page','loggedInUserID','userNameList','request','start_date','end_date'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        if (Auth::check()) {
            $authUser = Auth::user();
            $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;

            $user = new User();
            $userDetails = $user->getUserDetails($loggedInUserID);
            $reportingTo = $user->getReportingToListWithOnlyTeamLeader($loggedInUserID);
            return view('pms.compoffs.create', compact('userDetails', 'reportingTo'));
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
         echo '<pre>'; print_r($request->all());
        // echo '<pre>'; print_r(Auth::user()->id);
        // exit;
        if (Auth::check() && Auth::user()->hasPermission('tasks.create')) {

            $postData = $request->input();
            
            $validationRule['compoff_description'] = 'required|string|min:6';
            $validationRule['compoff_start_date'] = 'required|date_format:Y-m-d|before_or_equal:date';
            $validationRule['compoff_start_date'] = 'required|date_format:Y-m-d|after_or_equal:joinning_date';
            $validationRule['compoff_end_date'] = 'required|date_format:Y-m-d|after_or_equal:compoff_start_date';

            $validator = Validator::make(Input::all(), $validationRule);

            // if validation fails
            if ($validator->fails()) {
                // Alert::error('Oops, Form has some error!');
                return Redirect::to('compoffs/add')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                 $start = $request->compoff_start_date;
                $end = $request->compoff_end_date;
               
                $checkLeave = Leave::where('user_id','=',Auth::user()->id)
                                ->where(function ($query) use ($start, $end) {
                                 $query->where(function ($q) use ($start, $end) {
                                        $q->where('leave_start_date', '>=', $start)
                                           ->where('leave_start_date', '<', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('leave_start_date', '<=', $start)
                                           ->where('leave_end_date', '>', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('leave_end_date', '>=', $start)
                                           ->where('leave_end_date', '<=', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('leave_start_date', '>=', $start)
                                           ->where('leave_end_date', '<=', $end);
                                    });

                                })->count();
                $checkcompOff = CompOff::where('user_id','=',Auth::user()->id)
                                ->where(function ($query) use ($start, $end) {
                                 $query->where(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '>=', $start)
                                           ->where('compoff_start_date', '<', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '<=', $start)
                                           ->where('compoff_end_date', '>', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_end_date', '>=', $start)
                                           ->where('compoff_end_date', '<=', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '>=', $start)
                                           ->where('compoff_end_date', '<=', $end);
                                    });

                                })->count();
                if($checkcompOff > 0)
                {
                    alert()->info('Your already have comp-off on that day.')->persistent('close')->autoclose("3600");
                   return Redirect::to('compoffrequests');
                }        
                if($checkLeave > 0){
                   alert()->info('Your already have leave on that day.')->persistent('close')->autoclose("3600");
                   return Redirect::to('compoffrequests');
                }else{
                     $user = new User();

                $summary['compoff_start_date'] = $postData['compoff_start_date'];
                $summary['compoff_end_date'] = $postData['compoff_end_date'];
                $summary['compoff_start_type'] = $postData['compoff_start_type'];
                $summary['compoff_end_type'] = $postData['compoff_end_type'];
                $summaryDetails = $this->getCompoffSummary($summary);
                // store
                $compoff = new CompOff;
                $compoff->user_id = Auth::user()->id;
                $compoff->approver_id = $user->getReportingToListWithOnlyTeamLeader(Auth::user()->id);
                $compoff->compoff_start_date = date("Y-m-d", strtotime(Input::get('compoff_start_date')));
                $compoff->compoff_end_date = date("Y-m-d", strtotime(Input::get('compoff_end_date')));
                $compoff->compoff_days = $summaryDetails['compoff_days'];
                $compoff->compoff_start_type = Input::get('compoff_start_type');
                $compoff->compoff_end_type = Input::get('compoff_end_type');
                $compoff->compoff_description = Input::get('compoff_description');
                $compoff->compoff_status = 2;   //Pending
                $compoff->approver_comment = '';
                $compoff->save();

                $compoffID = $compoff->id;

                $compoffDetails = $compoff->getCompOffDetails($compoffID);
                
                $data = array();
                $data['id'] = $compoffID;
                $data['reporting_name'] = $compoffDetails['approver']['first_name'] . ' ' . $compoffDetails['approver']['last_name'];
                $data['employee_name'] = $compoffDetails['user']['first_name'] . ' ' . $compoffDetails['user']['last_name'];
                $data['compoff_start_date'] = date('jS F, Y', strtotime($compoffDetails['compoff_start_date']));
                $data['compoff_end_date'] = date('jS F, Y', strtotime($compoffDetails['compoff_end_date']));
                $data['compoff_days'] = $compoffDetails['compoff_days'];
                $data['compoff_description'] = $compoffDetails['compoff_description'];
               
                
                $authUser = Auth::user();
                $company_id = $authUser->company_id;
                
                $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany($company_id);
                $cc = $hr_admin_email['hr_email_address']; // hr email
                $bcc = $hr_admin_email['email']; //admin email
                $company     = new Company();
                $userCompanyMailData  = $company->getCompanyViseMailData($company_id)[0];  
                $data['regards'] = $userCompanyMailData['company_name'];
                 
                $from['email'] = $userCompanyMailData['from_address'];
                $from['name']  = $userCompanyMailData['company_name'];  
                $to = $compoffDetails['approver']['email'];
                $template = 'emails.compoff_request';
                $subject = 'Comp-off Request Of ' . $data['employee_name'];
                MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);  
                
                // redirect
                Alert::success('Comp-off Request Successfully Created!');
                return Redirect::to('compoffrequests');
                }
           
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function getCompoffSummary($data) {

        $startDate = isset($data['compoff_start_date']) ? date("Y-m-d", strtotime($data['compoff_start_date'])) : '';
        $endDate = isset($data['compoff_end_date']) ? date("Y-m-d", strtotime($data['compoff_end_date'])) : '';
        $startType = isset($data['compoff_start_type']) ? intval($data['compoff_start_type']) : '';
        $endType = isset($data['compoff_end_type']) ? intval($data['compoff_end_type']) : '';

        $datediff = strtotime($endDate) - strtotime($startDate);

        $compoffDays = round($datediff / (60 * 60 * 24));
        $compoffDays = $compoffDays + 1;

        if ($startType != 0) {
            $compoffDays = $compoffDays - 0.5;
        }
        if ($endType != 0) {
            $compoffDays = $compoffDays - 0.5;
        }
        if ($datediff == 0 && ($startType != 0 || $endType != 0)) {
            $compoffDays = abs($compoffDays - 0.5);
        }

        $returnData['compoff_days'] = $compoffDays;

        return $returnData;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompOff  $compoff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        try{
            $compoffId = $request->input('compoffId');
            $compoffEntry = CompOff::find($compoffId);
            $compoffEntry->delete();
            return Response::json(array('message'=>'Comp-off Entry Deleted Successfully','status'=>'success'));
        }catch(Exception $e){
            return Response::json(array('message'=>'Comp-off Entry Deleted Successfully','status'=>'error'));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function teamCompoffRequests(Request $request) {
        if (Auth::check()) {
            $page       = $request->get('page');
            $company     = new Company();
            $companyList     = $company->getCompanyName();
            $start_date = date("Y-m-d", strtotime($request->search_compoff_team_start_date) );
            $end_date = date("Y-m-d", strtotime($request->search_compoff_team_end_date) );
            if ($request->isMethod('post')) {
                $compoffStatusSelected = $request->compoff_status;
                $userSelected = $request->user;
                $compStatus = $request->ddlCompOffStatus;
                $userIdArray = $compoffData = $userNameArray = array();
                $authUser = Auth::user();
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;



                $user = new User();
                // if (Auth::check() && (Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug'))) || Auth::user()->designation_id == 16) {
                    $myTeamList = $user->getMyTeamList($loggedInUserID);
                // } 
                if (Auth::check() && (Auth::user()->hasRole(config('constant.hr_slug'))) ||
                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                        Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                        $company_id = '';
                        if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
                            $company_id = $authUser->company_id;
                        }else{
                            if ($request->has('search_submit') && $request->search_submit != '') {
                                if ($request->has('search_by_emp_compoff_company') && $request->search_by_emp_compoff_company != '') {
                                    $company_id = $request->search_by_emp_compoff_company;
                                }
                            }
                        }
                        $myTeamList = $user->getUserNameByCompany($company_id);
                   if(isset($compStatus) && !empty($compStatus)){
                        $compoffStatusSelected = $compStatus;
                    }
                }
                if (!empty($myTeamList)) {
                    foreach ($myTeamList as $userDetail) {
                        $userIdArray[] = $userDetail['id'];
                        if ($userDetail['id'] != 1)
                            $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
                
                $allUserList = $user->getUserNameByCompany($authUser->company_id);
                if (!empty($allUserList)) {
                    foreach ($allUserList as $userDetail) {
                        if ($userDetail['id'] != 1)
                            $allUserListArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
             
                if (!empty($userIdArray)) {
                    $compoff = new CompOff();
                    // if (Auth::check() && (Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug'))) || Auth::user()->designation_id == 16) {
                        $compoffData = $compoff->getCompOffTeamListing($userIdArray, $compoffStatusSelected,$request);
                    // } 
                    if (Auth::check() && (Auth::user()->hasRole(config('constant.hr_slug')))) {
                        $userIdArray2[$userSelected] = $userSelected;
                        $compoffData = $compoff->getCompOffTeamListing($userIdArray2, $compoffStatusSelected,$request);
                    } elseif(Auth::user()->hasRole(config('constant.admin_slug'))){
                        $userIdArray2[$userSelected] = $userSelected;
                        $compoffData = $compoff->getCompOffTeamListing($userIdArray2, $compoffStatusSelected,$request);
                    }
                }
            } else {
                $compoffStatusSelected = '';
                $userSelected = '0';
                $userIdArray = $userNameArray = $compoffData = array();
                $authUser = Auth::user();
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;

                $user = new User();
                // if (Auth::check() && ((Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))) || Auth::user()->designation_id == 16) {
                    $myTeamList = $user->getMyTeamList($loggedInUserID);
                // } 
                if ((Auth::user()->hasRole(config('constant.hr_slug'))) ||
                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                        Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                    $company_id = '';
                    if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
                        $company_id = $authUser->company_id;
                    }else{
                            if ($request->has('search_submit') && $request->search_submit != '') {
                                if ($request->has('search_by_emp_compoff_company') && $request->search_by_emp_compoff_company != '') {
                                    $company_id = $request->search_by_emp_compoff_company;
                                }
                            }
                        }
                    $myTeamList = $user->getUserNameByCompany($company_id);
                    $compoffStatusSelected = '';
                }

                $userNameArray = array();
                if (!empty($myTeamList)) {
                    foreach ($myTeamList as $userDetail) {
                        $userIdArray[] = $userDetail['id'];
                        if ($userDetail['id'] != 1)
                            $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
                if (!empty($userIdArray)) {
                    $compoff = new CompOff();
                    $compoffData = $compoff->getCompOffTeamListing($userIdArray, $compoffStatusSelected,$request);
                }
            }
            $allUserList = $user->getUserNameByCompany($authUser->company_id);
            if (!empty($allUserList)) {
                foreach ($allUserList as $userDetail) {
                    if ($userDetail['id'] != 1)
                        $allUserListArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                }
            }
            // all compoff status 
            $compoffStatus = array(
                array('id'=>1,'statusName'=>'Approved'),
                array('id'=>2,'statusName'=>'Pending'),
                array('id'=>3,'statusName'=>'Rejected')
            );
           
            return view('pms.compoffs.team_compoff_requests', compact('allUserListArray','compoffData','page','companyList', 'loggedInUserID', 
                    'compoffStatusSelected', 'userNameArray', 'userSelected','compoffStatus','start_date','end_date','request'));
        }else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompOff  $compoff
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
      
        $authUser = Auth::user();
        $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;

        $compoff = new CompOff();
        $compoffDetails = $compoff->getCompOffDetails($id);
        //var_dump($compoffDetails);exit;
        if (empty($compoffDetails)) {
            Alert::error('This compoff has been removed');
            return Redirect::to('teamcompoffs');
        }

        if (Auth::check() && (( 
                $compoffDetails['approver_id'] == $loggedInUserID) || 
                Auth::user()->hasRole(config('constant.project_manager_slug')))||
                Auth::user()->hasRole(config('constant.admin_slug')))  {

            $user = new User();
            $reportingToList = $user->getReportingToList();

            return view('pms.compoffs.edit', compact('compoffDetails', 'reportingToList'));
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompOff  $compoff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (Auth::check() && Auth::user()->hasPermission('tasks.edit')) {

            $postData = $request->input();

            $validationRule['compoff_status'] = 'required|integer';
            $validationRule['approver_comment'] = 'required|string|min:6';

            $validator = Validator::make(Input::all(), $validationRule);

            // if validation fails
            if ($validator->fails()) {
                Alert::error('Oops, Form has some error!');
                return Redirect::to('compoffs/approve/' . $id)
                                ->withErrors($validator)
                                ->withInput();
            } else {
                // store

                $compoff = CompOff::find($id);
                $compoff->approver_comment = Input::get('approver_comment');
                $compoff->compoff_status = Input::get('compoff_status');
                $compoff->save();

                $compoff = new CompOff();
                $compoffDetails = $compoff->getCompOffDetails($id);

                $compoffStatusArray = config('constant.compoff_status');

                if (isset($compoffDetails['compoff_status']) && $compoffDetails['compoff_status'] != 2) {
                    $userObj = new User();
                    $company_id = $userObj->getUserCompanyById($compoffDetails['user_id'])[0];
                    $company     = new Company();
                    $userCompanyMailData  = $company->getCompanyViseMailData($company_id['company_id'])[0];  
                    
                    $data = array();
                    $data['id'] = $id;
                    $data['reporting_name'] = $compoffDetails['approver']['first_name'] . ' ' . $compoffDetails['approver']['last_name'];
                    $data['employee_name'] = $compoffDetails['user']['first_name'] . ' ' . $compoffDetails['user']['last_name'];
                    $data['compoff_start_date'] = date('jS F, Y', strtotime($compoffDetails['compoff_start_date']));
                    $data['compoff_end_date'] = date('jS F, Y', strtotime($compoffDetails['compoff_end_date']));
                    $data['compoff_status'] = $compoffStatusArray[$compoffDetails['compoff_status']];
                    $data['regards'] = $userCompanyMailData['company_name'];
                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name'];
                    
                    $to = $compoffDetails['user']['email'];
                    $template = 'emails.compoff_approve';
                    $subject = 'Your Comp-off Request has been ' . $data['compoff_status'];
                    MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []);  
                
                    
                }
                // redirect
                Alert::success('Comp-off Request Successfully Updated!');
                return Redirect::to('teamcompoffs');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function editMarkedAs(Request $request, $id) {
        $authUser = Auth::user();
        $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;

        $compoff = new CompOff();
        $compoffDetails = $compoff->getCompOffDetails($id);

        if (empty($compoffDetails)) {
            Alert::error('This compoff has been removed');
            return Redirect::to('teamcompoffs');
        }

        if (Auth::check() && (Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))) {
            return view('pms.compoffs.edit_marked_as', compact('compoffDetails'));
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function updateMarkedAs(Request $request, $id) {

        if (Auth::check() && Auth::user()->hasRole(config('constant.hr_slug')) || 
                Auth::user()->hasRole(config('constant.admin_slug'))) {
            $postData = $compoffData = $request->input();

            $validationRule['compoff_marked_as'] = 'required|integer';
            if ($postData['compoff_marked_as'] == '1') {
                //$validationRule['compoff_encash_month_year'] = 'required|date_format:"m-Y"';
            }

            $validator = Validator::make(Input::all(), $validationRule);
            $data = array();
            // if validation fails
            if ($validator->fails()) {
                Alert::error('Oops, Form has some error!');
                return Redirect::to('compoffs/markedas/' . $id)
                                ->withErrors($validator)
                                ->withInput()
                                ->with(['data' => $postData['compoff_marked_as']]);
            } else {
                // store 
                $compoff = CompOff::find($id);
         

                $compoff->compoff_marked_as = $newMarkedAs = Input::get('compoff_marked_as');
                $user = new User();
                $update_compoff = 0;
                if($compoff->compoff_status != 1)
                {
                    Alert::success('Compoff Status Must be Approved!');
                    return Redirect::to('compoffs/markedas/' . $id);
                }
                 $leave_used = array();
                 $leave_used1 = array();
                 $leaveDays = 0;
                if($compoff->compoff_marked_as == 2){
                    $userDetails = $user->getUserDetails($compoff->user_id);
                    $firstDateOfMonth = date('Y-m-d',strtotime(date('Y-m-01')));
                    $compoffStartDate = $compoff->compoff_start_date;
                    if($userDetails['used_leaves'] > $userDetails['available_leaves'])
                    {
                        $checkExtraLeave = Leave::where('user_id', $compoff->user_id)->whereBetween('leave_start_date', [$firstDateOfMonth, $compoffStartDate])->first();
                        if(!empty($checkExtraLeave))
                        {
                            $leaveDays = $checkExtraLeave->leave_days;
                        }

                    }
                    $compoff_days = $compoff->compoff_days;
                    $days = $compoff_days - $leaveDays;
                    if($days > 0)
                    {
                        for($i = 0 ; $i < $days; $i++)
                        {
                             $leave_used[$i]= 0;
                        }
                        for($i = 0 ; $i < $leaveDays; $i++)
                        {
                             $leave_used1[$i]= 1;
                        }
                        $final_leave_used = array_merge($leave_used,$leave_used1);
                    }
                    else{
                        if($days == 0){
                            for($i = 0 ; $i < $leaveDays; $i++)
                            {
                                 $final_leave_used[$i]= 1;
                            }   
                        }
                        else{
                            for($i = 0 ; $i < $compoff_days; $i++)
                            {
                                 $final_leave_used[$i]= 1;
                            }  
                        }
                    }
                   $compoff->leave_used = implode(",", $final_leave_used);
                } 
                if ($newMarkedAs == 1) {
                    $compoff->compoff_encash_month_year = '';
                } else {
                    $compoff->compoff_encash_month_year = '';
                }

                $compoff->save();
                Alert::success('Marked As Comp-off Request Successfully Updated!');
                return Redirect::to('teamcompoffs');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    
    public function updateCheckedCompoffAjax(Request $request) {
        $action = $request['action'];
        
        if($action == 0){
            $compoff = new Compoff();
            $compoff->checkedCompoffDelete($request['compoffIdArray']);
        }else{
            $compoff = new Compoff();
            $compoff->checkedCompoffUpdate($request['compoffIdArray'],$action);
        }       
    }
}
