<?php

namespace App\Http\Controllers;

use App\TaskEntry;
use App\Task;
use App\User;
use Excel;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Exception;
use Alert;
use Response;

class TaskEntryController extends Controller
{ 
    public function viewTaskEntry(Request $request,$taskId)
    {
        if(Auth::check() && Auth::user()->hasPermission('task.entry.listing')){
            $taskEntry = TaskEntry::where('task_id',$taskId);

            if($request->has('search_submit') && $request->search_submit != '') {
                if($request->has('search_by_log_from_date') && $request->search_by_log_from_date != '' && $request->has('search_by_log_to_date') && $request->search_by_log_to_date != '') {
                        $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                        $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                        $taskEntry->whereBetween('log_date',array($fromDate,$toDate));
                
                }else{
                    if($request->has('search_by_log_from_date') && $request->search_by_log_from_date != '') {
                        $fromDate = date('Y-m-d',strtotime($request->search_by_log_from_date));
                        $taskEntry->where('log_date', 'like', '%' . $fromDate . '%');
                    }
                    if($request->has('search_by_log_to_date') && $request->search_by_log_to_date != '') {
                        $toDate = date('Y-m-d',strtotime($request->search_by_log_to_date));
                        $taskEntry->where('log_date', 'like', '%' . $toDate . '%');
                    }
                }
            }

            $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            
            $taskEntry = $taskEntry->sortable()->orderBy('id', 'desc')->paginate($page_limit);

            return view('pms.task_entry.index')->with('taskEntry',$taskEntry)->with('taskId',$taskId)->with('request',$request);
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function deleteTaskEntry(Request $request)
    {
        try{
            $taskEntryId = $request->input('taskEntryId');
            $taskEntry = TaskEntry::find($taskEntryId);
            $taskEntry->delete();

            return Response::json(array('message'=>'Task Entry Deleted Successfully','status'=>'success'));
        }catch(Exception $e){
            // $e->getMessage()
            return Response::json(array('message'=>'Task Entry Not Deleted Successfully','status'=>'error'));
        }
    }

    public function editTaskEntry(Request $request)
    {
        $taskEntryId = $request->input('taskEntryId');
        $editTaskEntry = TaskEntry::with('task')->find($taskEntryId);
        return view('pms.task.modal.edit_task_time_entry',compact('editTaskEntry'));
    }

    public function updateTaskEntry(Request $request)
    {
        $taskId = $request->input('editTaskId');
        $editTaskEntryId = $request->input('editTaskEntryId');

        try{
            $obj = TaskEntry::find($editTaskEntryId);

            if(!$obj){
                Alert::error('No Record Found!');
                return Redirect::to('view/task_entry/'.$taskId);
            }


            $obj->log_date        = date('Y-m-d',strtotime($request->input('editTaskDate')));
            $obj->log_hours       = $request->input('editWorkTimeHours').'.'.$request->input('editWorkTimeMinutes');
            $obj->log_description = $request->input('editTaskDescription');
            $obj->save();
            

            Alert::success('Task Entry Successfully Updated');
            return Redirect::to('view/task_entry/'.$taskId);
        }catch(Exception $e){
            // $e->getMessage()
            Alert::error('Task Entry Not Successfully Updated!');
            return Redirect::to('view/task_entry/'.$taskId);
        }
    }

    public function deleteMultipleTaskEntry(Request $request)
    {
        try{
            $taskEntryIdsArray = $request->input('taskEntryIdsArray');

            foreach($taskEntryIdsArray as $value){
                $taskEntry = TaskEntry::find($value);
                if($taskEntry){
                    $taskEntry->delete();
                }
            }

            return Response::json(array('message'=>'Task Entry Deleted Successfully','status'=>'success'));
        }catch(Exception $e){
            // $e->getMessage()
            return Response::json(array('message'=>'Task Entry Not Deleted Successfully','status'=>'error'));
        }
    }
}
