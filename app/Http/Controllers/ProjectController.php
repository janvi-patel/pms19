<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use App\Cr;
use Mail;
use App\ProjectType;
use App\ProjectUser;
use App\Designation;
use App\Company;
use App\ProjectCostInfo;
use App\ProjectTeam;
use App\CrCostInfo;
use App\Task;
use Illuminate\Http\Request;
use App\MiscBeanchTask;
use App\MiscBeanchTaskEntry;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Response;
use Exception;
use App\ProjectTasks;
use DB;
use Alert;
use App\ProjectEntry;
use App\Holiday;
use App\Jobs\MailJob;
use App\Setting;
use App\Helpers\CommonHelper;
use App\ProjectNote;
use App\Http\Controllers\ProjectTaskController;

class ProjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getProjectTypes()
    {
        return ProjectType::select('id', 'project_type_name')->orderBy('project_type_name')->get();
    }

    public function index(Request $request)
    {
        if(Auth::check() && Auth::user()->hasPermission('projects.listing')){
            $data              = [];
            $companyId         = Auth::user()->company_id;
            $allowPIS          = Auth::user()->allow_pis;
            $loggedInCompanyId = Auth::user()->company_id;
            $page_limit        = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $projectTypes = $this->getProjectTypes();
            $companies = Company::all();


            if (Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                if ($request->has('search_$assignedUserIdssubmit') && $request->company == 0) {
                    $companyId = 0;
                    $request->session()->put('company_id', $companyId);

                    $dataQuery = Project::leftjoin('clients', 'projects.client_id', '=', 'clients.id')->
                            leftjoin('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                            leftjoin('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                            leftjoin('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                            leftjoin('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                            leftjoin('project_types', 'projects.project_type', '=', 'project_types.id')->
                            leftjoin('currencies as pcurrency', 'projects.currency_id', '=', 'pcurrency.id')->
                            leftjoin('project_entries as pe', function($leftJoin)
                            {
                                $leftJoin->on('pe.project_id', '=', 'projects.id')
                                ->whereNull('pe.deleted_at');
                            });
                } elseif ($request->company != 0) {
                    $companyId = $request->company;
                    $request->session()->put('company_id', $companyId);
                    $other_projects = ProjectCostInfo::where('company_id', $request->company)
                            ->pluck('project_id');

                    $dataQuery = Project:: leftjoin('clients', 'projects.client_id', '=', 'clients.id')->
                            leftjoin('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                            leftjoin('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                            leftjoin('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                            leftjoin('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                            leftjoin('project_types', 'projects.project_type', '=', 'project_types.id')
                            ->leftJoin('project_cost_info', function($leftJoin)use($request) {
                                $leftJoin->on('projects.id', '=', 'project_cost_info.project_id');
                                $leftJoin->where('project_cost_info.company_id', $request->company);
                            })->
                            leftjoin('currencies as pcurrency', 'project_cost_info.currency_id', '=', 'pcurrency.id')->
                            leftjoin('project_entries as pe', function($leftJoin)
                            {
                                $leftJoin->on('pe.project_id', '=', 'projects.id')
                                ->whereNull('pe.deleted_at');
                            })
                            ->where(function($q) use($request, $other_projects) {
                                $q->where('projects.company_id', $request->company)
                                ->orWhere('projects.assigned_to', $request->company)
                                ->orWhereIn('projects.id', $other_projects);
                            });
                } else {
                    $companyId = $request->company;
                    $dataQuery = Project::leftjoin('clients', 'projects.client_id', '=', 'clients.id')->
                            leftjoin('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                            leftjoin('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                            leftjoin('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                            leftjoin('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                            leftjoin('project_types', 'projects.project_type', '=', 'project_types.id')->
                            leftjoin('currencies as pcurrency', 'projects.currency_id', '=', 'pcurrency.id')->
                            leftjoin('project_entries as pe', function($leftJoin)
                            {
                                $leftJoin->on('pe.project_id', '=', 'projects.id')
                                ->whereNull('pe.deleted_at');
                            });
                }
                $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                        ->select('cr.project_id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                        ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                        ->whereNull('cr.deleted_at')
                        ->groupBy('cr.project_id')
                        ->pluck('total_cr_hours','cr.project_id')->toArray();
            } else {
                if (Auth::user()->hasRole(config('constant.admin'))) {
                    $request->session()->put('company_id', $companyId);
                    $other_projects = ProjectCostInfo::where('company_id', $companyId)
                    ->pluck('project_id');

                    $dataQuery = Project::leftjoin('clients', 'projects.client_id', '=', 'clients.id')->
                        leftjoin('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                        leftjoin('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                        leftjoin('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                        leftjoin('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                        leftjoin('project_types', 'projects.project_type', '=', 'project_types.id')
                        ->leftJoin('project_cost_info', function($leftJoin)use($request, $companyId) {
                            $leftJoin->on('projects.id', '=', 'project_cost_info.project_id');
                            $leftJoin->where('project_cost_info.company_id', $companyId);
                        })->
                        leftjoin('currencies as pcurrency', 'projects.currency_id', '=', 'pcurrency.id')->
                        leftjoin('project_entries as pe', function($leftJoin)
                        {
                            $leftJoin->on('pe.project_id', '=', 'projects.id')
                            ->whereNull('pe.deleted_at');
                        })
                        ->where(function($q) use($request, $other_projects, $companyId) {
                            $q->Where('projects.assigned_to', $companyId);
                        });

            $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                        ->select('cr.project_id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                        ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                        ->whereNull('cr.deleted_at')
                        ->where(function($q) use($request, $other_projects, $companyId) {
                            $q->where('projects.company_id', $companyId)
                            ->orWhere('projects.assigned_to', $companyId)
                            ->orWhereIn('projects.id', $other_projects);
                        })
                        ->groupBy('cr.project_id')
                        ->pluck('total_cr_hours','cr.project_id')->toArray();


                }else{
                    $myTeam = getTeamUserId(auth()->user()->id);
                    $myTeam[] = auth()->user()->id;
                    $own_project =  ProjectTeam::where('company_id',Auth::user()->company_id)->whereIn('user_id',$myTeam)->pluck('project_id');
                    $dataQuery = Project::leftJoin('clients', 'projects.client_id', '=', 'clients.id')->
                            leftJoin('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                            leftJoin('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                            leftJoin('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                            leftJoin('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                            leftJoin('project_types', 'projects.project_type', '=', 'project_types.id')->
                            leftjoin('project_entries as pe', function($leftJoin)
                            {
                                $leftJoin->on('pe.project_id', '=', 'projects.id')
                                ->whereNull('pe.deleted_at');
                            })
                            ->where(function($q) use($own_project) {
                                $q->where('projects.created_by', Auth::user()->id)
                                ->orWhere('projects.team_leader',Auth::user()->id)
                                ->orWhere('projects.project_manager',Auth::user()->id)
                                ->orWhereIn('projects.id',$own_project)
                                ->orwhere(function($q){
                                    $q->orwhere('projects.assigned_to',Auth::user()->company_id)
                                    ->where(function($q){
                                            $q->where('projects.project_name','Miscellaneous Tasks')
                                            ->orwhere('projects.project_name','Beanch');
                                        });
                                });
                            });

                    $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                        ->select('cr.project_id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                        ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                        ->whereNull('cr.deleted_at')
                        // ->where(function($q){
                        //         $q->where('projects.created_by', Auth::user()->id)
                        //         ->orWhere('projects.team_leader',Auth::user()->id)
                        //         ->orWhere('projects.project_manager',Auth::user()->id);
                        // })
                        ->groupBy('cr.project_id')
                        ->pluck('total_cr_hours','cr.project_id')->toArray();
                }
            }

            $dataQuery->whereNull('projects.deleted_at')->groupBy('projects.id');
            $dataQuery->select(
                'projects.id',
                'clients.id as client_id',
                'accountmanager.id as accountmanager_id',
                'projectmanager.id as projectmanager_id',
                'projects.project_manager',
                'projects.assigned_to',
                'projects.project_status',
                'projects.project_start_date',
                'projects.company_id',
                'projects.project_cost',
                'projects.estimated_hours',
                'project_types.project_type_name',
                'projects.project_name as project_name',
                'projects.project_type',
                'projects.pms_hours',
                DB::raw('CONCAT(clients.first_name, " ", clients.last_name) as client_name'),
                DB::raw('CONCAT(projectmanager.first_name, " ", projectmanager.last_name) as project_manager_name'),
                DB::raw('CONCAT(accountmanager.first_name, " ", accountmanager.last_name) as account_manager_name'),
                'pcompany.company_name',
                'pcompany.id as cid',
                'acompany.company_name as assigned_company',
                'pcompany.id as company_id',
                DB::raw("SUM(pe.log_hours) As total_logged_hours")
            );

            $projectData = clone $dataQuery;
            if(!$request->has('search_project_status')){
                $dataQuery->where('project_status',1);
            }
            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                    $dataQuery->where('projects.id',$request->search_by_project_name);
                }
                if ($request->has('search_by_client') && $request->search_by_client != '') {
                    $search_by_client = $request->search_by_client;
                    $dataQuery->where('clients.id', $search_by_client );
                }
                if ($request->has('search_by_account') && $request->search_by_account != '') {
                    $search_by_account = $request->search_by_account;

                    $dataQuery->where('accountmanager.id', $search_by_account);
                }
                if ($request->has('search_by_project_manager') && $request->search_by_project_manager != '') {
                    $search_by_project_manager = $request->search_by_project_manager;

                    // $dataQuery->where('projectmanager.first_name', 'like', '%' . $search_by_project_manager . '%');
                    $dataQuery->where('projectmanager.id', $search_by_project_manager );
                }
                if ($request->has('search_by_company') && $request->search_by_company != '') {
                    $search_by_company = $request->search_by_company;

                    $dataQuery->where('pcompany.company_name', 'like', '%' . $search_by_company . '%');
                }
                if ($request->has('search_by_assign_company') && $request->search_by_assign_company != '') {
                    $search_by_assign_company = $request->search_by_assign_company;

                    $dataQuery->where('acompany.company_name', 'like', '%' . $search_by_assign_company . '%');
                }
                if (($request->has('search_by_start_date') && $request->search_by_start_date != '') && ($request->search_by_end_date == '')) {
                    $dataQuery->where('projects.project_start_date', '>=', date('Y-m-d',strtotime($request->search_by_start_date)));
                }
                if (($request->has('search_by_end_date') && $request->search_by_end_date != '') && ($request->search_by_start_date == '')) {
                    $dataQuery->where('projects.project_end_date', '<=', date('Y-m-d',strtotime($request->search_by_end_date)));
                }
                if (($request->has('search_by_start_date') && $request->search_by_start_date != '') && ($request->has('search_by_end_date') && $request->search_by_end_date != '')) {
                   $dataQuery->where('projects.project_start_date', '>=', date('Y-m-d',strtotime($request->search_by_start_date)));
                    $dataQuery->where('projects.project_end_date', '<=', date('Y-m-d',strtotime($request->search_by_end_date)));
                }
                if ($request->has('search_by_created_date') && $request->search_by_created_date != '') {
                    $dataQuery->where('projects.created_at', 'like', '%' . $request->search_by_created_date . '%');
                }
                if ($request->has('search_project_status') && $request->search_project_status != '') {
                    if($request->search_project_status != 0){
                        $dataQuery->where('projects.project_status', $request->search_project_status);
                    }
                }
                if ($request->has('search_project_type') && $request->search_project_type != '') {
                    if($request->search_project_type != 0){
                        $dataQuery->where('projects.project_type', $request->search_project_type);
                    }
                }
            }

             if ($request->has('sort') && $request->input('sort') != '') {
                $data = $dataQuery->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $data = $dataQuery->sortable()->orderBy('id', 'desc')->paginate($page_limit);
            }

            $project_name = $client_name = $company_name = $account_manager_name = array();
            $projectData =  $projectData->orderBy('id', 'desc')->get();
            if(!empty($projectData)){
                foreach ($projectData as $dataVal) {
                    $project_data[$dataVal->id] = $dataVal->assigned_to;
                    $project_name[$dataVal->id] = $dataVal->project_name;
                    $client_name[$dataVal->client_id] = $dataVal->client_name;
                    $company_name[$dataVal->company_name] = $dataVal->company_name;
                    $account_manager_name[$dataVal->accountmanager_id] = $dataVal->account_manager_name;
                    $project_manager_name[$dataVal->projectmanager_id] = $dataVal->project_manager_name;
                }
            }
            $projectName = '';
            $i = 1;
            $log_date = date('d-m-Y',strtotime("-".$i." days"));
            while(1){
                $isHoliday = $this->checkEntryDate($log_date);
                if($isHoliday == false){
                    break;
                }else{
                    $log_date = date('d-m-Y',strtotime("-1 days",strtotime($log_date)));
                }
                $i++;
            }
            return view('pms.project.index', compact('log_date','data','projectCrHours','project_name','client_name','project_manager_name',
                            'account_manager_name','request', 'allowPIS', 'loggedInCompanyId', 'companies','companyId','projectName','projectTypes'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function getCrInfo($project_id){
        $cr_details = CR::select('cr.id','cr.estimated_hours','cr.created_at','cr.pms_hours',DB::raw('DATE(created_at) as created_at'),'cr.title')
                ->where('project_id',$project_id)
                ->whereNull('deleted_at')
                ->sortable()->orderBy('cr.id', 'desc')->get();
        if($cr_details){
            $cr_details->toArray();
        }


        $project_details = Project::select('projects.estimated_hours',DB::raw('DATE(project_start_date) as start_date')
                                ,'projects.project_name','projects.project_start_date','projects.project_end_date','projects.pms_hours')
                        ->where('id',$project_id)->first();

        echo view('pms.project.cr_info_modal',compact('project_details','cr_details'));
    }

     public function viewCrInfo($project_id){
        $cr_details = CR::select('cr.id','cr.estimated_hours','cr.created_at','cr.pms_hours',DB::raw('DATE(created_at) as created_at'),'cr.title')
                ->where('project_id',$project_id)
                ->whereNull('deleted_at')
                ->orderBy('cr.id', 'desc')->get();
        if($cr_details){
            $cr_details->toArray();
        }

        $project_details = Project::select('projects.estimated_hours',DB::raw('DATE(project_start_date) as start_date')
                                ,'projects.project_name','projects.project_start_date','projects.project_end_date','projects.pms_hours')
                        ->where('id',$project_id)->first();


        echo view('pms.project.view_cr_info_modal',compact('project_details','cr_details'));
    }


    public function assignToTeam(Request $request)
    {
        $assignedUserIds = $teamLeaders = array();
        $projectId       = $request->input('projectId');
        $companyId       = $request->input('assignedToCompanyId');
        $projectManager  = '';
        $teamLeaderId    = null;
        $assignedUserIds = ProjectTeam::where('project_id',$projectId)->pluck('user_id')->toArray();

        if($companyId == config('constant.company.tridhya')){
            $id[] = config('constant.super_admin_id');
        }else{
            $id[] = Auth::user()->id;
        }

        $project = Project::find($projectId);

        if($project){
            $projectManager = $this->getUserName($project->project_manager);

            $id[] = $project->project_manager;
            if(!is_null($project->team_leader)){
                $teamLeaderId = $project->team_leader;
                $id[]         = $project->team_leader;
            }
        }
        $roleIds = array(
                            config('constant.created_role_id.admin_role_id'),
                            config('constant.created_role_id.super_admin_role_id'),
                            config('constant.created_role_id.account_manager_role_id'),
                            config('constant.created_role_id.hr_role_id')
                        );

        $user = User::join('role_user','users.id','=','role_user.user_id')
                        ->leftjoin('companies','users.company_id', '=', 'companies.id')
                        ->where('users.company_id',$companyId)
                        ->whereNotIn('role_user.role_id',$roleIds)
                        ->where('users.status',1)
                        ->whereNotIn('users.id',$id)
                        ->orderBy('users.first_name', 'ASC')
                        ->get(['users.id','companies.emp_short_name','users.employee_id','users.first_name','users.last_name']);

        if($project && is_null($project->team_leader)){
            $teamLeaders = User::join('role_user','users.id','=','role_user.user_id')
                        ->where('users.company_id',$companyId)
                        ->whereNotIn('role_user.role_id',$roleIds)
                        ->where('users.employee_status',1)
                        ->where('users.status',1)
                        ->where('users.designation_id',5)
                        ->whereNotIn('users.id',$id)
                        ->get(['users.id','users.first_name','users.last_name']);
        }elseif($project && !is_null($project->team_leader)){
            $teamLeaders = User::join('role_user','users.id','=','role_user.user_id')
                        ->where('users.id',$project->team_leader)
                        ->get(['users.id','users.first_name','users.last_name']);
        }

        return view('pms.project.assign_to_team')->with('projectManager',$projectManager)->with('teamLeaders',$teamLeaders)->with('users',$user)->with('projectId',$projectId)->with('companyId',$companyId)->with('assignedUserIds',$assignedUserIds)->with('teamLeaderId',$teamLeaderId);
    }
    public function removeTeamMember(Request $request) {
        $userId = $request->input('unSelectUserId');
        $projectId = $request->input('projectId');
        $projectEntry = ProjectEntry::where('user_id',$userId)->where('project_id',$projectId)->first();
        return $projectEntry;
    }
    public function saveTeam(Request $request)
    {
        try{
            if(Auth::check() && Auth::user()->hasPermission('create.project.team')){
                $team       = $request->input('teamAssign');
                $teamLeader = $request->input('teamLeader');
                $projectId  = $request->input('assginToTeamProjectId');
                $companyId  = $request->input('assginToTeamCompanyId');

                $alreadyAssigned= ProjectTeam::where('project_id',$projectId)->select('user_id')->get()->toArray();
                $alreadyAssignedArray = array();
                foreach ($alreadyAssigned  as $value) {
                    array_push($alreadyAssignedArray,$value['user_id']);
                }

                ProjectTeam::where('project_id',$projectId)->delete();

                $projectData = Project::select('project_name','project_start_date','project_end_date','team_leader');
                $projectData->where('id',$projectId)->first();

                $tlData = User::select('email','first_name','last_name');
                $tlData->where('id',$teamLeader)->first();


                if($projectData && $tlData ){

                    $tlData = $tlData->get()->toArray()[0];
                    $data['team_lead_name']=$tlData['first_name'].' '.$tlData['last_name'];

                    $projectData=$projectData->get()->toArray()[0];
                    $data['project_name']=$projectData['project_name'];
                    $data['project_start_date']=$projectData['project_start_date'];
                    $data['project_end_date']=$projectData['project_end_date'];
                }

                if(isset($teamLeader)){
                    $project = Project::find($projectId);
                    if($project){
                        $project->team_leader = $teamLeader;
                        $project->save();

                        if($projectData && $tlData &&  $projectData['team_leader'] != $teamLeader){
                            $company     = new Company();
                            $userCompanyMailData  = $company->getCompanyViseMailData($companyId)[0];
                            $from['email'] = $userCompanyMailData['from_address'];
                            $from['name']  = $userCompanyMailData['company_name'];

                            $to = $tlData['email'];
                            $template = 'emails.project_assigned_tl';
                            $subject = 'Project assigned';
                            $data['first_name'] = $tlData['first_name'];
                            $data['regards'] = $userCompanyMailData['company_name'];
                            // MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []);

                        }
                    }
                }


                if(isset($team) && count($team) > 0){
                    foreach($team as $id){
                        $obj             = new ProjectTeam();
                        $obj->project_id = $projectId;
                        $obj->company_id = $companyId;
                        $obj->user_id    = $id;
                        $obj->save();

                        if($projectData && $tlData && !in_array($id,$alreadyAssignedArray)){
                            $companyEmailData =  CommonHelper::getCompanyDetails(Auth::user()->company_id);
                            $userData = User::select('email','first_name');
                            $userData->where('id','=',$id)->first();
                            $userData=$userData->get()->toArray()[0];
                            $to = $userData['email'];
                            $template = 'emails.project_assigned_developer';
                            $subject = 'Project assigned';
                            $from['email'] = $companyEmailData->from_address;
                            $from['name'] = $companyEmailData->company_name;
                            $data['first_name'] = $userData['first_name'];
                            $data['regards'] = $companyEmailData->company_name;
                            // MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []);

                        }
                    }
                }

                Alert::success('Project Team Created Successfully.');
                return Redirect::back();
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }catch(Exception $e){
            //$e->getMessage();
            Alert::error('Project Team Not Created Successfully.');
            return Redirect::back();
        }

    }

    public function getUserName($id)
    {
        $user = User::find($id);
        return ($user)?$user->first_name.' '.$user->last_name:'';
    }

    public function myProjects(Request $request)
    {   $projectName = '';
        if(Auth::check() && Auth::user()->hasPermission('my.project.listing')){
            $data        = [];
            $companyId   = Auth::user()->company_id;
            $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $projectTeam = ProjectTeam::where('user_id',Auth::user()->id)->pluck('project_id');


            $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                        ->select('cr.project_id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                        ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                        ->whereNull('cr.deleted_at')
                        ->where(function($q) use ($projectTeam) {
                                $q->where('projects.team_leader',Auth::user()->id)
                                ->orWhereIn('projects.id',$projectTeam);
                            })
                        ->groupBy('cr.project_id')
                        ->pluck('total_cr_hours','cr.project_id')->toArray();


            $dataQuery = $this->getMyProjectName($request);
            $projectData = clone $dataQuery;

            if(!$request->has('search_project_status')){
                $dataQuery->where('project_status',1);
            }

            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                    $dataQuery->where('projects.id', $request->search_by_project_name);
                }
                if ($request->has('search_by_client') && $request->search_by_client != '') {
                    $search_by_client = $request->search_by_client;
                    $dataQuery->where('clients.id', $search_by_client );
                }

                if (($request->has('search_by_start_date') && $request->search_by_start_date != '') && ($request->search_by_end_date == '')) {
                    $dataQuery->where('projects.project_start_date', '>=', date('Y-m-d',strtotime($request->search_by_start_date)));
                }
                if (($request->has('search_by_end_date') && $request->search_by_end_date != '') && ($request->search_by_start_date == '')) {

                    $dataQuery->where('projects.project_end_date', '<=', date('Y-m-d',strtotime($request->search_by_end_date)));
                }
                if (($request->has('search_by_start_date') && $request->search_by_start_date != '') && ($request->has('search_by_end_date') && $request->search_by_end_date != '')) {
                    $dataQuery->where('projects.project_start_date', '>=', date('Y-m-d',strtotime($request->search_by_start_date)));
                    $dataQuery->where('projects.project_end_date', '<=', date('Y-m-d',strtotime($request->search_by_end_date)));
                }
                if ($request->has('search_by_created_date') && $request->search_by_created_date != '') {
                    $dataQuery->where('projects.created_at', 'like', '%' . date('Y-m-d',strtotime($request->search_by_created_date)) . '%');
                }
                if ($request->has('search_project_status') && $request->search_project_status != '') {
                    if($request->search_project_status != 0){
                        $dataQuery->where('projects.project_status', $request->search_project_status);
                    }
                }
            }

            if ($request->has('sort') && $request->input('sort') != '') {
                $data = $dataQuery->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            }else{
                $data = $dataQuery->sortable()->orderBy('id', 'desc')->paginate($page_limit);
            }

            $i = 1;
            $date = date('d-m-Y',strtotime("-".$i." days"));
            while(1){
                $isHoliday = $this->checkEntryDate($date);
                if($isHoliday == false){
                    break;
                }else{
                    $date = date('d-m-Y',strtotime("-1 days",strtotime($date)));
                }
                $i++;
            }
            $project_name = array();
            $client_name = array();
            $projectData =  $projectData->orderBy('id', 'desc')->get();
            if(!empty($projectData)){
                foreach ($projectData as $dataVal) {
                    $project_name[$dataVal->id] = $dataVal->project_name;
                    $client_name[$dataVal->client_id] = $dataVal->client_name;
                }
            }
            return view('pms.myproject.index')->with('log_date',$date)->with('project_name',$project_name)->with('client_name',$client_name)->with('data',$data)->with('request',$request)->with('projectCrHours',$projectCrHours)->with('projectName',$projectName);
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function getMyProjectName(Request $request) {
        $companyId   = Auth::user()->company_id;
        $projectTeam = ProjectTeam::where('user_id',Auth::user()->id)->pluck('project_id');
        $dataQuery = Project::
                            leftjoin('clients', 'projects.client_id', '=', 'clients.id')->
                            leftjoin('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                            leftjoin('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                            leftjoin('users as teamleader', 'projects.team_leader', '=', 'teamleader.id')->
                            leftjoin('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                            leftjoin('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                            leftjoin('project_types', 'projects.project_type', '=', 'project_types.id')->
                            leftjoin('currencies as pcurrency', 'projects.currency_id', '=', 'pcurrency.id')->
                            leftjoin('project_entries as pe', function($leftJoin)
                            {
                                $leftJoin->on('pe.project_id', '=', 'projects.id')
                                ->whereNull('pe.deleted_at');
                            })
                            ->where(function($q) use ($projectTeam) {
                                $q->where('projects.team_leader',Auth::user()->id)
                                ->orWhereIn('projects.id',$projectTeam);
                            })
                            ->whereNull('projects.deleted_at')->groupBy('projects.id')
                           ->select('projects.id', 'projects.project_manager','projects.assigned_to','projects.project_status',
                                    'projects.project_start_date','projects.company_id', 'projects.project_cost','projects.currency_id as pcurrency_id',
                                    'pcurrency.currency_name as pcurrency',
                                    'projects.estimated_hours',
                                    'projects.pms_hours',
                                    'project_types.project_type_name', 'projects.project_name', 'projects.invoice_status', 'projects.project_type',
                                    'clients.id as client_id',
                                    DB::raw('CONCAT(clients.first_name, " ", clients.last_name) as client_name'),
                                    DB::raw('CONCAT(teamleader.first_name, " ", teamleader.last_name) as teamleader'),
                                    DB::raw('CONCAT(projectmanager.first_name, " ", projectmanager.last_name) as project_manager_name'),
                                    DB::raw('CONCAT(accountmanager.first_name, " ", accountmanager.last_name) as account_manager_name'),
                                    'pcompany.company_name', 'pcompany.id as cid', 'acompany.company_name as assigned_company',
                                    'pcompany.id as company_id',
                                    DB::raw("floor(sum(floor(pe.log_hours)) + sum(pe.log_hours - floor(pe.log_hours)) * 100.0/60) +
                        ((sum(floor(pe.log_hours)) + sum(pe.log_hours - floor(pe.log_hours)) * 100.0/60) % 1) * 60.0/100  as total_logged_hours_bkp"),
                                            DB::raw("sum(pe.log_hours) as total_logged_hours"));
        return $dataQuery;
    }
    public function checkEntryDate($date) {
        $session = session()->all();
        $date = date("Y-m-d", strtotime($date));
        $holiday = new Holiday();
        $holidayList = $holiday->getHolidayDateList($date);
        $isWeekend = $holiday->getWeekendDateList($date);
        // if((date('N', strtotime($date)) >= 6)){
        if(count($isWeekend) > 0){
            return true;
        }else if(sizeof($holidayList) != 0 ){
            return true;
        }
        return false; //Not holiday
    }
    public function getTaskDetails($id)
    {
        return Project::find($id);
    }
    public function getProjectTask($id,$date){
        $date = date('Y-m-d',strtotime($date));
        // DB::enableQueryLog();
        $projectTask = ProjectTasks::where('project_id',$id)
                            ->where('user_id',Auth::user()->id)
                            ->where(function ($query) use ($date){
                                $query->where('task_start_date', '<=', $date)
                                      ->where('task_end_date', '>=', $date);
                            })
                            ->select('task_name','id')
                            ->get();
        // dd(DB::getQueryLog());
        // exit();
        return $projectTask;
    }

    public function newGetProjectTask($id,$taskid,$date){
        $date = date('Y-m-d',strtotime($date));
        $projectTask = ProjectTasks::where('project_id',$id);
        if(isset($taskid) && $taskid != "undefined"){
            $projectTask = $projectTask->where('id',$taskid);
        }
        $projectTask = $projectTask->where('user_id',Auth::user()->id)
            ->where(function ($query) use ($date){
                $query->where('task_start_date', '<=', $date)
                        ->where('task_end_date', '>=', $date);
            })
            ->select('task_name','id')
            ->get();
        return $projectTask;
    }

    public function getProjectDetail(Request $request)
    {
        $projectDetails = $pmtl = $usersId = array();
        $projectId      = $request->projectId;

        $project          = Project::find($projectId);
        $projectStartDate = ($project)?$project->project_start_date:'';
        $projectEndDate   = ($project)?$project->project_end_date:'';
        $projectDetails['projectStartDate'] = $projectStartDate;
        $projectDetails['projectEndDate']   = $projectEndDate;
        return $projectDetails;
    }

    /////////// NOTES FOR PROJECTS ////////////////

    public function viewProjectNotes(Request $request,$id){
        $validProject = new ProjectTaskController();
        if($id == 517 || $id == 516 && $id == 518 && $id == 458){
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }

        $validPro = $validProject->validateAccessData($id);
        if($validPro)
        {
            $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $user               = new User();
            $project_note       = new ProjectNote();
            $pro_usr            = clone $project_note;
            $project_team       = $pro_usr->pluck('user_id');
            $project_team_details = $user->whereIn('id',$project_team)->get();

            $project_note    = $project_note->leftJoin('users as uid','uid.id','project_notes.user_id')
                                ->where('project_notes.added_from','pms')
                                ->where('project_notes.project_id',$id)
                                ->where('project_notes.type',0)->orWhere(function($q) use ($id) {
                                    $q->where('project_notes.user_id',Auth::user()->id)
                                    ->where('project_notes.added_from','pms')
                                    ->where('project_notes.project_id',$id)
                                    ->where('project_notes.type',1);
                                })
                                ->whereNull('project_notes.deleted_at');
            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_user') && $request->search_user != '') {
                    $project_note = $project_note->where('uid.id', $request->search_user);
                }
            }
            $project_note = $project_note->select('project_notes.id as id','project_notes.project_id as project_id','project_notes.type','project_notes.note','project_notes.note_title','project_notes.created_at','uid.first_name','uid.last_name','project_notes.user_id as user_id');
            if ($request->has('sort') && $request->input('sort') != '') {
                $data = $project_note->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $data = $project_note->sortable()->orderBy('id', 'desc')->paginate($page_limit);
            }

            return view('pms.project_notes.project_notes_listing',compact('data','id','project_team_details','request'));
        }
        else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function addProjectNoteModal(Request $request){
        $projectId   = $request->input('projectId');
        if($projectId == 517 || $projectId == 516 && $projectId == 518 && $projectId == 458){
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
        $validProject = new ProjectTaskController();
        $validPro = $validProject->validateAccessData($projectId);
        if($validPro)
        {
            $user = new user();
            $project = Project::where('id',$projectId)->first();
            return view('pms.project_notes.modal.add_note_modal',compact('project'));
        }
        else{
            return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
        }
    }

    public function storeProjectNotes(Request $request) {
        $projectId       = $request->input('addNotesProjectId');
        if($projectId == 517 || $projectId == 516 && $projectId == 518 && $projectId == 458){
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
        $validProject = new ProjectTaskController();
        $validPro = $validProject->validateAccessData($projectId);
        if($validPro)
        {
            try{
                $obj                  = new ProjectNote();
                $obj->note            = $request->input('addNotes');
                $obj->note_title            = $request->input('addNotesTitle');
                $obj->user_id         = Auth::user()->id;
                $obj->project_id      = $request->input('addNotesProjectId');
                $obj->type            = $request->input('noteType') == 1?1:0;
                $obj->save();

                Alert::success('Notes Successfully Created');
                return Redirect::back();
            }catch(Exception $e){
                Alert::error('Notes Not Created');
                return Redirect::back();
            }
        }
        else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function editProjectNoteModal(Request $request)
    {
        $projectNoteId       = $request->input('projectNoteId');
        $noteDetails         = ProjectNote::where('id',$projectNoteId)->first();
        if($noteDetails->user_id == Auth::user()->id){
            $project = Project::where('id',$noteDetails->project_id)->first();
            return view('pms.project_notes.modal.edit_note_modal',compact('noteDetails','project'));
        }
        else{
            return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
        }
    }

    public function updateProjectNote(Request $request) {
        $editNoteId = $request->input('editProjectNoteId');
        $obj   =  ProjectNote::find($editNoteId);
        if($obj->user_id == Auth::user()->id)
        {
            try{
                $obj->note            = $request->input('editNotes');
                $obj->note_title      = $request->input('editNotesTitle');
                $obj->type            = $request->input('editNoteType') == 1?1:0;
                $obj->save();
                Alert::success('Notes Successfully Updated');
                return Redirect::back();
            }catch(Exception $e){
                Alert::error('Notes Not Updated Successfully');
                return Redirect::back();
            }
        }
        else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function deleteNotes(Request $request) {
        $deleteNoteId = $request->input('projectNoteId');
        $obj   =  ProjectNote::find($deleteNoteId);
        if($obj->user_id == Auth::user()->id)
        {
            try{
                $obj->delete();
                return Response::json(array('message'=>'Notes Successfully Deleted','status'=>'success'));
                // return Redirect::back();
            }catch(Exception $e){
                return Response::json(array('message'=>'Notes Not Deleted Successfully','status'=>'error'));
                // return Redirect::back();
            }
        }
        else{
            return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
            // return Redirect::to('dashboard');
        }
    }
}
