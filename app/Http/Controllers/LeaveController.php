<?php

namespace App\Http\Controllers;

use App\Leave;
use App\User;
use Alert;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Response;
use Log;
use App\CompOff;
use App\Jobs\MailJob;
use App\Company;
use App\LeaveDetail;
use App\Attendance;
use App\WorkFromHome;
use App\Http\Controllers\TimeSheetController;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userIdArray = array();
        $page       = $request->get('page');
        $authUser = Auth::user();
        $userName = new User();
        $company_id = '';
        if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
            $company_id = $authUser->company_id;
        }
        $approver_id = Leave::where('user_id','=',$authUser->id)->pluck("approver_id")->toArray();
        $userNameList = User::select('id','first_name','last_name')->whereIn('id',$approver_id)->get();

        $start_date = $request->search_leave_start_date;
        $end_date = $request->search_leave_end_date;
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

        $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
        if($loggedInUserID){
            $userIdArray = array($loggedInUserID);
        }

        $leave = Leave::with(['user', 'approver']);
        if (isset($request->id)) {
            $leaveStatus = $request->id;
        }else{
            $leaveStatus =0;
        }
        if ($userIdArray) {
            if ($leaveStatus != 0) {
                $leave->whereIn('leave_status', [$leaveStatus]);
            }
            $leave->whereIn('user_id', $userIdArray);
        }
        if (!($request->has('search_leave_start_date'))) {
            $start_date = date('d-m-Y',strtotime(date('01-04-Y')));
            if(date('m-d',strtotime($start_date)) > date('m-d'))
            {
                $year = date("Y", strtotime("-1 years"));
                $start_date = date('d-m-Y',strtotime(date('1-04-'.$year)));
            }
            $leave->where('leave_start_date',  '>=',  date("Y-m-d", strtotime($start_date)));
        }
        if (!($request->has('search_leave_end_date'))) {
            $end_date = date('d-m-Y',strtotime(date('31-12-Y')));
            $leave->where('leave_end_date',  '<=', date("Y-m-d", strtotime($end_date)));
        }
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                $leave->where('user_id',  '=', $request->search_by_employee);
            }
            if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                $leave->where('approver_id',  '=', $request->search_by_approver);
            }
            if ($request->has('search_leave_start_date') && $request->search_leave_start_date != '') {
                $leave->where('leave_start_date',  '>=', date("Y-m-d", strtotime($request->search_leave_start_date)));
            }
            if ($request->has('search_leave_end_date') && $request->search_leave_end_date != '') {
                $leave->where('leave_end_date',  '<=', date("Y-m-d", strtotime($request->search_leave_end_date)));
            }
            if ($request->has('search_by_leave_status') && $request->search_by_leave_status != '') {
                $leave->where('leave_status',  '=', $request->search_by_leave_status);
            }
        }
        $leavesData = $leave->sortable()->orderBy('leave_start_date', 'desc')->paginate($page_limit);
        return view('pms.leaves.index', compact('leavesData', 'loggedInUserID','page','userNameList','request','start_date','end_date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if (Auth::check()) {
            $authUser = Auth::user();
            $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
            $user = new User();
            $userDetails = $user->getUserDetails($loggedInUserID);
            $reportingTo = $user->getReportingToListWithOnlyTeamLeader($loggedInUserID);
            return view('pms.leaves.create', compact('userDetails', 'reportingTo'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r($request->all());
        // exit;
        $user = new User();

        if (Auth::check() && Auth::user()->hasPermission('leave.create')) {
            $getUid = $request->user()->id;
            $total_days = 0;
            $postData = $request->input();
            $validationRule['reason'] = 'required|string|min:6';
            $validationRule['leave_start_date'] = 'required|date_format:Y-m-d|before_or_equal:leave_end_date';
            $validationRule['leave_start_date'] = 'required|date_format:Y-m-d|after_or_equal:joinning_date';
            $validationRule['leave_end_date'] = 'required|date_format:Y-m-d|after_or_equal:leave_start_date';


            $validator = Validator::make(Input::all(), $validationRule);

            // if validation fails
            if ($validator->fails()) {
                // Alert::error('Oops, Form has some error!');
                return Redirect::to('leaves/add')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                if( ($request->leave_start_date==$request->leave_end_date)  && ($request->leave_start_type== 2) && ($request->leave_end_type== 0 || $request->leave_end_type == 1) ){
                    alert()->info('You can only select second half as leave end type.')->persistent('close')->autoclose("3600");
                    return Redirect::to('leaves/add');
                }
                if(($request->leave_start_type== 1 && $request->leave_end_type == 2) || ($request->leave_start_type== 2 && $request->leave_end_type == 1)  ){
                    alert()->info('Due to some restrictions, you have to create individual leaves.')->persistent('close')->autoclose("3600");
                    return Redirect::to('leaves/add');
                }
                $start = $request->leave_start_date;
                $end = $request->leave_end_date;

                $checkLeave = Leave::where('user_id','=',$getUid)
                                ->where(function ($query) use ($start, $end) {
                                 $query->where(function ($q) use ($start, $end) {
                                        $q->where('leave_start_date', '>=', $start)
                                           ->where('leave_start_date', '<', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('leave_start_date', '<=', $start)
                                           ->where('leave_end_date', '>', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('leave_end_date', '>=', $start)
                                           ->where('leave_end_date', '<=', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('leave_start_date', '>=', $start)
                                           ->where('leave_end_date', '<=', $end);
                                    });

                                })->count();

                if($checkLeave > 0){
                   alert()->info('Your already have leave on that day.')->persistent('close')->autoclose("3600");
                   return Redirect::to('leaverequests');
                }
                else{
                     $checkcompOff = CompOff::where('user_id','=',$getUid)
                                ->where(function ($query) use ($start, $end) {
                                 $query->where(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '>=', $start)
                                           ->where('compoff_start_date', '<', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '<=', $start)
                                           ->where('compoff_end_date', '>', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_end_date', '>=', $start)
                                           ->where('compoff_end_date', '<=', $end);

                                    })->orWhere(function ($q) use ($start, $end) {
                                        $q->where('compoff_start_date', '>=', $start)
                                           ->where('compoff_end_date', '<=', $end);
                                    });

                                })->count();
                      if($checkcompOff>0)
                      {
                          alert()->info('Your already have comp off on that day.')->persistent('close')->autoclose("3600");
                         return Redirect::to('leaverequests');
                      }else{
                        $leave = new Leave;


                    $summary['start_date'] = $postData['leave_start_date'];
                    $summary['end_date'] = $postData['leave_end_date'];
                    $summary['start_type'] = $postData['leave_start_type'];
                    $summary['end_type'] = $postData['leave_end_type'];
                    $summaryDetails = getLeaveSummary($summary);
                    if($summaryDetails < 0.5){
                        Alert::error('Oops, Something went wrong please try again!');
                        return Redirect::to('leaves/add');
                    }
                    if($summaryDetails['leave_days']==0 || $summaryDetails['leave_days']== -1){
                        // alert()->info('Your not allowed to add weekend as leave.')->persistent('close')->autoclose("3600");
                            Alert::error('Your not allowed to add weekend as leave.');
                           return Redirect::to('leaverequests');
                       }
                    $leave->user_id = Auth::user()->id;
                    $leave->approver_id =  $user->getReportingToListWithOnlyTeamLeader(Auth::user()->id);
                    $leave->leave_start_date = date("Y-m-d", strtotime(Input::get('leave_start_date')));
                    $leave->leave_end_date = date("Y-m-d", strtotime(Input::get('leave_end_date')));
                    $leave->return_date = date("Y-m-d", strtotime($summaryDetails['return_date']));
                    $leave->leave_days = $summaryDetails['leave_days'];
                    $leave->leave_start_type = Input::get('leave_start_type');
                    $leave->leave_end_type = Input::get('leave_end_type');
                    $leave->reason = Input::get('reason');
                    $leave->approver_comment = '';
                    $leave->leave_status = 2;
                    if(isset($postData['adhoc_leave']) && $postData['adhoc_leave'] == 'on'){
                        $leave->is_adhoc = 1;
                    }else{
                        $leave->is_adhoc = 0;
                    }
                    if((!isset($postData['adhoc_leave']))  &&  date('Y-m-d') > $leave->leave_start_date){
                        Alert::error('You can not add leave of pervious days');
                        return Redirect::to('leaverequests');
                    }


                    $leave->save();
                    $leaveID = $leave->id;
                    $leaveDetails = $leave->getLeaveDetails($leaveID);

                    $obj_leave_detail = new LeaveDetail();
                    $obj_leave_detail->createLeaveDetail($leaveDetails);


                    $user = new User();
                    $update_leave = 0;
                    // if((isset($postData['adhoc_leave']) && $postData['adhoc_leave'] == 'on')&& $leaveDetails['leave_status'] == 1){
                    //     $userDetails = $user->getUserDetails($leaveDetails['user']['id']);
                    //     $update_leave = $userDetails['used_leaves'] + $leaveDetails['leave_days'];
                    //     $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                    // }

                    $data = array();
                    $data['id'] = $leaveID;
                    $data['reporting_name'] = $leaveDetails['approver']['first_name'].' '.$leaveDetails['approver']['last_name'];
                    $data['employee_name'] = $leaveDetails['user']['first_name'].' '.$leaveDetails['user']['last_name'];
                    $data['leave_start_date'] = date('jS F, Y', strtotime($leaveDetails['leave_start_date']));
                    $data['leave_end_date'] = date('jS F, Y', strtotime($leaveDetails['leave_end_date']));
                    $data['return_date'] = date('jS F, Y', strtotime($leaveDetails['return_date']));
                    $data['leave_days'] = $leaveDetails['leave_days'];
                    $data['reason'] = $leaveDetails['reason'];


                    $authUser = Auth::user();
                    $company_id = $authUser->company_id;
                    $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany($company_id);
                    $cc = $bcc = '';
                    $cc_array = array($hr_admin_email['hr_email_address']);
                    $pm_email = getUsersPm(Auth::user()->id);
                    if($pm_email != false && $pm_email != $leaveDetails['approver']['email']){
                        $cc_array[] = $pm_email;
                    }
                    $cc = $cc_array;
                    $bcc = $hr_admin_email['email'];

                    $company     = new Company();
                    $userCompanyMailData  = $company->getCompanyViseMailData($company_id)[0];
                    $data['regards'] = $userCompanyMailData['company_name'];
                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name'];

                    $to = $leaveDetails['approver']['email'];
                    $template = 'emails.leave_request';
                    $subject = 'Leave Application Of '.$data['employee_name'];
                    MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);

                    // redirect
                    Alert::success('Leave Request Successfully Created!');
                    return Redirect::to('leaverequests');
                      }


                }
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
            if ((Auth::user()->hasPermission('team.leaves.listing') || checkReprtingPerson(auth()->user()->id)) ) {
                $authUser = Auth::user();
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;

                $leave = new Leave();
                $leaveDetails = $leave->getLeaveDetails($id);

                if(empty($leaveDetails)){
                    Alert::error('This leave has been removed');
                    return Redirect::to('teamleaves');
                }
                if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || in_array($leaveDetails['user_id'],getTeamUserId(auth()->user()->id))){
                    $user = new User();
                    $reportingToList = $user->getReportingToList();
                    return view('pms.leaves.edit', compact('leaveDetails', 'reportingToList'));
                } else {
                    Alert::error('You do not have permission to perform this action!');
                    return Redirect::to('dashboard');
                }
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::check()) {
            if ((Auth::user()->hasPermission('team.leaves.listing') || checkReprtingPerson(auth()->user()->id))) {
                $leave = Leave::find($id);
                $loggedInUserID = isset(auth()->user()->id) ? intval(auth()->user()->id) : 0;

                if(empty($leave)){
                    Alert::error('This leave has been removed');
                    return Redirect::to('teamleaves');
                }

                if(Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug')) || in_array($leave['user_id'],getTeamUserId(auth()->user()->id))) {
                    $postData = $request->input();

                    $validationRule['leave_status'] = 'required|integer';
                    $validationRule['approver_comment'] = 'required|string|min:6';

                    $validator = Validator::make(Input::all(), $validationRule);

                    if ($validator->fails()) {
                        return Redirect::to('leaves/approve/'.$id)
                            ->withErrors($validator)
                            ->withInput();
                    } else {
                        $beforeUpdate = $leave->getLeaveDetails($id);
                        $leave->approver_comment = Input::get('approver_comment');
                        $leave->leave_status = Input::get('leave_status');
                        $leave->approver_id = $loggedInUserID;
                        $leave->approver_date = date('Y-m-d');
                        $leave->save();

                        $leave = new Leave();
                        $leaveDetails = $leave->getLeaveDetails($id);
                        $obj_leave_detail = new LeaveDetail();
                        $obj_leave_detail->updateLeaveDetail($leaveDetails);

                        $leaveStatusArray = config('constant.leave_status');

                        $user = new User();
                        $update_leave = 0;
                        if(isset($leaveDetails['leave_status']) && $leaveDetails['leave_status'] != 2){

                            $userDetails = $user->getUserDetails($leaveDetails['user']['id']);
                            if($leaveDetails['leave_status'] == 3 || $leaveDetails['leave_status'] == 1){
                                if($leaveDetails['leave_status'] == 3 && $beforeUpdate['leave_status'] == 1){
                                    $remainingLeave = $userDetails['available_leaves'] - $userDetails['used_leaves'];

                                    if ($remainingLeave>0) {
                                        $update_leave = $userDetails['used_leaves'] - $leaveDetails['leave_days'];
                                        $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                                    } else{

                                        $update_leave = $userDetails['used_leaves'] - $leaveDetails['leave_days'];
                                        $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                                        $compoff = CompOff::where('user_id',$leaveDetails['user']['id'])->where('compoff_marked_as',2)->orderBy('id','desc')->get();
                                        // echo '<pre>'; print_r($compoff);exit;
                                        $leavecount = $leaveDetails['leave_days'];
                                        if(!empty($compoff)){
                                            $i=1;
                                            foreach($compoff as $key=>$value)
                                            {
                                                if(!empty($value->leave_used)){
                                                $leave = explode(",",$value->leave_used);
                                                foreach($leave as $key=>$val){
                                                    if($i <= $leavecount && $val == 1){
                                                            $leave[$key] = 0;
                                                            $i++;
                                                        }
                                                    }
                                                    $leave = implode(",",$leave);
                                                    $getdata =   CompOff::where('user_id',$leaveDetails['user']['id'])->where('compoff_marked_as',2)->where('id',$value->id)->first();
                                                    $getdata->leave_used =   $leave;
                                                    $getdata->save();
                                                }else{

                                                }
                                            }
                                        }
                                    }
                                }elseif($leaveDetails['leave_status'] == 1){
                                    // echo 'hii'; exit;
                                    $remainingLeave = $userDetails['available_leaves'] - $userDetails['used_leaves'];
                                        // echo '<pre>'; print_r($remainingLeave);

                                        if($remainingLeave>0){
                                            // echo "in if";exit();
                                            if($leaveDetails['leave_days']>$remainingLeave){
                                                $diffOfLeaveDays = $leaveDetails['leave_days']-$remainingLeave;
                                                // echo '<pre>'; print_r($diffOfLeaveDays); exit();
                                                if(!empty($diffOfLeaveDays)){
                                                    $compoff = CompOff::where('user_id',$leaveDetails['user']['id'])->where('compoff_marked_as',2)->orderBy('id','desc')->get();
                                                    // echo '<pre>'; print_r($compoff);
                                                    $leavecount = $diffOfLeaveDays;
                                                    if(!empty($compoff)){

                                                    $i=1;
                                                        foreach($compoff as $key=>$value)
                                                        {
                                                        if(!empty($value->leave_used) || $value->leave_used == 0){
                                                            // echo "hii"; exit;
                                                            $leave = explode(",",$value->leave_used);
                                                            foreach($leave as $key=>$val){
                                                                if($i <= $leavecount && $val == 0){
                                                                    $leave[$key] = 1;
                                                                    $i++;
                                                                }
                                                                }
                                                                $leave = implode(",",$leave);
                                                                $getdata =   CompOff::where('user_id',$leaveDetails['user']['id'])->where('compoff_marked_as',2)->where('id',$value->id)->first();
                                                                $getdata->leave_used =   $leave;
                                                                $getdata->save();
                                                                // echo "success";
                                                                // exit;
                                                            }else{
                                                                    // echo "in else"; exit;
                                                            }
                                                        }
                                                    }

                                                }else{

                                                    }

                                            }
                                        $update_leave = $userDetails['used_leaves'] + $leaveDetails['leave_days'];
                                        $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                                        }else{
                                            $update_leave = $userDetails['used_leaves'] + $leaveDetails['leave_days'];

                                            $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                                            $compoff = CompOff::where('user_id',$leaveDetails['user']['id'])->where('compoff_marked_as',2)->orderBy('id','desc')->get();
                                            // echo '<pre>'; print_r($compoff);exit;

                                            $leavecount = $leaveDetails['leave_days'];
                                            if(!empty($compoff)){

                                                $i=1;
                                                foreach($compoff as $key=>$value)
                                                {
                                                    if(!empty($value->leave_used) || $value->leave_used == 0){

                                                        // echo "hii"; exit;
                                                    $leave = explode(",",$value->leave_used);
                                                    foreach($leave as $key=>$val){
                                                        if($i <= $leavecount && $val == 0){
                                                                $leave[$key] = 1;
                                                                $i++;
                                                            }
                                                        }
                                                        $leave = implode(",",$leave);
                                                        // echo '<pre>'; print_r($leave);exit();
                                                        $getdata =   CompOff::where('user_id',$leaveDetails['user']['id'])->where('compoff_marked_as',2)->where('id',$value->id)->first();
                                                        $getdata->leave_used =   $leave;
                                                        $getdata->save();
                                                        // echo "yes";
                                                        // exit;
                                                    }else{

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            $data = array();
                            $data['id'] = $id;
                            $data['reporting_name'] = $leaveDetails['approver']['first_name'].' '.$leaveDetails['approver']['last_name'];
                            $data['employee_name'] = $leaveDetails['user']['first_name'].' '.$leaveDetails['user']['last_name'];
                            $data['leave_start_date'] = date('jS F, Y', strtotime($leaveDetails['leave_start_date']));
                            $data['leave_end_date'] = date('jS F, Y', strtotime($leaveDetails['leave_end_date']));
                            $data['leave_status'] = $leaveStatusArray[$leaveDetails['leave_status']];

                            $company     = new Company();
                            $userCompanyMailData  = $company->getCompanyViseMailData($leaveDetails['user']['company_id'])[0];
                            $data['regards'] = $userCompanyMailData['company_name'];
                            $from['email'] = $userCompanyMailData['from_address'];
                            $from['name']  = $userCompanyMailData['company_name'];

                            $to = $leaveDetails['user']['email'];
                            $template = 'emails.leave_approve';
                            $subject = 'Your Leave Application has been '.$data['leave_status'];

                            MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []);
                        }
                        // redirect
                        Alert::success('Leave Request Successfully Updated!');
                        return Redirect::to('teamleaves');
                    }
                } else {
                    Alert::error('You do not have permission to perform this action!');
                    return Redirect::to('dashboard');
                }
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function teamLeaveRequests(Request $request)
    {
        if (Auth::check()) {
            if ((Auth::user()->hasPermission('team.leaves.listing') || checkReprtingPerson(auth()->user()->id))) {

                 $year_start_date = date('Y-m-d',strtotime(date('01-04-Y')));
                    if(date('m-d',strtotime($year_start_date)) > date('m-d'))
                    {
                        $year = date("Y", strtotime("-1 years"));
                        $year_start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
                    }
                $company     = new Company();
                $companyList     = $company->getCompanyName();
                $leaveStatusSelected = '';
                if (isset($request->search_by_leave_status) && $request->search_by_leave_status != 2    ) {
                    $leaveStatusSelected = $request->search_by_leave_status;
                }else{
                    $leaveStatusSelected = '2';
                }
                $userIdArray = $leavesData = $userNameArray = array();
                $authUser = Auth::user();
                $user = new User();

                $page       = $request->get('page');
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
                $start_date = $request->search_leave_start_date;
                $end_date = $request->search_leave_end_date;
                if (Auth::user()->hasRole(config('constant.hr_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                    $company_id = '';
                    if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
                        $company_id = $authUser->company_id;
                    }
                    $allUserList = $userNameList = $user->getUserNameByCompany($company_id);
                } else {
                    $allUserList = $user->getUserNameByCompany($authUser->company_id);
                    $userNameList = $user->getMyTeamList($loggedInUserID);
                }

                $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
                if(!empty($userNameList)){
                    foreach ($userNameList as $userDetail) {
                        $userIdArray[] = $userDetail['id'];
                        $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
                if(!empty($allUserList)){
                    foreach ($allUserList as $userDetail) {
                        $allUserListArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                    }
                }
                $search_cmp = '';
                if ($request->has('search_submit') && $request->search_submit != '') {
                    if ($request->has('search_by_leave_emp_company') && $request->search_by_leave_emp_company != '') {
                        $search_cmp = $request->search_by_leave_emp_company;
                    }
                }

                $leave = Leave::with(['user','approver']);
                $leave->leftJoin('users as user','user.id','leaves.user_id');
                $leave->leftJoin('companies as c','c.id','user.company_id');
                $leave->whereIn('user_id', $userIdArray);
                if($search_cmp){
                    $leave->where('user.company_id','=',$search_cmp);
                }

                if(!empty($userIdArray)){
                    if ($userIdArray) {
                        if ($leaveStatusSelected != '' && $leaveStatusSelected != 0) {
                            $leave->whereIn('leave_status', [$leaveStatusSelected]);
                        }
                    }
                }
                if ($request->has('search_submit') && $request->search_submit != '') {
                    // add by mohit
                    if ($request->has('search_by_department') && count($request->search_by_department) > 0 ){
                        $leave =   $leave->whereIn('user.department',($request->search_by_department));
                    }
                    if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                        $param = $request->search_by_internal_company_id;
                        if(in_array("parent_company",$param) && count($param) >= 2){
                            $leave =   $leave->where(function ($q) use ($param) {
                                $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                            });
                        } else if (in_array("parent_company",$param)) {
                            $leave =   $leave->WhereNull('internal_company_id');
                        } else {
                            $leave =   $leave->whereIn('internal_company_id', ($param));
                        }
                    }
                    // end by mohit
                    if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                        $leave->where('leaves.user_id',  '=', $request->search_by_employee);
                    }
                    if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                        $leave->where('leaves.approver_id',  '=', $request->search_by_approver);
                    }
                    if($request->has('adhoc_leave') && $request->adhoc_leave == 'on'){
                        $leave->where('leaves.is_adhoc',  '=', 1);
                    }
                    if ($request->has('search_leave_start_date') && $request->search_leave_start_date != '') {
                        $leave->where('leaves.leave_start_date',  '>=', date("Y-m-d", strtotime($request->search_leave_start_date)));
                    }
                    if ($request->has('search_leave_end_date') && $request->search_leave_end_date != '') {
                        $leave->where('leaves.leave_end_date',  '<=',  date("Y-m-d", strtotime($request->search_leave_end_date)));
                    }
                    if ($request->has('search_by_leave_status') && $request->search_by_leave_status != '' && $request->search_by_leave_status != 0) {
                        $leave->where('leaves.leave_status',  '=', $request->search_by_leave_status);
                    }
                    if((!($request->has('search_leave_start_date') && $request->search_leave_start_date != '')) && (!($request->has('search_leave_end_date') && $request->search_leave_end_date != ''))){
                        //echo "dsa";exit;
                        $leave->where('leaves.leave_start_date',  '>=', $year_start_date);
                    }
                   // echo "<pre>";print_r($request->all());exit;
                }else{
                   // echo "dsa";exit;
                    $leave->where('leaves.leave_start_date',  '>=', $year_start_date);
                }
                $leave->select('leaves.*','c.company_name');
                if ($request->has('sort') && $request->input('sort') != '') {
                    $leavesData = $leave->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
                } else {
                    $leavesData = $leave->sortable()->orderBy('leaves.leave_start_date', 'DESC')->paginate($page_limit);
                }
                if (empty($userIdArray) && (Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->designation_id == 16 || Auth::user()->designation_id == 26 )){
                    $leavesData = array();
                }
              //  echo "<pre>";print_r($leavesData);exit;
                return view('pms.leaves.team_leave_requests', compact('allUserListArray','leavesData','companyList', 'loggedInUserID', 'leaveStatusSelected','page','userNameArray','request','start_date','end_date'));
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function updateCheckedLeaveAjax(Request $request) {
        $action = $request['action'];
        $user = new User();
        $update_leave = 0;
        if($action == 0){
            if(Auth::user()->hasRole(config('constant.admin_slug'))
            || Auth::user()->hasRole(config('constant.hr_slug'))
            || Auth::user()->hasRole(config('constant.superadmin_slug')))
            {
                $leave = new Leave();
                $leaveArray = $request['leaveIdArray'];
                foreach($leaveArray as $val){
                    $leaveEntry = Leave::find($val);
                    $leaveEntry->delete();
                    LeaveDetail::where("leave_group_id",$val)->delete();
                    $userDetails = $user->getUserDetails($leaveEntry['user']['id']);
                    if($leaveEntry['leave_status'] == 1){
                        $update_leave = $userDetails['used_leaves'] - $leaveEntry['leave_days'];
                        $user->updateUserUsedLeave($leaveEntry['user']['id'],$update_leave);
                    }
                }
                return Response::json(array('message'=>'Leave Entry Deleted Successfully','status'=>'success'));
            }else{
                return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
            }
        }elseif ($action == 4 || $action == 5) {
            $this->updateAttendanceLeaveDetail($request['leaveIdArray'],$action);
        } else{
            $leave = new Leave();
            $leave->checkedUpdate($request['leaveIdArray'],$action);
        }
    }

    public function updateAttendanceLeaveDetail($leaveIdArray,$action){
        if(isset($leaveIdArray) && count($leaveIdArray) > 0){
            foreach($leaveIdArray as $value){
                $leave = Leave::find($value);
                if(isset($leave->reason) && $leave->reason == "Auto Generated Leave Adhoc Leave" && $leave->leave_end_date < date("Y-m-d")){
                    $date = $leave->leave_start_date;
                    $leaveObj = new Leave();
                    $isHoliday = $leaveObj->isHoliday($date);
                    if($isHoliday != true){
                        if($action == 5){
                            $firstTime = "10:00:00";
                            $lastTime = "18:30:00";
                        }elseif($action == 4){
                            $firstTime = "15:00:00";
                            $lastTime = "19:30:00";
                        }
                        $attendance = Attendance::where(['user_id' => $leave->user_id, 'entry_date' => $date])->whereNull('deleted_at')->orderBy('id','desc')->first();
                        $request = new \Illuminate\Http\Request();
                        $request->replace(['hidenTotalElement' => 1,
                                    'hiddenAttendanceId' => $attendance->id,
                                    'inEntry_1' => $firstTime,
                                    'outEntry_1' => $lastTime]);
                        $result = (new TimeSheetController)->updateEntry($request);                               
                    }
                }
            }
        }
    }

    public function leaveDalete(Request $request)
    {
        try{
            $user = new User();
            $update_leave = 0;
            $leaveId = $request->input('leaveId');
            $leaveEntry = Leave::find($leaveId);
            if(isset($leaveEntry))
            {
                if((!Auth::user()->hasRole(config('constant.admin_slug')))
                && (!Auth::user()->hasRole(config('constant.hr_slug')))
                && (!Auth::user()->hasRole(config('constant.superadmin_slug'))))
                {
                    if($leaveEntry->user_id != Auth::user()->id || date('Y-m-d') > $leaveEntry->leave_start_date)
                    {
                        return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
                    }
                }

                $leaveEntry->delete();
                LeaveDetail::where("leave_group_id",$leaveId)->delete();

                $userDetails = $user->getUserDetails($leaveEntry['user']['id']);
                if($leaveEntry['leave_status'] == 1){
                    // $update_leave = $userDetails['used_leaves'] - $leaveEntry['leave_days'];
                    // $user->updateUserUsedLeave($leaveEntry['user']['id'],$update_leave);
                    $remainingLeave = $userDetails['available_leaves'] - $userDetails['used_leaves'];

                                if($remainingLeave>0){
                                  $update_leave = $userDetails['used_leaves'] - $leaveEntry['leave_days'];
                                  $user->updateUserUsedLeave($leaveEntry['user']['id'],$update_leave);
                                }else{

                                    $update_leave = $userDetails['used_leaves'] - $leaveEntry['leave_days'];
                                    $user->updateUserUsedLeave($leaveEntry['user']['id'],$update_leave);
                                    $compoff = CompOff::where('user_id',$leaveEntry['user']['id'])->where('compoff_marked_as',2)->orderBy('id','desc')->get();
                                     // echo '<pre>'; print_r($compoff);exit;
                                    $leavecount = $leaveEntry['leave_days'];
                                    if(!empty($compoff)){
                                        $i=1;
                                        foreach($compoff as $key=>$value)
                                        {
                                            if(!empty($value->leave_used)){
                                               $leave = explode(",",$value->leave_used);
                                               foreach($leave as $key=>$val){
                                                  if($i <= $leavecount && $val == 1){
                                                        $leave[$key] = 0;
                                                        $i++;
                                                    }
                                                }
                                                $leave = implode(",",$leave);
                                                $getdata =   CompOff::where('user_id',$leaveEntry['user']['id'])->where('compoff_marked_as',2)->where('id',$value->id)->first();
                                                $getdata->leave_used =   $leave;
                                                $getdata->save();
                                            }else{

                                            }
                                        }
                                    }
                                }
                }
                return Response::json(array('message'=>'Leave Entry Deleted Successfully','status'=>'success'));
            }else{
                return Response::json(array('message'=>'Something was wrong!','status'=>'error'));
            }
        }catch(Exception $e){
            return Response::json(array('message'=>'Leave Entry Deleted Successfully','status'=>'error'));
        }
    }

    public function isLeave($user_id,$date){
        $leave = Leave::query();
        $date = date("Y-m-d", strtotime($date));
        $leave->whereDate('leaves.leave_start_date','<=', $date);
        $leave->whereDate('leaves.leave_end_date','>=', $date);
        $leave->where('leave_status', 1);   //Approved
        $leave->where('user_id', $user_id);   //Approved
        $leaveData = $leave->get();
        if(isset($leaveData[0])){
            return true;
        }else{
            return false;
        }
    }
}
