<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Leave;
use Auth;
use App\Company;
use App\EmailTemplate;
use App\Jobs\MailJob;
use App\Designation;
use Alert;
use Validator;
use Excel;
use App\Imports\ImportAppraisal;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use jeremykenedy\LaravelRoles\Models\Role;
use App\Exports\ExportAppraisalFailedReport;
class AppraisalController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            $user = new User();
            $userDetails = $user->where('company_id',Auth::user()->company_id)->whereNull('deleted_at')->where('users.status',1)
                            ->orderBy('first_name', 'ASC')->orderBy('first_name', 'ASC')->get()->toArray();
            $designation = new Designation();
            $user_designation = $designation->getDesignationName();
            return view('pms.appraisal.index',compact('userDetails','user_designation'));
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function getDesignation($userID){
        $user = User::query()->with('designation');
        $desig_id = $user->where('users.id',$userID)->first()->toArray();
        return $desig_id['designation']['id'];
    }

    public function updateAppraisal(Request $request)
    {
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            $from_date = $request->from_date != ''?$request->from_date : '';
            $to_date = $request->to_date != ''?$request->to_date : '';
            $count = 0;
            foreach(array_keys($request->all()) as $val)
            {
                $emp = explode("_", $val);
                if($emp[0] == "name" && $request->$val != null)
                {
                    $user = new User;
                    $userDetails = $user->getUserDetails($request->$val);
                    $oldDesignation = $userDetails['designation_id'];
                    $newCtc = "newCtc_".$emp[1];
                    $designation = "designation_".$emp[1];
                    $newAmount = "amount_".$emp[1];
                    $ctc = $request->$newCtc;   
                    $amt = $request->$newAmount;
                    $newDesignation = $request->$designation;
                    $designation_name = Designation::find($userDetails['designation_id'])->designation_name;
                    if($newDesignation != $oldDesignation)
                    {
                        $updateUser = User::find($request->$val);
                        $updateUser->designation_id = $newDesignation;
                        $updateUser->save();
                        $role = Role::where('id', '=', $userDetails['roleuser']['id'])->first();
                        $updateUser->detachRole($role);
                        $role_details = Designation::find($newDesignation);
                        $designation_name = $role_details->designation_name;
                        $role = Role::where('id', '=', $role_details->role_id)->first();
                        $updateUser->attachRole($role);
                        $data['update_designation'] = "update_designation";
                    }
                    if($ctc != 0){
                        ////Mail System
                        $company     = new Company();
                        $userCompanyMailData  = $company->getCompanyViseMailData($userDetails['company_id'])[0];
                        $data['regards'] = $userCompanyMailData['company_name'];
                        $from['email'] = $userCompanyMailData['from_address'];
                        $from['name']  = $userCompanyMailData['company_name'];
                        $mailTemplate = new EmailTemplate();
                        $to = $userDetails['email'];
                        // $to = "trushaptridhyatech@gmail.com";
                        $cc = "";
                        $template = 'emails.employee_appraisal_changed';
                        $mail_subject = '';
                        $data['full_name'] = $userDetails['first_name'].' '.$userDetails['last_name'];
                        $data['first_name'] = $userDetails['first_name'];
                        $subject = 'Employee Appraisal';
                        $module = 'update_appraisal';
                        $mailTemp = $mailTemplate->emailTemplate($userDetails['company_id'],$module);
                        $data['temp'] = "";
                        $data['from_date'] = $from_date;
                        $data['to_date'] = $to_date;
                        $data['designation'] = $designation_name;
                        $data['new_ctc'] = $ctc;
                        $data['appraisal_amount'] = $amt;
                        if($mailTemp != null)
                        {
                            $keyword = [
                                'first_name'        => $data['first_name'],
                                'full_name'         => $data['full_name'],
                                'company_name'      => $data['regards'],
                                'new_ctc'           => $data['new_ctc'],
                                'designation'       => $data['designation'],
                                'from_date'         => $data['from_date'],
                                'to_date'           => $data['to_date'],
                                'appraisal_amount'  => $data['appraisal_amount']
                            ];
                            $subject = $mailTemp['subject'];
                            $data['temp'] = $mailTemplate->replaceKeyword($mailTemp['email_format'],$keyword);
                            $cc = $mailTemplate->sendToCC($userDetails['company_id'],$mailTemp['send_cc'],$userDetails['reporting_to']);
                        }
                        $subject = $subject != ''?$subject:$mail_subject;
                        MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []);
                        $count++;
                    }
                }
            }
            Alert::success(' '.$count.' Mail Successfully Sended!');
            return Redirect::back();
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function uploadAppraisal(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            if ($request->hasFile('csvfile')) {      
                $validator=Validator::make($request->all(),[
                    'csvfile'=>'required|mimes:xlsx,xls'
                ]); 
                if ($validator->fails()) {
                    Alert::error('Oops, Invalid file type or file seems to be missing!');
                    return Redirect::to('appraisal')
                    ->withErrors($validator)
                    ->withInput();
                }
                else {
                    $file = $request->file('csvfile');
                    $extension = $file->getClientOriginalExtension();
                    $filename = 'appraisal_amount_'.time().'.'.$extension;
                    if ($request) {
                        Storage::disk('local')->putFileAs('', $file, $filename);
                    }
                    $importError = array();
                    Excel::import(new ImportAppraisal, $filename);
                    $importError = Session::get('AppraisalImportError');
                    Session::forget('AppraisalImportError');
                    if(count($importError)>0){
                        $count = $importError[0]['success'];
                        Alert::success(' '.$count.' Mail Successfully Sended!');
                        if(count($importError[0]['skipped'])>0)
                        {
                            return Excel::download(new ExportAppraisalFailedReport($importError[0]['skipped']), 'failed_appraisal_amount.xlsx');
                        }
                    }
                    $importError = Session::forget('AppraisalImportError');
                    return Redirect::to('appraisal');
                }
            }
            return Redirect::to('appraisal');
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
}
