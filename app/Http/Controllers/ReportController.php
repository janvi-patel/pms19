<?php
namespace App\Http\Controllers;

set_time_limit(0);
use App\Leave;
use App\User;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use App\CompOff;
use App\Company;
use App\Attendance;
use DateTime;
use DateInterval;
use App\Designation;
use DatePeriod;
use App\Project;
use App\ProjectEntry;
use Carbon\Carbon;
use App\ProjectTasks;
use App\Holiday;
use Kyslik\ColumnSortable\Sortable;
use DB;
use Alert;
use Excel;
use App\Exports\ExportProjectReport;
use App\Exports\ExportMonthlyLeave;
use App\Exports\ExportTimeSheet;
use App\Exports\ExportBillableHoursReport;
use App\MonthlyLeaveBalance;
use App\BillableReport;

class ReportController extends Controller {

    public function leaveReport(Request $request){
        //echo "<pre>";print_r($request->all());exit;
        if(Auth::check() && Auth::user()->hasPermission('leave.report')){
            $year_start_date = date('Y-m-d',strtotime(date('01-04-Y')));
            if(date('m-d',strtotime($year_start_date)) > date('m-d'))
            {
                $year = date("Y", strtotime("-1 years"));
                $year_start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
            }
           // echo $year_start_date;exit;
            $company     = new Company();
            $companyList     = $company->getCompanyName();
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            if(Auth::check() && (Auth::user()->hasRole('super_admin') ||
                    Auth::user()->hasRole('admin') ||
                    Auth::user()->hasRole('hr') ||
                    Auth::user()->hasRole('tl')  ||
                    Auth::user()->hasRole('pm')) ||
                    Auth::user()->designation_id == 16
                    || Auth::user()->designation_id == 26 ) {

                $userIdArray = array();
               $userNameArray = array();
                $page       = $request->get('page');
                $authUser = Auth::user();
                $userName = new User();
                $company_id = '';
                $leaveStatus = 0;
                $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
                $company_id = '';

                if(!Auth::user()->hasRole('super_admin')){
                    $company_id = $authUser->company_id;
                }

                if (Auth::check() && (Auth::user()->hasRole(config('constant.hr_slug'))) ||
                    (Auth::user()->hasRole(config('constant.admin_slug'))) ||
                    Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                   $userNameList = $userName->getUserNameByCompany($company_id);
                }else{
                    $userNameList = $userName->getMyTeamList($authUser->id);
                }
                if(!empty($userNameList)){
                    foreach ($userNameList as $userDetail) {
                        $userIdArray[] = $userDetail['id'];
                        $userCodeArray[] = $userDetail['employee_id'];
                        $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
                        // echo '<pre>'; print_r($userNameArray);exit;
                    }
                }

                $start_date = $request->search_leave_start_date;
                $end_date = $request->search_leave_end_date;

                $leave = Leave::select('leaves.*','user.internal_company_id','user.department')->leftJoin('users as user','user.id','leaves.user_id')
                            ->leftJoin('companies as c','c.id','user.company_id');
                $leave =  $leave->where('leaves.leave_status','!=',3)->whereNull('user.deleted_at');
                
                if ($userIdArray) {
                   $leave =  $leave->whereIn('leaves.user_id', $userIdArray);
                }
                else{
                    $leave =  $leave->whereIn('leaves.user_id', []);
                }
                //for filter
                if ($request->has('search_submit') && $request->search_submit != '') {
                    if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                        $leave->whereIn('user.department',($request->search_by_department));
                    }
                    if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                        $param = $request->search_by_internal_company_id;
                        if(in_array("parent_company",$param) && count($param) >= 2){
                            $leave->where(function ($q) use ($param) {
                                $q->whereIn('user.internal_company_id', ($param))->orWhereNull('internal_company_id');
                            });
                        } else if (in_array("parent_company",$param)) {
                            $leave->WhereNull('user.internal_company_id');
                        } else {
                            $leave->whereIn('user.internal_company_id', ($param));
                        }
                    }
                    if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                        $leave->where('leaves.user_id',  '=', $request->search_by_employee);
                    }
                    if ($request->has('search_by_company') && $request->search_by_company != '') {
                        $leave->where('user.company_id','=',$request->search_by_company);
                    }
                    if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                        $leave->where('leaves.approver_id',  '=', $request->search_by_approver);
                    }
                    if ($request->has('search_leave_start_date') && $request->search_leave_start_date != '') {
                        $leave->where('leaves.leave_start_date',  '<=', $request->search_leave_start_date);
                        $leave->Where(function($query)use($request){
                            $query->Where('leaves.leave_end_date','>=',$request->search_leave_start_date);
                        });

                    }
                    if($request->has('adhoc_leave') && $request->adhoc_leave == 'on'){
                        $leave->where('leaves.is_adhoc',  '=', 1);
                    }
                    if ($request->has('search_month') && $request->search_month != '') {
                        $leave->whereMonth('leaves.leave_start_date',  '<=', $request->search_month);
                        $leave->Where(function($query)use($request){
                            $query->whereMonth('leaves.leave_end_date','>=',$request->search_month);
                        });
                    }
                     if ($request->has('search_year') && $request->search_year != '') {
                        $leave->whereYear('leaves.leave_start_date',  '<=', $request->search_year);
                        $leave->Where(function($query)use($request){
                            $query->whereYear('leaves.leave_start_date','>=',$request->search_year);
                        });
                    }
                }else{
                     $leave->where('leaves.leave_start_date',  '>=', $year_start_date);
                }




                if ($request->has('search_by_leave_status') && $request->search_by_leave_status != '') {
                    $leave->where('leaves.leave_status',$request->search_by_leave_status);
                }else{
                    // $leave->where('leaves.leave_status',1);
                }
                $leave->select('leaves.*','c.company_name','user.*');
                $leaveAllData = clone $leave;
                if ($request->has('sort') && $request->input('sort') != '') {
                    $leavesData = $leave->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate(config('constant.recordPerPage'));
                } else {
                    if($request->has('search_leave_start_date') && $request->search_leave_start_date != ''){
                        $leavesData = $leave->sortable()->orderBy('user.first_name', 'asc')->paginate($page_limit);
                    }else{
                        $leavesData = $leave->sortable()->orderBy('leaves.leave_start_date', 'desc')
                                        ->orderBy('user.first_name', 'asc')->paginate($page_limit);
                    }
                }
                $attance = $leaveEmpArry = array();
                if ($request->has('search_leave_start_date') && $request->search_leave_start_date != '') {
                        $leave->where('leaves.leave_start_date',  '<=', $request->search_leave_start_date);
                        $leave->Where(function($query)use($request){
                            $query->Where('leaves.leave_end_date','>=',$request->search_leave_start_date);
                        });

                        foreach($leaveAllData->get() as $leaveEmpId){
                            $leaveEmpArry[] =  $leaveEmpId->employee_id;
                        }

                        if(count($leaveEmpArry)>0){
                            $getCompanyName = Company::find(Auth::user()->company_id);
                            $attance = Attendance::leftjoin('users','users.employee_id','attendances.emp_code')->whereIn('emp_code', $userCodeArray)
                                        ->where('attendances.emp_comp',$getCompanyName->company_name)
                                        ->whereNotIn('emp_code',$leaveEmpArry)->whereNotIn('emp_code',array(004,0104,000))->Where(function($query){
                                            // echo '<pre>';print_r($quey->get());exit;
                                              $query->whereNotNull('half_day');
                                             $query->orWhereNotNull('absent');

                                       })->where('entry_date',  '=', $request->search_leave_start_date)
                                        ->leftJoin('users as user','user.employee_id','attendances.emp_code')
                                        ->leftJoin('users as approver','user.reporting_to','approver.id')
                                        ->where('user.status',1)
                                        ->Where(function($query1){
                                           $query1->where('user.company_id',Auth::user()->company_id);
                                       })
                                        ->leftJoin('companies as c','c.id','user.company_id')
                                        ->select(DB::raw('CONCAT(approver.first_name," ",approver.last_name) AS approver_full_name'),'attendances.*')
                                        ->get();
                        }

                }
                // dd(DB::getQueryLog());
                // dd($attance);
                // echo '<pre>';print_r($leavesData);exit;
                $current_years = date('Y');
                for ($y = config('constant.Start_year'); $y <= $current_years; $y++) {
                    $years[$y] = $y;
                }
                return view('pms.reports.leavereport', compact('leavesData','companyList','page','userNameArray','request','start_date','end_date','attance','years'));
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function actualLeaveReport(Request $request){
        set_time_limit(0);
        $company  = new Company();
        $user = new User();
        $holiday = new Holiday();
        $authUser = Auth::user();
        $company_id = '';
        if(!Auth::user()->hasRole('super_admin')){
            $company_id = $authUser->company_id;
        }

        $holidayList = $ofcHolidayArray = $userNameArray = array();
        $leave_start_date = $request->search_leave_date;
        $start_date = $request->search_leave_start_date;
        $end_date = $request->search_leave_end_date;
        $page       = $request->get('page');
        $departmentArray = array(9);
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

        if (date('m') < 4) {
            $financial_start_year = (date('Y')-1);
            $financial_end_year = date('Y');

        } else {
            $financial_start_year = date('Y');
            $financial_end_year = (date('Y')+1);
        }
        $currentYear = date('Y');
        $date_from = $financial_start_year."-04-01";
        $date_to = $financial_end_year."-03-31";

        $companyList  = $company->getCompanyName();

        if (Auth::check() && (Auth::user()->hasRole(config('constant.hr_slug'))) ||
            (Auth::user()->hasRole(config('constant.admin_slug'))) ||
            Auth::user()->hasRole(config('constant.superadmin_slug'))) {
               $userNameList = $user->getUserNameByCompany($company_id);
            }else{
                $userNameList = $user->getMyTeamList($authUser->id);
            }
        if(!empty($userNameList)){
            foreach ($userNameList as $userDetail) {
                $userIdArray[] = $userDetail['id'];
                $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
            }
        }
        $userIdArray[] = $authUser->id;
        $userNameArray[$authUser->id] = $authUser->first_name . ' ' .$authUser->last_name;
        $getLastAttendId = Attendance::select('entry_date')->orderby('entry_date','DESC')->first();
        $attend_to_date = $getLastAttendId['entry_date'];

        $ofcHoliday = Holiday::where('holiday_year','>=',$currentYear)->orderBy('holiday_date', 'ASC')->pluck('holiday_date');

        foreach($ofcHoliday as $ofcHolidayVal){
            if(!in_array($ofcHolidayVal, $ofcHolidayArray)){
                $ofcHolidayArray[] = $ofcHolidayVal;
            }
        }
        $date_start = strtotime($date_from);
        $date_end = strtotime($date_to);
        $workingList = array();
        $leaveData = array();

        for ($i=$date_start; $i<=$date_end; $i+=86400) {
            if(!in_array(date("Y-m-d", $i),$ofcHolidayArray)){
                    $workingList[]= date("Y-m-d", $i);
            }
        }
        $attendDataQue = Attendance::leftJoin('users','attendances.emp_code','users.employee_id')
                           ->whereIn('entry_date',$workingList);

            if(Auth::check() && (Auth::user()->hasRole('super_admin') ||
                        Auth::user()->hasRole('admin') ||
                        Auth::user()->hasRole('hr'))){
            }else{
                if(Auth::user()->hasRole('developer')){
                    $attendDataQue->Where('users.id',$authUser->id);
                }else{
                     $attendDataQue->WhereIn('users.id',$userIdArray);
                }
            }
                $attendDataQue->Where('users.status',1);
            if($company_id != ''){
                $companyName = $company->getCompanyNameById($company_id)->company_name;
                $attendDataQue->where('emp_comp',$companyName );
                $attendDataQue->Where('users.company_id',$company_id);
            }
            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                    $attendDataQue->where('users.id',  '=', $request->search_by_employee);
                }
                if ($request->has('search_leave_date') && $request->search_leave_date != '') {
                    $attendDataQue->where('entry_date',  '=', $request->search_leave_date);
                }
                if ($request->has('search_leave_start_date') && $request->search_leave_start_date != '') {
                    $attendDataQue->where('entry_date',  '>=', $request->search_leave_start_date);
                }
                if ($request->has('search_leave_end_date') && $request->search_leave_end_date != '') {
                    $attendDataQue->where('entry_date',  '<=', $request->search_leave_end_date);
                }
                if ($request->has('search_by_company') && $request->search_by_company != '') {
                    $attendDataQue->where('users.company_id',  '<=', $request->search_by_company);
                }
                if ($request->has('search_month') && $request->search_month != '') {
                    $attendDataQue->whereMonth('entry_date',  '<=', $request->search_month);
                    $attendDataQue->Where(function($query)use($request){
                        $query->whereMonth('entry_date','>=',$request->search_month);
                    });
                }
           }
        $attendDataQue->whereNotIn('users.department', $departmentArray);
        $attendDataQue = $attendDataQue->Where(function($query2) {
                            $query2->where('first_in','00:00:00')
                                ->where('last_out','00:00:00')
                            ->orWhere(function($query3) {
                                $query3->where('absent',1);
                            })->orWhere(function($query4) {
                                $query4->where('half_day',1);
                            });
                        })
                        ->select('entry_date','emp_code','absent','half_day','emp_name');

        if ($request->has('sort') && $request->input('sort') != '') {
            $attendDataQue = $attendDataQue->sortable()->orderBy($request->input('sort'), $request->input('direction'))->get();
        } else {
            $attendDataQue = $attendDataQue->sortable()->orderBy('users.first_name', 'ASC')->get();
        }
        $leaveDataArray = $leavesData = array();
        if (!empty($attendDataQue)) {
            $attendDataArray = $attendDataQue->toArray();
            foreach($attendDataArray as $leavesDataVal){
                if($request->has('search_by_type') && $request->search_by_type != ''){
                    if($request->search_by_type == 1){
                        $leaveDataArray[$leavesDataVal['entry_date']][] = $leavesDataVal;
                    }else{
                        $leaveDataArray[$leavesDataVal['emp_name']][] = $leavesDataVal;
                    }
                }else{
                    $leaveDataArray[$leavesDataVal['emp_name']][] = $leavesDataVal;
                }
            }
            $leavesData = $this->paginate($leaveDataArray, $perPage = $page_limit, $page = null, ['path'  => $request->url()])->appends($request->input('sort'), $request->input('direction'));
        }
        return view('pms.reports.actual_leavereport',compact('leave_start_date','leavesData','userNameArray','companyList','page','request','start_date','end_date'));
    }

    public function compOffsReport (Request $request){
        if(Auth::check() && Auth::user()->hasPermission('compoff.report')){

            $company     = new Company();
            $companyList     = $company->getCompanyName();
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            if(Auth::check() && (Auth::user()->hasRole('super_admin') ||
                Auth::user()->hasRole('admin') ||
                Auth::user()->hasRole('hr') ||
                Auth::user()->hasRole('tl')  ||
                Auth::user()->hasRole('pm'))||
                Auth::user()->designation_id == 16 ||
                 Auth::user()->designation_id == 26 ){
                $userIdArray = array();
                $page       = $request->get('page');
                $authUser = Auth::user();
                $userName = new User();
                $company_id = '';
                if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
                    $company_id = $authUser->company_id;
                }
                $userNameList = $userName->getUserNameByCompany($company_id);

                $start_date = date("Y-m-d", strtotime($request->search_compoff_start_date));
                $end_date = date("Y-m-d", strtotime($request->search_compoff_end_date));
                $compOffData = array();
                $compoff = new CompOff();
                $compOffData = $compoff->getCompOffListForReport($userIdArray,$request);

                return view('pms.reports.comp-offreport', compact('companyList','compOffData','page','userNameList','request','start_date','end_date'));
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function lateComerReport(Request $request){
        if(Auth::check() && Auth::user()->hasPermission('lateComer.report')){
            $userObj = new User();
            $attendanceObj = new Attendance();
            $queryForDropDown  = $userObj->getUserwithRoleForLateComerReport();
            $page       = $request->get('page');
            $page_limit = ($request['page_range'])?$request['page_range']:10;

            if(Auth::check() && Auth::user()->hasRole('super_admin')
                    || Auth::user()->hasRole('hr')
                    || Auth::user()->hasRole('admin')
                    || Auth::user()->hasRole('tl')
                    || Auth::user()->hasRole('pm')
                    || Auth::user()->designation_id == 16
                    || Auth::user()->designation_id == 26
                ){
            $company_name='';
            $company_id = 0;
            if(Auth::check() && Auth::user()->hasRole('super_admin')){
                    if($request->has('search_submit') && $request->search_submit != '') {
                        if ($request->has('search_by_company') && $request->search_by_company != '') {
                                $company_name = Company::select('company_name')->where('id',$request->search_by_company)->first()->toArray()['company_name'];
                                $company_id = $request->search_by_company;
                        }
                    }
            }else{
                    $company_name = Company::select('company_name')->where('id',Auth::user()->company_id)->first()->toArray()['company_name'];
                    $company_id = Auth::user()->company_id;
                    $queryForDropDown->where('users.company_id','=',Auth::user()->company_id);
            }
            $teamMemberIds= '';
            if(Auth::check() && (Auth::user()->hasRole('tl'))||Auth::user()->designation_id == 16 || Auth::user()->designation_id == 26 ){
                    $teamMemberIds= array();
                    $teamArray = $queryForDropDown->where('users.reporting_to','=',Auth::user()->id);
                    $teamArray = $teamArray->get()->toArray();
                    foreach ($teamArray as $teamMemberId) {
                            array_push($teamMemberIds,$teamMemberId['employee_id']);
                    }
            }
            $employee_id='';
            if($request->has('search_submit') && $request->search_submit != '') {
                    if ($request->has('search_by_month') && $request->search_by_month != '') {
                        $month = $request->search_by_month;
                    }elseif($request->has('search_by_month') && $request->search_by_month == ''){
                            $month = Carbon::now()->month;
                    }
                    if ($request->has('search_by_year') && $request->search_by_year != '' ) {
                        $year = $request->search_by_year;
                    }elseif($request->has('search_by_year') && $request->search_by_year == '' ){
                            $year = Carbon::now()->year;
                    }
                    if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                        $employee_id = $request->search_by_employee;
                    }
                }else{
                    $month = Carbon::now()->month;
                    $year = Carbon::now()->year;
                }
            $userDeatailsDropDown = $queryForDropDown->orderBy('users.first_name', 'ASC')->get()->toArray();
            $company     	  = new Company();
            $companyList      = $company->getCompanyName();
            $now['month']     = Carbon::now()->month;
            $now['year']      = Carbon::now()->year;
            for($m=1; $m<=12; ++$m){$months[$m] = date('F', mktime(0, 0, 0, $m, 1));}//month's array
            for($y=Carbon::now()->year; $y>=Carbon::now()->year-2;--$y){$years[$y] = $y;}//years array

            $query= $attendanceObj->lateComerUsersObj($request, $company_name, $employee_id, $teamMemberIds ,$month, $year,$company_id);
            $lateComeData= $query->paginate($page_limit);
                    return view('pms.reports.latedaysreport', compact('request','lateComeData','page','companyList','months','years','now','userDeatailsDropDown'));
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
}

    public function projectReport(Request $request){
        if(Auth::check() && Auth::user()->hasPermission('project.report')){
            $company        = new Company();
            $companyList    = $company->getCompanyName();
            $page_limit     = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

            $authUser       = Auth::user();
            $companyId      = $authUser->company_id;
            $user           = new User();
            $userReq           = new User();
            $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
            $userListingForTL  = array();
            $team_user_id = array();
            $req_user_team_name = array();
            $reportingToList = $reportingId = array();
            $non_billable_projects = Project::where('is_non_billable',1)->pluck('id')->toArray();
            if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                $reportingList = User::select('id','first_name','last_name','reporting_to')
                                    ->where('company_id',Auth::user()->company_id)
                                    ->whereNull('deleted_at')
                                    ->where('status',1)->groupBy('id')->get()->toArray();
                                    // echo '<pre>'; print_r(reportingList)
                foreach ($reportingList as $key => $reportingListVal) {
                    if (!in_array($reportingListVal['reporting_to'], $reportingId)) {
                        $reportingId[] = $reportingListVal['reporting_to'];
                        $reportingToList[] = User::where('id', $reportingListVal['reporting_to'])->select('first_name', 'last_name', 'id')->first();
                    }
                }
                if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                    if($request->search_by_reporting_to != 0){
                        $req_user_team = $userReq->getMyTeamList($request->search_by_reporting_to);
                        foreach ($req_user_team as $key => $reqVal){
                            $team_user_id[$key] = $reqVal['id'];
                            $req_user_team_name[$reqVal['id']] = $reqVal['first_name'].' '.$reqVal['last_name'];
                        }
                    }
                }
            }
            if(Auth::user()->hasRole('pm') || Auth::user()->hasRole('tl')){
                $userListing = $user->getMyTeamList($loggedInUserID);
                $userListing[$loggedInUserID] = ['id' => Auth::user()->id,
                                                'first_name' => Auth::user()->first_name,
                                                'last_name' => Auth::user()->last_name];
            }else{
                if($companyId){
                    $user =   $user->where('company_id',$companyId);
                }
                if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                    $user =   $user->whereIn('users.department',($request->search_by_department));
                }
                if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                    $param = $request->search_by_internal_company_id;
                    if(in_array("parent_company",$param) && count($param) >= 2){
                        $user =   $user->where(function ($q) use ($param) {
                            $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                        });
                    } else if (in_array("parent_company",$param)) {
                        $user =   $user->WhereNull('internal_company_id');
                    } else {
                        $user =   $user->whereIn('internal_company_id', ($param));
                    }
                }
                $userListing = $user->where('users.status',1)->orderBy('first_name', 'ASC')->get();
                if(Auth::user()->hasRole('developer'))
                {
                    $userListing = $user->where('id',$authUser->id)->get();
                }
            }

            $teamUserArray = $teamUserName = array();
            foreach ($userListing as $userListingVal){
                $teamUserArray[] = $userListingVal['id'];
                $teamUserName[$userListingVal['id']] = $userListingVal['first_name'].' '.$userListingVal['last_name'];
            }

            $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                            ->select('cr.project_id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                            ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                            ->whereNull('cr.deleted_at');
            if($request->has('search_by_project_hours_type') && $request->search_by_project_hours_type != '')
            {
                if($request->search_by_project_hours_type == "billable_hours")
                {
                    $projectCrHours = $projectCrHours->whereNotIn('projects.id',$non_billable_projects);
                }
                if($request->search_by_project_hours_type == "non_billable_hours")
                {
                    $projectCrHours = $projectCrHours->whereIn('projects.id',$non_billable_projects);
                }
            }
            $projectCrHours= $projectCrHours->groupBy('cr.project_id')
                            ->pluck('total_cr_hours','cr.project_id')->toArray();
            if(Auth::check() && (Auth::user()->hasRole('super_admin') ||
                Auth::user()->hasRole('admin') ||
                Auth::user()->hasRole('pm')||
                Auth::user()->hasRole('tl') ||
                Auth::user()->hasRole('developer')) ||
                array_key_exists(4, config('constant.department'))) {
                $firstDate = date('Y-m-01');$lastDate = date('Y-m-t');
                 if ($request->has('search_by_start_date') && $request->search_by_start_date != '') {
                    $firstDate = $request->search_by_start_date;
                }
                if ($request->has('search_by_end_date') && $request->search_by_end_date != '') {
                    $lastDate = $request->search_by_end_date;
                }
                if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('developer')) {
                        $projectQuery = Project::leftJoin('project_entries as pe','projects.id','pe.project_id');
                                        if (Auth::user()->hasRole('admin')) {
                                            $projectQuery->where('projects.assigned_to',$companyId);
                                        }
                                    $projectQuery = $projectQuery->groupBy('projects.id')
                                            ->select('projects.id','projects.project_name');
                    }else{
                        $projectQuery = Project::leftJoin('project_entries as pe','projects.id','pe.project_id');
                        if(Auth::user()->department ==4 ){
                            $projectQuery = $projectQuery->where(function($q){
                                                $q->where('projects.account_manager', Auth::user()->id)
                                                    ->whereNotIN('projects.project_name',array('Miscellaneous Tasks','Bench'));
                                            });
                        } else {
                            $getAllTeamList = getTeamUserId($loggedInUserID, "all");
                            $getAllTeamList[] = $loggedInUserID;
                            $teamProjectEntry = ProjectEntry::whereIn("user_id",$getAllTeamList)->groupBy("project_id")->pluck("project_id")->toArray();
                            $projectQuery = $projectQuery->whereIn("projects.id",$teamProjectEntry);
                        }
                        $projectQuery = $projectQuery->groupBy('projects.id')
                                            ->select('projects.id','projects.project_name','projects.pms_hours');
                    }
                    $allproject = clone $projectQuery;
                    $projectData =  $allproject->orderBy('projects.project_name', 'asc')->get();
                    $projectQuery->where('pe.log_date',  '>=', $firstDate);
                    $projectQuery->where('pe.log_date',  '<=', $lastDate);
                    $projectQuery->whereIn('pe.user_id',  $teamUserArray);
                    if(Auth::user()->hasRole('developer')){
                        $projectQuery->where('pe.user_id',Auth::user()->id);
                    }
                    if ($request->has('search_submit') && $request->search_submit != '' || $request->submit_type == 'export_excel' ) {
                        if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                            $projectQuery->where('projects.id',$request->search_by_project_name);
                        }
                        if($request->has('search_by_project_hours_type') && $request->search_by_project_hours_type != '')
                        {
                            if($request->search_by_project_hours_type == "billable_hours")
                            {
                                $projectQuery = $projectQuery->whereNotIn('projects.id',$non_billable_projects);
                            }
                            if($request->search_by_project_hours_type == "non_billable_hours")
                            {
                                $projectQuery = $projectQuery->whereIn('projects.id',$non_billable_projects);
                            }
                        }
                        if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                            $projectQuery->where('pe.user_id',$request->search_by_user_name);
                        }

                        if ($request->has('search_project_status') && $request->search_project_status != '') {
                            if($request->search_project_status != 0){
                                $projectQuery->where('projects.project_status', $request->search_project_status);
                            }
                        }
                        if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                            if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                                if($request->search_by_reporting_to != 0){
                                    $projectQuery->whereIn('pe.user_id',$team_user_id);
                                }
                            }
                        }
                    }
                    if ($request->has('sort') && $request->input('sort') != '') {
                         $projData = $projectQuery->sortable()->orderBy($request->input('sort'), $request->input('direction'))->get();//->paginate($page_limit);
                     } else {
                         $projData = $projectQuery->sortable()->orderBy('projects.id', 'desc')->get();//->paginate($page_limit);
                    }
                    $projectReportArray = $projectLoggedArray = $projectEmpLoggedArray = $projectRowCount = $projectoldEntry = $projectEstimatedArray = array();

                    foreach($projData as $projectDataVal){
                        $loggedHours = ProjectEntry::leftjoin('users','users.id','user_id');
                        $AssignedHours = ProjectTasks::leftjoin('users','users.id','user_id');
                        $allLoggedHours = clone $loggedHours;
                        $allAssigndHours = clone $AssignedHours;
                        $empLoggedHours = ProjectTasks::leftjoin('project_entries', function($leftJoin)use($firstDate,$lastDate)
                                            {
                                                $leftJoin->on('project_tasks.id', '=','project_entries.task_id')
                                                        ->whereNull('project_entries.deleted_at')
                                                        ->where('project_entries.log_date',  '>=', $firstDate)
                                                        ->where('project_entries.log_date',  '<=', $lastDate);
                                            });
                        $oldEntry = ProjectEntry::where('project_id',$projectDataVal['id'])
                                                ->where('log_date',  '>=', $firstDate)
                                                ->where('log_date',  '<=', $lastDate)
                                                ->whereNull('deleted_at');
                        $allLoggedHours =  $allLoggedHours->select(DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                                                            ->where('project_id',$projectDataVal['id'])
                                                            ->groupBy('project_entries.project_id')
                                                            ->pluck('total_logged_hours');
                        $allAssigndHours =  $allAssigndHours->select(DB::raw("sum(project_tasks.`estimated_hours`) as total_estimated_hours"))
                                                            ->where('project_id',$projectDataVal['id'])
                                                            ->groupBy('project_tasks.project_id')
                                                            ->pluck('total_estimated_hours');
                                $loggedHours = $loggedHours->where('project_entries.log_date',  '>=', $firstDate)
                                                            ->where('project_entries.log_date',  '<=', $lastDate);
                                if(Auth::user()->hasRole('developer')){
                                    $loggedHours = $loggedHours->where('project_entries.user_id',Auth::user()->id);
                                }
                                if ($request->has('search_submit') && $request->search_submit != '' || $request->submit_type == 'export_excel' ) {
                                    if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                        $loggedHours = $loggedHours->where('user_id',$request->search_by_user_name);
                                    }
                                    if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                                        if($request->search_by_reporting_to != 0){
                                            $loggedHours = $loggedHours->whereIn('user_id',$team_user_id);
                                        }
                                    }
                                }
                                if ((!Auth::user()->hasRole('super_admin') && !Auth::user()->hasRole('admin') )&&
                                        ($projectDataVal['project_name'] == 'Miscellaneous Tasks' || $projectDataVal['project_name'] == 'Bench')){
                                        $loggedHours = $loggedHours->whereIn('user_id',$teamUserArray);
                                        $empLoggedHours = $empLoggedHours->whereIn('project_tasks.user_id',$teamUserArray);
                                        $oldEntry = $oldEntry->whereIn('user_id',$teamUserArray);
                                        if(Auth::user()->hasRole('tl')){
                                            $loggedHours = $loggedHours->whereIn('user_id',$teamUserArray);
                                            $empLoggedHours = $empLoggedHours->whereIn('project_tasks.user_id',$teamUserArray);
                                            $oldEntry = $oldEntry->whereIn('user_id',$teamUserArray);
                                        }
                                }
                                $loggedHours =  $loggedHours->where('project_id',$projectDataVal['id']);
                                $query_task_id = $loggedHours->pluck("task_id")->toArray();
                                $loggedHours =  $loggedHours->select('users.id',DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                                                            ->groupBy('project_entries.user_id')
                                                            ->pluck('total_logged_hours','users.id')->toArray();

                                if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                   $empLoggedHours =  $empLoggedHours->where('project_tasks.user_id',$request->search_by_user_name);
                                   $oldEntry = $oldEntry->where('user_id',$request->search_by_user_name);
                                }
                                if(Auth::user()->hasRole('developer')){
                                    $empLoggedHours =  $empLoggedHours->where('project_tasks.user_id',Auth::user()->id);
                                    $oldEntry = $oldEntry->where('user_id',Auth::user()->id);
                                }

                                if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                                    if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                                        if($request->search_by_reporting_to != 0){
                                            $empLoggedHours =  $empLoggedHours->whereIn('project_tasks.user_id',$team_user_id);
                                            $oldEntry = $oldEntry->whereIn('user_id',$team_user_id);
                                        }
                                    }
                                }

                                $empLoggedHours = $empLoggedHours->where('project_tasks.project_id',$projectDataVal['id'])->whereIn("project_tasks.id",$query_task_id)
                                                            // ->where(function($q)use($firstDate,$lastDate){
                                                            //     $q->where('task_start_date',  '>=', date("Y-m-01",strtotime($firstDate)))
                                                            //         ->where('task_end_date',  '<=', date("Y-m-t",strtotime($lastDate)));
                                                            // })
                                                            ->select('project_tasks.user_id as id','project_tasks.id as taskId',
                                                                    DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"),
                                                                    'project_tasks.task_name','project_tasks.estimated_hours as taskHours')
                                                            ->groupBy('project_tasks.user_id','project_tasks.id')
                                                            ->get()->toArray();
                                $oldEntry = $oldEntry->select('user_id as id',DB::raw("sum(`log_hours`) as total_logged_hours"))
                                            ->whereNull('task_id')->groupBy('user_id')
                                            ->get()->toArray();
                        $totalCount = 0 ;

                        if(isset($loggedHours) && !empty($loggedHours)){
                            $projectReportArray[$projectDataVal['id']] = $loggedHours;
                            $projectLoggedArray[$projectDataVal['id']] = $allLoggedHours;
                            $projectEmpLoggedArray[$projectDataVal['id']] =array_merge($empLoggedHours,$oldEntry);
                            $projectEstimatedArray[$projectDataVal['id']] = $allAssigndHours;
                            $totalCount = $totalCount + count($empLoggedHours) + count($oldEntry);
                            $projectRowCount[$projectDataVal['id']] = $totalCount;
                        }
                    }

                    if($request->submit_type == 'export_excel'){
                        return Excel::download(new ExportProjectReport($projectRowCount,$projectLoggedArray,$projectReportArray,$projectCrHours,$projectEmpLoggedArray,$request,$projectEstimatedArray), 'Project_report.xls');
                    }else{
                        return view('pms.reports.projectreport',compact('projectRowCount','projectLoggedArray','projectReportArray','projectEstimatedArray','projectEmpLoggedArray','projData','projectData','request','teamUserName','projectCrHours','reportingToList','req_user_team_name','non_billable_projects'));
                    }
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function getTaskDetails(Request $request) {
        $project_id =  $request->project_id;
        $user_id =  $request->user_id;
        $taskInfo = ProjectTasks::where('project_id',$project_id)->where('user_id',$user_id)
                ->select('estimated_hours','task_name')->orderBy('id', 'desc')->paginate(10);
         return view('pms.reports.modal.task_info',compact('taskInfo'));
    }

    public function projectHours(Request $request) {
        if(Auth::check() && Auth::user()->hasPermission('project.hours.report')){
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

            $authUser      = Auth::user();
            $companyId     = $authUser->company_id;
            $user          = new User();
            $userReq       = new User();
            $reportingToList = $reportingId = array();
            if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                $reportingList = User::select('id','first_name','last_name','reporting_to')
                                    ->where('company_id',Auth::user()->company_id)
                                    ->whereNull('deleted_at')
                                    ->where('status',1)->groupBy('id')->get()->toArray();
                foreach ($reportingList as $key => $reportingListVal) {
                    if (!in_array($reportingListVal['reporting_to'], $reportingId)) {
                        $reportingId[] = $reportingListVal['reporting_to'];
                        $reportingToList[] = User::where('id', $reportingListVal['reporting_to'])->select('first_name', 'last_name', 'id')->first();
                    }
                }
            }
            $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
            if(Auth::user()->hasRole('pm')|| Auth::user()->hasRole('tl')){
                $userListing = $user->getMyTeamList($loggedInUserID);
                $userListing[$loggedInUserID] = ['id' => Auth::user()->id,
                                                'first_name' => Auth::user()->first_name,
                                                'last_name' => Auth::user()->last_name];
            }else{
                if($companyId){
                    $user = $user->where('company_id',$companyId);
                }
                if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                    $user =   $user->whereIn('users.department',($request->search_by_department));
                }
                if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                    $param = $request->search_by_internal_company_id;
                    if(in_array("parent_company",$param) && count($param) >= 2){
                        $user =   $user->where(function ($q) use ($param) {
                            $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                        });
                    } else if (in_array("parent_company",$param)) {
                        $user =   $user->WhereNull('internal_company_id');
                    } else {
                        $user =   $user->whereIn('internal_company_id', ($param));
                    }
                }
                $userListing = $user->where('users.status',1)->orderBy('first_name', 'ASC')->get();
                if(Auth::user()->hasRole('developer'))
                {
                    $userListing = $user->where('id',$authUser->id)->get();
                }
            }
            $teamUserArray = $teamUserName =array();
            foreach ($userListing as $userListingVal){
                $teamUserArray[] = $userListingVal['id'];
                $teamUserName[$userListingVal['id']] = $userListingVal['first_name'].' '.$userListingVal['last_name'];
            }
            $dates = range('2016', date('Y'));
            $financialYear = array();
            foreach($dates as $date){
                // if (date('m', strtotime($date)) <= 1) {
                    // $year = ($date-1);
                // } else {
                    $year = $date;
                // }
               $financialYear[] = $year;
            }
            $non_billable_projects = Project::where('is_non_billable',1)->pluck('id')->toArray();
            if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('developer')) {
                    $projectQuery = Project::leftJoin('project_entries as pe','projects.id','pe.project_id');
                                    if (Auth::user()->hasRole('admin')) {
                                        $projectQuery->where('projects.assigned_to',$companyId);
                                    }
            }else{
                $projectQuery = Project::leftJoin('project_entries as pe','projects.id','pe.project_id');
                if(Auth::user()->department ==4 ){
                    $projectQuery = $projectQuery->where(function($q){
                                        $q->where('projects.account_manager', Auth::user()->id)
                                            ->whereNotIN('projects.project_name',array('Miscellaneous Tasks','Bench'));
                                    });
                }else{
                    $projectQuery = $projectQuery->where(function($q){
                                        $q->where('projects.created_by', Auth::user()->id)
                                        ->orWhere('projects.team_leader',Auth::user()->id)
                                        ->orWhere('projects.project_manager',Auth::user()->id)
                                        ->orwhere(function($q){
                                            $q->orwhere('projects.assigned_to',Auth::user()->company_id)
                                            ->where(function($q){
                                                    $q->where('projects.project_name','Miscellaneous Tasks')
                                                    ->orwhere('projects.project_name','Bench');
                                                });
                                        });
                                    });

                }

            }
            $projectQuery = $projectQuery->groupBy('projects.id')
                                    ->select('projects.id','projects.project_name');
            $allproject = clone $projectQuery;
            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                    $projectQuery->where('projects.id',$request->search_by_project_name);
                }
                if($request->has('search_by_project_hours_type') && $request->search_by_project_hours_type != '')
                {
                    if($request->search_by_project_hours_type == "billable_hours")
                    {
                        $projectQuery = $projectQuery->whereNotIn('projects.id',$non_billable_projects);
                    }
                    if($request->search_by_project_hours_type == "non_billable_hours")
                    {
                        $projectQuery = $projectQuery->whereIn('projects.id',$non_billable_projects);
                    }
                }
            }
            $projectData =  $allproject->orderBy('projects.project_name', 'asc')->get();
            $projData = $projectQuery->sortable()->orderBy('projects.id', 'desc')->get();
            $projectReportArray = $projectEstimatedArray = $projectHoursArray = $projectLoggedHours = $projectLoggedArray = array();
            foreach($projData as $projectDataVal){
                $loggedHours = ProjectEntry::select('project_entries.user_id','project_entries.log_date','pt.task_name',
                                    DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"),//,'pt.task_name',
                                        'pt.estimated_hours as taskHours')
                                ->leftjoin('project_tasks as pt','project_entries.task_id','pt.id')
                                ->leftjoin('users','users.id','project_entries.user_id')
                                ->where('users.status',1)->whereIn('project_entries.user_id',$teamUserArray);
                                if ($request->has('search_month') && $request->search_month != '') {
                                  $loggedHours =  $loggedHours->whereMonth('project_entries.log_date',$request->search_month);
                                }else{
                                    $loggedHours = $loggedHours->whereMonth('project_entries.log_date',date('m'));
                                }
                                if ($request->has('search_by_start_date') && $request->search_by_start_date != '') {
                                    $loggedHours = $loggedHours->where('project_entries.log_date',$request->search_by_start_date);
                                }
                                if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                    $loggedHours = $loggedHours->where('project_entries.user_id',$request->search_by_user_name);
                                }
                                if (Auth::user()->hasRole('developer')) {
                                    $loggedHours = $loggedHours->where('project_entries.user_id',Auth::user()->id);
                                }
                                if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                                    if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                                        if($request->search_by_reporting_to != 0){
                                            $req_user_team = $userReq->getMyTeamList($request->search_by_reporting_to);
                                            $team_user_id = array();
                                            foreach ($req_user_team as $key => $reqVal){
                                                $team_user_id[$key] = $reqVal['id'];
                                            }
                                            $loggedHours = $loggedHours->whereIn('project_entries.user_id',$team_user_id);
                                        }
                                    }
                                }
                                if ($request->has('financial_year') && $request->financial_year != '') {
                                    $loggedHours = $loggedHours->whereYear('project_entries.log_date',$request->financial_year);
                                }else{
                                    $loggedHours = $loggedHours->whereYear('project_entries.log_date',date('Y'));
                                }
                               if ((!Auth::user()->hasRole('super_admin') && !Auth::user()->hasRole('admin') )&&($projectDataVal['id'] == 458|| $projectDataVal['id'] == 516)) {
                                        $loggedHours = $loggedHours->whereIn('project_entries.user_id',$teamUserArray);
                                }
                $loggedHours = $loggedHours->where('project_entries.project_id',$projectDataVal['id'])
                                ->groupBy('project_entries.user_id','project_entries.task_id','project_entries.log_date')
                                ->orderBy('project_entries.log_date', 'desc')
                                ->get()->toArray();
                // $totalProjectHours = Project::where('id',$projectDataVal['id'])->pluck('pms_hours');
                $totalProjectHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                            ->select('cr.project_id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                            ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                            ->whereNull('cr.deleted_at')
                            ->where('projects.id',$projectDataVal['id'])
                            ->groupBy('cr.project_id')
                            ->pluck('total_cr_hours')->toArray();
                $totalAssigndHours = ProjectTasks::select(DB::raw("sum(project_tasks.`estimated_hours`) as total_estimated_hours"))
                                ->where('project_id',$projectDataVal['id'])
                                ->whereNull('project_tasks.deleted_at')
                                ->groupBy('project_tasks.project_id')
                                ->pluck('total_estimated_hours')->toArray();
                $totalLoggedHours = ProjectEntry::select(DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                                ->where('project_id',$projectDataVal['id'])
                                ->whereNull('project_entries.deleted_at')
                                ->groupBy('project_entries.project_id')
                                ->pluck('total_logged_hours')->toArray();
                if(!empty($loggedHours)){
                    $projectReportArray[$projectDataVal['id']] = $loggedHours;
                    $projectEstimatedArray[$projectDataVal['id']] = $totalAssigndHours;
                    $projectHoursArray[$projectDataVal['id']] = $totalProjectHours;
                    $projectLoggedArray[$projectDataVal['id']] = $totalLoggedHours;
                }
            }

            return view('pms.reports.projecthoursreport',compact('financialYear','request','projectData','teamUserName','projectReportArray','projectEstimatedArray','projectHoursArray','projectLoggedArray','reportingToList','non_billable_projects'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function detailLeaveReport(Request $request) {
        if(Auth::check() && Auth::user()->hasPermission('leave.report')){
            set_time_limit(0);
            $company  = new Company();
            $user = new User();
            $authUser = Auth::user();
            $company_id = '';
            if(!Auth::user()->hasRole('super_admin')){
                $company_id = $authUser->company_id;
            }

            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            if (Auth::check() && (Auth::user()->hasRole(config('constant.hr_slug'))) ||
                (Auth::user()->hasRole(config('constant.admin_slug'))) ||
                Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                   $userNameList = $user->getUserNameByCompany($company_id);
                }else{
                    // echo 'hii'; exit;
                    $userNameList = $user->getMyTeamList($authUser->id);

                }
            if(!empty($userNameList)){
                foreach ($userNameList as $userDetail) {
                    $userIdArray[] = $userDetail['id'];
                    $userNameArray[$userDetail['id']] = $userDetail['first_name'] . ' ' . $userDetail['last_name'];

                }
            }
            $userIdArray[] = $authUser->id;
            $userNameArray[$authUser->id] = $authUser->first_name . ' ' .$authUser->last_name;
            // echo "<pre>";print_R($userNameArray);exit;
            $leavesData = array();
            $leave = User::leftjoin('companies','users.company_id','companies.id')->whereIn('users.id',$userIdArray)
                    ->select(DB::raw("CONCAT(users.first_name,' ',users.last_name)  as 'emp_name'"),'users.id','users.employee_id','users.available_leaves',
                            'companies.emp_short_name','users.used_leaves','users.confirmation_date','users.internal_company_id','users.designation_id');
            
            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                        $leave->whereIn('users.department',($request->search_by_department));
                    }
                if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                    $param = $request->search_by_internal_company_id;
                    if(in_array("parent_company",$param) && count($param) >= 2){
                        $leave->where(function ($q) use ($param) {
                            $q->whereIn('users.internal_company_id', ($param))->orWhereNull('internal_company_id');
                        });
                    } else if (in_array("parent_company",$param)) {
                        $leave->WhereNull('users.internal_company_id');
                    } else {
                        $leave->whereIn('users.internal_company_id', ($param));
                    }
                }
                if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                    $leave->where('users.id',$request->search_by_employee);
                }
            }
            if ($request->has('search_by_status') && $request->search_by_status != '') {
                $leave->where('users.status',$request->search_by_status);
            }else{
                $leave->where('users.status',1);
            }
            if ($request->has('sort') && $request->input('sort') != '') {
                $leavesData = $leave->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $leavesData = $leave->sortable()->orderBy('emp_name', 'asc')->paginate($page_limit);
            }

            return view('pms.reports.detail_leavereport',compact('userNameArray','leavesData','request'));

        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function updateLeave(Request $request) {
        if (Auth::check() && (Auth::user()->hasRole(config('constant.hr_slug'))) || (Auth::user()->hasRole(config('constant.admin_slug'))) || Auth::user()->hasRole(config('constant.superadmin_slug'))) {
            $userId = $request->userId;
            $leaveType = $request->leaveType;
            $newVal = $request->newVal;
            $user = User::find($userId);
            if($leaveType == 'edit_available_leaves_save'){
                $user->available_leaves =  $newVal;
            }
            if($leaveType == 'edit_used_leaves_save'){
                $user->used_leaves = $newVal;
            }
            $user->save();
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function monthlyLeaveReport(Request $request) {
        if(Auth::check() && Auth::user()->hasPermission('monthly.report')){
            $company     	  = new Company();
            $holiday          = new Holiday();
            $companyList      = $company->getCompanyName();
            $now['month']     = Carbon::now()->month;
            $now['year']      = Carbon::now()->year;

            $comapny_id = Auth::user()->company_id;
            if(Auth::user()->hasRole(config('constant.superadmin_slug')) && $request->has('search_submit') && $request->search_submit != ''){
                $comapny_id = $request->search_by_company;
            }
            $companyName = $company->getCompanyNameById($comapny_id)->company_name;

            for($m=1; $m<=12; ++$m){$months[$m] = date('F', mktime(0, 0, 0, $m, 1));}//month's array
            for($y=Carbon::now()->year; $y>=Carbon::now()->year-2;--$y){$years[$y] = $y;}//years array
            $weekOff = 0;
            if ($request->has('search_submit') && $request->search_submit != '') {
                $serachMonth = $request->search_by_month;
                $serachYear = $request->search_by_year;
                $firstDate = date("Y-m-01",strtotime("$serachYear-$serachMonth-01"));
                $lastDate = date('Y-m-t',strtotime("$serachYear-$serachMonth-01"));
                $start_date = "01-".$serachMonth."-".$serachYear;
                $start_time = strtotime($start_date);

                $end_time = strtotime("+1 month", $start_time);

                for($i=$start_time; $i<$end_time; $i+=86400)
                {
                    $dates[] = date('Y-m-d', $i);

                    $isWeekend = (new \App\Helpers\CommonHelper)->is_holiday(date('Y-m-d', $i));

                    $days[] = date('D', $i);
                    // if((date('D', $i)== 'Sat') || (date('D', $i) == 'Sun') )
                    if($isWeekend == true)
                    {
                      $weekOff++;
                    }

                }


                if(isset($request->search_report_type) && $request->search_report_type == "time_sheet_report"){
                    $getUserList = User::where('users.company_id', $comapny_id)->whereNull('deleted_at');

                    if($comapny_id == 2)
                    {
                        $getUserList = $getUserList->whereNotIn('employee_id',array('000','004'));
                    }
                    if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                        $getUserList->whereIn('users.department',($request->search_by_department));
                    }
                    if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                        $param = $request->search_by_internal_company_id;
                        if(in_array("parent_company",$param) && count($param) >= 2){
                            $getUserList->where(function ($q) use ($param) {
                                $q->whereIn('users.internal_company_id', ($param))->orWhereNull('internal_company_id');
                            });
                        } else if (in_array("parent_company",$param)) {
                            $getUserList->WhereNull('users.internal_company_id');
                        } else {
                            $getUserList->whereIn('users.internal_company_id', ($param));
                        }
                    }
                    $getUserList =  $getUserList->where('want_to_exclude_for_salary',0)->where('status',1)->orderBy('users.employee_id','asc')->get();
                    return Excel::download(new ExportTimeSheet($dates,$days,$getUserList,$companyName), 'time_sheet_report.xls');
                }

                $excelData = $userLeaveData =  array();
                $getUserList = User::where('users.company_id', $comapny_id)->whereNull('deleted_at');
                if($comapny_id == 2)
                {
                    $getUserList = $getUserList->whereNotIn('employee_id',array('000','004'));
                }
                if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                    $getUserList->whereIn('users.department',($request->search_by_department));
                }
                if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                    $param = $request->search_by_internal_company_id;
                    if(in_array("parent_company",$param) && count($param) >= 2){
                        $getUserList->where(function ($q) use ($param) {
                            $q->whereIn('users.internal_company_id', ($param))->orWhereNull('internal_company_id');
                        });
                    } else if (in_array("parent_company",$param)) {
                        $getUserList->WhereNull('users.internal_company_id');
                    } else {
                        $getUserList->whereIn('users.internal_company_id', ($param));
                    }
                }
                $getUserList =  $getUserList->orderBy('users.employee_id','asc')->pluck('employee_id');
                foreach($getUserList as $val){
                    $getLeave = Attendance::leftJoin('users', 'attendances.emp_code', 'users.employee_id')
                                        ->leftJoin('companies', 'users.company_id','companies.id' )
                                        ->where('attendances.emp_comp', $companyName)
                                        ->where('users.company_id', $comapny_id)
                                        ->where('users.employee_id',$val)
                                        ->whereMonth('attendances.entry_date',  sprintf("%02d",$serachMonth))
                                        ->whereYear('attendances.entry_date', $serachYear)
                                        ->whereNull('attendances.deleted_at')
                                        ->select(DB::raw('CONCAT(users.first_name," ",users.last_name) as name'),
                                                    DB::raw('CONCAT(companies.emp_short_name,users.employee_id) as empCode'),
                                                    'attendances.full_day','attendances.half_day','attendances.incorrect_entry','attendances.absent'
                                                ,'attendances.entry_date')
                                        ->get()->toArray();


                    if(count($getLeave)>0){
                        foreach($getLeave as  $v){
                            $excelData[$val]['name'] = $v['name'];
                            $excelData[$val]['empCode'] = $v['empCode'];
                            $excelData[$val][$v['entry_date']] = $v;
                        }
                    }
                    $getCreatedLeave = User::leftjoin('comp_offs as cf', function($leftJoin)use($serachMonth,$serachYear)
                                            {
                                                $leftJoin->on('users.id', '=','cf.user_id')
                                                        ->whereNull('cf.deleted_at')
                                                        ->where('cf.compoff_marked_as',2)
                                                        ->whereMonth('cf.compoff_start_date', sprintf("%02d",$serachMonth))
                                                        ->whereYear('cf.compoff_start_date', $serachYear);
                                            })
                                        ->where('employee_id',$val)
                                        ->where('users.company_id', $comapny_id)
                                        ->select(DB::raw('SUM(cf.compoff_days) AS compoffDay'),'users.employee_id','users.id as uid',
                                                    'users.designation_id','users.employee_status','users.confirmation_date','users.employee_join_as','users.joining_date')
                                        ->orderBy('users.id')
                                        ->get();

                    if(count($getCreatedLeave)>0){
                        foreach($getCreatedLeave as  $userData){
                            $geFinancialYearStartDate = getUsersFinancialYearStartDate($userData['uid']);
                            $userLeaveData[$val]['compoffDay'] = $userData['compoffDay'];

                            $userData['available_leaves'] = getUsersAvailableLeave(["where" =>["start_date" => "$serachYear-$serachMonth-01", "id" => $userData['uid']]]);

                            if(strtotime(date("Y-m",strtotime($userData['confirmation_date']))) > strtotime(date("Y-m",strtotime("$serachYear-$serachMonth-01")))){
                                $userData['count'] = getLeaveCount(['column' => "leave_days","whereLessOrEqual"=>["column" => "leave_date","query" => date("Y-m-t",strtotime($firstDate." -1 day"))],"where" =>["leave_status"=>1,"user_id"=>$userData['uid']]]);
                            } else {
                                $userData['count'] = getLeaveCount(['column' => "leave_days","whereBetween"=>["column" => "leave_date","query" => [$geFinancialYearStartDate,date("Y-m-t",strtotime($firstDate." -1 day"))]],"whereNull" => "deleted_at", "where" =>["leave_status"=>1,"user_id"=>$userData['uid']]]);
                            }

                            $userLeaveData[$val]['creditLeave'] = $userData['available_leaves'] - $userData['count'];
                            if(isset($userData['joining_date']) && $userData['joining_date'] != "" && strtotime(date('Y-m',strtotime($userData['joining_date']))) == strtotime(date('Y-m',strtotime("$serachYear-$serachMonth")))) {
                                $userLeaveData[$val]['creditLeave'] = $userData['available_leaves'];
                            }
                            if($userLeaveData[$val]['creditLeave']<=0){
                                $userLeaveData[$val]['creditLeave']=0;
                            }
                            $leaveBalance = MonthlyLeaveBalance::where(["user_id" => $userData['uid'], "year" => $serachYear, "month" => $serachMonth])->first();
                            if(isset($leaveBalance->leave_balance) && $leaveBalance->leave_balance != ""){
                                $userLeaveData[$val]['creditLeave'] = $leaveBalance->leave_balance;
                            } else {
                                $compOffData = CompOff::where(["user_id" => $userData['uid'], 'compoff_status' => 1,'compoff_marked_as' => 2])->where("compoff_start_date",">=",$geFinancialYearStartDate)->where("compoff_end_date","<=",date("Y-m-t",strtotime($firstDate." -1 day")))->get();
                                $userLeaveData[$val]['creditLeave'] = $userLeaveData[$val]['creditLeave'] + getUsersCompoffData($compOffData);
                            }
                        }
                    }
                }
                return Excel::download(new ExportMonthlyLeave($dates,$days,$weekOff,$excelData,$userLeaveData), 'Monthly_leave_report.xls');
            }else{
                return view('pms.reports.monthly_leavereport',compact('months','years','request','now','companyList'));
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function projectCalculation(Request $request){
        if(Auth::check() && Auth::user()->hasPermission('project.report') && (Auth::user()->hasRole(config('constant.superadmin_slug')) ||
                                        Auth::user()->hasRole(config('constant.admin_slug')) ||
                                        Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                                        Auth::user()->hasRole(config('constant.team_leader_slug')))){
            $company        = new Company();
            $companyList    = $company->getCompanyName();
            $page_limit     = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

            $authUser       = Auth::user();
            $companyId      = $authUser->company_id;
            $user           = new User();
            $userReq           = new User();
            $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
            $userListingForTL  = array();
            $team_user_id = array();
            $req_user_team_name = array();
            $reportingToList = $reportingId = array();
            $non_billable_projects = Project::where('is_non_billable',1)->pluck('id')->toArray();
            if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                $reportingList = User::select('id','first_name','last_name','reporting_to')
                                    ->where('company_id',Auth::user()->company_id)
                                    ->whereNull('deleted_at')
                                    ->where('status',1)->groupBy('id')->get()->toArray();
                                    // echo '<pre>'; print_r(reportingList)
                foreach ($reportingList as $key => $reportingListVal) {
                    if (!in_array($reportingListVal['reporting_to'], $reportingId)) {
                        $reportingId[] = $reportingListVal['reporting_to'];
                        $reportingToList[] = User::where('id', $reportingListVal['reporting_to'])->select('first_name', 'last_name', 'id')->first();
                    }
                }
                if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                    if($request->search_by_reporting_to != 0){
                        $req_user_team = $userReq->getMyTeamList($request->search_by_reporting_to);
                        foreach ($req_user_team as $key => $reqVal){
                            $team_user_id[$key] = $reqVal['id'];
                            $req_user_team_name[$reqVal['id']] = $reqVal['first_name'].' '.$reqVal['last_name'];
                        }
                    }
                }
            }
            if(Auth::user()->hasRole('pm') || Auth::user()->hasRole('tl')){
                $userListing = $user->getMyTeamList($loggedInUserID);
                $userListing[$loggedInUserID] = ['id' => Auth::user()->id,
                                                'first_name' => Auth::user()->first_name,
                                                'last_name' => Auth::user()->last_name];
            }else{
                if($companyId){
                    $user =   $user->where('company_id',$companyId);
                }
                if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                    $user =   $user->whereIn('users.department',($request->search_by_department));
                }
                if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                    $param = $request->search_by_internal_company_id;
                    if(in_array("parent_company",$param) && count($param) >= 2){
                        $user =   $user->where(function ($q) use ($param) {
                            $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                        });
                    } else if (in_array("parent_company",$param)) {
                        $user =   $user->WhereNull('internal_company_id');
                    } else {
                        $user =   $user->whereIn('internal_company_id', ($param));
                    }
                }
                $userListing = $user->where('users.status',1)->orderBy('first_name', 'ASC')->get();
                if(Auth::user()->hasRole('developer'))
                {
                    $userListing = $user->where('id',$authUser->id)->get();
                }
            }

            $teamUserArray = $teamUserName = array();
            foreach ($userListing as $userListingVal){
                $teamUserArray[] = $userListingVal['id'];
                $teamUserName[$userListingVal['id']] = $userListingVal['first_name'].' '.$userListingVal['last_name'];
            }

            $projectCrHours = Project::leftjoin('cr','projects.id', '=', 'cr.project_id')
                            ->select('cr.project_id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                            ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                            ->whereNull('cr.deleted_at');
            if($request->has('search_by_project_hours_type') && $request->search_by_project_hours_type != '')
            {
                if($request->search_by_project_hours_type == "billable_hours")
                {
                    $projectCrHours = $projectCrHours->whereNotIn('projects.id',$non_billable_projects);
                }
                if($request->search_by_project_hours_type == "non_billable_hours")
                {
                    $projectCrHours = $projectCrHours->whereIn('projects.id',$non_billable_projects);
                }
            }
            $projectCrHours= $projectCrHours->groupBy('cr.project_id')
                            ->pluck('total_cr_hours','cr.project_id')->toArray();
            if(Auth::check() && (Auth::user()->hasRole('super_admin') ||
                Auth::user()->hasRole('admin') ||
                Auth::user()->hasRole('pm')||
                Auth::user()->hasRole('tl') ||
                Auth::user()->hasRole('developer')) ||
                array_key_exists(4, config('constant.department'))) {
                $firstDate = date('Y-m-01');$lastDate = date('Y-m-t');
                 if ($request->has('search_by_start_date') && $request->search_by_start_date != '') {
                    $firstDate = $request->search_by_start_date;
                }
                if ($request->has('search_by_end_date') && $request->search_by_end_date != '') {
                    $lastDate = $request->search_by_end_date;
                }
                if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('developer')) {
                        $projectQuery = Project::leftJoin('project_entries as pe','projects.id','pe.project_id');
                                        if (Auth::user()->hasRole('admin')) {
                                            $projectQuery->where('projects.assigned_to',$companyId);
                                        }
                                    $projectQuery = $projectQuery->groupBy('projects.id')
                                            ->select('projects.id','projects.project_name');
                    }else{
                        $projectQuery = Project::leftJoin('project_entries as pe','projects.id','pe.project_id');
                        if(Auth::user()->department ==4 ){
                            $projectQuery = $projectQuery->where(function($q){
                                                $q->where('projects.account_manager', Auth::user()->id)
                                                    ->whereNotIN('projects.project_name',array('Miscellaneous Tasks','Bench'));
                                            });
                        } else {
                            $getAllTeamList = getTeamUserId($loggedInUserID, "all");
                            $getAllTeamList[] = $loggedInUserID;
                            $teamProjectEntry = ProjectEntry::whereIn("user_id",$getAllTeamList)->groupBy("project_id")->pluck("project_id")->toArray();
                            $projectQuery = $projectQuery->whereIn("projects.id",$teamProjectEntry);
                        }
                        $projectQuery = $projectQuery->groupBy('projects.id')
                                            ->select('projects.id','projects.project_name','projects.pms_hours');
                    }
                    $allproject = clone $projectQuery;
                    $projectData =  $allproject->orderBy('projects.project_name', 'asc')->get();
                    $projectQuery->where('pe.log_date',  '>=', $firstDate);
                    $projectQuery->where('pe.log_date',  '<=', $lastDate);
                    $projectQuery->whereIn('pe.user_id',  $teamUserArray);
                    if(Auth::user()->hasRole('developer')){
                        $projectQuery->where('pe.user_id',Auth::user()->id);
                    }
                    if ($request->has('search_submit') && $request->search_submit != '' || $request->submit_type == 'export_excel' ) {
                        if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                            $projectQuery->where('projects.id',$request->search_by_project_name);
                        }
                        if($request->has('search_by_project_hours_type') && $request->search_by_project_hours_type != '')
                        {
                            if($request->search_by_project_hours_type == "billable_hours")
                            {
                                $projectQuery = $projectQuery->whereNotIn('projects.id',$non_billable_projects);
                            }
                            if($request->search_by_project_hours_type == "non_billable_hours")
                            {
                                $projectQuery = $projectQuery->whereIn('projects.id',$non_billable_projects);
                            }
                        }
                        if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                            $projectQuery->where('pe.user_id',$request->search_by_user_name);
                        }

                        if ($request->has('search_project_status') && $request->search_project_status != '') {
                            if($request->search_project_status != 0){
                                $projectQuery->where('projects.project_status', $request->search_project_status);
                            }
                        }
                        if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                            if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                                if($request->search_by_reporting_to != 0){
                                    $projectQuery->whereIn('pe.user_id',$team_user_id);
                                }
                            }
                        }
                    }
                    if ($request->has('sort') && $request->input('sort') != '') {
                         $projData = $projectQuery->sortable()->orderBy($request->input('sort'), $request->input('direction'))->get();//->paginate($page_limit);
                     } else {
                         $projData = $projectQuery->sortable()->orderBy('projects.id', 'desc')->get();//->paginate($page_limit);
                    }
                    $projectReportArray = $projectLoggedArray = $projectEmpLoggedArray = $projectRowCount = $projectoldEntry = $projectEstimatedArray = array();

                    foreach($projData as $projectDataVal){
                        $loggedHours = ProjectEntry::with(["projectDetails" => function($q) { $q->with("projecttype");},"users" => function($q) {$q->select(DB::raw('CONCAT(first_name," ",last_name) AS user_name'),'id');}])->leftjoin('users','users.id','user_id');
                        $AssignedHours = ProjectTasks::leftjoin('users','users.id','user_id');
                        $allLoggedHours = clone $loggedHours;
                        $allAssigndHours = clone $AssignedHours;
                        $empLoggedHours = ProjectTasks::with(["users" => function($q) {$q->select(DB::raw('CONCAT(first_name," ",last_name) AS user_name'),'id');}])->leftjoin('project_entries', function($leftJoin)use($firstDate,$lastDate)
                                            {
                                                $leftJoin->on('project_tasks.id', '=','project_entries.task_id')
                                                        ->whereNull('project_entries.deleted_at')
                                                        ->where('project_entries.log_date',  '>=', $firstDate)
                                                        ->where('project_entries.log_date',  '<=', $lastDate);
                                            });
                        $oldEntry = ProjectEntry::where('project_id',$projectDataVal['id'])
                                                ->where('log_date',  '>=', $firstDate)
                                                ->where('log_date',  '<=', $lastDate)
                                                ->whereNull('deleted_at');
                        $allLoggedHours =  $allLoggedHours->select(DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                                                            ->where('project_id',$projectDataVal['id'])
                                                            ->groupBy('project_entries.project_id')
                                                            ->pluck('total_logged_hours');
                        $allAssigndHours =  $allAssigndHours->select(DB::raw("sum(project_tasks.`estimated_hours`) as total_estimated_hours"))
                                                            ->where('project_id',$projectDataVal['id'])
                                                            ->groupBy('project_tasks.project_id')
                                                            ->pluck('total_estimated_hours');
                                $loggedHours = $loggedHours->where('project_entries.log_date',  '>=', $firstDate)
                                                            ->where('project_entries.log_date',  '<=', $lastDate);
                                if(Auth::user()->hasRole('developer')){
                                    $loggedHours = $loggedHours->where('project_entries.user_id',Auth::user()->id);
                                }
                                if ($request->has('search_submit') && $request->search_submit != '' || $request->submit_type == 'export_excel' ) {
                                    if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                        $loggedHours = $loggedHours->where('user_id',$request->search_by_user_name);
                                    }
                                    if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                                        if($request->search_by_reporting_to != 0){
                                            $loggedHours = $loggedHours->whereIn('user_id',$team_user_id);
                                        }
                                    }
                                }
                                if ((!Auth::user()->hasRole('super_admin') && !Auth::user()->hasRole('admin') )&&
                                        ($projectDataVal['project_name'] == 'Miscellaneous Tasks' || $projectDataVal['project_name'] == 'Bench')){
                                        $loggedHours = $loggedHours->whereIn('user_id',$teamUserArray);
                                        $empLoggedHours = $empLoggedHours->whereIn('project_tasks.user_id',$teamUserArray);
                                        $oldEntry = $oldEntry->whereIn('user_id',$teamUserArray);
                                        if(Auth::user()->hasRole('tl')){
                                            $loggedHours = $loggedHours->whereIn('user_id',$teamUserArray);
                                            $empLoggedHours = $empLoggedHours->whereIn('project_tasks.user_id',$teamUserArray);
                                            $oldEntry = $oldEntry->whereIn('user_id',$teamUserArray);
                                        }
                                }
                                $loggedHours =  $loggedHours->where('project_id',$projectDataVal['id']);
                                $query_task_id = $loggedHours->pluck("task_id")->toArray();
                                $loggedHours =  $loggedHours->select('users.id',"project_entries.user_id",'project_id',DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"))
                                                            ->groupBy('project_entries.user_id');

                                if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                                   $empLoggedHours =  $empLoggedHours->where('project_tasks.user_id',$request->search_by_user_name);
                                   $oldEntry = $oldEntry->where('user_id',$request->search_by_user_name);
                                }
                                if(Auth::user()->hasRole('developer')){
                                    $empLoggedHours =  $empLoggedHours->where('project_tasks.user_id',Auth::user()->id);
                                    $oldEntry = $oldEntry->where('user_id',Auth::user()->id);
                                }

                                if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                                    if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                                        if($request->search_by_reporting_to != 0){
                                            $empLoggedHours =  $empLoggedHours->whereIn('project_tasks.user_id',$team_user_id);
                                            $oldEntry = $oldEntry->whereIn('user_id',$team_user_id);
                                        }
                                    }
                                }

                                $empLoggedHours = $empLoggedHours->where('project_tasks.project_id',$projectDataVal['id'])->whereIn("project_tasks.id",$query_task_id)
                                                            // ->where(function($q)use($firstDate,$lastDate){
                                                            //     $q->where('task_start_date',  '>=', date("Y-m-01",strtotime($firstDate)))
                                                            //         ->where('task_end_date',  '<=', date("Y-m-t",strtotime($lastDate)));
                                                            // })
                                                            ->select('project_tasks.user_id as id','project_tasks.user_id','project_tasks.id as taskId',
                                                                    DB::raw("sum(project_entries.`log_hours`) as total_logged_hours"),
                                                                    'project_tasks.task_name','project_tasks.estimated_hours as taskHours')
                                                            ->groupBy('project_tasks.user_id','project_tasks.id')
                                                            ->get()->toArray();
                                $oldEntry = $oldEntry->select('user_id as id',DB::raw("sum(`log_hours`) as total_logged_hours"))
                                            ->whereNull('task_id')->groupBy('user_id')
                                            ->get()->toArray();
                        $totalCount = 0 ;

                        if(isset($loggedHours) && count($loggedHours->get()->toArray()) > 0){
                            $projectReportArray[$projectDataVal['id']] = [$loggedHours->pluck('total_logged_hours','users.id')->toArray(), $loggedHours->first()->toArray()];
                            $projectLoggedArray[$projectDataVal['id']] = $allLoggedHours;
                            $projectEmpLoggedArray[$projectDataVal['id']] =array_merge($empLoggedHours,$oldEntry);
                            $projectEstimatedArray[$projectDataVal['id']] = $allAssigndHours;
                            $totalCount = $totalCount + count($empLoggedHours) + count($oldEntry);
                            $projectRowCount[$projectDataVal['id']] = $totalCount;
                        }
                    }
                    if($request->submit_type == 'export_excel'){
                        return Excel::download(new ExportProjectReport($projectRowCount,$projectLoggedArray,$projectReportArray,$projectCrHours,$projectEmpLoggedArray,$request,$projectEstimatedArray), 'Project_report.xls');
                    }else{
                        return view('pms.project_calculation.project_calculation',compact('projectRowCount','projectLoggedArray','projectReportArray','projectEstimatedArray','projectEmpLoggedArray','projData','projectData','request','teamUserName','projectCrHours','reportingToList','req_user_team_name','non_billable_projects'));
                    }
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function syncLeaveBalances(Request $request) {
        if(isset($request->search_by_month) && $request->search_by_month != "" && isset($request->search_by_year) && $request->search_by_year != ""){
            if (Auth::check() && (Auth::user()->hasRole(config('constant.hr_slug'))) || (Auth::user()->hasRole(config('constant.admin_slug'))) || Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                $array = [
                    "month" => $request->search_by_month,
                    "year" => $request->search_by_year
                ];
                $date = date("Y-m",strtotime($array['year']."-".$array['month']."-01"));
                if($date > date("Y-m")) {
                    return response()->json(['status' => "error", 'message' => 'You can\'t sync future leave!']);
                } else if ($date < date("Y-m",strtotime("2022-04-01"))) {
                    return response()->json(['status' => "error", 'message' => 'You can\'t sync future leave before 1\'st April 2022!']);
                } else {
                    syncMontlyBalanceLeave($array);
                    return response()->json(['status' => "success", 'message' => 'Leave synchronization Successfully!']);
                }
            }else{
                return response()->json(['status' => "error", 'message' => 'You do not have permission to perform this action!']);
            }
        } else {
            return response()->json(['status' => "error", 'message' => 'Please select proper month and year!']);
        }
    }

    public function billableReport(Request $request) {
        if(Auth::check() && Auth::user()->hasPermission('project.hours.report')){
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $authUser      = Auth::user();
            $designation = new Designation();
            $designationInclude = config('constant.designation');
            $companyId     = $authUser->company_id;
            $user          = new User();
            $userReq       = new User();
            $data['reporting_to_list'] = $data['reporting_id'] = array();
            $firstDate = date('Y-m-01');
            $lastDate = date('Y-m-t');

            if ($request->has('search_by_start_date') && $request->search_by_start_date != '') {
                $firstDate = $request->search_by_start_date = date('Y-m-01',strtotime($request->search_by_start_date));
            }

            if ($request->has('search_by_end_date') && $request->search_by_end_date != '') {
                $lastDate = $request->search_by_end_date = date('Y-m-t',strtotime($request->search_by_end_date));
            }

            if (Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('admin') ) {
                $reportingList = User::select('id','first_name','last_name','reporting_to')
                                    ->where('company_id',Auth::user()->company_id)
                                    ->whereNull('deleted_at')
                                    ->where('status',1)->groupBy('id')->get()->toArray();
                foreach ($reportingList as $key => $reportingListVal) {
                    if (!in_array($reportingListVal['reporting_to'], $data['reporting_id'])) {
                        $data['reporting_id'][] = $reportingListVal['reporting_to'];
                        $data['reporting_to_list'][] = User::where('id', $reportingListVal['reporting_to'])->select('first_name', 'last_name', 'id')->first();
                    }
                }

            }
            
            $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;

            if(Auth::user()->hasRole('pm')|| Auth::user()->hasRole('tl')){

                $userListing = $user->getMyTeamList($loggedInUserID);
                $userListing[$loggedInUserID] = ['id' => Auth::user()->id,
                                                'first_name' => Auth::user()->first_name,
                                                'last_name' => Auth::user()->last_name];
            } else {
                if($companyId) {
                    $user = $user->where('company_id',$companyId);
                }
                // if ($request->has('search_by_department') && count($request->search_by_department) > 0 ) {
                //     $user =   $user->whereIn('users.department',($request->search_by_department));
                // }
                if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                    $param = $request->search_by_internal_company_id;
                    if(in_array("parent_company",$param) && count($param) >= 2){
                        $user =   $user->where(function ($q) use ($param) {
                            $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                        });
                    } else if (in_array("parent_company",$param)) {
                        $user =   $user->WhereNull('internal_company_id');
                    } else {
                        $user =   $user->whereIn('internal_company_id', ($param));
                    }
                }
                
                $userListing = $user->where('users.status',1)->orderBy('first_name', 'ASC')->get();
            }

            $data['team_user_array'] = $data['team_user_name'] = $designation_id = array();
            foreach ($userListing as $userListingVal) {
                $data['team_user_array'][] = $userListingVal['id'];
                $data['team_user_name'][$userListingVal['id']] = $userListingVal['first_name'].' '.$userListingVal['last_name'];
                if(isset($userListingVal['designation_id'])){
                     $designation_id[] = $userListingVal['designation_id'];
                }
            }
            $designationList = Designation::whereIn('id', array_unique($designation_id))->get();

           $query = User::with('company')
                ->leftJoin("billable_reports", "billable_reports.user_id","users.id")
                ->whereBetween('start_date',[$firstDate, $lastDate])->whereBetween('start_date',[$firstDate, $lastDate]);
                if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                    $query->where('users.id',($request->search_by_user_name));
                }
                if (Auth::check() && (Auth::user()->hasRole('admin') ||Auth::user()->hasRole('hr') || Auth::user()->hasRole(config('constant.superadmin_slug')))) {
                    if ($request->has('search_designation') && $request->search_designation != '') {
                        $query->whereIn('designation_id', ($request->search_designation));
                    } else {
                        $query->whereIn('designation_id', $designationInclude);
                    }
                }
                $query->whereIn('users.id', $data['team_user_array']);
                if (($request->has('search_by_reporting_to')) && $request->search_by_reporting_to != '') {
                    $user          = new User();
                    $userListing = $user->getMyTeamList($request->search_by_reporting_to);
                    $teamUserArray = $teamUserName = array();
                    foreach ($userListing as $userListingVal){
                        $teamUserArray[] = $userListingVal['id'];
                    }
                    $query->whereIn('users.id',$teamUserArray);
                }
                $query->select("users.*",DB::raw("SUM(IFNULL(`billable_hours`,0)) as billable_hour"),DB::raw("SUM(IFNULL(`logged_hours`,0)) as logged_hours"),DB::raw("SUM(IFNULL(`non_billable_hours`,0)) as non_billable_hours"),DB::raw("SUM(IFNULL(`non_billable_logged_hours`,0)) as non_billable_logged_hours"),DB::raw("SUM(IFNULL(`working_days`,0)) as working_days"), 
                    DB::raw("if((SUM(IFNULL(`working_days`,0)) * 8) > 0, (SUM(IFNULL(`billable_hours`,0)) * 100) / (SUM(IFNULL(`working_days`,0)) * 8) , 0) as billability_percentage")
                )->groupBy('user_id');

                if(!($request->has('sort'))){
                    $query->orderBy("billable_hours",'desc');
                }
                $query = $query->sortable();
            if($request->submit_type == 'export_excel'){
                $data['result'] = $query->get()->toArray();
                $request->submit_type = '';
                return Excel::download(new ExportBillableHoursReport($data), 'Employee_Billable_Report_'.date("F, dS Y",strtotime($firstDate)).' to '.date("F, dS Y",strtotime($lastDate)).'.xls');
            } else {
                $data['result'] = $query->paginate($page_limit);
                return view('pms.reports.user_billable_report.index',compact('request','data','designationList'));
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
}

?>
