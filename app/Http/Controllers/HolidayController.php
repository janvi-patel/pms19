<?php

namespace App\Http\Controllers;

use App\Holiday;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Response;
use Exception;
use Alert;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::check() && Auth::user()->hasPermission('holiday')){
            return view('pms.holiday.index');
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if(Auth::check() && Auth::user()->hasPermission('holiday')){
                $holiday = new Holiday();
                $holiday->holiday_year = $request->input('year');
                $holiday->holiday_name = $request->input('holidayName');
                $holiday->public_holiday = $request->input('holidayType')=="weekend"?NULL:1;
                $holiday->holiday_date = $request->input('date');
                $holiday->save();
                $holidayType = $request->input('holidayType')=="weekend"?"Weekend":"Holiday";
                return Response::json(array('message'=>$holidayType.' Successfully Created!','status'=>'success'));
            }else{
                return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
            }
        }catch(Exception $e){
            // $e->getMessage()
            return Response::json(array('message'=>'Holiday Not Successfully Created!','status'=>'error'));
        }
    }

    public function getNextPreviousMonthYear($year,$month)
    {
        $previousMonth = $currentMonth = $nextMonth = null;
        $previousYear  = $currentYear = $nextYear = null;

        $currentMonth = $month;
        $currentYear  = $previousYear = $nextYear = $year;

        $previousMonth = $currentMonth - 1;
        if($previousMonth == 0){
            $previousMonth = 12;
            $previousYear  = $currentYear - 1; 
        }

        $nextMonth = $currentMonth + 1;
        if($nextMonth == 13){
            $nextMonth = 1;
            $nextYear = $currentYear + 1;
        }

        $firstDateOfPreviousMonth = $previousYear.'-'.sprintf("%02d",$previousMonth).'-'.'01'; 
        $lastDateOfNextMonth      = new \DateTime($nextYear.'-'.sprintf("%02d",$nextMonth));
        $lastDateOfNextMonth->modify('last day of this month');

        return array('previous'=> $firstDateOfPreviousMonth,'next'=> $lastDateOfNextMonth->format('Y-m-d'));
    }

    public function getMonthWiseHoliday(Request $request)
    {
        try{
            $result = array();
            $dates  = $this->getNextPreviousMonthYear($request->input('year'),sprintf("%02d", $request->input('month')));

            $holiday   = Holiday::whereBetween('holiday_date',$dates)
//                                ->where('public_holiday','=',1)
                                ->get(['id','holiday_name','holiday_date','public_holiday']);

            if(count($holiday) > 0){
                foreach($holiday as $key => $value){
                   $result[$key]['id']              =  $value->id;
                   $result[$key]['title']           =  $value->holiday_name;
                   $result[$key]['holidayType']           =  $value->public_holiday;
                   $result[$key]['year']            =  date('Y',strtotime($value->holiday_date));
                   $result[$key]['month']           =  date('m',strtotime($value->holiday_date));
                   $result[$key]['day']             =  date('d',strtotime($value->holiday_date));
                   $result[$key]['backgroundColor'] =  '#f56954';
                   $result[$key]['borderColor']     =  '#f56954';
                }
            }

            $message = (count($result) > 0)?'Record Found':'No Record Found';
            return Response::json(array('status'=>'success','message' => $message,'result' => $result));
        }catch(Exception $e){
            // $e->getMessage()
            $message = 'No Record Found';
            return Response::json(array('status'=>'error','message' => $message));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            if(Auth::check() && Auth::user()->hasPermission('holiday')) {
                Holiday::where('id',$request->input('id'))->delete();
                $holiday = new Holiday();
                $holiday->holiday_year = date("Y", strtotime($request->input('date')));;
                $holiday->holiday_name = $request->input('title');
                $holiday->public_holiday = $request->input('holidayType')=="weekend"?NULL:1;
                $holiday->holiday_date = $request->input('date');
                $holiday->save();
                $holidayType = $request->input('holidayType')=="weekend"?"Weekend":"Holiday";
                return Response::json(array('message'=>$holidayType.' Successfully Updated!','status'=>'success'));
            }else{
                return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
            }
        }catch(Exception $e){
            // $e->getMessage()
            return Response::json(array('message'=>'Holiday Not Successfully Updated!','status'=>'error'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if(Auth::check() && Auth::user()->hasPermission('holiday')){
                $holiday = Holiday::find($request->input('id'));
                $holidayType = $holiday->public_holiday==NULL?"Weekend":"Holiday";
                $holiday->delete();

                return Response::json(array('message'=>$holidayType.' Successfully Deleted!','status'=>'success'));
            }else{
                // $e->getMessage()
                return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error'));
            }
        }catch(Exception $e){
            return Response::json(array('message'=>'Holiday Not Successfully Deleted!','status'=>'error'));
        }
    }
}
