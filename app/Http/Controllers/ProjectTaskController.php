<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use App\Cr;
use App\Company;
use Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Response;
use Exception;
use App\ProjectTasks;
use DB;
use Alert;
use App\Jobs\MailJob;
use App\ProjectEntry;
use App\ProjectTeam;
use Kyslik\ColumnSortable\Sortable;
use App\ProjectCostInfo;
use App\Setting;
use App\Helpers\CommonHelper;

class ProjectTaskController extends Controller
{
    public function getTaskList(Request $request)
    {
        // dd($this->getChangesProjectCr(1381));
        $getDeletebleTaskId = array();
        $validateAccessData = $this->validateAccessData($request->id);
        if($validateAccessData)
        {
            $data['projectTaskId'] = $data['projectListedTaskId'] =[];
            $setting = new Setting();
            $days = $setting->getSettingTaskOperationData();
            $ProjectTasks = new ProjectTasks();
            $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $projectId = $request->id;
            $taskDropdown = ProjectTasks::where('project_tasks.project_id',$projectId)->whereNull('deleted_at')->orderBy('task_name')->select('id','task_name')->get()->toArray();
            $projectDataVal = Project::find($projectId);
            $getProjectTask = ProjectTasks::with("getCr")->leftJoin('project_entries as pe', function($leftJoin) {
                $leftJoin->on('project_tasks.id', '=','pe.task_id')->whereNull('pe.deleted_at');
            })->where('project_tasks.project_id',$projectId);
            if(Auth::check() && Auth::user()->hasPermission('projects.task.listing')){
                if(isset($request->search_user) && !empty($request->search_user)) {
                    $getProjectTask = $getProjectTask->where('project_tasks.user_id',$request->search_user);
                }
            } else {
                $getProjectTask = $getProjectTask->where('project_tasks.user_id',auth()->user()->id);
            }
            if(isset($request->search_by_task_name) && !empty($request->search_by_task_name)) {
                $getProjectTask = $getProjectTask->where('project_tasks.id', $request->search_by_task_name);
            }
            if(isset($request->search_by_start_date) && !empty($request->search_by_start_date)){
                $getProjectTask = $getProjectTask->where('project_tasks.task_start_date',">=", date('Y-m-d',strtotime($request->search_by_start_date)));
            }
            if(isset($request->search_by_end_date) && !empty($request->search_by_end_date)){
                $getProjectTask = $getProjectTask->where('project_tasks.task_end_date',"<=", date('Y-m-d',strtotime($request->search_by_end_date)));
            }
            if((isset($request->search_by_start_date) && !empty($request->search_by_start_date)) && (isset($request->search_by_end_date) && !empty($request->search_by_end_date))) {
                $getProjectTask = $getProjectTask->where('project_tasks.task_start_date', '>=', date('Y-m-d',strtotime($request->search_by_start_date)));
                $getProjectTask = $getProjectTask->where('project_tasks.task_end_date', '<=', date('Y-m-d',strtotime($request->search_by_end_date)));
            }
            if(isset($request->search_task_desc) && !empty($request->search_task_desc)){
                $getProjectTask = $getProjectTask->where('project_tasks.task_desc','LIKE','%'.$request->search_task_desc.'%');
            }
            $getTotalProjectTask = clone $getProjectTask;
            $getProjectTask->groupBy('project_tasks.id')->whereNull('project_tasks.deleted_at')
                                ->select(DB::raw("IF(sum(`log_hours`) IS NOT NULL,sum(`log_hours`), 0) as logged_hours"),'project_tasks.*');
            $user        = new User();
            $userListing = $userListingForTL = array();
            $companyId   = Auth::user()->company_id;
            $loggedInUserID = isset(Auth::user()->id) ? intval(Auth::user()->id) : 0;
            if(Auth::user()->hasRole('pm')){
                $userListing = $user->getMyTeamList($loggedInUserID);
            }else{
                if($companyId){
                    $user =   $user->where('company_id',$companyId);
                }
                if(Auth::user()->hasRole('tl')){
                    $userListingForTL = (new User())->getMyTeamList($loggedInUserID);
                }
                $userListing = $user->orderBy('first_name', 'ASC')->get();
            }
            $teamUserArray = $teamUserName = $tlTeamUserArray = array();
            foreach ($userListing as $userListingVal){
                $teamUserArray[] = $userListingVal['id'];
            }
            foreach($userListingForTL as $userListingForTLVal){
                $tlTeamUserArray[] = $userListingForTLVal['id'];
            }
            if ((!Auth::user()->hasRole('super_admin') && !Auth::user()->hasRole('admin') )&&
                    ($projectDataVal['project_name'] == 'Miscellaneous Tasks' || $projectDataVal['project_name'] == 'Bench')){
                    $getProjectTask = $getProjectTask->whereIn('project_tasks.user_id',$teamUserArray);
                    if(Auth::user()->hasRole('tl')){
                        if(($projectDataVal['project_name'] == 'Miscellaneous Tasks' || $projectDataVal['project_name'] == 'Bench')){
                            $tlTeamUserArray[] = $loggedInUserID;
                        }
                        $getProjectTask = $getProjectTask->whereIn('project_tasks.user_id',$tlTeamUserArray);
                    }
            }
            $data['projectTaskId'] = $getProjectTask->pluck("id")->toArray();
            $getProjectTask = $getProjectTask->sortable()->orderBy("cr_id", "desc")->orderBy('project_tasks.id', 'desc')->paginate($page_limit);
            $getProjectData = Project::leftjoin('cr','projects.id','cr.project_id')
                                ->whereNull('cr.deleted_at')
                                ->where('projects.id',$projectId)

                                ->select('projects.project_name',DB::raw("projects.`pms_hours` as total_project_hours"),
                                        DB::raw("sum(cr.`pms_hours`) as total_cr_hours"))
                                ->first();
            $loggedHours = ProjectEntry::where('project_id',$projectId)->select(DB::raw("sum(`log_hours`) as total_logged_hours"))->first();
            $data['projectListedTaskId'] = $getProjectTask->pluck("id")->toArray();
            $getTotalProjectTask = $getTotalProjectTask->get();
            foreach($getProjectTask as $getTaskId)
            {
                if(isset($data[$getTaskId->cr_id])){
                    $data[$getTaskId->cr_id] = ++$data[$getTaskId->cr_id];
                } else {
                    $data[$getTaskId->cr_id] = 1;
                }
                $data["task_id"][] = $getTaskId->id;
                $data["cr_task_id"][$getTaskId->cr_id][] = $getTaskId->id;
                if(count(ProjectEntry::where('task_id',$getTaskId->id)->get()) == 0 )
                {
                    $getDeletebleTaskId[] = $getTaskId->id;
                }
            }
            $projectName  = '';
            $i = 1;
            $log_date = date('d-m-Y',strtotime("-".$i." days"));
            while(1){
                $isHoliday = app('App\Http\Controllers\ProjectController')->checkEntryDate($log_date);
                if($isHoliday == false){
                    break;
                }else{
                    $log_date = date('d-m-Y',strtotime("-1 days",strtotime($log_date)));
                }
                $i++;
            }
            $project_team = ProjectTeam::where('project_id',$projectId)->pluck('user_id');
            $teamleader = Project::where('id',$projectId)->pluck('team_leader');
            $project_team_details=User::whereIn('id',$project_team)->orWhereIn('id',$teamleader)->get();
            return view('pms.task.project_task_listing',compact('getTotalProjectTask','projectName','getProjectTask','taskDropdown',
                    'getProjectData','loggedHours','data','log_date','projectId','request','project_team_details','getDeletebleTaskId','days'));
        }
        else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function addProjectTaskModal(Request $request){
        $user = new user();
        $projectId   = $request->input('projectId');
        $company_id = Auth::user()->company_id;
        $userNameList = $user->getUserNameByCompany($company_id);
        $project = Project::where('id',$projectId)->first();
        $obj_project = new Project();
        $settingMonth = (int)getSettingModules(["select" => "constant_int_value","value" => "constant_int_value","where" =>["module" => "cr_operation","company_id" => auth()->user()->company_id]]);
        $data["project_cr_list"] = $obj_project->getDetails(['where' => ["id" => $projectId,"project_status" => 1],"select" =>["id","project_name"],"with" => ["getProjectCr" => function($data) use ($settingMonth) { $data->select("id","project_id","title")->whereBetween("cr_start_date",[date("Y-m-01",strtotime("-".$settingMonth." months")),date("Y-m-t")])->where(["status" => 1])->orderBy("created_at","desc"); }]]);
        return view('pms.task.modal.add_project_task_modal',compact('project','userNameList',"data"));
    }

    public function getChangesProjectCr(Request $request){
        $id = $request->move_to_project_id;
        $obj_project = new Project();
        $settingMonth = (int)getSettingModules(["select" => "constant_int_value","value" => "constant_int_value","where" =>["module" => "cr_operation","company_id" => auth()->user()->company_id]]);
        $data["project_cr_list"] = $obj_project->getDetails(['where' => ["id" => $id,"project_status" => 1],"select" =>["id","project_name"],"with" => ["getProjectCr" => function($data) use ($settingMonth) { $data->select("id","project_id","title")->whereBetween("cr_start_date",[date("Y-m-01",strtotime("-".$settingMonth." months")),date("Y-m-t")])->where(["status" => 1])->orderBy("created_at","desc"); }]]);
        return response()->json($data);
    }

    public function moveProjectTask(Request $request) {
        if(Auth::check() && Auth::user()->hasPermission('projects.task.move')){
            $projectData = $this->getMyProject();
            $project_name = array();
            if(!empty($projectData)){
                foreach ($projectData as $dataVal) {
                    if($dataVal['project_name'] != 'Miscellaneous Tasks' && $dataVal['project_name'] != 'Bench'){
                        $project_name[$dataVal['id']] = $dataVal['project_name'];
                    }
                }
            }
            return view('pms.task.modal.move_task',compact('project_name'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function moveTaskEntry(Request $request) {
        if(Auth::check() && Auth::user()->hasPermission('projects.task.move')){
            $taskIdArray = $projectTasks = array();
            $projectId          = $request->input('move_to_project_id');
            $projectCrList = $request->input('project_cr_list');
            $validateAccessData = $this->validateAccessData($projectId);
            if($validateAccessData)
            {
                $taskIdArray                = $request->input('taskIdArray');
                if(count($taskIdArray) > 0){
                    $taskQuery              = ProjectTasks::whereIn('id',$taskIdArray)->whereIn('project_id',[517, 516, 518, 458])->whereNotNull('task_start_date')->whereNotNull('task_end_date');
                    $projectTasks           = $taskQuery->get()->toArray();
                    if(count($projectTasks) != count($taskIdArray)){
                        return Response::json(array('message'=>'Something went wrong!!','status'=>'error'));
                    }else{
                        $totalTaskHours         = $taskQuery->sum('estimated_hours');
                        $project                = Project::where('id',$projectId)->first()->toArray();
                        $remainingHours  = $this->checkTaskHours($project,$projectId,'');
                        $getCrDetails = $this->getProjectCrDetails(["project_id" => $projectId, "id" => $projectCrList]);
                        if($remainingHours < $totalTaskHours && $project['project_type'] !=3){
                            return Response::json(array('message'=>'Task hours is more than project hours!','status'=>'error'));
                        } else if ( !$getCrDetails['status'] ) {
                            return Response::json(array('message'=> $getCrDetails['message'],'status'=>'error'));
                        } else if ($getCrDetails['data']['remaining_hours'] < $totalTaskHours && $project['project_type'] != 3) {
                            return Response::json(array('message'=>'Task hours is more then selected project / cr hours','status'=>'error'));
                        } else {
                            $getMyTeamUserID        = $this->getMyTeamUserId();
                            $taskUsersId            = $taskQuery->pluck('user_id')->toArray();
                            $validUser              = array_diff($taskUsersId,$getMyTeamUserID);
                            if(count($validUser) > 0){
                                return Response::json(array('message'=>'Some Users Are Not Part Of Your Team!','status'=>'error'));
                            } else {
                                $taskQuery_custom       = clone $taskQuery;
                                $taskStartDateArray     = $taskQuery_custom->orderBy('task_start_date')->pluck('task_start_date')->toArray();
                                $taskEndDateArray       = $taskQuery->orderBy('task_end_date','desc')->pluck('task_end_date')->toArray();
                                if($taskStartDateArray[0] <= $project['project_start_date']){
                                    return Response::json(array('message'=>'The task start date must be after the project start date!','status'=>'error'));
                                }else{
                                    if(($project['project_end_date'] != null && $project['project_end_date'] != "0000-00-00")  && $taskEndDateArray[0] >= $project['project_end_date']){
                                        return Response::json(array('message'=>'The task end date must be before the project end date!','status'=>'error'));
                                    }else{
                                        $cr_id = str_contains($request->input('project_cr_list'), 'cr_') ? str_replace("cr_","",$request->input('project_cr_list')) : 0;
                                        ProjectTasks::whereIn('id',$taskIdArray)->update(['project_id' => $project['id'], "cr_id" => $cr_id, 'updated_by' => Auth::user()->id]);
                                        ProjectEntry::whereIn('task_id',$taskIdArray)->update(['project_id' => $project['id']]);
                                        return Response::json(array('message'=>'Tasks Successfully moved!','status'=>'success'));
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                return Response::json(array('message'=>'You do not have permission to perform this action!!!','status'=>'error'));
            }
        }
        else {
            return Response::json(array('message'=>'You do not have permission to perform this action!!!','status'=>'error'));
        }
    }

    public function storeProjectTask(Request $request) {
        try{
            $setting = new Setting();
            $days = $setting->getSettingTaskOperationData();
            $projectId       = $request->input('addTaskProjectId');
            $taskAssign         = $request->input('taskAssign');
            $startDate = date('Y-m',strtotime($request->input('task_start_date')));
            $endDate = date('Y-m',strtotime($request->input('task_end_date')));
            if(date('Y-m-d',strtotime($request->input('task_end_date'))) < date('Y-m-d',strtotime($request->input('task_start_date'))))
            {
                Alert::error('The task end date must be after the task start date')->autoclose(5000);
                return Redirect::back();
            }
            if(strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d")) && date('Y-m-d',strtotime($request->input('task_start_date'))) < date("Y-m-d",strtotime("first day of previous month"))){
                Alert::error('The task starting date must be after '.date("d-M-Y",strtotime("first day of previous month")).'!')->autoclose(5000);
                return Redirect::back();
            }
            else if(!(strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d"))) && date('Y-m') > $startDate){
                Alert::error('The task starting date must be after '.date("01-M-Y").'!')->autoclose(5000);
                return Redirect::back();
            }
            else{}
            if( $startDate != $endDate){
                Alert::error('Please select start date and end date with same month')->autoclose(5000);
                return Redirect::back();
            }
            $project = Project::find($request->input('addTaskProjectId'));
            if( $project->project_start_date > date('Y-m-d',strtotime($request->input('task_start_date')))){
                Alert::error('The task date must be after the project start date')->autoclose(5000);
                return Redirect::back();
            }
            if(count($taskAssign)>0){

                foreach($taskAssign as $taskAssignVal){
                    $taskHours = $request->input('addWorkTimeHours').'.'.$request->input('addWorkTimeMinutes');
                    $getProject = Project::where('id',$projectId)->select('project_type')->first();
                    $projectHours = $this->checkTaskHours($getProject,$projectId,'');
                    if($project->project_name != 'Miscellaneous Tasks' && $project->project_name != 'Bench'){
                        $getCrDetails = $this->getProjectCrDetails(["project_id" => $request->input('addTaskProjectId'), "id" => $request->input('project_cr_id')]);
                        if($projectHours < $taskHours && $getProject['project_type'] != 3){
                            Alert::error('Task hours is more then total project hours')->autoclose(5000);
                            return Redirect::back();
                        } else if ( !$getCrDetails['status'] ) {
                            Alert::error($getCrDetails['message'])->autoclose(5000);
                            return Redirect::back();
                        } else if ($getCrDetails['data']['remaining_hours'] < $taskHours && $getProject['project_type'] != 3 && "2023-01-01" < date("Y-m-d", strtotime($request->input('task_start_date')))) {
                            Alert::error("Task hours is more then selected project / cr hours")->autoclose(5000);
                            return Redirect::back();
                        }
                    }
                    $cr_id = str_contains($request->input('project_cr_id'), 'cr_') ? str_replace("cr_","",$request->input('project_cr_id')) : 0;

                    $obj                  =  new ProjectTasks();
                    $obj->task_name       = $request->input('taskName');
                    $obj->created_by      = Auth::user()->id;
                    $obj->user_id         = $taskAssignVal;
                    $obj->project_id      = $request->input('addTaskProjectId');
                    $obj->cr_id           = $cr_id;
                    $obj->task_desc       = $request->input('addTaskDescription');
                    $obj->task_start_date = date('Y-m-d',strtotime($request->input('task_start_date')));
                    $obj->task_end_date   = date('Y-m-d',strtotime($request->input('task_end_date')));
                    $obj->estimated_hours = $taskHours;
                    $obj->save();
                    $createTeam = new ProjectTeam();
                    $createTeam->createTeam($obj->project_id,$obj->user_id);
                }

            }
            alert()->success('Task Successfully Created')->persistent('close')->autoclose("3600");
            return Redirect::back();
        }catch(Exception $e){
            Alert::error('Task Not Created');
            return Redirect::back();
        }
    }

    public function deleteProjectTask(Request $request)
    {
        $projectTaskId   = '0';
        if(isset($request->projectTaskId) && $request->projectTaskId != '')
        {
            $projectTaskId   = $request->input('projectTaskId');
        }
        $ProjectTasks   = ProjectTasks::find($request->projectTaskId);
        $projectId = $ProjectTasks['project_id'] != '' ?$ProjectTasks['project_id'] : '0' ;
        if(Auth::check() && Auth::user()->hasPermission('projects.task.listing')){
            if(Auth::user()->hasRole(config('constant.superadmin_slug'))
            || Auth::user()->hasRole('admin')
            || Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
            || Auth::user()->hasRole(config('constant.project_manager_slug'))
            || Auth::user()->hasRole(config('constant.team_leader_slug')))
            {
                $validateAccessData = $this->validateAccessData($projectId);
                if($validateAccessData){
                    try{
                        if($ProjectTasks){
                            if(Auth::user()->hasRole(config('constant.associative_project_manager_slug'))
                            || Auth::user()->hasRole(config('constant.project_manager_slug'))
                            || Auth::user()->hasRole(config('constant.team_leader_slug'))){
                                if(count(ProjectEntry::where('task_id',$projectTaskId)->get()) != 0 )
                                {
                                    return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
                                }
                            }
                            $ProjectTasks->delete();
                            return Response::json(array('message'=>'Task Deleted Successfully','status'=>'success'));
                        }else{
                            return Response::json(array('message'=>'Task Not Deleted Successfully','status'=>'error'));
                        }
                    }catch(Exception $e){
                        return Response::json(array('message'=>'Task Not Deleted Successfully','status'=>'error'));
                    }
                }
                else{
                    return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
                }
            }
            else{
                return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
            }
        }elseif($projectId == 517 || $projectId == 516 || $projectId == 518 || $projectId == 458){
            $validateAccessData = $this->validateAccessData($projectId);
            if($validateAccessData){
                try{
                    if($ProjectTasks != null && $ProjectTasks->user_id == Auth::user()->id){
                        if(count(ProjectEntry::where('task_id',$projectTaskId)->get()) != 0 )
                        {
                            return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
                        }
                        $ProjectTasks->delete();
                        return Response::json(array('message'=>'Task Deleted Successfully','status'=>'success'));
                    }else{
                        return Response::json(array('message'=>'Task Not Deleted Successfully','status'=>'error'));
                    }
                }catch(Exception $e){
                    return Response::json(array('message'=>'Task Not Deleted Successfully','status'=>'error'));
                }
            }
            else{
                return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
            }
        }else{
            return Response::json(array('message'=>'You do not have permission to perform this action!','status'=>'error','code'=> 'userNotValid'));
        }
    }

    public function editProjectTaskModal(Request $request)
    {
        $user = new user();
        $projectTaskId       = $request->input('projectTaskId');
        $taskDetails= ProjectTasks::where('id',$projectTaskId)->first();
        $validateAccessData = $this->validateAccessData($taskDetails->project_id);
        if($validateAccessData)
        {
            $obj_project = new Project();
            $settingMonth = (int)getSettingModules(["select" => "constant_int_value","value" => "constant_int_value","where" =>["module" => "cr_operation","company_id" => auth()->user()->company_id]]);
            $data["project_cr_list"] = $obj_project->getDetails(['where' => ["id" => $taskDetails->project_id,"project_status" => 1],"select" =>["id","project_name"],"with" => ["getProjectCr" => function($data) use ($settingMonth,$taskDetails) { $data->select("id","project_id","title")->whereBetween("cr_start_date",[date("Y-m-01",strtotime("$taskDetails->task_start_date -".$settingMonth." months")),date("Y-m-t",strtotime($taskDetails->task_start_date))])->where(["status" => 1])->orderBy("created_at","desc"); }]]);
            $company_id = Auth::user()->company_id;
            $userNameList = $user->getUserNameByCompany($company_id);
            $project = Project::where('id',$taskDetails->project_id)->first();
            return view('pms.task.modal.edit_task_modal',compact('taskDetails','data','userNameList','project'));
        }
        else
        {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function updateProjectTaskModal(Request $request) {
        $projectTaskId   = '0';
        $projectId       = '0';
        if(isset($request->editTaskProjectId) && $request->editTaskProjectId != '')
        {
            $projectId       = $request->input('editTaskProjectId');
        }
        if(isset($request->editProjectTaskId) && $request->editProjectTaskId != '')
        {
            $projectTaskId   = $request->input('editProjectTaskId');
        }
        $validateAccessData = $this->validateAccessData($projectId);
        $validateTask = $this->validateTask($projectId,$projectTaskId);

        if($validateAccessData && $validateTask)
        {
            $obj   =  ProjectTasks::find($projectTaskId);
            if($obj->project_id == $projectId)
            {
                try{
                    $setting = new Setting();
                    $days = $setting->getSettingTaskOperationData();
                    $taskHours       = $request->input('editWorkTimeHours').'.'.$request->input('editWorkTimeMinutes');
                    $getProject      = Project::where('id',$projectId)->select('project_type')->first();
                    $getStartDate      = Project::where('id',$projectId)->select('project_start_date')->first();
                    $projectHours    = $this->checkTaskHours($getProject,$projectId,$projectTaskId);
                    $startDate       = date('Y-m',strtotime($request->input('task_start_date')));
                    $endDate         = date('Y-m',strtotime($request->input('task_end_date')));
                    if(date('Y-m-d',strtotime($request->input('task_end_date'))) < date('Y-m-d',strtotime($request->input('task_start_date'))))
                    {
                        Alert::error('The task end date must be after the task start date')->autoclose(5000);
                        return Redirect::back();
                    }
                    if( $startDate != $endDate){
                        Alert::error('Please select start date and end date with same month')->autoclose(5000);
                        return Redirect::back();
                    }
                    if(!($projectId == 517 || $projectId == 516 || $projectId == 518 || $projectId == 458)) {
                        if( $getStartDate['project_start_date'] > date('Y-m-d',strtotime($request->input('task_start_date')))){
                            Alert::error('The task date must be after the project start date')->autoclose(5000);
                            return Redirect::back();
                        }
                        $getCrDetails = $this->getProjectCrDetails(["project_id" => $projectId, "id" => $request->input('project_cr_id'), "task_id" => $projectTaskId]);
                        if($projectHours < $taskHours && $getProject['project_type'] != 3){
                            Alert::error('Task hours is more then total project hours')->autoclose(5000);
                            return Redirect::back();
                        } else if ( !$getCrDetails['status'] ) {
                            Alert::error($getCrDetails['message'])->autoclose(5000);
                            return Redirect::back();
                        } else if ($getCrDetails['data']['remaining_hours'] < $taskHours && $getProject['project_type'] != 3 && "2023-01-01" < date("Y-m-d", strtotime($request->input('task_start_date')))) {
                            Alert::error("Task hours is more then selected project / cr hours")->autoclose(5000);
                            return Redirect::back();
                        }
                    }
                    $oldData =  ProjectTasks::find($projectTaskId);
                    if(!Auth::user()->hasRole(config('constant.superadmin_slug')) && !Auth::user()->hasRole(config('constant.admin_slug'))){
                        if((date('Y-m',strtotime($obj->task_start_date)) >= date("Y-m",strtotime("-1 month"))
                        && (strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d")) || (date('Y-m',strtotime($obj->task_start_date)) == date("Y-m")))
                        && $obj->task_start_date != '') == false)
                        {
                            Alert::error('You do not have permission to perform this action!');
                            return Redirect::back();
                        }
                        if(strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d")) && date('Y-m-d',strtotime($request->input('task_start_date'))) < date("Y-m-d",strtotime("first day of previous month")))
                        {
                            Alert::error('The task starting date must be after '.date("d-M-Y",strtotime("first day of previous month")).'!')->autoclose(5000);
                            return Redirect::back();
                        }
                        else if(!(strtotime(date("Y-m-".$days)) > strtotime(date("Y-m-d"))) && date('Y-m') > $startDate){
                            Alert::error('The task starting date must be after '.date("01-M-Y").'!')->autoclose(5000);
                            return Redirect::to('view/task/list/'.$projectId);
                        }
                        else{}
                    }
                    if( $obj->task_start_date == null || $obj->task_start_date == null){
                        Alert::error('You do not have permission to perform this action!')->autoclose(5000);
                        return Redirect::back();
                    }
                        $cr_id = str_contains($request->input('project_cr_id'), 'cr_') ? str_replace("cr_","",$request->input('project_cr_id')) : 0;

                        $data['oldData']      = $oldData;
                        $data['createdName']  = (new \App\Helpers\CommonHelper)->getAnyUserById($obj->created_by);
                        $obj->task_name       = $request->input('taskName');
                        $obj->updated_by      =  Auth::user()->id;
                        $obj->cr_id           = $cr_id;
                        $obj->project_id      = $request->input('editTaskProjectId');
                        $obj->user_id         = $request->input('assigned_user_list');
                        $obj->task_desc       = $request->input('editTaskDescription');
                        $obj->task_start_date = date('Y-m-d',strtotime($request->input('task_start_date')));
                        $obj->task_end_date   = date('Y-m-d',strtotime($request->input('task_end_date')));
                        $obj->estimated_hours = $taskHours;
                        $obj->save();
                        $data['newData'] = $obj;
                        $data['userData'] = (new \App\Helpers\CommonHelper)->getAnyUserById($request->input('assigned_user_list'));
                        $data['updatedName'] = (new \App\Helpers\CommonHelper)->getAnyUserById($obj->updated_by);
                        $data['projectName'] = Project::where('id',$request->input('editTaskProjectId'))->first();
                        if(Auth::user()->company_id == 2){
                            $company     = new Company();
                            $userCompanyMailData  = $company->getCompanyViseMailData(Auth::user()->company_id)[0];
                            $data['regards'] = $userCompanyMailData['company_name'];
                            $from['email'] = $userCompanyMailData['from_address'];
                            $from['name']  = $userCompanyMailData['company_name'];

                            $to = $data['userData']['email'];
                            $cc = array('sagar.s@tridhya.com',Auth::user()->email);
                            $template = 'emails.task_update';
                            $subject = (($data['updatedName'])?$data['updatedName']->first_name.' '.$data['updatedName']->last_name:'').' has updated task for '.(($data['userData'])?$data['userData']->first_name.' '.$data['userData']->last_name:'');
                            MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc = '', []);
                        }

                        Alert::success('Task Successfully Updated');
                        return Redirect::back();
                }catch(Exception $e){
                    Alert::error('Task Not Updated Successfully')->autoclose(5000);
                    return Redirect::back();
                }
            } else {
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }
        else
        {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function getProjectPmsHours($projectId) {
        $projectCrHours = Project::leftjoin('cr',function($leftJoin)
                            {
                                $leftJoin->on('projects.id', '=', 'cr.project_id')->whereNull('cr.deleted_at');
                            })
                        ->select('projects.pms_hours','projects.id',DB::raw("floor(sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) +
                        ((sum(floor(cr.pms_hours)) + sum(cr.pms_hours - floor(cr.pms_hours)) * 100.0/60) % 1) * 60.0/100  as total_cr_hours"))
                        ->where('projects.id',$projectId)
                        //->groupBy('projects.id')
                        ->first();
        return $projectCrHours;
    }

    public function checkTaskHours($getProject,$projectId,$taskId) {
        $remainingHours = 0;
        if($getProject['project_type'] != 3){
            $getProjectHr = $this->getProjectPmsHours($projectId);
            $projectHour =  sprintf ("%.2f",$getProjectHr['pms_hours'] + $getProjectHr['total_cr_hours']);

            $taskHour = ProjectTasks::where('project_id',$projectId);
            if($taskId != ''){
                $taskHour->where('id','!=',$taskId);
            }
            $taskHour =  $taskHour->select(DB::raw("sum(`estimated_hours`) as total_task_hours"))->first();
            if($projectHour > $taskHour['total_task_hours']){
                $remainingHours = $projectHour - $taskHour['total_task_hours'];
            }
        }
        return $remainingHours;
    }

    public function validateAccessData($reqId){

        if(Auth::user()->hasRole(config('constant.superadmin_slug')))
        {
            return true;
        }
        $data              = [];
        $companyId         = Auth::user()->company_id;
        if (Auth::user()->hasRole(config('constant.admin'))) {
            $data = ProjectCostInfo::where('company_id', $companyId)
                ->pluck('project_id')->toArray();
        }
        else{
            $own_project =  ProjectTeam::where('company_id',Auth::user()->company_id)->where('user_id',Auth::user()->id)->pluck('project_id');
            $dataQuery =  Project::where(function($q) use($own_project) {
                $q->where('projects.created_by', Auth::user()->id)
                ->orWhere('projects.team_leader',Auth::user()->id)
                ->orWhere('projects.project_manager',Auth::user()->id)
                ->orWhereIn('projects.id',$own_project)
                ->orwhere(function($q){
                    $q->orwhere('projects.assigned_to',Auth::user()->company_id)
                    ->where(function($q){
                            $q->where('projects.project_name','Miscellaneous Tasks')
                            ->orwhere('projects.project_name','Beanch');
                        });
                });
            });
            $data = $dataQuery->pluck('id')->toArray();

        }
        if(in_array($reqId, $data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getMyProject(){
        $companyId         = Auth::user()->company_id;
        if (Auth::user()->hasRole(config('constant.admin'))) {
            $dataQuery = ProjectCostInfo::where('company_id', $companyId)
                ->pluck('project_id')->toArray();
                $data = Project::whereIn('id',$dataQuery)->select('id','project_name')->orderBy('project_name')->get()->toArray();
        }
        else{
            $own_project =  ProjectTeam::where('company_id',Auth::user()->company_id)->where('user_id',Auth::user()->id)->pluck('project_id');
            $dataQuery =  Project::where(function($q) use($own_project) {
                $q->where('projects.created_by', Auth::user()->id)
                ->orWhere('projects.team_leader',Auth::user()->id)
                ->orWhere('projects.project_manager',Auth::user()->id)
                ->orWhereIn('projects.id',$own_project)
                ->orwhere(function($q){
                    $q->orwhere('projects.assigned_to',Auth::user()->company_id)
                    ->where(function($q){
                            $q->where('projects.project_name','Miscellaneous Tasks')
                            ->orwhere('projects.project_name','Beanch');
                        });
                });
            });
            $dataQuery->where('project_status',1);
            $data = $dataQuery->select('id','project_name')->orderBy('project_name')->get()->toArray();

        }
        return $data;
    }

    public function validateTask($projectId,$reqId){
        $data              = [];
        $ProjectTasks = new ProjectTasks();
        $getProjectTask = $ProjectTasks->where('project_id',$projectId);
        $data = $getProjectTask->pluck('id')->toArray();
        if(in_array($reqId, $data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function teamTaskList(Request $request) {
        if(Auth::check() && Auth::user()->hasPermission('projects.task.listing')){
            $setting = new Setting();
            $user_id = array();
            $days = $setting->getSettingTaskOperationData();
            $page_limit     = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $companyId      = Auth::user()->company_id;
            $user = new User();
            $loggedInUserID = Auth::user()->id;
            $teamUserName = array();
            $getStartDate = date('Y-m-d', strtotime('first day of this month'));
            $getEndDate = date('Y-m-d', strtotime('last day of this month'));
            if(Auth::user()->hasRole('pm') || Auth::user()->hasRole('tl')){
                $userListing = $user->getMyTeamList($loggedInUserID);
            }else{
                if($companyId){
                $user =   $user->where('company_id',$companyId);
                }
                $userListing = $user->where('status',1)->WhereNotIn('designation_id',array(1,15))->orderBy('first_name', 'ASC')->get();
            }
            foreach ($userListing as $userListingVal){
                $teamUserName[$userListingVal['id']] = $userListingVal['first_name'].' '.$userListingVal['last_name'];
                $user_id[] = $userListingVal['id'];
            }
            $projectIdQue = new Project();
            if (!Auth::user()->hasRole('super_admin')){
                if (Auth::user()->hasRole('admin')) {
                    $projectIdQue= $projectIdQue->where('projects.assigned_to',$companyId);
                }elseif(Auth::user()->hasRole('pm') || Auth::user()->hasRole('tl')){
                    $projectIdQue = $projectIdQue;
                }else{
                    $projectTeam = ProjectTeam::where('company_id',$companyId)->where('user_id',$loggedInUserID)->pluck('project_id');
                    $projectIdQue = $projectIdQue->whereIn('projects.id',$projectTeam);
                }
            }
            $proArray = Project::whereIn('id',[516,517,518,458])->where('company_id','!=',$companyId)->pluck('id')->toArray();
            // $proArray Is store non-billable other compnys project
            $projectIds = $projectIdQue->where('projects.project_status',1)->whereNotIn('id',$proArray)->pluck('id')->toArray();
            $getProjectList = Project::whereIn('id',$projectIds)->pluck('project_name','id')->toArray();

            $dataQuery = Project::leftjoin('project_tasks as pt','projects.id','pt.project_id')
                                    ->leftjoin('project_entries as pe',function($leftJoin)
                                    {
                                        $leftJoin->on('pt.id', '=','pe.task_id')
                                        ->whereNull('pe.deleted_at');
                                    })
                                    ->leftjoin('users as u','pt.user_id','u.id')
                                    ->whereIn('projects.id',$projectIds)->whereNull('pt.deleted_at')
                                    ->whereIn('pt.user_id',$user_id);

            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                    $dataQuery->whereIn('projects.id', $request->search_by_project_name);
                }
                if ($request->has('search_task_name') && $request->search_task_name != '') {
                    $dataQuery->where('pt.task_name','LIKE','%'.$request->search_task_name.'%');
                }
                if ($request->has('search_task_start_date') && $request->search_task_start_date != '') {
                    $dataQuery->where('pt.task_start_date','>=',date('Y-m-d',strtotime($request->search_task_start_date)));
                }
                if ($request->has('search_task_end_date') && $request->search_task_end_date != '') {
                    $dataQuery->where('pt.task_end_date','<=',date('Y-m-d',strtotime($request->search_task_end_date)));
                }
                if ($request->has('search_by_user_name') && $request->search_by_user_name != '') {
                    $dataQuery->whereIn('u.id',$request->search_by_user_name);
                }
                if ($request->has('search_task_desc') && $request->search_task_desc != '') {
                    $dataQuery->where('task_desc','LIKE','%'.$request->search_task_desc.'%');
                }

                //Query for Project_Type: Billable, Non-billable or ALL.
                if($request->project_type==1){
                    // All Project_type no need of any additional query
                }
                elseif($request->project_type==2){
                    // Billable Project Type only:
                    $dataQuery->where('is_non_billable',0);
                }
                elseif($request->project_type==3){
                    // Non Billable Project Type only:
                    $dataQuery->where('is_non_billable',1);
                }
            }

            if(($request->has('search_task_start_date') === '' &&  $request->has('search_task_end_date') === '') ||
                    (!$request->has('search_task_start_date') && !$request->has('search_task_end_date'))){
                $dataQuery->where('pt.task_start_date','>=',date('Y-m-d',strtotime($getStartDate)))
                        ->where('pt.task_end_date','<=',date('Y-m-d',strtotime($getEndDate)));
            }

            $dataQuery->select('projects.project_name as projectName','pt.task_name as taskName','pt.estimated_hours as assignedHours'
                                ,'u.first_name as firstName','u.last_name as lastName',DB::raw("sum(pe.`log_hours`) as logged_hours"),'pt.id'
                    ,'pt.task_start_date','pt.task_end_date','pt.project_id','pt.user_id')
                            ->groupBy('pt.id')->get();
            if ($request->has('sort') && $request->input('sort') != '') {
                $getProjectData = $dataQuery->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $getProjectData = $dataQuery->sortable()->orderBy('pt.id', 'desc')->paginate($page_limit);
            }
            $getDeletebleTaskId = array();
            foreach($getProjectData as $getTaskId)
            {
                if(count(ProjectEntry::where('task_id',$getTaskId->id)->get()) == 0 )
                {
                    $getDeletebleTaskId[] = $getTaskId->id;
                }
            }
            $projectName = '';
            $i = 1;
            $log_date = date('d-m-Y',strtotime("-".$i." days"));
            while(1){
                $isHoliday = app('App\Http\Controllers\ProjectController')->checkEntryDate($log_date);
                if($isHoliday == false){
                    break;
                }else{
                    $log_date = date('d-m-Y',strtotime("-1 days",strtotime($log_date)));
                }
                $i++;
            }
            return view('pms.task.team_task_listing',compact('request','getProjectData','getProjectList','teamUserName'
                    ,'projectName','log_date','getStartDate','getEndDate','getDeletebleTaskId','days'));
        }
        else
        {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function myTaskList(Request $request) {
        // ini_set('max_execution_time', 0);
        $page_limit     = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        $companyId      = Auth::user()->company_id;
        $user = new User();
        $loggedInUserID = Auth::user()->id;
        $teamUserName = array();
        $getStartDate = date('Y-m-d', strtotime('first day of last month'));
        $getEndDate = date('Y-m-d', strtotime('last day of this month'));

        $projectTeam = ProjectTeam::where('company_id',$companyId)->where('user_id',$loggedInUserID)->pluck('project_id');
        $projectIds = Project::whereIn('projects.id',$projectTeam)->pluck('id')->toArray();
        $getProjectList = Project::whereIn('id',$projectIds)->pluck('project_name','id')->toArray();

        $dataQuery = Project::leftjoin('project_tasks as pt','projects.id','pt.project_id')
                                   ->leftjoin('project_entries as pe', function($leftJoin)
                                   {
                                       $leftJoin->on('pt.id', '=','pe.task_id')
                                       ->whereNull('pe.deleted_at');
                                   })
                                   ->leftjoin('users as u','pt.user_id','u.id')
                                    ->where('pt.user_id',$loggedInUserID)
                                    ->whereIn('projects.id',$projectIds)->whereNull('pt.deleted_at');

        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                $dataQuery->where('projects.id', $request->search_by_project_name);
            }
            if ($request->has('search_task_name') && $request->search_task_name != '') {
                $dataQuery->where('pt.task_name','LIKE','%'.$request->search_task_name.'%');
            }
            if ($request->has('search_task_start_date') && $request->search_task_start_date != '') {
                $dataQuery->where('pt.task_start_date','>=',date('Y-m-d',strtotime($request->search_task_start_date)));
            }
            if ($request->has('search_task_end_date') && $request->search_task_end_date != '') {
                $dataQuery->where('pt.task_end_date','<=',date('Y-m-d',strtotime($request->search_task_end_date)));
            }
            if ($request->has('search_task_desc') && $request->search_task_desc != '') {
                $dataQuery->where('pt.task_desc','LIKE','%'.$request->search_task_desc.'%');
            }
        }

        if(($request->has('search_task_start_date') === '' &&  $request->has('search_task_end_date') === '') ||
                (!$request->has('search_task_start_date') && !$request->has('search_task_end_date'))){
            $dataQuery->where('pt.task_start_date','>=',date('Y-m-d',strtotime($getStartDate)))
                    ->where('pt.task_end_date','<=',date('Y-m-d',strtotime($getEndDate)));
        }
        $dataQuery->select('projects.project_name as projectName','pt.task_name as taskName','pt.estimated_hours as assignedHours'
                            ,'u.first_name as firstName','u.last_name as lastName',DB::raw("sum(pe.`log_hours`) as logged_hours"),'pt.id'
                ,'pt.task_start_date','pt.task_end_date','pt.project_id')
                         ->groupBy('pt.id')->get();
        if ($request->has('sort') && $request->input('sort') != '') {
            $getProjectData = $dataQuery->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
        } else {
            $getProjectData = $dataQuery->sortable()->orderBy('pt.id', 'desc')->paginate($page_limit);
        }
        $projectName = '';
        $i = 1;
        $log_date = date('d-m-Y',strtotime("-".$i." days"));
        while(1){
            $isHoliday = app('App\Http\Controllers\ProjectController')->checkEntryDate($log_date);
            if($isHoliday == false){
                break;
            }else{
                $log_date = date('d-m-Y',strtotime("-1 days",strtotime($log_date)));
            }
            $i++;
        }
        return view('pms.task.mytask_listing',compact('request','getProjectData','getProjectList','projectName','log_date','getStartDate','getEndDate'));
    }

    public function getMyTeamUserId(){
        $userObj                = new User();
        $myTeam = $teamUserArray = array();
        if(Auth::user()->hasRole('pm')){
            $myTeam                 = $userObj->getMyTeamList(Auth::user()->id);
        }
        if(Auth::user()->hasRole('admin')){
            $user =   $userObj->where('company_id',Auth::user()->company_id);
            $myTeam = $user->where('users.status',1)->orderBy('first_name', 'ASC')->get();
        }
        if(count($myTeam) > 0){
            foreach ($myTeam as $userListingVal){
                $teamUserArray[] = $userListingVal['id'];
            }
        }
        return $teamUserArray;
    }

    public function getProjectCrDetails($array = []) {
        $totalHours = [];
        $remainingHours = 0;
        $updateWhereIn = null;
        if(isset($array['task_id']) && $array['task_id'] != ""){
            $updateWhereIn = ["column" => "id","array" => [$array['task_id']]];
        }
        if (str_contains($array['id'], 'project_')) {
            $hasRecord = Project::where(["id" => str_replace("project_","",$array['id']),"project_status" => 1])->pluck("pms_hours")->toArray();
            $taskCondition = ['column' => "estimated_hours", "where"=> ["project_id" => $array['project_id'],"cr_id" => 0]];
            if(isset($array['task_id']) && $array['task_id'] != ""){
                $taskCondition['whereNotIn'] = ["column" => "id","array" => [$array['task_id']]];
            }
            $taskAssignedHours = getProjectTaskSum($taskCondition);
        } else if (str_contains($array['id'], 'cr_')) {
            $settingMonth = (int)getSettingModules(["select" => "constant_int_value","value" => "constant_int_value","where" => ["module" => "cr_operation","company_id" => auth()->user()->company_id]]);
            $hasRecord = Cr::where(["id" =>str_replace("cr_","",$array['id']),"project_id" => $array['project_id'],"status" => 1])->whereBetween("cr_start_date",[date("Y-m-01",strtotime("-".$settingMonth." months")),date("Y-m-t")])->pluck("pms_hours")->toArray();
            $taskCondition = ['column' => "estimated_hours", "where"=> ["project_id" => $array['project_id'],"cr_id" => str_replace("cr_","",$array['id'])]];
            if(isset($array['task_id']) && $array['task_id'] != ""){
                $taskCondition['whereNotIn'] = ["column" => "id","array" => [$array['task_id']]];
            }
            $taskAssignedHours = getProjectTaskSum($taskCondition);
        }
        if(count($hasRecord) > 0) {
            $totalHours = (isset($hasRecord[0]) && $hasRecord[0] > 0) ? $hasRecord[0] : 0;
            $remainingHours = $totalHours - $taskAssignedHours;
            return ["status" => true, "data" => ["total_hours" => $totalHours,"task_assigned_hours" => $taskAssignedHours,"remaining_hours" => $remainingHours], "message" => "Selected project / cr is valid!"];
        } else {
            return ["status" => false, "data" => $hasRecord, "message" => "Selected project / cr is invalid!"];
        }
    }

    public function getProjectCrList(Request $request){
        $data = array();
        if(isset($request->pro_id) && $request->pro_id != ""){
            $obj_project = new Project();
            $settingMonth = (int)getSettingModules(["select" => "constant_int_value","value" => "constant_int_value","where" =>["module" => "cr_operation","company_id" => auth()->user()->company_id]]);
            $data["project_cr_list"] = $obj_project->getDetails(['where' => ["id" => $request->pro_id,"project_status" => 1],"select" =>["id","project_name"],"with" => ["getProjectCr" => function($data) use ($settingMonth) { $data->select("id","project_id","title")->whereBetween("cr_start_date",[date("Y-m-01",strtotime("-".$settingMonth." months")),date("Y-m-t")])->where(["status" => 1])->orderBy("created_at","desc"); }]]);
        }
        return $data;
    }
}
