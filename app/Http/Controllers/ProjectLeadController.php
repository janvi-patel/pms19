<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth; 
use App\ProjectType;
use App\Client;
use App\RoleUser;
use App\ProjectLead;
use App\LeadAttachment;
use App\LeadChat;
use DB;
use Redirect;
use Validator;
use Alert;
use App\Technology;
use Response;
use Session;
use App\Company;
use App\Helpers\CommonHelper;
use App\Jobs\MailJob;
class ProjectLeadController extends Controller
{
    ///////////////////For Lead Operation
    public function index(Request $request)
    {
        // print_r($request->all());
        // exit();
        if(Auth::check() && Auth::user()->hasPermission('lead.listing')){
            $select_team_leader = array();
            if(Auth::user()->hasRole(config('constant.project_manager_slug')))
            {
                $select_team_leader = $this->getTlList(Auth::user()->id);
            }
            $ids = $this->getLeadPermissionId();
            $leadData = [];
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $projectTypes = $this->getProjectTypes();
            $lead = ProjectLead::whereIn('project_lead.id',$ids)
            ->leftJoin('project_technology as tech', 'project_lead.technology', '=', 'tech.id')
            ->leftJoin('companies as company', 'project_lead.company_id', '=', 'company.id')
            ->leftJoin('users as projectmanager', 'project_lead.project_manager', '=', 'projectmanager.id')
            ->leftJoin('users as reviewer', 'project_lead.date_given_by', '=', 'reviewer.id')
            ->leftJoin('users as teamleader', 'project_lead.team_leader', '=', 'teamleader.id')
            ->leftJoin('users as added_by', 'project_lead.created_by', '=', 'added_by.id')
            ->leftjoin('project_types', 'project_lead.project_types', '=', 'project_types.id')
            ->leftJoin('lead_attachments as file', function($query) {
                $query->on('file.lead_id', '=', 'project_lead.id')
                ->where(function($query) {
                    $query->whereNull('file.deleted_at');
                    $query->whereNull('file.chat_id');
                    $query->WhereNull('file.docs_type');
                });
            })
            ->whereNull('project_lead.deleted_at')->groupBy('project_lead.id')
            ->select('project_lead.id as id','project_lead.lead_code','project_lead.source','project_lead.created_by','project_lead.reviewer_end_date',
                'company.company_name','project_lead.created_at','project_lead.updated_at','project_types.project_type_name','project_lead.client_name as client_name',
                'lead_title as title','tech.name as tech_name',
                DB::raw('CONCAT(projectmanager.first_name, " ", projectmanager.last_name) as project_manager_name'),
                DB::raw('CONCAT(reviewer.first_name, " ", reviewer.last_name) as reviewer_name'),  
                DB::raw('CONCAT(teamleader.first_name, " ", teamleader.last_name) as team_leader_name'),
                DB::raw('CONCAT(added_by.first_name, " ", added_by.last_name) as added_by'),
                'estimation_end_date as end_date','project_lead.estimation_status as status',DB::raw("COUNT(file.id) total_file"));
            $lead_id = $lead->pluck('project_lead.id')->toArray();
            $searchData = clone $lead;
            if (!isset($request->search_status)) {
                $lead->where('project_lead.estimation_status', '!=' ,3);
            }
            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_lead_name') && $request->search_by_lead_name != '') {
                    $lead->where('project_lead.lead_title',$request->search_by_lead_name);
                }
                if ($request->has('search_by_lead_code') && $request->search_by_lead_code != '') {
                    $lead->where('project_lead.lead_code',$request->search_by_lead_code);
                }
                if ($request->has('search_by_client') && $request->search_by_client != '') {
                    $lead->where('project_lead.client_name',$request->search_by_client);
                }
                if ($request->has('search_by_added') && $request->search_by_added != '') {
                    $lead->where(DB::raw('CONCAT(added_by.first_name, " ", added_by.last_name)'),$request->search_by_added);
                }
                if ($request->has('search_by_project_manager') && $request->search_by_project_manager != '') {
                    $lead->where(DB::raw('CONCAT(projectmanager.first_name, " ", projectmanager.last_name)'), $request->search_by_project_manager);
                }
                if ($request->has('search_by_team_leader') && $request->search_by_team_leader != '') {
                    $lead->where(DB::raw('CONCAT(teamleader.first_name, " ", teamleader.last_name)'),  $request->search_by_team_leader);
                }
                if ($request->has('search_project_type') && $request->search_project_type != '') {
                    $lead->where('project_types.project_type_name',$request->search_project_type);
                }
                if ($request->has('search_status')) {
                    if($request->search_status == '') {
                        $lead->where('project_lead.estimation_status', '!=' ,3);
                    } 
                    else if ( $request->search_status != 0 ) {
                        $lead->where('project_lead.estimation_status', $request->search_status);
                    } else {}
                }
                if ($request->has('search_by_technology') && $request->search_by_technology != '') {
                    $lead->where('tech.name', $request->search_by_technology);
                }
                if ($request->has('search_by_company') && $request->search_by_company != '') {
                    $lead->where('project_lead.company_id', $request->search_by_company);
                }
            }
        
            $unreadMsg = $this->getUnreadMessage($lead_id);
            $searchData = $searchData->orderBy('id', 'desc')->get()->toArray();
            $companies = new Company();
            $companies = $companies->getCompanyName();
            $lead_name = $lead_code = $client_name = $added_by = $project_manager = $team_leader = $technology = array();
            if(count($searchData)>0)
            {
                foreach($searchData as $key => $value){
                    $lead_name[$key] = $value['title'];
                    $client_name[$key] = $value['client_name'];
                    $added_by[$key] = $value['added_by'];
                    $project_manager[$key] = $value['project_manager_name'];
                    $team_leader[$key] = $value['team_leader_name'];
                    $technology[$key] = $value['tech_name'];
                    $lead_code[$key] = $value['lead_code'];
                }
            }
            if ($request->has('sort') && $request->input('sort') != '') {
                $data = $lead->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $data = $lead->sortable()->orderBy('project_lead.id','desc')->paginate($page_limit);
            }
            return view('pms.lead.index', compact('data','unreadMsg','request','lead_name','select_team_leader', 'lead_code','client_name', 'added_by','project_manager','team_leader','technology','projectTypes','companies'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function onGoingLead(Request $request)
    {
        if(Auth::check() && Auth::user()->hasPermission('lead.listing') && (Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.account_manager_slug')))){
            $leadData = [];
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $projectTypes = $this->getProjectTypes();
            $lead = ProjectLead::leftJoin('project_technology as tech', 'project_lead.technology', '=', 'tech.id')
            ->leftJoin('companies as company', 'project_lead.company_id', '=', 'company.id')
            ->leftJoin('users as projectmanager', 'project_lead.project_manager', '=', 'projectmanager.id')
            ->leftJoin('users as teamleader', 'project_lead.team_leader', '=', 'teamleader.id')
            ->leftJoin('users as added_by', 'project_lead.created_by', '=', 'added_by.id')
            ->leftjoin('project_types', 'project_lead.project_types', '=', 'project_types.id')
            ->whereNull('project_lead.deleted_at')->groupBy('project_lead.id')
            ->select('project_lead.id as id','project_lead.source','project_lead.created_by',
                'project_lead.reviewer_end_date','project_lead.created_at','project_lead.updated_at',
                'company.company_name','project_types.project_type_name','project_lead.client_name as client_name',
                'lead_title as title','tech.name as tech_name',
                DB::raw('CONCAT(projectmanager.first_name, " ", projectmanager.last_name) as project_manager_name'),
                DB::raw('CONCAT(teamleader.first_name, " ", teamleader.last_name) as team_leader_name'),
                DB::raw('CONCAT(added_by.first_name, " ", added_by.last_name) as added_by'),
                'estimation_end_date as end_date','project_lead.estimation_status as status');
            $lead_id = $lead->pluck('project_lead.id')->toArray();

            $searchData = clone $lead;
            if (!($request->has('search_status')) || $request->search_status == '') {
                $lead->whereIn('project_lead.estimation_status', [1,2]);
            }

            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_lead_name') && $request->search_by_lead_name != '') {
                    $lead->where('project_lead.lead_title',$request->search_by_lead_name);
                }
                if ($request->has('search_by_client') && $request->search_by_client != '') {
                    $lead->where('project_lead.client_name',$request->search_by_client);
                }
                if ($request->has('search_by_added') && $request->search_by_added != '') {
                    $lead->where(DB::raw('CONCAT(added_by.first_name, " ", added_by.last_name)'),$request->search_by_added);
                }
                if ($request->has('search_by_project_manager') && $request->search_by_project_manager != '') {
                    $lead->where(DB::raw('CONCAT(projectmanager.first_name, " ", projectmanager.last_name)'), $request->search_by_project_manager);
                }
                if ($request->has('search_by_team_leader') && $request->search_by_team_leader != '') {
                    $lead->where(DB::raw('CONCAT(teamleader.first_name, " ", teamleader.last_name)'),  $request->search_by_team_leader);
                }
                if ($request->has('search_project_type') && $request->search_project_type != '') {
                    $lead->where('project_types.project_type_name',$request->search_project_type);
                }
                if ($request->has('search_status') && $request->search_status != 0) {
                    $lead->where('project_lead.estimation_status', $request->search_status);
                }
                if ($request->has('search_by_technology') && $request->search_by_technology != '') {
                    $lead->where('tech.name', $request->search_by_technology);
                }
                if ($request->has('search_by_company') && $request->search_by_company != '') {
                    $lead->where('project_lead.company_id', $request->search_by_company);
                }
            }

            $companies = new Company();
            $companies = $companies->getCompanyName();
            $searchData = $searchData->orderBy('id', 'desc')->get()->toArray();
            $project_manager = $this->getPmList('all');
            $team_leader = $this->getTlList('all');
            $technology = $this->getTechnology();
            $lead_name = $client_name = $added_by = array();
            if(count($searchData)>0)
            {
                foreach($searchData as $key => $value){
                    $lead_name[$key] = $value['title'];
                    $client_name[$key] = $value['client_name'];
                    $added_by[$key] = $value['added_by'];
                }
            }

            if ($request->has('sort') && $request->input('sort') != '') { 
                $data = $lead->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $data = $lead->sortable()->orderBy('project_lead.created_at')->paginate($page_limit);
            }

            return view('pms.lead.on_going_lead', compact('data','request','lead_name', 'client_name', 'added_by','project_manager','team_leader','technology','projectTypes','companies'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function create()
    {
        if(Auth::check() && Auth::user()->hasPermission('lead.create')){
            $team_leader = array();
            $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug'))) ? config('constant.company.shaligram') : (Auth::user()->company_id);
            $companies = new Company();
            $companies = $companies->getCompanyName();
            $projectTypes = $this->getProjectTypes();
            $tech = $this->getProjectTechnology();
            $projectManagerList =  $this->getPmList($companyId);
            if (old('company_id')) {
                $projectManagerList =  $this->getPmList(old('company_id'));
            }
            if (old('project_manager') && old('project_manager')!= '' ) {
                $team_leader = $this->getTlList(old('project_manager'));
            }
            return view('pms.lead.create', compact('projectManagerList','team_leader', 'projectTypes','tech','companies'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function store(Request $request)
    {
        if(Auth::check() && Auth::user()->hasPermission('lead.create')){
            $rules = [];
            $company_id = Auth::user()->company_id;
            $requestData = $request->toArray();
            $rules['lead_title'] = 'required';
            $rules['technology'] = 'required';
            $rules['client_name'] = 'required';
            $rules['project_manager'] = 'required';
            $rules['company_id'] = 'required';
            $rules['project_types'] = 'required';
            $rules['estimation_status'] = 'required';
            $rules['project_attachment'] = 'max:10';
            $rules['project_attachment.*'] = 'mimes:pdf,xls,xlsx,doc,docx,jpeg,png,jpg,psd,mp4,mkv,zip,ppt,pptx,pptm';
            $rules['description'] = 'required';
            $messages = [
                'project_attachment.max' => "Please select maximum 10 documents to upload",
            ];
            $validator = Validator::make($requestData, $rules, $messages);
            if ($validator->fails()) {
                return Redirect::to('/lead/create')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try{
                    $attachFile = array();
                    $lead = new ProjectLead();
                    $lead->lead_title = isset($request->lead_title) ? $request->lead_title : '';
                    $lead->technology = isset($request->technology) ? $request->technology : 0;
                    $lead->client_name = isset($request->client_name) ? $request->client_name : '';
                    $lead->company_id = isset($request->company_id) ? $request->company_id : 0;
                    $lead->project_manager = isset($request->project_manager) ? $request->project_manager : 0;
                    $lead->team_leader = isset($request->team_leader) ? $request->team_leader : 0;
                    $lead->project_types = isset($request->project_types) ? $request->project_types : 0;
                    $lead->estimation_end_date = isset($request->estimation_end_date) ? date('Y-m-d',strtotime($request->estimation_end_date)) : null;
                    $lead->estimation_status = isset($request->estimation_status) ? $request->estimation_status : 0;
                    $lead->description = isset($request->description) ? $request->description : '';
                    $lead->source = isset($request->source) &&  $request->source == "on"? 1 : 0;
                    $lead->created_by = Auth::user()->id;
                    $lead->save();
                    $insertedId = $lead->id;
                    $updateLeadCode = ProjectLead::find($insertedId);
                    $updateLeadCode->lead_code = $this->getLeadCode($insertedId);
                    $updateLeadCode->save();
                    if ($request->hasFile('project_attachment') && isset($request->project_attachment) && !empty($request->project_attachment)) {
                        $destinationPath = public_path('/upload/leads');
                        foreach ($request->project_attachment as $key => $attachmentFile) {
                            $leadAttachement = new LeadAttachment();

                            $fullName = $attachmentFile->getClientOriginalName();
                            $name = explode('.', $fullName)[0];
                            $extension = $attachmentFile->getClientOriginalExtension();
                            $projectFile = 'document_' . $name . '_' . rand() . '.' . $extension;
                            $attachmentFile->move($destinationPath, $projectFile);
                            $attachFile[$key] = [$projectFile,$name.'.'.$extension];
                            $leadAttachement->file_name = $projectFile;
                            $leadAttachement->created_by = Auth::user()->id;
                            $leadAttachement->lead_id = $insertedId;
                            $leadAttachement->save();
                        }
                    }
                    Alert::success('Lead Successfully Created.');
                    $this->sendLeadMail($insertedId,$attachFile,'create');
                    return Redirect::to('/lead');
                }catch(Exception $e){
                    Alert::error('Lead Not Successfully Created.');
                    return Redirect::back(); 
                }
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function edit($id)
    {
        if(Auth::check() && Auth::user()->hasPermission('lead.create')){
            $valid = $this->verifyLeadUpdatePermission($id);
            if($valid == true){
                $lead = ProjectLead::where('id',$id)->first();
                $team_leader = array();
                $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug'))) ? config('constant.company.shaligram') : (Auth::user()->company_id);
                $projectTypes = $this->getProjectTypes();
                $tech = $this->getProjectTechnology();
                $companies = new Company();
                $companies = $companies->getCompanyName();
                $projectManagerList =  $this->getPmList($lead['company_id']);
                if (old('company_id')) {
                    $projectManagerList =  $this->getPmList(old('company_id'));
                }
                $team_leader = $this->getTlList($lead['project_manager']);
                return view('pms.lead.edit', compact('projectManagerList','team_leader', 'projectTypes','lead','tech','companies'));
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function update(Request $request)
    {
        if(Auth::check() && Auth::user()->hasPermission('lead.create')){
            $rules = [];
            $company_id = Auth::user()->company_id;
            $requestData = $request->toArray();
            $rules['lead_id'] = 'required';
            $rules['lead_title'] = 'required';
            $rules['technology'] = 'required';
            $rules['client_name'] = 'required';
            $rules['project_manager'] = 'required';
            $rules['company_id'] = 'required';
            $rules['project_types'] = 'required';
            $rules['estimation_status'] = 'required';
            $rules['description'] = 'required';
            $validator = Validator::make($requestData, $rules);
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $id = $request->lead_id;
                $valid = $this->verifyLeadUpdatePermission($id);
                if($valid == true){
                    try{
                        $lead = ProjectLead::find($id);
                        $oldLead = clone $lead;
                        $lead->lead_title = isset($request->lead_title) ? $request->lead_title : '';
                        $lead->technology = isset($request->technology) ? $request->technology : 0;
                        $lead->client_name = isset($request->client_name) ? $request->client_name : '';
                        $lead->company_id = isset($request->company_id) ? $request->company_id : 0;
                        $lead->project_manager = isset($request->project_manager) ? $request->project_manager : 0;
                        $lead->team_leader = isset($request->team_leader) ? $request->team_leader : 0;
                        $lead->project_types = isset($request->project_types) ? $request->project_types : 0;
                        $lead->estimation_end_date = isset($request->estimation_end_date) ? date('Y-m-d',strtotime($request->estimation_end_date)) : null;
                        $lead->estimation_status = isset($request->estimation_status) ? $request->estimation_status : 0;
                        $lead->description = isset($request->description) ? $request->description : '';
                        $lead->source = isset($request->source) &&  $request->source == "on"? 1 : 0;
                        $lead->updated_by = Auth::user()->id;
                        $lead->save();
                        Alert::success('Lead Successfully Updated.');
                        if(count(array_diff($lead->toArray(), $oldLead->toArray()))> 0)
                        {
                            $this->sendLeadMail($id,[],'edit',$oldLead);
                        }
                        return Redirect::to('/lead');
                    }catch(Exception $e){
                        Alert::error('Lead Not Successfully Updated.');
                        return Redirect::back();  
                    }
                }else{
                    Alert::error('You do not have permission to perform this action!');
                    return Redirect::to('dashboard');
                }
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function deleteLeads(Request $request){
        if(isset($request->deleteLead) && $request->deleteLead != '')
        {
            $valid = $this->verifyLeadUpdatePermission($request->deleteLead);
            if($valid == true){
                $deleteLead = ProjectLead::find($request->deleteLead);
                if(isset($deleteLead))
                {
                    try{
                        $deleteLead->delete();  
                        return Response::json(array('message'=>'Lead Deleted Successfully','status'=>'success'));
                    }catch(Exception $e){
                        return Response::json(array('message'=>'Lead Not Deleted Successfully','status'=>'error'));
                    }
                }else{
                    return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
                }
            }else{
                return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
            }
        }else{
            return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
        }
    }

    ///////////////////for Chat Details And Lead Details operation

    public function getLeadDetails($lead_id){
        if(Auth::check() && Auth::user()->hasPermission('lead.listing')){
            $getLeadPermissionId = $this->getLeadPermissionId();
            if(in_array($lead_id,$getLeadPermissionId)){
                $chats = array();
                $data = ProjectLead::where('project_lead.id',$lead_id)->first()->toArray();
                $leadAttachement =  new LeadAttachment();
                $data['total_file'] = $leadAttachement->leadAttachedFileCount($lead_id);
                $data['etotal_file'] = $leadAttachement->leadEstimationFileCount($lead_id);
                $chat = LeadChat::where('lead_chat.lead_id',$lead_id)
                        ->leftJoin('lead_attachments as file', function($query) {
                            $query->on('file.chat_id', '=', 'lead_chat.id')
                            ->where(function($query) {
                                $query->whereNull('file.deleted_at');
                            });
                        })
                        ->whereNull('lead_chat.deleted_at')->orderBy('lead_chat.created_at')
                        ->select('lead_chat.chat_text as chat_text','lead_chat.sender_id as sender_id',
                        'lead_chat.reply_id as reply_id','lead_chat.mark_as_read as mark_as_read',
                        'lead_chat.created_at as created_at',DB::raw("COUNT(file.id) total_file"),DB::raw("lead_chat.id as id"))
                        ->groupBy('lead_chat.id')->get()->toArray();
                        foreach($chat as $chatVal){
                            $chats[$chatVal['id']] = $chatVal;
                        }
                        $unreadMsg = $this->getUnreadMessage($lead_id);
                        if(isset($unreadMsg[$lead_id]) && $unreadMsg[$lead_id] > 0)
                        {
                            $this->readAllMessage($lead_id);
                        }
                return view('pms.lead.lead_details',compact('data','chats','unreadMsg','lead_id'));                   
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function chatListing(Request $request){
        $lead_id = isset($request->lead_id) && $request->lead_id != "" ? $request->lead_id : 0;
        if(Auth::check() && Auth::user()->hasPermission('lead.listing')){
            $getLeadPermissionId = $this->getLeadPermissionId();
            if(in_array($lead_id,$getLeadPermissionId)){
                $chats = array();
                $data = ProjectLead::where('project_lead.id',$lead_id)->first()->toArray();
                $leadAttachement =  new LeadAttachment();
                $data['total_file'] = $leadAttachement->leadAttachedFileCount($lead_id);
                $data['etotal_file'] = $leadAttachement->leadEstimationFileCount($lead_id);
                $chat = LeadChat::where('lead_chat.lead_id',$lead_id)
                        ->leftJoin('lead_attachments as file', function($query) {
                            $query->on('file.chat_id', '=', 'lead_chat.id')
                            ->where(function($query) {
                                $query->whereNull('file.deleted_at');
                            });
                        })
                        ->whereNull('lead_chat.deleted_at')->orderBy('lead_chat.created_at')
                        ->select('lead_chat.chat_text as chat_text','lead_chat.sender_id as sender_id',
                        'lead_chat.reply_id as reply_id','lead_chat.mark_as_read as mark_as_read',
                        'lead_chat.created_at as created_at',DB::raw("COUNT(file.id) total_file"),DB::raw("lead_chat.id as id"))
                        ->groupBy('lead_chat.id')->get()->toArray();
                        foreach($chat as $chatVal){
                            $chats[$chatVal['id']] = $chatVal;
                        }
                        $unreadMsg = $this->getUnreadMessage($lead_id);
                echo view('pms.lead.lead_details.ajax_chat',compact('data','chats','unreadMsg','lead_id'));                   
            }else{
                Alert::error('You do not have permission to perform this action!');
                return Redirect::to('dashboard');
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function getUnreadMessage($lead_id)
    {
        $leadId = [];
        if(!is_array($lead_id))
        {
            $leadId[] = $lead_id;
        }
        else{
            $leadId = $lead_id;
        }
        $msgCountArray = [];
        $authId = Auth::user()->id;
        if(count($leadId)>0)
        {
            foreach($leadId as $key => $id)
            {
                $count = 0;
                $chats = LeadChat::where('lead_id',$id)->where('sender_id','!=',$authId)->get()->pluck('mark_as_read')->toArray();
                foreach($chats as $chat)
                {
                    $read_list = explode(",",$chat);
                    if(!in_array($authId,$read_list))
                    {
                        $count++;
                    }
                }
                $msgCountArray[$id] = $count;
            }
        }
        return $msgCountArray;
    }

    public function readAllMessage($lead_id = "") {
        $authId = Auth::user()->id;
        if($lead_id != "")
        {
            $chats = LeadChat::where('lead_id',$lead_id)->where('sender_id','!=',$authId)->get()->toArray();
            foreach($chats as $chat)
            {
                $read_list = explode(",",$chat['mark_as_read']);
                if(!in_array($authId,$read_list))
                {
                    $chatRead = LeadChat::find($chat['id']);
                    if ( $chatRead->mark_as_read == null)
                    {
                        $readList = $authId;
                    } else {
                        $readList = $chatRead->mark_as_read.','.$authId;
                    }
                    $chatRead->mark_as_read = $readList;
                    $chatRead->save();
                }
            }
        }
        if(isset($chat['id']))
        {
            Session::flash('chat_id', $chat['id']);
        }
    }

    public function storeChats(Request $request)
    {
        if(Auth::check() && Auth::user()->hasPermission('lead.listing')){
            $rules = [];
            $requestData = $request->toArray();
            $rules['lead_id'] = 'required';
            $rules['chat_text'] = 'required';
            $rules['chat_attachment'] = 'max:10';
            $rules['chat_attachment.*'] = 'mimes:pdf,xls,xlsx,doc,docx,jpeg,png,jpg,psd,mp4,mkv,zip,ppt,pptx,pptm';
            $messages = [
                'chat_attachment.max' => "Please select maximum 10 documents to upload",
            ];
            $validator = Validator::make($requestData, $rules, $messages);
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                try{
                    $chat = new LeadChat();
                    $chat->lead_id = isset($request->lead_id) ? $request->lead_id : 0;
                    $chat->reply_id = isset($request->reply_id) ? $request->reply_id : null;
                    $chat->chat_text = isset($request->chat_text) ? $request->chat_text : '';
                    $chat->sender_id = Auth::user()->id;
                    $chat->save();
                    $attachFile = array();
                    if ($request->hasFile('chat_attachment') && isset($request->chat_attachment) && !empty($request->chat_attachment)) {
                        $destinationPath = public_path('/upload/leads');
                        foreach ($request->chat_attachment as $key => $attachmentFile) {
                            $leadAttachement = new LeadAttachment();

                            $fullName = $attachmentFile->getClientOriginalName();
                            $name = explode('.', $fullName)[0];
                            $extension = $attachmentFile->getClientOriginalExtension();
                            $projectFile = 'document_' . $name . '_' . rand() . '.' . $extension;
                            $attachmentFile->move($destinationPath, $projectFile);
                            $attachFile[$key] = [$projectFile,$name.'.'.$extension];
                            $leadAttachement->file_name = $projectFile;
                            $leadAttachement->created_by = Auth::user()->id;
                            $leadAttachement->lead_id = $chat->lead_id;
                            $leadAttachement->chat_id = $chat->id;
                            $leadAttachement->save();
                        }
                    }
                    Session::flash('chat_id', $chat->id);
                    $this->sendChatMail($chat,$attachFile,'create');
                    return Redirect::back();
                }catch(Exception $e){
                    Alert::error('Some thing was wrong please try again.');
                    return Redirect::back();
                }
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function editChat(Request $request){
        if(Auth::check() && Auth::user()->hasPermission('lead.listing')){
            $rules = [];
            $requestData = $request->toArray();
            $rules['edt_lead_id'] = 'required';
            $rules['edt_chat_id'] = 'required';
            $rules['edt_chat_text'] = 'required';
            $validator = Validator::make($requestData, $rules);
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $chat = LeadChat::find($request->edt_chat_id);
                if(isset($chat) && $chat->lead_id == $request->edt_lead_id && $chat->sender_id == Auth::user()->id)
                {
                    $oldchat = clone $chat;
                    try{
                        $chat->reply_id = $request->edt_reply_id;
                        $chat->chat_text = isset($request->edt_chat_text) ? $request->edt_chat_text : '';
                        $chat->mark_as_read = '';
                        $chat->save();
                        Session::flash('chat_id', $chat->id);
                        if(isset($request->inform_to_email)){
                            if(count(array_diff($chat->toArray(), $oldchat->toArray()))> 0)
                            {
                                $this->sendChatMail($chat,[],'edit');
                            }
                        }
                        return Redirect::back();
                    }catch(Exception $e){
                        Alert::error('Some thing was wrong please try again.');
                        return Redirect::back();
                    }
                }
                else{
                    Alert::error('You do not have permission to perform this action!');
                    return Redirect::back();
                } 
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function deleteChat(Request $request)
    {
        if(isset($request->chat_id) && $request->chat_id != '')
        {
            $chat = LeadChat::find($request->chat_id);
            if(isset($chat))
            {
                if($chat->sender_id == Auth::user()->id)
                {
                    try{
                        $chat->delete();
                        Session::flash('chat_id', $chat->id);
                        return Response::json(array('message'=>'Chat Deleted Successfully','status'=>'success'));
                    }catch(Exception $e){
                        return Response::json(array('message'=>'Chat Not Deleted Successfully','status'=>'error'));
                    }
                }else{
                    return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
                }
            }else{
                return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
            }
        }else{
            return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
        }
    }

    public function markAsRead($ChatId)
    {
        if(Auth::check() && Auth::user()->hasPermission('lead.listing')){
            $userId = Auth::user()->id;
            $chat = LeadChat::find($ChatId);
            if($chat->sender_id != $userId)
            {
                $read_list = $chat->mark_as_read;
                $checkList = explode(",",$read_list);
                if(!in_array($userId,$checkList)){
                    if($read_list==null)
                    {
                        $readList = $userId;
                    }else{
                        $readList = $read_list.','.$userId;
                    }
                    $chat->mark_as_read = $readList;
                    $chat->save();
                    $result = array('chat_id' => $chat->id,'lead_id' => $chat->lead_id);
                    return $result;
                }
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    ///////////////////For Documentation
    public function getLeadFiles($lead_id){
        $id = $lead_id;
        $type = 'lead';
        $created_id = ProjectLead::find($lead_id)->created_by;
        $leadFile = LeadAttachment::where('lead_id',$lead_id)
                ->whereNull('deleted_at')
                ->whereNull('chat_id')
                ->WhereNull('docs_type')
                ->sortable()->orderBy('id', 'desc')->get()->toArray();
        echo view('pms.lead.modal.file_download_modal',compact('leadFile','created_id','id','type'));
    }

    public function addLeadFiles($lead_id){
        $id = $lead_id;
        $type = 'lead';
        $created_id = ProjectLead::find($lead_id)->created_by;
        echo view('pms.lead.modal.add_files',compact('created_id','id','type'));
    }

    public function updateReviewerEndDate(Request $request){
        if(Auth::check() && Auth::user()->hasPermission('lead.listing')){
            $id = $request->id;
            $lead = ProjectLead::find($id);
            if($lead->team_leader == Auth::user()->id || $lead->project_manager == Auth::user()->id){
                try{
                    $lead->reviewer_end_date = isset($request->date) ? date('Y-m-d',strtotime($request->date)) : '';
                    $lead->date_given_by = Auth::user()->id;
                    if($lead->estimation_status < 2){
                        $lead->estimation_status = 2;
                    }
                    $lead->save();
                    $this->sendReviewerDateMail($lead);
                    return Response::json(array('message'=>'Date Successfully Updated','status'=>'success'));
                }catch(Exception $e){
                    return Response::json(array('message'=>'Date Not Updated Successfully','status'=>'error'));
                }
            }else{
                return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
            }
        }else{
            return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
        }
    }

    public function updateTeamLeaderByPm(Request $request){
        if(Auth::check() && Auth::user()->hasPermission('lead.listing')){
            $lead_id = $request->lead_id;
            $lead = ProjectLead::find($lead_id);
            $oldLead = clone $lead;
            if($lead->project_manager == Auth::user()->id){
                try{
                    $lead->team_leader = (isset($request->team_leader_id)) && $request->team_leader_id ? $request->team_leader_id : 0 ;
                    $lead->save();
                    $this->sendLeadMail($lead_id,[],'edit',$oldLead);
                    return Response::json(array('message'=>'Team Leader Successfully Updated','status'=>'success'));
                }catch(Exception $e){
                    return Response::json(array('message'=>'Team Leader Not Updated Successfully','status'=>'error'));
                }
            }else{
                return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
            }
        }else{
            return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
        }
    }

    public function updateDocsPermission(Request $request){
        if(Auth::check() && Auth::user()->hasPermission('lead.listing') && (Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))){
            $id = $request->id;
            $leadEstimationDocs = LeadAttachment::find($id);
            if($leadEstimationDocs->created_by == Auth::user()->id){
                try{
                    $leadEstimationDocs->show_to_bde = 1;
                    $leadEstimationDocs->save();
                    $attachFile[0] = array($leadEstimationDocs->file_name,$leadEstimationDocs->file_name);
                    $this->sendEstimationDocsMail($id,'edit',$attachFile);
                    return Response::json(array('message'=>'Docs Permission Successfully Updated','status'=>'success'));
                }catch(Exception $e){
                    return Response::json(array('message'=>'Docs Permission Not Updated Successfully','status'=>'error'));
                }
            }else{
                return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
            }
        }else{
            return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
        }
    }

    public function getChatFiles($chat_id){
        $id = $chat_id;
        $type = 'chat';
        $created_id = LeadChat::find($chat_id)->sender_id;
        $leadFile = LeadAttachment::where('chat_id',$chat_id)
                ->whereNull('deleted_at')
                ->WhereNull('docs_type')
                ->sortable()->orderBy('id', 'desc')->get()->toArray();
        echo view('pms.lead.modal.file_download_modal',compact('leadFile','created_id','id','type'));
    }

    public function addChatFiles($chat_id){
        $id = $chat_id;
        $type = 'chat';
        $created_id = LeadChat::find($chat_id)->sender_id;
        echo view('pms.lead.modal.add_files',compact('created_id','id','type'));
    }

    public function getEstimationFiles($lead_id){
        $id = $lead_id;
        $type = 'lead';
        $created_id = ProjectLead::find($lead_id)->created_by;
        $leadFile = LeadAttachment::where('lead_id',$lead_id)
                ->whereNull('deleted_at')
                ->whereNull('chat_id')
                ->whereNotNull('docs_type');
                if(Auth::user()->hasRole(config('constant.account_manager_slug'))){
                    $leadFile = $leadFile->where('show_to_bde',1);
                }
                $leadFile = $leadFile->orderBy('docs_type', 'asc')->orderBy('id', 'desc')->get()->toArray();
        echo view('pms.lead.modal.get_estimation_docs',compact('leadFile','created_id','id','type'));
    }

    public function addEstimationFiles($lead_id){
        $id = $lead_id;
        $type = 'lead';
        $created_id = ProjectLead::find($lead_id)->created_by;
        $leadFile = LeadAttachment::where('lead_id',$lead_id)
                ->whereNull('deleted_at')
                ->whereNull('chat_id')
                ->whereNotNull('docs_type');
                if(Auth::user()->hasRole(config('constant.account_manager_slug'))){
                    $leadFile = $leadFile->where('show_to_bde',1);
                }
                $leadFile = $leadFile->orderBy('docs_type', 'asc')->orderBy('id', 'desc')->get()->toArray();
        echo view('pms.lead.modal.add_estimation_docs',compact('leadFile','created_id','id','type'));
    }

    public function updateDocs(Request $request){
        $requestData = $request->toArray();
        $valid = false;
        if(isset($requestData['lead_id']) && $requestData['lead_id']!='')
        {
            $valid = $this->verifyLead($requestData['lead_id']);
        }
        if(isset($requestData['chat_id']) && $requestData['chat_id']!=''){
            $valid = $this->verifyChat($requestData['chat_id']);
        }
        if($valid == true){
            $rules = [];
            $rules['modal_attachment'] = 'max:10';
            $rules['modal_attachment.*'] = 'mimes:pdf,xls,xlsx,doc,docx,jpeg,png,jpg,psd,mp4,mkv,zip,ppt,pptx,pptm';
            $messages = [
                'modal_attachment.max' => "Please select maximum 10 documents to upload",
            ];
            $validator = Validator::make($requestData, $rules, $messages);
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else{
                try{
                    if ($request->hasFile('modal_attachment') && isset($request->modal_attachment) && !empty($request->modal_attachment)) {
                        foreach ($request->modal_attachment as $attachmentFile) {
                            $leadAttachement = new LeadAttachment();
        
                            $fullName = $attachmentFile->getClientOriginalName();
                            $name = explode('.', $fullName)[0];
                            $extension = $attachmentFile->getClientOriginalExtension();
                            $projectFile = 'document_' . $name . '_' . rand() . '.' . $extension;
        
                            $destinationPath = public_path('/upload/leads');
                            $attachmentFile->move($destinationPath, $projectFile);
                            $leadAttachement->file_name = $projectFile;
                            $leadAttachement->created_by = Auth::user()->id;
                            if(isset($requestData['lead_id']) && $requestData['lead_id']!='')
                            {
                                $leadAttachement->lead_id = $requestData['lead_id'];
                            }
                            else{
                                $leadAttachement->chat_id =$requestData['chat_id'];
                            }
                            $leadAttachement->save();
                        }
                    }
                    Alert::success('New Documents Successfully Added.'); 
                    return Redirect::back();
                }catch(Exception $e){
                    Alert::error('Some thing was wrong please try again.');
                    return Redirect::back();
                }
            }
        }
        else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::back();
        }
    }

    public function updateEstimationDocs(Request $request){
        $requestData = $request->toArray();
        $valid = false;
        if(isset($requestData['lead_id']) && $requestData['lead_id']!='')
        {
            $valid = $this->verifyTlPm($requestData['lead_id']);
        }
        if($valid == true){
            $rules = [];
            $rules['docs_type'] = 'required';
            if(isset($requestData['docs_type']) and $requestData['docs_type'] == '5'){
                $rules['other_type'] = 'required';
            }
            $rules['modal_attachment'] = 'max:10';
            $rules['modal_attachment.*'] = 'mimes:pdf,xls,xlsx,doc,docx,jpeg,png,jpg,psd,mp4,mkv,zip,ppt,pptx,pptm';
            $messages = [
                'modal_attachment.max' => "Please select maximum 10 documents to upload",
            ];
            $validator = Validator::make($requestData, $rules, $messages);
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else{
                try{
                    $destinationPath = public_path('/upload/leads');
                    if ($request->hasFile('modal_attachment') && isset($request->modal_attachment) && !empty($request->modal_attachment)) {
                        $attachFile = array();
                        foreach ($request->modal_attachment as $key => $attachmentFile) {
                            $leadAttachement = new LeadAttachment();
        
                            $fullName = $attachmentFile->getClientOriginalName();
                            $name = explode('.', $fullName)[0];
                            $extension = $attachmentFile->getClientOriginalExtension();
                            $projectFile = 'document_' . $name . '_' . rand() . '.' . $extension;
                            $attachmentFile->move($destinationPath, $projectFile);
                            $attachFile[$key] = [$projectFile,$name.'.'.$extension];
                            $leadAttachement->file_name = $projectFile;
                            $leadAttachement->created_by = Auth::user()->id;
                            $leadAttachement->lead_id = $requestData['lead_id'];
                            $leadAttachement->docs_type = isset($request->docs_type) ? $request->docs_type : null;
                            if($leadAttachement->docs_type == 5){
                                $leadAttachement->other_type_name = isset($request->other_type) ? $request->other_type : 'Other Type';  
                            }
                            if($leadAttachement->docs_type == 2)
                            {
                                $leadAttachement->show_to_bde = isset($request->show_to_bde) ? 1 : 0;  
                            }
                            $leadAttachement->save();
                        }
                        $this->sendEstimationDocsMail($leadAttachement->id,'new',$attachFile);
                    }
                    Alert::success('New Documents Successfully Added.'); 
                    return Redirect::back();
                }catch(Exception $e){
                    Alert::error('Some thing was wrong please try again.');
                    return Redirect::back();
                }
            }
        }
        else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::back();
        }
    }

    public function deleteDocs(Request $request)
    {
        if(isset($request->dataDocsID) && $request->dataDocsID != '')
        {
            $docsId = LeadAttachment::find($request->dataDocsID);
            if(isset($docsId))
            {
                if($docsId->created_by == Auth::user()->id)
                {   
                    try{
                        $docsId->delete();  
                        return Response::json(array('message'=>'Docs Deleted Successfully','status'=>'success'));
                    }catch(Exception $e){
                        return Response::json(array('message'=>'Docs Not Deleted Successfully','status'=>'error'));
                    }
                }else{
                    return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
                }
            }else{
                return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
            }
        }else{
            return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
        }
    }
    ///////////////////For Other Listing Operation

    public function getPmList($companyId){
        $list = array();
        if(isset($companyId) && $companyId != ''){
            $roleUserArray = RoleUser::whereIn('role_id',['2','9','14'])->pluck('user_id')->toArray();
            $list = User::select('id',DB::raw('CONCAT(first_name, " ", last_name) as fullName'))->where('status', '=', 1);
            if($companyId != "all"){
                $list = $list->where('company_id',$companyId);
            }
            $list = $list->where(function ($query) use ($roleUserArray){
                $query->whereIn('id',$roleUserArray)
                ->orWhereIn('designation_id',['2','3','32']);
            })->orderBy('fullName')->get()->toArray();
        }
        
        return $list;
    }

    public function getTlList($pmId)
    {
        $list = array();
        if($pmId != "0" && $pmId != '')
        {
            $roleUserArray = RoleUser::whereIn('role_id',['10','3'])->pluck('user_id')->toArray();
            $list = User::select('id',DB::raw('CONCAT(first_name, " ", last_name) as fullName'))->where('status',1);
            if($pmId != "all"){
                $user = new User();
                $teamId = $user->getMyTeamLeader($pmId);
                $list = $list->whereIn('id',$teamId);
            }
            $list = $list->where(function ($query) use ($roleUserArray){
                $query->whereIn('id',$roleUserArray)
                ->orWhereIn('designation_id',['4','5']);
            })->orderBy('fullName')->get()->toArray();
        }
        return $list;
    }

    public function getProjectTypes()
    {
        return ProjectType::select('id', 'project_type_name')->orderBy('project_type_name')->get();
    }

    public function getProjectTechnology()
    {
        return Technology::select('id', 'name')->orderBy('name')->get();
    }

    public function verifyChat($chatId){
        $senderId = LeadChat::where('sender_id',Auth::user()->id)->pluck('id')->toArray();
        if(in_array($chatId,$senderId))
        {
            return true;
        }
        else{
            return false;
        }
    }

    public function verifyLead($leadId){
        $createdId = ProjectLead::where('created_by',Auth::user()->id)->pluck('id')->toArray();
        if(in_array($leadId,$createdId))
        {
            return true;
        }
        else{
            return false;
        }
    }

    public function verifyTlPm($leadId){
        $createdId = ProjectLead::where('team_leader',Auth::user()->id)->orWhere('project_manager',Auth::user()->id)->pluck('id')->toArray();
        if(in_array($leadId,$createdId))
        {
            return true;
        }
        else{
            return false;
        }
    }

    public function getLeadPermissionId(){
        if(Auth::user()->hasRole(config('constant.superadmin_slug'))){
            $permission_id = ProjectLead::all()->pluck('id')->toArray();
        }
        else if(Auth::user()->hasRole(config('constant.admin_slug'))){
            $permission_id = ProjectLead::where('company_id',Auth::user()->company_id)->pluck('id')->toArray();
        
        }
        else{
            $authId = Auth::user()->id;
            $permission_id = ProjectLead::where('team_leader',$authId)->orWhere('project_manager',$authId)->orWhere('created_by',$authId)->pluck('id')->toArray();
        }
        return $permission_id;
    }

    public function verifyLeadUpdatePermission($leadId = ''){
        if(Auth::user()->hasRole(config('constant.superadmin_slug'))){
            $permission_id = ProjectLead::all()->pluck('id')->toArray();
        }
        else if(Auth::user()->hasRole(config('constant.admin_slug'))){
            $permission_id = ProjectLead::where('company_id',Auth::user()->company_id)->pluck('id')->toArray();
        }
        else{
            $authId = Auth::user()->id;
            $permission_id = ProjectLead::where('created_by',$authId)->pluck('id')->toArray();
        }
        if(in_array($leadId,$permission_id))
        {
            return true;
        }
        else{
            return false;
        }
    }

/////////////////////////////////Technology

    public function listTech(Request $request){
        if(Auth::check() && (Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))){
            $page_limit        = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
            $tech = Technology::whereNull('deleted_at');
            if ($request->has('sort') && $request->input('sort') != '') {
                $data = $tech->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $data = $tech->sortable()->orderBy('id', 'desc')->paginate($page_limit);
            }
            return view('pms.lead.technology', compact('data','request'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function addTech(Request $request){
        if(Auth::check() && (Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))){
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:project_technology',
            ]);
            if ($validator->fails()) {
                return Response::json(array('message'=>$validator->errors()->all(),'status'=>'error'));
            }
            else{
                try{
                    $tech = new Technology;
                    $tech->name = $request->name;
                    $tech->save();
                    return Response::json(array('message'=>'Technology Successfully Added','status'=>'success'));
                }
                catch(Exception $e){
                    return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
                }
            }
        }else{
            return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
        }
    }

    public function getTechnology(){
        $list = array();
        $list = Technology::whereNull('deleted_at')->select('id','name')->orderBy('name')->get()->toArray();
        return $list;
    }

    public function editTech($id){
        $tech = Technology::where('id',$id)->select('id','name')->first()->toArray();
        return $tech;
    }

    public function updateTech(Request $request){
        if(Auth::check() && (Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))){
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'edit_name' => 'required|unique:project_technology,name,' . $request->id,
            ]);
            if ($validator->fails()) {
                return Response::json(array('message'=>$validator->errors()->all(),'status'=>'error'));
            }
            else{
                $tech = Technology::find($request->id);
                if(isset($tech)){
                    try{
                        $tech->name = $request->edit_name;
                        $tech->save();
                        return Response::json(array('message'=>'Technology Successfully Updated','status'=>'success'));
                    }catch(Exception $e){
                        return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
                    }
                }else{
                    return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
                }
            }
        }else{
            return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
        }
    }

    public function deleteTech(Request $request)
    {
        
        if(Auth::check() && (Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')))){
            if(isset($request->techId) && $request->techId != '')
            {
                $tech = ProjectLead::where('technology',$request->techId)->first();
                if(isset($tech))
                {
                    return Response::json(array('message'=>'The technology has already used somewhere.','status'=>'error'));
                }
                else{
                    $techId = Technology::find($request->techId);
                    if(isset($techId))
                    {
                        try{
                            $techId->delete();  
                            return Response::json(array('message'=>'Technology Deleted Successfully','status'=>'success'));
                        }catch(Exception $e){
                            return Response::json(array('message'=>'Technology Not Deleted Successfully','status'=>'error'));
                        }
                    }else{
                        return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
                    }
                }
            }else{
                return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
            }
        }else{
            return Response::json(array('message'=>'you do not have permission to perform this action!','status'=>'error'));
        }
    }

    ///////////////////////////Mail System
    public function sendChatMail($chat,$attachFile,$status){
        if(isset($chat)){
            $lead = ProjectLead::find($chat->lead_id);
            if(isset($lead))
            {
                $title = 'New message by ';
                if($status == 'edit'){
                    $title = 'Previous message updated by ';
                }
                $companyId = (isset($lead->company_id) && $lead->company_id) != 0 ? $lead->company_id : Auth::user()->company_id;
                $sendToArray = [$lead->created_by,$lead->project_manager,$lead->team_leader];
                $userData = User::select('id','email','first_name','last_name')
                            ->whereIn('id',$sendToArray)->get()->toArray();
                if(count($userData)>0){
                    $data['sender'] = Auth::user()->first_name.' '.Auth::user()->last_name;
                    $data['chat'] = $chat->chat_text;
                    foreach($userData as $key => $user)
                    {
                        if($user['id'] != $chat->sender_id){
                            $to[$key] = $user['email'];
                        }
                    }
                    if(count($to)>0){
                        $userObj = new User();
                        $cc = $userObj->getUsersHrAndAdminEmailWithCompany($companyId)->email;
                        $data['attachFile'] = $attachFile;
                        $companyEmailData =  CommonHelper::getCompanyDetails($companyId);
                        $data['link'] = url()->previous();
                        $template = 'emails.project_lead_chat';
                        $subject = $title.''.$data['sender'] ." : ".$lead->lead_title." ( ".$lead->lead_code." ) ";
                        $from['email'] = $companyEmailData->from_address;
                        $from['name'] = $companyEmailData->company_name;
                        try{
                            MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []); 
                        }catch(\Exception $e){}
                    }
                }
            }
        }
    }

    public function sendLeadMail($id,$attachFile,$status,$oldLead = array()){
        if(isset($id))
        {
            $lead = ProjectLead::find($id);
            if(isset($lead))
            {
                $data = array();
                $data = $lead->toArray();
                $data['status'] = $status;
                $title = 'New Project Lead By ';
                if($status == 'edit'){
                    $title = 'Project Lead Updated By ';
                    $data['oldLead'] = $oldLead->toArray();
                }
                $companyId = (isset($data['company_id']) && $data['company_id']) != 0 ? $data['company_id'] : Auth::user()->company_id;
                $sendToArray = [$data['project_manager'],$data['team_leader']];
                if($data['created_by'] != Auth::user()->id)
                {
                    array_push($sendToArray,$data['created_by']);
                }
                $userData = User::select('id','email','first_name','last_name')
                            ->whereIn('id',$sendToArray)->get()->toArray();
                $to = array();
                foreach($userData as $key => $user)
                {
                    if($user['id'] != Auth::user()->id){
                        $to[$key] = $user['email'];
                    }
                }
                if(count($to)>0){
                    $data['attachFile'] = $attachFile;
                    $data['sender'] = Auth::user()->first_name.' '.Auth::user()->last_name;
                    $userObj = new User();
                    $cc = $userObj->getUsersHrAndAdminEmailWithCompany($companyId)->email;
                    $companyEmailData =  CommonHelper::getCompanyDetails($companyId);
                    $data['link'] = url('/lead/details/'.$data['id']);
                    $template = 'emails.project_lead';
                    $from['email'] = $companyEmailData->from_address;
                    $from['name'] = $companyEmailData->company_name;
                    $data['regards'] = $companyEmailData->company_name;
                    $subject =$title.$data['sender'].' : '. $data['lead_title']." ( ".$data['lead_code']." ) ";
                    try{
                        MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []); 
                    }catch(\Exception $e){}
                }
            }
        }
    }

    public function sendEstimationDocsMail($id,$status,$attachFile = array()){
        if(isset($id))
        {
            $leadData = LeadAttachment::find($id);
            $lead_docs_type = config('constant.lead_docs_type');
            $lead = ProjectLead::find($leadData->lead_id);
            if(isset($lead))
            {
                $companyId = (isset($data['company_id']) && $data['company_id']) != 0 ? $data['company_id'] : Auth::user()->company_id;
                if($leadData['docs_type'] == 5){
                    $type_name = $leadData['other_type_name'];
                }else{
                    $type_name = $lead_docs_type[$leadData['docs_type']];
                }
                $title = 'New '.$type_name.' Uploaded';
                $to = array();
                if($status == 'new')
                {
                    $sendToArray = [$lead->project_manager,$lead->team_leader];
                    if($leadData->show_to_bde == 1){
                        array_push($sendToArray,$lead->created_by);
                    }
                    $userData = User::select('id','email','first_name','last_name')
                        ->whereIn('id',$sendToArray)->get()->toArray();
                    foreach($userData as $key => $user)
                    {
                        if($user['id'] != Auth::user()->id){
                            $to[$key] = $user['email'];
                        }
                    }
                }else{
                    $userData = User::select('id','email','first_name','last_name')
                        ->where('id',$lead->created_by)->first()->toArray();
                        array_push($to,$userData['email']);
                }
                if(count($to)>0){
                    $userObj = new User();
                    $cc = $userObj->getUsersHrAndAdminEmailWithCompany($companyId)->email;
                    $data['attachFile'] = $attachFile;
                    $data['lead_title'] = $lead->lead_title;
                    $data['sender'] = Auth::user()->first_name.' '.Auth::user()->last_name;
                    $companyEmailData =  CommonHelper::getCompanyDetails($companyId);
                    $data['link'] = url()->previous();
                    $template = 'emails.project_estimation_docs';
                    $subject = $title.' By '.$data['sender'].' : ' .$data['lead_title']." ( ".$lead->lead_code." ) ";
                    $from['email'] = $companyEmailData->from_address;
                    $from['name'] = $companyEmailData->company_name;
                    try{
                        MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []); 
                    }catch(\Exception $e){}
                }
            }
        }
    }

    public function sendReviewerDateMail($lead){
        if(isset($lead))
        {
            if($lead->estimation_status <= 2){
                $companyId = (isset($lead['company_id']) && $lead['company_id']) != 0 ? $lead['company_id'] : Auth::user()->company_id;
                $userData = User::select('id','email','first_name','last_name')
                        ->where('id',$lead->created_by)->first()->toArray();
                $updater = Auth::user()->first_name.' '.Auth::user()->last_name;
                if(count($userData)>0){
                    $data['lead_title'] = $lead->lead_title;
                    $userObj = new User();
                    $cc = $userObj->getUsersHrAndAdminEmailWithCompany($companyId)->email;
                    $to = $userData['email'];
                    $companyEmailData =  CommonHelper::getCompanyDetails($companyId);
                    $data['link'] = url('/lead/details/'.$lead->id);
                    $template = 'emails.project_lead_date_update';
                    $subject = 'Reviewer Date updated By '.$updater.' : '.$lead->lead_title." ( ".$lead->lead_code." ) ";
                    $data['name'] = $userData['first_name'];
                    $from['email'] = $companyEmailData->from_address;
                    $from['name'] = $companyEmailData->company_name;
                    $data['regards'] = $updater;
                    try{
                        MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []);
                    }catch(\Exception $e){}
                }
            }
        }
    }

    public function getLeadCode($id = ''){
        $code = (isset($id) and $id != '') ? ''.$id : '0';
        $count = strlen($code);
        $zero = [
            7 => 'LID',
            6 => 'LID0',
            5 => 'LID00',
            4 => 'LID000',
            3 => 'LID0000',
            2 => 'LID00000',
            1 => 'LID000000',
            0 => 'LID0000000'
        ];
        if($count<=7){
            return $zero[$count].$code;
        }
        else{
            return "LID".$code;
        }
    }

    public function updateLeadCode(){
        $j=0;
        $id = ProjectLead::where('lead_code','LID0000000')->pluck('id')->toArray();
        foreach($id as $i){
            $updateLIDCode = ProjectLead::find($i);
            $updateLIDCode->lead_code = $this->getLeadCode($i);
            $updateLIDCode->save();
            $j++;
        }
        if(count($id) == $j)
        {
            echo 'done';
        }else{
            echo $j;
        }
    }

     public function updateCheckedLeadsAjax(Request $request) {
        $action = $request['action'];
        
            $projectlead = new ProjectLead();
            $projectlead->checkedLeadsUpdate($request['leadIdArray'],$action);
        // }       
    }
}
