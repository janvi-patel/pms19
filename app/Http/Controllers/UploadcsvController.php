<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Jobs\UploadSalaryCsv;
use Illuminate\Support\Facades\Storage;
use App\Imports\UploadSalary;
use App\User;
use Validator;
use Excel;
use PDF;
use Alert;
use Config;
use App\SalaryMailLog;
use DB;
use App\Exports\ExportEmailLogs;

class UploadcsvController extends Controller {

    public function uploadcsv(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadsalaryslip')) {
            if ($request->hasFile('csvfile')) {      
                $method = strtolower($request->method());
                if ($method == 'post') {
                    $validator=Validator::make($request->all(),[
                        'csvfile'=>'required|mimes:xlsx'
                    ],$messages = [
                        'csvfile.mimes' => "The selected document must be a file of type: xlsx.",
                    ]); 
                    // if validation fails
                    if ($validator->fails()) {
                        return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
                    }
                    else {
                        $re_upload = false;
                        if(isset($request->re_upload) && $request->re_upload == 'on')
                        {
                            $re_upload = true;
                        }
                        $file = $request->file('csvfile');
                        $extension = $file->getClientOriginalExtension();
                        $filename = 'salary_'.time().'.'.$extension;
                        if ($request) {
                            Storage::disk('local')->putFileAs('', $file, $filename);
                        }
                        $importError = array();
                        Excel::import(new UploadSalary($re_upload), $filename);
                        $importError = Session::get('salarySheetImportError');
                        if(count($importError)>0){
                           Alert::success($importError[0]. ' E-Mail Sent Successfully')->persistent("Close"); 
                        }
                        $importError = Session::forget('salarySheetImportError');
                        return Redirect::to('/old/salary/logs');
                    }
                }
                else {
                    return view('pms.uploadcsv');
                }
            }
            return view('pms.uploadcsv');
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function salarySlipHtml() {
        return view('pms.salary_slip_pdf');
    }

    public function oldSalaryLogs(Request $request)
    {
        $page_limit  = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadsalaryslip')) {
            $comp_id = Auth::user()->company_id;
            $logs = SalaryMailLog::where('upload_salary_email_log.uploaded_by_comp',$comp_id)
            ->leftJoin('companies as comp','upload_salary_email_log.emp_comp','comp.id')
            ->leftJoin('users as u','upload_salary_email_log.user_id','u.id')
            ->leftJoin('users','upload_salary_email_log.uploaded_by','users.id')
            ->whereNull('upload_salary_email_log.deleted_at')->orderBy('upload_salary_email_log.entry_date','DESC')
            ->select('upload_salary_email_log.id as id',
                    'upload_salary_email_log.emp_id',
                    'upload_salary_email_log.entry_date',
                    'upload_salary_email_log.status',
                    'upload_salary_email_log.reason',
                    'upload_salary_email_log.email',
                    DB::raw('CONCAT(u.first_name, " ", u.last_name) as user_id'),
                    DB::raw('CONCAT(comp.company_name) as emp_comp'),
                    DB::raw('CONCAT(users.first_name, " ", users.last_name) as uploaded_by')    
            )->orderBy('upload_salary_email_log.entry_date','desc')
            ->orderBy('upload_salary_email_log.status','desc')
            ->orderBy('upload_salary_email_log.id','desc')
            ->groupBy('upload_salary_email_log.id');

            if(isset($request->search_by_entry_date) && !empty($request->search_by_entry_date)){
                $logs = $logs->where('upload_salary_email_log.entry_date',date('Y-m-d',strtotime($request->search_by_entry_date)));
            }
            $exportLogs = clone $logs;
            if(isset($request->search_by_status) && $request->search_by_status != ''){
                $logs = $logs->where('upload_salary_email_log.status',$request->search_by_status);
            }
            $logs = $logs->paginate($page_limit);
            if($request->submit_type == 'export_excel'){
                $request->submit_type = '';
                $exportLogs = $exportLogs->where('upload_salary_email_log.status',3)->get()->toArray();
                return Excel::download(new ExportEmailLogs($exportLogs, $request), 'Mail_Logs.xlsx');
            }else{
                return view('pms.salary_email_log.index', compact('request','logs'));
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
}
