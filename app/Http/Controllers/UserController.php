<?php

namespace App\Http\Controllers;

use App\User;
use App\Designation;
use App\Company;
use App\RoleUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use jeremykenedy\LaravelRoles\Models\Role;
use App\Imports\UsersLeaveImport;
use App\Imports\UserDataImport;
use Excel;
use Sortable;
use App\Notifications\EmpStatusChange;
use Alert;
use Response;
use App\Jobs\MailJob;
use App\Helpers\UserHelper;
use App\Project;
use App\Fields;
use App\ProjectTeam;
use App\UserWorkFromHome;
use App\EmailTemplate;
use App\Exports\ExportUserInformation;
use Illuminate\Support\Facades\Schema;
use DB;
use Illuminate\Support\Facades\Storage;
use App\InternalCompany;

class UserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('users.listing')) {
            $usersData   = array();
            $page        = $request->get('page');
            $designation = new Designation();
            $company     = new Company();
            $user        = new User();
            $getTeamUserId = array();
            $getConfirmDatePendingList = array();
            $departmentArray = array(5,8,9);
            $loggedInUserID = isset(Auth::user()->id) ? intval(Auth::user()->id) : 0;
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

            $editReporting = $reportingToList = $user->getReportingToName();
            $designationList = $designation->getDesignationName();
            $companyList     = $company->getCompanyName();
            $dataQuery = array();
            $getAllUserDataArray =  array();

            if(Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                Auth::user()->hasRole(config('constant.team_leader_slug'))||
                Auth::user()->designation_id == 16 ){
                $teamList = $user->getMyTeamList($loggedInUserID);
                foreach($teamList AS $editReportingVal){
                    $getTeamUserId[] = $editReportingVal['id'];
                }
            }

            $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug')))
                        ?$request->search_by_company
                        :Auth::user()->company_id;

            $superAdminRoleId = $this->getSuperAdminRoleId();
            $superAdminId = RoleUser::where('role_id',$superAdminRoleId)->pluck('user_id')->toArray();
            if($companyId == 0){
                $dataQuery = User::with('designation', 'reportingto', 'company')
                                ->where('users.id','!=',Auth::user()->id)
                                ->whereNotIn('users.id',$superAdminId);

            }else{
                if(Auth::user()->hasRole(config('constant.project_manager_slug')) ||
                Auth::user()->hasRole(config('constant.team_leader_slug'))||
                Auth::user()->designation_id == 16 ){
                    if(count($getTeamUserId)>0){

                        $dataQuery = User::with('designation', 'reportingto', 'company')
                                    ->whereIn('users.id',$getTeamUserId);
                    }
                }else{
                    $dataQuery = User::with('designation', 'reportingto', 'company')
                                    ->where('users.company_id', $companyId)
                                    ->where('users.id','!=',Auth::user()->id)
                                    ->whereNotIn('users.id',$superAdminId);
                }

            }


            if(!empty($dataQuery)){

                $getAllUserQue = clone $dataQuery;
                $getConfirmDatePendingList = clone $dataQuery;
                $getConfirmDatePendingList = $getConfirmDatePendingList->where('users.status',1)->whereNull('users.deleted_at')->where('employee_status',1)->whereNull('users.confirmation_date')->orderBy('first_name')->select(DB::raw('CONCAT(first_name, " ", last_name) as user_name'),'id')->get()->toArray();

                $getAllUserData = $getAllUserQue->select('from_shift')->distinct()->orderBy('from_shift', 'asc')->get();

                if(isset($getAllUserData)){
                    foreach($getAllUserData as $getAllUserDataVal){
                        $getAllUserDataArray['from_shift'][] = $getAllUserDataVal['from_shift'];
                    }
                }

                if(Auth::user()->hasRole(config('constant.superadmin_slug'))|| Auth::user()->hasRole(config('constant.admin'))){
                    if ($request->has('search_submit') && $request->search_submit != '') {
                        if ($request->has('search_by_department') && $request->search_by_department != '') {
                            $dataQuery->whereIn('users.department',($request->search_by_department));
                        }
                    }else{
                        $dataQuery->whereNotIn('users.department', $departmentArray);

                    }
                }


                if ($request->has('search_submit') && $request->search_submit != '') {
                    if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                        $param = $request->search_by_internal_company_id;
                        if(in_array("parent_company",$param) && count($param) >= 2){
                            $dataQuery->where(function ($q) use ($param) {
                                $q->whereIn('users.internal_company_id', ($param))->orWhereNull('internal_company_id');
                            });
                        } else if (in_array("parent_company",$param)) {
                            $dataQuery->WhereNull('users.internal_company_id');
                        } else {
                            $dataQuery->whereIn('users.internal_company_id', ($param));
                        }
                    }
                    if ($request->has('search_by_first_name') && $request->search_by_first_name != '') {
                        $dataQuery->where('users.first_name', 'LIKE', "%$request->search_by_first_name%");
                    }

                    if ($request->has('search_by_last_name') && $request->search_by_last_name != '') {
                        $dataQuery->where('users.last_name', 'LIKE', "%$request->search_by_last_name%");
                    }

                    if ($request->has('search_by_email') && $request->search_by_email != '') {
                        $dataQuery->where('users.email', 'LIKE', "%$request->search_by_email%");
                    }
                  //  echo "<pre>";print_r($request->daterange);exit;
                    if ($request->has('daterange') && $request->daterange != '') {

                        $rangeDateArr = explode(" - ", $request->daterange);
                       // echo "<pre>";print_r($rangeDateArr);exit;
                        $dateFrom     = date('Y-m-d 00:00:00', strtotime(str_replace(",", "", trim($rangeDateArr[0]))));
                        $dateTo       = date('Y-m-d 23:59:59', strtotime(str_replace(",", "", trim($rangeDateArr[1]))));
                        $date         = [$dateFrom, $dateTo];

                        $dataQuery->whereBetween('users.joining_date',$date);
                    }

                    if ($request->has('search_by_employee_code') && $request->search_by_employee_code != '') {
                        $emp_code = strtoupper($request->search_by_employee_code);
                        $com_srt_nm = $user->getAllCompanyShortName();
                        if(count($com_srt_nm)>0){
                            $emp_code = str_replace($com_srt_nm, "", $emp_code);
                        }
                        $dataQuery->where('users.employee_id', 'LIKE', "%$emp_code%");
                    }

                    if ($request->has('search_by_designation') && $request->search_by_designation != '') {
                        $dataQuery->where('users.designation_id', '=', $request->search_by_designation);
                    }

                    if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                        $dataQuery->where('users.reporting_to', '=', $request->search_by_reporting_to);
                    }
                    if ($request->has('search_by_department') && $request->search_by_department != '' ) {
                        $dataQuery->whereIn('users.department',($request->search_by_department));
                    }

                    if ($request->has('search_by_from_shift') && $request->search_by_from_shift != '') {
                        $dataQuery->where('users.from_shift',($request->search_by_from_shift));
                    }

                    if ($request->has('search_by_empstatus') && $request->search_by_empstatus != '') {
                          $dataQuery->where('users.employee_status',($request->search_by_empstatus));
                    }
                }

                if ($request->has('search_by_status') && $request->search_by_status != '') {
                    $dataQuery->where('users.status',($request->search_by_status));
                }else{
                    $dataQuery->where('users.status', 1);
                }


                $usersData = $dataQuery->sortable()
                            ->orderBy('id', 'desc')
                            ->paginate($page_limit);
            }

            $searchFilter['reportingToList'] = $reportingToList;
            $searchFilter['designationList'] = $designationList;
            $searchFilter['companyList']     = $companyList;
            $array['table_name'] = 'users';
            $array['user_id'] = Auth::user()->id;
            $array['sorting_column']= 'sort_number';
            $array['sorting_direction']= 'asc';
            $fields = UserHelper::getfieldlist($array);
            $array['pluck'] ='field_text';
            $fielddata = UserHelper::getfieldlist($array);

            // echo "<pre>";print_r($fields->toArray());exit;
             if(empty($fielddata)){
                $sort_data = config('constant.user_fields_default_selected');
                $fielddata = array_keys($sort_data);
             }
             if(empty($fields->toArray())){
                $data = config('constant.user_fields_default_selected');
                $k = 0;
                foreach ($data as $key => $value) {
                    $fields[$k] = array("field_name" => $key);
                    $k++;
                }
             }
           // echo "<pre>";print_r($fields);exit;
            if($request->submit_type == 'export_excel'){
                $request->submit_type = '';
                return Excel::download(new ExportUserInformation($usersData, $getAllUserDataArray, $searchFilter, $request, $companyId, $page, $editReporting), 'User_Information.xls');
            }else{
 //
                return view('pms.users.index', compact('usersData','getAllUserDataArray','searchFilter', 'request', 'page','getConfirmDatePendingList','companyId','editReporting','fielddata','fields'));
            }
        } else {
            //Session::flash('error', 'You do not have permission to perform this action!');
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function teamUser(Request $request) {
            $usersData   = array();
            $page        = $request->get('page');
            $designation = new Designation();
            $company     = new Company();
            $user        = new User();
            $getTeamUserId = array();
            $dataQuery = array();
            $loggedInUserID = isset(Auth::user()->id) ? intval(Auth::user()->id) : 0;
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

            $editReporting = $reportingToList = $user->getReportingToName();
            $designationList = $designation->getDesignationName();
            $companyList     = $company->getCompanyName();
            // if(Auth::user()->hasRole(config('constant.team_leader_slug'))||Auth::user()->designation_id == 16 ){
                $editReporting = $user->getMyTeamList($loggedInUserID);
                foreach($editReporting AS $editReportingVal){
                    $getTeamUserId[] = $editReportingVal['id'];
                }
            // }
            $companyId = (Auth::user()->hasRole(config('constant.superadmin_slug')))
                        ?$request->search_by_company
                        :Auth::user()->company_id;
            if(count($getTeamUserId)>0){
                $dataQuery = User::with('designation', 'reportingto', 'company')
                            ->whereIn('users.id',$getTeamUserId);

            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_first_name') && $request->search_by_first_name != '') {
                    $dataQuery->where('first_name', 'LIKE', "%$request->search_by_first_name%");
                }

                if ($request->has('search_by_last_name') && $request->search_by_last_name != '') {
                    $dataQuery->where('last_name', 'LIKE', "%$request->search_by_last_name%");
                }

                if ($request->has('search_by_email') && $request->search_by_email != '') {
                    $dataQuery->where('email', 'LIKE', "%$request->search_by_email%");
                }

                if ($request->has('search_by_employee_code') && $request->search_by_employee_code != '') {
                    $dataQuery->where('employee_id', 'LIKE', "%$request->search_by_employee_code%");
                }

                if ($request->has('search_by_designation') && $request->search_by_designation != '') {
                    $dataQuery->where('designation_id', '=', $request->search_by_designation);
                }
                if ($request->has('search_by_reporting_to') && $request->search_by_reporting_to != '') {
                    $dataQuery->where('reporting_to', '=', $request->search_by_reporting_to);
                }
            }
            }

            if(!empty($dataQuery)){
                $usersData = $dataQuery->sortable()
                            ->orderBy('id', 'desc')
                            ->paginate($page_limit);

            }
            $searchFilter['reportingToList'] = $reportingToList;
            $searchFilter['designationList'] = $designationList;
            $searchFilter['companyList']     = $companyList;
            $editReporting[auth()->user()->id] = [
                'id' =>  auth()->user()->id,
                'first_name' => auth()->user()->first_name,
                'last_name' => auth()->user()->last_name,
                'employee_id' => auth()->user()->employee_id,
                'reporting_to' => auth()->user()->reporting_to,
                'email' => auth()->user()->email,
                'phone' => auth()->user()->phone,
                'designation_name' => auth()->user()->designation->designation_name
            ];
            return view('pms.users.team_user', compact('usersData', 'searchFilter', 'request', 'page','companyId','editReporting'));

    }
    public function getSuperAdminRoleId()
    {
        $role = Role::where('slug','super_admin')->first(['id']);
        return ($role)?$role->id:null;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function createEmployeeCodeNew($companyId)
    {
        $companyId = $companyId;
        $company   = Company::find($companyId);
        $shortName = ($company)?$company->emp_short_name:'';
        $user = User::where('company_id',$companyId)
                    ->where('id','!=',84)
                    ->where('employee_id','REGEXP','^[0-9]+$')
                    ->orderBy('employee_id','desc')
                    ->withTrashed()
                    ->get()
                    ->first();
        if($user){
           $user=$user-> toArray();
           return $shortName.sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $user['employee_id']) + 1));
        }else{
           return $shortName.'001';
        }
    }
    public function createEmployeeCode(Request $request,$companyId = null){
        $companyId = (is_null($companyId))?$request->input('companyId'):$companyId;
        $company   = Company::find($companyId);
        $shortName = ($company)?$company->emp_short_name:'';
        $user = User::where('company_id',$companyId)
                    ->where('id','!=',84)
                    ->where('employee_id','REGEXP','^[0-9]+$')
                    ->orderBy('employee_id','desc')
                    ->withTrashed()
                    ->get()
                    ->first();
        if($user){
           $user=$user-> toArray();
           return $shortName.sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $user['employee_id']) + 1));
        }else{
           return $shortName.'001';
        }
    }
    public function getNumFromEmployeeCode($employeeCode,$companyId)
    {
        $company = Company::find($companyId);
        return str_replace($company->emp_short_name,'',$employeeCode);
    }

    public function create(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            $user_id = Auth::user()->id;
            $company   = Company::select();
            if(Auth::user()->hasRole(config('constant.superadmin_slug'))){
                $company= $company->get()->toArray();
            }else{
                $company=$company->where('id',Auth::user()->company_id)->get()->toArray();
            }
            $designation = new Designation();
            $designationData = $designation->getAllDesignationList();
            $roleList = Role::get()->toArray();
            $user = new User();
            $reportingToList = $user->getReportingToList();
            return view('pms.users.create', compact('company','designationData', 'reportingToList', 'roleList'));
        } else {
            //Session::flash('error', 'You do not have permission to perform this action!');
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // echo '<pre>'; print_r($request->input('punch_consideration'));exit;
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            //validation rules

            $rules = array(
                'designation_id' => 'required|integer',
                //'employee_id' => 'required|unique:users,employee_id',
                'employee_id' => 'required',
                'company_id' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users',
                'personal_email' => 'nullable|email|different:email',
                'password' => 'required|string|min:6',
                'confirm_password' => 'required|string|min:6|same:password',
                'phone' => 'required',
                'dob' => 'nullable|date',
                'bank_account_no' => 'nullable|numeric|digits_between:9,18',
                'ifsc_code' => 'nullable|regex:/^[A-Z]{4}0[A-Z0-9]{6}$/',
                'reporting_to' => 'required|integer',
                'available_leaves' => 'required|numeric|between:0,99.99',
                'department' => 'required|integer',
                'employee_status' => 'required|integer',
                'joining_date' => 'nullable|date',
                'confirmation_date' => 'nullable|date',
                'gratuity' => 'required|integer',
                'provident_fund' => 'required',
                'employee_join_as' => 'required',
                'trainee_probation_month' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);

            // if validation fails
            if ($validator->fails()) {
                return Redirect::to('users/create')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                // store
                $user = new User;
                $user->company_id = Input::get('company_id');
                $user->internal_company_id = isset($request->internal_company_id) && $request->internal_company_id != "" ? $request->internal_company_id : null;
                $user->designation_id = Input::get('designation_id');
                $employeeCode = $this->createEmployeeCode($request,Input::get('company_id'));
                $user->employee_id = $this->getNumFromEmployeeCode($employeeCode,Input::get('company_id'));
                //$user->employee_id = Input::get('employee_id');
                $user->first_name = Input::get('first_name');
                $user->last_name = Input::get('last_name');
                $user->email = Input::get('email');
                $user->personal_email = Input::get('personal_email');
                $user->password = Hash::make(Input::get('password'));
                $user->phone = Input::get('phone');
                $user->joining_date = date("Y-m-d", strtotime(Input::get('joining_date')));
                $user->reporting_to = Input::get('reporting_to');
                $user->department = Input::get('department');
                $user->bank_name = Input::get('bank_name');
                $user->bank_account_no = Input::get('bank_account_no');
                $user->ifsc_code = Input::get('ifsc_code');
                $user->pan_no = Input::get('pan_no');
                $user->employee_status = Input::get('employee_status');
                $user->punch_consideration = Input::get('punch_consideration');
                $user->trainee_probation_month = Input::get('trainee_probation_month');
                if($user->employee_status == 1)
                {
                    $user->confirmation_date = date("Y-m-d", strtotime(Input::get('confirmation_date')));
                }
                else{
                    $user->confirmation_date = null;
                }
                $user->gratuity = Input::get('gratuity');
                $user->provident_fund = Input::get('provident_fund');
                if(Input::get('provident_fund') == 1){
                    $user->provident_fund_amount = Input::get('provident_fund_amount');
                }else{
                    $user->provident_fund_amount = '';
                }

                $user->from_shift= date("H:i:s", strtotime(Input::get('from_shift')));
                $user->to_shift= date("H:i:s", strtotime(Input::get('to_shift')));
                $user->available_leaves = Input::get('available_leaves');
                $user->used_leaves = 0.00;
                $user->birthdate = date("Y-m-d", strtotime(Input::get('dob')));
                if($request['exclude_user'] == 'on')
                {

                $user->want_to_exclude = 1;
                }else
                {
                     $user->want_to_exclude = 0;
                }
                $user->want_to_exclude_for_salary = (isset($request['want_to_exclude_for_salary']) && $request['want_to_exclude_for_salary'] == "on") ? 1 : 0;
                $user->bonus_applicable = Input::get('bonus_applicable');
                $user->salary_structure = Input::get('salary_structure');
                $user->employee_join_as = Input::get('employee_join_as');
                $user->save();


                $project_assign_id = Project::select('id')->WhereIn('project_name',['Miscellaneous Tasks','Bench'])->where('project_status',1)->where('company_id',$user->company_id)->get()->toArray();
                if(!empty($project_assign_id)){
                    foreach($project_assign_id as $project_assign_id_val){
                        $ProjectTeam = new ProjectTeam;
                        $ProjectTeam['project_id'] = $project_assign_id_val['id'];
                        $ProjectTeam['company_id'] = $user->company_id;
                        $ProjectTeam['user_id'] = $user->id;
                        $ProjectTeam->save();
                    }
                }
                // $role = Role::where('id', '=', Input::get('role_id'))->first();
                // $user->attachRole($role);

                if($request['work_home'] == 'on' && $request['work_start_date'] != '' && $request['work_end_date'] != ''){
                    $userWorkFromHome = new UserWorkFromHome;
                    $userWorkFromHome['user_id'] = $user->id;
                    $userWorkFromHome['start_date'] = date("Y-m-d", strtotime($request['work_start_date']));
                    $userWorkFromHome['end_date'] = date("Y-m-d", strtotime($request['work_end_date']));
                    $userWorkFromHome->save();

                }

                // if ((Auth::user()->hasRole(config('constant.superadmin_slug')))) {
                    $role = Role::where('id', '=', Input::get('role_id'))->first();
                // } else {
                //     $role_details = Designation::find(Input::get('designation_id'));
                //     $role = Role::where('id', '=', $role_details->role_id)->first();
                // }
                $user->attachRole($role);
                $company     = new Company();
                $mailTemplate = new EmailTemplate();
                $userCompanyMailData  = $company->getCompanyViseMailData($user->company_id)[0];
                $from['email'] = $userCompanyMailData['from_address'];
                $from['name']  = $userCompanyMailData['company_name'];
                $data['regards'] = $userCompanyMailData['company_name'];
                // sends email about user resgistration to user
                $to = Input::get('email');
                // $to = "trushaptridhyatech@gmail.com";
                $cc = "";
                $template = 'emails.user_create';
                $subject = '';
                $mail_subject = 'Registration!';
                $module = 'add_user';
                $data['first_name'] = Input::get('first_name');
                $data['full_name'] = Input::get('first_name').' '.Input::get('last_name');
                $data['username'] = Input::get('email');
                $data['password'] = Input::get('password');
                $mailTemp = $mailTemplate->emailTemplate($user->company_id,$module);
                $data['temp'] = "";
                if($mailTemp != null)
                {
                    $keyword = [
                        'first_name'        => $data['first_name'],
                        'full_name'         => $data['full_name'],
                        'user_name'         => $data['username'],
                        'password'          => $data['password'],
                        'company_name'      => $data['regards']
                    ];
                    $subject = $mailTemp['subject'];
                    $data['temp'] = $mailTemplate->replaceKeyword($mailTemp['email_format'],$keyword);
                    $cc = $mailTemplate->sendToCC($user->company_id,$mailTemp['send_cc'],$user->reporting_to);
                }
                $subject = $subject != ''?$subject:$mail_subject;
                MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []);

                Alert::success('User Successfully Created!');
                return Redirect::to('users');
            }
        } else {

            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Auth::check() && Auth::user()->hasPermission('users.edit')) {
            //fetch designation list for designation dropdown
            $designation = new Designation();
            $designationData = $designation->getAllDesignationList();

            $roleList = Role::get()->toArray();

            $company   = Company::select();
            if(Auth::user()->hasRole(config('constant.superadmin_slug'))){
                $company= $company->get()->toArray();

            }else{
                $company=$company->where('id',Auth::user()->company_id)->get()->toArray();
            }
            $user = new User();
            $userDetails = $user->getUserDetails($id);
            if ($userDetails['birthdate'] == '' || $userDetails['birthdate'] == '0000-00-00') {
                $userDetails['birthdate'] = '';
            }
            if ($userDetails['joining_date'] == '' || $userDetails['joining_date'] == '0000-00-00 00:00:00' || $userDetails['joining_date'] == '1970-01-01 00:00:00') {
                $userDetails['joining_date'] = '';
            }

            $userDetails['from_shift']= ($userDetails['from_shift'] != '')?(date("g:iA", strtotime($userDetails['from_shift']))):'';
            $userDetails['to_shift']= ($userDetails['to_shift'] != '')?(date("g:iA", strtotime($userDetails['to_shift']))):'';
            $reportingToList = $user->getReportingToList($id);

            $companyShortName          = Company::find($userDetails['company_id']);
            $userDetails['employee_id'] = $companyShortName->emp_short_name.sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $userDetails['employee_id'])));
            $workFromHome = UserWorkFromHome::where('user_id',$id)->first();
            return view('pms.users.edit', compact('workFromHome','company','designationData', 'userDetails', 'reportingToList', 'roleList'));
        } else {
            //Session::flash('error', 'You do not have permission to perform this action!');
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function getRoleDesignation($designationID)
    {
        $designation = new Designation();
        $role_id = $designation->getDesignationDetails($designationID);
        if(count($role_id)>0)
        {
            $role = $role_id['role_id'];
        }
        else{
            $role = "13";
        }
        return $role;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, $id)
    {
        if (Auth::check() && Auth::user()->hasPermission('users.edit')) {
          $rules = array(
                'password' => 'required|string|min:6',
                'confirm_password' => 'required|string|min:6|same:password',
          );
          $validator = Validator::make(Input::all(), $rules);

          if ($validator->fails()) {
                // Alert::error('Oops, Password has some error!');
                return Redirect::to('users/edit/' . $id)
                                ->withErrors($validator)
                                ->withInput();
          } else {
                $user = User::find($id);
                $user->password = Hash::make(Input::get('password'));
                $user->save();
                Alert::success('Password Successfully Updated');
                return Redirect::to('users');
            }
        }
        else {
              Alert::error('You do not have permission to perform this action!');
              return Redirect::to('dashboard');
        }
    }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        if (Auth::check() && Auth::user()->hasPermission('users.edit')) {

            $validationRule['designation_id'] = 'required|integer';
            //$validationRule['employee_id'] = 'required|unique:users,employee_id,' . $id;
            $validationRule['employee_id'] = 'required' ;
            $validationRule['company_id'] = 'required' ;
            $validationRule['first_name'] = 'required';
            $validationRule['last_name'] = 'required';
            $validationRule['employee_id'] = 'required|unique:users,employee_id,' . $id;
            $validationRule['email'] = 'required|email|unique:users,email,' . $id;
            $validationRule['personal_email'] = 'nullable|email|different:email';
            $validationRule['phone'] = 'required';
            $validationRule['reporting_to'] = 'required|integer';
            $validationRule['available_leaves'] = 'required|numeric|between:0,99.99';
            $validationRule['used_leaves'] = 'required|numeric|between:0,99.99';
            $validationRule['department'] = 'required|integer';
            $validationRule['dob'] = 'nullable|date';
            $validationRule['provident_fund'] = 'required';
            $validationRule['employee_join_as'] = 'required';
            $validationRule['trainee_probation_month'] = 'required';


            $validator = Validator::make(Input::all(), $validationRule);

            // if validation fails
            if ($validator->fails()) {
                // Alert::error('Oops, Form has some error!');
                return Redirect::to('users/edit/' . $id)
                                ->withErrors($validator)
                                ->withInput();
            } else {
                // store

                $user = User::find($id);
                //save old data for check status is changed or not if status changed then Team Leader and Team member received email

                $getOriginal = $user->getOriginal();
                // save old data end

                if($user->company_id == Input::get('company_id')){
                    $user->employee_id = $this->getNumFromEmployeeCode(Input::get('employee_id'),Input::get('company_id'));
                }else{
                    $employeeCode = $this->createEmployeeCode($request,Input::get('company_id'));
                    $user->employee_id = $this->getNumFromEmployeeCode($employeeCode,Input::get('company_id'));
                }
                $user->designation_id = Input::get('designation_id');
                //$user->employee_id = Input::get('employee_id');
                $user->company_id = Input::get('company_id');
                $user->internal_company_id = isset($request->internal_company_id) && $request->internal_company_id != "" ? $request->internal_company_id : null;
                $user->first_name = Input::get('first_name');
                $user->last_name = Input::get('last_name');
                $user->email = Input::get('email');
                $user->personal_email = Input::get('personal_email');
                $user->phone = Input::get('phone');
                $user->joining_date = date("Y-m-d", strtotime(Input::get('joining_date')));
                $user->reporting_to = Input::get('reporting_to');
                $user->department = Input::get('department');
                $user->bank_name = Input::get('bank_name');
                $user->bank_account_no = Input::get('bank_account_no');
                $user->ifsc_code = Input::get('ifsc_code');
                $user->pan_no = Input::get('pan_no');
                $user->employee_status = Input::get('employee_status');
                $user->punch_consideration = Input::get('punch_consideration');
                $user->gratuity = Input::get('gratuity');
                $user->trainee_probation_month = Input::get('trainee_probation_month');
                if($user->employee_status == 1)
                {
                    $user->confirmation_date = date("Y-m-d", strtotime(Input::get('confirmation_date')));
                    $user->last_date = NULL;
                }else if($user->employee_status == 3){
                    $user->last_date =  date("Y-m-d", strtotime(Input::get('user_last_date')));
                }
                else{
                    $user->confirmation_date = null;
                    $user->last_date = NULL;
                }
                $user->provident_fund = Input::get('provident_fund');
                $user->bonus_applicable = Input::get('bonus_applicable');
                $user->salary_structure = Input::get('salary_structure');
                $user->employee_join_as = Input::get('employee_join_as');
                if(Input::get('provident_fund') == 1){
                    $user->provident_fund_amount = Input::get('provident_fund_amount');
                }else{
                    $user->provident_fund_amount = '';
                }

                $user->status = Input::get('user_status');

                $user->from_shift= date("H:i:s", strtotime(Input::get('from_shift')));
                $user->to_shift= date("H:i:s", strtotime(Input::get('to_shift')));
                $user->birthdate = date("Y-m-d", strtotime(Input::get('dob')));
                $user->available_leaves = Input::get('available_leaves');
                $user->used_leaves = Input::get('used_leaves');
                    if($request['exclude_user'] == 'on')
                {

                $user->want_to_exclude = 1;
                }else
                {
                     $user->want_to_exclude = 0;
                }
                $user->want_to_exclude_for_salary = (isset($request['want_to_exclude_for_salary']) && $request['want_to_exclude_for_salary'] == "on") ? 1 : 0;

                $user->save();

                $company     = new Company();
                $mailTemplate = new EmailTemplate();
                $userCompanyMailData  = $company->getCompanyViseMailData($user->company_id)[0];
                $data['regards'] = $userCompanyMailData['company_name'];
                $from['email'] = $userCompanyMailData['from_address'];
                $from['name']  = $userCompanyMailData['company_name'];

                // checks designation changes and send email to user
                if($getOriginal['designation_id'] != $user->designation_id){
                    $designation = new Designation();
                    $data_designation = $designation->getDesignationList()->where('id',$user->designation_id)->first();
                    $data_designation_old = $designation->getDesignationList()->where('id',$getOriginal['designation_id'])->first();
                    if ($data_designation && $data_designation_old){
                        $data['designation']    = $data_designation->toArray()['designation_name'];
                        $data['designation_old']= $data_designation_old->toArray()['designation_name'];
                        if ($data['designation'] && $data['designation_old']){
                            $to = Input::get('email');
                            // $to = "trushaptridhyatech@gmail.com";
                            $cc = "";
                            $data['first_name'] = Input::get('first_name');
                            $data['full_name'] = Input::get('first_name').' '.Input::get('last_name');
                            $template = 'emails.designation_updated';
                            $subject = '';
                            $mail_subject = 'Designation Updated!';
                            $module = 'update_designation';
                            $mailTemp = $mailTemplate->emailTemplate($user->company_id,$module);
                            $data['temp'] = "";
                            if($mailTemp != null)
                            {
                                $keyword = [
                                    'first_name'        => $data['first_name'],
                                    'full_name'         => $data['full_name'],
                                    'old_designation'   => $data['designation_old'],
                                    'new_designation'   => $data['designation'],
                                    'company_name'      => $data['regards']
                                ];
                                $subject = $mailTemp['subject'];
                                $data['temp'] = $mailTemplate->replaceKeyword($mailTemp['email_format'],$keyword);
                                $cc = $mailTemplate->sendToCC($user->company_id,$mailTemp['send_cc'],$user->reporting_to);
                            }
                            $subject = $subject != ''?$subject:$mail_subject;

                            MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []);
                        }
                    }
                }

                $checkWorkHomeEntry = UserWorkFromHome::where('user_id',$id)->first();
                if($checkWorkHomeEntry){
                    $userWorkHome = UserWorkFromHome::find($checkWorkHomeEntry->id);
                    if(isset($request['work_home'])){
                        $userWorkHome->start_date = date("Y-m-d", strtotime($request['work_start_date']));
                        $userWorkHome->end_date = date("Y-m-d", strtotime($request['work_end_date']));
                        $userWorkHome->save();
                    }else{
                        $userWorkHome->delete();
                    }
                }else{
                    if(isset($request['work_home'])){
                        $userWorkFromHome = new UserWorkFromHome;
                        $userWorkFromHome['user_id'] = $user->id;
                        $userWorkFromHome['start_date'] = date("Y-m-d", strtotime($request['work_start_date']));
                        $userWorkFromHome['end_date'] = date("Y-m-d", strtotime($request['work_end_date']));
                        $userWorkFromHome->save();
                    }
                }
                // checks employement status changes, probation or confirmed and send email to user
                if ($getOriginal['employee_status'] && $user->employee_status && $getOriginal['employee_status'] > $user->employee_status ){

                    $to = Input::get('email');
                    // $to = "trushaptridhyatech@gmail.com";
                    $cc = "";
                    $template = 'emails.employeement_status_changed';
                    $mail_subject = 'Confirmation about you employeement status';
                    $data['first_name'] = Input::get('first_name');
                    $data['full_name'] = Input::get('first_name').' '.Input::get('last_name');
                    $subject = '';
                    $module = 'update_employment';
                    $mailTemp = $mailTemplate->emailTemplate($user->company_id,$module);
                    $data['temp'] = "";
                    if($mailTemp != null)
                    {
                        $keyword = [
                            'first_name'        => $data['first_name'],
                            'full_name'         => $data['full_name'],
                            'company_name'      => $data['regards']
                        ];
                        $subject = $mailTemp['subject'];
                        $data['temp'] = $mailTemplate->replaceKeyword($mailTemp['email_format'],$keyword);
                        $cc = $mailTemplate->sendToCC($user->company_id,$mailTemp['send_cc'],$user->reporting_to);
                    }
                    $subject = $subject != ''?$subject:$mail_subject;
                    MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []);
                }

                //assignes role as per designation
                $role = Role::where('id', '=', Input::get('old_role_id'))->first();
                $user->detachRole($role);

                // if ((Auth::user()->hasRole(config('constant.superadmin_slug')))) {
                    $role = Role::where('id', '=', Input::get('role_id'))->first();
                // } else {
                //     $role_details = Designation::find(Input::get('designation_id'));
                //     $role = Role::where('id', '=', $role_details->role_id)->first();
                // }

                $user->attachRole($role);

                // redirect

                Alert::success('User Successfully Updated');
                return Redirect::to('users');
            }
        } else {

            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function deleteUser(Request $request)
    {
        if (Auth::check() && Auth::user()->hasPermission('users.delete')) {
            try{
                $userId = $request->input('userId');
                $user = User::find($userId);
                $user->delete();
                return Response::json(array('message'=>'User Deleted Successfully','status'=>'success'));
            }catch(Exception $e){
                // $e->getMessage()
                return Response::json(array('message'=>'User Not Deleted Successfully','status'=>'error'));
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function setReportingTo(Request $request) {
        $user = User::find($request->id);
        $user->reporting_to = $request->reporting_to;
        $user->save();
        $reportingData = User::select('id', 'first_name', 'last_name')->where('id', $request->reporting_to)->get();
        return json_encode($reportingData);
    }

    public function getReportingToAjax(Request $request) {
        $reportingToList = User::select('id', 'first_name', 'last_name')->where('id', '!=', $request->id)->get();
        return json_encode($reportingToList);
    }

    public function getReportingTo(Request $request) {
        $companyId = $request->input('companyId');
        $userId = $request->input('userId');
        $user = User::query();
        $companyId = $companyId;
        $user->where(function($query)use($companyId) {
            $query->where('company_id', $companyId)
                    ->orWhere('id', '=', 84);
        });
        $reportingToList = $user->orderBy('first_name', 'ASC')->get()->toArray();
        if (isset($userId)) {
            $user = new User();
            $userDetails = $user->getUserDetails($userId);
            return view('pis.users.reportingto_edituser')->with('reportingToList', $reportingToList)->with('userDetails', $userDetails);
        } else {
            return view('pis.users.reportingto')->with('reportingToList', $reportingToList);
        }
    }

    public function updateCheckedUser(Request $request) {
        if(Auth::user()->hasRole(config('constant.superadmin_slug')) || Auth::user()->hasRole(config('constant.admin_slug')) || Auth::user()->hasRole(config('constant.hr_slug'))) {
            $action = $request['action'];
            if($action == 2){
                $user = new User();
                $user->checkedDelete($request['userIdArrayData']);
            }
            else if($action == 3 || $action == 4){
                if($action == 3)
                {
                    $status = 1; // confirm
                }
                else{
                    $status = 2; //probation
                }
                $user = new User();
                $user->changeEmploymentStatus($request['userIdArrayData'],$status);
            }else{
                $user = new User();
                $user->checkedUpdate($request['userIdArrayData'],$action);
            }
            return response()->json(['status' => "success", 'message' => "Selected records successfully updated!"]);
        } else {
            return response()->json(['status' => "error", 'message' => "You do not have permission to perform this action!"]);
        }
    }

    public function updateReporting(Request $request) {
        $user_id = isset($request['user_id']) && $request['user_id'] != "" ? $request['user_id'] : "";
        $reporting_id = isset($request['reporting_id']) && $request['reporting_id'] != "" ? $request['reporting_id'] : "";
        if(auth()->check() && (auth()->user()->hasRole(config('constant.project_manager_slug')) || auth()->user()->hasRole(config('constant.admin_slug'))) && ($user_id != $reporting_id)) {
            if(auth()->user()->hasRole(config('constant.project_manager_slug'))){
                $teamUserArray = getTeamUserId(auth()->user()->id);
                $teamUserArray[] = auth()->user()->id;
                if(!in_array($user_id,$teamUserArray)) {
                    return response()->json(['status' => "error", 'message' => "This User is not part of your Team!"]);
                }
                if(!in_array($reporting_id,$teamUserArray)) {
                    return response()->json(['status' => "error", 'message' => "This Reporting person is not part of your Team!"]);
                }
            }
            $user = User::find($user_id);
            $user->reporting_to = $reporting_id;
            $user->save();
            return response()->json(['status' => "success", 'message' => "Reporting Person Successfully Updated!"]);
        } else {
            return response()->json(['status' => "error", 'message' => "You do not have permission to perform this action!"]);
        }
    }

    public function EmailTemplate(Request $request)
    {
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            $emailTemp = new EmailTemplate();
            $emailTemp = $emailTemp->where('company_id',Auth::user()->company_id)->get();
            $dataArray = array();
            foreach($emailTemp as $DataVal){
                $dataArray[$DataVal['module']] = $DataVal;
            }
            return view('pms.users.update_email',compact('dataArray'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function updateEmail(Request $request)
    {
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            $rules = array(
                'subject' => 'required',
                'email_format' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            // if validation fails
            if ($validator->fails()) {
                return Redirect::to('users/email/update')
                                ->withErrors($validator)
                                ->withInput();
            } else {
                $email_type = $request->input()['form_type'];
                $setMailTemplate = EmailTemplate::where('company_id',Auth::user()->company_id)
                                ->where('module',$email_type);
                $mailTemplate  = clone $setMailTemplate;
                if(count($mailTemplate->get()))
                {
                    try{
                        $send_cc = '';
                        if((isset($request->input()['send_cc'])) && $request->input()['send_cc'] != null)
                        {
                            $ccData = $request->input()['send_cc'];
                            foreach($ccData as $val){
                                $send_cc .= $val;
                            }
                        }
                        $subject = (isset($request->input()['subject']))?$request->input()['subject']:'';
                        $email_format = (isset($request->input()['email_format']))?$request->input()['email_format']:'';
                        $setMailTemplate->update(['send_cc' => $send_cc,'subject'=>$subject,'email_format'=>$email_format]);
                        Alert::success('Template Successfully Updated!');
                        return Redirect::back();
                    }catch(Exception $e){
                        Alert::error('Template Not Updated Successfully');
                        return Redirect::back();
                    }
                }
                else{
                    Alert::error('Someting Was Wrong!');
                    return Redirect::back();
                }
            }
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function LeaveUploadView(){
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            return view('pms.users.user_leave_upload');
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

    public function UserLeaveUpload(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('users.create')) {
            if ($request->hasFile('csvfile')) {
                $validator=Validator::make($request->all(),[
                    'csvfile'=>'required|mimes:xlsx,xls'
                ]);
                if ($validator->fails()) {
                    Alert::error('Oops, Invalid file type or file seems to be missing!');
                    return Redirect::to('users/leave/upload/view')
                    ->withErrors($validator)
                    ->withInput();
                }
                else {
                    $file = $request->file('csvfile');
                    $extension = $file->getClientOriginalExtension();
                    $filename = 'user_leave_'.time().'.'.$extension;
                    if ($request) {
                        Storage::disk('local')->putFileAs('', $file, $filename);
                    }
                    $importError = array();
                    Excel::import(new UsersLeaveImport, $filename);
                    $importError = Session::get('UserLeaveImportError');
                    // Session::forget('UserLeaveImportError');
                    $type = 'success';
                    if(count($importError)>0){

                            $message = '';
                          if($importError[0]['success'] != ''){
                              $count = $importError[0]['success'];
                                $message = ''.$count . ' Record Successfully Updated!';
                            }

                          if(count($importError[0]['companyError'])>0){
                                $message .= implode(", ",$importError[0]['companyError']);
                                $type = 'warning';
                            }
                     // alert()->$type($message)->persistent('close')->autoclose("3600");
                       Alert::$type($message)->persistent("Dismiss");
                    }


                    $importError = Session::forget('UserLeaveImportError');
                    return Redirect::to('users/leave/upload/view');
                }
            }
            return Redirect::to('users/leave/upload/view');
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
      public function ImportuserModal(){
      // if (Auth::check() && Auth::user()->hasPermission('pms.upload.docs')) {
            echo view('pms.users.import_userinformation_modal');
        // }else{
        //     return Response::json(array('message'=>'Something Was Wrong!','status'=>'error'));
        // }
    }
    public function showUserDetails(Request $request){
            $id = $request->id;
            $designation = new Designation();
            $designationData = $designation->getAllDesignationList();

            $roleList = Role::get()->toArray();

            $company   = Company::select();
            if(Auth::user()->hasRole(config('constant.superadmin_slug'))){
                $company= $company->get()->toArray();

            }else{
                $company=$company->where('id',Auth::user()->company_id)->get()->toArray();
            }

            $user = new User();
            $userDetails = $user->getUserDetails($id);
            if ($userDetails['birthdate'] == '' || $userDetails['birthdate'] == '0000-00-00') {
                $userDetails['birthdate'] = '';
            }
            if ($userDetails['joining_date'] == '' || $userDetails['joining_date'] == '0000-00-00 00:00:00' || $userDetails['joining_date'] == '1970-01-01 00:00:00') {
                $userDetails['joining_date'] = '';
            }

            $userDetails['from_shift']= ($userDetails['from_shift'] != '')?(date("g:iA", strtotime($userDetails['from_shift']))):'';
            $userDetails['to_shift']= ($userDetails['to_shift'] != '')?(date("g:iA", strtotime($userDetails['to_shift']))):'';
            $reportingToList = $user->getReportingToList($id);

            $companyShortName          = Company::find($userDetails['company_id']);
            $userDetails['employee_id'] = $companyShortName->emp_short_name.sprintf("%03d", (preg_replace("/[^0-9 ]/", '', $userDetails['employee_id'])));
            $workFromHome = UserWorkFromHome::where('user_id',$id)->first();
           // echo"<pre>";print_r($userDetails);exit;

       $html = view('pms.users.model.user_details', compact('workFromHome','company','designationData', 'userDetails', 'reportingToList', 'roleList'))->render();
       return Response::json(array('status' => 'success', "html" => $html));
    }
    public function StoreImportUserData(Request $request)
    {
        ini_set('memory_limit', '-1');
        // if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry')) {
            $method = strtolower($request->method());
            if ($method == 'post') {
                $validator = Validator::make($request->all(), [
                         'xlsfile' => 'required|mimes:xls,xlsx'
                ]);

                $file = $request->file('xlsfile');

                $extension = $file->getClientOriginalExtension();
                $filename = 'user_imported_data_' . time() . '.' . $extension;
                if ($request) {
                    Storage::disk('local')->putFileAs('', $file, $filename);
                }
                Excel::import(new UserDataImport, $filename);
                Alert::success('User Data Successfully Uploaded.');

                return Redirect::to('/users');
            } else {
                return view('pms.index');
            }
        // } else {
        //     Alert::error('You do not have permission to perform this action!');
        //     return Redirect::to('dashboard');
        // }
    }
    public function dynamicFields(Request $request){
        if (Auth::check() && Auth::user()->hasRole(config('constant.admin')) || Auth::user()->hasRole(config('constant.hr_slug'))) {

            $array['table_name'] = 'users';
            $array['user_id'] = Auth::user()->id;
            $array['pluck'] ='field_name';
            $field_id = UserHelper::getfieldlist($array);
             $fields = Schema::getColumnListing('users');
             $excludeFields = array('id','company_id','email_verified_at','password','is_admin','remember_token','created_at','updated_at','deleted_at');
             $final_data = [];
             $sort_data = [];
             foreach ($fields as $key => $value) {
                if(!in_array($value,$excludeFields)){
                    $final_data[$value] = ucwords(str_replace('_',' ',$value));
                    $data = UserHelper::getfieldlist(['table_name' => 'users','user_id' => Auth::user()->id,'field_name' => $value,'first' => 1]);
                    if(isset($data) && !empty($data)){
                        $sort_data[$value]= $data->sort_number;
                    }else{
                        $sort_data[$value] = '';
                    }
                }

             }
             if(empty($field_id)){
                $sort_data = config('constant.user_fields_default_selected');
                $field_id = array_keys($sort_data);
             }
             if(empty($sort_data)){
                $sort_data = config('constant.user_fields_default_selected');
             }
           // / echo "<pre>";print_r($field_id);exit;
            return view('pms.users.UpdateUserField',compact('field_id','final_data','sort_data'));
        } else {

            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
    public function dynamicFieldsStore(Request $request){
        if (Auth::check() && Auth::user()->hasRole(config('constant.admin')) || Auth::user()->hasRole(config('constant.hr_slug'))) {
            $fields = Schema::getColumnListing('users');
             $final_data = [];
             foreach ($fields as $key => $value) {
                 $final_data[$value] = ucwords(str_replace('_',' ',$value));
             }
            $fields = UserHelper::getfieldlist(['table_name' => 'users','user_id' => Auth::user()->id]);
            foreach($fields as $value)
            {
                $value->delete();
            }

            $FieldData=$request->update_user_row;
          //  echo "<pre>";print_r($request->all());exit;
            if(isset($FieldData) && !empty($FieldData))
            {
                foreach($FieldData as $val)
                {
                    $columnData = "sort_column_".$val;
                    UserHelper::storeFieldTable(["field_name" => $val,"field_text" => $final_data[$val],"table_name" => 'users',"sort_number" => $request->$columnData,"user_id" => Auth::user()->id]);
                }
            }
            alert()->success('Display Fields Changed Successfully')->persistent('close')->autoclose("36000");
            return redirect('/users');
        } else {

            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }

}
