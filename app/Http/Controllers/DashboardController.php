<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Holiday;
use App\User;
use App\Leave;
use App\Designation;
use App\Attendance;
use App\ProjectUser;
use App\Project;
use App\Company;
use App\AttendanceDetail;
use App\Cr;
use App\CompOff;
use App\Client;
use App\TaskEntry;
use App\Task;
use Session;
use Mail;
use Carbon\Carbon;
use Sortable;
use Alert;
use App\ProjectLead;
use DB;

// use App\Jobs\MailJob;


class DashboardController extends Controller
{
    public function versionone(Request $request)
    {
        $userIdArray = array();
        $authUser = Auth::user();
        $loggedInUserID = isset($authUser->id) ? intval($authUser->id) : 0;
        $todayLeaveListing = $upcomingLeaveListing = $pendingLeaveListing = $employeeAllLeaves = $employeeLeaves = array();
        if ($loggedInUserID) {
            $leave = new Leave();
            $company = new Company();
            $taskEntry = new TaskEntry();
            $project = new ProjectUser();
            $pro = new Project();
            $holiday = new Holiday();
            $client = new Client();
            $user   = new User();
            $attendanceObj   = new Attendance();
            $average_total = 0;
            $monthly_working_hours = '00:00';
            $pending_leaves = getLeaveCount(['column' => "leave_days","whereGreterOrEqual"=>["column" => "leave_date","query" => date("Y-m-d")],"where" =>["leave_status"=>2,"user_id"=>$loggedInUserID]]);
            $taken_leaves = getLeaveCount(['column' => "leave_days","whereGreterOrEqual"=>["column" => "leave_date","query" => getUsersFinancialYearStartDate($loggedInUserID)],"where" =>["leave_status"=>1,"user_id"=>$loggedInUserID]]);
            $total_leaves['taken_leaves'] = getLeaveCount(['column' => "leave_days","whereBetween"=>["column" => "leave_date","query" => [getUsersFinancialYearStartDate($loggedInUserID),date("Y-m-d")]],"where" =>["leave_status"=>1,"user_id"=>$loggedInUserID]]);
            $total_leaves['future_leave_taken'] = getLeaveCount(['column' => "leave_days","whereGreterOrEqual"=>["column" => "leave_date","query" => date("Y-m-d")],"where" =>["leave_status"=>1,"user_id"=>$loggedInUserID]]);
            $currentYear = date('Y');
            $searchFilter['request_year'] = $currentYear;
            $holidaysData = $holiday->getHolidayList($searchFilter);
            $userLeave = $user->getUserDetails($loggedInUserID);
            // $compoffdata = CompOff::where('user_id',$userLeave['id'])->where('compoff_marked_as',2)->orderBy('id','desc')->get();
            $remainingLeave = $userLeave['available_leaves'] - $taken_leaves;
            $remainingLeave = ($remainingLeave > 0 ) ? $remainingLeave : 0;
            // $compoffleave = 0;

            //    if(!empty($compoffdata)){

            //         foreach($compoffdata as $key=>$compoff)
            //          {

            //             if($compoff->leave_used != null){
            //                 $comma = ',';
            //                 if(strpos($compoff->leave_used,$comma) == true ) {

            //                     $compoffArry = explode(",",$compoff->leave_used);


            //                     foreach($compoffArry as $key=>$cof){
            //                         if($cof == 0){
            //                          $compoffleave++;

            //                         }
            //                     }
            //                 }else{


            //                         if($compoff->leave_used==0){

            //                          $compoffleave++;
            //                         }
            //                     }
            //             }
            //         }
            //     }

            $startDate = getUsersFinancialYearStartDate($userLeave['id']);
            $compoffdata = CompOff::where('user_id',$userLeave['id'])->where('compoff_start_date', '>=', $startDate)->where('compoff_marked_as',2)->orderBy('id','desc')->get();

            $leaveData = [
                'remaningLeave' => $remainingLeave - $pending_leaves,
                'usedLeave' => $taken_leaves,
                'leavePendingToApprove' => $pending_leaves,
                'compoffleave' => getUsersCompoffData($compoffdata)
            ];
            $searchFilter['date'] = date('Y-m-d');
            if(!Auth::user()->hasRole(config('constant.superadmin_slug'))){
                $searchFilter['company_id'] = $authUser->company_id;
            }
             $todayLeaveListing = $leave->getEmployeeLeaveListing($searchFilter);
             $upcomingLeaveListing = $leave->getUpcomingLeaveListing($searchFilter);
            $attendances =  Attendance::query();
            $companyName = $company->getCompanyNameById($authUser->company_id)->company_name;

            if(Auth::user()->hasRole(config('constant.hr_slug'))){
                $birthdayList = $user->getUsersBirthDayOnPreviousHoliday($searchFilter['company_id']);
                $userListingWithJoiningDateOnly = userListingWithJoiningDateOnly($searchFilter['company_id']);
                $totalUser = count($user->getAllUserListByCompany($searchFilter['company_id']));
                $traineeList = $user->getTraineeList($searchFilter['company_id']);
                $onProbationList = $user->getEmpListOnProbation($searchFilter['company_id']);
                $remaningEntry = $attendanceObj->getRemainingTimeEntries(Carbon::now()->month,Carbon::now()->year,Carbon::now()->day,$searchFilter['company_id']);
                $resignUser = User::where('employee_status',3)->where('last_date','>=',date('Y-m-d'))->where('company_id',$searchFilter['company_id'])->where('status',1)->orderBy('last_date','ASC')->get();
                $joiningUser = User::with('reportingto','designation')->where('joining_date','>=',date('Y-m-d'))->where('company_id',$searchFilter['company_id'])->orderBy('joining_date','ASC')->get();
                return view('dashboard.v1',compact('leaveData','userLeave','todayLeaveListing','upcomingLeaveListing','holidaysData','totalUser','remaningEntry','onProbationList','traineeList','birthdayList','resignUser','joiningUser','total_leaves','userListingWithJoiningDateOnly'));
            }
            elseif(Auth::user()->hasRole(config('constant.team_leader_slug'))||
                    Auth::user()->designation_id == 16 ||
                   Auth::user()->hasRole(config('constant.project_manager_slug'))){
                    $leadEndDateReminder = $this->getLeadEndDateReminderListing();
                    $leadPendingDateReminder = $this->getLeadpendingDateReminderListing();
                $getTeamMember = $user->getMyTeamList($loggedInUserID);
                $team_task_entry = array();
                if(Auth::user()->hasRole(config('constant.team_leader_slug')) || Auth::user()->hasRole(config('constant.project_manager_slug')))
                {
                    if ($request->has('search_by_start_date') && $request->search_by_start_date != '' && $request->search_by_start_date < date('Y-m-01')) {
                        Alert::error("for the previous month task please use the 'View in Details' button");
                        return Redirect::back();
                    }
                    if ($request->has('search_by_end_date') && $request->search_by_end_date != '' && $request->search_by_end_date < date('Y-m-01')) {
                        Alert::error("for the previous month task please use the 'View in Details' button");
                        return Redirect::back();
                    }
                    $team_task_entry     = $taskEntry->teamNonBillableLoggedHours($getTeamMember,$request);
                }
                $getNonBillableLoggedHours = $taskEntry->getNonBillableLoggedHours($getTeamMember);
                $getBillableAssignedHours = $taskEntry->getBillableAssignedHours($getTeamMember);
                $getAssignedProj = count($project->getAssignedProj($loggedInUserID));
                $getActiveProjects = count($project->getActiveProjects($loggedInUserID));
                $userAttendanceData =  $taskEntry->dashboardTimeDetails($companyName,Carbon::now()->month,Carbon::now()->year);
                $late_days = 0;

                foreach($userAttendanceData['records'] as $userSummaryDetail){
                    $late_days = $userSummaryDetail['late_days'];
                }
                return view('dashboard.v1',compact('late_days','todayLeaveListing','upcomingLeaveListing','holidaysData','getTeamMember','getAssignedProj','getActiveProjects','leaveData','team_task_entry','leadEndDateReminder','leadPendingDateReminder','request','getNonBillableLoggedHours','getBillableAssignedHours','total_leaves','total_leaves'));
            }
            elseif(Auth::user()->hasRole(config('constant.superadmin_slug'))||
                    Auth::user()->hasRole(config('constant.admin_slug'))){
                $company_id = '';
                if(isset($searchFilter['company_id']) && !Auth::user()->hasRole(config('constant.superadmin_slug'))){
                    $company_id = $searchFilter['company_id'];
                }
                $resignUser = User::where('employee_status',3)->where('last_date','>=',date('Y-m-d'))->where('status',1)->where('company_id',$company_id)->orderBy('last_date','ASC')->get();
                $joiningUser = User::with('reportingto','designation')->where('joining_date','>=',date('Y-m-d'))->where('company_id',$company_id)->orderBy('joining_date','ASC')->get();
                $birthdayList = $user->getUsersBirthDayOnPreviousHoliday($company_id);
                $userListingWithJoiningDateOnly = userListingWithJoiningDateOnly($company_id);
                $totalUser = count($user->getAllUserListByCompany($company_id));
                $traineeList = $user->getTraineeList($company_id);
                $onProbationList = $user->getEmpListOnProbation($company_id);
                $getTotalActiveProjects = $pro->getTotalActiveProject($company_id)[0]['totalProject'];
                $getTotalClosedProject = $pro->getTotalClosedProject($company_id)[0]['totalProject'];
                $getTotalClient = $client->totalClient($company_id)[0]['totalClients'];
                $openProjectList = $pro->getOpenProjectList($company_id);
                return view('dashboard.v1',compact('todayLeaveListing','upcomingLeaveListing','holidaysData','totalUser','getTotalActiveProjects','getTotalClosedProject','traineeList','onProbationList','getTotalClient','openProjectList','birthdayList','resignUser','joiningUser','total_leaves','userListingWithJoiningDateOnly'));
            }
            else{
                $leadEndDateReminder = $this->getLeadEndDateReminderListing();
                $leadPendingDateReminder = $this->getLeadpendingDateReminderListing();
                $birthdayList = $user->getUsersBirthDayOnPreviousHoliday($authUser->company_id);
                $userListingWithJoiningDateOnly = userListingWithJoiningDateOnly($authUser->company_id);
                $projectList = $project->getAssignedProj($loggedInUserID);
                $resignUser = User::where('employee_status',3)->where('last_date','>=',date('Y-m-d'))->where('status',1)->where('company_id',$authUser->company_id)->orderBy('last_date','ASC')->get();
                $joiningUser = User::with('reportingto','designation')->where('joining_date','>=',date('Y-m-d'))->where('company_id',$authUser->company_id)->orderBy('joining_date','ASC')->get();
                $assignedProject = count($projectList);
                $userAttendanceData =  $taskEntry->dashboardTimeDetails($companyName,Carbon::now()->month,Carbon::now()->year);
                $totalWorkingDay = 0;
                $late_days = 0;
                $avg_log_hr  = '00:00:00';

                foreach($userAttendanceData['records'] as $userSummaryDetail){
                    $totalWorkingDay = $userSummaryDetail['full_days'] + ($userSummaryDetail['half_days'] / 2);
                    $late_days = $userSummaryDetail['late_days'];
                    if(array_key_exists($userSummaryDetail['emp_code'],$userAttendanceData['loggedHours'])){
                        //$logged_hours = sprintf('%0.2f',$userAttendanceData['loggedHours'][$userSummaryDetail['emp_code']]);
                        $logged_hours = (new \App\Helpers\CommonHelper)->displayTaskTime($userAttendanceData['loggedHours'][$userSummaryDetail['emp_code']]);
                        $hoursMinutes = (explode(".",$logged_hours));
                        $hours = $minutes = '00';
                        if(isset($hoursMinutes[0])){
                            $hours = $hoursMinutes[0];
                            $hours = (strlen($hours) == 1)?'0'.$hours:$hours;
                        }

                        if(isset($hoursMinutes[1])){
                            $minutes = $hoursMinutes[1];
                            $minutes = (strlen($minutes) == 1)?$minutes.'0':$minutes;
                        }

                        $str_time = $hours.':'.$minutes.':00';

                        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

                        $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
                        if($totalWorkingDay != 0){

                            $avg_log_hr = gmdate("H:i:s", $time_seconds/$totalWorkingDay);
                        }
                    }
                    else{
                       $avg_log_hr  = '00:00:00';
                    }
                }
                $seconds = 0 ;
                $avg_working_hr = '00:00:00';

                foreach($userAttendanceData['working_details'] as $working_details_val){
                    if($working_details_val['emp_code'] == $userSummaryDetail['emp_code']){
                        $parsed = date_parse($working_details_val['working_hours']);
                        $seconds = $seconds + $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                    }
                }
                if($totalWorkingDay != 0){
                    $avg_working_hr = gmdate("H:i:s", $seconds/$totalWorkingDay);
                }

                return view('dashboard.v1', compact('totalWorkingDay','avg_working_hr','late_days','avg_log_hr','leaveData','assignedProject','todayLeaveListing','upcomingLeaveListing','holidaysData','resignUser','joiningUser','birthdayList','leadEndDateReminder','leadPendingDateReminder','total_leaves','userListingWithJoiningDateOnly'));
            }
        }

    }
    public function empTotalWorkDays(Request $request) {
        $company = new Company();
        $userLoggedHour = array();
        $totalWorkingHour = array();
        $cart_data = array();
        $task = new Task();
        $attendanceObj   = new Attendance();
        $user_id  = $request['user_id'];
        $empWorkDays = $attendanceObj->monthlyTotalWorksDay(Carbon::now()->month,Carbon::now()->year);
        foreach($empWorkDays AS $empWorkDayVal){
            if(Carbon::now()->day >= $empWorkDayVal){
                $date = Carbon::now()->year.'-'.Carbon::now()->month.'-'.$empWorkDayVal;
                $userLoggedHourByDay = $task->userLoggedHourByDay($user_id, $date);
                $company_id = Auth::user()->company_id;
                $companyName = $company->getCompanyNameById($company_id)['company_name'];
                $workingHour = $attendanceObj->workingHourbyUser($user_id, $date, $companyName);
                if(!empty($workingHour)){
                    $timestamp = strtotime($workingHour[0]['entry_date']);
                    $getDay = date("d/m", $timestamp);
                    $totalWorkingHour[$getDay] = $workingHour[0];
                }
                if(!empty($userLoggedHourByDay[0]['total_logedHours'])){
                    $timestamp = strtotime($userLoggedHourByDay[0]['log_date']);
                    $getDay = date("d/m", $timestamp);
                    $userLoggedHour[$getDay] =   $userLoggedHourByDay[0];
                }
            }
        }
        $mergeArray = array_merge_recursive($totalWorkingHour,$userLoggedHour);
        $chartArray = [];
        $i=0;
        foreach($empWorkDays AS $empWorkDayVal){
            if(Carbon::now()->day >= $empWorkDayVal){
                 $key = $empWorkDayVal.'/'.sprintf("%02d",Carbon::now()->month);
                 $chartArray[$i]['date'] = $key;
                if(array_key_exists($key,$mergeArray)){
                    if(count($mergeArray[$key]) == 4){
                         $chartArray[$i]['working_hours'] = str_replace(":",".",$mergeArray[$key]['working_hours'] < 0 ?'00.00':$mergeArray[$key]['working_hours']);
                         $chartArray[$i]['logged_hours'] = str_replace(":",".",gmdate('H:i', floor($mergeArray[$key]['total_logedHours'] * 3600)));
                    }elseif(count($mergeArray[$key]) == 2){
                        $chartArray[$i]['working_hours'] = str_replace(":",".",(array_key_exists('working_hours',$mergeArray[$key])?($mergeArray[$key]['working_hours'] < 0 ?'00.00':$mergeArray[$key]['working_hours']) :'00.00'));
                        $chartArray[$i]['logged_hours'] = str_replace(":",".",(array_key_exists('total_logedHours',$mergeArray[$key])?gmdate('H:i', floor($mergeArray[$key]['total_logedHours'] * 3600)) :'00.00'));
                    }

                }else{
                    $chartArray[$i]['working_hours'] = '00.00';
                    $chartArray[$i]['logged_hours'] = '00.00';
                }
            }
            $i++;
        }
        echo json_encode($chartArray); exit;
    }


    public function versiontwo()
    {
        return view('dashboard.v2');
    }
    public function versionthree()
    {
        return view('dashboard.v3');
    }


    public function changePassword(Request $request) {

        if ($request->isMethod('post')) {
            $data = $request->input();
            $user = Auth::user();
            if (Hash::check($data['password'], $user->password)) {
                $validator = Validator::make($request->all(), [
                     'new_password' => 'required|different:password',
                     'confirm_password' => 'required|same:new_password',
                ]);

                if ($validator->fails()) {
                    return Redirect::to('changepassword')->withErrors($validator);
                }
            }else{
                $validator = Validator::make($request->all(), [
                        'password' => 'required|same:old password'
                ]);
                if ($validator->fails()) {
                    return Redirect::to('changepassword')->withErrors($validator);
                }
            }
            $user->password = Hash::make($data['new_password']);
            $user->save();

            $userData = new User();
            $userDetails = $userData->getUserDetails($user->id);

            $sendData = array('password' => $data['new_password'], 'first_name' => $userDetails['first_name'], 'last_name' => $userDetails['last_name']);

            Alert::success('Your password changed successfully!');
            return Redirect::to('changepassword');

        }

        return view('pms.change_password');
    }

    public function viewProfile(Request $request) {

        $user = Auth::user();
        if ($user->id) {
            if ($request->isMethod('post')) {
                $data = $request->input();

                $validator = Validator::make($request->all(), [
                            'personal_email' => 'nullable|email|unique:users,email,' . $user->id,
                            'phone' => 'required|numeric|digits:10',
                            'dob' => 'nullable|date',
                ]);
                if ($validator->fails()) {
                    return Redirect::to('viewprofile')->withErrors($validator);
                }
                $user->personal_email = $data['personal_email'];
                $user->phone = $data['phone'];
                $user->birthdate = (isset($data['dob']) && $data['dob'] != '')?$data['dob']:NULL;
                $user->save();

                Alert::success('Your personal details changed successfully!');
                return Redirect::back();
            }
            $designation = new Designation();
            $designationDetails = $designation->getDesignationDetails($user->designation_id);
            $designation_name = $designationDetails['designation_name'];
            $userData = new User();
            $userDetails = $userData->getUserDetails($user->reporting_to);
            $reportingPerson = $userDetails['first_name'] . ' ' . $userDetails['last_name'];

            if ($user['birthdate'] == '' || $user['birthdate'] == '0000-00-00' || $user['birthdate'] == '1970-01-01') {
                $user['birthdate'] = '';
            }
        } else {
            return redirect('/login')->with('flash_message_error', 'Please login to access');
        }
//echo "<pre>";print_R($user);exit;
        return view('pms.view_profile', compact('designation_name', 'user', 'reportingPerson'));
    }

    public function getLeadEndDateReminderListing(){
        $leadData = array('leadAlert' => 0,'allLead' => array());
        $leadId = ProjectLead::where('team_leader',Auth::user()->id)
            ->orWhere('project_manager',Auth::user()->id)->pluck('id')->toArray();
        $allLead = ProjectLead::whereIn('id',$leadId)
            ->where('estimation_status','!=','3')
            ->whereNotNull('reviewer_end_date')
            ->select('id','lead_title','reviewer_end_date')
            ->orderBy('reviewer_end_date')->get()->toArray();
        $leadAlert = ProjectLead::whereIn('id',$leadId)
            ->whereNotNull('reviewer_end_date')
            ->where('reviewer_end_date','<=',date('Y-m-d'))
            ->where('estimation_status','!=','3')->get()->count();
        if($leadAlert > 0){
            $leadData['leadAlert'] = $leadAlert;
        }
        if($allLead){
            $leadData['allLead'] =  $allLead;
        }
        return $leadData;
    }

    public function getLeadpendingDateReminderListing(){
        $leadData = array('leadAlert' => 0,'allLead' => array());
        $leadId = ProjectLead::where('team_leader',Auth::user()->id)
            ->orWhere('project_manager',Auth::user()->id)->pluck('id')->toArray();
        $allLead = ProjectLead::whereIn('id',$leadId)
            ->where('estimation_status','!=','3')
            ->whereNull('reviewer_end_date')
            ->select('id','lead_title','created_at')
            ->orderBy('created_at')->get()->toArray();
        $leadAlert = ProjectLead::whereIn('id',$leadId)
            ->whereNull('reviewer_end_date')
            ->where('created_at','<=',date('Y-m-d',strtotime('-2 days')))
            ->where('estimation_status','!=','3')->get()->count();
        if($leadAlert > 0){
            $leadData['leadAlert'] = $leadAlert;
        }
        if($allLead){
            $leadData['allLead'] =  $allLead;
        }
        return $leadData;
    }
    public function getBreakTimeUser(){
        if (Auth::user()->hasRole(config('constant.admin'))) {
            $data = AttendanceDetail::rightjoin('attendances', 'attendance_details.attendance_id', '=', 'attendances.id')->whereBetween('attendances.entry_date', ["2022-11-01", "2022-11-30"])->where("attendances.emp_comp","Tridhya Tech")->select('attendances.id',"attendances.emp_name","attendances.emp_code","attendances.entry_date",DB::raw('SUBTIME(TIMEDIFF( attendances.last_out, attendances.first_in),SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( attendance_details.out_time, attendance_details.in_time))))) as break_hours'))->groupBy("attendances.id")->get()->toArray();
            $dateArray = [];
            foreach ($data as $key => $value) {
                if(isset($value['break_hours']) && !strtotime($value['break_hours'])){
                    $dateArray[$value['entry_date']][] = $value;
                }
            }
            ksort($dateArray);
            echo "<pre>";print_r($dateArray);
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
}
