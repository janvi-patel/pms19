<?php

namespace App\Http\Controllers;

use App\Project;
use App\Cr;
use Mail;
use App\User;
use App\ProjectType;
use App\ProjectUser;
use App\CrCostInfo;
use App\Designation;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use DB;
use App\ProjectTeam;
use Alert;

class CrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if(Auth::check() && Auth::user()->hasPermission('cr.listing')){
            $data      = [];
            $companyId = Auth::user()->company_id;
            $companies = Company::all();
            $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');

            $dataQuery = Cr::join('projects', 'cr.project_id', '=', 'projects.id')->
                    join('clients', 'projects.client_id', '=', 'clients.id')->
                    join('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                    join('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                    join('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                    join('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->whereNull('projects.deleted_at')->
                    select('cr.id', 'cr.title', 'projects.project_name', 'cr.cr_start_date','clients.id as client_id', 
                    'cr.cr_end_date', 'cr.created_at', 'cr.updated_at','cr.estimated_hours','cr.pms_hours', 'accountmanager.id as account_manager_id', 
                    'projectmanager.id as project_manager_id', 'acompany.id as aid', 
                    DB::raw('CONCAT(clients.first_name, " ", clients.last_name) as client_name'), 
                    DB::raw('CONCAT(projectmanager.first_name, " ", projectmanager.last_name) as project_manager_name'), 
                    DB::raw('CONCAT(accountmanager.first_name, " ", accountmanager.last_name) as account_manager_name'), 
                    'pcompany.company_name', 'pcompany.id as cid', 'acompany.company_name as assigned_company', 
                    'pcompany.id as company_id', 'cr.invoice_status as invoice_status');
            if (Auth::user()->hasRole(config('constant.superadmin_slug'))) {
                if ($request->has('search_submit') && $request->company == 0){
                    $companyId = $request->company;
                }elseif($request->company != 0){
                    $other_projects = CrCostInfo::where('company_id', $request->company)->pluck('cr_id');
                    $companyId = $request->company;

                    $dataQuery->where(function($query) use ($other_projects, $request) {
                        $query->where('projects.company_id', $request->company)
                                ->orWhere('projects.assigned_to', $request->company)
                                ->orWhereIn('cr.id', $other_projects);
                    });
                }else{
                    $companyId = $request->company;
                }
            }else{
                if(Auth::user()->hasRole(config('constant.admin'))){
                    $company = Auth::user()->company_id;
                    if($company == config('constant.company.shaligram')){
                        $other_projects = CrCostInfo::where('company_id', $company)->pluck('cr_id');
                        $dataQuery->where(function($query) use ($companyId, $other_projects) {
                            $query->where('projects.company_id', $companyId)
                                    ->orWhere('projects.assigned_to', $companyId)
                                    ->orWhereIn('cr.id', $other_projects);
                        });
                    }else{
                        $dataQuery->where(function($query) use ($companyId) {
                            $query->where('projects.company_id', $companyId)
                                    ->orWhere('projects.assigned_to', $companyId);
                        });
                    }
                }else{
                    $myTeam = getTeamUserId(auth()->user()->id);
                    $myTeam[] = auth()->user()->id;
                    $own_project =  ProjectTeam::where('company_id',Auth::user()->company_id)->whereIn('user_id',$myTeam)->pluck('project_id');
                    $dataQuery = Cr::join('projects', 'cr.project_id', '=', 'projects.id')->
                    join('clients', 'projects.client_id', '=', 'clients.id')->
                    join('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                    join('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                    join('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                    join('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                    where(function($q) use ($own_project) {
                        $q->where('projects.created_by', Auth::user()->id)
                        ->orWhere('projects.team_leader',Auth::user()->id)
                        ->orWhere('projects.project_manager',Auth::user()->id)
                        ->orWhereIn('projects.id',$own_project);
                    })->
                    whereNull('projects.deleted_at')->
                    select('cr.id', 'cr.title', 'projects.project_name', 'cr.cr_start_date', 'clients.id as client_id', 
                        'accountmanager.id as account_manager_id', 'projectmanager.id as project_manager_id', 
                        'cr.cr_end_date', 'cr.created_at','cr.pms_hours', 'cr.updated_at', 'acompany.id as aid','pcompany.id', 
                        DB::raw('CONCAT(clients.first_name, " ", clients.last_name) as client_name'), 
                        DB::raw('CONCAT(projectmanager.first_name, " ", projectmanager.last_name) as project_manager_name'), 
                        DB::raw('CONCAT(accountmanager.first_name, " ", accountmanager.last_name) as account_manager_name'), 
                        'pcompany.company_name', 'pcompany.id as cid', 'acompany.company_name as assigned_company', 
                        'pcompany.id as company_id', 'cr.invoice_status as invoice_status');
                }
            }
            
            $projectName = clone $dataQuery;
            $clientName = clone $dataQuery;
            $accountManager = clone $dataQuery;
            $projectManager = clone $dataQuery;
            $assignedCompany = clone $dataQuery;
            $companyName = clone $dataQuery;
            $crTitle = clone $dataQuery;
            $crTitle = $crTitle->groupBy('title')->orderBy('title')->pluck('title');
            $assignedCompany = $assignedCompany->groupBy('assigned_company')->orderBy('assigned_company')->pluck('assigned_company','aid');
            $companyName = $companyName->groupBy('company_name')->orderBy('company_name')->pluck('company_name','cid');
            $projectManager = $projectManager->groupBy('project_manager_name')->orderBy('project_manager_name')->pluck('project_manager_name','project_manager_id');
            $accountManager = $accountManager->groupBy('account_manager_name')->orderBy('account_manager_name')->pluck('account_manager_name','account_manager_id');
            $projectName = $projectName->groupBy('project_name')->orderBy('project_name')->pluck('project_name');
            $clientName = $clientName->groupBy('client_name')->orderBy('client_name')->pluck('client_name','client_id');
            if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_project_name') && $request->search_by_project_name != '') {
                    $dataQuery->where('projects.project_name', 'like', '%' . $request->search_by_project_name . '%');
                }
                if ($request->has('search_by_cr_title') && $request->search_by_cr_title != '') {
                    $dataQuery->where('cr.title', 'like', '%' . $request->search_by_cr_title . '%');
                }   
                if ($request->has('search_by_client') && $request->search_by_client != '') {
                    $dataQuery->where('clients.id',$request->search_by_client);
                }
                if ($request->has('search_by_account') && $request->search_by_account != '') {
                    $dataQuery->where('accountmanager.id', $request->search_by_account);
                }
                if ($request->has('search_by_project_manager') && $request->search_by_project_manager != '') {
                    $dataQuery->where('projectmanager.id', $request->search_by_project_manager);
                }
                if ($request->has('search_by_company') && $request->search_by_company != '') {
                    $dataQuery->where('pcompany.id',$request->search_by_company);
                }
                if ($request->has('search_by_assign_company') && $request->search_by_assign_company != '') {
                    $dataQuery->where('acompany.id',$request->search_by_assign_company);
                }
                if (($request->has('search_by_start_date') && $request->search_by_start_date != '') && ($request->search_by_end_date == '')) {
                    $dataQuery->where('cr.cr_start_date', '>=', date('Y-m-d',strtotime($request->search_by_start_date)));
                }
                if (($request->has('search_by_end_date') && $request->search_by_end_date != '') && ($request->search_by_start_date == '')) {
                    $dataQuery->where('cr.cr_end_date', '<=', date('Y-m-d',strtotime($request->search_by_end_date)));
                }
                if (($request->has('search_by_start_date') && $request->search_by_start_date != '') && ($request->has('search_by_end_date') && $request->search_by_end_date != '')) {
                    $dataQuery->where('cr.cr_start_date', '>=', date('Y-m-d',strtotime($request->search_by_start_date)));
                    $dataQuery->where('cr.cr_end_date', '<=', date('Y-m-d',strtotime($request->search_by_end_date)));
                }
                if ($request->has('search_by_created_date') && $request->search_by_created_date != '') {
                    $dataQuery->where('cr.created_at', 'like', '%' . $request->search_by_created_date . '%');
                }
            }

            if ($request->has('sort') && $request->input('sort') != '') {
                $data = $dataQuery->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
            } else {
                $data = $dataQuery->sortable()->orderBy('cr.id', 'desc')->paginate($page_limit);
            }
 
            return view('pms.cr.index', compact('data', 'request',  'companies', 'companyId','projectName', 'clientName', 'accountManager','projectManager','companyName','assignedCompany','crTitle'));
        }else{
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }
}
