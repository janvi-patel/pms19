<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Imports\CheckTimeEntryFile;
use Illuminate\Support\Facades\Storage;
use App\Exports\ExportIncorrectTimeEntry;
use Validator;
Use Alert;
use Excel;
use Session;



class CheckTimeEntryController extends Controller
{
    public function upload(Request $request) {
        if (Auth::check() && Auth::user()->hasPermission('pms.uploadtimeentry')) {
            $method = strtolower($request->method());
            if ($method == 'post') {
                if($request->submit_type == 'export'){
                    if(isset($request->data) && $request->data != "") {
                        $data = json_decode($request->data, true);
                        return Excel::download(new ExportIncorrectTimeEntry($data), 'IncorrectTimeEntry_report.csv');
                    } else {
                        return view('pms.check_time_entries');
                    }
                } else {
                    $validator = Validator::make($request->all(), [
                        // 'xlsfile' => 'required|mimes:csv',
                        // 'timeEntryDate' => 'required|date',
                    ]);
             
                    if ($validator->fails()) {
                        Alert::error('Oops, Form has some error!');
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        $file = $request->file('xlsfile');
                        $extension = $file->getClientOriginalExtension();
                        $filename = 'attendance_' . time() . '.' . $extension;
                        if ($request) {
                            Storage::disk('local')->putFileAs('', $file, $filename);
                        }
                        Excel::import(new CheckTimeEntryFile($request->timeEntryDate), $filename);

                        $TimeEntryData = array();
                        $TimeEntryData = Session::get('TimeEntryData');
                        Session::forget('TimeEntryData');      
                        return view('pms.check_time_entry.view_time_entry',compact('TimeEntryData'));
                    }
                }
            } else {
                return view('pms.check_time_entry.check_time_entries');
            }
        } else {
            Alert::error('You do not have permission to perform this action!');
            return Redirect::to('dashboard');
        }
    }


   
}
