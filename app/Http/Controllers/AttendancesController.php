<?php

namespace App\Http\Controllers;
use App\Attendance;
use App\User;
use App\Holiday;
use App\TaskEntry;
use App\Task;
use App\LateFrom;
use Excel;
use Illuminate\Http\Request;
use Config;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AttendancesController extends Controller
{
    
    public function calculateSummary($pms_records){
        $total_days = 0;
        $working_days = 0;
        $present_days = 0;
        $absent_days = 0;
        $late_days = 0;
        $early_going_days = 0;
        $average_total = 0;
        $average = '00:00';

        $percent = 0;

        

        foreach($pms_records as $pms_record){
            $pmsDate = $pms_record['year'].'-'.$pms_record['month'].'-'.$pms_record['date'];
            
            $isHoliday = $this->isHoliday($pmsDate);
            if(!$isHoliday){
                $total_days++;
            }
            
            //if punch data is not empty then process the data
            if(!empty($pms_record['punch_data'])){
                $all_in_times = array();
                $all_out_times = array();
                $pms_record['total_hours'] = '-';
                $pms_record['working_hours'] = '-';
                $pms_record['break_hours'] = '-';

                if (strpos($pms_record['punch_data'], ';')) {
                    $time_array = array_filter(explode(";",$pms_record['punch_data']));
                    foreach($time_array as $key=>$value){
                        if($key % 2 == 0){
                            $all_in_times[]= strstr($value, '(', true);
                        }else{
                            $all_out_times[]= strstr($value, '(', true);
                        }
                    }
                } else {
                    $time_array = str_replace(":in", ":00", $pms_record['punch_data']);
                    $time_array = str_replace(":out", ":00", $time_array);
                    if (strpos($time_array, ':(')) {
                        $time_array = str_replace(":(", ":00(", $time_array);
                    }
                    $time_array = array_filter(explode("(Sales) ",$time_array));
                    foreach($time_array as $key=>$value){
                        if($key % 2 == 0){
                            $all_in_times[]= $value;
                        } else {
                            $all_out_times[]= $value;
                        }
                    }
                }

                $pms_record['all_in_times'] = $all_in_times;
                $pms_record['all_out_times'] = $all_out_times;
                $pms_record['missing_entry'] = (sizeof($all_in_times)==sizeof($all_out_times))?0:1;
                $in_out_count = sizeof($all_in_times);
                
                if($pms_record['missing_entry']==0)
                {  
                    $total_working_hours = 0;
                    for($i=0;$i<$in_out_count;$i++)
                    {
                        $total_working_hours += strtotime($all_out_times[$i])-strtotime($all_in_times[$i]);
                    }
                    $total_hours = strtotime($pms_record['last_out'])-strtotime($pms_record['first_in']);
                    $total_break_hours = $total_hours-$total_working_hours;
                    $pms_record['total_hours'] = date('H:i:s',$total_hours);
                    $pms_record['working_hours'] = date('H:i:s',$total_working_hours);
                    $pms_record['break_hours'] = date('H:i:s',$total_break_hours);
                    
                    if($pms_record['working_hours']>=(config('constant.half_day_from')) && $pms_record['working_hours']<(config('constant.half_day_to'))){
                        $working_days += 0.5;

                        if(!$isHoliday){
                            $present_days++;

                            $timeParts = explode(":", $pms_record['working_hours']); //if you know its safe
                            $seconds = ($timeParts[0] * 60 * 60 + $timeParts[1] * 60 + $timeParts[2]);
                            $tempWorkingHours = gmdate("H:i:s", ($seconds * 2));
                            $average_total += strtotime($tempWorkingHours);
                        }
                    } elseif (isset($pms_record['working_hours']) && $pms_record['working_hours'] != '00:00:00' && $pms_record['working_hours'] >= (config('constant.half_day_to'))) {
                         
                        $working_days++;
                        if(!$isHoliday){
                            $present_days++;
                            $average_total += strtotime($pms_record['working_hours']);
                        }
                        
                        $lateFrom = $this->getLateFromValue($pmsDate);
                        if($pms_record['first_in']>$lateFrom && !$isHoliday){
                            $late_days++;
                        }
                    }
                    if($pms_record['last_out']<(config('constant.shift_to')) && !$isHoliday){
                        $early_going_days++;
                    }
                }
            }  
        }

        if($total_days >= $working_days){
            $absent_days = $total_days - $working_days;
        }

        if($average_total && $present_days){
            $average = date('H:i:s',intval($average_total/$present_days));
        }
        if($total_days && $late_days){
           $percent=round(($late_days / $total_days ) * 100);
        }        
         
        
        $summary = [
            'total_days' => $total_days,
            'working_days' => $working_days,
            'absent_days' => $absent_days,
            'late_days' => $late_days,
            'early_going_days' => $early_going_days,
            'average' => $average,
            'emp_code' => $pms_record['emp_code'],
            'emp_name' => $pms_record['emp_name'],
            'month' => date('F',mktime(0, 0, 0, $pms_record['month'], 10)),
            'year' => $pms_record['year'],
            'percent' => $percent,

        ];
        return $summary;
    }

    
    public function isHoliday($date) {
        $session = session()->all();
        $date = date("Y-m-d", strtotime($date));
        $holiday = new Holiday();
        $holidayList = $holiday->getHolidayDateList($date);
        $isWeekend = $holiday->getWeekendDateList($date);
        // if((date('N', strtotime($date)) >= 6)){
        if(count($isWeekend) > 0){
            return true;
        }else if(sizeof($holidayList) != 0 ){
            return true;
        }
        return false;
    }


    public function getLateFromValue($date) {
        $lateFromArr = LateFrom::where('affected_from_date', '<=', $date)->orderBy('affected_from_date', 'desc')->first();
        if(!empty($lateFromArr))
        {
            $lateFromValue = $lateFromArr->late_from;
            return $lateFromValue;
        }
        throw new \Exception("Value for late_from is not defined in database table named 'late_from_settings' for ".$date.".");
    }
    public function userAttendancesData($user_id) {
        
    }
}
