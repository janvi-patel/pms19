<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Attendance;
use Carbon\Carbon;

class MiscBeanchTaskEntry extends Model {

    use SoftDeletes;
    use Sortable;

    protected $table = 'misc_beanch_task_entries';

    
}
