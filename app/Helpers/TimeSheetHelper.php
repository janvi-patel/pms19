<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\Holiday;
use App\TimeEntryStatus;
use App\AttendanceDetail;
use App\User;
use App\Company;
use App\Jobs\MailJob;
use App\Attendance;
use App\Setting;
use App\Leave;
use App\LeaveDetail;
use App\UserWorkFromHome;
use App\WorkFromHome;
class TimeSheetHelper
{
    public static function getPunchData($array = []) {
        $holiday = new Holiday();
        $data = [];
        $isHoliday = $holiday->getWeekendWithHolidayList($array['date']);

        if( count($isHoliday) > 0 ) {
            return array("status" => false,"data" => $data, "message" => $array['date']."is a holiday!");
        }

        $postData = '{"FromDate":"'.$array['date'].'","ToDate":"'.$array['date'].'","CmpPrefix":"'.$array['company_code'].'"}';

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://122.170.108.155:9998/DailyLogs/GetDailyLogs',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_SSL_VERIFYHOST =>  0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$postData,
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_error($curl);

        curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $mainResponse = json_decode($response,true);

        if(isset($mainResponse['DailyLogs']) && !empty($mainResponse['DailyLogs'])){
            $data = $mainResponse['DailyLogs'];
        }
       // echo "<pre>";print_r($data);exit;
        return array("status" => true, "data" => $data, "message" => "Successfully got the records!");

    }

    public static function getLeaveSummary($data){
        $datediff = 0;
        $startDate = isset($data['start_date']) ? date("Y-m-d", strtotime($data['start_date'])) : '';
        $endDate = isset($data['end_date']) ? date("Y-m-d", strtotime($data['end_date'])) : '';

        $leave = new Leave;
        $LeaveTotalDates = (new \App\Helpers\CommonHelper)->getAllDates($data['start_date'],$data['end_date']);
        foreach($LeaveTotalDates as $LeaveDate){
            $isHoliday = $leave->isHoliday($LeaveDate);
            if(!$isHoliday){
                $datediff++;
            }
        }
        $leaveDays = $datediff;

        $startType = isset($data['start_type']) ? $data['start_type'] : '';
        $endType = isset($data['end_type']) ? $data['end_type'] : '';

        if($startType != 0){
            $leaveDays = $leaveDays - 0.5;
        }
        if($endType != 0){
            $leaveDays = $leaveDays - 0.5;
        }
        if($datediff == 1 && ($startType != 0 || $endType != 0)){
            $leaveDays = abs($leaveDays - 0.5);
        }
        if($endType == 1){
            $returnDate = date('Y-m-d', strtotime($endDate));
        }else{
            $returnDate = date('Y-m-d', strtotime($endDate . ' +1 Weekday'));
        }
        $returnData['return_date'] = $returnDate;
        $returnData['leave_days'] = $leaveDays;

        return $returnData;
    }

    public static function updateLeave($uploadedCompanyId,$absetIdArrayVal,$absentDate,$type)
    {
        $userName = (new \App\Helpers\CommonHelper)->getUserByEmpCodeWithCompany($absetIdArrayVal,$uploadedCompanyId);
        $exclude = array();
        $excludeobj = User::where('want_to_exclude',1)->select('first_name','last_name')->get();
        foreach($excludeobj as $key=>$value)
        {
            $exclude[] = $value->first_name.' '.$value->last_name;
        }
        if(isset($userName)){
            $companyName = (new \App\Helpers\CommonHelper)->getCompanyDetails($userName['company_id']);
            $emp_name = isset($userName['first_name']) ? $userName['first_name'].' '.$userName['last_name'] : '';
            if(!in_array($emp_name,$exclude)){
                $leave = new Leave;
                $checkLeave = $leave->where('user_id',$userName['id'])->whereDate('leaves.leave_start_date','<=', $absentDate)->whereDate('leaves.leave_end_date','>=', $absentDate)->first();

                $user = new User();
                $summary['start_date'] = $absentDate;
                $summary['end_date'] = $absentDate;
                $summary['start_type'] = $type;
                $summary['end_type'] = $type;
                $summaryDetails = TimeSheetHelper::getLeaveSummary($summary);
                $userDetails = $user->getUserDetails($userName['id']);
                $update_leave = $userDetails['used_leaves'] + $summaryDetails['leave_days'];

                if(!isset($checkLeave)){
                    $leave->user_id = $userName['id'];
                    $leave->approver_id = $userName['reporting_to'];
                    $leave->leave_start_date = $absentDate;
                    $leave->leave_end_date = $absentDate;
                    $leave->return_date = date("Y-m-d", strtotime($summaryDetails['return_date']));
                    $leave->leave_days = $summaryDetails['leave_days'];
                    if($leave->leave_days == 0.5){
                    $leave->leave_start_type = 1;
                    $leave->leave_end_type = 1;
                    }else{
                    $leave->leave_start_type = 0;
                    $leave->leave_end_type = 0;
                    }
                    $leave->is_adhoc = 1;
                    $leave->reason = "Auto Generated Leave Adhoc Leave";
                    $leave->approver_comment = '';
                    $leave->leave_status = 2;
                    $leave->save();
                    $leaveID = $leave->id;

                    $leaveDetails = $leave->getLeaveDetails($leaveID);
                    $obj_leave_detail = new LeaveDetail();
                    $obj_leave_detail->createLeaveDetail($leaveDetails);

                    $user->updateUserUsedLeave($userName['id'],$update_leave);

                    $company     = new Company();
                    $userCompanyMailData  = $company->getCompanyViseMailData($userName['company_id'])[0];

                    $data['regards'] = $userCompanyMailData['company_name'];
                    $data['start_date'] = $absentDate;
                    $data['employee_name'] = $emp_name;
                    $data['leave_days'] = $summaryDetails['leave_days'];
                    $data['reason'] = $leave->reason;
                    $data['id'] = $leaveID;
                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name'];


                    $approverData = (new \App\Helpers\CommonHelper)->getUserById($userName['reporting_to']);
                    $to = $userName['email'];
                    $cc = isset($approverData['email']) ? $approverData['email'] : '';
                    $template = 'emails.auto_leave_request';
                    $subject = 'Auto Adhoc Leave Apply Of '.$absentDate;

                    $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany($userName['company_id']);
                    $cc = $hr_admin_email['hr_email_address'];
                    $bcc = $hr_admin_email['email'];

                    MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);
                }
            }
        }
    }

    public static function importTimeEntries($array = []) {
        $uploadedCompanyId = isset($array['company_id']) && $array['company_id'] != "" ? $array['company_id'] : 0;
        $companyObj = Company::find($uploadedCompanyId);
        $short_code = isset($companyObj->emp_short_name) && $companyObj->emp_short_name != "" ? $companyObj->emp_short_name : "";
        if($short_code != "" && $array['date'] != "") {
            $entries = $sortArr = $companyEmpIdArray = $empCodeArray = $data = array();
            $mainResponse = TimeSheetHelper::getPunchData(["date" => $array['date'], "company_code" => $short_code]);
            if(isset($mainResponse['data']) && count($mainResponse['data']) > 0){
                $workFromHomeData = WorkFromHome::leftJoin("users","users.id","work_from_home.user_id")->select('work_from_home.*', 'users.employee_id as emp_code')->where("users.company_id",$array['company_id'])->where('start_date', '<=', $array['date'])->where('end_date', '>=', $array['date'])->where('work_from_home.status', 1)->get()->toArray();
                if(isset($workFromHomeData) && count($workFromHomeData) > 0){
                    foreach ($workFromHomeData as $value) {
                        $key = $short_code.$value['emp_code'];
                        $value['FullLogTime'] = "10:00 (WFH start),13:00 (WFH end),14:00 (WFH start),19:30 (WFH end)";
                        if($value['start_date'] == $array['date'] || $value['end_date'] == $array['date']){
                            if($value['start_type'] != "0" || $value['end_type'] != "0"){
                                if($value['start_type'] == "1") {
                                    $value['FullLogTime'] = "10:00 (WFH start),14:30 (WFH end)";
                                } else {
                                    $value['FullLogTime'] = "15:00 (WFH start),19:30 (WFH end)";
                                }
                            }
                        }
                        $sheetDate = $array['date'];
                        $userDeatails = User::where("employee_id", $value['emp_code'])->where('company_id',$uploadedCompanyId)->first();
                        $sortArr[$key]['empcode'] = str_replace($short_code,'',$value['emp_code']);
                        $sortArr[$key]['name'] = (!empty($userDeatails))?$userDeatails->first_name. " ".$userDeatails->last_name :"";
                        $sortArr[$key]['user_id'] = (!empty($userDeatails))?$userDeatails->id. " ".$userDeatails->id :"";
                        $sortArr[$key]['time'] = $value['FullLogTime'];
                        $punch_time = explode(",",substr($value['FullLogTime'],0, -1));

                        if(isset($punch_time) && $punch_time > 0){
                            $punchKey = 0;
                            foreach($punch_time as $k => $val) {
                                if($k%2==0){
                                    if($k != 0){
                                        $sortArr[$key]['punch_time'][$punchKey]['in'] = date('H:i:s', strtotime(strtok($val,' ')));
                                    } else {
                                        if(isset($val) && $val != ""){
                                            $sortArr[$key]['punch_time'][$punchKey]['in'] =  date('H:i:s', strtotime(strtok($val,' ')));
                                        }
                                    }
                                } else {
                                    $sortArr[$key]['punch_time'][$punchKey]['out'] = isset($val) && $val != "" ? date('H:i:s', strtotime(strtok($val,' '))) : "00:00:00";
                                    $punchKey++;
                                }
                            }
                        }
                    }
                }

                foreach ($mainResponse['data'] as $value) {
                    $key = $value['EmployeeCode'];
                    $sheetDate = date("Y-m-d", strtotime($value['LogDate']));
                    $userDeatails = User::where("employee_id",str_replace($short_code,'',$value['EmployeeCode']))->where('company_id',$uploadedCompanyId)->first();
                    $sortArr[$key]['empcode'] = str_replace($short_code,'',$value['EmployeeCode']);
                    $sortArr[$key]['name'] = (!empty($userDeatails))?$userDeatails->first_name. " ".$userDeatails->last_name :"";
                    $sortArr[$key]['user_id'] = (!empty($userDeatails))?$userDeatails->id. " ".$userDeatails->id :"";
                    $sortArr[$key]['time'] = $value['FullLogTime'];
                    $punch_time = explode(",",substr($value['FullLogTime'],0, -1));

                    if(isset($punch_time) && $punch_time > 0){
                        $punchKey = 0;
                        foreach($punch_time as $k => $val) {
                            if($k%2==0){
                                if($k != 0){
                                    $sortArr[$key]['punch_time'][$punchKey]['in'] = date('H:i:s', strtotime(strtok($val,' ')));
                                } else {
                                    if(isset($val) && $val != ""){
                                        $sortArr[$key]['punch_time'][$punchKey]['in'] =  date('H:i:s', strtotime(strtok($val,' ')));
                                    }
                                }
                            } else {
                                $sortArr[$key]['punch_time'][$punchKey]['out'] = isset($val) && $val != "" ? date('H:i:s', strtotime(strtok($val,' '))) : "00:00:00";
                                $punchKey++;
                            }
                        }
                    }
                }
                $companyEmpIdArray = User::where('company_id',$uploadedCompanyId)->where('status',1)->whereDate('joining_date','<=',$sheetDate)->pluck('employee_id')->toArray();
                if(isset($sortArr) && count($sortArr) > 0){
                    foreach ($sortArr as $k => $row)
                    {
                        $date = $sheetDate;
                        $empcode = $row['empcode'];
                        $empcode = str_replace($companyObj->emp_short_name,'',$row['empcode']);
                        $name = $row['name'];
                        $punch_time = (isset($row['punch_time']) && count($row['punch_time']) > 0 ) ? $row['punch_time'] : [];
                        $entries[$date][$empcode]['emp_code'] = $row['empcode'];
                        $entries[$date][$empcode]['emp_name'] = $name;
                        $entries[$date][$empcode]['user_id'] = $row['user_id'];
                        $entries[$date][$empcode]['punch_time'] = $punch_time;
                        $entries[$date][$empcode]['time'] = $row['time'];
                    }
                    if(isset($entries) && count($entries) > 0) {
                        foreach($entries as $date => $entry) {
                            foreach($entry as $empcode => $detail) {
                                if(isset($detail['punch_time'][0]['in']) && $detail['punch_time'][0]['in'] != '00:00:00') {
                                    $insert = [];
                                    $insert['entry_date'] = $date;
                                    $insert['emp_code'] = $empcode;
                                    $leave = new leave();
                                    $isHoliday = $leave->isHoliday($date);

                                    if(!$isHoliday)
                                    {
                                        $empCodeArray[$date][] = $empcode;
                                    }
                                    $insert['emp_name'] = isset($detail['emp_name']) ? $detail['emp_name'] : '';
                                    $insert['user_id'] = isset($detail['user_id']) ? $detail['user_id'] : '';
                                    $insert['emp_comp'] = $companyObj->company_name;
                                    $insert['first_in'] = $detail['punch_time'][0]['in'];
                                    $index = count($detail['punch_time']) - 1;
                                    $insert['last_out'] = isset($detail['punch_time'][$index]['out']) ? $detail['punch_time'][$index]['out'] : $detail['punch_time'][$index]['in'];
                                    $attendance = Attendance::where(['entry_date' => $date, 'emp_comp' => $companyObj->company_name, 'emp_code' => str_replace($short_code,'',$empcode)])->first();
                                    if(empty($attendance)){
                                        $attendance = Attendance::create($insert);// $emp_code
                                    }

                                    $insert_detail = [];

                                    foreach($detail['punch_time'] as $d => $time) {
                                        if(isset($time['in'])) {
                                            $insert_detail[$d]['attendance_id'] = $attendance->id;
                                            $insert_detail[$d]['in_time'] = $time['in'];
                                            $insert_detail[$d]['out_time'] = isset($time['out']) ? $time['out'] : '00:00:00';
                                        }
                                    }

                                    AttendanceDetail::insert($insert_detail);
                                    $attendance_detail = AttendanceDetail::selectRaw('SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))) AS working_hours')->where('attendance_id', $attendance->id)->first();

                                    $parsed = date_parse($attendance_detail->working_hours);
                                    $total_working = (int) $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                                    if($attendance_detail->working_hours == "00:00:00")
                                    {
                                        $total_working = 1;
                                    }
                                    if($total_working <= 0) {
                                        $attendance->incorrect_entry = 1;
                                    }
                                    else if($total_working >= config('constant.full_day_seconds') || ($total_working >= config('constant.full_day_seconds') && $total_working < config('constant.early_leave_end_seconds'))) {
                                        $attendance->full_day = 1;
                                        if($total_working >= config('constant.full_day_seconds') && $total_working < config('constant.early_leave_end_seconds')){
                                            $attendance->early_going = 1;
                                        }
                                    }
                                    else if($total_working >= config('constant.half_day_seconds') && $total_working < config('constant.full_day_seconds')) {
                                        $attendance->half_day = 1;
                                        TimeSheetHelper::updateLeave($uploadedCompanyId,$empcode,$insert['entry_date'],0.5);
                                    }
                                    else if($total_working > 0 && $total_working < config('constant.half_day_seconds')) {
                                        $attendance->absent = 1;
                                        TimeSheetHelper::updateLeave($uploadedCompanyId,$empcode,$insert['entry_date'],0);
                                    }
                                    $user = User::select('from_shift')->where(['employee_id'=> $empcode,'company_id' => $uploadedCompanyId])->first();
                                    if($user) {
                                        $parsed = date_parse($user->from_shift);
                                        $shift_start = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

                                        $parsed = date_parse($attendance->first_in);
                                        $first_in_seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

                                        $settingData = Setting::where('company_id',$array['company_id'])->where('module','=','LateComeTime')->get()->toArray();
                                        $settingData = $settingData[0];
                                        if(($first_in_seconds > $shift_start + $settingData['constant_str_value']) && $attendance->half_day != 1)
                                            $attendance->late_comer = 1;
                                    }

                                    $attendance->content = $detail['time'];
                                    $attendance->last_out = $insert['last_out'];
                                    $attendance->save();
                                }
                            }
                        }
                        $exclude = array();
                        $excludeId = array();
                        $excludeobj = User::where('want_to_exclude',1)->where('company_id', $uploadedCompanyId)->select('employee_id','first_name','last_name')->get();
                        foreach($excludeobj as $key=>$value)
                        {
                            $exclude[] = $value->first_name.' '.$value->last_name;
                            $excludeId[$key]  = $value->employee_id;
                        }
                        $companyEmpIdArray = array_diff($companyEmpIdArray,$excludeId);
                        if(count($empCodeArray)>0 ){
                            foreach($empCodeArray as $key => $empCodeArrayVal){
                                $absetIdArray = array_diff($companyEmpIdArray, $empCodeArrayVal);
                                if(count($absetIdArray)>0){
                                    $userObj = new User();
                                    foreach($absetIdArray as $absetIdArrayVal){
                                        $getUserId = $userObj->getUserDataByEmpCode($absetIdArrayVal,$uploadedCompanyId);
                                        $checkWorkFromHome = UserWorkFromHome::whereDate('end_date', '>=', $key)->whereDate('start_date', '<=', $key)->where('user_id',$getUserId['id'])->first();
                                        if(!$checkWorkFromHome){
                                            $userName = (new \App\Helpers\CommonHelper)->getUserByEmpCodeWithCompany($absetIdArrayVal,$uploadedCompanyId);
                                            $companyName = (new \App\Helpers\CommonHelper)->getCompanyDetails($userName['company_id']);
                                            $company_name = isset($companyName['company_name']) ? $companyName['company_name'] : '';
                                            $isEntry = Attendance::where('entry_date',$key)->where('emp_comp',$company_name)->where('emp_code',$absetIdArrayVal)->get()->toArray();
                                            if(count($isEntry) == 0){
                                                $insert = [];
                                                $absentDate = $key;
                                                $insert['entry_date'] = $key;
                                                $insert['emp_code'] = $absetIdArrayVal;
                                                $insert['emp_name'] = isset($userName['first_name']) ? $userName['first_name'].' '.$userName['last_name'] : '';
                                                $insert['user_id'] = isset($userName['id']) ? $userName['id'] : '';
                                                $insert['emp_comp'] = isset($companyName['company_name']) ? $companyName['company_name'] : '';
                                                $insert['first_in'] = '00:00:00';
                                                $insert['last_out'] =  '00:00:00';
                                                $attendance = Attendance::create($insert);
                                                $attendance->absent =  1;
                                                $attendance->save();
                                                if(!in_array($insert['emp_name'],$exclude)) {
                                                    $leave = new Leave;
                                                    $checkLeave = $leave->where('user_id',$userName['id'])->whereDate('leaves.leave_start_date','<=', $absentDate)->whereDate('leaves.leave_end_date','>=', $absentDate)->first();

                                                    $user = new User();
                                                    $summary['start_date'] = $absentDate;
                                                    $summary['end_date'] = $absentDate;
                                                    $summary['start_type'] = 0;
                                                    $summary['end_type'] = 0;
                                                    $summaryDetails = TimeSheetHelper::getLeaveSummary($summary);
                                                    $userDetails = $user->getUserDetails($userName['id']);
                                                    $update_leave = $userDetails['used_leaves'] + $summaryDetails['leave_days'];

                                                    if(!isset($checkLeave)){
                                                        $leave->user_id = $userName['id'];
                                                        $leave->approver_id = $userName['reporting_to'];
                                                        $leave->leave_start_date = $absentDate;
                                                        $leave->leave_end_date = $absentDate;
                                                        $leave->return_date = date("Y-m-d", strtotime($summaryDetails['return_date']));
                                                        $leave->leave_days = $summaryDetails['leave_days'];
                                                        if($leave->leave_days == 0.5){
                                                            $leave->leave_start_type = 1;
                                                            $leave->leave_end_type = 1;
                                                        }else{
                                                            $leave->leave_start_type = 0;
                                                            $leave->leave_end_type = 0;
                                                        }

                                                        $leave->is_adhoc = 1;

                                                        $leave->reason = "Auto Generated Leave Adhoc Leave";
                                                        $leave->approver_comment = '';
                                                        $leave->leave_status = 2;
                                                        $leave->save();
                                                        $leaveID = $leave->id;

                                                        $leaveDetails = $leave->getLeaveDetails($leaveID);
                                                        $obj_leave_detail = new LeaveDetail();
                                                        $obj_leave_detail->createLeaveDetail($leaveDetails);

                                                        $user->updateUserUsedLeave($userName['id'],$update_leave);

                                                        $company     = new Company();
                                                        $userCompanyMailData  = $company->getCompanyViseMailData($userName['company_id'])[0];

                                                        $data['regards'] = $userCompanyMailData['company_name'];
                                                        $data['start_date'] = $absentDate;
                                                        $data['employee_name'] = $insert['emp_name'];
                                                        $data['leave_days'] = $summaryDetails['leave_days'];
                                                        $data['reason'] = $leave->reason;
                                                        $data['id'] = $leaveID;
                                                        $from['email'] = $userCompanyMailData['from_address'];
                                                        $from['name']  = $userCompanyMailData['company_name'];


                                                        $approverData = (new \App\Helpers\CommonHelper)->getUserById($userName['reporting_to']);
                                                        $to = $userName['email'];
                                                        $cc = isset($approverData['email']) ? $approverData['email'] : '';
                                                        $template = 'emails.auto_leave_request';
                                                        $subject = 'Auto Adhoc Leave Apply Of '.$absentDate;

                                                        $hr_admin_email = $user->getUsersHrAndAdminEmailWithCompany($userName['company_id']);
                                                        $cc = $hr_admin_email['hr_email_address'];
                                                        $bcc = $hr_admin_email['email'];

                                                        MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, $bcc, []);
                                                    }
                                                }
                                            }
                                        }else{
                                            $getEmpCode = (new \App\Helpers\CommonHelper)->getUserById($checkWorkFromHome->user_id);
                                            if($getEmpCode){
                                                $startTimeSec = 0;
                                                $endTime = '00:00:00';
                                                $parsed = date_parse($getEmpCode->from_shift);
                                                $startTimeSec = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

                                                $workFromHomeWorkingSec = config('constant.workFromHomeWorkingSec');
                                                $endTime =  $workFromHomeWorkingSec + $startTimeSec;
                                                $companyName = (new \App\Helpers\CommonHelper)->getCompanyDetails($getEmpCode->company_id);
                                                $company_name = isset($companyName['company_name']) ? $companyName['company_name'] : '';
                                                $isEntry = Attendance::where('entry_date',$key)->where('emp_comp',$company_name)->where('emp_code',$empcode)->get()->toArray();
                                                if(count($isEntry) == 0){
                                                    $attendance = new Attendance();
                                                    $attendance->entry_date =  $key;
                                                    $attendance->emp_code =  $getEmpCode->employee_id;
                                                    $attendance->emp_name =  $getEmpCode->first_name.' '.$getEmpCode->last_name;
                                                    $attendance->emp_comp =  $company_name;
                                                    $attendance->first_in =  $getEmpCode->from_shift;
                                                    $attendance->last_out =  date('H:i:s',$endTime);
                                                    $attendance->full_day =  1;
                                                    $attendance->save();

                                                    $insert_attendance = [];
                                                    $insert_attendance[$d]['attendance_id'] = $attendance->id;
                                                    $insert_attendance[$d]['in_time'] = $getEmpCode->from_shift;
                                                    $insert_attendance[$d]['out_time'] = date('H:i:s',$endTime);
                                                    AttendanceDetail::insert($insert_attendance);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
