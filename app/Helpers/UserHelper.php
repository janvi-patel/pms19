<?php
namespace App\Helpers;
use App\Company;
use App\User;
use App\Project;
use App\Technology;
use Carbon\Carbon;
use Auth;
use DB;
use App\Fields;
use App\ProjectEntry;
use App\ProjectTasks;
use App\Holiday;

class UserHelper {
    /**
     * @param int $user_id User-id
     * 
     * @return string
     */
    public static function storeFieldTable($array = [])
    {
       if(!empty($array)){
            $fields=new Fields();
            foreach ($array as $key => $value) {
                $fields->$key=$value;
            }
            $fields->save();
       }
    }
    public static function getfieldlist($array = []){
            $data = Fields::select();
            if(isset($array['table_name']) && !empty($array['table_name'])){
                $data->where('table_name',$array['table_name']);
            }
            if(isset($array['user_id']) && !empty($array['user_id'])){
                $data->where('user_id',$array['user_id']);
            }
            if(isset($array['field_name']) && !empty($array['field_name'])){
                $data->where('field_name',$array['field_name']);
            }
            if(isset($array['sorting_column']) && !empty($array['sorting_column']) && isset($array['sorting_direction']) && !empty($array['sorting_direction'])){
                $data->orderby($array['sorting_column'],$array['sorting_direction']);
            }
            if(isset($array['pluck']) && !empty($array['pluck'])){
                $data = $data->pluck($array['pluck'])->toArray();
            }elseif(isset($array['first']) && $array['first'] == 1){
                $data = $data->first(); 
            }else{
                $data = $data->get();
            }   

            return $data;
        }
}