<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\Holiday;
use App\TimeEntryStatus;
use App\AttendanceDetail;
use App\User;
use App\Company;
use App\Jobs\MailJob;
use App\Attendance;
use App\Setting;
use App\Leave;
use App\UserWorkFromHome;
use App\ProjectEntry;
class EmployeeMonthlyDetails
{
    
        public static function getEmployeeMonthlyHoursDetails($array) {
            $leave = new Leave;
            $attandQuery = Attendance::whereBetween('entry_date', [$array['start_date'], $array['end_date']]);
            $total_days_qry = clone $attandQuery;
            $total_days_qry = $total_days_qry->groupBy('entry_date')->pluck('entry_date')->toArray();
            $totalDays = 0;
            foreach ($total_days_qry as $date) {
                if($leave->isHoliday($date) == false){
                    $totalDays++;
                }
            }
            $userLeaveDetails = $attandQuery->selectRaw('SUM(attendances.full_day) as full_days, SUM(attendances.half_day) as half_days, SUM(attendances.absent) as absent_days, SUM(attendances.incorrect_entry) as incorrect_days')->where('attendances.emp_code', $array['employee_id'])->where('attendances.emp_comp', $array['company_name'])->first();
            $totalWorkingDay = $userLeaveDetails['full_days'] + ($userLeaveDetails['half_days'] / 2);
            $absentDays = (($userLeaveDetails->absent_days == '')?0:($userLeaveDetails->absent_days)) + (($userLeaveDetails->half_days == '')?0:($userLeaveDetails->half_days/2)) + (($userLeaveDetails->incorrect_days == '')?0:($userLeaveDetails->incorrect_days));
            $workingSec = AttendanceDetail::selectRaw('sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time))) AS working_hours')
                            ->rightJoin('attendances', 'attendances.id', '=', 'attendance_details.attendance_id')
                            ->where('attendances.absent', NULL)
                            ->where('first_in','!=','00:00:00')
                            ->where('last_out','!=','00:00:00')
                            ->where('attendances.incorrect_entry', NULL)
                            ->where('attendances.emp_code', $array['employee_id'])
                            ->where('attendances.emp_comp',$array['company_name'])
                            ->whereBetween('attendances.entry_date', [$array['start_date'], $array['end_date']])
                            ->groupBy('attendances.emp_code')->value('working_hours');
    
            $loggedHours = ProjectEntry::selectRaw("sum(`log_hours`) as log_hours")
                        ->whereBetween('log_date', [$array['start_date'], $array['end_date']])
                        ->where('user_id',$array['user_id'])
                        ->groupBy('user_id')
                        ->value('log_hours');
    
            $loggedHours = (new \App\Helpers\CommonHelper)->displayTaskTime($loggedHours);
                list($hours,$mins) = explode(':',$loggedHours);
                $loggedSec = mktime($hours,$mins,"00") - mktime(0,0,0);
    
            $data = [
                'total_days' => $totalDays,
                'total_working_day' => $totalWorkingDay,
                'absentDays' => $absentDays,
                'avg_working_hr' => ($totalWorkingDay != 0)?gmdate('H:i:s',$workingSec / $totalWorkingDay):'00:00:00',
                'avg_logged_hr' => ($totalWorkingDay != 0)?gmdate('H:i:s',$loggedSec / $totalWorkingDay):'00:00:00',
                'leave_details' => getUserMonthlyLeaveDetails(["where" => ['user_id' => $array['user_id'] ],"between" => ["start_date" => $array['start_date'], "end_date" => $array['end_date']]]),
            ];
    
            return $data;
        }
    
}
