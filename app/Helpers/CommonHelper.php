<?php
namespace App\Helpers;
use App\Company;
use App\User;
use App\Project;
use App\Technology;
use Carbon\Carbon;
use Auth;
use DB;
use App\ProjectEntry;
use App\ProjectTasks;
use App\Holiday;

class CommonHelper {
    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    public static function convertTaskTime($hours = 0, $minutes = 0) {
        $resultHour = 0;
        if($minutes == '15'){
            $resultHour = $hours.'.25';
        }else if($minutes == '30'){
            $resultHour = $hours.'.50';
        }else if($minutes == '45'){
            $resultHour = $hours.'.75';
        }else{
            $resultHour = $hours.'.00';
        }

        return $resultHour;
    }

    public static function displayTaskTime($taskHours) {
        $resultHour = '';

        $taskHours = number_format($taskHours, 2);
        $timeLog = explode('.', $taskHours);

        if(!empty($timeLog) && isset($timeLog[0])){
            if(isset($timeLog[1]) && $timeLog[1] == '25'){
                $resultHour = $timeLog[0].':15';
            }else if(isset($timeLog[1]) && $timeLog[1] == '50'){
                $resultHour = $timeLog[0].':30';
            }else if(isset($timeLog[1]) && $timeLog[1] == '75'){
                $resultHour = $timeLog[0].':45';
            }else{
                $resultHour = $timeLog[0].':00';
            }
        }

        return $resultHour;
    }

    public static function inserTaskTime($taskMin) {
        if($taskMin == 15){
            $resultMin = '25';
        }elseif($taskMin == 30){
            $resultMin = '5';
        }elseif($taskMin == 45){
            $resultMin = '75';
        }else{
            $resultMin = '00';
        }

        return $resultMin;
    }

    public static function displayDate($date) {

        $returnDate = date('d-m-Y', strtotime($date));

        return $returnDate;
    }
    public static function displayDay($date) {

        $returnDay = date('l', strtotime($date));

        return $returnDay;
    }

    public static function takeEmpShortName($company_code) {
        $company = Company::find($company_code);
        return $company->emp_short_name;
    }

    public static function getCompanyName($company_code) {
        $company = Company::find($company_code);
        return $company->company_name;
    }

    public static function getCompanyId($company_name) {
        $company = Company::where('company_name',$company_name)->select('id')->first();
        return $company->id;
    }

    public static function getCompanyDetails($company_id){
        $company = Company::where('id',$company_id)->first();
        return $company;
    }

    public static function displayLeaveInDashboard($leaveTotal) {
        $leave = explode(".",$leaveTotal);
        if(isset($leave[1]) && ($leave[1] == 0)){
            $leaveDisplay = $leave[0];
        }else{
            $leaveDisplay = $leaveTotal;
        }
        return $leaveDisplay;
    }

    public static function checkUserStatus($empCode) {
        $userStatus = User::where('employee_id',$empCode);
        if(Auth::check() && (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('hr'))){
            $userStatus->where('status',1);
        }
        $user = $userStatus->get();
        $status = 0;
        if(count($user)>0){
          $status = 1;
      }
      return $status;
    }
    public function getAllDates($start,$end) {
      $startDate = new Carbon($start);
      $endDate = new Carbon($end);
      $all_dates = array();
      while ($startDate->lte($endDate)){
        $all_dates[] = $startDate->toDateString();
        $startDate->addDay();
    }
    return $all_dates;
  }

    public function getCompanyUser($empCode,$company_name) {
        $company_id = $this->getCompanyId($company_name);
        $user = User::where('employee_id',$empCode)->where('company_id',$company_id)->where('status',1)->first();
        return $user;
    }

    public function getUserById($empId) {
        $user = User::where('id',$empId)->where('status',1)->first();

        return $user;
    }
    public function getAnyUserById($empId) {
        $user = User::where('id',$empId)->first();
        return $user;
    }

    public function getUserByEmpCode($empCode) {
        $user = User::where('employee_id',$empCode)->where('status',1)->first();
        return $user;
    }

    public function getUserByEmpCodeWithCompany($empCode,$company_id) {
        $user = User::where('employee_id',$empCode)->where('company_id',$company_id)->where('status',1)->first();
        return $user;
    }

    public function getProjectData($projectId) {
        $project = Project::where('id',$projectId)->first();
        return $project;
  }
  public function getProjectType($id){
    $data = Project::select("project_types.project_type_name")->join('project_types', 'projects.project_type', '=',
    'project_types.id')->where('projects.id',$id)->first();
    return $data;
  }
  public function getTaskHours($projectId,$userId) {
        $loggedHours = ProjectTasks::where('project_id',$projectId);
        if($userId != ''){
           $loggedHours = $loggedHours->where('user_id',$userId);
        }
        $loggedHours =  $loggedHours->select(DB::raw("sum(`estimated_hours`) as total_task_hours"))->first();
      return $loggedHours;
  }
  public function getUserId($empcode,$company_id) {
      $user = User::where('employee_id',$empcode)->where('company_id',$company_id)->where('status',1)->first();
      return $user;
  }

  public function getTechnology($id) {
    $tech = Technology::where('id',$id)->first();
    return $tech;
}

  public function getLoggedHours($projectId,$userId) {
      $loggedHours = ProjectEntry::where('project_id',$projectId)
                        ->where('user_id',$userId)
                        ->select(DB::raw("sum(`log_hours`) as user_logged_hours"))->first();
      return $loggedHours;
  }
  public function getUserTaskToatalHr($projectId,$userId,$taskId){
      $loggedHours = ProjectEntry::where('project_id',$projectId);
        if($userId != ''){
           $loggedHours = $loggedHours->where('user_id',$userId);
        }
        if($taskId != 0){
            $loggedHours = $loggedHours->where('task_id',$taskId);
        }else{
            $loggedHours = $loggedHours->whereNull('task_id');
        }
        $loggedHours =  $loggedHours->select(DB::raw("sum(`log_hours`) as user_logged_hours"))->first();
      return $loggedHours;
  }

   public function CheckReprtingPerson($uid){

    $isReprtingPerson = User::where('reporting_to',$uid)->whereStatus('1')->get();
     // echo '<pre>'; print_r($isReprtingPerson);exit;
    if(!($isReprtingPerson->isEmpty()))
    {
        return 1;
    }else{
        return 0;
    }

   }

    public function is_holiday($date){
        $query =  Holiday::select('id');
        $query->where('holiday_date',$date);
        $query->whereNull('deleted_at');
        $holidayData = $query->orderBy('holiday_date','ASC')->get()->toArray();

        if(count($holidayData) > 0){
            return true;
        } else {
            return false;
        }
    }

}
