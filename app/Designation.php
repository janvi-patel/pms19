<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Designation extends Model
{
    //
    use SoftDeletes;
    use Sortable;

    protected $table = 'designation';
    
    public function getDesignationList(Request $request=Null){
        $designation = Designation::query();
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        $designationData = $designation->sortable()->orderBy('designation_name','ASC')->paginate($page_limit);
        return $designationData;
    }
    public function getAllDesignationList(Request $request=Null){
        $designation = Designation::query();
        $designationData = $designation->orderBy('designation_name','ASC')->get()->toArray(); 
        return $designationData;
    }    
    public function getDesignationName(){
        return Designation::select('id', 'designation_name')->orderBy('designation_name','asc')->get();
    }    

    public function getDesignationDetails($designationID){
        $designation = Designation::query();
        if ($designationID) {
            $designation->where('id', $designationID);
        }
        $designationDetail = $designation->first()->toArray();
        // echo '<pre>';
        // print_r($usersData);
        // die;
        return $designationDetail;
    }

    public function accountmanagerByDesignation(){
        return $this->hasOne(User::class, 'designation_id', 'account_manager');
    }
}
