<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Holiday;
use App\LeaveDetail;
class Leave extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'leaves';

    public $sortable = ['user.first_name','leave_start_date','leave_end_date','return_date',
                        'leave_days','reason','user.first_name','leave_status'];


    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function approver(){
        return $this->hasOne(User::class, 'id', 'approver_id');
    }

    public function getLeavesListing($userIDs, $leaveStatus=0){
        $leave = Leave::query()->with(['user', 'approver']);

        if ($userIDs) {
            if ($leaveStatus != 0) {
                $leave->whereIn('leave_status', [$leaveStatus]);
            }
            $leave->whereIn('user_id', $userIDs);
        }
        $leaveData = $leave->orderBy('id','ASC')->get()->toArray();
        return $leaveData;
    }

    public function getLeaveDetails($leaveID){
        $leave = Leave::query()->with(['user', 'approver']);

        if ($leaveID) {
            $leave->where('id', $leaveID);
        }
        $leaveDetail = $leave->first();
        if($leaveDetail){
            $leaveDetail = $leaveDetail->toArray();
        }

        return $leaveDetail;
    }

    public function getEmployeeLeaveListing($filter){
        $leave = Leave::query()->with(['user', 'approver']);
        $leave->leftJoin('users as u','u.id','leaves.user_id');
        if(isset($filter['company_id']) && $filter['company_id']){
            $leave->where('u.company_id','=', $filter['company_id']);
        }
        if(isset($filter['date']) && $filter['date']){
            $todayDate = date("Y-m-d", strtotime($filter['date']));
            $leave->whereDate('leaves.leave_start_date','<=', $todayDate);
            $leave->whereDate('leaves.leave_end_date','>=', $todayDate);
        }

        $leave->where('leave_status', 1);   //Approved

        $leaveData = $leave->orderBy('leaves.leave_start_date','ASC')->get()->toArray();

        return $leaveData;
    }

    public function getUpcomingLeaveListing($filter){
        $leave = Leave::query()->with(['user', 'approver']);
        $leave->leftJoin('users as u','u.id','leaves.user_id');
        if(isset($filter['company_id']) && $filter['company_id']){
            $leave->where('u.company_id','=', $filter['company_id']);
        }
        if(isset($filter['date']) && $filter['date']){
            $todayDate = date("Y-m-d", strtotime($filter['date']));
            $leave->whereDate('leaves.leave_start_date','>', $todayDate);
        }
        $leave->where('leaves.leave_status', 1);   //Approved

        $leaveData = $leave->orderBy('leaves.leave_start_date','ASC')->get()->toArray();
        return $leaveData;
    }

    public function getPendingLeaveListing($filter){
        $leave = Leave::query()->with(['user', 'approver']);

        if(isset($filter['approver_id']) && $filter['approver_id']){
            $leave->where('approver_id', $filter['approver_id']);
        }
        $leave->where('leave_status', 2);   //Pending

        $leaveData = $leave->orderBy('id','ASC')->get()->toArray();

        return $leaveData;
    }

    public function getEmployeeAllLeaves($userID){
        $leave = Leave::query();

        if(isset($userID)) {
            $leave->where('user_id','=', $userID);
        }
        $leave->where('leave_status', 1);   //Approved

        $leaveData = $leave->get()->toArray();

        return $leaveData;
    }


    public function getEmployessUsedLeave($userId){
        $leave = Leave::select(DB::raw('sum(leave_days) AS totalLeave'));

        $leave->where('leave_status', 1);   //approved
        $leave->where('user_id','=',$userId);
        $leaveData = $leave->get()->toArray();
        return $leaveData;
    }

    public function getEmployeeAllLeavesReport(){
        $leave = Leave::query()->with(['user', 'approver']);

        $leaveData = $leave->sortable()->orderBy('id','ASC')->paginate(config('constant.recordPerPage'));

        return $leaveData;
    }
    public function checkedDelete($leaveIdArray) {
        $leave = Leave::query()->whereIn('id',$leaveIdArray)->delete();
        return $leave;

    }
    public function checkedUpdate($leaveIdArray,$action) {
        $user = new User();
        foreach($leaveIdArray AS $leaveIdArrayVal){
            $beforeUpdate = $this->getLeaveDetails($leaveIdArrayVal);
            $leave = Leave::query()->where('id',$leaveIdArrayVal)->update(['leave_status' => $action,'approver_date' => date('Y-m-d')]);
            $leaveDetails = $this->getLeaveDetails($leaveIdArrayVal);
            $userDetails = $user->getUserDetails($leaveDetails['user']['id']);
            $obj_leave_detail = new LeaveDetail();
            $obj_leave_detail->updateLeaveDetail($leaveDetails);
            if($leaveDetails['leave_status'] == 3 || $leaveDetails['leave_status'] == 1){
                if($leaveDetails['leave_status'] == 3 && $beforeUpdate['leave_status'] == 1){
                    $update_leave = $userDetails['used_leaves'] - $leaveDetails['leave_days'];
                    $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                }elseif($leaveDetails['leave_status'] == 1){
                    $update_leave = $userDetails['used_leaves'] + $leaveDetails['leave_days'];
                    $user->updateUserUsedLeave($leaveDetails['user']['id'],$update_leave);
                }
            }
        }
        return $leave;
    }
    public function isHoliday($date) {
        $session = session()->all();
        $date = date("Y-m-d", strtotime($date));
        $holiday = new Holiday();
        $holidayList = $holiday->getHolidayDateList($date);
        $isWeekend = $holiday->getWeekendDateList($date);
        // if((date('N', strtotime($date)) >= 6)){
        if(count($isWeekend) > 0){
            return true;
        }else if(sizeof($holidayList) != 0 ){
            return true;
        }
        return false;
    }

    function getNextWorkingDate($array = []) {
        $days = 1;
        $returnDate = date("Y-m-d",strtotime($array['date']));
        if(isset($array['type']) && $array['type'] == "next_working_day"){
            $returnDate = date("Y-m-d",strtotime($array['date']." +$days days"));
        }
        while($this->isHoliday($returnDate) || (date("Y",strtotime($returnDate)) <= "2019" && date("l",strtotime($returnDate)) == "Saturday" || date("l",strtotime($returnDate)) == "Sunday")) {
            $returnDate = date("Y-m-d",strtotime($array['date']." +$days days"));
            $days++;
        }
        return $returnDate;
    }
}
