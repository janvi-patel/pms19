<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Holiday;

class EarlyLeave extends BaseModel
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'early_leaves';

    public $sortable = ['user.first_name','date','reason','user.first_name','status'];


    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function approver(){
        return $this->hasOne(User::class, 'id', 'approver_id');
    }
    public function getLeaveDetails($leaveID){
        $leave = EarlyLeave::query()->with(['user', 'approver']);

        if ($leaveID) {
            $leave->where('id', $leaveID);
        }
        $leaveDetail = $leave->first();
        if($leaveDetail){
            $leaveDetail = $leaveDetail->toArray();
        }

        return $leaveDetail;
    }
    
}
