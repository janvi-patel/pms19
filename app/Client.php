<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use Sortable;
    use SoftDeletes;
    
    public $table = 'clients';
    
    protected $fillable = ['company_name','country_id','company_id'];
    
    public $sortable = ['first_name',
                        'last_name',
                        'email',
                        'phone',
                        'company.company_name',
                        'skype_id',
                        'country.country_name',
                        'created_at',
                        'updated_at'];
    
    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');
    }
    public function company()
    {
        return $this->hasOne('App\Company','id','company_id');
    }
    public function totalClient($company_id = '')
    {
        $query = Client::select(DB::raw('COUNT(id) AS totalClients'));
        if(isset($company_id) && $company_id){
            $query->where('company_id','=',$company_id);
        }
        $query->whereNull('deleted_at');
        $clients = $query->get()->toArray();
        return $clients;
    }
}
