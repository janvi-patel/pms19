<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Company;
use Illuminate\Support\Facades\Auth;
class CompOff extends BaseModel
{
    use SoftDeletes;
    use Sortable;
     
    protected $table = 'comp_offs';
    public $sortable = ['user.first_name','compoff_start_date','compoff_end_date','compoff_days','compoff_description',
                        'user.first_name' ,'compoff_status','compoff_marked_as'];
    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function approver(){
        return $this->hasOne(User::class, 'id', 'approver_id');
    }
   

    public function getCompOffTeamListing($userIDs, $compOffStatus=0,$request){
        $search_cmp = '';
        
        $compoff = CompOff::query()->with(['user', 'approver']);
        $compoff->leftJoin('users as user','user.id','comp_offs.user_id');
        $compoff->leftJoin('companies as c','c.id','user.company_id');
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        
        if ($userIDs) {
            if ($compOffStatus != 0) { 
                $compoff->whereIn('comp_offs.compoff_status', [$compOffStatus]);
            }  
            if(isset($userIDs[0]) && $userIDs[0] != 0){
                $compoff->whereIn('comp_offs.user_id', $userIDs);
            }elseif(!isset($userIDs[0])){ 
                $compoff->whereIn('comp_offs.user_id', $userIDs);
            }    
        }
        
        if(Auth::user()->hasRole(config('constant.superadmin_slug'))|| Auth::user()->hasRole(config('constant.admin'))){
            if( $request->searchMarkedStatus == '' && $request->has('ddlCompOffStatus') == ''){
                $compoff->where('comp_offs.compoff_status',2);
                $compoff->whereNull('comp_offs.compoff_marked_as');
            }
        } 
        if ($request->has('search_submit') && $request->search_submit != '') {
            // add by Mohit
            if ($request->has('search_by_department') && count($request->search_by_department) > 0 ){
                $compoff =   $compoff->whereIn('user.department',($request->search_by_department));
            }
            if ($request->has('search_by_internal_company_id') && count($request->search_by_internal_company_id) > 0) {
                $param = $request->search_by_internal_company_id;
                if(in_array("parent_company",$param) && count($param) >= 2){
                    $compoff =   $compoff->where(function ($q) use ($param) {
                        $q->whereIn('internal_company_id', ($param))->orWhereNull('internal_company_id');
                    });
                } else if (in_array("parent_company",$param)) {
                    $compoff =  $compoff->WhereNull('internal_company_id');
                } else {
                    $compoff =   $compoff->whereIn('internal_company_id', ($param));
                }
            }
            // end by Mohit
            if ($request->has('search_team_compoff_emp') && $request->search_team_compoff_emp != '') {
                $compoff->where('comp_offs.user_id',  '=', $request->search_team_compoff_emp);
            }
            if ($request->has('search_team_compoff_approver') && $request->search_team_compoff_approver != '') {
                $compoff->where('comp_offs.approver_id',  '=', $request->search_team_compoff_approver);
            }
            if ($request->has('search_compoff_team_start_date') && $request->search_compoff_team_start_date != '') {
                $compoff->where('comp_offs.compoff_start_date',  '>=', date("Y-m-d", strtotime($request->search_compoff_team_start_date)));
            }
            if ($request->has('search_compoff_team_end_date') && $request->search_compoff_team_end_date != '') {
                $compoff->where('comp_offs.compoff_end_date',  '<=', date("Y-m-d", strtotime($request->search_compoff_team_end_date)));
            }
            if ($request->has('ddlCompOffStatus') && $request->ddlCompOffStatus != '') {
                $compoff->where('comp_offs.compoff_status',  '=', $request->ddlCompOffStatus);
            }
            if ($request->has('searchMarkedStatus') && $request->searchMarkedStatus != '' && $request->searchMarkedStatus != 0) {
                $compoff->where('comp_offs.compoff_marked_as',  '=', $request->searchMarkedStatus);
            }
        }
//        echo "<pre>";print_R($compoff->toSql());exit;
        $compoff->select('comp_offs.*');
        if ($request->has('sort') && $request->input('sort') != '') {
            $compoffData = $compoff->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
        } else {
            $compoffData = $compoff->sortable()->orderBy('comp_offs.id', 'desc')->paginate($page_limit);
        }
        return $compoffData;
    }
    
    public function getCompOffIndexListing($userIDs,$request){
        $compoff = CompOff::query()->with(['user', 'approver']);
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        if ($userIDs) { 
            if(isset($userIDs[0]) && $userIDs[0] != 0){
                $compoff->whereIn('user_id', $userIDs);
            }elseif(!isset($userIDs[0])){ 
                $compoff->whereIn('user_id', $userIDs);
            }    
        }
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                $compoff->where('user_id',  '=', $request->search_by_employee);
            }
            if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                $compoff->where('approver_id',  '=', $request->search_by_approver);
            }
            if ($request->has('search_compoff_start_date') && $request->search_compoff_start_date != '') {
                $compoff->where('compoff_start_date',  '>=', date("Y-m-d", strtotime($request->search_compoff_start_date)));
            }
            if ($request->has('search_compoff_end_date') && $request->search_compoff_end_date != '') {
                $compoff->where('compoff_end_date',  '<=', date("Y-m-d", strtotime($request->search_compoff_end_date)));
            }
            if ($request->has('search_by_compoff_status') && $request->search_by_compoff_status != '') {
                $compoff->where('compoff_status',  '=', $request->search_by_compoff_status);
            }
        }
        $compoffData = $compoff->sortable()->orderBy('compoff_start_date','desc')->paginate($page_limit);
        
        return $compoffData;
    }
     public function getCompOffListForReport($userIDs,$request){
        $company_id = '';
        if(Auth::user()->hasRole('super_admin')){
             if ($request->has('search_submit') && $request->search_submit != '') {
                if ($request->has('search_by_company') && $request->search_by_company != '') {
                    $company_id = $request->search_by_company;
                }
            }
        }else{
            $company_id = Auth::user()->company_id;
        }
        $compoff = CompOff::query()->with(['user', 'approver']);
        $compoff->leftJoin('users as user','user.id','comp_offs.user_id');
        $compoff->leftJoin('companies as c','c.id','user.company_id');
        $compoff->where('user.status',1);
        if($company_id){
            $compoff->where('user.company_id','=',$company_id);
        }
        if(Auth::check() && Auth::user()->hasRole('tl') || Auth::user()->designation_id == 16 || Auth::user()->designation_id == 26 ){
            $compoff->where('user.reporting_to','=', Auth::user()->id);
        }         
        
        $page_limit = ($request['page_range'])?$request['page_range']:config('constant.recordPerPage');
        if ($userIDs) { 
            if(isset($userIDs[0]) && $userIDs[0] != 0){
                $compoff->whereIn('comp_offs.user_id', $userIDs);
            }elseif(!isset($userIDs[0])){ 
                $compoff->whereIn('comp_offs.user_id', $userIDs);
            }    
        }
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('search_by_employee') && $request->search_by_employee != '') {
                $compoff->where('comp_offs.user_id',  '=', $request->search_by_employee);
            }
            if ($request->has('search_by_approver') && $request->search_by_approver != '') {
                $compoff->where('comp_offs.approver_id',  '=', $request->search_by_approver);
            }
            if ($request->has('search_compoff_start_date') && $request->search_compoff_start_date != '') {
                $compoff->where('comp_offs.compoff_start_date',  '>=', date("Y-m-d", strtotime($request->search_compoff_start_date)));
            }
            if ($request->has('search_compoff_end_date') && $request->search_compoff_end_date != '') {
                $compoff->where('comp_offs.compoff_end_date',  '<=', date("Y-m-d", strtotime($request->search_compoff_end_date)));
            }
            if ($request->has('search_by_compoff_status') && $request->search_by_compoff_status != '') {
                $compoff->where('comp_offs.compoff_status',  '=', $request->search_by_compoff_status);
            }
            if ($request->has('search_by_marked_status') && $request->search_by_marked_status != '') {
                $compoff->where('comp_offs.compoff_marked_as',  '=', $request->search_by_marked_status);
            }
        }
        $compoff->select('comp_offs.*','c.company_name');
       
        if ($request->has('sort') && $request->input('sort') != '') {
            $compoffData = $compoff->sortable()->orderBy($request->input('sort'), $request->input('direction'))->paginate($page_limit);
        } else {
            $compoffData = $compoff->sortable()->orderBy('comp_offs.compoff_start_date', 'desc')->paginate($page_limit);
        }
        return $compoffData;
    }

    public function getCompOffDetails($compoffID){
        $compoff = CompOff::query()->with(['user', 'approver']);
        if ($compoffID) {
            $compoff->where('id', $compoffID);
        }
        $compoffDetail = $compoff->first();
        if($compoffDetail){
            $compoffDetail = $compoffDetail->toArray();
        }

        return $compoffDetail;
    }
    
    public function getUserCompOffsCount($userID){
        $compoff = CompOff::query();
        $compoffDetail = array();
        $compOffDays = 0;
        if ($userID) {
            $compoff->where('user_id', $userID);
            $compoff->where('compoff_status', 1);
            $compoff->where('compoff_marked_as', 2);
        }
        $compoffDetail = $compoff->get()->toArray();     
        if(count($compoffDetail) > 0){
            foreach ($compoffDetail as $compoffData) {
                $compOffDays += $compoffData['compoff_days'];
            }
        }
        return $compOffDays;
    }
    
    public function checkedCompoffDelete($compoffIdArray) {
        $compoff = CompOff::query()->whereIn('id',$compoffIdArray)->delete();
        return $compoff;

    }
    public function checkedCompoffUpdate($compoffIdArray,$action) {
        if($action == 11 || $action == 22){
            if($action == 11){
                $action = 1;
                $compoff_leave_used = '';
                $compoff_encashed = '';
            }else{
                $action = 2;
                $compoff_days = CompOff::query()->whereIn('id',$compoffIdArray)->get();
                $leave_used = array();
                foreach($compoff_days as $key => $val){
                    for($i = 0 ; $i < $val->compoff_days; $i++)
                    {
                         $leave_used[$i]= 0;
                    } 
                }
                $compoff_leave_used = implode(",", $leave_used);
                $compoff_encashed = '';
            }
            $cnt =0;

            $ifAllPending = CompOff::query()->whereIn('id',$compoffIdArray)->get();
            $countOfPending =$ifAllPending->count();
           
            foreach($ifAllPending as $key=>$val){
                if($val->compoff_status==2)
                {
                    $cnt++;
                }
            }

            if($cnt==$countOfPending){
                // echo 'hii';exit;
               echo  $pending = 'allpending';
                return $pending;
            }else{

            echo $compoff = CompOff::query()->whereIn('id',$compoffIdArray)->where('compoff_status',1)->update(['compoff_marked_as' => $action , 'leave_used' => $compoff_leave_used , 'compoff_encash_month_year' => $compoff_encashed]);
            }
        }else{  
            echo $compoff = CompOff::query()->whereIn('id',$compoffIdArray)->update(['compoff_status' => $action]);        }
        return $compoff;
    }

}
