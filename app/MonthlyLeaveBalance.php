<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class MonthlyLeaveBalance extends BaseModel
{
    protected $fillable = ['month','year', 'leave_balance', 'user_id'];

    function createMontlyBalanceData($array){
        $data['month'] = $array['month'];
        $data['year'] = $array['year'];
        $array['date'] = $array['year']."-".$array['month']."-01";
        $array['first_date'] = date("Y-m-01",strtotime($array['date']));
        $getUserList = User::whereNull('deleted_at')->select('users.employee_id','users.id as uid', 'users.designation_id', 'users.employee_status', 'users.confirmation_date', 'users.employee_join_as', 'users.joining_date')->get()->toArray();
        foreach($getUserList as $userData) {
            $array['user_id'] = $data["user_id"] = $userData['uid'];
            $array['financial_year_start_date'] = getUsersFinancialYearStartDate($data["user_id"]);
            $array['joining_date'] = $userData['joining_date'];
            $array['confirmation_date'] = $userData['confirmation_date'];
            $userBalanceLeave = self::where(["user_id" => $array['user_id'], "month" => date("m",strtotime($array['first_date']." -1 day")), "year" => date("Y",strtotime($array['first_date']." -1 day"))])->first();
            if(date("Y-m-01",strtotime($array['financial_year_start_date'])) == date("Y-m-01",strtotime($array['date']))){
                $data["leave_balance"] = $this->getCreditLeave($array);
            } else {
                $getCompOffCount = getCompOffCount(['column' => "compoff_days","whereBetween"=>["column" => "compoff_start_date","query" => [date("Y-m-01",strtotime($array['first_date']." -1 day")),date("Y-m-t",strtotime($array['first_date']." -1 day"))]],"whereNull" => "deleted_at", "where" =>["compoff_status"=>1, "compoff_marked_as" => 2,"user_id" => $data["user_id"] ]]);
                if(isset($userBalanceLeave->leave_balance) && $userBalanceLeave->leave_balance != "") {
                    $userData['count'] = getLeaveCount(['column' => "leave_days","whereBetween"=>["column" => "leave_date","query" => [date("Y-m-01",strtotime($array['first_date']." -1 day")),date("Y-m-t",strtotime($array['first_date']." -1 day"))]],"whereNull" => "deleted_at", "where" =>["leave_status"=>1,"user_id"=>$array['user_id']]]);
                    $data["leave_balance"] = ($userBalanceLeave->leave_balance + $getCompOffCount) - $userData['count'];
                } else {
                    $data["leave_balance"] = $this->getCreditLeave($array);
                }
            }
            if($data["leave_balance"]<=0) {
                $data["leave_balance"]=0;
            }
            $checkRecord = self::where(["user_id" => $array['user_id'], "year" => $data['year'], "month" => $data['month']])->first();
            if(isset($checkRecord->id) && $checkRecord->id != "") {
                self::where("id",$checkRecord->id)->update($data);
            } else {
                self::create($data);
            }
        }
    }

    public function getCreditLeave($array) {
        $array['first_date'] = date("Y-m-01",strtotime($array['date']));

        $array['available_leaves'] = getUsersAvailableLeave(["where" =>["start_date" => $array['date'], "id" => $array['user_id']]]);

        if(strtotime(date("Y-m",strtotime($array['confirmation_date']))) > strtotime(date("Y-m",strtotime($array['date'])))){
            $array['count'] = getLeaveCount(['column' => "leave_days","whereLessOrEqual"=>["column" => "leave_date","query" => date("Y-m-t",strtotime($array['first_date']." -1 day"))],"where" =>["leave_status"=>1,"user_id"=>$array['user_id']]]);
        } else {
            $array['count'] = getLeaveCount(['column' => "leave_days","whereBetween"=>["column" => "leave_date","query" => [$array['financial_year_start_date'],date("Y-m-t",strtotime($array['first_date']." -1 day"))]],"whereNull" => "deleted_at", "where" =>["leave_status"=>1,"user_id"=>$array['user_id']]]);
        }

        $array['creditLeave'] = $array['available_leaves'] - $array['count'];
        
        if(isset($array['joining_date']) && $array['joining_date'] != "" && strtotime(date('Y-m',strtotime($array['joining_date']))) == strtotime(date('Y-m',strtotime($array['date'])))) {
            $array['creditLeave'] = $array['available_leaves'];
        }
        
        if($array['creditLeave']<=0){
            $array['creditLeave']=0;
        }

        return $array['creditLeave'];
    }
}
