<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
class Holiday extends Model
{
    use SoftDeletes;

    protected $table = 'holidays';

    /** 
     * It will return the count of holidays in a year
     */
    public static function getHolidayCount($searchFilter = array()){
    	$query = Holiday::query();
        if(isset($searchFilter['request_month'])){
            $query->whereMonth('holiday_date', $searchFilter['request_month']);
        }
        if(isset($searchFilter['request_year'])){
            $query->where('holiday_year', $searchFilter['request_year']);
        }
        $query->where('public_holiday','=',1);
        return $query->count();        
    }

    public static function getHolidayList($searchFilter = array()){
        $previousHolidays = $upCommingholidayData = array();
    	$query = Holiday::query();

        if(isset($searchFilter['request_year'])){
            $query->where('holiday_year', $searchFilter['request_year']);
        }
        $query->where('public_holiday','=',1);
        $upQuery = clone $query;
        $peQuery = clone $query;
        $peQuery->where('holiday_date','<',date('Y-m-d'));
        $peQuery->orderBy('holiday_date', 'ASC');
        $upQuery->where('holiday_date','>=',date('Y-m-d'));
        $upQuery->orderBy('holiday_date', 'ASC');
        if(count($peQuery->get())>0){
            $previousHolidays =  $peQuery->get()->toArray();
        }
        if(count($upQuery->get())>0){
            $upCommingholidayData = $upQuery->get()->toArray();
        }
        return array_merge($upCommingholidayData,$previousHolidays);
    }
    
    public static function getHolidayDateList($date){
        $query =  Holiday::select('id');
        $query->where('holiday_date',$date);
        $query->where('public_holiday','=',1);
        $holidayData = $query->orderBy('holiday_date', 'ASC')->get()->toArray();
        
        return $holidayData;
    }

    public static function getWeekendWithHolidayList($date){
        $query =  Holiday::select('id','holiday_date');
        $query->where('holiday_date',$date);
        $query->whereNull('deleted_at');
        $holidayData = $query->orderBy('holiday_date', 'ASC')->get()->toArray();
        
        return $holidayData;
    }

    public function getHolidayDetails($id){
        $query = Holiday::query();
        if ($id) {
            $query->where('id', $id);
        }
        $query->where('public_holiday','=',1);
        $holidayDetail = $query->first()->toArray();
        
        return $holidayDetail;
    }
    
    public function getMonthlyHoliday($month,$year){
        $query = Holiday::query();
        $query->where(DB::raw('Month(`holiday_date`)'), $month);
        $query->where('holiday_year', $year);
        $query->where('public_holiday','=',1);
        $totalHoliday = $query->get()->toArray();
        return $totalHoliday;
    }

    public static function getWeekendDateList($date){
        $query =  Holiday::select('id');
        $query->where('holiday_date',$date);
        $query->where('public_holiday','=',null);
        $query->whereNull('deleted_at');
        $holidayData = $query->orderBy('holiday_date','ASC')->get()->toArray();
        
        return $holidayData;
    }
}
