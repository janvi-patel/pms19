<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Attendance;
use App\TaskEntry;
use App\User;
use App\Jobs\MailJob;
use Carbon\Carbon;
use App\Company;
use App\Task;

class TaskEntriesNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taskEntries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       // For Send User an email when task entries not done 3 days before
        $userObj       =  new User();
        $taskEntry     = new TaskEntry();
        $attendanceObj = new Attendance();
        $task          = new Task();
        $company       = new Company();
        $comapny_id    =  2;
        $comapny_name    =  'Tridhya Tech';
        $userDeatails  = $userObj->getUserwithDepartment();
        
        $attque = Attendance::select('entry_date');
        $attque->whereMonth('entry_date', '=', date(Carbon::now()->month));
        $attque->whereYear('entry_date', '=', date(Carbon::now()->year));
        $attque->orderBy('entry_date','DESC');
        $date = $attque->get()->first()['entry_date'];
        
        /*mail for developer QA and TL for time entry not entered*/

        foreach ($userDeatails as $user) {
            if($user['company_id'] == $comapny_id){
                
                $userId = $user['id'];
                $emp_code = $user['employee_id'];
                $userNotEntered=$taskEntry->getTaskEntryDate($emp_code,$userId,$comapny_name,$comapny_id);
                $userCompanyMailData  = $company->getCompanyViseMailData($user['company_id'])[0];
               
                if($userNotEntered){
                    $reportingToEmail = $userObj->getReportingToEmail($userId)['email'];
                    $developerEmail = $user['email'];
                    $hr_admin_email = $userObj ->getUsersHrAndAdminEmailWithCompany($user['company_id']);
                    $cc = $hr_admin_email['hr_email_address'];  

                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name'];  
                    $data['regards'] = $userCompanyMailData['company_name'];
                    $data['entryDate'] = $userNotEntered;
                    if($reportingToEmail){
                        // email to tl and HR 
//                        $to       = $reportingToEmail;
                        $to       = 'pankit.s@tridhya.com';
                        $template = 'emails.time_entry_pending';
                        $subject  = 'Pending Time Entry For '.$user['first_name'].' '.$user['last_name'];
                        $data['first_name'] = $user['first_name'];
                        $data['last_name'] = $user['last_name'];

                        MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []);
                        $cc='';
                    }
                    if($developerEmail){    
                        // email to user
//                        $to       = $developerEmail;
                        $to       = 'pankit.s@tridhya.com';
                        $template = 'emails.time_entry_developer';
                        $subject  = 'Pending Time Entry - '.$data['entryDate'];
                        $data['first_name'] = $user['first_name'];
                        MailJob::dispatch($to, $from, $subject, '', $template, $data, '', '', []); 
                    }
                }
            }
        }
    }
}
