<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncLeaveBalances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncleavebalances:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For synchronizing monthly leave balances';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $array = [
            "month" => date("m",strtotime("first day of previous month")),
            "year" => date("Y",strtotime("first day of previous month"))
        ];
        syncMontlyBalanceLeave($array);
    }
}
