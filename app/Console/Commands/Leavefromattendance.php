<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Attendance;
use App\User;
use App\Leave;
use Carbon\Carbon;
use DB;


class Leavefromattendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leave:from_Attendence';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For check absent entry in leave';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
               
           DB::enableQueryLog();
            
         $attque = Attendance::whereBetween('attendances.entry_date',[Carbon::now()->startOfMonth()->subMonth(),Carbon::now()]);
    
        $attque->where('attendances.used_for_leave_cron',0);
        $attque->where('attendances.emp_comp','Tridhya Tech');
        $attque->where('attendances.full_day',NULL);
        $attque->whereNotIn('attendances.emp_code', [000,004]);
        $attque->orderBy('attendances.entry_date','DESC');
        $attendencedata = $attque->get();   
   
        foreach($attendencedata as $key=>$value)
        {
            $userid = User::where('employee_id',$value->emp_code)->where('company_id',2)->first();
            $findleave= DB::table('leaves')->whereRaw('"'.$value->entry_date.'" between `leave_start_date` and `leave_end_date`')->where('user_id',$userid->id)->where('deleted_at',NULL)->first();

            $user = new User;
            if(!empty($findleave)){
                if($value['entry_date'] == $findleave->leave_start_date && $value['entry_date'] == $findleave->leave_end_date){
                        $leaveObject = Leave::find($findleave->id);
                        $availeave = $leaveObject->leave_days;
                        if($value['full_day'] == NULL && $value['half_day'] == NULL){
                            $leaveObject->leave_start_type = 0;
                            $leaveObject->leave_end_type = 0;
                            if($availeave==1)
                            {

                            }else
                            {
                                $leave_day =  1 - $availeave;
                                $userid['used_leaves'] = isset($userid['used_leaves'])?$userid['used_leaves']:0;
                   
                                $update_leave = $userid['used_leaves'] + $leave_day;
                                $user->updateUserUsedLeave($userid->id,$update_leave);
                            }
                            $leaveObject->leave_days = 1;
                            $leaveObject->save();
                        }else if($value['full_day'] == NULL && $value['half_day'] == 1){
                            $leaveObject->leave_start_type = 1;
                            $leaveObject->leave_end_type = 1;
                            if($availeave==0.5)
                            {

                            }else
                            {
                                $leave_day =  $availeave - 0.5;
                                $userid['used_leaves'] = isset($userid['used_leaves'])?$userid['used_leaves']:0;
                                $update_leave = $userid['used_leaves'] - $leave_day;
                                $user->updateUserUsedLeave($userid->id,$update_leave);
                            }
                            $leaveObject->leave_days = 0.5;
                            $leaveObject->save();
                            $userid['used_leaves'] = isset($userid['used_leaves'])?$userid['used_leaves']:0;
                        }
                }
            }else{
                    $leave = new Leave;
                    $user = new User();
                    $summary['start_date'] = $value->entry_date;
                    $summary['end_date'] = $value->entry_date;
                    $summary['start_type'] = 0;
                    $summary['end_type'] = 0;
                    if($value->half_day == '1')
                    {
                        $total_leave = 0.5;
                    }
                    else{
                        $total_leave = 1;
                    }
                    $summaryDetails = $this->getLeaveSummary($summary);
                    $userDetails = $user->getUserDetails($userid->id);
                     $userid['used_leaves'] = isset($userid['used_leaves'])?$userid['used_leaves']:0;
                   
                
                    $userDetails = $user->getUserDetails($userid->id);
                    $leave->user_id = isset($userid->id)?$userid->id : '';
                    $leave->approver_id = $userid->reporting_to;
                    $leave->leave_start_date = $value->entry_date;
                    $leave->leave_end_date = $value->entry_date;
                    $leave->return_date = date("Y-m-d", strtotime($summaryDetails['return_date']));
                    $leave->leave_days = $total_leave;
                    $leave->leave_start_type = 0;
                    $leave->leave_end_type = 0;
                    $leave->is_adhoc = 1;
                    $leave->reason = "Auto Generated Leave Adhoc Leave";
                    $leave->approver_comment = '';
                    $leave->leave_status = 1;
                    $leave->save();
                   
                    $leaveID = $leave->id;
                    $update_leave = $userid['used_leaves'] +  $leave->leave_days;
                    $user->updateUserUsedLeave($userid->id,$update_leave);
                }
                $updateleave_cron = Attendance::find($value->id);
                $updateleave_cron->used_for_leave_cron = 1;
                $updateleave_cron->save();
                // echo '<pre>'; print_r($updateleave_cron);exit;
        } 
    }
    public function getLeaveSummary($data){
        $datediff = 0;
        $startDate = isset($data['start_date']) ? date("Y-m-d", strtotime($data['start_date'])) : '';
        $endDate = isset($data['end_date']) ? date("Y-m-d", strtotime($data['end_date'])) : '';
        
        $leave = new Leave;
        $LeaveTotalDates = (new \App\Helpers\CommonHelper)->getAllDates($data['start_date'],$data['end_date']);
        foreach($LeaveTotalDates as $LeaveDate){
            $isHoliday = $leave->isHoliday($LeaveDate);
            if(!$isHoliday){
                $datediff++;
            }
        }
        $leaveDays = $datediff;
        
        $startType = isset($data['start_type']) ? $data['start_type'] : '';
        $endType = isset($data['end_type']) ? $data['end_type'] : '';
       
        if($startType != 0){
            $leaveDays = $leaveDays - 0.5;
        }
        if($endType != 0){
            $leaveDays = $leaveDays - 0.5;
        }
        if($datediff == 1 && ($startType != 0 || $endType != 0)){
            $leaveDays = abs($leaveDays - 0.5);
        }
        if($endType == 1){
            $returnDate = date('Y-m-d', strtotime($endDate));
        }else{
            $returnDate = date('Y-m-d', strtotime($endDate . ' +1 Weekday'));
        }
        $returnData['return_date'] = $returnDate;
        $returnData['leave_days'] = $leaveDays;

        return $returnData;
    }
}
