<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Leave;
use App\Jobs\MailJob;
use Illuminate\Support\Facades\Mail;
use DB;
use File;
class DailyLeaveUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leave:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used To Update Employees Leave Every Day!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start_date = date('Y-m-d',strtotime(date('01-04-Y')));
        if(date('m-d',strtotime($start_date)) > date('m-d'))
        {
            $year = date("Y", strtotime("-1 years"));
            $start_date = date('Y-m-d',strtotime(date('1-04-'.$year)));
        }
        $userId = User::where('users.status',1)->whereNull('users.deleted_at')->select('id','confirmation_date')->get()->toArray();
        if(count($userId))
        {
            foreach($userId as $value)
            {
                $date = $start_date;
                if(isset($value['confirmation_date']) && (strtotime($start_date) < strtotime($value['confirmation_date'])))
                {
                    
                    $date = $value['confirmation_date'];
                }
                $leaves = Leave::where('user_id',$value['id'])->where('leave_start_date','>=',$date)->where('leave_status',1)->pluck('leave_days')->toArray();
               
               
                $usedLeave = array_sum($leaves);
                $user = User::find($value['id']);
                if($user)
                {
                    $user->used_leaves = $usedLeave;
                    $user->save();
                }
                $user = null;

                   

            }
        }
          $data = array();
                  $data['to'] = 'sagar.s@tridhya.com';
                  $data['from'] = 'pms@tridhya.com';
                  $data['name'] = 'Sagar Shah';
                // sagar.s@tridhya.com
              
             
                  
            //  Mail::send('emails.daily_leave_update', ['data'=> $data], function ($message)use($data)
            // {
            //     $message->to($data['to'],$data['name'])->from($data['from'])->subject('Regarding DailyLeaveUpdate cron In PMS');
            
            // });
            
    }
}
