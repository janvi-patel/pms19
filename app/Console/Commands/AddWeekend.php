<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Holiday;

class AddWeekend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Weekend:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add weekend for every 3 months.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $dateS = Carbon::now()->startOfMonth();
       $dateE = Carbon::now()->startOfMonth()->addMonth(3); 
       $all_dates = array();
       $getdates = array();
       while ($dateS->lte($dateE)){
         $all_dates[] = $dateS->toDateString();
         $dateS->addDay();
        }
        foreach ($all_dates as $key => $value) {
            if(Carbon::parse($value)->isWeekend())
            {
                $getdates[] = $value;
            }
        }
        foreach($getdates as $key => $value1)
        {  
            $avilbelHoliday = Holiday::where('holiday_date', Carbon::parse($value1)->format('Y-m-d'))->first();
            if(!empty($avilbelHoliday)) {
                continue;
            }else{
                $holiday = new Holiday();
                $holiday->holiday_year = Carbon::parse($value1)->format('Y');
                $holiday->holiday_name = 'Weekend';
                $holiday->public_holiday = NULL;
                $holiday->holiday_date = Carbon::parse($value1)->format('Y-m-d');
                $holiday->save();
            }
        }         
        print_r('weekend added succesfully');
    }
}
