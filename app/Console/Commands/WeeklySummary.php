<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Company;
use App\Jobs\MailJob;
use App\Attendance;
use App\TaskEntry;
use Carbon\Carbon;
use App\Task;
use Log;
class WeeklySummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weeklysummary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For send weekly summary like working days,leaves,avg working hr,avg logged hr';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $probationArray = array();
        $appraisalArray = array();
        $userObj = new User();
        $companyObj = new Company();
        $taskEntry     = new TaskEntry();
        $attendanceObj = new Attendance();
        $task          = new Task();
        $company       = new Company();
        $userDeatails  = $userObj->getUserwithRole();

        $lastWeekStartDate = date("Y-m-d", strtotime("last week monday"));
        $lastWeekEndDate = date("Y-m-d", strtotime("last week sunday"));
        config(['logging.channels.weekly_summary.path' => storage_path('summary_logs/'.date("Y").'/'.date("M").'/'.'weekly_summary_log/'.$lastWeekStartDate .' to '.$lastWeekEndDate.'.log')]);
        $loggedhours = array();
        $attendanceObj = new Attendance();
        $workingHours = array();
        $averageWorkingHours ='';
        $averageLoggedHours = '';

        foreach($userDeatails as $user){
            $workingHour = array();
            $emp_code = $user['employee_id'];
            $userData = ['employee name' => $user['first_name']." ".$user['last_name'],'employee code' => $emp_code];
            $user_id = $user['id'];
            $company_id = $user['company_id'];
            $avg_working_time_in_sec = $taskEntry->findAvgWorkingTime($company_id);
            $companyName = $company->getCompanyNameById($company_id)['company_name'];
            $userCompanyMailData  = $company->getCompanyViseMailData($company_id)[0];
            $loggedhoursum = 0;
            $absentDays = 0;
            $workingHoursum = $working_seconds = 0;
            $totalWorkingDay = 0;
            $avg_working_hr = $avg_log_hr = '0:00:00';
            $userAttendData = $taskEntry->userWeeklyAttendDataForMail($companyName,$emp_code,$lastWeekStartDate,$lastWeekEndDate,$company_id);

            if(count($userAttendData) > 0 ){
                $totalWorkingDay = $userAttendData[0]['full_days'] + ($userAttendData[0]['half_days'] / 2);
                $loggedhours = $taskEntry->userWeeklyLoggedHoursForMail($user_id,$lastWeekStartDate,$lastWeekEndDate);

                if (array_key_exists($emp_code,$loggedhours)){
                    $logged_hours = (new \App\Helpers\CommonHelper)->displayTaskTime($loggedhours[$emp_code]);
                    $absentDays = (($userAttendData[0]['absent_days'] == '')?0:($userAttendData[0]['absent_days'])) +
                                    (($userAttendData[0]['half_days'] == '')?0:($userAttendData[0]['half_days']/2)) +
                                    (($userAttendData[0]['incorrect_days'] == '')?0:($userAttendData[0]['incorrect_days']));

                    $hoursMinutes = (explode(".",$logged_hours));
                    $hours = $minutes = '00';

                    if(isset($hoursMinutes[0])){
                        $hours = $hoursMinutes[0];
                        $hours = (strlen($hours) == 1)?'0'.$hours:$hours;
                    }
                    if(isset($hoursMinutes[1])){
                        $minutes = $hoursMinutes[1];
                        $minutes = (strlen($minutes) == 1)?$minutes.'0':$minutes;
                    }
                    $str_time = $hours.':'.$minutes.':00';
                    sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
                    $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
                    if($totalWorkingDay != 0){
                        $avg_log_hr_sec =  $time_seconds/$totalWorkingDay;
                        $avg_log_hr = gmdate("H:i:s", $avg_log_hr_sec);
                    }
                }
                $workingHours = $attendanceObj->weeklyWorkingHourforMail($emp_code,$companyName,$lastWeekStartDate,$lastWeekEndDate);
                foreach($workingHours as $working_details_val){
                    $parsed = date_parse($working_details_val['working_hours']);
                    $working_seconds = $working_seconds + $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                }

                if($totalWorkingDay != 0){
                    $avg_hr_sec = $working_seconds/$totalWorkingDay;
                    $avg_working_hr = gmdate("H:i:s",$avg_hr_sec );
                }

                $avg_log_limit = $taskEntry->findAvgLoggedTime($company_id);
                $hr_admin_email = $userObj->getUsersHrAndAdminEmailWithCompany($company_id);
                $reportingToEmail = $userObj->getPmTlEmail($user_id); //Reporting email
                $cc = array();
                if(isset($hr_admin_email['hr_email_address']) && $hr_admin_email['hr_email_address'] != "") { $cc[] = $hr_admin_email['hr_email_address']; }
                if(isset($hr_admin_email['email']) && $hr_admin_email['email'] != "") { $cc[] = $hr_admin_email['email']; }
                if(isset($reportingToEmail['pm']) && $reportingToEmail['pm'] != "") { $cc[] = $reportingToEmail['pm']; }
                if(isset($reportingToEmail['tl']) && $reportingToEmail['tl'] != "") { $cc[] = $reportingToEmail['tl']; }
                $from['email'] = $userCompanyMailData['from_address'];
                $from['name']  = $userCompanyMailData['company_name'];

                $to = $user['email'];

                $template = 'emails.employee_weekly_summary';
                $subject = 'Weekly Summary ('.$lastWeekStartDate.' To '.$lastWeekEndDate .')';
                $data['name'] = $user['first_name'];
                $data['regards'] = $userCompanyMailData['company_name'];

                $data['name'] = $user['first_name'].' '.$user['last_name'];
                $data['avg_loggeed'] = $avg_log_hr;
                $data['avg_working'] = $avg_working_hr;
                $data['totalWorkingDay'] = $totalWorkingDay;
                $data['leaveDay'] = $absentDays;
                $data['avg_log_limit'] = $avg_log_limit;
                $data['avg_hr_sec'] = $avg_hr_sec;
                $data['avg_working_time'] = gmdate('H:i:s', $avg_working_time_in_sec);
                try {
                    MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc, '', []);
                    $this->loggedType('success',$userData);
                }catch (\Exception $ex) {
                    $this->loggedType('failed',$userData,$ex->getMessage());
                }
            }
            else
            {
                $this->loggedType('time_entry_error',$userData);
            }
        }
    }

    public function loggedType($status,$user,$exception = null)
    {
        if($status == 'success')
        {
            Log::channel('weekly_summary')->info("\t ".$status."\t",$user);
        }
        if($status == "failed")
        {
            Log::channel('weekly_summary')->error("\t failed ".$exception."\t",$user);
        }
        if($status == "loggeed_hour_error")
        {
            Log::channel('weekly_summary')->error("\t logged hour is pending\t",$user);
        }
        if($status == "time_entry_error")
        {
            Log::channel('weekly_summary')->error("\t maybe the employee is absent or maybe time entry is pending\t",$user);
        }
    }
}
