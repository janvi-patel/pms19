<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\ProjectLead;
use App\Jobs\MailJob;
use Illuminate\Support\Facades\Mail;
use DB;
use File;
class DailyLeadsClose extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lead:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to close leads pending more then 2 months!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $leadIdArray = ProjectLead::where('updated_at','<',date("Y-m-d", strtotime ( '-2 month' , strtotime(date("Y-m-d")  ))))
            ->where('company_id',2)
            ->whereIn('estimation_status', [1,2])
            ->whereNull('deleted_at')
            ->groupBy('id')->pluck('id');
       
            $updateLeads = ProjectLead::query()->whereIn('id',$leadIdArray)->update(['estimation_status' => 3]);
            if($updateLeads){
                echo 'leads closed succesfully' ;
            }
    }
}
