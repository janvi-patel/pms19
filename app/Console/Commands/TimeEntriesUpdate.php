<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Attendance;
use TimeSheetHelper;
class TimeEntriesUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timeentries:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically time entry update after every 30 min';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $currentTime = date("H:i");
        if($currentTime >= '23:45'){
            TimeSheetHelper::importTimeEntries(["company_id" => 2,"date" => date("Y-m-d")]);
        }
        if($currentTime < '23:45'){
            $mainResponse = TimeSheetHelper::getPunchData(["date" => date("Y-m-d"), "company_code" => "TRD"]);
           // echo"<pre>";print_r($mainResponse);exit;
            if(isset($mainResponse['data']) && count($mainResponse['data']) > 0){
                foreach($mainResponse['data'] as $key => $value){
                    $isEntry = Attendance::where(['entry_date' => $value['LogDate'], 'emp_comp' => 'Tridhya Tech','emp_code' => str_replace('TRD','',$value['EmployeeCode'])]);
                    if(count($isEntry->get()->toArray()) <= 0){
                        $userDeatails = User::where("employee_id",str_replace('TRD','',$value['EmployeeCode']))->where('company_id',2)->first();
                        $isEntry = new Attendance();
                        if(isset($userDeatails->first_name)){
                            $isEntry->emp_name = $userDeatails->first_name. " ".$userDeatails->last_name;
                            $isEntry->user_id = $userDeatails->id;
                        }
                        $first_in = '';
                        if(!empty($value['FullLogTime'])){
                            $array = explode(',',$value['FullLogTime']);
                            if(isset($array)){
                                $first_time = explode('(',$array[0]);
                                if(isset($first_time[0])){
                                    $first_in = $first_time[0];
                                }
                            }
                        }
                        $isEntry->first_in = $first_in;
                        $isEntry->entry_date = $value['LogDate'];
                        $isEntry->emp_comp = 'Tridhya Tech';
                        $isEntry->emp_code = str_replace('TRD','',$value['EmployeeCode']);
                        $isEntry->content = $value['FullLogTime'];
                        $isEntry->save();
                    } else {
                        $isEntry = $isEntry->first();
                        $isEntry->content = $value['FullLogTime'];
                        $isEntry->save();
                    }
                }
            }
        }
    }
}
