<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Company;
use App\Jobs\MailJob;
use App\Attendance;
use App\TaskEntry;
use Carbon\Carbon;
use App\Task;
use Log;

class MonthlyAvg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monthlyavg:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For monthly average not matches';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $probationArray = array();
        $appraisalArray = array();
        $userObj = new User();
        $companyObj = new Company();
        $taskEntry     = new TaskEntry();
        $attendanceObj = new Attendance();
        $task          = new Task();
        $company       = new Company();
        $userDeatails  = $userObj->getUserwithRole();
        
        $last_month = date('m', strtotime('last month'));
        $last_month_year = date('Y', strtotime('last month'));
        $first_date =  date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        config(['logging.channels.monthly_working_avg.path' => storage_path('summary_logs/'.date("Y").'/'.date('M', strtotime('last month')).'/'.'monthly_avg_working_log/'.$first_date .' to '.$last_date.'.log')]);
        $attque = Attendance::select('entry_date');
        $attque->whereMonth('entry_date', '=', date($last_month));
        $attque->whereYear('entry_date', '=', date($last_month_year));
        $attque->orderBy('entry_date','DESC');
        $date = $attque->get()->first()['entry_date'];
        foreach($userDeatails as $user){
            $workingHour = array();
            $emp_code = $user['employee_id'];
            $userData = ['employee name' => $user['first_name']." ".$user['last_name'],'employee code' => $emp_code];
            $user_id = $user['id'];
            $company_id = $user['company_id'];
            $avg_working_time_in_sec = $taskEntry->findAvgWorkingTime($company_id);
            $userCompanyMailData  = $company->getCompanyViseMailData($company_id)[0];
            $companyName = $company->getCompanyNameById($company_id)['company_name'];
            $working_seconds = 0 ;
            $avg_working_hr_sec = 0 ;
            $avg_hr = '00:00:00';
            $totalWorkingDay = 0;
            $userAttendData = $taskEntry->userAttendDataForMail($companyName,$emp_code,$last_month,$last_month_year,$company_id);
            
            if(count($userAttendData)>0){
                $totalWorkingDay = $userAttendData[0]['full_days'] + ($userAttendData[0]['half_days'] / 2);
                $workingHours = $attendanceObj->workingHourforMail($emp_code,$companyName,$last_month,$last_month_year,$date);
                foreach($workingHours as $working_details_val){
                    $parsed = date_parse($working_details_val['working_hours']);
                    $working_seconds = $working_seconds + $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                }
                if($totalWorkingDay != 0){
                    $avg_working_hr_sec = $working_seconds/$totalWorkingDay;
                }
                if($avg_working_hr_sec < $avg_working_time_in_sec){
                    $reportingToEmail = $userObj->getReportingToEmail($user_id)['email']; //Reporting email
                    $hr_admin_email = $userObj->getUsersHrAndAdminEmailWithCompany($company_id);
                    $cc = array($hr_admin_email['hr_email_address'],$hr_admin_email['email'],$reportingToEmail);

                    $from['email'] = $userCompanyMailData['from_address'];
                    $from['name']  = $userCompanyMailData['company_name']; 
                    $data['regards'] = $userCompanyMailData['company_name'];

                    $to = $user['email'];
                        
                    $template = 'emails.employee_working_hours_status';
                    $subject = 'Short of average working hours for -'.date('M', strtotime('last month')).'/'.$last_month_year;
                    $data['name'] = $user['first_name']." ".$user['last_name'];
                    $data['hours'] = gmdate('H:i:s',$avg_working_hr_sec);
                    $data['standard_hours'] = gmdate('H:i:s', $avg_working_time_in_sec);
                    $data['month'] = date('M', strtotime('last month'));
                    try {
                        MailJob::dispatch($to, $from, $subject, '', $template, $data,$cc, '', []);
                        $this->loggedType('success',$userData);
                    }catch (\Exception $ex) {
                        $this->loggedType('failed',$userData,$ex->getMessage());     
                    }
                }
                else
                {
                    $this->loggedType('avg_hour_completed',$userData);
                }
            }
            else
            {
                $this->loggedType('time_entry_error',$userData);
            }
        }
    }

    public function loggedType($status,$user,$exception = null)
    {
        if($status == 'success')
        {
            Log::channel('monthly_working_avg')->info("\t ".$status."\t",$user);
        }
        if($status == "failed")
        {
            Log::channel('monthly_working_avg')->error("\t ".$exception."\t",$user);
        }
        if($status == "avg_hour_completed")
        {
            Log::channel('monthly_working_avg')->error("\t an average working hour has been completed. No Need to send a e-mail\t",$user);
        }
        if($status == "time_entry_error")
        {
            Log::channel('monthly_working_avg')->error("\t maybe the employee is absent or maybe time entry is pending\t",$user);
        }
    }
}
