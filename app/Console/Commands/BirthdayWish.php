<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Company;
use App\Jobs\MailJob;

class BirthdayWish extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthday:wish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $toDayDate = date("m-d");
        $userObj = new User;
        $getUser = $userObj->where('birthdate','LIKE','%'.$toDayDate.'%')->where('status',1)->select('email','company_id','id')->get();
       // echo "<pre>";print_r($getUser);exit;
        if(!$getUser->isEmpty()){
            foreach($getUser as $val){
                if($val->email != ''){
                    $company_data = Company::where('id',$val->company_id)->first();

                    $from['email'] = $company_data['from_address'];
                    $from['name']  = $company_data['company_name'];
                    $data['regards'] = $company_data['company_name'];
                    $data['comapny_id'] = $val->company_id;
                    $hr_admin_email = $userObj->getUsersHrAndAdminEmailWithCompany($val->company_id);
                    $cc_array = array($hr_admin_email['hr_email_address'],$hr_admin_email['email'],'prakash.s@tridhya.com');
                    $pm_email = getUsersPm($val->id);
                //    dd($pm_email);
                    if($pm_email != false){
                        $cc_array[] = $pm_email;
                    }
                    //$cc = array($hr_admin_email['hr_email_address'],$hr_admin_email['email'],$reportingToEmail['pm'],$reportingToEmail['tl']);
                    $to = $val->email;
                    $template = 'emails.birthday_wish';
                    $subject = 'Happy Birthday From '.$company_data['company_name'];

                    MailJob::dispatch($to, $from, $subject, '', $template, $data, $cc_array, '', []);
                }
            }
        }
    }
}
