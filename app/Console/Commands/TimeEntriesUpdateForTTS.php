<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use TimeSheetHelper;
class TimeEntriesUpdateForTTS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timeentriestts:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically time entry update after every 30 min';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        TimeSheetHelper::importTimeEntries(["company_id" => 6,"date" => date("Y-m-d")]);
    }
}
