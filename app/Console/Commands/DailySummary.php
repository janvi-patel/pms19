<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Company;
use App\Attendance;
use App\TaskEntry;
use Carbon\Carbon;
use App\Task;
use App\Jobs\MailJob;
use Log;
use App\Holiday;
class DailySummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dailysummary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For send daily summary like leaves,working hr,logged hr';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->checkHoliday(date("Y-m-d")) || $this->checkWeekend(date("Y-m-d"))){
            return true;
        }
        $count = 1;
        $probationArray = array();
        $appraisalArray = array();
        $userObj = new User();
        $taskEntry     = new TaskEntry();
        $task          = new Task();
        $company       = new Company();
        $userDeatails  = $userObj->getUserwithRole();
        $yesterdayDate = date("Y-m-d", strtotime("-1 day"));
        while($this->checkHoliday($yesterdayDate) || $this->checkWeekend($yesterdayDate))
        {
            $count++;
            $yesterdayDate = date("Y-m-d", strtotime("-".$count." day"));
        }
        config(['logging.channels.daily_summary.path' => storage_path('summary_logs/'.date("Y").'/'.date("M").'/'.'daily_summary_log/'.$yesterdayDate.'.log')]);
        $loggedhours = array();
        $attendanceObj = new Attendance();
        $workingHours = array();
        $averageWorkingHours ='';
        $averageLoggedHours = '';
        foreach($userDeatails as $user){
            $workingHour = array();
            $emp_code = $user['employee_id'];
            $userData = ['employee name' => $user['first_name']." ".$user['last_name'],'employee code' => $emp_code];
            $user_id = $user['id'];
            $company_id = $user['company_id'];
            $companyName = $company->getCompanyNameById($company_id)['company_name'];
            $userCompanyMailData  = $company->getCompanyViseMailData($company_id)[0];
            $loggedhoursum = 0;
            $absentDays = 0;
            $workingHoursum = $working_seconds = 0;
            $totalWorkingDay = 0;
            $avg_working_hr = $avg_log_hr = '0:00:00';
            $userAttendData = $taskEntry->userDailyAttendDataForMail($companyName,$emp_code,$yesterdayDate,$company_id);
            if(count($userAttendData) > 0 ){
                $totalWorkingDay = $userAttendData[0]['full_days'] + ($userAttendData[0]['half_days'] / 2);
                $loggedhours = $taskEntry->userDailyLoggedHoursForMail($user_id,$yesterdayDate);

                if (array_key_exists($emp_code,$loggedhours)){
                    $logged_hours = (new \App\Helpers\CommonHelper)->displayTaskTime($loggedhours[$emp_code]);
                    $absentDays = (($userAttendData[0]['absent_days'] == '')?0:($userAttendData[0]['absent_days'])) +
                                    (($userAttendData[0]['half_days'] == '')?0:($userAttendData[0]['half_days']/2)) +
                                    (($userAttendData[0]['incorrect_days'] == '')?0:($userAttendData[0]['incorrect_days']));
                    $hoursMinutes = (explode(".",$logged_hours));
                    $hours = $minutes = '00';

                    if(isset($hoursMinutes[0])){
                        $hours = $hoursMinutes[0];
                        $hours = (strlen($hours) == 1)?'0'.$hours:$hours;
                    }
                    if(isset($hoursMinutes[1])){
                        $minutes = $hoursMinutes[1];
                        $minutes = (strlen($minutes) == 1)?$minutes.'0':$minutes;
                    }
                    $str_time = $hours.':'.$minutes.':00';
                    sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
                    $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
                    if($totalWorkingDay != 0){
                        $avg_log_hr_sec =  $time_seconds/$totalWorkingDay;
                        if($totalWorkingDay == 0.5)
                        {
                            $avg_log_hr_sec =  $time_seconds;
                        }
                        $avg_log_hr = gmdate("H:i:s", $avg_log_hr_sec);
                    }
                }
                $workingHours = $attendanceObj->workingHourbyUser($user_id,$yesterdayDate,$companyName);
                foreach($workingHours as $working_details_val){
                    $parsed = date_parse($working_details_val['working_hours']);
                    $working_seconds = $working_seconds + $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                }

                if($totalWorkingDay != 0){
                    $avg_hr_sec = $working_seconds/$totalWorkingDay;
                    if($totalWorkingDay == 0.5)
                    {
                        $avg_hr_sec =  $working_seconds;
                    }
                    $avg_working_hr = gmdate("H:i:s",$avg_hr_sec );
                }

                $avg_log_limit = $taskEntry->findAvgLoggedTime($company_id);
                $hr_admin_email = $userObj->getUsersHrAndAdminEmailWithCompany($company_id);

                $reportingToEmail = $userObj->getPmTlEmail($user_id); //Reporting email
                // $cc = array($hr_admin_email['hr_email_address'],$hr_admin_email['email'],$reportingToEmail['pm'],$reportingToEmail['tl']);
                $cc = array();
                if(isset($hr_admin_email['hr_email_address']) && $hr_admin_email['hr_email_address'] != "") { $cc[] = $hr_admin_email['hr_email_address']; }
                if(isset($hr_admin_email['email']) && $hr_admin_email['email'] != "") { $cc[] = $hr_admin_email['email']; }
                if(isset($reportingToEmail['pm']) && $reportingToEmail['pm'] != "") { $cc[] = $reportingToEmail['pm']; }
                if(isset($reportingToEmail['tl']) && $reportingToEmail['tl'] != "") { $cc[] = $reportingToEmail['tl']; }

                $from['email'] = $userCompanyMailData['from_address'];
                $from['name']  = $userCompanyMailData['company_name'];

                $to = $user['email'];

                $template = 'emails.employee_daily_summary';
                $subject = 'Daily Summary ('.$yesterdayDate.')';
                $data['regards'] = $userCompanyMailData['company_name'];

                $data['name'] = $user['first_name'].' '.$user['last_name'];
                $data['loggeed_hour'] = $avg_log_hr;
                $data['working_hour'] = $avg_working_hr;
                $data['date'] = $yesterdayDate;
                // $data['totalWorkingDay'] = $totalWorkingDay;
                // $data['leaveDay'] = $absentDays;
                try {
                     MailJob::dispatch($to, $from, $subject, '', $template, $data,$cc, '', []);
                    $this->loggedType('success',$userData);
                }catch (\Exception $ex) {
                    $this->loggedType('failed',$userData,$ex->getMessage());
                }
            }
            else
            {
                $this->loggedType('time_entry_error',$userData);
            }
        }
    }

    public function loggedType($status,$user,$exception = null)
    {
        if($status == 'success')
        {
            Log::channel('daily_summary')->info("\t ".$status."\t",$user);
        }
        if($status == "failed")
        {
            Log::channel('daily_summary')->error("\t failed ".$exception."\t",$user);
        }
        if($status == "loggeed_hour_error")
        {
            Log::channel('daily_summary')->error("\t logged hour is pending\t",$user);
        }
        if($status == "time_entry_error")
        {
            Log::channel('daily_summary')->error("\t maybe time entry is pending\t",$user);
        }
    }

    public function checkHoliday($yesterdayDate)
    {
        $holiday = new Holiday();
        $isHoliday =  $holiday->getHolidayDateList($yesterdayDate);
        if(count($isHoliday) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function checkWeekend($yesterdayDate){
        $holiday = new Holiday();
        // $date = date("l", strtotime($yesterdayDate));
        $isWeekend = $holiday->getWeekendDateList($yesterdayDate);
        if(count($isWeekend) > 0){
            return true;
        } else {
            return false;
        }
    }
}
