<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Leave;
use App\Jobs\MailJob;
use Illuminate\Support\Facades\Mail;
use DB;
use File;
class ResignUserDisable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resign:disable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resign user should be disable after resign date.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $userList = User::where('users.status',1)->whereNull('users.deleted_at')->where('employee_status',3)->where('last_date' ,'<',date('Y-m-d'))->get();
       //echo "<pre>";print_r($userList);exit;
        if(!empty($userList)){
            foreach ($userList as $key => $value) {
                $user = User::find($value->id);
                $user->status = 0;
                $user->save();
            }
        }
       // echo "<pre>";print_r($userList);exit;
    }
}
