<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Company;
use App\Jobs\MailJob;
use App\Attendance;
use App\TaskEntry;
use Carbon\Carbon;
use App\Task;
use Log;
use App\RoleUser;

class MonthlyLoggedAvg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monthlyloggedavg:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For logged hours are less then logged hr limit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $probationArray = array();
        $appraisalArray = array();
        $userObj = new User();
        $role_user = new RoleUser();
        $companyObj = new Company();
        $taskEntry     = new TaskEntry();
        $attendanceObj = new Attendance();
        $task          = new Task();
        $company       = new Company();
        $userDeatails  = $userObj->getUserwithRole();
        $userArray = $role_user->whereIn('role_id',[22,9,14,7,5])->orderBy('user_id')->pluck('user_id')->toArray();
        $last_month = date('m', strtotime('last month'));
        $last_month_year = date('Y', strtotime('last month'));
        $first_date =  date('Y-m-d', strtotime('first day of last month'));
        $last_date = date('Y-m-d', strtotime('last day of last month'));
        config(['logging.channels.monthly_legged_avg.path' => storage_path('summary_logs/'.date("Y").'/'.date('M', strtotime('last month')).'/'.'monthly_avg_legged_log/'.$first_date .' to '.$last_date.'.log')]);
        $attque = Attendance::select('entry_date');
        $attque->whereMonth('entry_date', '=', date($last_month));
        $attque->whereYear('entry_date', '=', date($last_month_year));
        $attque->orderBy('entry_date','DESC');
        $date = $attque->get()->first()['entry_date'];
        $loggedhours = array();
        $workingHours = array();
        $averageWorkingHours ='';
        $averageLoggedHours = '';

        foreach($userDeatails as $user){
            if(!in_array($user['id'],$userArray)){
                $workingHour = array();
                $emp_code = $user['employee_id'];
                $userData = ['employee name' => $user['first_name']." ".$user['last_name'],'employee code' => $emp_code];
                $user_id = $user['id'];
                $company_id = $user['company_id'];
                $companyName = $company->getCompanyNameById($company_id)['company_name'];
                $userCompanyMailData  = $company->getCompanyViseMailData($company_id)[0];
                $loggedhoursum = 0;
                $workingHoursum = 0;
                $totalWorkingDay = 0;
                $userAttendData = $taskEntry->userAttendDataForMail($companyName,$emp_code,$last_month,$last_month_year,$company_id);
                $avg_log_hr = '0:00:00';
                if(count($userAttendData)> 0 ){
                    $totalWorkingDay = $userAttendData[0]['full_days'] + ($userAttendData[0]['half_days'] / 2);
                    $loggedhours = $taskEntry->userLoggedHoursForMail($user_id,$last_month,$last_month_year,$date);
                    if (array_key_exists($emp_code,$loggedhours)){
                        $logged_hours = (new \App\Helpers\CommonHelper)->displayTaskTime($loggedhours[$emp_code]);
                        $hoursMinutes = (explode(".",$logged_hours));
                        $hours = $minutes = '00';

                        if(isset($hoursMinutes[0])){
                            $hours = $hoursMinutes[0]; 
                            $hours = (strlen($hours) == 1)?'0'.$hours:$hours;
                        }

                        if(isset($hoursMinutes[1])){
                            $minutes = $hoursMinutes[1];
                            $minutes = (strlen($minutes) == 1)?$minutes.'0':$minutes;
                        }

                        $str_time = $hours.':'.$minutes.':00';
                        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

                        $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
                        if($totalWorkingDay != 0){
                            $avg_log_hr_sec =  $time_seconds/$totalWorkingDay;
                            $avg_log_hr = gmdate("H:i:s", $avg_log_hr_sec); 
                        }
                    }
                    $avg_log_limit = $taskEntry->findAvgLoggedTime($company_id);
                    if(strtotime($avg_log_limit) > strtotime($avg_log_hr)){
                        $hr_admin_email = $userObj->getUsersHrAndAdminEmailWithCompany($company_id);
                        $reportingToEmail = $userObj->getReportingToEmail($user_id)['email'];
                        $cc = array($hr_admin_email['hr_email_address'],$hr_admin_email['email'],$reportingToEmail);
                            //Reporting email

                        $from['email'] = $userCompanyMailData['from_address'];
                        $from['name']  = $userCompanyMailData['company_name']; 

                        $to = $user['email'];
                    
                        $template = 'emails.employee_logged_hours_status';
                        $subject = 'Short of average logged hours -'.date('M', strtotime('last month')).'/'.$last_month_year;
                        $data['name'] = $user['first_name'].' '.$user['last_name'] ;
                        $data['regards'] = $userCompanyMailData['company_name'];
                        $data['hours'] = $avg_log_hr;
                        $data['standard_hours'] = $avg_log_limit;
                        $data['month'] = date('M', strtotime('last month'));
                        try {
                            MailJob::dispatch($to, $from, $subject, '', $template, $data,$cc, '', []);
                            $this->loggedType('success',$userData);
                        }catch (\Exception $ex) {
                            $this->loggedType('failed',$userData,$ex->getMessage());     
                        }
                    }
                    else
                    {
                        $this->loggedType('avg_hour_completed',$userData);
                    } 
                }
                else
                {
                    $this->loggedType('time_entry_error',$userData);
                }
            }
        }
    }

    public function loggedType($status,$user,$exception = null)
    {
        if($status == 'success')
        {
            Log::channel('monthly_legged_avg')->info("\t ".$status."\t",$user);
        }
        if($status == "failed")
        {
            Log::channel('monthly_legged_avg')->error("\t ".$exception."\t",$user);
        }
        if($status == "loggeed_hour_error")
        {
            Log::channel('monthly_legged_avg')->error("\t logged hour is pending\t",$user);
        }
        if($status == "time_entry_error")
        {
            Log::channel('monthly_legged_avg')->error("\t maybe the employee is absent or maybe time entry is pending\t",$user);
        }
        if($status == "avg_hour_completed")
        {
            Log::channel('monthly_legged_avg')->info("\t an average Logged hour has been completed. No Need to send a e-mail\t",$user);
        }
    }
}
