<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         'App\Console\Commands\MonthlyAvg',
         'App\Console\Commands\MonthlyLoggedAvg',
         'App\Console\Commands\monthlysummary',
         'App\Console\Commands\weeklysummary',
         'App\Console\Commands\dailysummary',
         'App\Console\Commands\BirthdayWish',
         'App\Console\Commands\DailyLeaveUpdate',
         'App\Console\Commands\Leavefromattendance',
          'App\Console\Commands\DailyLeadsClose',
         'App\Console\Commands\AddWeekend',
         'App\Console\Commands\ResignUserDisable',
         'App\Console\Commands\TimeEntriesUpdate',
         'App\Console\Commands\TimeEntriesUpdateForTTS',
         'App\Console\Commands\SyncLeaveBalances',

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
//        $schedule->command('timeEntry:HR')->everyMinute();
//        $schedule->command('endofprobation:hr')->monthly();                 
//        $schedule->command('latecomer:users')->monthly(); 
//        $schedule->command('taskEntries')->->everyMinute();               
        
         $schedule->command('monthlysummary')->monthlyOn(3, '15:00'); //->monthly();                 
         $schedule->command('monthlyavg:users')->monthlyOn(3, '15:30'); //->monthly();                 
         $schedule->command('monthlyloggedavg:users')->monthlyOn(3, '16:00'); //->monthly(); 
         $schedule->command('syncleavebalances:users')->monthlyOn(1, '10:00'); //->monthly(); 
         $schedule->command('Weekend:add')->monthlyOn(1, '10:00'); //->monthly();                 
         $schedule->command('weeklysummary')->weeklyOn(1, '15:00'); //->weekly();
         $schedule->command('dailysummary')->dailyAt('15:00');  
         $schedule->command('birthday:wish')->dailyAt('10:00');  
         $schedule->command('leave:update')->hourly();
         $schedule->command('lead:close')->dailyAt('14:00');
         $schedule->command('leave:from_Attendence')->dailyAt('10:00');  
         $schedule->command('resign:disable')->dailyAt('00:00');  
         $schedule->command('timeentries:update')->everyFifteenMinutes();  
         $schedule->command('timeentriestts:update')->dailyAt('23:45');  

//        $schedule->command('monthlysummary')->dailyAt('20:36'); //->monthly();                 
//        $schedule->command('monthlyavg:users')->dailyAt('20:37'); //->monthly();                 
//        $schedule->command('monthlyloggedavg:users')->dailyAt('20:38'); //->monthly();                 
//        $schedule->command('weeklysummary')->dailyAt('20:39'); //->weekly();
//        $schedule->command('dailysummary')->dailyAt('20:40');               
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
