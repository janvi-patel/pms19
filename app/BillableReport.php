<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class BillableReport extends BaseModel
{
    protected $fillable = ['user_id', 'billable_hours', 'logged_hours', 'non_billable_hours', 'non_billable_logged_hours', 'start_date', 'end_date', 'working_days'];

    function createMontlyBillableReport($array) {
        $data['working_days'] = 0;
        $array['date'] = $array['year']."-".$array['month']."-01";
        $data['start_date'] = date("Y-m-01", strtotime($array['date']));
        $data['end_date'] = date("Y-m-t", strtotime($array['date']));
        $getUsers = User::whereNull('deleted_at')->where('status',1)->pluck('id')->toArray();
        foreach($getUsers as $userId) {
            $userHasHours  = getProjectEntrySum(['column' => "log_hours", "where" => ["user_id" => $userId], "whereNull" => "deleted_at", "whereGreterOrEqual" => ["column" => "log_date","query" => "2022-03-01"]]);
            if($userHasHours > 0) {
                $data['user_id'] = $userId;

        $query = Attendance::selectRaw('emp_code, concat(`users`.`first_name`," ",`users`.`last_name`) as emp_name,
        SUM(full_day) as full_days, SUM(half_day) as half_days, SUM(early_going) as early_going_days,SUM(adjustment) as
        adjustment,SUM(absent) as absent_days,
        SUM(late_comer) as late_days, SUM(incorrect_entry) as incorrect_days,users.reporting_to,users.department,users.company_id,users.id as userId, users.designation_id');
        $query->leftJoin('users', 'user_id', 'users.id');
        $query->where('users.id',$userId);
        $query->where('entry_date','>=',$data['start_date']);
        $query->where('entry_date','<=', $data['end_date'] );
        $record = $query->get();
        $totalWorkingDay = 0;
        foreach($record as $userSummaryDetail){
            $totalWorkingDay = $userSummaryDetail['full_days'] + $userSummaryDetail['half_days'] / 2;
        }
                $data['working_days'] = $totalWorkingDay;
                $data['billable_hours'] = getProjectTaskSum(['column' => "estimated_hours", "whereNull" => "deleted_at", "where" => ["user_id" => $userId], "whereBetween"=> ["column" => "task_start_date", "query" => [$data['start_date'],$data['end_date']]], "whereNotIn" => ["column" => "project_id", "array" => [517, 516, 518, 458]]]);
                $data['logged_hours'] = getProjectEntrySum(['column' => "log_hours", "whereNull" => "deleted_at", "where" => ["user_id" => $userId], "whereBetween"=>["column" => "log_date", "query" => [$data['start_date'],$data['end_date']]], "whereNotIn" => ["column" => "project_id","array" => [517, 516, 518, 458]]]);
                $data['non_billable_hours'] = getProjectTaskSum(['column' => "estimated_hours", "whereNull" => "deleted_at", "where" => ["user_id" => $userId], "whereBetween"=> ["column" => "task_start_date", "query" => [$data['start_date'],$data['end_date']]], "whereIn" => ["column" => "project_id","array" => [517, 516, 518, 458]]]);
                $data['non_billable_logged_hours'] = getProjectEntrySum(['column' => "log_hours", "whereNull" => "deleted_at", "where" => ["user_id" => $userId], "whereBetween"=> ["column" => "log_date", "query" => [$data['start_date'],$data['end_date']]], "whereIn" => ["column" => "project_id","array" => [517, 516, 518, 458]]]);
                $checkRecord = self::where(["user_id" => $data['user_id'], "start_date" => $data['start_date'], "end_date" => $data['end_date']])->first();
                if(isset($checkRecord->id) && $checkRecord->id != "") {
                    self::where("id",$checkRecord->id)->update($data);
                } else {
                    self::create($data);
                }
            }
        }
    }
}
