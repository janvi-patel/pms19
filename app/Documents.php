<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Documents extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = "pms_documents";
}
