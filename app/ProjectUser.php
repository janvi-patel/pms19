<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Project;

class ProjectUser extends Model
{
    use SoftDeletes;

    protected $table = 'project_users'; 
    
    public function user(){
        return $this->hasMany(User::class, 'id', 'user_id');
    }
    public function projects() {
        return $this->hasOne(Projects::class, 'id', 'project_id');
    }
    public function getProjectUsers($projectID){
        $projectUser = ProjectUser::query()->with(['user']);

        if ($projectID) {
            $projectUser->where('project_id', $projectID);
        }

        $projectData = $projectUser->orderBy('id','ASC')->get()->toArray();
        
        return $projectData;
    }
    
    // get projects name
    public function projectList(){
        return $this->hasOne('App\Project', 'id', 'project_id')->select('id','project_name');
    }
    
    public function getAssignedProj($userID) {
//        $projectUser = ProjectUser::select('id');
//        $projectUser->where('user_id',$userID);
//       
//        return $projectUser->get()->toArray();  
        
        

        /*$dataQuery = Project::leftjoin('cr', function($leftJoin)use($companyId) {
                                $leftJoin->on('projects.id', '=', 'cr.project_id');
                                $leftJoin->whereNull('cr.deleted_at');
                            })->
                            leftjoin('clients', 'projects.client_id', '=', 'clients.id')->
                            leftjoin('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                            leftjoin('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                            leftjoin('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                            leftjoin('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                            leftjoin('project_types', 'projects.project_type', '=', 'project_types.id')->
                            where(function($q) use ($projectTeam) {
                                $q->where('projects.team_leader',Auth::user()->id)
                                ->orWhereIn('projects.id',$projectTeam);
                            })->
                            whereNull('projects.deleted_at')->groupBy('projects.id')->select('projects.id');*/
        
        $data        = [];
        $companyId   = Auth::user()->company_id;
        $projectTeam = ProjectTeam::where('user_id',$userID)->pluck('project_id');
        $dataQuery = Project::where('projects.team_leader',Auth::user()->id)
                        ->orWhereIn('projects.id',$projectTeam)
                        ->orWhere('projects.created_by', Auth::user()->id)
                        ->orWhere('projects.project_manager',Auth::user()->id)
                        ->whereNull('projects.deleted_at')
                        ->groupBy('projects.id')->select('projects.id');
        
        return $dataQuery->get()->toArray();
                    
    }
    
     public function getActiveProjects($userID) {
       /* $query = ProjectUser::select("project_users.id");
        $query->leftJoin('projects', 'projects.id', '=', 'project_users.project_id');
        $query->whereNull('projects.deleted_at');
        $query->where('project_users.user_id','=',$userId);
        $query->where('projects.project_status','=',1);
        $projects = $query->get()->toArray();
        return $projects;*/
        

        /*$dataQuery = Project::leftjoin('cr', function($leftJoin)use($companyId) {
                        $leftJoin->on('projects.id', '=', 'cr.project_id');
                        $leftJoin->whereNull('cr.deleted_at');
                    })->
                    leftjoin('clients', 'projects.client_id', '=', 'clients.id')->
                    leftjoin('users as accountmanager', 'projects.account_manager', '=', 'accountmanager.id')->
                    leftjoin('users as projectmanager', 'projects.project_manager', '=', 'projectmanager.id')->
                    leftjoin('companies as pcompany', 'projects.company_id', '=', 'pcompany.id')->
                    leftjoin('companies as acompany', 'projects.assigned_to', '=', 'acompany.id')->
                    leftjoin('project_types', 'projects.project_type', '=', 'project_types.id')->
                    where(function($q) use ($projectTeam) {
                        $q->where('projects.team_leader',Auth::user()->id)
                         ->where('projects.project_status','=',1)
                        ->orWhereIn('projects.id',$projectTeam);
                    })->
                    whereNull('projects.deleted_at')->groupBy('projects.id')->select('projects.id');*/
        
        $data        = [];
        $companyId   = Auth::user()->company_id;
        $projectTeam = ProjectTeam::where('user_id',$userID)->pluck('project_id');
        $dataQuery = Project::where('projects.project_status','=',1);
            $dataQuery->where(function($qury) use($projectTeam){
                  $qury->where(function($q){
          $q->where('projects.project_manager',Auth::user()->id);
           $q->orWhere('projects.team_leader',Auth::user()->id);
           $q->orWhere('projects.created_by', Auth::user()->id);
          });
        if(count($projectTeam) > 0 ){
           $qury->orWhereIn('projects.id',$projectTeam);
        }
    });
        $dataQuery = $dataQuery->whereNull('projects.deleted_at')
            ->groupBy('projects.id')->select('projects.id');
        $projects = $dataQuery->get()->toArray();
        return $projects;
    }
}
