<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Cr;
class ProjectTasks extends BaseModel
{
    use SoftDeletes;
    use Sortable;
    protected $table = 'project_tasks';
    public $sortable = ['task_name',
                        'task_desc',
                        'estimated_hours',
                        'users.first_name'];
    public function users() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getCr() {
        return $this->hasOne(Cr::class, 'id', 'cr_id');
    }
}
