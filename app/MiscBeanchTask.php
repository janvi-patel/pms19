<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Attendance;
use Carbon\Carbon;

class MiscBeanchTask extends Model {

    use SoftDeletes;
    use Sortable;

    protected $table = 'misc_beanch_task';

    public $sortable = ['task_title','task_start_date','task_end_date'];

    public function project() {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    

}
