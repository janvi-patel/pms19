<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class CrCostInfo extends Model
{
	// use SoftDeletes;
    use Sortable;
    protected $table = 'cr_cost_info';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cr_id', 'company_id', 'currency_id', 'cr_cost', 'hourly_rate', 'current_rate'
    ];
}
