<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
class BaseModel extends Model
{
    /**
     * Return the item List.
     * Used in
     * @param  Array  $array
     * @return array
     */
    public function getList($array = [])
    {
        $query = self::select($array['select']);
        if (isset($array['with'])){ $query->with($array['with']); }
        if (isset($array['where'])){ $query->where($array['where']); }
        if (isset($array['whereIn']['column'])) { $query->whereIn($array['whereIn']['column'], $array['whereIn']['array']); }
        if (isset($array['whereNotIn']['column'])) { $query->whereNotIn($array['whereNotIn']['column'], $array['whereNotIn']['array']); }
        if (isset($array['whereNull'])) { $query->whereNull($array['whereNull']); }
        if (isset($array['whereNotNull'])) { $query->whereNotNull($array['whereNotNull']); }
        if (isset($array['whereBetween'])) { $query->whereBetween($array['whereBetween']['column'], $array['whereBetween']['query']); }
        if (isset($array['whereGreterOrEqual'])) { $query->where($array['whereGreterOrEqual']['column'],">=" ,$array['whereGreterOrEqual']['query']); }
        if (isset($array['whereLessOrEqual'])) { $query->where($array['whereLessOrEqual']['column'],"<=" ,$array['whereLessOrEqual']['query']); }
        if (isset($array['orderBy'])){ $query->orderBy($array['orderBy']['name'], $array['orderBy']['sort']); }
        if (isset($array['pluck'])) { return $query->pluck($array['pluck'])->toArray(); }
        return $query->get()->toArray();
    }

    /**
     * Return the detail.
     *
     * @param  array  $array
     * @return array
     */
    public static function getDetails($array = [])
    {
        $query = self::select($array['select']);
        if (isset($array['with'])){ $query->with($array['with']); }
        if (isset($array['where'])){ $query->where($array['where']); }
        if (isset($array['whereIn']['column'])) { $query->whereIn($array['whereIn']['column'], $array['whereIn']['array']); }
        if (isset($array['whereNotIn']['column'])) { $query->whereNotIn($array['whereNotIn']['column'], $array['whereNotIn']['array']); }
        if (isset($array['whereNull'])) { $query->whereNull($array['whereNull']); }
        if (isset($array['whereNotNull'])) { $query->whereNotNull($array['whereNotNull']); }
        if (isset($array['whereBetween'])) { $query->whereBetween($array['whereBetween']['column'], $array['whereBetween']['query']); }
        if (isset($array['whereGreterOrEqual'])) { $query->where($array['whereGreterOrEqual']['column'],">=" ,$array['whereGreterOrEqual']['query']); }
        if (isset($array['whereLessOrEqual'])) { $query->where($array['whereLessOrEqual']['column'],"<=" ,$array['whereLessOrEqual']['query']); }
        if (isset($array['orderBy'])){ $query->orderBy($array['orderBy']['name'], $array['orderBy']['sort']); }
        if (isset($array['value'])){ return $query->value($array['value']); }
        $query = $query->first();
        if($query && !empty($query)) {
            return $query->toArray();
        }
        return [];
    }

    /**
     * Calculate the Sum of the field.
     *
     * @param  array $array
     * @return integer or decimal
     */
    public function getSum($array = [])
    {
        $query = self::select('*');
        if (isset($array['with'])){ $query->with($array['with']); }
        if (isset($array['where'])){ $query->where($array['where']); }
        if (isset($array['whereIn']['column'])) { $query->whereIn($array['whereIn']['column'], $array['whereIn']['array']); }
        if (isset($array['whereNotIn']['column'])) { $query->whereNotIn($array['whereNotIn']['column'], $array['whereNotIn']['array']); }
        if (isset($array['whereNull'])) { $query->whereNull($array['whereNull']); }
        if (isset($array['whereNotNull'])) { $query->whereNotNull($array['whereNotNull']); }
        if (isset($array['whereBetween'])) { $query->whereBetween($array['whereBetween']['column'], $array['whereBetween']['query']); }
        if (isset($array['whereGreterOrEqual'])) { $query->where($array['whereGreterOrEqual']['column'],">=" ,$array['whereGreterOrEqual']['query']); }
        if (isset($array['whereLessOrEqual'])) { $query->where($array['whereLessOrEqual']['column'],"<=" ,$array['whereLessOrEqual']['query']); }
        return $query->sum($array['column']);
    }
}
