<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Project;
use App\Client;

class Cr extends BaseModel
{
	use SoftDeletes;
    use Sortable;

    protected $table = 'cr';
    public $sortable = ['title','invoice_status'];
    protected $fillable = [
        'project_id', 'title', 'estimated_hours', 'status', 'cr_type', 'cr_start_date', 'cr_end_date', 'added_by'
    ];

    public function project() {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }
    public function company_name($query, $direction)
    {
        return $query->join('user_details', 'users.id', '=', 'user_details.user_id')
                    ->orderBy('address', $direction)
                    ->select('users.*');
    }
    public function crcostinfo(){
        return $this->hasMany(CrCostInfo::class, 'cr_id', 'id');
    }
}
