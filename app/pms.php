<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pms extends Model
{
    protected $table = 'pms';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'month', 'year', 'emp_code', 'emp_name', 'emp_comp', 'emp_desg', 'first_in','last_out','punch_data',
    ];
    // protected $casts = [
    //     'date' => 'dd-mmm-yy'
    // ];

    public function getTimeEntryByID($id){
        $pms = pms::query();
        if ($id) {
            $pms->where('id', $id);
        }
        $timeEntryDetail = $pms->first()->toArray();
        
        return $timeEntryDetail;
    }
}
