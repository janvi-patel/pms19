<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Auth;

class Company extends Model
{
    use Sortable;
    public $table = 'companies';
    
    protected $fillable = ['company_name','country_id'];
    
    public $sortable = ['company_name','short_name','emp_short_name','country.country_name','currencies.currency_name',
                        'created_at',
                        'updated_at'];
    
    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');
    }

    public function currencies()
    {
        return $this->hasOne('App\Currency','id','default_currency');
    }

    public function getCompanyName(){
        return Company::select('id', 'company_name')->get();
    }

    public function getAllCompanyName(){
        return Company::pluck('company_name')->toArray();
    }
    
    public function getCompanyViseMailData($company_id){
        return Company::select('company_name','from_address')->where('id',$company_id)->get()->toArray();
    }
    
    public function getCompanyList(){
        $company = Company::query();
        if(Auth::user()->hasRole(config('constant.superadmin_slug'))){
            $companyData = $company->orderBy('companies.id','ASC')
                                    ->distinct()
                                    ->get()
                                    ->toArray();
        }else{
            $companyId = Auth::user()->company_id;
            $companyData = $company->where('id',$companyId)
                                    ->orderBy('companies.id','ASC')
                                    ->distinct()
                                    ->get()
                                    ->toArray();
        }

        return $companyData;
    }
    public function getCompanyNameById($cmp_id) {
        return Company::select('company_name')->where('id',$cmp_id)->first();
    }
    
    public function getcompanyIdbyName($company) {
        $company = Company::select('id')->where('company_name',$company)->first();
        if($company){
            $result = $company->toArray()['id'];            
        }
        return $result;
    }

    public function getCompanyByEmpShortName() {
        return Company::select('emp_short_name')->where('id',$cmp_id)->first();
    }
    
    public function getCompanyDataById($cmp_id) {
        return Company::where('id',$cmp_id)->first();
    }

    public function getCompanyDataByName($cmp_name) {
        return Company::where('company_name',$cmp_name)->first();
    }
}
