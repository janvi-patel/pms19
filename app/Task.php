<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Attendance;
use Carbon\Carbon;
use App\ProjectEntry;

class Task extends Model {

    use SoftDeletes;
    use Sortable;

    protected $table = 'tasks';

    public $sortable = [
        'task_name',
        'task_start_date',
        'task_end_date',
        'user.first_name',
        'task_status',
        'task_hours',
        'project'.'project_name'];

    public function project() {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function taskentry() {
        return $this->hasMany(TaskEntry::class, 'task_id', 'id');
    }
    public function taskentrysum() {
        return $this->hasMany(TaskEntry::class, 'task_id', 'id');
    }

    public function getTaskList_Backup($projectID) {
        $task = Task::query()->with(['project', 'user']);

        if ($projectID) {
            $task->where('project_id', $projectID);
        }

        $taskData = $task->orderBy('id', 'ASC')->paginate(config('constant.recordPerPage'));

        return $taskData;
    }

    public function getTaskList($filter) {
        // DB::enableQueryLog();
        //$query = Task::select("tasks.*", "p.project_name", DB::raw("CONCAT(u.first_name,' ',u.last_name) as assigned_to"), DB::raw("SUM(te.log_hours) As total_logged_hours"));
        $query = Task::select("tasks.*", "p.project_name", DB::raw("CONCAT(u.first_name,' ',u.last_name) as assigned_to"),
                DB::raw("floor(sum(floor(te.`log_hours`)) + sum(te.`log_hours` - floor(te.`log_hours`)) * 100.0/60) +((sum(floor(te.`log_hours`)) + sum(te.`log_hours` - floor(te.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_logged_hours"));
        $query->join('projects as p', 'p.id', '=', 'tasks.project_id');
        $query->join('users as u', 'u.id', '=', 'tasks.user_id');
        $query->leftJoin('task_entries as te', function($join) {
            $join->on('te.task_id', '=', 'tasks.id');
            $join->where('te.deleted_at', null); 
        });

        if (isset($filter['project_id']) && $filter['project_id']) {
            $query->where('project_id', $filter['project_id']);
        }
        if (isset($filter['user_ids']) && !empty($filter['user_ids'])) {
            $query->whereIn("user_id", $filter['user_ids']);
        }

        $taskData = $query->orderBy('id', 'ASC')->groupBy('tasks.id')->get()->toArray();
        // print_r(DB::getQueryLog());
        // die;
        return $taskData;
    }

    public function getTaskDetails($id) {
        $task = Task::query()->with('user');
        if ($id) {
            $task->where('id', $id);
        }
        $taskDetail = $task->first()->toArray();
        // echo '<pre>';
        // print_r($usersData);
        // die;
        return $taskDetail;
    }

    /**
     * get user task by $filter and also requested filter in $r
     * @param array $filter user ids and project id
     * @param request $r request
     * @return array
     */
    public function getUserTasks($filter, $r = array()) {
        $tasks = array();
        if (isset($filter['user_ids']) && !empty($filter['user_ids'])) {
            //$query = Task::select("tasks.*", "p.project_name", DB::raw("CONCAT(u.first_name,' ',u.last_name) as assigned_to"), DB::raw("SUM(te.log_hours) As total_logged_hours"));
            $query = Task::select("tasks.*", "p.project_name", DB::raw("CONCAT(u.first_name,' ',u.last_name) as assigned_to"), 
                    DB::raw("floor(sum(floor(te.`log_hours`)) + sum(te.`log_hours` - floor(te.`log_hours`)) * 100.0/60) +((sum(floor(te.`log_hours`)) + sum(te.`log_hours` - floor(te.`log_hours`)) * 100.0/60) % 1) * 60.0/100  as total_logged_hours"));
            $query->join('projects as p', 'p.id', '=', 'tasks.project_id');
            $query->join('users as u', 'u.id', '=', 'tasks.user_id');
            $query->leftJoin('task_entries as te', function($join) {
                $join->on('te.task_id', '=', 'tasks.id');
                $join->where('te.deleted_at', null); 
            });
            $query->whereIn("tasks.user_id", $filter['user_ids']);

            if (isset($filter['project_id']) && $filter['project_id']) {
                $query->where("tasks.project_id", $filter['project_id']);
            }

            // my team task filter
            if (isset($r) && !empty($r) && $r->isMethod('POST') && $r->reqType == 'teamTask') {
                if ($r->has('txtTaskName') && !empty($r->txtTaskName)) {
                    $query = $query->where('tasks.task_name', 'like', '%' . $r->txtTaskName . '%');
                }
                if ($r->has('ddlProject') && !empty($r->ddlProject)) {
                    $query = $query->where('p.id', $r->ddlProject);
                }
                if ($r->has('ddlUser') && !empty($r->ddlUser)) {
                    $query = $query->where('tasks.user_id', $r->ddlUser);
                }
                if ($r->has('ddlTaskStatus') && !empty($r->ddlTaskStatus)) {
                    $query = $query->where('tasks.task_status', $r->ddlTaskStatus);
                }
            }
            
            // my task filter
            if(isset($r) && !empty($r) && $r->isMethod('POST') && $r->reqType == 'myTask'){
                if ($r->has('ddlProject') && !empty($r->ddlProject)) {
                    $query = $query->where('p.id', $r->ddlProject);
                }
                if($r->has('taskStartDate') && !empty($r->taskStartDate)){
                    $query = $query->whereDate('tasks.task_start_date','=',$r->taskStartDate);
                }
                if($r->has('ddlTaskStatus') && !empty($r->ddlTaskStatus)){
                    $query = $query->where('tasks.task_status',$r->ddlTaskStatus);
                }
            }
            //dd($query->toSql());
            $tasks = $query->groupBy('tasks.id')->orderBy('tasks.task_start_date', 'desc')->get()->toArray();
        }

        return $tasks;
    }

    public function delete() {
        // delete all related photos 
        $this->taskentry()->delete();
        // as suggested by Dirk in comment,
        // it's an uglier alternative, but faster
        // Photo::where("user_id", $this->id)->delete()
        // delete the user
        return parent::delete();
    }
    
    public function userLoggedHourByDay($user_id,$date) {
              
        /*$query = Task::select(DB::raw("floor(sum(floor(te.log_hours)) + sum(te.log_hours - floor(te.log_hours)) * 100.0/60) +
                        ((sum(floor(te.log_hours)) + sum(te.log_hours - floor(te.log_hours)) * 100.0/60) % 1) * 60.0/100  as total_logedHours")
                        ,'te.log_date');
        $query->join('users as u', 'u.id', '=', 'tasks.user_id');
        $query->leftJoin('task_entries as te', function($join) {
            $join->on('te.task_id', '=', 'tasks.id');
            $join->where('te.deleted_at', null); 
        });
        $query->where('te.log_date',$date);
        $query->where('u.id',$user_id);
        $query->where('u.deleted_at',null);
        $taskData = $query->get()->toArray();*/
        
        
       $query = ProjectEntry::select(DB::raw("sum(project_entries.`log_hours`) as total_logedHours"),'log_date')
                    ->join('users','users.id','user_id')
                    ->where('log_date',$date)
                    ->where('users.id',$user_id)
                    ->groupBy('project_entries.user_id');
       $taskData = $query->get()->toArray();
        return $taskData;
    }

}
