<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class ProjectLead extends Model
{
    use SoftDeletes;
    use Sortable;
    protected $table = "project_lead";

    public function checkedLeadsUpdate($leadIdArray,$action) {
        
        echo $leads = ProjectLead::query()->whereIn('id',$leadIdArray)->update(['estimation_status' => $action]);
           
       
        return $leads;
    }
}


 
