<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $table = "email_template";

    public function emailTemplate($comp_id,$module)
    {
        $emailTemplate = new EmailTemplate();
        $emailTemp  = $emailTemplate->where('company_id',$comp_id)->where('module',$module)->first();
        if($emailTemp == null)
        {
            return null;
        }
        else{
            return $emailTemp;
        }
    }

    public function sendToCC($comp_id,$cc,$reporting_to = "")
    {
        $sendCC = [];
        $userObj = new User();
        $reporting_email = $userObj->find($reporting_to);
        $cc_id = config('constant.send_mail_to_cc');
        $arr = str_split($cc);
        $result = array_diff(array_keys($cc_id),$arr);
        $hr_admin_email = $userObj->getUsersHrAndAdminEmailWithCompany($comp_id);
        
        if(!in_array("1", $result)){
            $sendCC[1] = $hr_admin_email['hr_email_address'];
        }
        if(!in_array("2", $result)){
            $sendCC[2] = $hr_admin_email['email'];
        }
        if(!in_array("3", $result)){
            if($reporting_email != null){
                $sendCC[3] = $reporting_email->email;
            }
        }
        return $sendCC;
    }

    public function replaceKeyword($temp = null,$Keyword = [])
    {
        if(count($Keyword)>0)
        {
            foreach($Keyword as $key => $value)
            {
                $temp = str_replace('['.$key.']',$value,$temp);
            }
        }
        return $temp;
    }
}
