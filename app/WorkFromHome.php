<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use App\Holiday;

class WorkFromHome extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'work_from_home';

    public $sortable = ['user.first_name','start_date','end_date','return_date',
                        'days','reason','user.first_name','status'];


    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function approver(){
        return $this->hasOne(User::class, 'id', 'approver_id');
    }
    public function getLeaveDetails($leaveID){
        $leave = WorkFromHome::query()->with(['user', 'approver']);

        if ($leaveID) {
            $leave->where('id', $leaveID);
        }
        $leaveDetail = $leave->first();
        if($leaveDetail){
            $leaveDetail = $leaveDetail->toArray();
        }

        return $leaveDetail;
    }
    
}
