<?php

namespace App;
use App\Holiday;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\AttendanceDetail;
use App\User;
use App\Task;
use Kyslik\ColumnSortable\Sortable;


class Attendance extends Model
{
    protected $table = 'attendances';
    use Sortable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entry_date', 'emp_code', 'emp_name', 'emp_comp', 'emp_desg', 'first_in','last_out','user_id'
    ];
    // protected $casts = [
    //     'date' => 'dd-mmm-yy'
    // ];
    public $sortable = ['emp_comp','emp_name','emp_code','users.first_name'];
    public function attendance_details() {
        return $this->hasOne(attendance_details::class, 'id', 'attendance_id');
    }
    
    public function getTimeEntryByID($id){
        $attendance = Attendance::query();
        if ($id) {
            $attendance->where('id', $id);
        }
        $timeEntryDetail = $attendance->first()->toArray();
        
        return $timeEntryDetail;
    }
    
    public function monthlyTotalWorksDay($cur_month,$cur_year) {
        $remaningEntry = 0;
        $holidayDate = array();
    
        $holiday = new Holiday();

        $totalHoliday = $holiday->getMonthlyHoliday($cur_month,$cur_year);
        if($holidayDate){
            foreach($totalHoliday As $totalHoliday){
                $holidayDate[] = date("d", strtotime($totalHoliday['holiday_date']));
            }
        }
       
        $workDate = array();
        $type = CAL_GREGORIAN;
        $month = date($cur_month); 
        $year = date($cur_year);
        // $day_count = cal_days_in_month($type, $month, $year); // Get the amount of days
        $day_count = date('t', mktime(0, 0, 0, $month, 1, $year));

        //loop through all days
        for ($i = 1; $i <= $day_count; $i++) {
                $date = $year.'/'.$month.'/'.$i; //format date
                $isWeekend = $holiday->getWeekendDateList($date);
                // $get_name = date('l', strtotime($date)); //get week day
                // $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
                // if($day_name != 'Sun' && $day_name != 'Sat'){
                if(count($isWeekend) == 0){
                    $workDate[] = str_pad($i, 2, '0', STR_PAD_LEFT); 
                }
        }
        $empWorkDays = array_merge(array_diff($workDate,$holidayDate));
        return $empWorkDays;
    }
    
    public function getRemainingTimeEntries($cur_month,$cur_year,$day,$company_id) {
        
        $remaningEntry = 0;
        $empWorkDays = $this->monthlyTotalWorksDay($cur_month,$cur_year);
        $attendance = Attendance::select('entry_date');
        $attendance->leftJoin('users as u','u.employee_id','attendances.emp_code');
        if(isset($company_id) && $company_id){
            $attendance->where('u.company_id','=', $company_id);
        }

        $result = $attendance->orderBy('entry_date', 'desc')->first();
        if($result){
            $result = $result->toArray()['entry_date'];
        }
        $lastDay = date('d',strtotime($result));
        foreach($empWorkDays AS $empWorkDay){
            if($empWorkDay > $lastDay && $day >= $empWorkDay){
                $remaningEntry ++;
            }
        }
        return $remaningEntry;
    }
    
    public function workingHourbyUser($user_id,$date,$companyName) {
        $query = AttendanceDetail::select('attendances.entry_date',DB::raw('TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))), "%h:%i:%s") AS working_hours'));
        $query->leftJoin('attendances as attendances', 'attendance_details.attendance_id', '=', 'attendances.id');
        $query->leftJoin('users as users', 'users.employee_id', '=', 'attendances.emp_code');
        $query->where('attendances.entry_date', '=', $date);
        $query->where('attendances.emp_comp', '=', $companyName);
        $query->where('users.id', '=', $user_id);
        $query->groupBy('attendance_details.attendance_id');
        $userTimeEnteryData = $query->get()->toArray();
        return $userTimeEnteryData;
   
    }
    public function userAttendancesData($companyName,$user_code,$cur_month,$cur_year) {


        $query = Attendance::select(DB::raw('SUM(late_comer) as late_comer') ,DB::raw('SUM(full_day) as full_Day'), DB::raw('SUM(half_day) as half_Day'),DB::raw('SUM(early_going) as early_going'));
        $query->whereMonth('entry_date', '=', date($cur_month));
        $query->whereYear('entry_date', '=', date($cur_year));
        $query->where('emp_code', '=', $user_code);
        $query->where('emp_comp', 'LIKE', $companyName);
        $query->groupBy('emp_code');
        $attendanceData = $query->get()->toArray();
        return $attendanceData;
    }
    
    public function userTimeEntryDetail($companyName,$user_code,$cur_month,$cur_year) {
        $query = AttendanceDetail::select('attendance_details.attendance_id',DB::raw('TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))), "%h:%i:%s") AS working_hours'),
                 DB::raw('TIMEDIFF( attendances.last_out, attendances.first_in) AS total_hours'),
                 DB::raw('SUBTIME(TIMEDIFF( attendances.last_out, attendances.first_in)'),
                 DB::raw('TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))), "%h:%i:%s")) as break_hours'));
        $query->leftJoin('attendances as attendances', 'attendance_details.attendance_id', '=', 'attendances.id');
        $query->groupBy('attendance_details.attendance_id');
        $query->whereMonth('attendances.entry_date', '=', date($cur_month));
        $query->whereYear('attendances.entry_date', '=', date($cur_year));
        $query->where('attendances.emp_code', '=', $user_code);
        $query->where('attendances.emp_comp', 'LIKE', $companyName);
        $userTimeEnteryData = $query->get()->toArray();
        return $userTimeEnteryData;
    }

    public static function getLastEntryHr (){
        $attendance = Attendance::query()->orderBy('entry_date', 'desc')->first();
        $entryDate = strtotime($attendance['entry_date']);
        $now = strtotime("now");
        ($now - $entryDate > 259200)?true:flase;
    }    
    
    public function workingHourforMail($emp_code,$companyName,$month,$year,$date) {
        
        $queryAttend = AttendanceDetail::selectRaw('attendances.emp_code, TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))), "%h:%i:%s") AS working_hours')
                        ->rightJoin('attendances', 'attendances.id', '=', 'attendance_details.attendance_id')
                        ->where('attendances.absent', NULL)
                        ->where('first_in','!=','00:00:00')
                        ->where('last_out','!=','00:00:00')
                        ->where('attendances.incorrect_entry', NULL)
                        ->where('attendances.emp_code', $emp_code)
                        ->where('attendances.emp_comp',$companyName )
                        ->whereMonth('attendances.entry_date', $month)
                        ->whereYear('attendances.entry_date', $year)
                        ->groupBy('attendance_details.attendance_id');

        $working_details = $queryAttend->get()->toArray();
            
    
        return $working_details;
    }
    public function weeklyWorkingHourforMail($emp_code,$companyName,$lastWeekStartDate,$lastWeekEndDate) {
        
        $queryAttend = AttendanceDetail::selectRaw('attendances.emp_code, TIME_FORMAT(SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))), "%h:%i:%s") AS working_hours')
                        ->rightJoin('attendances', 'attendances.id', '=', 'attendance_details.attendance_id')
                        ->where('attendances.absent', NULL)
                        ->where('first_in','!=','00:00:00')
                        ->where('last_out','!=','00:00:00')
                        ->where('attendances.incorrect_entry', NULL)
                        ->where('attendances.emp_code', $emp_code)
                        ->where('attendances.emp_comp',$companyName )
                        ->whereBetween('entry_date', [$lastWeekStartDate, $lastWeekEndDate])
                        ->groupBy('attendance_details.attendance_id');

        $working_details = $queryAttend->get()->toArray();
            
    
        return $working_details;
    }

    public function lateComerUsersObj($request, $company_name, $employee_id, $teamMemberIds ,$month ,  $year ,$company_id){
        $query = Attendance::select(DB::raw('SUM(late_comer) as late_comer') ,DB::raw('SUM(full_day) as full_Day'), DB::raw('SUM(half_day) as half_Day'),DB::raw('SUM(early_going) as early_going'),'emp_name','emp_comp','emp_code');
        $query->leftJoin('users As u','u.employee_id','=','emp_code');
        $query->where('u.status',1);
        if($company_id!=''){
           $query->where('u.company_id',$company_id);
        }
        if($teamMemberIds!=''){
            $query->whereIn('emp_code',$teamMemberIds);
        }
        if($month!=''){
            $query->whereMonth('entry_date',date($month));
        }
        if($year!=''){
            $query->whereYear('entry_date',date($year));
        }
        if ($employee_id != '') {
            $query->where('emp_code',  '=', $employee_id);
        }        
        $query->havingRaw('SUM(late_comer) >'.config('constant.late_come_limit'));
        $lateComeData=$query->groupBy('emp_code','emp_comp')->sortable();

        return $lateComeData; 
    } 
}

