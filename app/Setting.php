<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;


class Setting extends BaseModel
{
    protected $table = 'settings';

    public function getSettingData($module) {
        $setting = Setting::query();
        if($module != ''){
            $setting->where('module','=',$module);
        }
        $setting->where('company_id',Auth::user()->company_id);
        return $setting->get()->toArray();
    }

    public function getSettingDetails($array = []) {
        $setting = Setting::query();
        if(isset($array['select'])){
            $setting->select($array['select']);
        }
        if(isset($array['where'])){
            $setting->where($array['where']);
        }
        $setting->where('company_id',Auth::user()->company_id);
        return $setting->first();
    }

    public function getSettingTaskOperationData() {
        $settingDays = 3;
        $setting = Setting::query();
        $setting->where('module','=','task_operation');
        $setting->where('company_id',Auth::user()->company_id);
        $days = $setting->get()->pluck('days');
        if(isset($days[0]) && $days[0] != '')
        {
            $settingDays = $days[0];
        }
        return $settingDays+1;
    }

    public function getSettingAvgWorkingTime($module,$company_id) {
        $setting = new Setting();
        $average_working_time = $setting->where('module','=',$module)
        ->where('company_id',$company_id)->first();
        if($average_working_time != null && $average_working_time->constant_str_value != null){
            return $average_working_time->constant_str_value;
        }
        return null;
    }

    public function getSettingAvgLoggedTime($module,$company_id) {
        $setting = new Setting();
        $average_logged_time = $setting->where('module','=',$module)
        ->where('company_id',$company_id)->first();
        if($average_logged_time != null && $average_logged_time->constant_time != null){
            return $average_logged_time->constant_time;
        }
        return null;
    }
    public function getSettingEntryTime($module,$company_id) {
        $setting = new Setting();
        $full_day_time = $setting->where('module','=',$module)
        ->where('company_id',$company_id)->first();
        if($full_day_time != null && $full_day_time->constant_time != null){
            return $full_day_time->constant_time;
        }
        return null;
    }

}
