$('#today_leave_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5, order: false});
$('#upcoming_leave_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5, order: false});
$('#holidays').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,ordering: false});
$.fn.DataTable.ext.pager.numbers_length = 5;
$('#on_probation_listing, #completed_probation_listing, #resign_user_listing, #upcoming_joining_user_listing,#today_leave_without_pay_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,ordering: false});
$('#emp_on_training_on_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,ordering: false});
$('#team_task_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,ordering: false});
$('#emp_birthday_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,ordering: false});
$('#emp_joining_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,ordering: false});
$('#open_project_list').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,order: [[2, "desc"]]});
$('#search_by_user_name').select2();
$('#lead_end_date_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,ordering: false});
$('#lead_pending_date_listing').DataTable({searching: false, paging: true, info: true, lengthChange: false, pageLength: 5,ordering: false});
$("#search_by_start_date").datepicker({
    todayBtn: 1,
    autoclose: true,
    format: 'yyyy-mm-dd',
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#search_by_end_date').datepicker('setStartDate', minDate);
});

$("#search_by_end_date").datepicker({
    autoclose: true,
    todayBtn: 1,
    format: 'yyyy-mm-dd',
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#search_by_start_date').datepicker('setEndDate', maxDate);
});

$("#reset_startdate").click(function () {
    $('#search_by_start_date').val('');
});

$("#reset_enddate").click(function () {
    $('#search_by_end_date').val('');
});

$("#search_redirect").click(function () {
    var url = getsiteurl() + "/reports/projects";
    $('#search_filter').attr('action', url).submit();
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').popover({html:true});
    $('.information').popover("show"); 
});
$(function () {
    var login_role_name = $('#login_role_name').val();
    var login_user_role_type = $('#login_user_role_type').val();
    
    $('#team_member').select2();
    var user_id = $("#team_member option:selected" ).val();
    if(user_id != undefined){
        setChart(user_id);
    }
    
    
    $('#team_member').change(function(){
        var user_id = $("#team_member option:selected" ).val();
        setChart(user_id);
    });
    
    function setChart(user_id){
        
        $.ajax({ 
            url: getsiteurl() + "/dashboard/empTotalWorkDays",
            data: {user_id:user_id},
            dataType: 'json',
            success: function(data){
                if(login_user_role_type == 'team_leader'){
                    area.setData(data);
                }
            }
        });
        
        var area = new Morris.Area({
                element   : 'revenue-chart',
                data: [ ],
                xkey: 'date',
                ykeys: ['working_hours','logged_hours'], 
                labels: ['Working Hours', 'logged Hours'],
                fillOpacity: 0.6,
                hideHover: 'auto',
                parseTime: false,
                behaveLikeLine: true,
                resize: true,
                pointFillColors:['#ffffff'],
                pointStrokeColors: ['black'],
                lineColors: ['#495057', '#007cff'],
            });
        $('.box ul.nav a').on('shown.bs.tab', function () {
            area.redraw()
        })
    }
    
});