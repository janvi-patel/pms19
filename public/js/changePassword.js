/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $( document ).ready(function() {
        $('#showPswd').click(function(){
            var x = document.getElementById("new_password");
            var y = document.getElementById("confirm_password");
            if (x.type === "password") {
              x.type = "text";
              y.type = "text";
            } else {
              x.type = "password";
              y.type = "password";
            } 
        });
        $('#generatePswdButton').click(function(){
              var hash = randomPassword(8);
            $('#new_password').val(hash);
            $('#confirm_password').val(hash);
        });

    });
        $("#user_create").validate({
        rules: {
                password: "required",
                new_password: "required",
                confirm_password: "required"
                
        },
        messages:{
                password: "Please enter password",
                confirm_password: "Please enter new password.",
                confirm_password: "Please enter confirm password."
        },
        submitHandler: function (form) {
            form.submit();
        }


          });

    
    function randomPassword(length) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }
