$(document).ready(function(){
    $('#validatedCustomFile').change(function(e){
        var fileName = e.target.files[0].name;
        $('#uploadedFileName').text(fileName);
    });
    
    $('#validatedManuallySheet').change(function(e){
        var fileName = e.target.files[0].name;
        $('#uploadedManuallySheetFileName').text(fileName);
    });

    $("#frmUploadTimeEntry").validate({
        rules: {
		    xlsfile: {required:true,extension:"csv"},
		    timeEntryDate: {required: true}
        },
        messages: {
            xlsfile: {
            	required: "Please select file",
            	extension:"Please select .csv extension file to upload"
            },
            timeEntryDate: {
            	required: "The time entry date field is required.",
            }
        },
        submitHandler: function (form){
            form.submit();
        }
    });
    $("#entryDate").datepicker({
        todayBtn: 1,
        autoclose: true,
        format: 'dd-mm-yyyy',
    });

    $('#deleteTimeEntry').click(function(){
        var companyId = $('#company').val();
        var entryDate = $('#entryDate').val();
        if(companyId != '' && entryDate != ''){
                swal({
                    title: "Are you sure you want to delete "+entryDate+" entry",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete) {
                if (willDelete) {
                  deleteTimeEntry(companyId,entryDate);
                }
             });
        }
    });	
    $('#uploadTimeEntryUsingCron').click(function(){
        var entryDate = $('#timeEntryDateForCron').val();
       // alert(entryDate);
        if(entryDate != ''){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            jQuery.ajax({
                url  : getsiteurl() + '/upload/time_entries_using_cron',
                type : "POST",
                data: {_token: CSRF_TOKEN,entryDate:entryDate},
                beforeSend: function(){
                    $('.spinner-border').removeClass('d-none');
                },
                success: function(response) {
                    if(response.status == 'success'){
                        swal(response.message,"", "success");
                    }else if(response.status == 'error'){
                        swal(response.message,"", "error");
                        console.log(response.message);
                    }
                  location.reload();
                },
                complete:function() {
                    $('.spinner-border').addClass('d-none');
                }
            });
        }
    });
    
});
function deleteTimeEntry(companyId,entryDate){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
            url  : getsiteurl() + '/delete/time_entries',
            type : "DELETE",
            data: {_token: CSRF_TOKEN,companyId:companyId,entryDate:entryDate},
            success: function(response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
                 location.reload();
        }
    });
}