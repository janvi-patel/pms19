$(document).ready(function () {
    $('.select2').select2();

    $("#search_task_start_date,#search_task_end_date").datepicker({autoclose: true, format: 'dd-mm-yyyy'});

    $(document).on("click", ".deleteProjectTask", function(){
        var projectTaskId = $(this).attr('data-task-id');

        if(projectTaskId != ''){
            swal({
                title: "Are you sure you want to delete project task ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  deleteProjectTask(projectTaskId);
                }
              });
        }
    });
    function deleteProjectTask(projectTaskId){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            jQuery.ajax({
            url  : getsiteurl() + '/project_task/delete',
            type : "DELETE",
            data: {_token: CSRF_TOKEN,projectTaskId:projectTaskId},
            success: function(response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                    window.location.reload();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                    if(response.code == 'userNotValid')
                    {
                        location.href = getsiteurl();
                    }
                }
            }
        });
    }
});
function  getProjectTaskList(projectId) {
    jQuery.ajax({
        url: getsiteurl() + '/get/project/task_list/' + projectId + '/' + $('#taskDate').val(),
        type: 'GET',
        success: function (data) {
            var html = '';
            $('#task_list').html('');
            html += '<option value="0">Select Task</option>';


            if (Object.keys(data).length > 0) {
                jQuery.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.task_name + '</option>';

                });
                $('#task_list').append(html);

                $('#task_list').change(function () {
                    if ($('#task_list').val() != 0) {
                        $('#task_list-error').text('');
                    }
                });
            }

        }
    });
}

function  newGetProjectTaskList(projectId,projectTaskId) {
    jQuery.ajax({
        url: getsiteurl() + '/get/project/new_task_list/' + projectId +'/' + projectTaskId + '/' + $('#taskDate').val(),
        type: 'GET',
        success: function (data) {
            var html = '';
            $('#task_list').html('');
            html += '<option value="0">Select Task</option>';


            if (Object.keys(data).length > 0) {
                jQuery.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.task_name + '</option>';

                });
                $('#task_list').append(html);

                $('#task_list').change(function () {
                    if ($('#task_list').val() != 0) {
                        $('#task_list-error').text('');
                    }
                });
            }

        }
    });
}