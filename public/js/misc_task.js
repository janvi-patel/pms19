$(document).ready(function () {
    $('.select2').select2();
    $(document).on("click", ".addProjectTaskBtn", function () {
        var projectId = $(this).attr('data-project-id');
        if (projectId != '') {
            jQuery.ajax({
                url: getsiteurl() + '/add/addProjectTaskModal',
                type: "GET",
                data: {projectId: projectId},
                success: function (response) {
                    $('#addProjectTaskModal').empty();
                    $('#addProjectTaskModal').append(response);
                    manageAddTask();
                    $('#addProjectTaskModal').modal('show');
                }
            });
        }
    });
    function manageAddTask() {
        $("#addProjectTask").validate({
            rules: {
                taskName: "required",
                addWorkTimeHours: {required: true, digits: true},
                addWorkTimeMinutes: "required",
                task_start_date: 'required',
                task_end_date: 'required', 
            },
            messages: {
                taskName: "Please enter task name",
                addWorkTimeHours: "Please enter hours",
                addWorkTimeMinutes: "Please select minutes",
                task_start_date: "Please select start date",
                task_end_date: "Please select end date", 
            },
            submitHandler: function (form) {
                var workTimeHours = $('#addWorkTimeHours').val();
                var workTimeMinutes = $('#addWorkTimeMinutes').val();
                if (workTimeHours == 0 && workTimeMinutes == 0) {
                    $('#addWorkTimeHours-error').css({'display': 'block'});
                    $('#addWorkTimeMinutes-error').css({'display': 'block'});

                    $('#addWorkTimeHours-error').text('Please select hours');
                    $('#addWorkTimeMinutes-error').text('Please select minutes');
                } else {
                    form.submit();
                }
            }
        });
    }
    $(document).on("click", ".editProjectTask", function(){
        var projectTaskId = $(this).attr('data-task-id');
        if(projectTaskId != ''){
                jQuery.ajax({
                url  : getsiteurl() + '/edit/editProjectTaskModal',
                type : "GET",
                data: {projectTaskId:projectTaskId},
                success: function(response) {
                        $('#editProjectTaskModal').empty();
                        $('#editProjectTaskModal').append(response);
                        manageEditTask();
                        $('#editProjectTaskModal').modal('show');
                }
            });
        }
    });

    function manageEditTask(){
        $('.select2').select2();
               
        $("#updateProjectTask").validate({
            rules: {
                    taskName: "required",
                    editWorkTimeHours: "required",
                    editWorkTimeMinutes: "required",
                    assigned_user_list: "required",
            },
            messages: {
                taskName: "Please enter task name",
                editWorkTimeHours: "Please select hours",
                editWorkTimeMinutes: "Please select minutes",
                assigned_user_list: "Please select user",
            },
            submitHandler: function (form) {
                var workTimeHours   = $('#editWorkTimeHours').val();
                var workTimeMinutes = $('#editWorkTimeMinutes').val();
                if(workTimeHours == 0 && workTimeMinutes == 0){
                        $('#editWorkTimeHours-error').css({'display':'block'});
                        $('#editWorkTimeMinutes-error').css({'display':'block'});

                        $('#editWorkTimeHours-error').text('Please select hours');
                        $('#editWorkTimeMinutes-error').text('Please select minutes');
                }else{
                    form.submit();
                }
            }
        });
    }
});
