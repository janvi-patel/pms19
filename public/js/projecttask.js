$(document).ready(function() {
    $('#search_user').select2();
    $('#project_type').select2();
    $('#search_by_task_name').select2();
    $("#search_by_start_date").datepicker({
        todayBtn: 1,
        autoclose: true,
        format: 'dd-mm-yyyy',
    }).on('changeDate', function(selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#search_by_end_date').datepicker('setStartDate', minDate);
    });

    $("#search_by_end_date").datepicker({
        autoclose: true,
        todayBtn: 1,
        format: 'dd-mm-yyyy',
    }).on('changeDate', function(selected) {
        var maxDate = new Date(selected.date.valueOf());
        $('#search_by_start_date').datepicker('setEndDate', maxDate);
    });

    $("#reset_startdate").click(function() {
        $('#search_by_start_date').val('');
    });

    $("#reset_enddate").click(function() {
        $('#search_by_end_date').val('');
    });

    $(document).on("click", ".deleteProjectTask", function() {
        var projectTaskId = $(this).attr('data-task-id');

        if (projectTaskId != '') {
            swal({
                title: "Are you sure you want to delete project task ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteProjectTask(projectTaskId);
                }
            });
        }
    });

    function deleteProjectTask(projectTaskId) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
            url: getsiteurl() + '/project_task/delete',
            type: "DELETE",
            data: { _token: CSRF_TOKEN, projectTaskId: projectTaskId },
            success: function(response) {
                if (response.status == 'success') {
                    swal(response.message, "", "success");
                    window.location.reload();
                } else if (response.status == 'error') {
                    swal(response.message, "", "error");
                    if (response.code == 'userNotValid') {
                        location.href = getsiteurl();
                    }
                }
            }
        });
    }
    $(document).on("click", ".addProjectTaskBtn", function() {
        var projectId = $(this).attr('data-project-id');
        if (projectId != '') {
            jQuery.ajax({
                url: getsiteurl() + '/add/addProjectTaskModal',
                type: "GET",
                data: { projectId: projectId },
                success: function(response) {
                    //console.log(response);
                    $('#addProjectTaskModal').empty();
                    $('#addProjectTaskModal').append(response);
                    manageAddTask();
                    $('#addProjectTaskModal').modal('show');
                }
            });
        }
    });

    function manageAddTask() {
        $("#addProjectTask").validate({
            rules: {
                taskName: "required",
                addWorkTimeHours: { required: true, digits: true },
                addWorkTimeMinutes: "required",
                task_start_date: 'required',
                task_end_date: 'required',
                'taskAssign[]': "required",
            },
            messages: {
                taskName: "Please enter task name",
                addWorkTimeHours: "Please enter hours",
                addWorkTimeMinutes: "Please select minutes",
                task_start_date: "Please select start date",
                task_end_date: "Please select end date",
                'taskAssign[]': "Please select user",
            },
            submitHandler: function(form) {
                var workTimeHours = $('#addWorkTimeHours').val();
                var workTimeMinutes = $('#addWorkTimeMinutes').val();
                if (workTimeHours == 0 && workTimeMinutes == 0) {
                    $('#addWorkTimeHours-error').css({ 'display': 'block' });
                    $('#addWorkTimeMinutes-error').css({ 'display': 'block' });

                    $('#addWorkTimeHours-error').text('Please select hours');
                    $('#addWorkTimeMinutes-error').text('Please select minutes');
                } else {
                    form.submit();
                }
            }
        });
    }
    $(document).on("click", ".editProjectTask", function() {
        var projectTaskId = $(this).attr('data-task-id');
        if (projectTaskId != '') {
            jQuery.ajax({
                url: getsiteurl() + '/edit/editProjectTaskModal',
                type: "GET",
                data: { projectTaskId: projectTaskId },
                success: function(response) {
                    $('#editProjectTaskModal').empty();
                    $('#editProjectTaskModal').append(response);
                    manageEditTask();
                    $('#editProjectTaskModal').modal('show');
                }
            });
        }
    });

    function manageEditTask() {
        $('.select2').select2();

        $("#updateProjectTask").validate({
            rules: {
                taskName: "required",
                editWorkTimeHours: "required",
                editWorkTimeMinutes: "required",
                assigned_user_list: "required",
            },
            messages: {
                taskName: "Please enter task name",
                editWorkTimeHours: "Please select hours",
                editWorkTimeMinutes: "Please select minutes",
                assigned_user_list: "Please select user",
            },
            submitHandler: function(form) {
                var workTimeHours = $('#editWorkTimeHours').val();
                var workTimeMinutes = $('#editWorkTimeMinutes').val();
                if (workTimeHours == 0 && workTimeMinutes == 0) {
                    $('#editWorkTimeHours-error').css({ 'display': 'block' });
                    $('#editWorkTimeMinutes-error').css({ 'display': 'block' });

                    $('#editWorkTimeHours-error').text('Please select hours');
                    $('#editWorkTimeMinutes-error').text('Please select minutes');
                } else {
                    form.submit();
                }
            }
        });
    }
    $('#page_range_dropdown').change(function() {
        var limit = $(this).val();
        var link = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if (limit != '' && query_string.indexOf('search_submit') != -1) {
            if (query_string.indexOf('page_range') == 1) {
                var page_range = query_string.slice(0, query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link + page_range + '=' + limit + srch_str;
            } else {
                var redirectLink = link + '?page_range=' + limit + '&' + search_str;
            }
        } else {
            var redirectLink = link + '?page_range=' + limit;
        }
        window.location = redirectLink;

    });

    $('#checkAll').click(function() {
        $('input:checkbox').prop('checked', this.checked);
    });
    $(".update_task_row").change(function() {
        if (!$(this).prop("checked")) {
            $("#move_task").prop("checked", false);
        }
    });


    $('#move_checked_task').change(function() {
        var taskIdArray = [];
        $.each($(".update_task_row:checked"), function() {
            taskIdArray.push($(this).val());
        });
        var action = $(this).val();
        if (action != '' && taskIdArray.length != 0) {
            if (action == 1) {
                swal({
                    title: "Are you sure you want to move selected task?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete) {
                    if (willDelete) {
                        jQuery.ajax({
                            url: getsiteurl() + '/move/moveProjectTask',
                            type: "GET",
                            data: { taskIdArray: taskIdArray },
                            success: function(response) {
                                $('#moveTaskModal').html(response);
                                $('#moveTaskModal').modal('show');
                            }
                        });
                    }
                });
            }
        } else {
            if (taskIdArray.length == 0) {
                swal("Please select task.", "", "error");
            }
        }
        if (action == 1) {
            $('#move_checked_task').val('');
        }
    });
    $('.update_task_row').click(function() {
        if ($('.update_task_row:checked').length > 0) {
            $('#move_task').css('display', 'block');
        } else {
            $('#move_task').css('display', 'none');
        }
    })
    if (!$(this).prop("checked")) {
        $("#checkAll").prop("checked", false);
    }

    // $(document).on("click", ".move_task_entry_btn", function () {
    $(document).on('submit', '#moveTaskForm', function(evt) {
        evt.preventDefault();
        var taskIdArray = [];
        $.each($(".update_task_row:checked"), function() {
            taskIdArray.push($(this).val());
        });
        var move_to_project_id  = $('#move_to_project_id option:selected').val();
        var project_cr_list     = $('#project_cr_list option:selected').val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
            url: getsiteurl() + '/move/task/entry',
            type: "POST",
            data: { _token: CSRF_TOKEN, move_to_project_id: move_to_project_id,project_cr_list:project_cr_list, taskIdArray: taskIdArray },
            success: function(response) {
                if (response.status == 'success') {
                    swal(response.message, "", "success");
                    location.reload();
                } else if (response.status == 'error') {
                    swal(response.message, "", "error");
                    if (response.code == 'userNotValid') {
                        location.href = getsiteurl();
                    }
                }
            }
        });
    });
});
