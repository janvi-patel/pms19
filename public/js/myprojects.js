$(document).ready(function () {
	$('.select2').select2();

    $('#checkAll').click(function () {
        $('input:checkbox').prop('checked', this.checked);
    });

    if (!$(this).prop("checked")) {
            $("#checkAll").prop("checked", false);
    }
	$('#company').select2(); 
	$('.select2').select2()

	// $('#page_range_dropdown').change(function(){
	//     var limit = $(this).val();
    //         $('#page_range').val(limit);
    //         $('#search_filter').submit();
    //      });


	$('#page_range_dropdown').change(function(){
	    var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;
    
    });
	
	$("#search_by_created_date").datepicker({
	    todayBtn: 1,
	    autoclose: true,
	    format: 'dd-mm-yyyy',
	});

	$("#search_by_start_date").datepicker({
	    todayBtn: 1,
	    autoclose: true,
	    format: 'dd-mm-yyyy',
	}).on('changeDate', function (selected) {
	    var minDate = new Date(selected.date.valueOf());
	    $('#search_by_end_date').datepicker('setStartDate', minDate);
	});

	$("#search_by_end_date").datepicker({
	    autoclose: true,
	    todayBtn: 1,
	    format: 'dd-mm-yyyy',
	}).on('changeDate', function (selected) {
	    var maxDate = new Date(selected.date.valueOf());
	    $('#search_by_start_date').datepicker('setEndDate', maxDate);
	});

	$("#reset_startdate").click(function () {
	    $('#search_by_start_date').val('');
	});

	$("#reset_enddate").click(function () {
	    $('#search_by_end_date').val('');
	});

	$("#reset_createddate").click(function () {
	    $('#search_by_created_date').val('');
	});

//    if($('#taskSuccessError').val() == true){
//        swal("Task Successfully Created","", "success");
//    }
//    if($('#taskSuccessError').val() == false){
//        swal('Task Not Created Successfully',"", "error");
//    }
	 // Task Time Entry
         
    
    $("#saveTaskEntry").validate({
        rules: {
            taskDate: "required",
            workTimeHours: "required",
            workTimeMinutes: "required",
            //taskDescription: "required"
        },
        messages: {
            taskDate: "Please select date",
            workTimeHours: "Please select hours",
            workTimeMinutes: "Please select minutes",
            //taskDescription: "Please enter task description"
        },
        submitHandler: function (form) {
            var workTimeHours = $('#workTimeHours').val();
            var workTimeMinutes = $('#workTimeMinutes').val();
            var taskList = $('#task_list option:selected').val();
            
            var error = 0 ;
            if (workTimeHours == 0 && workTimeMinutes == 0) {
                $('#workTimeHours-error').css({'display': 'block'});
                $('#workTimeMinutes-error').css({'display': 'block'});

                $('#workTimeHours-error').text('Please select hours');
                $('#workTimeMinutes-error').text('Please select minutes');
                error ++;
            }
            if(taskList == 0 ||  typeof taskList === "undefined"){
                $('#task_list-error').css({'display': 'block'});
                $('#task_list-error').text('Please select task');
                error ++;
            } 
            if(error == 0){
                $('#saveTaskEntry .projectTaskEntry').attr("disabled", true);
                form.submit();
            }
        }
    });
    
    $("#miscBenchSaveTaskEntry").validate({
        rules: {
            taskDate: "required",
            workTimeHours: "required",
            workTimeMinutes: "required",
            //taskDescription: "required"
        },
        messages: {
            taskDate: "Please select date",
            workTimeHours: "Please select hours",
            workTimeMinutes: "Please select minutes",
            //taskDescription: "Please enter task description"
        },
        submitHandler: function (form) {
            var workTimeHours = $('#workTimeHours').val();
            var workTimeMinutes = $('#workTimeMinutes').val();
            var error = 0 ;
            if (workTimeHours == 0 && workTimeMinutes == 0) {
                $('#workTimeHours-error').css({'display': 'block'});
                $('#workTimeMinutes-error').css({'display': 'block'});

                $('#workTimeHours-error').text('Please select hours');
                $('#workTimeMinutes-error').text('Please select minutes');
                error ++;
            }
            
            if(error == 0){
                $('#miscBenchSaveTaskEntry .projectTaskEntry').attr("disabled", true);
                form.submit();
            }
        }
    });
    
    $(document).on("click", ".timeEntry", function () {
        
        $('#saveTaskEntry,#miscBenchSaveTaskEntry').trigger("reset");
        var projectId = $(this).attr('data-task-id');
        getProjectTaskList(projectId);
        var ProjeTaskId = $(this).attr('data-proje-task-id');
        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : ((fullDate.getMonth() + 1) < 10 ? '0' + (fullDate.getMonth() + 1) : (fullDate.getMonth() + 1));//'0' + (fullDate.getMonth() + 1);
        var todayDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
        jQuery.ajax({
            url  : getsiteurl() + '/check/time_entry',
            type : "GET",
            success: function(res) {
                jQuery.ajax({
                    url  : getsiteurl() + '/get/project/' + projectId,
                    type : "GET",
                    success: function(response) {
                        var startDate = '';
                        $('#taskDate').datepicker('destroy');
                        if(res == 0){
                           startDate = response.project_start_date; 
                        }else{
                            startDate = res;
                        }
                        $("#taskDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',
                            startDate: new Date(startDate.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
                            endDate: new Date(),
                            onSelect: function() {
                                return $(this).trigger('change');
                            }
                        });
                        $("#taskDate").on("change",function (){
                            $('#task_list').select2("val", 0);
                            getProjectTaskList(projectId);
                        });
                        jQuery.ajax({
                            url  : getsiteurl() + '/remainingProjectEntry',
                            type : "GET",
                            success: function(response) {
                                $('#workTimeHours option[value='+response['hours']+']').attr('selected','selected');
                                $('#workTimeMinutes option[value='+response['min']+']').attr('selected','selected');
                                $('#workTimeHours').trigger('change');
                                $('#workTimeMinutes').trigger('change');
                            }
                        });
                        $('#projectId').val(projectId);
                        if(ProjeTaskId != ''){
                            $('#projectTaskId').val(ProjeTaskId);
                        }

                        $('#taskTimeEntryModal').modal();
                    }
                });
            }
        });
        
    });
    
    function  getProjectTaskList(projectId){
        jQuery.ajax({
            url: getsiteurl() + '/get/project/task_list/'+projectId+'/'+$('#taskDate').val(),
            type: 'GET',
            success: function(data){
                var html = '';
                $('#task_list').html('');
                html += '<option value="0">Select Task</option>';
                if(Object.keys(data).length > 0){
                    jQuery.each( data, function( key, value ) {
                        html += '<option value="'+value.id+'">'+value.task_name+'</option>';
                      });
                      $('#task_list').html(html);
                      $('#task_list').change(function(){
                            if($('#task_list').val() != 0){
                                $('#task_list-error').text('');
                            }
                      });
                }
                
            }
        });        
    }
    function remainingProjectEntry(){
        jQuery.ajax({
                url  : getsiteurl() + '/remainingProjectEntry',
                type : "GET",
                success: function(response) {
                        return response;
                }
        });
	}
	$(document).on("click", ".createTeam", function(){
		var assignedToCompanyId = $(this).attr('data-assiged-to-companyid');
		var projectId = $(this).attr('id');
	
		jQuery.ajax({
				url: getsiteurl() + "/project/assign_to_team",
				method: 'GET',
				data: {assignedToCompanyId:assignedToCompanyId,projectId:projectId},
				success: function (response) {
					$('.assign_to_team_records').empty();
					$(".assign_to_team_records").append(response);
					$('#teamAssign').select2({'placeholder':'Select Developer'});
	
					manageCreateTeamInputs();
	
					$('#createTeamModal').modal();
					var selectedUserId = $('#teamAssign').val();
					$.each(selectedUserId, function (index, value) {
						jQuery.ajax({
							url: getsiteurl() + "/project/remove_team_member",
							method: 'GET',
							data: {unSelectUserId:value,projectId:projectId},
							success: function (response) {
								if(response){
									$("#teamAssign option[value='" + value + "']").addClass('unselecting');
								}
							}
						});
					});
					$('#teamAssign').on('select2:unselecting', function (e) {
						var unSelectUserId = e.params.args.data.id;
						var result = $("#teamAssign option[value='" + unSelectUserId + "']").attr('class');
						if(result == 'unselecting'){
							swal({
								title: "User already add project entry in this project,so you can't remove.",
								icon: "warning",
								buttons: true,
								dangerMode: true,
							});return false;
						}
					});
				}
			});
		});
	
		function manageCreateTeamInputs(){
			window.setTimeout(function(){
				$('.select2').select2();
			},300);
	
			$("#saveTeamForm").validate({
				rules: {
					teamLeader: "required"
				},
				messages: {
					teamLeader: "Please select team leader"
				},
				submitHandler: function (form) {
					form.submit();
				}
			});
		}
    $(document).on("click", "#deleteTask", function(){
        var taskEntryId = $(this).attr('data-task-id');
        if(taskEntryId != ''){
                swal({
                title: "Are you sure you want to delete project entry ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    jQuery.ajax({
                        url  : getsiteurl() + '/project_misc_task_entry/delete',
                        type : "DELETE",
                        data: {_token: CSRF_TOKEN,taskEntryId:taskEntryId},
                        success: function(response) {
                            if(response.status == 'success'){
                                swal(response.message,"", "success");
                                $('#taskEntryTable > tbody tr#'+taskEntryId).slideUp('slow');
                                window.location.reload();
                            }else if(response.status == 'error'){
                                swal(response.message,"", "error");
                            }
                        }
                    });
                }
              });
        }
    });
    $(document).on("click", "#miscUpdateProjectTask", function(){
        var projectTaskEntryId = $(this).attr('data-task-id');

        if(projectTaskEntryId != ''){
                jQuery.ajax({
                url  : getsiteurl() + '/edit/editProjectTask',
                type : "GET",
                data: {projectTaskEntryId:projectTaskEntryId},
                success: function(response) {
                        $('#editTask').html(response);
                        manageEditTaskEntryInputs();
                        $('#editTask').modal('show');
                }
            });
        }
    });

    function manageEditTaskEntryInputs(){
            $('.select2').select2();
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : ((fullDate.getMonth() + 1) < 10 ? '0' + (fullDate.getMonth() + 1) : (fullDate.getMonth() + 1));
            var todayDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
            var projectStartDate = $('#editTaskProjectStartDate').val();
            var projectEndDate = $('#editTaskProjectEndDate').val();
            $('#updateTask #taskStartDate').datepicker('destroy');
            $('#updateTask #taskEndDate').datepicker('destroy');
            if(projectEndDate == 'undefined'){
                $("#updateTask #taskStartDate").datepicker({
                    todayBtn: 1,
                    autoclose: true,
                    format: 'dd-mm-yyyy',
                    startDate: new Date(projectStartDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
                }).on('changeDate', function (selected) {
                    var endDate = new Date(((selected.date).getFullYear()), ((selected.date).getMonth()+1), 0);
                    var minDate = new Date(selected.date.valueOf());
                    $('#updateTask #taskEndDate').datepicker('setStartDate', minDate);
                    $('#updateTask #taskEndDate').datepicker('setEndDate', endDate);
                });
                $("#updateTask #taskEndDate").datepicker({
                    autoclose: true,
                    todayBtn: 1,
                    format: 'dd-mm-yyyy',
                }).on('changeDate', function (selected) {
                    var maxDate = new Date(selected.date.valueOf());
                    $('#updateTask #taskStartDate').datepicker('setEndDate', maxDate);
                });  
            }else{
                $("#updateTask #taskStartDate").datepicker({
                        todayBtn: 1,
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        startDate: new Date(projectStartDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                }).on('changeDate', function (selected) {
                    var endDate = new Date(((selected.date).getFullYear()), ((selected.date).getMonth()+1), 0);
                    var minDate = new Date(selected.date.valueOf());
                    $('#updateTask #taskEndDate').datepicker('setStartDate', minDate);
                    $('#updateTask #taskEndDate').datepicker('setEndDate', endDate);
                });

                $("#updateTask #taskEndDate").datepicker({
                    autoclose: true,
                    todayBtn: 1,
                    format: 'dd-mm-yyyy',
                }).on('changeDate', function (selected) {
                    var maxDate = new Date(selected.date.valueOf());
                    $('#updateTask #taskStartDate').datepicker('setEndDate', maxDate);
                });  
            }
    }
    $("#updateTask").validate({
        rules: {
            taskName: "required",
            taskStartDate: "required",
            taskEndDate: "required",
        },
        messages: {
            taskName: "Please enter task name",
            taskStartDate: "Please select start date",
            taskEndDate: "Please select end date",
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
   
         
    $("#saveTask").validate({
            rules: {
                taskName: "required",
                taskStartDate: "required",
                taskEndDate: "required",
            },
            messages: {
                taskName: "Please enter task name",
                taskStartDate: "Please select start date",
                taskEndDate: "Please select end date",
            },
            submitHandler: function (form) {
                form.submit();
            }
    });
        
        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        $('.project-info').on('click', function () {
            var project_id = $(this).attr('project_id');
            $.ajax({
                type: "get",
                url: getsiteurl() + '/project/get_project_cr_details/' + project_id,
                data: "",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#modal-body').html(response);
                    $('.view_cr_info').attr("href", getsiteurl() +'/project/project_info/'+project_id);
                    $('#project_cost').modal();
                }
            });
            return false;
        });
        $('#saveTask #taskStartDate').datepicker({
            todayBtn: 1,
            autoclose: true,
            format: 'dd-mm-yyyy',
        }).on('changeDate', function (selected) {
            var endDate = new Date(((selected.date).getFullYear()), ((selected.date).getMonth()+1), 0);
            var minDate = new Date(selected.date.valueOf());
            $('#saveTask #taskEndDate').datepicker('setStartDate', minDate);
            $('#saveTask #taskEndDate').datepicker('setEndDate', endDate);
        });
        $('#saveTask #taskEndDate').datepicker({
            autoclose: true,
            todayBtn: 1,
            format: 'dd-mm-yyyy',
        }).on('changeDate', function (selected) {
            var maxDate = new Date(selected.date.valueOf());
            $('#saveTask #taskStartDate').datepicker('setEndDate', maxDate);
        });  
        
        
});
	
