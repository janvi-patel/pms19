$(function () {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    function ini_events(ele) {
        ele.each(function () {

        var eventObject = {
          title: $.trim($(this).text())
        }

        $(this).data('eventObject', eventObject)

        $(this).draggable({
          zIndex        : 1070,
          revert        : true,
          revertDuration: 0
        })
      })
    }

    ini_events($('#external-events div.external-event'))

    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    $('#calendar').fullCalendar({
        header : {
            right  : 'prev,next',
            left: 'title'
        },
        editable  : true,
        droppable : true,
        firstDay  : 1,
        eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
            if(event.holidayType == null)
            {
                var holidayType = "weekend";
                var day = new Date(convertDate(event.start)).getDay();
                if(day === 0 || day === 6)
                {
                    updateHoliday(event.id,convertDate(event.start),event.title,holidayType);
                }
                else
                {
                    swal({
                          title: "This Is Not a Weekend Date",
                          icon: "warning",
                          button: true,
                    });
                    refereshCalender();
                }
            }
            else
            {
                var holidayType = "holiday";
                updateHoliday(event.id,convertDate(event.start),event.title,holidayType);
            }
        },
        eventClick: function(calEvent, jsEvent, view){
            var holidayType = calEvent.holidayType == null ? "weekend" : "holiday";
            swal({
              title: "Are you sure you want to delete " + calEvent.title + " " + holidayType + " ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((willDelete) => {
              if (willDelete) {
                deleteHoliday(calEvent);
              }
            });
        },
        viewRender: function (view, element) {
            refereshCalender();
        },
        drop : function (date, allDay){
            var eventTitle          = $(this).data('eventObject').title
            var originalEventObject = $(this).data('eventObject')
            var copiedEventObject   = $.extend({}, originalEventObject)

            copiedEventObject.start           = date
            copiedEventObject.allDay          = allDay
            copiedEventObject.backgroundColor = $(this).css('background-color')
            copiedEventObject.borderColor     = $(this).css('border-color')

            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)
            $(this).remove()
            if($(this).hasClass("weekend"))
            {
                var holidayType = "weekend";
                var day = new Date(date).getDay();
                if(day === 0 || day === 6)
                {
                    saveHoliday(new Date(date).getFullYear(),convertDate(date),eventTitle,holidayType);
                }
                else
                {
                    swal({
                          title: "This Is Not a Weekend Date",
                          icon: "warning",
                          button: true,
                    });
                    refereshCalender();
                }
            }
            else
            {
                var holidayType = "holiday";
                saveHoliday(new Date(date).getFullYear(),convertDate(date),eventTitle,holidayType);
            }
        }
    })

    function convertDate(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth()+1)).slice(-2),
            day  = ("0" + date.getDate()).slice(-2);
        return [ date.getFullYear(), mnth, day ].join("-");
    }

    function saveHoliday(year,date,holidayName,holidayType){
        jQuery.ajax({
            url: getsiteurl() + "/save/holiday",
            method: 'POST',
            data: {_token: CSRF_TOKEN, year: year,date: date,holidayName: holidayName,holidayType: holidayType},
            success: function (response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                    refereshCalender();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
            }
        });   
    }

    function getMonthWiseHolidayList(month,year){
        jQuery.ajax({
            url: getsiteurl() + "/get/holiday/month_wise",
            method: 'POST',
            data: {_token: CSRF_TOKEN, month: month, year: year},
            success: function (response) {
                if(response.status == 'success'){
                    if(response.result.length != 0){
                        $('#calendar').fullCalendar('removeEvents');
                        $(response.result).each(function(index,value) {
                            var event={id:value.id,title:value.title,holidayType:value.holidayType,start:new Date(value.year,value.month-1,value.day),backgroundColor:value.backgroundColor,borderColor:value.borderColor};
                            $('#calendar').fullCalendar('renderEvent',event,true);
                        });
                    }
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
            }
        });
    }

    function updateHoliday(id,date,title,holidayType){
        jQuery.ajax({
            url: getsiteurl() + "/update/holiday",
            method: 'POST',
            data: {_token:CSRF_TOKEN,id:id,date:date,title:title,holidayType:holidayType},
            success: function (response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                    refereshCalender();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
            }
        });
    }

    function deleteHoliday(calEvent){
        jQuery.ajax({
            url: getsiteurl() + "/delete/holiday",
            method: 'DELETE',
            data: {_token:CSRF_TOKEN,id:calEvent._id},
            success: function (response) {
                if(response.status == 'success'){
                    $('#calendar').fullCalendar('removeEvents', calEvent._id);
                    swal(response.message,"", "success");
                    refereshCalender();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
            }
        });
    }

    function refereshCalender(){
        var month = $('#calendar').fullCalendar('getDate').month();
        var year  = $('#calendar').fullCalendar('getDate').year();
        getMonthWiseHolidayList(month+1,year);
    }

    var currColor = '#3c8dbc'
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
        e.preventDefault()
        currColor = $(this).css('color')
        $('#add-new-event').css({
            'background-color': currColor,
            'border-color'    : currColor
        })
    })

    $('#weekend-color-chooser > li > a').click(function (e) {
        e.preventDefault()
        currColor = $(this).css('color')
        $('#add-new-weekend-event').css({
            'background-color': currColor,
            'border-color'    : currColor
        })
    })

    $('#add-new-event').click(function (e) {
        e.preventDefault()
        var val = $('#new-event').val()
        if (val.length == 0) {
            return
        }

        //Create events
        var event = $('<div />')
        event.css({
            'background-color': currColor,
            'border-color'    : currColor,
            'color'           : '#fff'
        }).addClass('external-event')
        
        event.html(val)
        $('#external-events').prepend(event)

        //Add draggable funtionality
        ini_events(event)

        //Remove event from text input
        $('#new-event').val('')
    })

    $('#add-new-weekend-event').click(function (e) {
        e.preventDefault()
        var val = $('#new-weekend-event').val()
        if (val.length == 0) {
            return
        }

        //Create events
        var event = $('<div />')
        event.css({
            'background-color': currColor,
            'border-color'    : currColor,
            'color'           : '#fff'
        }).addClass('external-event weekend')
        
        event.html(val)
        $('#external-events').prepend(event)

        //Add draggable funtionality
        ini_events(event)

        //Remove event from text input
        $('#new-weekend-event').val('')
    })
})