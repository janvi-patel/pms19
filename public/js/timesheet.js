var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(function () {
    $('.select2').select2();
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    
})
function addRow() {
    $('#last_element_ref').before('<div class="col-md-6 row-block" id="rowx"><label class="control-label"></label><input class="form-control form-white" placeholder="In entry" type="text" name="entry_in" id="entry_in" value="" required /></div><div class="col-md-6 row-block" id="rowy"><label class="control-label"></label><div class="input-group"><input class="form-control form-white" placeholder="Out entry" type="text" name="entry_out" id="entry_out" value="" required /><div class="input-group-addon"><a href="javascript:void(0)" class="delete-button" id="delete_row_x" onclick="deleteRow()"><i class="mdi mdi-close"></i></a></div></div></div>');
    reassignInputID();
    reassignAnchorID();
    return false;
}

function deleteRow(elementID) {
    $('#row' + elementID).remove();
    $('#row' + (elementID - 1)).remove();
    reassignInputID();
    reassignAnchorID();
    return false;
}

function reassignInputID() {
    var newID = '';
    $("#time_entry_row :input").each(function (index, value) {
        var elementID = $(this).attr('id');
        newID = 'entry' + (index + 1);
        $('#' + elementID).attr('name', newID);
        $('#' + elementID).attr('id', newID);
        $(this).closest('.row-block').attr('id', 'row' + (index + 1));
    });
    return true;
}

function reassignAnchorID() {
    var newID = '';
    var i = 2;
    $(".delete-button").each(function (index, value) {
        var elementID = $(this).attr('id');
        newID = 'delete_row_' + (i);
        $('#' + elementID).attr('id', newID);
        $('#' + newID).attr("onclick", "deleteRow(" + i + ")");
        i = (i + 2);
    });
    return true;
}

function displayTimeEntryPopup(timeEntryID) {
    $.ajax({
        type: "POST",
        url: getsiteurl() + '/pms/gettimeentry',
        data: {_token: CSRF_TOKEN, "id": timeEntryID},
        success: function (response) {
            $('#time-entry-content').html(response);
            $('#time-entry-content').html(response);
            $('#update-time-entry').modal('show');
            return false;
        }
    });
}

$("#update_timeentry").submit(function (event) {
    // cancels the form submission
    event.preventDefault();
    updateTimeEntry();
});
function updateTimeEntry() {
    var count = $('#time_entry_count').val();
    var data = $("#update_timeentry").serialize();
    $.ajax({
        type: "POST",
        url: getsiteurl() + '/pms/updatetimeentry',
        data: {
            _token: CSRF_TOKEN,
            data:data
        },
        success: function (response) {
            $('#time_success_msg').hide();
            $('#time_error_msg').hide();
            if (response == 'success') {
                $('#time_success_msg').show();
                setTimeout(function () {
                    $('#update-time-entry').modal('hide');
                    location.reload();
                }, 2000);
            } else {
                $('#time_error_msg').show();
            }
            return false;
        }
    });
    return false;
}

$("#timeSheetFilterForm").validate({
    rules: {
        search_month: "required",
        search_year: "required"
    },
    messages: {
        search_month: "Please select month",
        search_year: "Please select year"
    },
    submitHandler: function (form) {
        form.submit();
    }
});