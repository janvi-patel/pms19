$(document).ready(function () {
	$('#company').select2(); 
	$('.select2').select2();

	$(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

	


  $(document).on("click", ".createTeam", function(){
    var assignedToCompanyId = $(this).attr('data-assiged-to-companyid');
    var projectId = $(this).attr('id');

    jQuery.ajax({
            url: getsiteurl() + "/project/assign_to_team",
            method: 'GET',
            data: {assignedToCompanyId:assignedToCompanyId,projectId:projectId},
            success: function (response) {
                $('.assign_to_team_records').empty();
                $(".assign_to_team_records").append(response);
                $('#teamAssign').select2({'placeholder':'Select Developer'});

                manageCreateTeamInputs();

                $('#createTeamModal').modal();
                var selectedUserId = $('#teamAssign').val();
                $.each(selectedUserId, function (index, value) {
                    jQuery.ajax({
                        url: getsiteurl() + "/project/remove_team_member",
                        method: 'GET',
                        data: {unSelectUserId:value,projectId:projectId},
                        success: function (response) {
                            if(response){
                                
                                $("#teamAssign option[value='" + value + "']").addClass('unselecting');
                            }
                        }
                    });
                });
                $('#teamAssign').on('select2:unselecting', function (e) {
                    $('.select2-search input').prop('focus', 1);
                    manageCreateTeamInputs(); 
                    var unSelectUserId = e.params.args.data.id;
                    var result = $("#teamAssign option[value='" + unSelectUserId + "']").attr('class');
                    if(result == 'unselecting'){
                        swal({
                            title: "User already add project entry in this project,so you can't remove.",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        });return false;
                    }
                   
                });
            }
        });
    });

    function manageCreateTeamInputs(){
        window.setTimeout(function(){
            $('.select2').select2();
            // if (!select2.opened()) {
            // select2.open();
        // }
        },300);

        $("#saveTeamForm").validate({
            rules: {
                teamLeader: "required"
            },
            messages: {
                teamLeader: "Please select team leader"
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }
    $('.project-info').on('click', function () {
        var project_id = $(this).attr('project_id');
        $.ajax({
            type: "get",
            url: getsiteurl() + '/project/get_project_cr_details/' + project_id,
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#modal-body').html(response);
                $('.view_cr_info').attr("href", getsiteurl() +'/project/project_info/'+project_id);
                $('#project_cost').modal();
            }
        });
        return false;
    });
});
