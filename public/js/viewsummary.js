$(document).ready(function(){   
    $(document).on("mouseover", ".punchData", function(){
    	var id = $(this).attr('id');

    	if(id != ''){
    		var tableId = 'punchInOut_'+id; 
		    $('#'+tableId).empty();
			$('#'+tableId).append('<tr><td colspan="2">Loading...</td></tr>');

    		jQuery.ajax({
		      	url  : getsiteurl() + '/get_punch_data',
		    	type : "GET",
		      	data: {attendanceId:id},
		      	success: function(response) {
		    		$('#'+tableId).empty();
		      		if(response.status == 'success'){		      			
		      			$.each(response.result, function(index, value) {
							$('#'+tableId).append('<tr><td>'+value.in_time+'</td><td>'+value.out_time+'</td></tr>');
						});
		      		}
		       	}
		    });
    	}
    });
    $(document).on("mouseover", ".todayPunchData", function(){
        var id = $(this).attr('id');

        if(id != ''){
            var tableId = 'punchInOut_'+id; 
            $('#'+tableId).empty();
            $('#'+tableId).append('<tr><td colspan="2">Loading...</td></tr>');

            jQuery.ajax({
                url  : getsiteurl() + '/get_punch_data_today',
                type : "GET",
                data: {attendanceId:id},
                success: function(response) {
                     //alert(response);
                    $('#'+tableId).empty();
                    if(response.status == 'success'){   
                        var test = 0;                    
                        $.each(response.result, function(index, value) {
                           // console.log(value.out);
                            if(value.out == undefined){
                                value.out = '';
                            }
                           $('#'+tableId).append('<tr><td>'+value.in+'</td><td>'+value.out+'</td></tr>');
                            
                        });
                    }
                }
            });
        }
    });

    // 
    $(document).on("click", "#addRow", function(){
    	var totalElement = $(".element").length;
        if(totalElement > 0){
            var lastid = $(".element:last").attr("id");
            var split_id = lastid.split("_");
            var nextindex = Number(split_id[1]) + 1;
            $('#hidenTotalElement').val(nextindex);
            $(".element:last").after('<div class="row element" id="inOutEntryDiv_'+ nextindex +'"><div class="form-group col-lg col-md-6 col-sm-6"><input type="text" id="inEntry_'+ nextindex +'" name="inEntry_'+ nextindex +'" value="" class="form-control allownumeric getEnteryKey" data-key="'+nextindex+'" autocomplete="off" placeholder="In Entry (hh:mm:ss)" /><div class="invalid-feedback inEntry_'+ nextindex +'" role="alert"></div></div><div class="form-group col-lg col-md-6 col-sm-6"><input type="text" id="outEntry_'+ nextindex +'" name="outEntry_'+ nextindex +'" value="" class="form-control allownumeric" autocomplete="off" placeholder="Out Entry (hh:mm:ss)" /><div class="invalid-feedback outEntry_'+ nextindex +'" role="alert"></div></div><div class="form-group removeIcon"><a href="javascript:void(0);" title="Remove Time Entry" id="remove_'+ nextindex +'" data-attendance-detail-id="" class="remove">X</a></div></div>');
        }else{
            var nextindex = 1;
            $('#hidenTotalElement').val(nextindex);
            $('.timeEntryBody_modal').html('');
            $('.timeEntryBody_modal').html('<div class="row element" id="inOutEntryDiv_'+ nextindex +'"><div class="form-group col-lg col-md-6 col-sm-6"><input type="text" id="inEntry_'+ nextindex +'" name="inEntry_'+ nextindex +'" value="" class="form-control allownumeric getEnteryKey" data-key="'+nextindex+'" autocomplete="off" placeholder="In Entry (hh:mm:ss)" /><div class="invalid-feedback inEntry_'+ nextindex +'" role="alert"></div></div><div class="form-group col-lg col-md-6 col-sm-6"><input type="text" id="outEntry_'+ nextindex +'" name="outEntry_'+ nextindex +'" value="" class="form-control allownumeric" autocomplete="off" placeholder="Out Entry (hh:mm:ss)" /><div class="invalid-feedback outEntry_'+ nextindex +'" role="alert"></div></div><div class="form-group removeIcon"><a href="javascript:void(0);" title="Remove Time Entry" id="remove_'+ nextindex +'" data-attendance-detail-id="" class="remove">X</a></div></div>');
        }
    });

    $(document).on("click", ".remove", function(){
    	var attendanceDetailId = $(this).attr('data-attendance-detail-id');
        var row_id = this.id;

    	if(attendanceDetailId != ''){
            deleteTimeEntry(attendanceDetailId,row_id);   	
//    		swal({
//                title: 'Are you sure you want to delete time entry !',
//                icon: "warning",
//                buttons: true,
//                dangerMode: true,
//            }).then((willDelete) => {
//                if (willDelete) {
//             		deleteTimeEntry(attendanceDetailId);   	
//                }
//            });
    	}else{
            var split_id = row_id.split("_");
            var deleteindex = split_id[1];
            $("#inOutEntryDiv_" + deleteindex).remove();
		}
		
		
    });

    function deleteTimeEntry(attendanceDetailId,row_id){
    	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    	jQuery.ajax({
	      	url  : getsiteurl() + '/delete/punch_data',
	    	type : "DELETE",
	      	data: {_token: CSRF_TOKEN,attendanceDetailId:attendanceDetailId},
	      	success: function(response) {
                    if(response.status == 'success'){
                        //swal(response.message,"", "success");
                        var split_id = row_id.split("_");
                        var deleteindex = split_id[1];
                        $("#inOutEntryDiv_" + deleteindex).remove();
                        $('#hidenTotalElement').val($(".element").length);
//	        		window.setTimeout(function(){
//	        			location.reload()
//	        		},2000);
                    }else if(response.status == 'error'){
                        swal(response.message,"", "error");
//                                    window.setTimeout(function(){
//	        			location.reload()
//	        		},2000);
                    }
	       	}
		});
		
    }

    $(document).on("click", ".updateTimeEntry", function(){
        var attendanceId = $(this).attr('id');
    	$('.modal-body.timeEntryBody_modal').empty();
    	$('.modal-body.timeEntryBody_modal').append('Loading...');

    	jQuery.ajax({
	      	url  : getsiteurl() + '/punch_data',
	    	type : "GET",
	      	data: {attendanceId:attendanceId},
	      	success: function(response) {
                if(response.status == 'success'){
                    $('#updateTimeEntryModal').html(response.html).modal('show');;
                    $('#hidenTotalElement').val($(".element").length);
                } else {
                    swal(response.message,"", "success");
                }
	       	}
	    });
    });
     
    $(document).on("click", ".addTimeEntriesBtn", function(){
    	$('.modal-body.timeEntryBody_modal').empty();
    	$('.modal-body.timeEntryBody_modal').append('Loading...');
        var selectedMonthDate = $("#selectedMonthDate").val();
    	jQuery.ajax({
	      	url  : getsiteurl() + '/add_punch_data',
	    	type : "GET",
            data : {selectedMonthDate:selectedMonthDate},
	      	success: function(response) {
                if(response.status == 'success'){
                    $('#addTimeEntryModal').html(response.html).modal('show');
                    $('#hidenTotalElement').val($(".element").length);
                } else {
                    swal(response.message,"", "success");
                }
	       	}
	    });
    });
	    
	// Validation 
	var colunCount = 0;
    $(document).on("keypress keyup", ".allownumeric", function(event){
    	if($(this).val() != ''){
    		var arr    =[];
			arr        =$(this).val().split(':');

			if(arr.length == 3){
				colunCount = 3;
			}else if(arr.length == 2){
				colunCount = 1;
			}else if(arr.length == 1){
				colunCount = 0;
			}
    	}else{
	    	if(event.which == 58){
	    		colunCount = colunCount + 1;
	    	}
    	}

    	if(colunCount > 2){
    		if ((event.which < 48 || event.which > 57)) {
	            event.preventDefault();
	    	}
    	}else{
	    	if ((event.which < 48 || event.which > 58)) {
	            event.preventDefault();
	    	}
    	}
    });

    $(document).on("blur", ".allownumeric", function(event){
    	colunCount = 0;
    });
    // Validation

    $(document).on("click", "#updateEntry", function(event){
        var getEnteryKey = [];
        event.preventDefault();
        $('.getEnteryKey').each(function(key,value) {
            getEnteryKey[key] = $(this).data('key');
        });
        data = $("#timeEntryForm").serializeArray();
        data.push({name: "getEnteryKey",value:getEnteryKey});
        var submitForm   = false;
        jQuery.ajax({
            url  : getsiteurl() + '/validate_punch_data',
            type : "GET",
            data : data,
            success: function(response) {
                if(typeof response.status !== "undefined"){
                    if(response.status == "success"){
                        submitForm = true;
                        $('#timeEntryError').css({'display':'none'});
                        $('#timeEntryErrorMessage').text('');
                        $("#timeEntryForm").submit();
                    } else {
                        swal(response.message,"", "error");
                    }
                } else {
                    swal("Something went wrong!","", "error");
                    location.reload();
                }
            },error: function(response) {
                if($('.invalid-feedback strong').length>0){$('.invalid-feedback strong').remove();}
                var errObj=jQuery.parseJSON(response.responseText);
                $.each( errObj.errors, function( key, value ){
                    $("."+key).html('<strong>'+value[Object.keys(value)[0]]+'</strong>').fadeIn();
                });
            }
        });
    });
    $(document).on('change','.auto_leave_check_box',function() {
        var checkBoxId = $(this).attr('id');
        if($('#'+checkBoxId).is(":checked")){
            $('.' + checkBoxId).prop('disabled', false);
        } else {
            $('.' + checkBoxId).prop('disabled', true);
        }
    });
    // $(document).on("click", "#addAutoLeaveBtn", function(event){
	//     event.preventDefault();
    //         var submitForm   = true;
	//     if(submitForm == true){
	//     	$("#addAutoLeaveForm").submit();
	//     }
    // });
});