$(document).ready(function(){
    $('[data-toggle="tooltip"]').popover({html:true});
    $("#search_by_entry_date").datepicker({
        todayBtn: 1,
        autoclose: true,
        format: 'dd-mm-yyyy',
    });
    $("#reset_entry_date").click(function () {
        $('#search_by_entry_date').val('');
    });
    $('#export_to_excel').on('click', function () {
        var link  = window.location.pathname;
        var query_string = window.location.search;
        if(query_string != '')
        {
            var redirectLink = link+query_string+'&'+'submit_type=export_excel';
        }
        else{
            var redirectLink = link +'?'+'submit_type=export_excel';
        }
        window.location = redirectLink;
    });

    $('#page_rang_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;
        
    });	
});