/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//user listing action
$(document).ready(function(){
   
    
    
    $('.hidden').hide();
    $('.information').tooltip("show");
    $('#search_by_department').select2();
    $('#search_by_internal_company_id').select2();
    // $('.techModal').click(function () {
    //       $('[data-toggle="tooltip"]').tooltip("hide");

    //    });
	$('[data-toggle="tooltip"]').tooltip();
	//checked user action
    $("#update_checked_user").prop("disabled", true);
	$('#checkAll').click(function () {
         if($('#checkAll:checkbox:checked').length){
            $("#update_checked_user").prop("disabled", false);
         }else{
            $("#update_checked_user").prop("disabled", true);
         }
        $('input:checkbox').prop('checked', this.checked);
    });

    $(".update_user_row").change(function(){
        if (!$(this).prop("checked")){
            $("#checkAll").prop("checked",false);
        }
        var atLeastOneIsChecked = $('#user_data :checkbox:checked').length >0;
        if(atLeastOneIsChecked){
            $("#update_checked_user").prop("disabled", false);
        }else{
            $("#update_checked_user").prop("disabled", true);
        }

    });

    $(".update_checked_user_disabled").click(function(){
        var is_disabled = $('.update_checked_user_disabled select')[0].hasAttribute("disabled");
        if(is_disabled){
            swal({
            title: "Please select user to perform action.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        }
    });

    if($( "#work_home:checked" ).length != 0){
            $('.work_home_end_date,.work_home_start_date').show();
        }
    $('#work_home').change(function(){
        if($( "#work_home:checked" ).length != 0){
            $('.work_home_end_date,.work_home_start_date').show();
        }else{
            $('.work_home_end_date,.work_home_start_date').hide();
            $('#work_start_date,#work_end_date').datepicker('setDate', null);
        }
    });
    $("#designation_id").on('change',function() {
        var designation_id = $(this).val();
        $.ajax({
            type: "GET",
            url: getsiteurl() + '/get_role/' + designation_id,
            success: function (response) {
                $('.role_id').select2("val",response);
            }
        });
    });
    if($('#employee_status').val() == 1){
        $('.status_date').removeClass('d-none');
    }
    $(".emp_status").on('change',function() {
        var employee_status = $('#employee_status').val();
        if(employee_status == 1){
            $('.status_date').removeClass('d-none');
        }else{
            $('.status_date').addClass('d-none');
            $('.confirmation_date').datepicker('setDate', null);
        }
    });
    $('#update_checked_user').change(function(){
        var userIdArrayData = [];
        $.each($(".update_user_row:checked"), function() {
            userIdArrayData.push($(this).val());
        });
        var action = $(this).val();
        if(action != '' && userIdArrayData.length != 0){
            if(action == 2){
                swal({
                    title: "Are you sure you want to delete?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        updateUserStatus(userIdArrayData,action);
                    }
                  });
            }else if(action == 3 || action == 4){
                swal({
                    title: "Are you sure you want to change the employment status?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('.hidden').show();
                        updateUserStatus(userIdArrayData,action);
                    }
                  });
            }else{
                swal({
                    title: "Are you sure you want to change the status?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                      updateUserStatus(userIdArrayData,action);
                    }
                  });
            }
        }

    });

    // delete user action
    $(document).on("click", ".deleteUser", function(){
		var userId = $(this).attr('data-user-id');
		if(userId != ''){
			swal({
              title: "Are you sure you want to delete user.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((willDelete) => {
              if (willDelete) {
                deleteUser(userId);
              }
            });
		}
	});

    $('#page_range_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;

    });
	// user create
    // for employee code
    $('#company_id').change(function(){
        var  companyId = $('#company_id').val();
        createEmployeeCode(companyId);
    })
    //datepicker
    $('#joining_date,#user_last_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy'

    });
    $('#dob').datepicker({
        maxDate: 0,
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy',
        endDate: "today"


    });
    $('#work_start_date,#work_end_date,#confirmation_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy'

    });

    $('#joining_date').datepicker("setDate", new Date($('#joining_date').val()));
    $('#user_last_date').datepicker("setDate", new Date($('#user_last_date').val()));
    $('#dob').datepicker("setDate", new Date($('#dob').val()));
    $('#joining_date,#dob,#work_start_date,#work_end_date').attr('readonly','readonly');

    //shift time picker
    $('#from_shift').timepicker({
        showInputs: true,
        minuteStep: 30,
        format:'hh:mm',
        template:'dropdown',
        defaultTime:'10:30 AM'

    });
    $('#to_shift').timepicker({
        showInputs: true,
        minuteStep: 30,
        format:'hh:mm',
        template:'dropdown',
        defaultTime:'07:30 PM'
    });

    $('#from_shift,#to_shift').attr('readonly','readonly');
    $('select#company_id').select2();
    $("select#internal_company_id").select2();
    $("select#provident_fund").select2();
    //select2 for all select menu
    $('#user_status,#update_checked_user,#page_range_dropdown,#search_by_designation,#search_by_reporting_to,#search_by_company,#department,#reporting_to,#employee_status,#gratuity,#designation_id,#role_id').select2();
	// validate IFSC code
    $(".ifsc_code").change(function () {
        var inputvalues = $(this).val();
          var reg = /[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$/;
                        if (inputvalues.match(reg)) {
                            $(".ifsc_err").text("");
                            return true;
                        }
                        else {
                             $(".ifsc_code").val("");
                            // alert("You entered invalid IFSC code");
                            $(".ifsc_err").text("You entered invalid IFSC code");
                            // document.getElementById("ifsc_err").focus();
                            return false;
                        }
    });
	//form validation
	$("#user_create").validate({
		rules: {
                first_name: "required",
                last_name: "required",
                email: {required: true,email: true},
                password: "required",
                confirm_password: "required",
                phone: "required",
                employee_id: "required",
                company_id: "required",
                total_leaves: "required",
                work_start_date:{required:function(element) {
                                    return $( "#work_home:checked" ).length != 0;
                                    },
                                    lessThanEqual:'#work_end_date'
                                },
                confirmation_date:{required:function(element) {
                                    return $('#employee_status').val() == 1;
                                    }
                                },
                work_end_date:{required:function(element) {
                                    return $( "#work_home:checked" ).length != 0;
                                    },
                                    greaterThanEqual:'#work_start_date'
                                },
                designation_id: "required",
                department: "required",
                reporting_to: "required",
                bank_account_no: {
                    minlength: 9,
                    maxlength: 18,
                    digits: true,
                },
                role_id: "required",
                employee_status: "required",
                gratuity: "required",
                provident_fund : "required",
                trainee_probation_month : "required",
                provident_fund_amount:{required:function(element) {
                    return $('#provident_fund').val() == 1;
                    }
                },
                from_shift: "required",
                to_shift: "required"
        },
        messages:{
                first_name: "Please enter first name.",
                last_name: "Please enter last name.",
                email: {
                    required: "Please enter email.",
                    email: "Your email address must be in the format of name@domain.com",
                },
                password: "Please enter password.",
                confirm_password: "Please enter confirm password.",
                phone: "Please enter contact number.",
                employee_id: "Please enter employee id.",
                total_leaves: "Please enter total leaves.",
                designation_id: "Please select Designation.",
                department: "Please select Department.",
                reporting_to: "Please select reporting to.",
                work_start_date:"Please select start date",
                work_end_date:"Please select end date",
                confirmation_date:"Please select confirmation date",
                role_id: "Please select role.",
                employee_status: "Please select employee status.",
                gratuity: "Please select gratuity.",
                provident_fund: "Please select provident fund.",
                provident_fund_amount:"Please select provident fund amount",
                company_id: "Please select company.",
                from_shift: "Please enter from shift time.",
                to_shift: "Please enter to shift time.",
                trainee_probation_month: "Please enter Trainee / Probation Months.",
                bank_account_no:{
                    digits: 'Account Number must be an number',
                    minlength: 'Account number cannot be less than 8 digits',
                    maxlength: 'Account number cannot be more than 18 digits',
                },
                work_start_date:{
                    lessThanEqual : 'Start date must be less than end date.',
                },
                 work_end_date:{
                    greaterThanEqual : 'End date must be greater than start date.',
                },
        },
        submitHandler: function (form) {
            form.submit();
        }


	});

	$('#user_edit').validate({
		rules: {
                first_name: "required",
                last_name: "required",
                email: {required: true,email: true},
                phone: "required",
                designation_id: "required",
                work_start_date:{required:function(element) {
                                    return $( "#work_home:checked" ).length != 0;
                                    }
                                },
                confirmation_date:{required:function(element) {
                                    return $('#employee_status').val() == 1;
                                    }
                                },
                work_end_date:{required:function(element) {
                                    return $( "#work_home:checked" ).length != 0;
                                    }
                                },
                company_id: "required",
                department: "required",
                reporting_to: "required",
                role_id: "required",
                employee_status: "required",
                gratuity:"required",
                provident_fund : "required",
                provident_fund_amount:{required:function(element) {
                    return $('#provident_fund').val() == 1;
                    }

                },
                 user_last_date:{required:function(element) {
                    return $('#employee_status').val() == 3;
                    }
                },
                user_status: "required",
                from_shift: "required",
                to_shift: "required"
        },
        messages:{
                first_name: "Please enter first name.",
                last_name: "Please enter last name.",
                company_id: "Please select company.",
                email: {
                    required: "Please enter email.",
                    email: "Your email address must be in the format of name@domain.com",
                },
                phone: "Please enter contact number.",
                work_start_date:"Please select start date",
                work_end_date:"Please select end date",
                confirmation_date:"Please select confirmation date",
                designation_id: "Please select Designation.",
                department: "Please select Department.",
                reporting_to: "Please select reporting to.",
                role_id: "Please select role.",
                employee_status: "Please select employee status.",
                gratuity:"Please select Gratuity",
                provident_fund: "Please select provident fund.",
                provident_fund_amount:"Please select provident fund amount",
                user_status: "Please select user status.",
                user_last_date:"Last date is required.",
                from_shift: "Please enter from shift time.",
                to_shift: "Please enter to shift time."
        },
        submitHandler: function (form) {
            form.submit();
        }

	});
    // validate select
    $('select').change(function(){
        if ($(this).val()!="")
        {
            $(this).valid();
        }
    });

    $('.fa.fa-sort-alpha-desc').attr('class','fa fa-sort-desc');
    $('.fa.fa-sort-alpha-asc').attr('class','fa fa-sort-asc');

    $('#pan_no').change(function(){
       var panVal = $(this).val();
       if(panVal!=''){
           var respnose= validatePanNumber(panVal);
           if(respnose){
            $('.pan_no').html('<b>Please Enter Valid PAN Number.</b>');
           }else{
            $('.pan_no').html(' ');
           }
       }else{
            $('.pan_no').html(' ');
       }
    });

    $('#dob,#joining_date').change(function(){

        var dob_val = $('#dob').val();
        var dob_stamp = validateDobJoiningDate(dob_val);

        var joinning_val = $('#joining_date').val();
        var joinning_stamp = validateDobJoiningDate(joinning_val);

        if (dob_stamp!='' && joinning_stamp!=''){
            if(dob_stamp>joinning_stamp){
                $('.dob_error').html('<b>Joining Date must be greater than Date Of Birth.</b>');
            }else{
                $('.dob_error').html(' ');
            }
        }

    });
    $('.edit_reporting_form').click(function(){
        $(this).prev().prev().css('display', 'none');
        $(this).prev().css('display', 'inline');
    });

    $(document).on("change",".edit_reporting_to",function() {
        var reporting_id = $(this).val();
        var user_id = $(this).prev('.reporting_edit').attr('id');
        if(user_id != '' && reporting_id != ''){
            swal({
              title: "Are you sure you want to change reporting user.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                            url: getsiteurl() + '/users/updatereportingajax',
                            type: 'GET',
                            data:{reporting_id: reporting_id,user_id: user_id},
                            success: function(data) {
                                if(data.status == "success"){
                                    swal(data.message,"", "success");
                                } else {
                                    swal(data.message,"", "error");
                                }
                                setTimeout(function(){
                                    location.reload();
                                },2000)
                            }
                    });
                }
            });
        }
    });
    // $('#user_status').change(function(){
    //      // alert($(this).val());
    //    if($(this).val() === ''||$(this).val() == 1){
    //     $('.user_last_date').css("display","none");
    //    }
    //    else{
    //        $('.user_last_date').css("display","block");
    //    }
    // });
    $('#employee_status').change(function(){
         // alert($(this).val());
       if($(this).val() === ''|| $(this).val() != 3){
        $('.user_last_date').css("display","none");
       }
       else{
           $('.user_last_date').css("display","block");
       }
    });

       $('#provident_fund').change(function(){
       if($(this).val() == 1){
        $('.provident_fund_amount').css("display","block");
       }else{
           $('.provident_fund_amount').css("display","none");
       }
    });
});



	// action delete user
	function deleteUser(userId){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({
	      	url  : getsiteurl() + '/users/delete',
	    	type : "DELETE",
	      	data: {_token: CSRF_TOKEN,userId:userId},
	      	success: function(response) {
	        	if(response.status == 'success'){
                    swal(response.message,"", "success");
                    $('#user_data > tbody tr#'+userId).slideUp('slow');
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
	       	}
	    });
	}

	function  updateUserStatus(userIdArrayData,action){
	    $.ajax({
	       	url: getsiteurl() + '/users/updateuserajax',
	        type: 'GET',
	        data:{userIdArrayData: userIdArrayData,action: action},
	        success: function(data){
	            if(data.status == "success"){
                    swal(data.message,"", "success");
                } else {
                    swal(data.message,"", "error");
                }
                setTimeout(function(){
                    location.reload();
                },2000);
	        }
	    });
 	}
    function createEmployeeCode(companyId){
        if(companyId != ''){
            $.ajax({
                type: "GET",
                url: getsiteurl() + '/users/create_employee_code',
                data: {'companyId': companyId},
                success: function (response) {
                    $('#employee_id').val(response);
                }
            });
        }else{
            $('#employee_id').val('');
        }
    }
    function validateKeyStrokesAlphaOnly(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return true;
        }
        return false;
    }
    function validateKeyStrokesNumberOnly(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function validateKeyStrokesDecimal(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57)){
            return false;
        }
        return true;
    }

    function validatePanNumber(panVal){
        var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
        if(regpan.test(panVal)){
           return false;
        } else {
           return true;
        }

    }

    function validateDobJoiningDate (dob_val) {
        var dobArray = dob_val.split("-");
        var dob_std = dobArray[1]+"/"+dobArray[0]+"/"+dobArray[2];
        var time_dob_std = new Date(dob_std);
        var dob_stamp = time_dob_std.getTime();
        return dob_stamp;
    }

    $('#export_to_excel').on('click', function () {
        var link  = window.location.pathname;
        var query_string = window.location.search;
        if(query_string != '')
        {
            var redirectLink = link+query_string+'&'+'submit_type=export_excel';
        }
        else{
            var redirectLink = link +'?'+'submit_type=export_excel';
        }
        window.location = redirectLink;
    });
// to import user
 $(document).on("click", ".techModal", function(){
        $.ajax({
            type: "get",
            url: getsiteurl() + '/importuser_modal/',
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#addDocsModal').html(response);
                $('#addDocsModal').modal();
            }
        });
        return false;
    });

 $(document).on("click", ".userDetailsModal", function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr('data-id');

        $.ajax({
            type: "post",
            url: getsiteurl() + '/show_user_details',
            data: {_token: CSRF_TOKEN,id:id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#userDetailsModal').html(response.html);
                $('#userDetailsModal').modal();
            }
        });
        return false;
    });
