$( document ).ready(function() {
    window.location.href = "#last_chat";
    $('[data-toggle="tooltip"]').popover({html:true});
    $('.select2').select2();
    $('.reply_btn').on('click',function () {
        var chat_id = $(this).attr('chat_id');
        var sender_name = $(this).attr('sender_name');
        $("#reply_id").val(chat_id);
        $(".reply_to").removeClass("d-none");
        $(".remove_to_replay").removeClass("d-none");
        $(".reply_to").html("reply to @"+sender_name);
    });

    $('.mark-readed').on('click',function (){
        var chat_id = $(this).attr('chat_id');
        readMessage(chat_id);
    });

    $( ".on_hover_read_message" ).hover(function(){
        var chat_id = $(this).attr('chat_id');
        readMessage(chat_id);
    });

    $('.edt_msg_btn').on('click',function () {
        var chat_id = $(this).attr('chat_id');
        var chat_text = $(this).attr('chat_text');
        var r_id = $(this).attr('r_id');
        var sender_name = $(this).attr('sender_name');
        $("#send_message").addClass("d-none");
        $("#edit_message").removeClass("d-none");
        $(".edt_reply_id").val(r_id);
        $(".inform_to_email").prop('checked', true);
        $(".edt_chat_id").val(chat_id);
        if(r_id  !== "")
        {
            $(".edt_reply_to").removeClass("d-none");
            $(".edt_remove_to_replay").removeClass("d-none");
            $(".edt_reply_to").html("reply to @"+sender_name);
        }
        $(".edt_chat_text").text(chat_text);
    });

    $('.cancel_msg').on('click',function () {
        $("#send_message").removeClass("d-none");
        $("#edit_message").addClass("d-none");
        $(".edt_reply_id").val("");
        $(".edt_chat_id").val("");
        $(".edt_chat_text").text("");
        $(".edt_reply_to").addClass("d-none");
    });

    $('.delete_msg').on('click',function () {
        var chat_id = $(this).attr('chat_id');

        if(chat_id != ''){
            swal({
                title: "Are you sure you want to delete Chat?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteChat(chat_id);
                }
            });
        }
    });

    function deleteChat(chat_id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
        url  : getsiteurl() + '/chat/delete',
        type : "DELETE",
        data: {_token: CSRF_TOKEN,chat_id:chat_id},
        success: function(response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                    location.reload();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
            }
        });
    }

    $('.chat_file').on('click', function () {
        var chat_id = $(this).attr('chat_id');
        $.ajax({
            type: "get",
            url: getsiteurl() + '/lead_chat/get_file_details/' + chat_id,
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#lead_file').html(response);
                $('#lead_file').modal();
            }
        });
        return false;
    });

    $('.addChatFiles').on('click', function () {
        var chat_id = $(this).attr('chat_id');
        $.ajax({
            type: "get",
            url: getsiteurl() + '/lead_chat/add_file_details/' + chat_id,
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#addLeadDocs').html(response);
                $('#addLeadDocs').modal();
            }
        });
        return false;
    });

    function readMessage(chat_id){
        $.ajax({
            type: "get",
            url: getsiteurl() + '/chat/mark_as_read/' + chat_id,
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if(typeof response.lead_id !== "undefined")
                {
                    reloadRead(response.lead_id,response.chat_id);
                }
            }
        });
    }

});