 $( document ).ready(function() {
     $('[data-toggle="tooltip"]').tooltip(); 
    //For index
     $('#search_compoff_start_date, #search_compoff_end_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy'
    });
    $('#search_compoff_start_date, #search_compoff_end_date,#compoff_start_date, \n\
      #compoff_end_date,compoff_encash_month_year ,#search_compoff_team_start_date, #search_compoff_team_end_date').attr('readonly','readonly');
    //For create
    $('#compoff_start_date, #compoff_end_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
    
    //For edit_marked_as
//    $('#compoff_encash_month_year').datepicker({
//        autoclose: true,
//        todayHighlight: true,
//        format: 'mm-yyyy'
//    });
    $("#compoff_marked_as").change(function(){
        if($(this).val() == '1'){
            $('#monthDiv').show();
        }else{
            $('#monthDiv').hide();
        }
    });
    $(".update_compoff_row").change(function(){
        if (!$(this).prop("checked")){
             $("#checkAll").prop("checked",false);
        }
    });
    //For team compoffs
    $('#search_compoff_team_start_date, #search_compoff_team_end_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy'
    });
    $('.compoff_approver,#compoff_start_type,#compoff_end_type,#compoff_status,#compoff_marked_as').select2();
    $('#update_checked_compoff,#page_rang_dropdown').select2();
    $('#search_by_employee,#search_by_approver,#search_by_compoff_status,#search_team_compoff_emp,#search_team_compoff_approver,#ddlCompOffStatus').select2();
    
    
    $("#compoff_create").validate({
        rules: {
            compoff_start_date:{required:true,lessThanEqual:'#compoff_end_date'},               
            compoff_end_date:{required:true,greaterThanEqual:'#compoff_start_date'},                
            compoff_description: "required"                
        },
        messages: {
        
            compoff_start_date: {required:"The comp-off start date field is required.",lessThanEqual :'The comp-off start date is less than end date'} ,
            compoff_end_date: {required:"The comp-off end date field is required.",greaterThanEqual :'The comp-off end date is greater than start date'} ,
            compoff_description: "The comp-off description field is required."
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    
    $("#compoff_update").validate({
        rules: {
            approver_comment: "required"               
        },
        messages: {
            approver_comment: "The approver comment field is required."
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    
//    $("#compoff_marked_as_update").validate({
//        rules: {
//            compoff_encash_month_year: "required"               
//        },
//        submitHandler: function (form) {
//            form.submit();
//        }
//    });
    $('#checkAll').click(function () {    
        $('input:checkbox').prop('checked', this.checked);    
    });
    
    $('#update_checked_compoff').change(function(){
        var compoffIdArray = [];
        $.each($(".update_compoff_row:checked"), function() {
            compoffIdArray.push($(this).val());
        });
        var action = $(this).val();
        if(action != '' && compoffIdArray.length != 0){
            if(action == 0){
                swal({
                    title: "Are you sure you want to delete?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete)  {
                    if (willDelete) {
                        updateCompoffConfirm(compoffIdArray,action);
                    }
                  });
            }else{
                swal({
                    title: "Are you sure you want to change the status?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete)  {
                    if (willDelete) {
                      updateCompoffConfirm(compoffIdArray,action);
                    }
                  });
            }
        }
    });
    
    // $('#page_rang_dropdown').change(function(){
    //      var limit = $(this).val();
    //     $('#page_range').val(limit);
    //     $('#search_filter').submit();
    // });

    $('#page_rang_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;
    
    });

    $('.deletecompoff').click(function(){
        var compoffId = $(this).attr('data-task-entry-id');
        if(compoffId != ''){
                swal({
                    title: "Are you sure you want to delete comp-off entry",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete)  {
                if (willDelete) {
                  deleteCompoff(compoffId);
                }
             });
        }
    });	
    
 });
 function deleteCompoff(compoffId){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
        url  : getsiteurl() + '/compoffs/delete',
        type : "DELETE",
        data: {_token: CSRF_TOKEN,compoffId:compoffId},
        success: function(response) {
            if(response.status == 'success'){
                swal(response.message,"", "success");
                $('.compoff_listing > tbody tr#'+compoffId).slideUp('slow');
            }else if(response.status == 'error'){
                swal(response.message,"", "error");
            }
        }
    });
}
 function updateCompoffConfirm(compoffIdArray,action){
    $.ajax({
        url: getsiteurl() + '/compoffs/updatecheckedcompoff',
        type: 'GET',
        data:{compoffIdArray: compoffIdArray,action: action},
        success: function(data){
            var message = data+" out of "+compoffIdArray.length+" comp-off  updated successfully.";
            var icon  = "success";
            var dangerMode = false;
            if(action == 11 || action == 22){
               
                if(data == 'allpending'){

                var message = "Please approve comp-off first";
                  var icon = "warning";
                var dangerMode = true;
                }else{
                var message = data+" out of "+compoffIdArray.length+" comp-off Encashed/Leave Added successfully";
                var icon = "warning";
                var dangerMode = true;    
                }
                
            }
            if(compoffIdArray.length == data){
                var icon = "success";
                var dangerMode = false;
            }else{
                var icon = "warning";
                 var dangerMode = true;
            }
            swal({
                title: message,
                icon: icon,
                buttons: true,
                dangerMode: dangerMode,
            }).then(function(successData)  {
                if (successData) {
                  location.reload();
                }
            });
        }  
    });
 }



   