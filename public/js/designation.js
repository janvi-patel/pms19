/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Designation index 
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
    $('#page_rang_dropdown').change(function(){
	    var limit = $(this).val();
	    var link  = window.location.pathname;
	    var query_string = window.location.search;
	    var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
	    if(limit != '' && query_string.indexOf('search_submit') != -1){
	       var redirectLink = link +'?page_range='+limit + '&'+search_str;
	    }else{
	        var redirectLink = link +'?page_range='+limit;
	    }
	    window.location = redirectLink;
    
    });	
	
	$('#designation_create,#designation_update').validate({
		rules: {
                designation_name: "required"
        },
        messages:{
                designation_name: "Please enter designation name."              
        },
        submitHandler: function (form) {
            form.submit();
        }

	});

	$("#page_rang_dropdown").select2();

    $(document).on("click", ".deleteDesigantion", function(){
		var id = $(this).attr('data-user-id');
		if(id != ''){
			swal({
              title: "Are you sure you want to delete Designation.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((willDelete) => {
              if (willDelete) {
                deleteDesigantion(id);
              }
            });
		}
	});


});
	function deleteDesigantion(id){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		
		jQuery.ajax({
	      	url  : getsiteurl() + '/designation/destroy',
	    	type : "delete",
	      	data: {_token: CSRF_TOKEN,id:id},
	      	success: function(response) {
	        	if(response.status == 'success'){
                    swal(response.message,"", "success");
                    $('#designation_listing > tbody tr#'+id).slideUp('slow');
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
	       	}
	    });
	}