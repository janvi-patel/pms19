$(document).ready(function(){
	$('.select2').select2();
	
	$('#checkAll').click(function () {    
        $('input:checkbox').prop('checked', this.checked);    
    });

	$(".chboxTaskEntry").change(function(){
		if (!$(this).prop("checked")){
			$("#checkAll").prop("checked",false);
		}
	});

    $('#taskEntryDropdown').change(function(){
    	var selectedTaskEntryDropdownAction = $("#taskEntryDropdown option:selected" ).val();

            if(selectedTaskEntryDropdownAction != ''){
                var taskEntryIdsArray = [];

                $.each($(".chboxTaskEntry:checked"), function() {				
                taskEntryIdsArray.push($(this).val());
            });

            if(taskEntryIdsArray.length != 0){
                swal({
                    title: "Are you sure you want delete selected project entry?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        deleteMultipleTaskEntry(taskEntryIdsArray);
                    }
                });
            }
            }
    });

    function deleteMultipleTaskEntry(taskEntryIdsArray){
    	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		jQuery.ajax({
	      	url  : getsiteurl() + '/project_entry/multiple/delete',
	    	type : "DELETE",
	      	data: {_token: CSRF_TOKEN,taskEntryIdsArray:taskEntryIdsArray},
	      	success: function(response) {
                    if(response.status == 'success'){
                    swal(response.message,"", "success");
                    window.setTimeout(function(){
                            location.reload()
                    },2000);
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                    window.setTimeout(function(){
                            location.reload()
                    },2000);
                }
	       	}
	    });
    }

	// $('#page_range_dropdown').change(function(){
	//     var limit = $(this).val();
    //         $('#page_range').val(limit);
    //         $('#search_filter').submit();
	    
    // });
    
	$('#page_range_dropdown').change(function(){
	    var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;
    
    });
	
	$("#reset_log_to_date").click(function () {
	    $('#search_by_log_to_date').val('');
	});

	$("#search_by_log_to_date").datepicker({
	    todayBtn: 1,
	    autoclose: true,
	    format: 'dd-mm-yyyy',
	});
        
        $("#task_start_date,#task_end_date").datepicker({
	    todayBtn: 1,
	    autoclose: true,
	    format: 'dd-mm-yyyy',
	});

	$("#reset_log_from_date").click(function () {
	    $('#search_by_log_from_date').val('');
	    $('#search_by_log_to_date').val('');
	});

	$("#search_by_log_from_date").datepicker({
	    todayBtn: 1,
	    autoclose: true,
	    format: 'dd-mm-yyyy',
	});
        
        $("#search_by_log_to_date").datepicker({
	    todayBtn: 1,
	    autoclose: true,
	    format: 'dd-mm-yyyy',
	});
        
	$(document).on("click", ".deleteTaskEntry", function(){
		var projectEntryId = $(this).attr('data-task-entry-id');

		if(projectEntryId != ''){
			swal({
              title: "Are you sure you want to delete project entry ?",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((willDelete) => {
              if (willDelete) {
                deleteTaskEntry(projectEntryId);
              }
            });
		}
	});
	
	function deleteTaskEntry(projectEntryId){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		jQuery.ajax({
	      	url  : getsiteurl() + '/project_entry/delete',
	    	type : "DELETE",
	      	data: {_token: CSRF_TOKEN,projectEntryId:projectEntryId},
	      	success: function(response) {
	        	if(response.status == 'success'){
                            swal(response.message,"", "success");
//                            $('#taskEntryTable > tbody tr#'+taskEntryId).slideUp('slow');
                            window.location.reload();
                        }else if(response.status == 'error'){
                            swal(response.message,"", "error");
                        }
	       	}
	    });
	}

	$(document).on("click", ".updateTaskEntry", function(){
		var projectEntryId = $(this).attr('data-task-entry-id');
		var indentify = $(this).attr('data-class');
                var pageIdentifier = window.location.pathname;
		if(projectEntryId != ''){
			jQuery.ajax({
		      	url  : getsiteurl() + '/edit/project_entry',
		    	type : "GET",
		      	data: {projectEntryId:projectEntryId},
		      	success: function(response) {
		      		$('#editTaskEntryDiv').empty()
		      		$('#editTaskEntryDiv').append(response);
                                if(indentify === 'old_project_entry' ||indentify === 'misc_task_entry' ){
                                    $('.task_listing_block').css('display','none');
                                }
                                var projectId = $('#editProjectId').val();
                                getProjectTaskList(projectId);
		      		manageEditTaskEntryInputs();
		        	$('#editTaskTimeEntryModal').modal('show');
                                $('#pageLink').val(pageIdentifier);
                                $("#editTaskDate").on("change",function (){ 
                                    var projectId = $('#editProjectId').val();
                                    getProjectTaskList(projectId);
                                });
		       	}
		    });
		}
	});
        function  getProjectTaskList(projectId){
            jQuery.ajax({
                url: getsiteurl() + '/get/project/task_list/'+projectId+'/'+$('#editTaskDate').val(),
                type: 'GET',
                success: function(data){
                    var html = '';
                    $('#task_list').html('');
                    html += '<option value="0">Select Task</option>';
                    if(Object.keys(data).length > 0){
                        jQuery.each( data, function( key, value ) {
                            html += '<option value="'+value.id+'">'+value.task_name+'</option>';
                        });
                        $('#task_list').html(html);
                        $("#task_list").select2().select2('val', $('#editTaskId').val());
                    }

                }
            });        
        }
        
	function manageEditTaskEntryInputs(){
		$('.select2').select2();
                var fullDate = new Date();
                var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : ((fullDate.getMonth() + 1) < 10 ? '0' + (fullDate.getMonth() + 1) : (fullDate.getMonth() + 1));
                var todayDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
		$('#editTaskDate').datepicker('destroy');
		$("#editTaskDate").datepicker({
		    todayBtn: 1,
		    autoclose: true,
		    format: 'dd-mm-yyyy',
		    startDate: new Date(($('#editProjectTaskStartDate').val() != ''?$('#editProjectTaskStartDate').val():$('#editTaskStartDate').val()).replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
//		    endDate: new Date(($('#editProjectTaskEndDate').val() != ''?$('#editProjectTaskEndDate').val():todayDate).replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                    endDate: new Date(),
		});

		$("#updateTaskEntry").validate({
	        rules: {
                        editTaskDate: "required",
                        editWorkTimeHours: "required",
                        editWorkTimeMinutes: "required",
	        },
	        messages: {
	            editTaskDate: "Please select date",
	            editWorkTimeHours: "Please select hours",
	            editWorkTimeMinutes: "Please select minutes",
	        },
	        submitHandler: function (form) {
	            var workTimeHours   = $('#editWorkTimeHours').val();
                    var workTimeMinutes = $('#editWorkTimeMinutes').val();
                    var taskList = $('#task_list option:selected').val();
                    var error = 0 ;
                    if(workTimeHours == 0 && workTimeMinutes == 0){
                            $('#editWorkTimeHours-error').css({'display':'block'});
                            $('#editWorkTimeMinutes-error').css({'display':'block'});

                            $('#editWorkTimeHours-error').text('Please select hours');
                            $('#editWorkTimeMinutes-error').text('Please select minutes');
                    }
                    var currenct_url = window.location.pathname;
                    var check_url = 'view/project_task_entry';
                    if(currenct_url.indexOf(check_url) != -1){
                        if(taskList == 0 ||  typeof taskList === "undefined"){
                            $('#task_list-error').css({'display': 'block'});
                            $('#task_list-error').text('Please select task');
                            error ++;
                        }
                    }
                    
                    if(error == 0){
                        form.submit();
                    }
	        }
	    });
	}
});