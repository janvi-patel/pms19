$( document ).ready(function() {
    $('[data-toggle="tooltip"]').popover({html:true});
    $('.select2').select2();
    $(".estimation_end_date").datepicker('destroy');
    $(".estimation_end_date").datepicker({
        todayBtn: 1,
        autoclose: true,
        format: 'dd-mm-yyyy',
        startDate: new Date(),
    });
    $(".reviewer_end_date").datepicker({
        todayBtn: 1,
        autoclose: true,
        format: 'dd-mm-yyyy',
        startDate: new Date(),
    });
    $(document).on("click",".permission_to_bde",  function() {
        var id = $(this).attr('data-docs-id');
        if(id != ''){
            swal({
              title: "Are you sure you want to change Permission.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: getsiteurl() + '/lead/estimation_docs/permission_to_bde',
                        type: 'GET',
                        data:{id: id},
                        success: function(data){
                            if(data.status == 'success'){
                                swal(data.message,"", "success");
                                location.reload();
                            }else if(data.status == 'error'){
                                swal(data.message,"", "error");
                            }
                        }
                    });    
                }
            });
        }
    });
    $('.reviewer_end_date').on("change",  function() {
        var date = $(this).val();
        var id = $(this).prev('.r_end_date').attr('lead_id');
        if(id != '' && date != ''){
            swal({
              title: "Are you sure you want to change Date.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: getsiteurl() + '/lead/update/reviewer_end_date',
                        type: 'GET',
                        data:{date: date,id: id},
                        success: function(data){
                            if(data.status == 'success'){
                                swal(data.message,"", "success");
                                location.reload();
                            }else if(data.status == 'error'){
                                swal(data.message,"", "error");
                            }
                        }
                    });    
                }
            });
        }
    });

    $('#page_range_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;
    
    });
    $('.updateDate').click(function(){
        $(this).prev().prev().css('display', 'none');
        $(this).prev().css('display', 'inline');
    });
    $(document).on("click" ,"#addDocs", function(){
        var lastname_id = $('.fileInput input[type=file]:nth-child(1)').last().attr('file-tag');
        var split_id = lastname_id.split('_');
        var index = Number(split_id[1]) + 1;
        if(index <= 10)
        {
            var newel = $('.fileInput:last').clone(true);
            // $(newel).find('label[custom=label_file]').first().attr("data_val_file","uploadedFileName_"+index);
            $(newel).find('input[type=file]:nth-child(1)').first().attr("file-tag","uploadedFileName_"+index).val("");
            $(newel).find('span[type=label]:nth-child(2)').first().attr("id","uploadedFileName_"+index).text('Drag and Drop file...');
            $(newel).insertAfter(".fileInput:last");
        }
        else{
            swal("Please select maximum 10 documents to upload","", "error");
        }
    });
    manageForm();
    function manageForm() {
        $("#leadAdd").validate({
            rules: {
                lead_title: "required",
                technology: "required",
                client_name: "required",
                company_id: 'required',
                project_manager: 'required',
                project_types: 'required',
                estimation_status: 'required',
                description: 'required',
            },
            messages: {
                lead_title: "Please enter title",
                technology: "Please select technology",
                client_name: "Please enter client name",
                company_id: "Please select company",
                project_manager: "Please select project manager",
                project_types: "Please select project type",
                estimation_status: "Please select estimation status",
                description: "Please  enter description",
            },
        });
    }

    newChatForm();
    function newChatForm() {
        $("#send_message").validate({
            rules: {
                chat_text: "required",
            },
            messages: {
                chat_text: "Please enter message",
            },
        });
    }

    editChatForm();
    function editChatForm() {
        $("#edit_message").validate({
            rules: {
                edt_chat_text: "required",
            },
            messages: {
                edt_chat_text: "Please enter message",
            },
        });
    }

    $('#company_id').on('change',function(){
        var Id = $('#company_id').val();
        var companyId = (Id == '')? 0 : Id;
        jQuery.ajax({
            type: "GET",
            url: getsiteurl() + '/get_pm_list/'+companyId,
            success: function (data) {
                var html = '';
                html += '<option value="">Select Project Manager</option>';
                if (Object.keys(data).length > 0) {
                    jQuery.each(data, function (key, value) {
                        html += '<option value="' + value.id + '">' + value.fullName + '</option>';
                    });
                }
                $('#project_manager').html(html);
                $('#project_manager').select2();
                var tl = '<option value="">Select Team Leader</option>';
                $('#team_leader').html(tl);
                $('#team_leader').select2();
            }
        });
    });

    $('#project_manager').on('change',function(){
        var Id = $('#project_manager').val();
        var proId = (Id == '')? 0 : Id;
        jQuery.ajax({
            type: "GET",
            url: getsiteurl() + '/get_tl_list/'+proId,
            success: function (data) {
                var html = '';
                html += '<option value="">Select Team Leader</option>';
                if (Object.keys(data).length > 0) {
                    jQuery.each(data, function (key, value) {
                        html += '<option value="' + value.id + '">' + value.fullName + '</option>';
                    });
                }
                $('#team_leader').html(html);
                $('#team_leader').select2();
            }
        });
    });

    $('.project_attachment').change(function(e){
        var Ids = $(e.target).attr('file-tag');
        var files =$(this).prop("files");
        if(files.length !== 0){
            var ext = $("[file-tag|='"+Ids+"']").val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['pdf','xls','xlsx','doc','docx','jpeg','png','jpg','psd','mp4','mkv','ppt','pptx','pptm']) == -1) {
                $(this).val("");
                $("#"+Ids).text("Drag and Drop file...");
                swal("Only Pdf, Xls, Xlsx, Doc, Docx, Ppt, Pptx, Pptm, Jpeg, Jpg, Png, Psd, mp4, mkv files are accepted","", "error");
            }
            else{
                var names  = $.map(files, function(val) { return val.name; });
                $("#"+Ids).text(names);
            }
        }
        else{
            $("#"+Ids).text("Drag and Drop file...");
        }
    });

    $('#chat_attachment').change(function(e){
        var files =$(this).prop("files");
        var inValid = false;
        if(files.length !== 0){
            $.each(files, function (index,value) {
                var ext = value.name.split('.').pop().toLowerCase();
                if($.inArray(ext, ['pdf','xls','xlsx','doc','docx','jpeg','png','jpg','psd','mp4','mkv','ppt','pptx','pptm']) == -1) {
                    inValid = true;
                }
            });
            if(inValid) {
                $(this).val("");
                $('#uploadedFileName').text("Drag and Drop up to 10 files...");
                swal("Only Pdf, Xls, Xlsx, Doc, Docx, Ppt, Pptx, Pptm, Jpeg, Jpg, Png, Psd, mp4, mkv files are accepted","", "error");
            }
            else{
                var names  = $.map(files, function(val) { return val.name; });
                $("#uploadedFileName").text(names);
            }
        }
        else{
            $('#uploadedFileName').text("Drag and Drop up to 10 files...");
        }
    });

    $(document).ready(function() {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target
            e.relatedTarget
        });
    });

    $('.attach_docs').on('click',function () {
        $(".chat_attachment_file").removeClass("d-none");
    });

     // validate select
    $('select').change(function(){
        if ($(this).val()!="")
        {
            $(this).valid();
        }
    });

    // $('.reply_btn').on('click',function () {
    //     var chat_id = $(this).attr('chat_id');
    //     var sender_name = $(this).attr('sender_name');
    //     $("#reply_id").val(chat_id);
    //     $(".reply_to").removeClass("d-none");
    //     $(".remove_to_replay").removeClass("d-none");
    //     $(".reply_to").html("reply to @"+sender_name);
    // });

    $('.remove_to_replay').on('click',function () {
        $("#reply_id").val("");
        $(".reply_to").addClass("d-none");
        $(".remove_to_replay").addClass("d-none");
        $(".reply_to").html("");
    });

    // $('.mark-readed').on('click',function (){
    //     var chat_id = $(this).attr('chat_id');
    //     $.ajax({
    //         type: "get",
    //         url: getsiteurl() + '/chat/mark_as_read/' + chat_id,
    //         data: "",
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         success: function () {
    //             location.reload();
    //         }
    //     });
    // });

    $(document).on("click", ".deleteDocs", function(){
        var dataDocsID = $(this).attr('data-docs-id');

        if(dataDocsID != ''){
            swal({
                title: "Are you sure you want to delete Document ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  deleteDocs(dataDocsID);
                }
              });
        }
    });
    function deleteDocs(dataDocsID){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
        url  : getsiteurl() + '/docs/delete',
        type : "DELETE",
        data: {_token: CSRF_TOKEN,dataDocsID:dataDocsID},
        success: function(response) {
            if(response.status == 'success'){
                swal(response.message,"", "success");
                $('.docs'+dataDocsID).addClass('d-none');
            }else if(response.status == 'error'){
                swal(response.message,"", "error");
            }
        }
        });
    }

    $(document).on("click", ".deleteEstimationDocs", function(){
        var dataDocsID = $(this).attr('data-docs-id');

        if(dataDocsID != ''){
            swal({
                title: "Are you sure you want to delete Document ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                  deleteEstimationDocs(dataDocsID);
                }
            });
        }
    });
    function deleteEstimationDocs(dataDocsID){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
        url  : getsiteurl() + '/docs/estimation/delete',
        type : "DELETE",
        data: {_token: CSRF_TOKEN,dataDocsID:dataDocsID},
        success: function(response) {
            if(response.status == 'success'){
                swal(response.message,"", "success");
                $('.docs'+dataDocsID).addClass('d-none');
            }else if(response.status == 'error'){
                swal(response.message,"", "error");
            }
        }
        });
    }

    $(document).on("click", ".deleteLead", function(){
        var deleteLead = $(this).attr('data-lead-id');

        if(deleteLead != ''){
            swal({
                title: "Are you sure you want to delete Project Lead ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    deleteLeadById(deleteLead);
                }
            });
        }
    });
    function deleteLeadById(deleteLead){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
        url  : getsiteurl() + '/lead/delete',
        type : "DELETE",
        data: {_token: CSRF_TOKEN,deleteLead:deleteLead},
        success: function(response) {
                if(response.status == 'success'){
                    swal(response.message,"", "success");
                    location.reload();
                }else if(response.status == 'error'){
                    swal(response.message,"", "error");
                }
            }
        });
    }

    $('.ViewEstimationDocs').on('click', function () {
        var lead_id = $(this).attr('lead-id');
        $.ajax({
            type: "get",
            url: getsiteurl() + '/lead_estimation/get_file_details/' + lead_id,
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#ViewEstimationDocs').html(response);
                $('#ViewEstimationDocs').modal();
            }
        });
        return false;
    });

    $('.AddEstimationDocs').on('click', function () {
        var lead_id = $(this).attr('lead-id');
        $.ajax({
            type: "get",
            url: getsiteurl() + '/lead_estimation/add_file_details/' + lead_id,
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#AddEstimationDocs').html(response);
                $('#AddEstimationDocs').modal();
            }
        });
        return false;
    });

    $('.lead-file').on('click', function () {
        var lead_id = $(this).attr('lead-id');
        $.ajax({
            type: "get",
            url: getsiteurl() + '/lead_attachment/get_file_details/' + lead_id,
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#lead_file').html(response);
                $('#lead_file').modal();
            }
        });
        return false;
    });

    $('.addLeadDocs').on('click', function () {
        var lead_id = $(this).attr('lead-id');
        $.ajax({
            type: "get",
            url: getsiteurl() + '/lead_attachment/add_file_details/' + lead_id,
            data: "",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#addLeadDocs').html(response);
                $('#addLeadDocs').modal();
            }
        });
        return false;
    });

    // $('.chat_file').on('click', function () {
    //     var chat_id = $(this).attr('chat_id');
    //     $.ajax({
    //         type: "get",
    //         url: getsiteurl() + '/lead_chat/get_file_details/' + chat_id,
    //         data: "",
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         success: function (response) {
    //             $('#lead_file').html(response);
    //             $('#lead_file').modal();
    //         }
    //     });
    //     return false;
    // });

    // $('.addChatFiles').on('click', function () {
    //     var chat_id = $(this).attr('chat_id');
    //     $.ajax({
    //         type: "get",
    //         url: getsiteurl() + '/lead_chat/add_file_details/' + chat_id,
    //         data: "",
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         success: function (response) {
    //             $('#addLeadDocs').html(response);
    //             $('#addLeadDocs').modal();
    //         }
    //     });
    //     return false;
    // });

    $(document).ready(function() {
        $('#chat_attachment').filedrop();
        $('#project_attachment').filedrop();
    });
    
    // $('.edt_msg_btn').on('click',function () {
    //     var chat_id = $(this).attr('chat_id');
    //     var chat_text = $(this).attr('chat_text');
    //     var r_id = $(this).attr('r_id');
    //     var sender_name = $(this).attr('sender_name');
    //     $("#send_message").addClass("d-none");
    //     $("#edit_message").removeClass("d-none");
    //     $(".edt_reply_id").val(r_id);
    //     $(".inform_to_email").prop('checked', true);
    //     $(".edt_chat_id").val(chat_id);
    //     if(r_id  !== "")
    //     {
    //         $(".edt_reply_to").removeClass("d-none");
    //         $(".edt_remove_to_replay").removeClass("d-none");
    //         $(".edt_reply_to").html("reply to @"+sender_name);
    //     }
    //     $(".edt_chat_text").text(chat_text);
    // });

    // $('.cancel_msg').on('click',function () {
    //     $("#send_message").removeClass("d-none");
    //     $("#edit_message").addClass("d-none");
    //     $(".edt_reply_id").val("");
    //     $(".edt_chat_id").val("");
    //     $(".edt_chat_text").text("");
    //     $(".edt_reply_to").addClass("d-none");
    // });

    // $('.delete_msg').on('click',function () {
    //     var chat_id = $(this).attr('chat_id');

    //     if(chat_id != ''){
    //         swal({
    //             title: "Are you sure you want to delete Chat?",
    //             icon: "warning",
    //             buttons: true,
    //             dangerMode: true,
    //         }).then((willDelete) => {
    //             if (willDelete) {
    //                 deleteChat(chat_id);
    //             }
    //         });
    //     }
    // });

    // function deleteChat(chat_id){
    //     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //     jQuery.ajax({
    //     url  : getsiteurl() + '/chat/delete',
    //     type : "DELETE",
    //     data: {_token: CSRF_TOKEN,chat_id:chat_id},
    //     success: function(response) {
    //             if(response.status == 'success'){
    //                 swal(response.message,"", "success");
    //                 location.reload();
    //             }else if(response.status == 'error'){
    //                 swal(response.message,"", "error");
    //             }
    //         }
    //     });
    // }

    $('.edt_remove_to_replay').on('click',function () {
        $(".edt_reply_id").val("");
        $(".edt_reply_to").addClass("d-none");
        $(".edt_remove_to_replay").addClass("d-none");
        $(".edt_reply_to").html("");
    });

    $("#reset_end_date").click(function () {
        $('#estimation_end_date').val('');
    });

     $(".update_lead_row").change(function(){
        if (!$(this).prop("checked")){
             $("#checkAll").prop("checked",false);
        }
    });

       $('#checkAll').click(function () {    
        $('input:checkbox').prop('checked', this.checked);    
    });
});



      $('#update_checked_lead').change(function(){
        var leadIdArray = [];
        $.each($(".update_lead_row:checked"), function() {
            leadIdArray.push($(this).val());
        });
        var action = $(this).val();
        if(action != '' && leadIdArray.length != 0){
           
                swal({
                    title: "Are you sure you want to change the status?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete)  {
                    if (willDelete) {
                      updateLeadConfirm(leadIdArray,action);
                    }
                  });
            }
        // }
    });

      function updateLeadConfirm(leadIdArray,action){
    $.ajax({
        url: getsiteurl() + '/lead/updatecheckedleads',
        type: 'GET',
        data:{leadIdArray: leadIdArray,action: action},
        success: function(data){
            var message = data+" out of "+leadIdArray.length+" leads  updated successfully.";
            var icon  = "success";
            var dangerMode = false;
       
            if(leadIdArray.length == data){
                var icon = "success";
                var dangerMode = false;
            }else{
                var icon = "warning";
                 var dangerMode = true;
            }
            swal({
                title: message,
                icon: icon,
                buttons: true,
                dangerMode: dangerMode,
            }).then(function(successData)  {
                if (successData) {
                  location.reload();
                }
            });
        }  
    });
}