$(document).ready(function(){ 
    $('.select2').select2();
    
    // $('#page_range_dropdown').change(function(){
    //     var limit = $(this).val();
    //     $('#page_range').val(limit);
    //     $('#search_filter').submit();
    // });
    $('#page_range_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;
    
    });
    $("#saveTaskEntry").validate({
        rules: {
            taskDate: "required",
            workTimeHours: "required",
            workTimeMinutes: "required",
            //taskDescription: "required"
        },
        messages: {
            taskDate: "Please select date",
            workTimeHours: "Please select hours",
            workTimeMinutes: "Please select minutes",
            //taskDescription: "Please enter task description"
        },
        submitHandler: function (form) {
            var workTimeHours = $('#workTimeHours').val();
            var workTimeMinutes = $('#workTimeMinutes').val();
            var taskList = $('#task_list option:selected').val();
            var error = 0 ;
            if (workTimeHours == 0 && workTimeMinutes == 0) {
                $('#workTimeHours-error').css({'display': 'block'});
                $('#workTimeMinutes-error').css({'display': 'block'});

                $('#workTimeHours-error').text('Please select hours');
                $('#workTimeMinutes-error').text('Please select minutes');
                error ++;
            }
            if(taskList == 0 ||  typeof taskList === "undefined"){
                $('#task_list-error').css({'display': 'block'});
                $('#task_list-error').text('Please select task');
                error ++;
            } 
            if(error == 0){
                $('#saveTaskEntry .projectTaskEntry').attr("disabled", true);
                form.submit();
            }
        }
    });
    $(document).on("click", ".timeEntry", function () {
        $('#saveTaskEntry').trigger("reset");
        $('#task_list').trigger("change");
        var projectId = $(this).attr('data-task-id');
       // var ProjeTaskId = $(this).attr('data-id');
        var ProjeTaskId = $(this).attr('data-id');
        newGetProjectTaskList(projectId,ProjeTaskId);
        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : ((fullDate.getMonth() + 1) < 10 ? '0' + (fullDate.getMonth() + 1) : (fullDate.getMonth() + 1));//'0' + (fullDate.getMonth() + 1);
        var todayDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
        jQuery.ajax({
            url  : getsiteurl() + '/check/time_entry',
            type : "GET",
            success: function(res) {
                jQuery.ajax({
                    url  : getsiteurl() + '/get/project/' + projectId,
                    type : "GET",
                    success: function(response) {
                        $('#taskDate').datepicker('destroy');
                        if(res == 0){
                            startDate = response.project_start_date; 
                        }else{
                            startDate = res;
                        }
                        $("#taskDate").datepicker({
                            todayBtn: 1,
                            autoclose: true,
                            format: 'dd-mm-yyyy',
                            startDate: new Date(startDate.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")),
                            //endDate: new Date(todayDate.replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))
                            endDate: new Date()
                        });
                        $("#taskDate").on("change",function (){
                            $('#task_list').select2("val", 0);
                            newGetProjectTaskList(projectId,ProjeTaskId);
                        });
                        jQuery.ajax({
                            url  : getsiteurl() + '/remainingProjectEntry',
                            type : "GET",
                            success: function(response) {
                                $('#workTimeHours option[value='+response['hours']+']').attr('selected','selected');
                                $('#workTimeMinutes option[value='+response['min']+']').attr('selected','selected');
                                $('#workTimeHours').trigger('change');
                                $('#workTimeMinutes').trigger('change');
                            }
                        });
                        $('#projectId').val(projectId);
                        if(ProjeTaskId != ''){
                            $('#projectTaskId').val(ProjeTaskId);
                        }
                        
                        $('#taskTimeEntryModal').modal();
                    }
                });
            }
        });
    });
});
function  newGetProjectTaskList(projectId,projectTaskId) {
    jQuery.ajax({
        url: getsiteurl() + '/get/project/new_task_list/' + projectId +'/' + projectTaskId + '/' + $('#taskDate').val(),
        type: 'GET',
        success: function (data) {
            var html = '';
            $('#task_list').html('');
            html += '<option value="0">Select Task</option>';


            if (Object.keys(data).length > 0) {
                jQuery.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.task_name + '</option>';

                });
                $('#task_list').append(html);

                $('#task_list').change(function () {
                    if ($('#task_list').val() != 0) {
                        $('#task_list-error').text('');
                    }
                });
            }

        }
    });
}
 function  getProjectTaskList(projectId){
        jQuery.ajax({
            url: getsiteurl() + '/get/project/task_list/'+projectId+'/'+$('#taskDate').val(),
            type: 'GET',
            success: function(data){
                var html = '';
                $('#task_list').html('');
                html += '<option value="0">Select Task</option>';
                if(Object.keys(data).length > 0){
                    jQuery.each( data, function( key, value ) {
                        html += '<option value="'+value.id+'">'+value.task_name+'</option>';
                      });
                      $('#task_list').html(html);
                      $('#task_list').change(function(){
                            if($('#task_list').val() != 0){
                                $('#task_list-error').text('');
                            }
                      });
                }
                
            }
        });        
    }
$("#search_by_created_date").datepicker({
    todayBtn: 1,
    autoclose: true,
    format: 'yyyy-mm-dd',
});

$("#search_by_start_date").datepicker({
    todayBtn: 1,
    autoclose: true,
    format: 'dd-mm-yyyy',
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#search_by_end_date').datepicker('setStartDate', minDate);
});

$("#search_by_end_date").datepicker({
    autoclose: true,
    todayBtn: 1,
    format: 'dd-mm-yyyy',
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#search_by_start_date').datepicker('setEndDate', maxDate);
});

$("#reset_startdate").click(function () {
    $('#search_by_start_date').val('');
});

$("#reset_enddate").click(function () {
    $('#search_by_end_date').val('');
});

$("#reset_createddate").click(function () {
    $('#search_by_created_date').val('');
});

