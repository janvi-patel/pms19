 $( document ).ready(function() {
    $('[data-toggle="tooltip"]').tooltip(); 
    //For index
    $('#search_leave_start_date,#search_leave_end_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom auto"
    });

     //For create
    $('#cre_leave_start_date, #cre_leave_end_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
       // daysOfWeekDisabled: [0,6],
        orientation: "bottom auto"
    });
    $('#leave_start_type,#leave_end_type,.leave_approver,#leave_status').select2();
    $('#search_by_approver,#search_by_employee,#search_by_leave_status').select2();
    $('#update_checked_leave,#page_rang_dropdown').select2();
    
    $('#search_leave_start_date,#search_leave_end_date,#cre_leave_start_date, #cre_leave_end_date').attr('readonly','readonly');
     
    $("#task_leave_create").validate({
        rules: {
                leave_start_date:  {required:true,lessThanEqual:'#cre_leave_end_date'},               
                leave_end_date: {required:true,greaterThanEqual:'#cre_leave_start_date'},                
                reason: {required:true,minlength:6}                
        },
        messages: {
            leave_start_date:{required: "The leave start date field is required.",lessThanEqual :'The leave start date is less than end date'} ,
            leave_end_date:{required: "The leave end date field is required.",greaterThanEqual :'The leave end date is greater than start date'} ,
            reason: {
                required: "The reason field is required.",
                minlength : "The reason must be at least 6 characters.",
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    
    $(".update_leave_row").change(function(){
        if (!$(this).prop("checked")){
             $("#checkAll").prop("checked",false);
        }
    });
    $("#leave_update").validate({
        rules: {
            approver_comment: "required"        
        },
        messages: {
            approver_comment: "The approver comment field is required."
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#checkAll').click(function () {    
        $('input:checkbox').prop('checked', this.checked);    
    });
    
    $('#update_checked_leave').change(function(){
        var leaveIdArray = [];
        $.each($(".update_leave_row:checked"), function() {
            leaveIdArray.push($(this).val());
        });
        var action = $(this).val();
        if(action != '' && leaveIdArray.length != 0){
            if(action == 0){
                swal({
                    title: "Are you sure you want to delete?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete){
                    if (willDelete) {
                       updateLeaveConfirm(leaveIdArray,action);
                    }
                  });
            }else{
                swal({
                    title: "Are you sure you want to change the status?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete){
                    if (willDelete) {
                      updateLeaveConfirm(leaveIdArray,action);
                    }
                  });
            }
        }
    });
    
    // $('#page_rang_dropdown').change(function(){
    //     var limit = $(this).val();
    //     $('#page_range').val(limit);
    //     $('#search_filter').submit();
    // });

    $('#page_rang_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
       window.location = redirectLink;
    
    });
    
    $('.deleteleave').click(function(){
        var leaveId = $(this).attr('data-task-entry-id');
        if(leaveId != ''){
                swal({
                    title: "Are you sure you want to delete leave entry",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then(function(willDelete) {
                if (willDelete) {
                  deleteLeave(leaveId);
                }
             });
        }
    });	
 });
function deleteLeave(leaveId){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        jQuery.ajax({
        url  : getsiteurl() + '/leave_entry/delete',
        type : "DELETE",
        data: {_token: CSRF_TOKEN,leaveId:leaveId},
        success: function(response) {
            if(response.status == 'success'){
                swal(response.message,"", "success");
                location.reload();
            }else if(response.status == 'error'){
                swal(response.message,"", "error");
            }
        }
    });
}
 function  updateLeaveConfirm(leaveIdArray,action){
    $.ajax({
        url: getsiteurl() + '/leaves/updatecheckedleave',
        type: 'GET',
        data:{leaveIdArray: leaveIdArray,action: action},
        success: function(data){
            if(data.status == 'success'){
                swal(data.message,"", "success");
            }else if(data.status == 'error'){
                swal(data.message,"", "error");
            }
            location.reload();
        }
    });        
 }
 