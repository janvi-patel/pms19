$(document).ready(function(){
    // leave report
    $('[data-toggle="tooltip"]').tooltip(); 
    $('#search_leave_start_date,#search_leave_end_date,#search_by_start_date,#search_leave_date,#search_by_end_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
    $('#search_compoff_start_date, #search_compoff_end_date').datepicker({
        autoclose: true,
        todayHighlight: true, 
        format: 'dd-mm-yyyy'
    });
    // comp-off report && leave report
    $('#search_by_approver,#search_by_leave_status,#search_by_employee,#search_by_compoff_status,#search_by_reporting_to').select2();
    $('.select2').select2();
    // comp-off report
    
    $('#search_by_compoff_status,#page_rang_dropdown,#search_by_year,#search_by_month,#search_by_company').select2();
    // $('#page_rang_dropdown').change(function(){
    //     var limit = $(this).val();
    //     $('#page_range').val(limit);
    //     $('#search_filter').submit();
    // });

    $('#page_rang_dropdown').change(function(){
        var limit = $(this).val();
        var link  = window.location.pathname;
        var query_string = window.location.search;

        var search_str = (query_string.indexOf('?') == 0 ? query_string.substring(1) : query_string);
        if(limit != '' && query_string.indexOf('search_submit') != -1){
            if(query_string.indexOf('page_range')==1){
                var page_range = query_string.slice(0,query_string.indexOf('='));
                var srch_str = query_string.slice(query_string.indexOf('&'));
                var redirectLink = link +page_range+'='+limit+srch_str;
            }else{
                var redirectLink = link +'?page_range='+limit + '&'+search_str;
            }
        }else{
            var redirectLink = link +'?page_range='+limit;
        }
        // console.log(redirectLink);
       window.location = redirectLink;
    
    });

    $('.task-info').click(function dosomething(userId = '',page = 1,projectId = '',count){
        var count = (typeof count === 'undefined')?0:count;
        var project_id = $(this).attr('project_id');
        var user_id = $(this).attr('user_id');
        if(count == 1){
            project_id = projectId; 
            user_id = userId;
        }
        var url = getsiteurl() + '/reports/get_task_details/';
        $.ajax({
            type: "get",
            url: url,
            data: {'project_id':project_id,'user_id':user_id,'page':page},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#taskInfo').empty()
                $('#taskInfo').append(response);
                $('#taskInfo').modal('show');
                $('#taskInfoModal .pagination .page-item').first().css('display','none');
                $('#taskInfoModal .pagination .page-item').last().css('display','none');
                $('#taskInfoModal .pagination .page-item').on('click', function(e){
                    e.preventDefault();
                    var page = $($(this).html()).text();
                    dosomething(user_id,page,project_id,count= 1);
                });
            }
        });
        return false;
    });
});
    $('.edit_available_leaves,.edit_used_leaves').click(function(){
        $(this).prev().css('display','none');
        var newBeforeField = $('<input/>').attr('type','text').attr('value',$(this).prev().text())
                                .attr('name',this.id+'_val').attr('class',this.id+'_val');
        var newAfterField = $('<i/>').attr('class','fa fa-save '+this.id+'_save').attr('data-id',$(this).attr("data-id")).attr('data-value',this.id+'_save');
        $(newAfterField).on('click', editLeave);
        $(this).before(newBeforeField);
        $(this).after(newAfterField);
    });
    function editLeave(){
        var leaveType = '';
        var newVal = $('.'+$(this).attr("data-value").replace("_save", "_val")).val();
        var userId = $(this).attr("data-id");
        var leaveType = $(this).attr("data-value");
        if(newVal != '' && leaveType != ''){
            swal({
                title: "Are you sure you want to change leave.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              }).then((willDelete) => {
                  if (willDelete) {
                      $.ajax({
                              url: getsiteurl() + '/reports/updateLeave',
                              type: 'get',
                              data:{newVal: newVal,leaveType: leaveType,userId:userId },
                              headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                              success: function(data){
                                  location.reload();
                              }
                      });    
                  }
              });
        }
    }

$('#export_to_excel').on('click', function () {
    $('#submit_type').val('export_excel');
    $('#search_filter').submit();
})

$('#download_excel').on('click', function () {
    $('#submit_type').val('export');
    $('#search_filter').submit();
});
