<?php

use Monolog\Handler\StreamHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single'],
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 7,
        ],

        'daily_summary' => [
            'driver' => 'single',
            'path' => storage_path('summary_logs/'.date("Y").'/'.date("M").'/'.'daily_summary_log/'.date("Y-m-d").'.log'),
            'level' => 'info',
        ],

        'weekly_summary' => [
            'driver' => 'single',
            'path' => storage_path('summary_logs/'.date("Y").'/'.date("M").'/'.'weekly_summary_log/'.date("Y-m-d").'.log'),
            'level' => 'info',
        ],

        'monthly_summary' => [
            'driver' => 'single',
            'path' => storage_path('summary_logs/'.date("Y").'/'.date("M").'/'.'monthly_summary_log/'.date("Y-m-d").'.log'),
            'level' => 'info',
        ],

        'monthly_legged_avg' => [
            'driver' => 'single',
            'path' => storage_path('summary_logs/'.date("Y").'/'.date("M").'/'.'monthly_avg_legged_log/'.date("Y-m-d").'.log'),
            'level' => 'info',
        ],

        'monthly_working_avg' => [
            'driver' => 'single',
            'path' => storage_path('summary_logs/'.date("Y").'/'.date("M").'/'.'monthly_avg_working_log/'.date("Y-m-d").'.log'),
            'level' => 'info',
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
    ],

];
