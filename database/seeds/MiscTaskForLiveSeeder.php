<?php

use Illuminate\Database\Seeder;

class MiscTaskForLiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check_task = DB::connection('old_mysql')->table('misc_beanch_task')->where('created_at', '>=' ,'2019-08-09')->get();
        if(count($check_task)>0){
            foreach($check_task as $check_task_val){
                DB::connection('mysql')->table('misc_beanch_task')->insert([
                    'task_title'        => isset($check_task_val->task_title)?$check_task_val->task_title:NULL,
                    'project_id'        => $check_task_val->project_id,
                    'user_id'           => $check_task_val->user_id,
                    'old_task_id'       => isset($check_task_val->old_task_id)?$check_task_val->old_task_id:NULL,
                    'task_start_date'   => isset($check_task_val->task_start_date)?$check_task_val->task_start_date:NULL,
                    'task_end_date'     => isset($check_task_val->task_end_date)?$check_task_val->task_end_date:NULL,
                    'created_at'        => isset($check_task_val->created_at)?$check_task_val->created_at:NULL,
                    'updated_at'        => isset($check_task_val->updated_at)?$check_task_val->updated_at:NULL,
                    'deleted_at'        => isset($check_task_val->deleted_at)?$check_task_val->deleted_at:NULL,
                ]);
                $new_task_id = DB::getPdo()->lastInsertId();
                
                $old_task_entry = DB::connection('old_mysql')->table('misc_beanch_task_entries')->where('misc_beanch_task_id',$check_task_val->id)->get();
                if(count($old_task_entry)>0){
                    foreach($old_task_entry as $old_task_entry_val){
                        
                        $old_project_entry = DB::connection('old_mysql')->table('project_entries')->where('id',$old_task_entry_val->project_entries_id)->get();
                        
                        if(count($old_project_entry)>0){
                            foreach($old_project_entry as $old_project_entry_val){
                                DB::connection('mysql')->table('project_entries')->insert([
                                    'project_id'       => $old_project_entry_val->project_id,
                                    'user_id'          => $old_project_entry_val->user_id,
                                    'log_hours'        => $old_project_entry_val->log_hours,
                                    'desc'             => isset($old_project_entry_val->desc)?$old_project_entry_val->desc:NULL,
                                    'log_date'         => $old_project_entry_val->log_date,
                                    'created_at'       =>$old_project_entry_val->created_at,
                                    'updated_at'       => isset($old_project_entry_val->updated_at)?$old_project_entry_val->updated_at:NULL,
                                    'deleted_at'       => isset($old_project_entry_val->deleted_at)?$old_project_entry_val->deleted_at:NULL,
                                ]);
                                $new_project_id = DB::getPdo()->lastInsertId();
                            }
                        }
                        DB::connection('mysql')->table('misc_beanch_task_entries')->insert([
                                'project_entries_id'        => $new_project_id,
                                'misc_beanch_task_id'        => $new_task_id,
                                'created_at'        => $old_task_entry_val->created_at,
                                'updated_at'        => isset($old_task_entry_val->updated_at)?$old_task_entry_val->updated_at:NULL,
                                'deleted_at'        => isset($old_task_entry_val->deleted_at)?$old_task_entry_val->deleted_at:NULL,
                        ]);
                    }
                }
            }
        }
    }
}
