<?php

use Illuminate\Database\Seeder;

class ProjectEntriesTableShaligramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $old_task_entry = DB::connection('old_mysql')->table('dailytimeentry')
                ->LeftJoin('task_pms','dailytimeentry.TaskId','task_pms.TaskId')
                ->select('task_pms.ProjectId','dailytimeentry.EffortHours','dailytimeentry.CreatedBy',
                    'dailytimeentry.Comments','dailytimeentry.CreatedDate','dailytimeentry.ModifiedDate')
                ->where('dailytimeentry.EffortHours','>',0)
                ->get()->toArray();
        foreach ($old_task_entry as $key => $row) {
            $old_project_id = $row->ProjectId;
            $old_project_name = DB::connection('old_mysql')->table('project')->where('ProjectId',$old_project_id)->select('ProjectName')->first();
            if($old_project_name != ''){
                $new_project_id = DB::connection('mysql')->table('projects')->where('project_name',$old_project_name->ProjectName)->select('id')->first();
                if($new_project_id !=  ''){
                    $new_user_id = '';
                    $old_user_mail =  DB::connection('old_mysql')->table('employee')->where('EmployeeId',$row->CreatedBy)->select('EmailAddress')->first();
                    if($old_user_mail != ''){
                        $new_user_id =  DB::connection('mysql')->table('users')->where('email',$old_user_mail->EmailAddress)->select('id','employee_id')->first();
                    }   
                    if($new_project_id != '' && $new_user_id!= ''){
                        DB::connection('mysql')->table('project_entries')->insert([
                            'project_id'       => $new_project_id->id,
                            'user_id'          => $new_user_id->id,
                            'log_hours'        => $row->EffortHours,
                            'desc'             => $row->Comments,
                            'log_date'         => $row->CreatedDate,
                            'created_at'       => $row->CreatedDate,
                            'updated_at'       => $row->ModifiedDate,
                            'deleted_at'       => NULL,
                        ]);
                    }
                }
            }
        }
    }
}
