<?php

use Illuminate\Database\Seeder;
use App\LeaveDetail;
use App\Leave;
class UpdateLeaveDetailsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $leave = Leave::orderBy('id','asc')->where("leave_start_date",">=","2022-06-01")->withTrashed()->get()->toArray();
        foreach($leave as $key => $value){
            $this->updateLeaveDetail($value);
        }
    }

    function updateLeaveDetail($leave) {
        $leaveDetail = LeaveDetail::where("leave_group_id",$leave['id'])->get()->toArray();
        if(count($leaveDetail) > 0) {
            $leaveData = array(
                'approver_comment' => $leave['approver_comment'],
                'leave_status' => $leave['leave_status'],
                'approver_id' => $leave['approver_id'],
                'approver_date' => $leave['approver_date'],
                'deleted_at' => $leave['deleted_at'],
            );
            LeaveDetail::withTrashed()->where("leave_group_id",$leave['id'])->update($leaveData);
        } else {
            $leaveDetails = new LeaveDetail();
            $leaveDetails->createLeaveDetail($leave);
        }

        return true;
    }
}
