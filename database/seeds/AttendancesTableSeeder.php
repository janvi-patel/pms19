<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Attendance;
use App\AttendanceDetail;
use App\User;
use App\Company;
use App\Holiday;
use App\Setting;
class AttendancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { $total_working = 26064;
       
        $exclude_emp_code = array('TRD000','TRD003',' Employee Code ','C1','C2','C1-2066558','C2-6179053','Brinda Dubal','emp_name','C3-6014537','Employee Code','C4-6988963');
        $pms = DB::connection('old_mysql')->table('pms')->whereNotIn('emp_code',$exclude_emp_code)->get()->toArray();
//        $pms = DB::connection('old_mysql')->table('pms')->where('emp_code','TRD042')->where('month','07')->get()->toArray();
        $emp_key = [];
        $time_array = [];
        $j = 0;
        foreach ($pms as $key => $row) {
            $new_user_data = $new_cmp_name = '';
            if($row->emp_comp){
                $companyArray = config('constant.company');
                foreach($companyArray AS $cmpName => $companyVal){
                    if($cmpName == lcfirst($row->emp_comp)){
                        $companyId = $companyVal;
                    }
                }
            }
            if($companyId){
                 $companyObj = Company::find($companyId);
                 $new_empcode =  str_replace($companyObj->emp_short_name,'',$row->emp_code);
            }
            
            $old_user_mail =  DB::connection('old_mysql')->table('users')->where('employee_id',$row->emp_code)->select('email')->first();
            
            if($old_user_mail != ''){
                $new_user_data =  DB::connection('mysql')->table('users')->where('email',$old_user_mail->email)->select('first_name','last_name','employee_id','company_id')->first();
            }
            if($new_user_data != ''){
                $new_cmp_name =  DB::connection('mysql')->table('companies')->where('id',$new_user_data->company_id)->select('company_name')->first();
            }
            
            if(isset($new_user_data) && !empty($new_user_data)){               
                if (strpos($row->punch_data, ';')) {
                    $time_array = array_filter(explode(";", $row->punch_data));     
                } else {
                    $time_array = str_replace(":in", ":00", $row->punch_data);
                    $time_array = str_replace(":out", ":00", $time_array);
                    if (strpos($time_array, ':(')) {
                        $time_array = str_replace(":(", ":00(", $time_array);
                    }
                    $time_array = array_filter(explode("(Sales) ", $time_array));
                }
                if(count($time_array) <= 0){
                    $insert = [];
                    $insert['entry_date'] = $row->year.'-'.$row->month.'-'.$row->date;
                    $insert['emp_code'] = $new_user_data->employee_id;
                    $insert['emp_name'] = $new_user_data->first_name.' '.$new_user_data->last_name;
                    $insert['emp_comp'] = $new_cmp_name->company_name;
                    $insert['first_in'] = '00:00:00';
                    $insert['last_out'] = '00:00:00';
                    //$insert['absent'] = 1;
                    if(!Holiday::getHolidayDateList($row->year.'-'.$row->month.'-'.$row->date)){
                        $timestamp = strtotime($row->year.'-'.$row->month.'-'.$row->date);
                        $weekday= date("l", $timestamp );
                        if (($weekday !="Saturday" && $weekday !="Sunday")) {
                            $insert['absent'] = 1;
                        }
                    }
                    $attendance = Attendance::create($insert);
                }else{
                    for($i=0;$i<count($time_array);$i++){
                   if (strpos($time_array[$i], '(in)') !== false) {
                        $time = str_replace('(in)','',$time_array[$i]);
                    }elseif (strpos($time_array[$i], '(out)') !== false) {
                        $time = str_replace('(out)','',$time_array[$i]);
                    }elseif (strpos($time_array[$i], '(Sales') !== false) {
                        $time = str_replace('(Sales','',$time_array[$i]);
                    }elseif (strpos($time_array[$i], '(S') !== false) {
                        $time = str_replace('(S','',$time_array[$i]);
                    }else{
                        $time = $time_array[$i];
                    }
                    if(strpos($time, ':') !== false){
                        if(count(array_filter(explode(':',$time))) == 2){
                            $punch_time = preg_match('/:$/',$time) ? $time.'00' : $time.':00';
                        }elseif(count(array_filter(explode(':',$time))) == 1){
                            $punch_time = preg_match('/:$/',$time) ? $time.'00:00' : $time.':00:00';
                        }else{
                            $punch_time = $time;
                        }
                    }else{
                        $punch_time = '00:00:00';
                    }
                    if($new_user_data != ''){
                        $sortArr[$j]['fulldate'] = $row->date.'-'.$row->month.'-'.$row->year;
                        $sortArr[$j]['date'] = $row->year.'-'.$row->month.'-'.$row->date;
                        $sortArr[$j]['time'] = str_replace("(", "", $punch_time);
                        $sortArr[$j]['empcode'] = $new_user_data->employee_id;
                        $sortArr[$j]['name'] = $new_user_data->first_name.' '.$new_user_data->last_name;
                        $sortArr[$j]['company'] = $new_cmp_name->company_name;
                        $sortArr[$j]['dept'] = 'Default';
                    }
                    $j++;
               }
                }
            }else{
                $insert = [];
                $insert['entry_date'] = $row->year.'-'.$row->month.'-'.$row->date;
                $insert['emp_code'] = $new_user_data->employee_id;
                $insert['emp_name'] = $new_user_data->first_name.' '.$new_user_data->last_name;
                $insert['emp_comp'] = $new_cmp_name->company_name;
                $insert['first_in'] = '00:00:00';
                $insert['last_out'] = '00:00:00';
                $insert['absent'] = 1;

                $attendance = Attendance::create($insert);
            }
        }
        
        // Make in, out entry array
        $entries = [];
        $emp_key = [];
        if(!empty($sortArr)){
            
            foreach ($sortArr as $k => $row) 
            {
                $fulldate = $row['fulldate'];
                $date = $row['date'];
                $time = $row['time'];
                $empcode = $row['empcode'];
                $company = $row['company'];

                $name = $row['name'];
                $dept = $row['dept'];
                $emp_key[$empcode][$date] = isset($emp_key[$empcode][$date]) ? $emp_key[$empcode][$date] : 0;
                $entries[$date][$empcode]['emp_name'] = $name;

                $entries[$date][$empcode]['company'] = $company;
                $entries[$date][$empcode]['dept'] = $dept;

                if(!isset($entries[$date][$empcode][$emp_key[$empcode][$date]]['in']))
                    $entries[$date][$empcode][$emp_key[$empcode][$date]]['in'] = $time;
                else if(!isset($entries[$date][$empcode][$emp_key[$empcode][$date]]['out'])) {
                    $entries[$date][$empcode][$emp_key[$empcode][$date]]['out'] = isset($time) ? $time : '00:00:00';
                    $emp_key[$empcode][$date]++;
                }  

            }
            $i = 0;
            foreach($entries as $date => $entry) {
                foreach($entry as $empcode => $detail) {
                    if(isset($detail[0]['in']) && $detail[0]['in'] != '00:00:00') 
                    {
                        // dd($date);
                        $index = count($detail) - 4;
                        if(isset($detail[$index]['out'])){
                            $last_out_time = $detail[$index]['out'];
                            if(strlen($last_out_time)>8){
                              $last_out_time  = substr($last_out_time,0,8);
                            }
                        }else{ 
                            $last_out_time = $detail[$index]['in'];
                            if(strlen($last_out_time)>8){
                              $last_out_time  = substr($last_out_time,0,8);
                            }
                        }
                        $insert = [];
                        $insert['entry_date'] = $date;
                        $insert['emp_code'] = $empcode;
                        $insert['emp_name'] = isset($detail['emp_name']) ? $detail['emp_name'] : '';
                        $insert['emp_comp'] = isset($detail['company']) ? $detail['company'] : '';
                        $insert['emp_desg'] = isset($detail['dept']) ? $detail['dept'] : '';
                        $insert['first_in'] = preg_replace("/[^:0-9\s]/", "0", $detail[0]['in']);
                        $insert['last_out'] = preg_replace("/[^:0-9\s]/", "0", $last_out_time);

                        $attendance = Attendance::create($insert);// $emp_code
                        $insert_detail = [];

                        foreach($detail as $d => $time) {
                            if(isset($time['in'])) {
                                if(isset($time['out'])){
                                    $out_time = preg_replace('/\./', ':', $time['out']);
                                    if(strlen($out_time)>8){
                                      $out_time  = substr($out_time,0,8);
                                    }
                                }else{
                                    $out_time =  '00:00:00';
                                }
                                $in_time = preg_replace('/\./', ':', $time['in']);
                                if(strlen($in_time)>8){
                                    $in_time  = substr($in_time,0,8);
                                }
                                $insert_detail[$d]['attendance_id'] = $attendance->id;
                                $insert_detail[$d]['in_time'] = preg_replace("/[^:0-9\s]/", "0", $in_time);
                                $insert_detail[$d]['out_time'] = preg_replace("/[^:0-9\s]/", "0", $out_time);
                            }
                        }
                        AttendanceDetail::insert($insert_detail);
                        $attendance_detail = AttendanceDetail::selectRaw('SEC_TO_TIME(sum(TIME_TO_SEC(TIMEDIFF( out_time, in_time)))) AS working_hours')->where('attendance_id', $attendance->id)->first();

                        $parsed = date_parse($attendance_detail->working_hours);
                        $total_working = (int) $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];;
                        if($total_working <= 0) {
                            $attendance->incorrect_entry = 1;
                        }
                        else if($total_working >= config('constant.full_day_seconds') || ($total_working >= config('constant.full_day_seconds') && $total_working < config('constant.early_leave_end_seconds'))) {
                            $attendance->full_day = 1;
                            if($total_working >= config('constant.full_day_seconds') && $total_working < config('constant.early_leave_end_seconds')){
                                $attendance->early_going = 1;
                            }
                        }
                        else if($total_working >= config('constant.half_day_seconds') && $total_working < config('constant.full_day_seconds')) {
                            $attendance->half_day = 1;
                        }
                        else if($total_working > 0 && $total_working < config('constant.half_day_seconds')) {
                            $attendance->absent = 1;
                        }
                        $user = User::select('from_shift')->where('employee_id', $empcode)->first();
                        if($user) {
                            $parsed = date_parse($user->from_shift);
                            $shift_start = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];

                            $parsed = date_parse($attendance->first_in);
                            $first_in_seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
                            
                            $setting = new Setting();
                            $settingData = $setting->getSettingData('LateComeTime')[0];
                            
                            if($first_in_seconds > $shift_start + $settingData['constant_str_value'])
                                $attendance->late_comer = 1;
                        }

                        $attendance->save();

                    }
                    $i++;
                }

            }
        }
    }
}
