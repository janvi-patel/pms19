<?php

use Illuminate\Database\Seeder;

class ProjectTeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_id = 2; //Tridhya tech
        $projectId_array = array(458,516); //For Tridhya tech
        $new_user_id =  DB::connection('mysql')->table('users')->where('company_id',$company_id)->whereNull('deleted_at')->select('id')->get();
        if(count($new_user_id) != 0){
            foreach($new_user_id as $new_user_id_val){
                foreach($projectId_array as $projectId_array_val){
                    DB::connection('mysql')->table('project_team')->insert([
                            'project_id'       => $projectId_array_val,
                            'company_id'       => $company_id,
                            'user_id'          => $new_user_id_val->id,
                            'created_at'       => date("Y-m-d h:i:s"),
                            'updated_at'       => date("Y-m-d h:i:s"),
                            'deleted_at'       => NULL,
                    ]);
                }
            }
        }
        
    }
}
