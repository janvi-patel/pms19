<?php

use Illuminate\Database\Seeder;

class UserEmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $explodeIdArray = array(1,2);
        $companyId = 2;
        $users = DB::connection('mysql')->table('users')->select('email')
                ->where('company_id','=',$companyId)
                ->whereNotIn('id',$explodeIdArray)
                ->where('email','LIKE', '%tridhyatech.com%')->get()->toArray();
        foreach($users as $users){
            $expStr = (explode("@",$users->email));
            $removeDot = str_replace(".", "", $expStr[0]);
            $cmpName = 'tridhyatech';
            $mailStr = '@gmail.com';
            $newEmail = $removeDot.$cmpName.$mailStr;
            DB::connection('mysql')->table('users')
                        ->where('email',$users->email)->where('company_id',$companyId)
                        ->update(['email' => $newEmail, ]);
        }
        
        $explodeIdArray = array(1,2);
        $companyId = 2;
        $users = DB::connection('mysql')->table('users')->select('email')
                ->where('company_id','=',$companyId)
                ->whereNotIn('id',$explodeIdArray)
                ->where('email','LIKE', '%shaligraminfotech.com%')->get()->toArray();
        foreach($users as $users){
            $expStr = (explode("@",$users->email));
            $removeDot = str_replace(".", "", $expStr[0]);
            $cmpName = 'tridhyatech';
            $mailStr = '@gmail.com';
            $newEmail = $removeDot.$cmpName.$mailStr;
            DB::connection('mysql')->table('users')
                        ->where('email',$users->email)->where('company_id',$companyId)
                        ->update(['email' => $newEmail, ]);
        }
    }
}
