<?php

use Illuminate\Database\Seeder;

class ProjectTaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // For old to new by project Name
        /*$old_tasks = DB::connection('old_mysql')->table('tasks')->get()->toArray();
        foreach ($old_tasks as $key => $row) {
            $old_project_id = $row->project_id;
            $old_project_name = DB::connection('old_mysql')->table('projects')->where('id',$old_project_id)->select('project_name')->first();
            if($old_project_name != ''){
                $new_project_id = DB::connection('mysql')->table('projects')->where('project_name',$old_project_name->project_name)->select('id')->first();
                if($new_project_id !=  ''){
                    DB::connection('mysql')->table('project_tasks')->insert([
	                'task_name'          =>$row->task_name,
                        'created_by'         =>$row->user_id,
                        'user_id'            =>$row->user_id,
                        'project_id'         =>$new_project_id->id,
                        'estimated_hours'    =>$row->task_hours,
                        'created_at'         =>$row->created_at,
                        'updated_at'         =>$row->updated_at, 
                        'deleted_at'         =>$row->deleted_at,    
	            ]);
                }else{
                    $missing_project_id = DB::connection('mysql')->table('missing_projects')->where('project_id',$old_project_id)->select('id')->first();
                    if($missing_project_id == ''){
                        DB::connection('mysql')->table('missing_projects')->insert([
                            'project_id'          => $old_project_id,
                            'project_name'        => $old_project_name->project_name,
                         ]);   
                    }
                }
            }
        }*/
        
        /*
        //For old and new project id
        $project_data = DB::connection('mysql')->table('missing_projects')->get()->toArray();
        foreach ($project_data as $key => $row) {
            $new_projectId = $row->new_project_id;
            if($new_projectId != 0){
                $old_projectId = $row->project_id;
                $old_tasks = DB::connection('old_mysql')->table('tasks')->where('project_id',$old_projectId)->get()->toArray();
                if(count($old_tasks)>0){
                    foreach($old_tasks as $old_key => $old_val){
                        DB::connection('mysql')->table('project_tasks')->insert([
                            'task_name'          =>$old_val->task_name,
                            'created_by'         =>$old_val->user_id,
                            'user_id'            =>$old_val->user_id,
                            'project_id'         =>$row->new_project_id,
                            'estimated_hours'    =>$old_val->task_hours,
                            'created_at'         =>$old_val->created_at,
                            'updated_at'         =>$old_val->updated_at, 
                            'deleted_at'         =>$old_val->deleted_at,    
                        ]);
                    }
                }
            }
        }*/
        
        //For old and new project with multiple id
        $project_data = DB::connection('mysql')->table('missing_projects_2')->get()->toArray();
        foreach ($project_data as $key => $row) {
            $old_projectId = $row->old_id;
            $department = $row->department;
            $new_projectId = $row->new;
            if($old_projectId != 0){
                $old_tasks = DB::connection('old_mysql')->table('tasks')
                                ->leftJoin('users','tasks.user_id','users.id')
                                ->where('project_id',$old_projectId)
                                ->where('users.department',$department)
                                ->whereNull('tasks.deleted_at')
                                ->get()->toArray();
                
                if(count($old_tasks)>0){
                    foreach($old_tasks as $old_key => $old_val){
//                        echo "<pre>";print_R($old_val);
                        DB::connection('mysql')->table('project_tasks')->insert([
                            'task_name'          =>$old_val->task_name,
                            'created_by'         =>$old_val->user_id,
                            'user_id'            =>$old_val->user_id,
                            'project_id'         =>$new_projectId,
                            'estimated_hours'    =>$old_val->task_hours,
                            'created_at'         => date('Y-m-d h:i:s', time()),
                            'updated_at'         => date('Y-m-d h:i:s', time()), 
                            'deleted_at'         => date('Y-m-d h:i:s', time()),    
                        ]);
                     }
                }
            }
        }
    }
}
