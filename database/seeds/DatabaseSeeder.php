<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProjectTlPmUpdate::class);
        
        
        $this->call(AttendancesTableSeeder::class);
        $this->call(LeavesTableSeeder::class);
        $this->call(CompoffsTableSeeder::class);
        $this->call(ProjectEntriesTableSeeder::class);
        $this->call(ProjectTeamTableSeeder::class);
        //$this->call(TasksTableSeeder::class);
    }
}
