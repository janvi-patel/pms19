<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //For saligram user update data
        
        /*$users = DB::connection('mysql')->table('users')->get();
        $saligram_users = DB::connection('mysql')->table('saligram_user')->get();
        foreach($saligram_users as $saligram_users_val){
            $check_user = DB::connection('mysql')->table('users')->where('email',$saligram_users_val->email)->get();
            if(count($check_user)>0){
                DB::connection('mysql')->table('users')
                        ->where('email',$saligram_users_val->email)->where('company_id',1)
                        ->update([
                                'first_name' => ($saligram_users_val->first_name != '')?"$saligram_users_val->first_name":NULL,
                                'last_name' => ($saligram_users_val->last_name != '')?"$saligram_users_val->last_name":NULL,
                                'employee_id' => ($saligram_users_val->emp_code != '')?substr($saligram_users_val->emp_code, 3):NULL,
                                'phone' => ($saligram_users_val->phone != '')?($saligram_users_val->phone):NULL,
                                'birthdate' => ($saligram_users_val->birthday != '' && $saligram_users_val->birthday != '0000-00-00')?"$saligram_users_val->birthday":NULL,
                                'joining_date' => ($saligram_users_val->joining_date != '' && $saligram_users_val->joining_date != '0000-00-00 00:00:00')?"$saligram_users_val->joining_date":NULL,
                        ]);
            }
        }*/
        
        //For old pms user add
        $old_users = DB::connection('old_mysql')->table('users')->get();

        foreach ($old_users as $user) {
            $password = (($user->password) != '')?trim($user->password):NULL;
            $email_verified = ($user->email_verified_at != '')?trim($user->email_verified_at):NULL;
            DB::connection('mysql')->table('users')->insert([
                'id' => ($user->id != '')?trim($user->id):NULL,
                'company_id' => 1,
                'designation_id' => ($user->designation_id != '')?trim($user->designation_id):NULL,
                'employee_id' => ($user->employee_id != '')?preg_replace("/[^0-9]/", "", $user->employee_id):0,
                'first_name' => ($user->first_name != '')?trim($user->first_name):NULL, 
                'last_name' => ($user->last_name != '')?trim($user->last_name):NULL,
                'email' => ($user->email != '')?trim($user->email):NULL,
                'from_shift' => '10:30:00',
                'to_shift' => '19:30:00',
                'personal_email' => ($user->personal_email != '')?trim($user->personal_email):NULL,
                'phone' => ($user->phone != '')?trim($user->phone):NULL,
                'email_verified_at' => ($email_verified == '0000-00-00 00:00:00')?NULL:$email_verified,
                'password' => "$password",
                'is_admin' => ($user->is_admin != '')?trim($user->is_admin):NULL,
                'remember_token' => ($user->remember_token != '')?trim($user->remember_token):NULL,
                'birthdate' => ($user->birthdate != '')?trim($user->birthdate):NULL,
                'joining_date' => ($user->joining_date != '')?trim($user->joining_date):NULL,
                'department' => ($user->department != '')?trim($user->department):1,
                'bank_name' => ($user->bank_name != '')?trim($user->bank_name):NULL,
                'bank_account_no' => ($user->bank_account_no != '')?trim($user->bank_account_no):NULL,
                'pan_no'=> ($user->pan_no != '')?trim($user->pan_no):NULL,
                'employee_status' => ($user->employee_status != '')?trim($user->employee_status):1,
                'available_leaves' => ($user->available_leaves != '')?trim($user->available_leaves):0.00,
                'used_leaves' => ($user->used_leaves != '')?trim($user->used_leaves):0.00,
                'status' => ($user->status != '')?trim($user->status):1,
                'reporting_to' => ($user->reporting_to != '')?trim($user->reporting_to):0,
                
            ]);
        }
        DB::connection('mysql')->table('users')->insert([
                'company_id' => 1,
                'designation_id' => 1,
                'employee_id' => 001,
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'email' => 'superadmin@gmail.com',
                'personal_email' => 'superpersonalq123@gmail.com',
                'phone' => 9000074513,
                'email_verified_at' => $user->email_verified_at,
                'password' => '$2y$10$Rwh46.lUU98TcPOP/qwL7.uy8Lcno4hv1F0G54/PxFXuRFP6UzaMi',
                'is_admin' => NULL,
                'remember_token' => '9BjxRYrMkR1rXhHXmIFpDlOBzb5AE5hP7mjCuOuylDm607mP6KSkcKS0Kiar',
                'birthdate' => '2019-03-15',
                'joining_date' => '2019-01-01',
                'department' => 1,
                'bank_name' => 'HDFC',
                'bank_account_no' => '132465465978',
                'pan_no'=> 'DKMD123456W',
                'employee_status' =>1,
                'available_leaves' => 0,
                'used_leaves' => 0,
                'status' => 1,
                'reporting_to' => 0,
                
            ]);
    }
}
