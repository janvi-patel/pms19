<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Attendance;
use App\AttendanceDetail;
use App\User;
use App\Company;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $old_tasks = DB::connection('old_mysql')->table('tasks')->get()->toArray();
        foreach ($old_tasks as $key => $row) {
            $old_project_id = $row->project_id;
            $old_project_name = DB::connection('old_mysql')->table('projects')->where('id',$old_project_id)->select('project_name')->first();
            if($old_project_name != ''){
                $new_project_id = DB::connection('mysql')->table('projects')->where('project_name',$old_project_name->project_name)->select('id')->first();
                if($new_project_id !=  ''){
                    $old_task_id = $row->id;
                    //$userId = $row->user_id;
                    
                    $new_created_id = $new_user_id = '';
                    $old_user_mail =  DB::connection('old_mysql')->table('users')->where('id',$row->user_id)->select('email')->first();
                    if($old_user_mail != ''){
                        $new_user_id =  DB::connection('mysql')->table('users')->where('email',$old_user_mail->email)->select('id','employee_id')->first();
                    }            
                    $old_approver_mail =  DB::connection('old_mysql')->table('users')->where('id',$row->created_by)->select('email')->first();
                    if($old_approver_mail != ''){
                        $new_created_id =  DB::connection('mysql')->table('users')->where('email',$old_approver_mail->email)->select('id')->first();
                    }
            
                    DB::connection('mysql')->table('tasks')->insert([
	                'project_id'        =>$new_project_id->id,
                        'user_id'           =>$new_user_id->id,
                        'created_by'        =>$new_created_id->id,
                        'task_name'         =>$row->task_name,
                        'task_start_date'   =>$row->task_start_date,
                        'task_end_date'     =>$row->task_end_date,
                        'task_status'       =>$row->task_status,
                        'task_hours'        =>$row->task_hours,
                        'created_at'        =>$row->created_at,
                        'updated_at'        =>$row->updated_at, 
                        'deleted_at'        =>$row->deleted_at,    
	            ]);
                    $newTaskID = DB::getPdo()->lastInsertId();
                    
                    $old_task_entry = DB::connection('old_mysql')->table('task_entries')->where('task_id',$old_task_id)->get()->toArray();
                    foreach($old_task_entry as $old_task_entry_val){
                        DB::connection('mysql')->table('task_entries')->insert([
                            'task_id'          =>$newTaskID,
                            'emp_code'         =>$new_user_id->employee_id,
                            'log_date'         =>$old_task_entry_val->log_date,
                            'log_hours'        =>$old_task_entry_val->log_hours,
                            'log_description'  =>$old_task_entry_val->log_description,
                            'created_at'       =>$old_task_entry_val->created_at,
                            'updated_at'       =>$old_task_entry_val->updated_at,
                            'deleted_at'       =>$old_task_entry_val->deleted_at,
                           
                        ]);
                    }
                }else{
                    $missing_project_id = DB::connection('mysql')->table('missing_projects')->where('project_id',$old_project_id)->select('id')->first();
                    if($missing_project_id == ''){
                        DB::connection('mysql')->table('missing_projects')->insert([
                            'project_id'          => $old_project_id,
                            'project_name'        => $old_project_name->project_name,
                         ]);   
                    }
                }
            }
        }
    }
}

