<?php

use Illuminate\Database\Seeder;

class SyncLeaveBalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "01" => ["month" => "04", "year" => "2022"],
            "02" => ["month" => "05", "year" => "2022"],
            "03" => ["month" => "06", "year" => "2022"],
            "04" => ["month" => "07", "year" => "2022"],
            "05" => ["month" => "08", "year" => "2022"],
            "06" => ["month" => "09", "year" => "2022"],
            "07" => ["month" => "10", "year" => "2022"],
            "08" => ["month" => "11", "year" => "2022"],
            "09" => ["month" => "12", "year" => "2022"],
            "10" => ["month" => "01", "year" => "2023"],
            "11" => ["month" => "02", "year" => "2023"],
            "12" => ["month" => "03", "year" => "2023"],
        ];
        foreach($data as $array) {
            syncMontlyBalanceLeave($array);
        }
    }
}
