<?php

use Illuminate\Database\Seeder;

class ProjectTlPmShaligramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $excludeEmp = array('viraj.p@shaligraminfotech.com','parth.patel@shaligraminfotech.com','gaurang.p@tridhya.com'
             ,'dipak.p@tridhya.com','hiren.d@sgit.in','sagar.s@tridhya.com');

        $old_project = DB::connection('old_mysql')->table('project')
                ->leftJoin('projectassociatepmtl as pa','project.ProjectId','pa.ProjectId')
                ->select('project.ProjectName','project.ProjectManagerId as pmId','project.ProjectId','pa.EmployeeId as tlId')
                ->groupBy('project.ProjectId')->get()->toArray();
        
        foreach($old_project as $old_project_val ){
            if(isset($old_project_val->tlId)){
                $getOldTlData = DB::connection('old_mysql')->table('employee')
                            ->where('EmployeeId',$old_project_val->tlId)
                            ->select('EmailAddress')->first();

                if(isset($getOldTlData->EmailAddress)){
                    $getNewTlId =  DB::connection('mysql')->table('users')
                                    ->where('email',$getOldTlData->EmailAddress)
                                    ->whereNotIn('email',$excludeEmp)
                                    ->select('id')->first();
                    if($getNewTlId != ''){
                        $getNewTlId =  DB::connection('mysql')->table('users')
                         ->where('email','am.msp@msp-group.co.uk')
                         ->select('id')->first();
                    }
                }
            }else{
                $getNewTlId =  DB::connection('mysql')->table('users')
                         ->where('email','am.msp@msp-group.co.uk')
                         ->select('id')->first();
            }
            if(isset($old_project_val->pmId)){
                $getOldPmData = DB::connection('old_mysql')->table('employee')
                            ->where('EmployeeId',$old_project_val->pmId)
                            ->select('EmailAddress')->first();

                if(isset($getOldPmData->EmailAddress)){
                       $getNewPmId =  DB::connection('mysql')->table('users')
                                ->where('email',$getOldPmData->EmailAddress)
                                ->select('id')->first();
                }
            }else{
                $getNewPmId = '';
            }
            
            DB::connection('mysql')->table('projects')
                ->where('project_name',$old_project_val->ProjectName)
                ->where('assigned_to',1)
                ->update([
                        'project_manager' => ($getNewPmId != '')?"$getNewPmId->id":NULL,
                        'team_leader' => ($getNewTlId != '')?"$getNewTlId->id":NULL
            ]);
            
        }
    }
}
