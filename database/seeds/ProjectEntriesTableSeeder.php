<?php

use Illuminate\Database\Seeder;

class ProjectEntriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::raw("sum(task_entries.log_hours) as log_hours")
        $company_id = 2;
        $old_task_entry = DB::connection('old_mysql')->table('task_entries')
                ->LeftJoin('tasks','task_entries.task_id','tasks.id')
                ->select('tasks.project_id','tasks.user_id','tasks.task_name',
                        'tasks.task_start_date','tasks.task_end_date','tasks.id as oldTaskId',
                        'tasks.created_at as task_created','tasks.updated_at as task_updated','tasks.deleted_at as task_deleted',
                        'task_entries.log_date','task_entries.log_hours','task_entries.log_description',
                        'task_entries.created_at as te_created','task_entries.updated_at as te_updated','task_entries.deleted_at as te_deleted')
                ->get()->toArray();
        $totalTaskLogHours = 0;
        foreach ($old_task_entry as $key => $row) {
            $old_project_id = $row->project_id;
            $old_project_name = DB::connection('old_mysql')->table('projects')->where('id',$old_project_id)->select('project_name')->first();
            if($old_project_name != ''){
                $new_project_id = DB::connection('mysql')->table('projects')->where('project_name',$old_project_name->project_name)
                        ->where('assigned_to',$company_id)->select('id')->first();
                
                if($new_project_id !=  ''){
                    $new_user_id = '';
                    $old_user_mail =  DB::connection('old_mysql')->table('users')->where('id',$row->user_id)->select('email')->first();
                    if($old_user_mail != ''){
                        $new_user_id =  DB::connection('mysql')->table('users')->where('email',$old_user_mail->email)->select('id','employee_id')->first();
                    }   
                    if($new_project_id != '' && $new_user_id!= ''){
                        DB::connection('mysql')->table('project_entries')->insert([
                            'project_id'       => $new_project_id->id,
                            'user_id'          => $new_user_id->id,
                            'log_hours'        => $row->log_hours,
                            'desc'             => $row->log_description,
                            'log_date'         => $row->log_date,
                            'created_at'       => $row->te_created,
                            'updated_at'       => $row->te_updated,
                            'deleted_at'       => $row->te_deleted,
                        ]);
                        $project_entries_id = DB::getPdo()->lastInsertId();
                        
                        if($old_project_name->project_name == 'Miscellaneous Tasks'){
                            $check_task = DB::connection('mysql')->table('misc_beanch_task')->where('old_task_id',$row->oldTaskId)->select('id')->first();
                            if($check_task == ''){
                                DB::connection('mysql')->table('misc_beanch_task')->insert([
                                    'task_title'        => isset($row->task_name)?$row->task_name:NULL,
                                    'project_id'        => $new_project_id->id,
                                    'user_id'           => $new_user_id->id,
                                    'old_task_id'       => $row->oldTaskId,
                                    'task_start_date'   => $row->task_start_date,
                                    'task_end_date'     => $row->task_end_date,
                                    'created_at'        => $row->task_created,
                                    'updated_at'        => $row->task_updated,
                                    'deleted_at'        => $row->task_deleted,
                                ]);
                                $task_id = DB::getPdo()->lastInsertId();
                            }else{
                                $task_id =  $check_task->id;
                            }
                            DB::connection('mysql')->table('misc_beanch_task_entries')->insert([
                                'project_entries_id'        => $project_entries_id,
                                'misc_beanch_task_id'        => $task_id,
                                'created_at'        => $row->te_created,
                                'updated_at'        => $row->te_updated,
                                'deleted_at'        => $row->te_deleted,
                            ]);
                        }
                    }
                }else{
                    $missing_project_id = DB::connection('mysql')->table('missing_projects')->where('project_id',$old_project_id)->select('id')->first();
                    if($missing_project_id == ''){
                        DB::connection('mysql')->table('missing_projects')->insert([
                            'project_id'          => $old_project_id,
                            'project_name'        => $old_project_name->project_name,
                         ]);   
                    }
                }
            }
        }
    }
}
