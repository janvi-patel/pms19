<?php

use Illuminate\Database\Seeder;
use App\Leave;
use App\LeaveDetail;
use App\User;
use App\Company;
use App\Attendance;
class UpdateLeaveModules extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $leave = Leave::orderBy('id','asc')->withTrashed()->get()->toArray();
        $obj_leave_detail = new LeaveDetail();
        foreach($leave as $key => $value){
            $obj_leave_detail->createLeaveDetail($value);
        }
        // $userData = User::withTrashed()->get();
        // foreach ($userData as $key => $value) {
        //     $companyObj = Company::find($value->company_id);
        //     $companyName = '';
        //     if(isset($companyObj->company_name)){
        //         $companyName = $companyObj->company_name;
        //     }
        //     $attendanceData = Attendance::where('emp_code',$value->employee_id)->where('emp_comp',$companyName)->update(['user_id' => $value->id]);
        // }
    }
}
