<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\User;

class LeavesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $old_leaves = DB::connection('old_mysql')->table('leaves')->get();

        foreach ($old_leaves as $leaves) {
            $new_approver_id = $new_user_id = '';
            $old_user_mail =  DB::connection('old_mysql')->table('users')->where('id',$leaves->user_id)->select('email')->first();
            if($old_user_mail != ''){
                $new_user_id =  DB::connection('mysql')->table('users')->where('email',$old_user_mail->email)->select('id')->first();
            }            
            $old_approver_mail =  DB::connection('old_mysql')->table('users')->where('id',$leaves->approver_id)->select('email')->first();
            if($old_approver_mail != ''){
                $new_approver_id =  DB::connection('mysql')->table('users')->where('email',$old_approver_mail->email)->select('id')->first();
            }
            if($new_user_id != '' && $new_approver_id != ''){
                DB::connection('mysql')->table('leaves')->insert([
                   'user_id'  => $new_user_id->id,
                   'approver_id'=> $new_approver_id->id,
                   'leave_start_date'=> $leaves->leave_start_date,
                   'leave_end_date'=> $leaves->leave_end_date, 
                   'leave_start_type'=> $leaves->leave_start_type,
                   'leave_end_type'=> $leaves->leave_end_type,
                   'return_date'=> $leaves->return_date,
                   'leave_days'=> $leaves->leave_days,
                   'reason'=> $leaves->reason,
                   'approver_comment'=> $leaves->approver_comment, 
                   'leave_status'=> $leaves->leave_status,
                   'created_at'=> $leaves->created_at,
                   'updated_at'=> $leaves->updated_at,
                   'deleted_at'=> $leaves->deleted_at
                ]);
            }
        }
    }
}
