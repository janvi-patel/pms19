<?php

use Illuminate\Database\Seeder;

class ProjectTlPmUpdate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_id = 2;
        $temp_project = DB::connection('mysql')->table('temp_project')->select('pm_name','tl_name','project_name')->get()->toArray();
        foreach ($temp_project as $temp_project_val) {
            $project_pm_id = DB::connection('mysql')->table('users')->select('id')->where(DB::raw('CONCAT(first_name, " ",last_name)'),$temp_project_val->pm_name)->first();
            $project_tl_id = DB::connection('mysql')->table('users')->select('id')->where(DB::raw('CONCAT(first_name, " ",last_name)'),$temp_project_val->tl_name)->first();
            if(!empty($project_pm_id) || !empty($project_tl_id)){
                DB::connection('mysql')->table('temp_project_with_id')->insert([
                        'project_name'  => $temp_project_val->project_name,
                        'pm_id'=> isset($project_pm_id)?$project_pm_id->id:4,
                        'tl_id'=> isset($project_tl_id)?$project_tl_id->id:36,
                ]);
            }
        }
        $project_name_get = DB::connection('mysql')->table('temp_project_with_id')->whereNotNull('tl_id')->get()->toArray();
        foreach($project_name_get as $project_name_get_val){
            $projects = DB::connection('mysql')->table('projects')->select('id')->where('project_name',$project_name_get_val->project_name)
                        ->where('assigned_to',$company_id)->first();
            if(!empty($projects)){
                DB::connection('mysql')->table('projects')
                        ->where('id',$projects->id)
                        ->update([
                                'project_manager' => ($project_name_get_val->pm_id != '')?"$project_name_get_val->pm_id":NULL,
                                'team_leader' => ($project_name_get_val->tl_id != '')?"$project_name_get_val->tl_id":NULL
                        ]);
            }
        }
    }
}
