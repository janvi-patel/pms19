<?php

use Illuminate\Database\Seeder;
use App\User;
class UserLeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_id  = 2;
        /*$temp_user = DB::connection('mysql')->table('users_leave_data')->get();
        foreach($temp_user as $temp_user_val){
            DB::connection('mysql')->table('users')
                    ->where('email',$temp_user_val->email)
                    ->update([
                            'available_leaves' => $temp_user_val->availavailable_leaves,
                            'used_leaves' => $temp_user_val->used_leaves 
                    ]);
            
        }*/
        
        $temp_user = DB::connection('mysql')->table('user_confirmation')->get();
        foreach($temp_user as $temp_user_val){
            $empCodeLeng = strlen((string)$temp_user_val->emp_code);
            $empCode = ($empCodeLeng != 3)?(($empCodeLeng == 2)?'0'.$temp_user_val->emp_code:'00'.$temp_user_val->emp_code):$temp_user_val->emp_code;
            $userData = User::where('employee_id',$empCode)->where('company_id',$company_id)->first();
            if($userData){
                $userData->confirmation_date = ($temp_user_val->date ==  '0000-00-00' || $temp_user_val->date == '')?NULL:date('Y-m-d',strtotime($temp_user_val->date));
                $userData->save();
            }
        }
    }
}
