<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Attendance;

class LateCommer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('company_id', 2)->get();
        foreach ($users as $key => $value) {
            $shift = strtotime($value->from_shift);
            $startTime = date("H:i:s", strtotime('+30 minutes', $shift));
            $attendance_details = Attendance::where('emp_code', $value->employee_id)->whereBetween('entry_date', array('2019-09-01', '2019-09-30'))->where('first_in','>',$startTime)->get();
            if (!empty($attendance_details)) { 
                foreach($attendance_details as $att){
                   Attendance::where('id',$att->id)->update(array('late_comer'=>1)); 
                }
            }
        }
    }
}
