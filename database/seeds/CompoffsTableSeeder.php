<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CompoffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $old_compoffs = DB::connection('old_mysql')->table('comp_offs')->get();

        foreach ($old_compoffs as $comp_offs) {
            $new_approver_id = $new_user_id = '';
            $old_user_mail =  DB::connection('old_mysql')->table('users')->where('id',$comp_offs->user_id)->select('email')->first();
            if($old_user_mail != ''){
                $new_user_id =  DB::connection('mysql')->table('users')->where('email',$old_user_mail->email)->select('id')->first();
            }
            $old_approver_mail =  DB::connection('old_mysql')->table('users')->where('id',$comp_offs->approver_id)->select('email')->first();
            if($old_approver_mail != ''){
                $new_approver_id =  DB::connection('mysql')->table('users')->where('email',$old_approver_mail->email)->select('id')->first();
            }
            if($new_user_id != '' && $new_approver_id != ''){
                DB::connection('mysql')->table('comp_offs')->insert([
                   'user_id' =>$new_user_id->id,
                   'approver_id' =>$new_approver_id->id,
                   'compoff_start_date' =>$comp_offs->compoff_start_date,
                   'compoff_end_date' =>$comp_offs->compoff_end_date,
                   'compoff_start_type' =>$comp_offs->compoff_start_type,
                   'compoff_end_type' =>$comp_offs->compoff_end_type,
                   'compoff_days' =>$comp_offs->compoff_days,
                   'compoff_description' =>$comp_offs->compoff_description,
                   'approver_comment' =>$comp_offs->approver_comment,
                   'compoff_status' =>$comp_offs->compoff_status,
                   'created_at' =>$comp_offs->created_at,
                   'updated_at' =>$comp_offs->updated_at,
                   'deleted_at' =>$comp_offs->deleted_at,
                   'compoff_marked_as' =>$comp_offs->compoff_marked_as,
                   'compoff_encash_month_year' =>$comp_offs->compoff_encash_month_year
                ]);
            }
        }
    }
}
