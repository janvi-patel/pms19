<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillableReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billable_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->double('billable_hours', 8, 2);
            $table->double('logged_hours', 8, 2);
            $table->double('non_billable_hours', 8, 2);
            $table->double('non_billable_logged_hours', 8, 2);
            $table->double('working_days', 8, 2);
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billable_reports');
    }
}
